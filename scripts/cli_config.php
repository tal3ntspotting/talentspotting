<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '4000M');

defined('ABSOLUTE_PATH') || define('ABSOLUTE_PATH', realpath(dirname(__FILE__) . '/..'));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', ABSOLUTE_PATH . '/application');
$inis = parse_ini_file(APPLICATION_PATH.'/configs/application.ini');
$sitename = $inis['resources.Settings.sitename'];
defined('APPLICATION_URL') || define('APPLICATION_URL', $sitename);

switch (@$_SERVER['APPLICATION_ENV']) {
    case 'production':
        error_reporting(0);
        ini_set('display_errors', 0);
        $env = 'production';
        break;

    case 'staging':
        error_reporting(0);
        ini_set('display_errors', 0);
        $env = 'staging';
        break;

    case 'development':
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'development';
        break;
    case "testing":
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'testing';
        break;

    default:
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'production';
}

defined('APPLICATION_ENV') || define('APPLICATION_ENV', ($env ? $env : 'production'));

set_include_path(implode(PATH_SEPARATOR, array(
    ABSOLUTE_PATH . '/library',
    APPLICATION_PATH
)));

include_once 'WebApp/Tools.php';

WebApp_Tools::initRuntime();

/** Zend_Application */
include_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
);



include_once 'smarty/Smarty.class.php';
include_once 'smarty/SmartyBC.class.php';

$config = $application->getOptions();

$cache = Zend_Cache::factory($config['resources']['Cache']['frontend'], $config['resources']['Cache']['backend'], $config['resources']['Cache']['frontendOptions'], $config['resources']['Cache']['backendOptions']);
Zend_Registry::set('Zend_Cache_Core', $cache);
Zend_Registry::set('queryCacheEnabled', $config['resources']['Cache']['queryCacheEnabled']);

$dbAdapter = Zend_Db::factory($config['resources']['Db']['adapter'], $config['resources']['Db']['params']);
$client = ($argv[1]);
if(isset($config['resources']['Db']['clients'][$client]) && $config['resources']['Db']['clients'][$client] != "")
{
    $config['resources']['Db']['params']['dbname'] = $config['resources']['Db']['clients'][$client];
    $dbAdapter = Zend_Db::factory($config['resources']['Db']['adapter'], $config['resources']['Db']['params']);
}

Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);

Zend_Registry::set('dbMysqlAdapter', $dbAdapter);
Zend_Registry::set('_LANG_DETAILS', 1);
Zend_Registry::set('Zend_Translate', []);
