<?php 

// this cron is maded to separate candiate from recommenders after track_time end ( Just hired user they will stay connected )
// this cron should be runned daily

require_once __DIR__."/../cli_config.php" ;
$cron = new models_Cron;
    $result = $cron->trackTimeLine();
    foreach($result as $key => $company) {
        $today = strtotime(date("Y-m-d H:i:s"));
        $track_timeline_days = $company['track_timeline']*30;
        $created = strtotime($company['created']);
        $users = $company['users'];
        
        $time_stamp_difference =   $today - $created ;
        $days = intval($time_stamp_difference/86400);

        if($days % $track_timeline_days == 0) {
            foreach($users as $user) {
                $isHired = $cron->IsUserHired($user['id_user']);
                if(empty($isHired)) {
                    // remove recommender
                    $cron->removeParent($user['id_user']);
                }
            }
        }
    }
?>
