<?php 

    /**
     *  This Job should be run every day to check if any campaign is finished 
     */

require_once __DIR__."/../cli_config.php" ;
$cron = new models_Cron ;
$result = $cron->CampaignBounty();
if(!empty($result)) {
    Foreach($result as $key => $company) {
        $campaigns = $company['campaigns'];
        foreach($campaigns as $campaign) {
            $data['id_company'] = $company['id_company'];
            $data['id_campaign'] = $campaign['id_campaign'];
            $data['end'] = $campaign['campaign_deadline'];
            $data['status'] = "initiated";
            $data['bounty'] = $campaign['bounty'];
            $cron->setCampaignBounty($data);
            $cron->disableCampaignBounty($data['id_campaign']);
        }
    }
}

?>
