<?php


ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

$_SERVER['HTTP_HOST'] = 'www';
$_SERVER['REQUEST_URI'] = 'www';
$_SERVER['SERVER_NAME'] = 'lh.talentspotting.com';
$_SERVER['REMOTE_ADDR'] = 'lh.talentspotting.com';
$_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36';
defined('ABSOLUTE_PATH') || define('ABSOLUTE_PATH', realpath(dirname(__FILE__) . '/..'));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', ABSOLUTE_PATH . '/application');
defined('APPLICATION_URL') || define('APPLICATION_URL', 'http://lh.talentspotting.com');


set_include_path(implode(PATH_SEPARATOR, array(
    ABSOLUTE_PATH . '/library',
    APPLICATION_PATH,
    get_include_path()
)));



/** Zend_Application */
include_once 'Zend/Application.php';
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';
require_once 'Zend/Test/PHPUnit/DatabaseTestCase.php';
require_once APPLICATION_PATH . '/../tests/Zend.php';
require_once APPLICATION_PATH . '/../tests/ZendIntegration.php';
require_once APPLICATION_PATH . '/../tests/Helper.php';

// Create application, bootstrap, and run
//$application = new Zend_Application(
//    APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
//);
//$application->bootstrap()->run();
//
//include_once 'smarty/Smarty.class.php';
//include_once 'smarty/SmartyBC.class.php';
//
//$config = $application->getOptions();
//
//$cache = Zend_Cache::factory($config['resources']['Cache']['frontend'], $config['resources']['Cache']['backend'], $config['resources']['Cache']['frontendOptions'], $config['resources']['Cache']['backendOptions']);
//Zend_Registry::set('Zend_Cache_Core', $cache);
//Zend_Registry::set('queryCacheEnabled', $config['resources']['Cache']['queryCacheEnabled']);
//
//
//$dbAdapter = Zend_Db::factory($config['resources']['Db']['adapter'], $config['resources']['Db']['params']);
//Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
//
//Zend_Registry::set('dbMysqlAdapter', $dbAdapter);
//Zend_Registry::set('_LANG_DETAILS', 1);
