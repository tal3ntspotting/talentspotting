<?php


// //https://jsfiddle.net/carm3n/t6ac1qso/27/

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

class activeJobRecommender extends models_BaseCron
{

    public function getData()
    {
        return $this->_cronModel->getAllUsersByCompany(92, 1);
    }

    public function execute()
    {
        $data = $this->getData();
        foreach ($data as $key => $value) {

            if (!empty($value['users'])) {
                foreach ($value['users'] as $user) {
                    $id_user = $user['id_user'];
                    $user_array = (object) $this->_UserModel->getUserById($id_user);
                    Zend_Auth::getInstance()->getStorage()->write($user_array);
                    $userObject = Domain_User_UserFactory::createInstance($id_user);
                    $userObject->setSettings($this->_settings_array);
                    $userObject->activeJobs();
                }
            }
        }

    }

}


$cron = new activeJobRecommender;
$cron->execute();

?>