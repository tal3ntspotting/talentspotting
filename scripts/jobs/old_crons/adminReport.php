<?php
/**
 *  o data pe saptamana vineri pentru admini
 */
require_once __DIR__ . "/../cli_config.php";
$CronModel = new models_Cron;
$result = $CronModel->adminReport();

$smarty = new Smarty;


$params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
$transport = null;
$configuration = ['auth' => "login",
    'username' => $params_productions->resources->Settings->email_smtp->username,
    'password' => $params_productions->resources->Settings->email_smtp->password
];
$transport = new Zend_Mail_Transport_Smtp($params_productions->resources->Settings->email_smtp->host, $configuration);
foreach ($result as $key => $value)
    {

    $smarty->assign("applications", $value['applications']);
    $smarty->assign("reached", $value['user_reached']);
    $smarty->assign("bounty", $value['bountyToPay']);

    if (!empty($value['users']))
        {
        foreach ($value['users'] as $user)
            {
            $mail = new WebApp_Mail();
            $mail->setFrom($value['email_company'], "https://talentspotting.com");
            $mail->setSubject($value['email_subject']);
            $smarty->assign("username", $user['firstname']);
            $mail->addTo($user['user_email'], $value['email_subject']);
            $sitename = $params_productions->resources->Settings->sitename;
            $smarty->assign('subdomain_url',$sitename);
            $CronModel->updareCronTipsTable($user['id_user'], $value['id_company']);
            $resource = $smarty->fetch('eval:' . $value['email_content']);



            $company = new models_Company;
            $template = $company->getCompanyEmailTemplate($sitename);
            $smarty->assign('subject',$value['email_subject']);
            $smarty->assign('content',$resource);
            $content = $smarty->fetch('eval:'.$template);
            $mail->setBodyHtml($content);

            $cron['id_user'] = $user['id_user'];
            $cron['email_user'] = $user['user_email'];
            $cron['email_type'] = 37;
            $cron['cron_file'] = "adminReport.php";
            $cron['conditions'] = "Friday, 9:00";
            $cron['subject'] = $value['email_subject'];
            $cron['content'] = $content;
            $cron['date'] = date('Y-m-d H:i:s');


            if (filter_var($user['user_email'], FILTER_VALIDATE_EMAIL))
                {
                if($resource == "" or $value['email_subject'] == "")
                {
                    $cron['send'] = 0;
                    $CronModel->logCron($cron);
                    continue;
                }
                    $mail->send($transport);
                    $cron['send'] = 1;
                    $CronModel->logCron($cron);
                }
            }
        }
    }
