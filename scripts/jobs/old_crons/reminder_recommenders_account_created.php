<?php

// Every 3 days after  7days
require_once __DIR__ . "/../cli_config.php";
$CronModel = new models_Cron;
$params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);

$sitename = $params->resources->Settings->sitename;

$transport = null;
$config = ['auth' => "login",
    'username' => $params->resources->Settings->email_smtp->username,
    'password' => $params->resources->Settings->email_smtp->password
];
$transport = new Zend_Mail_Transport_Smtp($params->resources->Settings->email_smtp->host, $config);

$company = new models_Company;
$result = $CronModel->getNotLoggedUsers();
$sub = $result['email']['subject'];
$con = $result['email']['content'];
// echo '<pre>';print_r($result);die;
foreach($result['users'] as $key => $value)
{


    $smarty = new Smarty;
    $mail = new WebApp_Mail ;
    $smarty->assign('settings',['sitename' => $sitename]);
    $smarty->assign('sitename',$sitename);
    $smarty->assign('user',$value);
    $smarty->assign('subdomain_url',$sitename);
    $subject = $smarty->fetch('eval:'.$sub);
    $content = $smarty->fetch('eval:'.$con);
    $mail->setFrom($result['company']['email'], $result['company']['title']);
    $mail->setSubject($subject);
    $mail->addTo($value['email'], $subject);


    $company = new models_Company;
    $template = $company->getCompanyEmailTemplate($sitename);
    $smarty->assign('subject',$subject);
    $smarty->assign('content',$content);
    $content = $smarty->fetch('eval:'.$template);
    $mail->setBodyHtml($content);

    if (filter_var($value['email'], FILTER_VALIDATE_EMAIL))
    {
        $sql = $CronModel->select()
            ->from(['rr' => 'recommender_reminder'],['*'])
            ->where('rr.id_user =?',(int) $value['id'])
            ->order('rr.date DESC');
        $result = $CronModel->fetchAll($sql);
        if(count($result) >= 3) continue;
        $date = date('Y-m-d H:i:s', time() - (7 * 86400));
        $sql = $CronModel->select()->from(['rr' => 'recommender_reminder'],['*'])
            ->where('rr.date >= ?',$date)
            ->where('rr.id_user =?',(int) $value['id'])->limit(1);
        $return = $CronModel->fetchRow($sql);

        $cron['id_user'] = $value['id'];
        $cron['email_user'] = $value['email'];
        $cron['email_type'] = 63;
        $cron['cron_file'] = "reminder_recommenders_account_created.php";
        $cron['conditions'] = "Recommender has first login null , send 3 time one after 7 days";
        $cron['subject'] = $subject;
        $cron['content'] = $content;
        $cron['date'] = date('Y-m-d H:i:s');

        if(empty($return))
        {
            if (filter_var($value['email'], FILTER_VALIDATE_EMAIL))
            {
                if($content == "" or $subject == "")
                {
                    $cron['send'] = 0;
                    $CronModel->logCron($cron);
                    continue;
                }

                $cron['send'] = 1;
                $CronModel->logCron($cron);
                $mail->send($transport);
                $CronModel->insert(['id_user' => $value['id'],'date' => date('Y-m-d H:i:d')],'recommender_reminder');
            }

        }
    }

}

//echo '<pre>';
//print_r($result['company']);
//die;

?>
