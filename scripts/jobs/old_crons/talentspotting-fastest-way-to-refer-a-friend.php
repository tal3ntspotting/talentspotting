<?php

// Every day
require_once __DIR__ . "/../cli_config.php";
$CronModel = new models_Cron;
$params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);

$smarty = new Smarty;
$sitename = $params->resources->Settings->sitename;

$transport = null;
$config = ['auth' => "login",
    'username' => $params->resources->Settings->email_smtp->username,
    'password' => $params->resources->Settings->email_smtp->password
];
$transport = new Zend_Mail_Transport_Smtp($params->resources->Settings->email_smtp->host, $config);

$company = new models_Company;
$result = $CronModel->getAllUsersByCompany(60, 1);

$now = date('Y-m-d H:i:s');
$t1 = StrToTime($now);

foreach ($result as $key => $value) {

    if (!empty($value['users'])) {
        foreach ($value['users'] as $user) {

            $t2 = StrToTime($user['first_login']);
            $diff = $t1 - $t2;
            $hours = $diff / ( 60 * 60 );
            if ($hours > 120 && $hours <= 144) {
                $mail = new WebApp_Mail();
                $mail->setFrom($value['email_company'], $value['email_company']);
                $mail->setSubject($value['email_subject']);
                $smarty->assign("recommender", $user);
                $smarty->assign("sitename", $sitename);
                $smarty->assign("settings", ['sitename' => $sitename]);
                $smarty->assign("company", ['title' => $value['title_company']]);
                $smarty->assign('subdomain_url',$sitename);
                $mail->addTo($user['user_email'], $value['email_subject']);
                $resource = $smarty->fetch('eval:' . $value['email_content']);


                $company = new models_Company;
                $template = $company->getCompanyEmailTemplate($sitename);
                $smarty->assign('subject',$value['email_subject']);
                $smarty->assign('content',$resource);
                $content = $smarty->fetch('eval:'.$template);
                $mail->setBodyHtml($content);

                $cron['id_user'] = $user['id_user'];
                $cron['email_user'] = $user['user_email'];
                $cron['email_type'] = 60 ;
                $cron['cron_file'] = "talentspotting-fastest-way-to-refer-a-friend.php";
                $cron['conditions'] = "first login + 5 days";
                $cron['subject'] = $value['email_subject'];
                $cron['content'] = $content;
                $cron['date'] = date('Y-m-d H:i:s');

               if (filter_var($user['user_email'], FILTER_VALIDATE_EMAIL))
                {
                    if($resource == "" or $value['email_subject'] == "")
                    {
                        $cron['send'] = 0;
                        $CronModel->logCron($cron);
                        continue;
                    }

                    $mail->send($transport);
                    $cron['send'] = 1;
                    $CronModel->logCron($cron);
                }
            }
        }
    }
}


echo '<pre>';
print_r($result);
die;
?>














