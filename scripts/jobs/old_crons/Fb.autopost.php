  <?php

require_once __DIR__ . "/../cli_config.php";
require_once APPLICATION_PATH . '/../library/WebApp/Mail/Oempro/libraries/facebook.php';
$CronModel = new models_Cron;
$userModel = new models_User;
$jobs = $CronModel->getAutopostJobs();

$config = array();
$config['appId'] = '1062377973856382';
$config['secret'] = '8f8718a3c26a44fb4c83e38a3ff4c099';
$config['fileUpload'] = false; // optional

foreach ($jobs as $index => $job)
    {
    $fb = new Facebook($config);
    $privacy = ['value' => 'EVERYONE'];
    $params = [
        "access_token" => $job['facebook_token'],
        "link" => $job['share_link'],
        "picture" => APPLICATION_URL.'/resources/files/companies/original/'.$job['company']['logo'],
        'privacy' => $privacy,
        "name" => $job['job']['title'],
        "caption" => $job['job']['title'],
        "description" => $job['job']['description']
    ];
    try
        {
        $ret = $fb->api('/me/feed', 'POST', $params);
        $cron['id_user'] = $job['id_user'];
        $cron['email_user'] = $job['email'];
        $cron['email_type'] = 0;
        $cron['cron_file'] = "Fb.autopost.php";
        $cron['conditions'] = "Facebook autopost";
        $cron['subject'] = "WITHOUT SUBJECT";
        $cron['content'] = "WITHOUT CONTENT";
        $cron['date'] = date('Y-m-d H:i:s');
        $cron['send'] = 1;
        $CronModel->logCron($cron);
        $userModel->updateAutoPostDate($job['id']);
        } catch (Exception $e)
        {
    echo $e->getMessage();
//        $CronModel->removeFacebookToken($job['id_user']);
        }
    }



