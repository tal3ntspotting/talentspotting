<?php 

/**
 *  Cron should be runned once a day to reminder candidate if any job was added in last 24 hours
 *
 */

require_once __DIR__ . "/../cli_config.php";
$CronModel = new models_Cron;
$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',"production",$options);
$smarty = new Smarty;
$sitename = $config->resources->Settings->sitename ; 

$params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
$transport = null;
$configuration = ['auth' => "login",
    'username' => $params_productions->resources->Settings->email_smtp->username,
    'password' => $params_productions->resources->Settings->email_smtp->password
];
$transport = new Zend_Mail_Transport_Smtp($params_productions->resources->Settings->email_smtp->host, $configuration);

$result = $CronModel->reminderJobs();

foreach($result as $key => $value) 
{
    $users = $result[$key]['users'];
    $jobs = $result[$key]['jobs'];

    foreach($users as $index => $user) {
        $userName = $user['firstname'];
        $userLastName = $user['lastname'];
        $userEmail = $user['user_email'];
        foreach($jobs as $K => $job) {
//            
            $smarty->assign('job',$job);
            $smarty->assign('user',$user);
            $smarty->assign('subdomain_url',$sitename);
            $smarty->assign('settings',['sitename' => $sitename]);
            if($user['recommender_firstname'] != "" || $user['recommender_lastname'] != "") {
                $smarty->assign('recommender', ['recommender_firstname' => $user['recommender_firstname'], 'recommender_lastname' => $user['recommender_lastname']]);
            } else {
                $smarty->assign('email', $job['company_email']);
            }
            $jobTitle = $job['title'];
            $companyEmail = $job['company_email'];
            $companytitle = $job['company_title'];
            $resource = $smarty->fetch("eval:".$value['email_content']);
            $subject = $smarty->fetch("eval:".$value['email_subject']);
    }
            $mail = new WebApp_Mail();
            $mail->setFrom($companyEmail, $sitename);
            $mail->setSubject($subject);
            $mail->addTo($userEmail, $companyEmail);

            $company = new models_Company;
            $template = $company->getCompanyEmailTemplate($sitename);
            $smarty->assign('subject',$subject);
            $smarty->assign('content',$resource);
            $content = $smarty->fetch('eval:'.$template);
            $mail->setBodyHtml($content);

            echo $content;die;

            $cron['id_user'] = $user['id_user'];
            $cron['email_user'] = $user['user_email'];
            $cron['email_type'] = 43;
            $cron['cron_file'] = "reminder_job_for_candidates.php";
            $cron['conditions'] = "once a day to reminder candidate if any job was added in last 24 hours";
            $cron['subject'] = $subject;
            $cron['content'] = $content;
            $cron['date'] = date('Y-m-d H:i:s');

             if (filter_var($userEmail, FILTER_VALIDATE_EMAIL))
                {
                 if(!empty($jobs))
                 {
                     if($resource == "" or $subject == "")
                     {
                         $cron['send'] = 0;
                         $CronModel->logCron($cron);
                         continue;
                     }

                     $mail->send($transport);
                     $cron['send'] = 1;
                     $CronModel->logCron($cron);
                 }
                }
        }
}
echo '<pre>';
print_r($result);die;

?>
