<?php

/**
 *  Cron should be runned once a day to reminder candidate if any job was added
 */
require_once __DIR__ . "/../cli_config.php";
$CronModel = new models_Cron;
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
$smarty = new Smarty;
$sitename = $config->resources->Settings->sitename;

$params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
$transport = null;
$configuration = ['auth' => "login",
    'username' => $params_productions->resources->Settings->email_smtp->username,
    'password' => $params_productions->resources->Settings->email_smtp->password
];
$transport = new Zend_Mail_Transport_Smtp($params_productions->resources->Settings->email_smtp->host, $configuration);
$result = $CronModel->reminder_7_Jobs();


foreach ($result as $key => $value)
    {

    $users = $result[$value['id_company']]['users'];
    $jobs = $result[$value['id_company']]['jobs'];

    foreach ($jobs as $key => $job)
        {
        $jobs[$key]['url'] = WebApp_Tools::createJobSlug($job);
        }
        
         $view = new Zend_View();
    $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
    $partialHelper = new Zend_View_Helper_Partial();
    $partialHelper->setView($view);
    $table = $partialHelper->partial('reminder_jobs.phtml', ['data' => $jobs]);




    if(!empty($jobs)) {
        foreach ($users as $index => $user) {
            $userName = $user['firstname'];
            $userLastName = $user['lastname'];
            $userEmail = $user['user_email'];

            $contentModel = new models_Content;
            $email = $contentModel->getEmailByType(44);
            $smarty = new Smarty;
            $smarty->assign("username", $userName);
            $smarty->assign("jobs", $jobs);
            $smarty->assign("table", $table);
            $smarty->assign("settings", $config->resources->Settings);
            $smarty->assign("settings", ['sitename' => $sitename]);
            $smarty->assign("company", $result['company']['title_company']);
            $smarty->assign('subdomain_url',$sitename);
            $sub = 'What\'s new in {$company} jobs - open for referral on Talentspotting';
            $subject = $smarty->fetch('eval:' . $result['company']['email_subject']);
            $resource = $smarty->fetch('eval:' . $result['company']['email_content']);

            $mail = new WebApp_Mail();
            $mail->setFrom($result['company']['email_company'], $sitename);
            $mail->setSubject($subject);
            $mail->addTo($userEmail, $companyEmail);

            $company = new models_Company;
            $template = $company->getCompanyEmailTemplate($sitename);
            $smarty->assign('subject',$subject);
            $smarty->assign('content',$resource);
            $content = $smarty->fetch('eval:'.$template);
            $mail->setBodyHtml($content);


            $cron['id_user'] = $user['id_user'];
            $cron['email_user'] = $user['user_email'];
            $cron['email_type'] = 44;
            $cron['cron_file'] = "reminder_job.php";
            $cron['conditions'] = "once per day to reminder recommender if any job was added";
            $cron['subject'] = $subject;
            $cron['content'] = $content;
            $cron['date'] = date('Y-m-d H:i:s');

            if (filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
                if($resource == "" or $subject == "")
                {
                    $cron['send'] = 0;
                    $CronModel->logCron($cron);
                    continue;
                }

                $mail->send($transport);
                $cron['send'] = 1;
                $CronModel->logCron($cron);
            }
        }
    }
    }
echo '<pre>';
print_r($result);
die;
?>
