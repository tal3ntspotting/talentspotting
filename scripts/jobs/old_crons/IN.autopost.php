<?php

require_once __DIR__ . "/../cli_config.php";
Zend_Loader::loadFile("linkedin/vendor/autoload.php");
$params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
$linkedIn = new Happyr\LinkedIn\LinkedIn($params_productions->resources->Settings->linkedin->e_api_key, 
        $params_productions->resources->Settings->linkedin->e_secret);



//$options = array('json'=>
//    array(
//        'comment' => 'Im testing Happyr LinkedIn client! https://github.com/Happyr/LinkedIn-API-client',
//        'visibility' => array(
//            'code' => 'anyone'
//        )
//    )
//);
//
//$result = $linkedIn->post('v1/people/~/shares', $options);
//
//var_dump($result);

$CronModel = new models_Cron;
$userModel = new models_User;
$jobs = $CronModel->getAutopostLinkedinJobs();

//echo '<pre>';print_r($jobs);die;
foreach ($jobs as $index => $job)
    {
//        echo $job['linkedin_token'].PHP_EOL;die;
    $cron['id_user'] = $job['id_user'];
    $cron['email_user'] = $job['email'];
    $cron['email_type'] = 0;
    $cron['cron_file'] = "IN.autopost.php";
    $cron['conditions'] = "Linkedin autopost";
    $cron['subject'] = "WITHOUT SUBJECT";
    $cron['content'] = "WITHOUT CONTENT";
    $cron['date'] = date('Y-m-d H:i:s');
    $cron['send'] = 1;
    $CronModel->logCron($cron);
        $linkedIn->setAccessToken($job['linkedin_token']);
        $allJobs = $job['job'];
        foreach ($allJobs as $key => $j)
            {
                $options = ['json'=>
                    [
                        'visibility' => [
                            'code' => 'anyone'
                        ],
                        'content' => [
                            "title" =>  $job['job']['title'],
                            "description" => $job['job']['description'],
                            "submitted-url" => $job['share_link'],
                            "submitted-image-url" => APPLICATION_URL.'/resources/files/companies/original/'.$job['company']['logo']
                        ]
                    ]
                ];
                $result = $linkedIn->post('v1/people/~/shares', $options);
                if($result['updateKey'] || $result['errorCode']) break ;
            }

    }

