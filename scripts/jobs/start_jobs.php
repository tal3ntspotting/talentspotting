<?php


/**
 *  this cron should run every day to check if job is expire and disactivate it
 */

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

$CronModel = new models_Cron;
$CronModel->enableJobs();
$cron['id_user'] = 0;
$cron['email_user'] = "";
$cron['email_type'] = 0;
$cron['cron_file'] = "start_job.php";
$cron['conditions'] = "start jobs";
$cron['subject'] = "WITHOUT SUBJECT";
$cron['content'] = "WITHOUT CONTENT";
$cron['date'] = date('Y-m-d H:i:s');
$cron['send'] = 1;
$CronModel->logCron($cron);
?>
