<?php


// https://jsfiddle.net/carm3n/pqo74ehf/56/
// 0 0 1 * * /usr/bin/foo first day of month cron

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

class reportRecommender extends models_BaseCron
{

    public function getData()
    {
        return $this->_cronModel->getAllUsersByCompany(93, 1);
    }

    public function execute()
    {
        $data = $this->getData();
        foreach ($data as $key => $value) {

            if (!empty($value['users'])) {
                foreach ($value['users'] as $user) {

                    $id_user = $user['id_user'];
                    $user_array = (object) $this->_UserModel->getUserById($id_user);
                    Zend_Auth::getInstance()->getStorage()->write($user_array);
                    $userObject = Domain_User_UserFactory::createInstance($id_user);
                    $userObject->setSettings($this->_settings_array);
                    $userObject->report();

                }
            }
        }

    }

}


$cron = new reportRecommender();
$cron->execute();

?>