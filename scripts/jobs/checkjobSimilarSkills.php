<?php


// https://jsfiddle.net/carm3n/8vL4g9y6/6/

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

class checkjobSimilarSkills extends models_BaseCron
{

    public function getData()
    {
        return $this->_cronModel->getAllUsersByCompany(95, 9);
    }

    public function execute()
    {
        $data = $this->getData();
        foreach ($data as $key => $value) {

            if (!empty($value['users'])) {
                foreach ($value['users'] as $user) {
                        $id_user = $user['id_user'];
                        $user_array = (object) $this->_UserModel->getUserById($id_user);
                        Zend_Auth::getInstance()->getStorage()->write($user_array);
                        $userObject = Domain_User_UserFactory::createInstance($id_user);
                        $userObject->setSettings($this->_settings_array);
                        $userObject->similarJobSkill();
                }
            }
        }

    }

}


$cron = new checkjobSimilarSkills;
$cron->execute();

?>