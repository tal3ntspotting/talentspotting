<?php


// https://jsfiddle.net/carm3n/amowq1xe/3/

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

class feedbackRecommender extends models_BaseCron
{

    public function getData()
    {
        return $this->_cronModel->getAllUsersByCompany(91, 1);
    }

    public function execute()
    {
        $data = $this->getData();
        $now = date('Y-m-d H:i:s');
        $t1 = StrToTime($now);
        foreach ($data as $key => $value) {

            if (!empty($value['users'])) {
                foreach ($value['users'] as $user) {
                    $t2 = StrToTime($user['first_login']);
                    $diff = $t1 - $t2;
                    $hours = $diff / ( 60 * 60 );
                    if ($hours > 720 && $hours <= 744) {

                        $id_user = $user['id_user'];
                        $user_array = (object) $this->_UserModel->getUserById($id_user);
                        Zend_Auth::getInstance()->getStorage()->write($user_array);
                        $userObject = Domain_User_UserFactory::createInstance($id_user);
                        $userObject->setSettings($this->_settings_array);
                        $userObject->feedback();

                    }
                }
            }
        }

    }

}


$cron = new feedbackRecommender();
$cron->execute();

?>