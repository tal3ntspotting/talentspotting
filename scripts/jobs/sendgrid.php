<?php

// Every Minute to check if hard bounced

require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');
$CronModel = new models_Cron;
$params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);

$mUser = new models_User ;

$key = $params->resources->Settings->email_smtp->password;

$jsonData = '';
$config = array(
    'adapter' => 'Zend_Http_Client_Adapter_Curl',
    'curloptions' => array(CURLOPT_FOLLOWLOCATION => true,CURLOPT_SSL_VERIFYPEER =>FALSE),
);
//$url='https://api.sendgrid.com/v3/suppression/invalid_emails';
$url='https://api.sendgrid.com/v3/suppression/bounces';
$client = new Zend_Http_Client($url, $config);
$client->setHeaders('Content-Type', 'application/json');
$client->setHeaders('Authorization', 'Bearer '.$key);
$response = $client->request('GET');
$jsonData= json_decode($response->getBody(),true);
$mUser->disableBouncedUser($jsonData);



?>
