<?php
/* Smarty version 3.1.29, created on 2018-10-11 17:27:16
  from "a938b8db99e192763ead6c90d5440092cac35a12" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5bbf5dc4d59671_87615486',
  'file_dependency' => 
  array (
    'a938b8db99e192763ead6c90d5440092cac35a12' => 
    array (
      0 => 'a938b8db99e192763ead6c90d5440092cac35a12',
      1 => 0,
      2 => 'string',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbf5dc4d59671_87615486 ($_smarty_tpl) {
?>

<!DOCTYPE html>
<html>
<head>
<title></title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<style type="text/css">/* FONTS */


        .button{
            font-size: 14px;
            line-height: 19px;
            max-width: 50%;
            height: 43px;
            text-align: center;
            border: 2px solid #393c4e!important; /*{$branding_button_border_color}*/
            border-radius: 3px;
            color: #333646!important;/*{$branding_button_border_color}*/
            background: white !important;
            /*display: inline-block;*/
            padding: 9px 12px;
            /*padding-top: 7px;*/
            margin-bottom: 0;
            text-shadow: none;
            box-shadow: none;
            word-wrap: break-word;
            cursor: pointer;
            text-decoration: none;
        }

        @media screen {
            @font-face {
                font-family: 'Open-Sans', 'Arial', 'sans-serif';
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Open-Sans', 'Arial', 'sans-serif';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Open-Sans', 'Arial', 'sans-serif';
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Open-Sans', 'Arial', 'sans-serif';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

        /* RESET STYLES */
        table { border-collapse: collapse !important; }
        body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px){
            h1 {
                font-size: 26px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
</style>
<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">Thanks for choosing us as your referral tool</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%"><!-- LOGO -->
	<tbody>
		<tr>
			<td align="center" bgcolor="#fff"><!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
			<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" width="100%">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;" valign="top"><!--  <?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
 ===============> https://tpp.talentspotting.com/resources/frontend/landing/files/icons/logo.png --><img height="70" src="<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
" style="text-align: center" width="auto" /></td>
					</tr>
				</tbody>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" id="heroimage" style="width: 100%;height:150px;
                   background-image: url(<?php echo $_smarty_tpl->tpl_vars['landing']->value;?>
) !important;

            background-position: center;
            background-size: cover;
            -webkit-transition: -webkit-transform 1000ms ease;
            transition: -webkit-transform 1000ms ease;
            transition: transform 1000ms ease;
            transition: transform 1000ms ease, -webkit-transform 1000ms ease;
            position: static;
            left: 0px;
            top: 0px;
            right: 0px;
            bottom: 0px;
            min-height: 100px;
            width:100%;
" width="100%">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;" valign="top"><img height="300" src="https://preview.ibb.co/gn0iCy/header_email.jpg" style="display:none" width="600" /></td>
					</tr>
				</tbody>
			</table>
			<!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]--></td>
		</tr>
		<!-- HERO -->
		<tr>
			<td align="center" bgcolor="#fff" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
			<table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px;" width="100%">
				<tbody>
					<tr>
						<td align="center" bgcolor="#ffffff" mc:edit="main heading" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" valign="top">
						<h1 style="font-size: 42px; font-weight: 400; margin: 0;"><!--<?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
--><?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
</h1>
						</td>
					</tr>
				</tbody>
			</table>
			<!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]--></td>
		</tr>
		<!-- COPY BLOCK -->
		<tr>
			<td align="center" bgcolor="#f4f4f4" style="padding: 0px 10px 0px 10px;"><!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]--><?php echo $_smarty_tpl->tpl_vars['content']->value;?>

			<table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px;" width="100%"><!-- COPY -->
				<tbody>
					<tr>
					</tr>
					<!-- HEADLINE -->
				</tbody>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" width="100%">
				<tbody>
					<tr>
						<td align="center" style="padding: 10px;" valign="top"><!--  <?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
 ===============> https://tpp.talentspotting.com/resources/frontend/landing/files/icons/logo.png --><img height="70" src="<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
" style="text-align: center" width="auto" /></td>
					</tr>
				</tbody>
			</table>
			<?php echo $_smarty_tpl->tpl_vars['social']->value;?>
<br />
			<br />
			<a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

			<p>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
            </center></body>
            </html>
            <?php }
}
