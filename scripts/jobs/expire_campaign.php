<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of expire_campaign
 *
 * @author edin
 */
require_once (__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cli_config.php');

class expire_campaign extends models_BaseCron {

    public function run()
    {
        $expired = $this->_cronModel->getExpiredCampaigns();
//        echo '<pre>';
//        print_r($expired);
//        die;
        $this->disableCampaing($expired);
    }

    public function disableCampaing($campaigns)
    {

        foreach ($campaigns as $key => $campaign)
            {

            if ($campaign['end'] < date('Y-m-d'))
                {
                $this->sendCampaignResult($campaign['id'], $campaign['title']);
                $this->_cronModel->update(['status' => 0], "id = {$campaign['id']}", "campaigns");
                }
            }
    }

    public function sendCampaignResult($id_campaign, $title)
    {

//        $sql = $this->_cronModel->select()
//                ->from(array("u" => "users"), array(
//                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
//                ))
//                ->joinLeft(array("cad" => "campaign_actions_detailed"), "cad.id_recommender = u.id", array("sum(cad.value) as total_points"))
//                ->join(['c' => 'campaigns'], "c.id = cad.id_campaign", ['title'])
//                ->group("u.id")
//                ->where("cad.id_campaign =?", (int) $id_campaign)
//                ->order("total_points DESC")->limit(3);
//        $data = $this->_cronModel->fetchAll($sql);

        $mCampaign = new models_Campaign ;
        $data[] = $mCampaign->getCampaignFirstPlacesById($id_campaign)[0];
        $data[] = $mCampaign->getCampaignFirstPlacesById($id_campaign)[1];
        $data[] = $mCampaign->getCampaignFirstPlacesById($id_campaign)[2];
        $data = array_filter($data);

        $users = $this->_cronModel->getUserByType(2);


        foreach ($users as $index => $user)
            {
                $id_user = $user['id'];
                $user_array = (object) $this->_UserModel->getUserById($id_user);
                Zend_Auth::getInstance()->getStorage()->write($user_array);
                $userObject = Domain_User_UserFactory::createInstance($id_user);
                $userObject->setSettings($this->_settings_array);
                $userObject->campaignWinners($data,$title);
            }
    }

}

$campaign = new expire_campaign;

$campaign->run();
