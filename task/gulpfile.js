// var gulp = require('gulp') ;
// var yaml = require('js-yaml') ;
// var path = require('path') ;
// var fs = require('fs') ;
//
// gulp.task('watch',function()
// {
//     var doc = yaml.safeLoad(fs.readFileSync(path.join(__dirname,"../api/swagger/swagger.yaml")));
//     fs.writeFileSync(
//         path.join(__dirname,"../api/swagger/swagger.yaml"),
//         JSON.stringify(doc,null,' ')
//     )
// })

// Minimal requirements
var map     = require('map-stream');
var yaml    = require('js-yaml');
var gulp    = require('gulp');
var gutil = require('gulp-util');

// Create JS files from YML files
gulp.task('watch', function(){
    gulp.src('../api/swagger/*.yaml')
        .pipe(map(function(file,cb){
            if (file.isNull()) return cb(null, file); // pass along
            if (file.isStream()) return cb(new Error("Streaming not supported"));

            var json;

            try {
                json = yaml.load(String(file.contents.toString('utf8')));
            } catch(e) {
                console.log(e);
                console.log(json);
            }

            file.path = gutil.replaceExtension(file.path, '.json');
            file.contents = new Buffer(JSON.stringify(json));

            cb(null,file);
        }))
        .pipe(gulp.dest('../public'));
    gulp.watch('../api/swagger/*.yaml', ['watch'])
    gulp.start('watch');
});