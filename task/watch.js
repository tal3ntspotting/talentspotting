var yaml = require('gulp-yaml');
var gulp = require('gulp')
    gulp.src('../api/swagger/swagger.yaml')
    .pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
    .pipe(gulp.dest('../public'))
