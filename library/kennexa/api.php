<?php

class kennexa_api {

    protected $_data;
    protected $_command = ['add', 'edit', 'update', 'delete'];
    protected $_settings;
    protected $_url;
    protected $_isActive;
    protected $_modelContent;
    protected $_modelJob;
    protected $_modelUser;
    protected $_DataBind;
    protected $_type;
    protected $_career_levels = [];
    protected $_user;
    private $_token;
    static protected $events = array();

    const create = 'add';
    const edit = 'edit';
    const update = 'update';
    const delete = 'delete';

    function __construct($data = [], $command = null)
    {

        $this->_data = $data;
        $this->_user = Zend_Auth::getInstance()->getIdentity();
        $this->_modelContent = new models_Content;
        $this->_modelJob = new models_Jobs;
        $this->_modelUser = new models_User;
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'api');
        $_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $this->_url = $config->resources->Settings->kennexa->url;
        $this->_isActive = $config->resources->Settings->kennexa->active;
        $this->_career_levels = $_config->resources->Settings->career_levels->toArray();
        $this->_type = $_config->resources->Settings->job_types;
        if (!in_array($command, $this->_command))
            {
            throw new Exception("command not found");
            return false;
            }
        $this->_DataBind['command'] = $command;
    }

    public function getPosts()
    {
        return $this->_data;
    }

    public function createJob()
    {
        if ($this->_isActive)
            {
            $data = $this->getPosts();

            $this->getJobCities()
                    ->getSkills()
                    ->getJobDepartment()
                    ->career_level()
                    ->getDates()
                    ->getJobDetails()
                    ->getUserDetails()
                    ->setToken();

//            echo '<pre>';
//            print_r($this->_data);
//            print_r($this->_DataBind);
//            print_r($this->_url . "job/" . $this->_DataBind['command']);
//            die;
            $client = new Zend_Http_Client($this->_url . "job/" . $this->_DataBind['command']);
            $client->setParameterPost($this->_DataBind);
            $client->setHeaders('Authorization', "_token:{$this->_token}");
            $response = $client->request(Zend_Http_Client::POST)->getBody();
            $return = json_decode($response, true);
//            echo '<pre>';
//            print_r($return);
//            print_r($client);
//            print_r($response);
//            print_r($this->_DataBind);
////        print_r($_COOKIE);
//            die;
            }
    }

    protected function getJobCities()
    {
        $array = $this->_data['cities'];
        foreach ($array as $city)
            {
            $this->_DataBind['job_location'][] = $this->_modelContent->getCityById($city)['title'];
            }
        return $this;
    }

    protected function getJobDepartment()
    {
        $array = $this->_data['departments'];
        foreach ($array as $department)
            {
            $this->_DataBind['departments'][] = $this->_modelContent->getDepartmentById($department)['title'];
            }
        return $this;
    }

    protected function career_level()
    {
        
        $this->_DataBind['carrer_level'] = $this->_career_levels[$this->_data['career_level']];
        $this->_DataBind['type'] =$this->_type->{$this->_data['type']};
        return $this;
    }

    public function getDates()
    {
        $this->_DataBind['job_startdate'] = $this->_data['start'];
        $this->_DataBind['job_enddate'] = $this->_data['end'];
        return $this;
    }

    public function getSkills()
    {
        $skills = $this->_data['skills'];
        foreach ($skills as $skill)
            {
            $this->_DataBind['job_skills'][] = $this->_modelContent->getSkillById($skill)['title'];
            }
        return $this;
    }

    public function getDescriptions()
    {
        $this->_DataBind['job_description'] = $this->_data['description'];
        $this->_DataBind['job_requirements'] = $this->_data['requirements'];
        return $this;
    }

    public function getJobDetails()
    {
        $id_job = (int) $this->_data['id_job'];
        $job = $this->_modelJob->getJobById($id_job);
        $this->_DataBind['job_id'] = $id_job;
        $this->_DataBind['job_title'] = $job['title'];
        $this->_DataBind['job_url'] = WebApp_Tools::createJobSlug($job);
        $this->_DataBind['requirements'] = $this->_data['requirements'];
        $this->_DataBind['description'] = $this->_data['description'];
//        $this->_DataBind['job_ad_lifetime'] = $this->_data['job_ad_lifetime'];
        return $this;
    }

    public function getUserDetails()
    {
//        $this->_DataBind['username'] = $this->_user->email;
//        $this->_DataBind['password'] = $this->_modelUser->getUserDetailsByEmail($this->_user->email)['password'];
//        $this->_DataBind['salt'] = $this->_user->salt;

        $this->_DataBind['contact_name'] = $this->_user->firstname . ' ' . $this->_user->lastname;
        $this->_DataBind['created_by'] = $this->_user->id;
        $this->_DataBind['contact_telephone'] = $this->_user->phone;
        $this->_DataBind['contact_user_email'] = $this->_user->email;
//        $this->_DataBind['application_email'] = $this->_user->email;
        switch ($this->_DataBind['command'])
            {
            case(self::edit) :
                $this->_DataBind['modified_by'] = $this->_user->id;
                break;
            }

        return $this;
    }

    public function setToken()
    {
        $user = $this->_modelUser->getUserDetailsByEmail($this->_user->email);
        $this->_token = base64_encode($user['email'] . ':' . $user['pasword']);
    }

    public function delete($id_job)
    {
        $this->setToken();
        $this->_DataBind['job_id'] = $id_job;
        $client = new Zend_Http_Client($this->_url . "job/" . $this->_DataBind['command']);
        $client->setParameterPost($this->_DataBind);
        $client->setHeaders('Authorization', "_token:{$this->_token}");
        $response = $client->request(Zend_Http_Client::POST)->getBody();
        $return = json_decode($response, true);

    }
    
    public function createUser() {
         if ($this->_isActive){
        $modelContent = new models_Content;
        $this->_DataBind['id'] = $this->_data['id'];
        $this->_DataBind['firstname'] = $this->_data['firstname'];
        $this->_DataBind['lastname'] = $this->_data['firstname'];
        $this->_DataBind['lastname'] = $this->_data['lastname'];
        $this->_DataBind['email'] = $this->_data['email'];
        $this->_DataBind['linkedin_link'] = $this->_data['linkedin_link'];
        $this->_DataBind['phone'] = $this->_data['phone'];
        $this->_DataBind['location'] = $this->_data['location'];
        $this->_DataBind['status'] = $this->_data['status'];
        $this->_DataBind['last_login'] = $this->_data['last_login'];
        $this->_DataBind['recommended_date'] = $this->getUserRecommendedDate($this->_data['id']);
//        $cv_link = APPLICATION_URL.'/resources/files/cv/'.$this->_modelUser->getUserCv($this->_data['id'])['file'];
        $this->_DataBind['cv'] = $this->getUserCv();
        
        $user['created_at'] = date('y-m-d H:i:s');
        if($this->_data['facebook_id'] != "") $this->_DataBind['source'] = 'facebook';
        if($this->_data['google_id'] != "") $this->_DataBind['source'] = 'google';
        if($this->_data['linkedin_id'] != "") $this->_DataBind['source'] = 'linkedin';
        $this->_DataBind['id_parent'] = $this->_data['id_parent'];
        $this->_DataBind['type'] = $this->_data['type'];
        if(!empty($this->_modelUser->getUserSkills($id_user))) $this->_DataBind['skills'] = $this->_modelUser->getUserSkills($id_user);
//        if(!empty($this->_modelUser->getUserSkills($id_user))) $this->_DataBind['skills'] = $modelContent->getUserSkillsByAttributes($id_user,['title','slug']);
        if(!empty($this->_modelUser->getUserPositions($id_user))) $this->_DataBind['positions'] = $this->_modelUser->getUserPositions($id_user);
//        if(!empty($this->_modelUser->getUserPositions($id_user))) $this->_DataBind['positions'] = $this->_modelUser->getUserPositionsByAttribute($id_user,['id','title']);
        $this->requestUser();
         }
    }
    
    public function requestUser() {
        $this->setToken();
        $client = new Zend_Http_Client($this->_url . "user/" . $this->_DataBind['command']);
        $client->setParameterPost($this->_DataBind);
        $client->setHeaders('Authorization', "_token:{$this->_token}");
        $response = $client->request(Zend_Http_Client::POST)->getBody();
        $return = json_decode($response, true);
    }
    
    public function updateUSer($command)
    {
        $this->_DataBind['command'] = $command;
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'api');
        $_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $this->_url = $config->resources->Settings->kennexa->url;
        $this->_isActive = $config->resources->Settings->kennexa->active;
        $modelContent = new models_Content;
        $id_user = Zend_Auth::getInstance()->getIdentity()->id;
        $this->_data = $this->_modelUser->getUserById($id_user);
        if($this->_isActive) {
        
        $this->_DataBind['id'] = $this->_data['id'];
        $this->_DataBind['firstname'] = $this->_data['firstname'];
        $this->_DataBind['lastname'] = $this->_data['firstname'];
        $this->_DataBind['lastname'] = $this->_data['lastname'];
        $this->_DataBind['email'] = $this->_data['email'];
        $this->_DataBind['linkedin_link'] = $this->_data['linkedin_link'];
        $this->_DataBind['phone'] = $this->_data['phone'];
        
        $user['created_at'] = date('y-m-d H:i:s');
        if($this->_data['facebook_id'] != "") $this->_DataBind['source'] = 'facebook';
        if($this->_data['google_id'] != "") $this->_DataBind['source'] = 'google';
        if($this->_data['linkedin_id'] != "") $this->_DataBind['source'] = 'linkedin';
        $this->_DataBind['id_parent'] = $this->_data['id_parent'];
        $this->_DataBind['type'] = $this->_data['type'];
        if(!empty($this->_modelUser->getUserSkills($id_user))) $this->_DataBind['skills'] = $this->_modelUser->getUserSkills($id_user);
        if(!empty($this->_modelUser->getUserPositions($id_user))) $this->_DataBind['positions'] = $this->_modelUser->getUserPositions($id_user);
        $this->requestUser();
        }
        
    }
    
    public function Delete_USer($command)
    {
          
        $this->_DataBind['command'] = $command;
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'api');
        $_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $this->_url = $config->resources->Settings->kennexa->url;
        $this->_isActive = $config->resources->Settings->kennexa->active;
        $modelContent = new models_Content;
        $id_user = Zend_Auth::getInstance()->getIdentity()->id;
        $this->_data = $this->_modelUser->getUserById($id_user);
        if($this->_isActive) {
          $this->_DataBind['id'] = Zend_Auth::getInstance()->getIdentity()->id;
          $this->requestUser();
        }
          
    }
    
    public function getUserCv()
    {
                
           $cvs = $this->_modelUser->getUserCv($this->_DataBind['id']);
           foreach($cvs  as $key => &$value) {
//               $value['file'] = APPLICATION_URL.'/resources/files/cv/'.$value['file'];
               $value['file'] = APPLICATION_PATH.'/storage/cv/'.$value['file'];
           }
           return $cvs ;
               
    }
    
    public function jobApplication($id_application)
    {
        if($this->_isActive) {
        $modeJob = new models_Jobs ;
        $application = $modeJob->getJobApplicationById($id_application);
        $status =   [
            [0 , 0,"new"],
            [1 , 0,"viewed"],
            [4 , 9,"processing"],
            [3 , 0,"moved_to_another_process"],
            [2 , 1,"unfit_candidate"],
            [4 , 5,"accepted"],
//            [4 , 0,"called_for_interview"],
            [4 , 0,"offered"],
            [4 , 2,"tested"],
            [4 , 6,"offer_rejected"],
            [4 , 7,"no_answer"],
            [4 , 8,"dropped_out"],
            [5 , 1,"no_show"],
            [5 , 2,"banned"],
            [5 , 3,"final_interview"]
            
        ];
      
        
        $this->_DataBind['id'] = $application['id'] ; 
        $this->_DataBind['id_job'] = $application['id_job'] ; 
        $this->_DataBind['id_user'] = $application['id_user'] ; 
        $this->_DataBind['id_recommender'] = $application['id_recommender'] ; 
        $this->_DataBind['modified_by'] = $application['modified_by'] ; 
        $this->_DataBind['updated'] = $application['updated'] ; 
        $this->_DataBind['source'] = $application['source'] ; 
        foreach($status as $index => $stat) {
            if($stat[0] == $application['status'] && $stat[1] ==$application['status_reason'] )  $this->_DataBind['status'] = $stat[2] ; 
        } 
        $this->requestApplication();
        }
    }
    
       public function requestApplication() {

        $this->setToken();
        $client = new Zend_Http_Client($this->_url . "application/" . $this->_DataBind['command']);
        $client->setParameterPost($this->_DataBind);
        $client->setHeaders('Authorization', "_token:{$this->_token}");
        $response = $client->request(Zend_Http_Client::POST)->getBody();
        $return = json_decode($response, true);
    }
    
    public function updateApplicationStatus()
    {
        if($this->_isActive) {
        $id_application = $this->_data['id_application'];
        $modeJob = new models_Jobs ;
        $application = $modeJob->getJobApplicationById($id_application);
         $status =   [
            [0 , 0,"new"],
            [1 , 0,"viewed"],
            [4 , 9,"processing"],
            [3 , 0,"moved_to_another_process"],
            [2 , 1,"unfit_candidate"],
            [4 , 5,"accepted"],
//            [4 , 0,"called_for_interview"],
            [4 , 0,"offered"],
            [4 , 2,"tested"],
            [4 , 6,"offer_rejected"],
            [4 , 7,"no_answer"],
            [4 , 8,"dropped_out"],
            [5 , 1,"no_show"],
            [5 , 2,"banned"],
            [5 , 3,"final_interview"]
            
        ];
        $this->_DataBind['id'] = $application['id'] ; 
        $this->_DataBind['id_job'] = $application['id_job'] ; 
        $this->_DataBind['id_user'] = $application['id_user'] ; 
        $this->_DataBind['id_recommender'] = $application['id_recommender'] ; 
        $this->_DataBind['modified_by'] = $application['modified_by'] ; 
        $this->_DataBind['updated'] = $application['updated'] ; 
        $this->_DataBind['source'] = $application['source'] ; 
        foreach($status as $index => $stat) {
            if($stat[0] == $application['status'] && $stat[1] ==$application['status_reason'] )  $this->_DataBind['status'] = $stat[2] ; 
        } 
        $this->requestApplication();
    }
    }
    
    public function getUserRecommendedDate($id_user)
    {
        $sql = $this->_modelUser->select()
                ->from(['jr' => 'job_recommendations'],['date'])
                ->order('jr.date ASC')
                ->where("jr.id_recommender =?",(int) $id_user);
        $data = $this->_modelUser->fetchAll($sql);
        return $data[0]['date'];
        
        
    }

}

?>
