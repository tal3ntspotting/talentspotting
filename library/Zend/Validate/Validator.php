<?php

class Zend_Validate_Validator extends Zend_Validate_Abstract
{
    const INTEGER = 'Integer';

    protected $_messageTemplates = array(
        self::INTEGER => "'%value%' is not an integer"
    );

    public function isValid($value)
    {
        $this->_setValue($value);
        $value = (int) $value;
        if (is_int($value) && $value > 0 ) {
            return true;
        }
//        var_dump($value);die;
        $this->_error(self::INTEGER);
        return false;
    }
}
?>