<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// STEP 6 Page Parsing: Preview and Save
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep6($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// List recipient mail lists - STARTED
	$ObjectTemplate->define_block("LIST:MAILLISTS");
	
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."'";
	$SQLQuery .= "ORDER BY tblMailLists.Name ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	include_once("../system/libraries/maillists.inc.php");

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Find the total active member of the mail list - STARTED
		// $SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		// $SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		// $SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
		// $SQLQuery .= "WHERE tblAssign.RelMailListID = '".$EachRow['MailListID']."' AND tblAssign.SubscriptionStatus='Subscribed' AND tblMembers.BounceType!='Hard bounce'";
		// $ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// 	$TotalMembers = mysql_fetch_assoc($ResultSet2);
		// 		$TotalMembers = $TotalMembers['TotalFound'];
		// 
		// if ($EachRow['SourceType'] != 'Local')
		// 	{
		// 	$TotalMembers += FindTotalMemberOfOutsourcedList($EachRow, $ArrayConfig, array());
		// 	}
		// Find the total active member of the mail list - FINISHED

		$ArrayReplaceList = array(
								"_ListMailListID_"		=>	$EachRow['MailListID'],
								"_ListName_"			=>	$EachRow['Name'],
								"_ListTotalMembers_"	=>	number_format($TotalMembers),
								"_ListSourceType_"		=>	$ArrayLanguage["Texts"]["00034"][$EachRow['SourceType']],
								);
		$ObjectTemplate->duplicate_block("LIST:MAILLISTS", "LIST:MAILLISTS", $ArrayReplaceList);
		}	
	// List recipient mail lists - FINISHED

	// List clients - STARTED
	$ObjectTemplate->define_block("LIST:CLIENTS");
	
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_clients AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."clients AS tblClients ";
	$SQLQuery .= "ON tblAssign.RelClientID = tblClients.ClientID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."'";
	$SQLQuery .= "ORDER BY tblClients.Name ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	if (mysql_num_rows($ResultSet) > 0)
		{
		$ObjectTemplate->remove_block("ENABLE:NO:CLIENTS");
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			
			$ArrayReplaceList = array(
									"_ListClientID_"		=>	$EachRow['ClientID'],
									"_ListName_"			=>	$EachRow['Name'],
									"_ListEmail_"			=>	$EachRow['Email'],
									);
			$ObjectTemplate->duplicate_block("LIST:CLIENTS", "LIST:CLIENTS", $ArrayReplaceList);
			}	
		}
	// List clients - FINISHED

	// Retrieve the from, reply to and return path email addresses - STARTED
	if ($ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);
	// Retrieve the from, reply to and return path email addresses - FINISHED

	// Learn the content type of the campaign - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT TrackLinkClicks, TrackReads, Personalize, EmbedObjects, ContentType FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			$ContentType = $ArrayContent['ContentType'];

		// Decide the "email features" - STARTED
		$EmailFeatures = array();
		if ($ArrayContent['TrackLinkClicks'] == 'Yes')
			{
			$EmailFeatures[] = $ArrayLanguage["Texts"]["00197"];
			}
		if ($ArrayContent['TrackReads'] == 'Yes')
			{
			$EmailFeatures[] = $ArrayLanguage["Texts"]["00198"];
			}
		if ($ArrayContent['Personalize'] == 'Yes')
			{
			$EmailFeatures[] = $ArrayLanguage["Texts"]["00199"];
			}
		if ($ArrayContent['EmbedObjects'] == 'Yes')
			{
			$EmailFeatures[] = $ArrayLanguage["Texts"]["00200"];
			}

		if (count($EmailFeatures) > 0)
			{
			$EmailFeatures = $ArrayLanguage["Symbols"]["00001"]." ".implode("<br>\n".$ArrayLanguage["Symbols"]["00001"]." ", $EmailFeatures);
			}
		else
			{
			$EmailFeatures = $ArrayLanguage["Texts"]["00201"];
			}
		// Decide the "email features" - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT TrackLinkClicks, TrackReads, Personalize, EmbedObjects, RelNewsletterTemplateID FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		if (mysql_num_rows($ResultSet) < 1)
			{
			$SQLQuery = "SELECT TrackLinkClicks, TrackReads, Personalize, EmbedObjects, ContentType FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayContent = mysql_fetch_assoc($ResultSet);
				$ContentType = $ArrayContent['ContentType'];

			// Decide the "email features" - STARTED
			$EmailFeatures = array();
			if ($ArrayContent['TrackLinkClicks'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00197"];
				}
			if ($ArrayContent['TrackReads'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00198"];
				}
			if ($ArrayContent['Personalize'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00199"];
				}
			if ($ArrayContent['EmbedObjects'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00200"];
				}

			if (count($EmailFeatures) > 0)
				{
				$EmailFeatures = $ArrayLanguage["Symbols"]["00001"]." ".implode("<br>\n".$ArrayLanguage["Symbols"]["00001"]." ", $EmailFeatures);
				}
			else
				{
				$EmailFeatures = $ArrayLanguage["Texts"]["00201"];
				}
			// Decide the "email features" - FINISHED
			}
		else
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);
				$NewsletterTemplateID = $ArrayCMS['RelNewsletterTemplateID'];

			// Decide the "email features" - STARTED
			$EmailFeatures = array();
			if ($ArrayCMS['TrackLinkClicks'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00197"];
				}
			if ($ArrayCMS['TrackReads'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00198"];
				}
			if ($ArrayCMS['Personalize'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00199"];
				}
			if ($ArrayCMS['EmbedObjects'] == 'Yes')
				{
				$EmailFeatures[] = $ArrayLanguage["Texts"]["00200"];
				}

			if (count($EmailFeatures) > 0)
				{
				$EmailFeatures = $ArrayLanguage["Symbols"]["00001"]." ".implode("<br>\n".$ArrayLanguage["Symbols"]["00001"]." ", $EmailFeatures);
				}
			else
				{
				$EmailFeatures = $ArrayLanguage["Texts"]["00201"];
				}
			// Decide the "email features" - FINISHED

			$SQLQuery = "SELECT ContentType FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$NewsletterTemplateID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ContentType = mysql_fetch_assoc($ResultSet);
				$ContentType = $ContentType['ContentType'];
			}
		}
	// Learn the content type of the campaign - FINISHED

	// Decide the "report to" feature - STARTED
	$ReportTo = array();
	if ($ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		$ReportTo[0] = $ArrayLanguage["Texts"]["00194"];
			$ReportTo[0] = str_replace("_LoggedAdmin:Email_", $ArrayLoggedAdmin['Email'], $ReportTo[0]);
		}
	if ($ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		$ReportTo[1] = $ArrayLanguage["Texts"]["00195"];
		}

	if (count($ReportTo) > 0)
		{
		$ReportTo = $ArrayLanguage["Symbols"]["00001"]." ".implode("<br>\n".$ArrayLanguage["Symbols"]["00001"]." ", $ReportTo);
		}
	else
		{
		$ReportTo = $ArrayLanguage["Texts"]["00196"];
		}
	// Decide the "report to" feature - FINISHED

	// Decide the send through options (SMTP servers, Qmail or MTA) - STARTED
		if ($ArrayCampaign['SendEngine'] == 'SMTP')
			{
			// Retrieve and list all SMTP servers - STARTED
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' ORDER BY tblSMTP.ServerName ASC";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			$SendThrough = array();
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$SendThrough[] = $EachRow['ServerName']." (".$EachRow['ServerHost'].")";
				}
			$SendThrough = $ArrayLanguage["Symbols"]["00001"]." ".implode("<br>".$ArrayLanguage["Symbols"]["00001"]." ", $SendThrough);
			// Retrieve and list all SMTP servers - FINISHED
			}
		elseif ($ArrayCampaign['SendEngine'] == 'MTA')
			{
			$SendThrough = $ArrayLanguage["Texts"]["00202"];
			}
		elseif ($ArrayCampaign['SendEngine'] == 'QMail')
			{
			$SendThrough = $ArrayLanguage["Texts"]["00203"];
			}
		elseif ($ArrayCampaign['SendEngine'] == 'Sendmail')
			{
			$SendThrough = $ArrayLanguage["Texts"]["00204"];
			}
	// Decide the send through options (SMTP servers, Qmail or MTA) - FINISHED
	
	// Retrive set schedule option - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArraySchedule = mysql_fetch_assoc($ResultSet);
	
	$ScheduledOn = $ArrayLanguage["Texts"]["00208"][$ArraySchedule['ScheduleType']];
	// Retrive set schedule option - FINISHED
	
	// Parse the brief campaign information - STARTED
	$ArrayReplaceList = array(
							"_infoCampaignName_"		=>	$ArrayCampaign['CampaignName'],
							"_infoCampaignType_"		=>	$ArrayLanguage["Texts"]["00193"][$ArrayCampaign['CampaignType']],
							"_infoFromName_"			=>	$ArrayFromEmail['Name'],
							"_infoFromEmail_"			=>	$ArrayFromEmail['EmailAddress'],
							"_infoReplyToName_"			=>	$ArrayReplyToEmail['Name'],
							"_infoReplyToEmail_"		=>	$ArrayReplyToEmail['EmailAddress'],
							"_infoReturnPathEmail_"		=>	($ArrayLoggedAdmin['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayLoggedAdmin['ReturnPathEmail']),
							"_infoContentType_"			=>	$ArrayLanguage["Texts"]["00067"][$ContentType],
							"_infoSendReportTo_"		=>	$ReportTo,
							"_infoEmailFeatures_"		=>	$EmailFeatures,
							"_infoSendThrough_"			=>	$SendThrough,
							"_infoScheduledOn_"			=>	$ScheduledOn,
							);
	$ObjectTemplate->replace($ArrayReplaceList);						
	// Parse the brief campaign information - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 6 Page Parsing: Preview and Save
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep6($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// List RSS feeds - STARTED
	$ObjectTemplate->define_block('LIST:RSSFEEDS');
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_rss WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' ORDER BY RSSFeedName ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayFieldnValues = array(
								'_ListRSSFeedID_'		=>	$EachRow['RSSFeedID'],
								'_ListRSSFeedName_'		=>	$EachRow['RSSFeedName'],
								);
		$ObjectTemplate->duplicate_block('LIST:RSSFEEDS', 'LIST:RSSFEEDS', $ArrayFieldnValues);
		}
	// List RSS feeds - FINISHED
	
	// List Archives - STARTED
	$ObjectTemplate->define_block('LIST:ARCHIVES');

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_list WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' ORDER BY ListName ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayFieldnValues = array(
								'_ListArchiveID_'		=>	$EachRow['ArchiveListID'],
								'_ListArchiveName_'		=>	$EachRow['ListName'],
								);
		$ObjectTemplate->duplicate_block('LIST:ARCHIVES', 'LIST:ARCHIVES', $ArrayFieldnValues);
		}
	// List Archives - FINISHED
	
	// Select already assigned RSS and archive items - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_list_campaigns WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."' AND RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayReplaceList = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList['_selectedmultlistArchiveID'.$EachRow['RelArchiveListID'].'_'] = 'selected';
		}
	$ObjectTemplate->replace($ArrayReplaceList);

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_rss_campaigns WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."' AND RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayReplaceList = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList['_selectedmultlistRSSFeedID'.$EachRow['RelRSSFeedID'].'_'] = 'selected';
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Select already assigned RSS and archive items - FINISHED

	// Show/Hide send method section(s) if forced sending method feature is enabled - STARTED
	if ($ArrayLoggedAdmin['ForcedSendingMethod'] != 'N/A')
		{
		$ObjectTemplate->remove_block('SHOWHIDE:SENDTHROUGH');
		}
	// Show/Hide send method section(s) if forced sending method feature is enabled - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 5 Page Parsing: Content creating
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep5($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Retrieve the content and decide how to proceed - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		// Retrieve content - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);
		// Retrieve content - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);
			$ArrayContent = $ArrayCMS;
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve the content and decide how to proceed - FINISHED

	// Display the correct content section - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$ObjectTemplate->remove_block("SHOW:CMS:CONTENT");
	
		// Decide to proceed with step 2 or step 1 - STARTED
		if ($ArrayContent['ContentType'] != '')
			{
			$Step2Proceed = true;
			}
		else
			{
			if  ($ArrayErrorMessages['PageMessage'] == '')
				{
				$Step2Proceed = false;
				$ArrayErrorMessages['PageMessage'] = $ArrayLanguage["Texts"]["00184"];
				}
			}
		// Decide to proceed with step 2 or step 1 - FINISHED
	
		// Show/Hide second step - STARTED
		if ($Step2Proceed != true)
			{
			$ObjectTemplate->remove_block("ENABLE:STEP2");
			}
		else
			{
			$ObjectTemplate->remove_block("ENABLE:STEP1");

			// Decide the content type - STARTED
			$TMPContentType		= ($ArrayPostVariables['FormValue_ContentType'] == '' ? $ArrayContent['ContentType'] : $ArrayPostVariables['FormValue_ContentType']);
			// Decide the content type - FINISHED

			// Show/Hide rich text editor according to content type selection and administrator prefs - STARTED
			if (($TMPContentType == 'Plain') || ($ArrayLoggedAdmin['RichTextMode'] == 'Disabled'))
				{
				$ObjectTemplate->remove_block("SHOW:RICH:TEXT");
				}
			else
				{
				$ObjectTemplate->remove_block("SHOW:TEXT:AREA");

				// Rich text editor parsing - STARTED
				include_once('../system/libraries/richtext.inc.php');
			
					$ArrayParameters	= array(
												'FieldName'					=>	'FormValue_ContentMain',
												'Value'						=>	(count($ArrayPostVariables) < 1 ? $ArrayContent['ContentMain'] : stripslashes($ArrayPostVariables['FormValue_ContentMain'])),
												'BasePath'					=>	$ArrayConfig['URLs']['Data'].'/editors/fckeditor/',
												'ToolbarSet'				=>	'oemPro',
												'Width'						=>	'100%',
												'Height'					=>	'500',
												'FullPage'					=>	true,
												'AutoDetectLanguage'		=>	false,
												'DefaultLanguage'			=>	'en',
												'StartUpFocus'				=>	false,
												'UseBROnCarriageReturn'		=>	false,
												'EnableXHTML'				=>	true,
												'EnableSourceXHTML'			=>	true,
												);
					$EditorPath			= '../system/data/editors';
				$RichTextHTMLCode = GenerateRichText($ArrayParameters, $EditorPath, $ArrayLanguage, $ArrayConfig);
			
				$ArrayReplaceList = array(
										'_InsertRichTextEditor_'		=> $RichTextHTMLCode,
										);
				$ObjectTemplate->Replace($ArrayReplaceList);
				// Rich text editor parsing - FINISHED
				}
			// Show/Hide rich text editor according to content type selection and administrator prefs - FINISHED

			// Show/Hide alternate content - STARTED
			if (($TMPContentType == 'Plain') || ($TMPContentType == 'Html'))
				{
				$ObjectTemplate->remove_block("SHOW:ALTERNATE:CONTENT");
				}
			// Show/Hide alternate content - FINISHED
			}
		// Show/Hide second step - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$ObjectTemplate->remove_block("SHOW:QUICK:CONTENT");

		// Decide to proceed with step 2 or step 1 - STARTED
		if ($ArrayContent['RelCampaignID'] != '')
			{
			$Step2Proceed = true;
			}
		else
			{
			if  ($ArrayErrorMessages['PageMessage'] == '')
				{
				$Step2Proceed = false;
				$ArrayErrorMessages['PageMessage'] = $ArrayLanguage["Texts"]["00185"];
				}
			}
		// Decide to proceed with step 2 or step 1 - FINISHED
	
		// List newsletter templates - STARTED
		$ObjectTemplate->define_block("LIST:NEWSLETTER:TEMPLATES");
		
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE (RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' OR RelAdministratorID='0') ORDER BY Name ASC";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayReplaceList = array(
									"_ListNewsletterTemplateID_"		=>	$EachRow['NewsletterTemplateID'],
									"_ListName_"						=>	(strlen($EachRow['Name']) > 20 ? substr($EachRow['Name'], 0, 20)."..." : $EachRow['Name']),
									"_ListContentType_"					=>	$ArrayLanguage["Texts"]["00067"][$EachRow['ContentType']],
									"_ListTemplateType_"				=>	$ArrayLanguage["Texts"]["00163"][$EachRow['TemplateType']],
									);
			$ObjectTemplate->duplicate_block("LIST:NEWSLETTER:TEMPLATES", "LIST:NEWSLETTER:TEMPLATES", $ArrayReplaceList);
			}
		// List newsletter templates - FINISHED

		// Show/Hide second step - STARTED
		if ($Step2Proceed != true)
			{
			$ObjectTemplate->remove_block("ENABLE:STEP2");
			}
		else
			{
			$ObjectTemplate->remove_block("ENABLE:STEP1");
			}
		// Show/Hide second step - FINISHED

		// Retrieve the newsletter information if selected - STARTED
		if ($Step2Proceed == true)
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayTemplate = mysql_fetch_assoc($ResultSet);

			// Make some replacements - STARTED
			$ArrayReplaceList = array(
									"_enteredNewsletterTemplateID_"		=>	$ArrayTemplate['NewsletterTemplateID'],
									);
			$ObjectTemplate->replace($ArrayReplaceList);
			// Make some replacements - FINISHED

			// Show "traditional" or "cms" editing part - STARTED	
			if ($ArrayContent['ContentMain'] != '')
				{
				$ObjectTemplate->remove_block("SHOW:TEMPLATETYPE:CMS");

				// Show/Hide rich text editor according to content type selection and administrator prefs - STARTED
				if (($ArrayContent['ContentType'] == 'Plain') || ($ArrayLoggedAdmin['RichTextMode'] == 'Disabled'))
					{
					$ObjectTemplate->remove_block("SHOW:RICH:TEXT");
					}
				else
					{
					$ObjectTemplate->remove_block("SHOW:TEXT:AREA");
			
					// Rich text editor parsing - STARTED
					include_once('../system/libraries/richtext.inc.php');
				
						$ArrayParameters	= array(
													'FieldName'					=>	'FormValue_ContentMain',
													'Value'						=>	(count($ArrayPostVariables) < 1 ? $ArrayContent['ContentMain'] : stripslashes($ArrayPostVariables['FormValue_ContentMain'])),
													'BasePath'					=>	$ArrayConfig['URLs']['Data'].'/editors/fckeditor/',
													'ToolbarSet'				=>	'oemPro',
													'Width'						=>	'100%',
													'Height'					=>	'500',
													'FullPage'					=>	true,
													'AutoDetectLanguage'		=>	false,
													'DefaultLanguage'			=>	'en',
													'StartUpFocus'				=>	false,
													'UseBROnCarriageReturn'		=>	false,
													'EnableXHTML'				=>	true,
													'EnableSourceXHTML'			=>	true,
													);
						$EditorPath			= '../system/data/editors';
					$RichTextHTMLCode = GenerateRichText($ArrayParameters, $EditorPath, $ArrayLanguage, $ArrayConfig);
				
					$ArrayReplaceList = array(
											'_InsertRichTextEditor_'		=> $RichTextHTMLCode,
											);
					$ObjectTemplate->Replace($ArrayReplaceList);
					// Rich text editor parsing - FINISHED
					}
				// Show/Hide rich text editor according to content type selection and administrator prefs - FINISHED
			
				// Show/Hide alternate content - STARTED
				if (($ArrayContent['ContentType'] == 'Plain') || ($ArrayContent['ContentType'] == 'Html'))
					{
					$ObjectTemplate->remove_block("SHOW:ALTERNATE:CONTENT");
					}
				// Show/Hide alternate content - FINISHED
				}
			else
				{
				$ObjectTemplate->remove_block("SHOW:TEMPLATETYPE:TRADITIONAL");

				// Analyse the body and retrieve the CMS parts - STARTED
					// Include the required module - STARTED
					include_once("../system/libraries/campaign_cms.inc.php");
					// Include the required module - FINISHED

						if ($ArrayTemplate['ContentType'] == 'Html')
							{
							$Template1 = $ArrayTemplate['ContentHTML'];
							}
						elseif ($ArrayTemplate['ContentType'] == 'Plain')
							{
							$Template1 = $ArrayTemplate['ContentPlain'];
							}
						elseif ($ArrayTemplate['ContentType'] == 'Both')
							{
							$Template1 = $ArrayTemplate['ContentHTML'];
							$Template2 = $ArrayTemplate['ContentPlain'];
							}
				$ArrayCMSParts = RetrieveCMSParts($Template1);
				if ($Template2 != '')
					{
					$ArrayCMSParts = array_merge($ArrayCMSParts, RetrieveCMSParts($Template2));
					}
				// Analyse the body and retrieve the CMS parts - FINISHED

				// List the CMS Parts in the page - STARTED
				$ObjectTemplate->define_block("LIST:CMS:PARTS");
					$ObjectTemplate->define_block_in_block("SHOW:RICH:TEXT", "LIST:CMS:PARTS");
					$ObjectTemplate->define_block_in_block("SHOW:TEXT:AREA", "LIST:CMS:PARTS");
				
				$TMPCounter = 0;
				foreach ($ArrayCMSParts as $Key => $ArrayValues)
					{
					$ArrayFormFields['CMSContent'.$TMPCounter]	=	'TextArea';

					// Display the correct field (HTML or Plain) - STARTED
					if (($ArrayValues['FieldType'] == 'html') && ($ArrayLoggedAdmin['RichTextMode'] != 'Disabled'))
						{
						// Rich text editor parsing - STARTED
						include_once('../system/libraries/richtext.inc.php');
					
							$ArrayParameters	= array(
														'FieldName'					=>	'FormValue_CMSContent'.$TMPCounter,
														'Value'						=>	($ArrayPostVariables['FormValue_CMSContent'.$TMPCounter] == '' ?  $ArrayContent['CMSParts'][$ArrayValues['PartName']]['Content'] : stripslashes($ArrayPostVariables['FormValue_CMSContent'.$TMPCounter])),
														'BasePath'					=>	$ArrayConfig['URLs']['Data'].'/editors/fckeditor/',
														'ToolbarSet'				=>	'oemPro',
														'Width'						=>	'100%',
														'Height'					=>	'500',
														'FullPage'					=>	false,
														'AutoDetectLanguage'		=>	false,
														'DefaultLanguage'			=>	'en',
														'StartUpFocus'				=>	false,
														'UseBROnCarriageReturn'		=>	false,
														'EnableXHTML'				=>	true,
														'EnableSourceXHTML'			=>	true,
														);
							$EditorPath			= '../system/data/editors';
						$RichTextHTMLCode = GenerateRichText($ArrayParameters, $EditorPath, $ArrayLanguage, $ArrayConfig);
					
						$ArrayReplaceList = array(
												'_InsertRichTextEditor'.$TMPCounter.'_'		=> $RichTextHTMLCode,
												);
						$ObjectTemplate->Replace($ArrayReplaceList);
						// If form is not posted, display the default value - FINISHED

						// Rich text editor parsing - FINISHED

						$ArrayReplaceList = array(
												"_TMPCounter_"			=>		$TMPCounter,
												"_PartName_"			=>		$ArrayValues['PartName'],
												"_FieldType_"			=>		$ArrayValues['FieldType'],
												);
						$ObjectTemplate->duplicate_block_in_block("SHOW:RICH:TEXT", "LIST:CMS:PARTS", $ArrayReplaceList);
						}
					elseif (($ArrayValues['FieldType'] == 'html') && ($ArrayLoggedAdmin['RichTextMode'] == 'Disabled'))
						{
						$ArrayReplaceList = array(
												"_TMPCounter_"			=>		$TMPCounter,
												"_PartName_"			=>		$ArrayValues['PartName'],
												"_FieldType_"			=>		$ArrayValues['FieldType'],
												);
						$ObjectTemplate->duplicate_block_in_block("SHOW:TEXT:AREA", "LIST:CMS:PARTS", $ArrayReplaceList);

						// If form is not posted, display the default value - STARTED
						if ($ArrayPostVariables['FormButton_Proceed5'] == '')
							{
							$ArrayPostVariables['FormValue_CMSContent'.$TMPCounter]	=	$ArrayContent['CMSParts'][$ArrayValues['PartName']]['Content'];
							}
						// If form is not posted, display the default value - FINISHED
						}
					elseif ($ArrayValues['FieldType'] == 'plain')
						{
						$ArrayReplaceList = array(
												"_TMPCounter_"			=>		$TMPCounter,
												"_PartName_"			=>		$ArrayValues['PartName'],
												"_FieldType_"			=>		$ArrayValues['FieldType'],
												);
						$ObjectTemplate->duplicate_block_in_block("SHOW:TEXT:AREA", "LIST:CMS:PARTS", $ArrayReplaceList);
						
						// If form is not posted, display the default value - STARTED
						if ($ArrayPostVariables['FormButton_Proceed5'] == '')
							{
							$ArrayPostVariables['FormValue_CMSContent'.$TMPCounter]	=	$ArrayContent['CMSParts'][$ArrayValues['PartName']]['Content'];
							}
						// If form is not posted, display the default value - FINISHED
						}
					else
						{
						$ArrayReplaceList = array(
												"_TMPCounter_"			=>		$TMPCounter,
												"_PartName_"			=>		$ArrayValues['PartName'],
												"_FieldType_"			=>		$ArrayValues['FieldType'],
												);
						$ObjectTemplate->duplicate_block_in_block("SHOW:TEXT:AREA", "LIST:CMS:PARTS", $ArrayReplaceList);
						}
					// Display the correct field (HTML or Plain) - FINISHED

					$ArrayReplaceList = array(
											"_PartName_"			=>		$ArrayValues['PartName'],
											"_FieldType_"			=>		$ArrayValues['FieldType'],
											);
					$ObjectTemplate->duplicate_block("LIST:CMS:PARTS", "LIST:CMS:PARTS", $ArrayReplaceList);

					$TMPCounter++;
					}
				// List the CMS Parts in the page - FINISHED
				}	
			// Show "traditional" or "cms" editing part - FINISHED
			}
		// Retrieve the newsletter information if selected - FINISHED
		}
	// Display the correct content section - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 5 Page Parsing: Content creating
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep5($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Retrieve the content and decide how to proceed - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		// Retrieve content - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);
		// Retrieve content - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);
			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve the content and decide how to proceed - FINISHED

	// Parse content information - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		include_once('../system/classes/class_octethfiller.php');
		$ObjectFiller = new OctethFiller();
		
			$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
			$ObjectFiller->Prefix				=	"_entered";
			$ObjectFiller->RecordContent		=	$ArrayContent;
		
			$ObjectFiller->ParseTemplate();
		$ObjectTemplate = $ObjectFiller->ReturnTemplate();
		
		$ArrayReplaceList = array(
								"_selectedradioContentMainSourceUrl_"			=>		($ArrayContent['ContentMainURL'] != '' ? "checked" : ""),
								"_selectedradioContentMainSourceHere_"			=>		($ArrayContent['ContentMainURL'] == '' ? "checked" : ""),
								"_selectedradioContentAlternateSourceUrl_"		=>		($ArrayContent['ContentAlternateURL'] != '' ? "checked" : ""),
								"_selectedradioContentAlternateSourceHere_"		=>		($ArrayContent['ContentAlternateURL'] == '' ? "checked" : "")
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		if ($ArrayContent['ContentMain'] != '')
			{
			include_once('../system/classes/class_octethfiller.php');
			$ObjectFiller = new OctethFiller();

				$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
				$ObjectFiller->Prefix				=	"_entered";
				$ObjectFiller->RecordContent		=	$ArrayContent;
			
				$ObjectFiller->ParseTemplate();
			$ObjectTemplate = $ObjectFiller->ReturnTemplate();
			}
		else
			{
			include_once('../system/classes/class_octethfiller.php');
			$ObjectFiller = new OctethFiller();

				$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
				$ObjectFiller->Prefix				=	"_entered";
				$ObjectFiller->RecordContent		=	$ArrayContent;

				$ObjectFiller->ParseTemplate();
			$ObjectTemplate = $ObjectFiller->ReturnTemplate();

			// Select the newsletter template - STARTED
			$ArrayReplaceList = array(
									"_selectedlistNewsletterTemplateID".$ArrayContent['RelNewsletterTemplateID']."_"			=>		"selected",
									);
			$ObjectTemplate->replace($ArrayReplaceList);
			// Select the newsletter template - FINISHED
			}
		}
	// Parse content information - STARTED

	// Parse generic information - STARTED
	if ($ArrayContent['Personalize'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxTrackingFeaturesPersonalize_"			=>		"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	if ($ArrayContent['TrackLinkClicks'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxTrackingFeaturesTrackLinks_"			=>		"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	if ($ArrayContent['TrackReads'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxTrackingFeaturesTrackReads_"			=>		"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	if ($ArrayContent['EmbedObjects'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxEmbedObjectsYes_"			=>		"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	$ArrayReplaceList = array(
							"_selectedradioContentType".$ArrayContent['ContentType']."_"		=>		"checked",
							"_selectedlistCharSet".$ArrayContent['CharSet']."_"					=>		"selected",
							"_selectedlistPriority".$ArrayContent['Priority']."_"				=>		"selected",
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse generic information - FINISHED

	// Detect if multi-subject mode is used. If yes, switch to multi-subject mode - STARTED
	if (count(explode("\n", $ArrayContent['Subject'])) > 1)
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxMultiSubjectsEnableYes_"				=>		"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	// Detect if multi-subject mode is used. If yes, switch to multi-subject mode - FINISHED


	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}
	
//########################################################################################
// STEP 4 Page Parsing: SMTP Server and MTA Setting
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep4($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// If "forced sending method" feature is enabled for this administrator, then add the default sending method and skip this step - STARTED
	if ($ArrayLoggedAdmin['ForcedSendingMethod'] != 'N/A')
		{
		// Update campaign - STARTED
		if ($ErrorExists == false)
			{
			if ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Qmail')
				{
				$SendingMethod = 'QMail';
				}
			elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Sendmail')
				{
				$SendingMethod = 'Sendmail';
				}
			elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Local MTA')
				{
				$SendingMethod = 'MTA';
				}
			elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Raw File')
				{
				$SendingMethod = 'SaveAsFile';
				}
			elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'SMTP Server')
				{
				$SendingMethod = 'SMTP';
				}
			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."campaigns SET SendEngine='".$SendingMethod."' WHERE CampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		// Update campaign - FINISHED

		// Add record - STARTED
		if ($ErrorExists == false)
			{
			if ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'SMTP Server')
				{
				$ArrayFieldnValues = array(
										"AssignID"			=>	'',
										"RelSMTPServerID"	=>	$ArrayLoggedAdmin['ForcedSendingMethodID'],
										"RelCampaignID"		=>	$ArrayCampaign['CampaignID'],
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList ="'".$ValueList."'";
		
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."campaigns_smtp (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				}
			}
		// Add record - FINISHED
	
		// Update the last step of the campaign - STARTED
		if ($ErrorExists == false)
			{
			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."campaigns SET IntegrityLastStep='5' WHERE CampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		// Update the last step of the campaign - FINISHED

		// Proceed with the next step - STARTED
		if ($ErrorExists == false)
			{
			FinalProcesses();
			header("Location: ./campaign_details.php?CampaignID=".$ArrayCampaign['CampaignID']."&Step=5");
			exit;
			}
		// Proceed with the next step - FINISHED
		}
	// If "forced sending method" feature is enabled for this administrator, then add the default sending method and skip this step - FINISHED
	
	// Retrieve all assigned mail lists for the campaign - STARTED
	$SQLQuery = "SELECT RelMailListID FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		$ArrayMailLists = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayMailLists[] = $EachRow['RelMailListID'];
		}
	// Retrieve all assigned mail lists for the campaign - FINISHED

	// List SMTP Servers of the selected mail lists - STARTED
		// Include required library - STARTED
		include_once("../system/libraries/maillists.inc.php");
		// Include required library - FINISHED

	$ObjectTemplate->define_block("LIST:SMTP");

	$ArraySMTP = GetSMTPServersOfMailLists($ArrayMailLists, $ArrayConfig);

	if (count($ArraySMTP) > 0)
		{
		foreach ($ArraySMTP as $Key=>$ArrayVals)
			{
			$ArrayReplaceList = array(
									"_ListSMTPServerID_"	=>	$ArrayVals['SMTPServerID'],
									"_ListServerName_"		=>	$ArrayVals['Name'],
									"_ListServerHost_"		=>	$ArrayVals['Host'],
									);
			$ObjectTemplate->duplicate_block("LIST:SMTP", "LIST:SMTP", $ArrayReplaceList);
			}
		}
	else
		{
		$ObjectTemplate->remove_block("ENABLE:SMTP");
		}
	// List SMTP Servers of the selected mail lists - FINISHED
	
	// Enable/Disable QMail and Sendmail option according to configuration - STARTED
	if ($ArrayConfig['Settings']['IsQMailInstalled'] == 'No')
		{
		$ObjectTemplate->remove_block("ENABLE:QMAIL");
		}
	if ($ArrayConfig['Settings']['IsSendmailInstalled'] == 'No')
		{
		$ObjectTemplate->remove_block("ENABLE:SENDMAIL");
		}
	if ($ArrayConfig['Settings']['IsSaveRawEmailsInstalled'] == 'Disabled')
		{
		$ObjectTemplate->remove_block("ENABLE:RAWEMAIL");
		}
	// Enable/Disable QMail and Sendmail option according to configuration - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}
	
//########################################################################################
// STEP 4 Page Parsing: SMTP Server and MTA Setting
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep4($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Parse campaign smtp server type and servers - STARTED
		$ArrayReplaceList = array(
								"_selectedradioSendEngine".$ArrayCampaign['SendEngine']."_"			=>	"checked",
								);
	$ObjectTemplate->replace($ArrayReplaceList);	
	// Parse campaign smtp server type and servers - FINISHED
	
	// Select the pre-selected SMTP servers - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayReplaceList = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList["_selectedmultlistSMTPServerID".$EachRow['RelSMTPServerID']."_"]	=	"selected";
		}
	$ObjectTemplate->replace($ArrayReplaceList);	
	// Select the pre-selected SMTP servers - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}
	
//########################################################################################
// STEP 3 Page Parsing: Campaign Information and Clients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep3($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED
	
	// List clients in the list field - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."clients WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' ORDER BY Name ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ObjectTemplate->define_block("LIST:CLIENTS");
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList = array(
								"_ListClientID_"		=>		$EachRow['ClientID'],
								"_ListName_"			=>		$EachRow['Name'],
								"_ListEmail_"			=>		$EachRow['Email'],
								);
		$ObjectTemplate->duplicate_block("LIST:CLIENTS", "LIST:CLIENTS", $ArrayReplaceList);
		}
	// List clients in the list field - FINISHED

	// List email addresses - STARTED
		// Include required library - STARTED
		include_once("../system/libraries/maillists.inc.php");
		// Include required library - FINISHED

	$ObjectTemplate->define_block("LIST:EMAILS:FROM");
	$ObjectTemplate->define_block("LIST:EMAILS:REPLYTO");
	$ObjectTemplate->define_block("LIST:EMAILS:RETURN");
	$ObjectTemplate->define_block("LIST:EMAILS");
	$ObjectTemplate->define_block("LIST:ADMIN:EMAILS:FROM");
	$ObjectTemplate->define_block("LIST:ADMIN:EMAILS:REPLYTO");
	$ObjectTemplate->define_block("LIST:ADMIN:EMAILS:RETURN");

		// Retrieve the campaign mail lists - STARTED
		$ArrayMailListIDs = array();
		
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListIDs[] = $EachRow['RelMailListID'];
			}
		// Retrieve the campaign mail lists - FINISHED
		
	$ArrayEmailAddresses = GetEmailAddressesOfMailLists($ArrayMailListIDs, $ArrayConfig);
	
	foreach ($ArrayEmailAddresses as $Key=>$ArrayVals)
		{
		$ArrayReplaceList = array(
								"_ListEmailID_"		=>	"M".$ArrayVals['EmailID'],
								"_ListName_"		=>	$ArrayVals['Name'],
								"_ListEmail_"		=>	$ArrayVals['Email'],
								);
		$ObjectTemplate->duplicate_block("LIST:EMAILS:FROM", "LIST:EMAILS:FROM", $ArrayReplaceList);
		$ObjectTemplate->duplicate_block("LIST:EMAILS:REPLYTO", "LIST:EMAILS:REPLYTO", $ArrayReplaceList);
		$ObjectTemplate->duplicate_block("LIST:EMAILS:RETURN", "LIST:EMAILS:RETURN", $ArrayReplaceList);
		}

	$ArrayEmailAddresses = GetEmailAddressesOfAdministrator($ArrayLoggedAdmin['AdministratorID'], $ArrayConfig);
	
	foreach ($ArrayEmailAddresses as $Key=>$ArrayVals)
		{
		$ArrayReplaceList = array(
								"_ListEmailID_"		=>	"A".$ArrayVals['EmailID'],
								"_ListName_"		=>	$ArrayVals['Name'],
								"_ListEmail_"		=>	$ArrayVals['Email'],
								);
		$ObjectTemplate->duplicate_block("LIST:ADMIN:EMAILS:FROM", "LIST:ADMIN:EMAILS:FROM", $ArrayReplaceList);
		$ObjectTemplate->duplicate_block("LIST:ADMIN:EMAILS:REPLYTO", "LIST:ADMIN:EMAILS:REPLYTO", $ArrayReplaceList);
		$ObjectTemplate->duplicate_block("LIST:ADMIN:EMAILS:RETURN", "LIST:ADMIN:EMAILS:RETURN", $ArrayReplaceList);
		}
	// List email addresses - FINISHED

	// Show/Hide return path email address based on administrator information - STARTED
	if ($ArrayLoggedAdmin['ReturnPathEmail'] != '')
		{
		$ObjectTemplate->remove_block('SHOWHIDE:RETURNPATH');
		}
	// Show/Hide return path email address based on administrator information - FINISHED

	// List years in the specific date section - STARTED
	$ObjectTemplate->define_block("LIST:SPECIFICDATE:YEAR");
	
	$CurrentYear	= date("Y");
	$TopYear		= $CurrentYear + 10;
	
	for ($TMPCounter = $CurrentYear; $TMPCounter <= $TopYear; $TMPCounter++)
		{
		$ArrayReplaceList = array(
								"_ListYear_"		=>	$TMPCounter,
								);
		$ObjectTemplate->duplicate_block("LIST:SPECIFICDATE:YEAR", "LIST:SPECIFICDATE:YEAR", $ArrayReplaceList);
		}
	// List years in the specific date section - FINISHED

	// Parse server time - STARTED
	$ArrayReplaceList = array(
							"_ServerTime_"	=> date('r'),
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse server time - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 3 Page Parsing: Campaign Information and Clients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep3($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Select the campaign clients - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_clients WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayReplaceList = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList["_selectedmultlistClientIDs".$EachRow['RelClientID']."_"] = "selected";
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Select the campaign clients - FINISHED
	
	// Parse schedule options - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArraySchedule = mysql_fetch_assoc($ResultSet);

	include_once('../system/classes/class_octethfiller.php');
	$ObjectFiller = new OctethFiller();
	
		$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
		$ObjectFiller->Prefix				=	"_entered";
		$ObjectFiller->RecordContent		=	$ArraySchedule;
	
		$ObjectFiller->ParseTemplate();
	$ObjectTemplate = $ObjectFiller->ReturnTemplate();

		$ArrayReplaceList = array(
								"_selectedradioScheduleType".$ArraySchedule['ScheduleType']."_"					=>	"checked",
								);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse schedule options - FINISHED

	// Parse "one-time scheduled" options - STARTED
	$TMPArrayDate = explode(" ", $ArraySchedule['ScheduleSendDate']);
		$TMPArrayDate['Date'] = explode("-", $TMPArrayDate[0]);
		$TMPArrayDate['Time'] = explode(":", $TMPArrayDate[1]);

		$ArrayReplaceList = array(
								"_selectedlistScheduleSendDateHour".$TMPArrayDate['Time'][0]."_"					=>	"selected",
								"_selectedlistScheduleSendDateMinute".$TMPArrayDate['Time'][1]."_"					=>	"selected",
								"_selectedlistScheduleSendDateDay".$TMPArrayDate['Date'][2]."_"					=>	"selected",
								"_selectedlistScheduleSendDateMonth".$TMPArrayDate['Date'][1]."_"					=>	"selected",
								"_selectedlistScheduleSendDateYear".$TMPArrayDate['Date'][0]."_"					=>	"selected",
								);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse "one-time scheduled" options - FINISHED
	
	// Parse "recursively scheduled" options - STARTED
	$TMPArrayDoM	=	explode(",", $ArraySchedule['ScheduleDayOfMonth']);
	$TMPArrayDoW	=	explode(",", $ArraySchedule['ScheduleDayOfWeek']);
	$TMPArrayMonth	=	explode(",", $ArraySchedule['ScheduleMonth']);
	$TMPArrayHour	=	explode(",", $ArraySchedule['ScheduleHour']);
	$TMPArrayMinute	=	explode(",", $ArraySchedule['ScheduleMinute']);

	$ArrayReplaceList = array();
	foreach ($TMPArrayDoM as $Each)
		{
		$ArrayReplaceList["_selectedmultlistScheduleDayOfMonth".$Each."_"]	=	"selected";
		}
	foreach ($TMPArrayDoW as $Each)
		{
		$ArrayReplaceList["_selectedmultlistScheduleDayOfWeek".$Each."_"]	=	"selected";
		}
	foreach ($TMPArrayMonth as $Each)
		{
		$ArrayReplaceList["_selectedmultlistScheduleMonth".$Each."_"]	=	"selected";
		}
	foreach ($TMPArrayHour as $Each)
		{
		$ArrayReplaceList["_selectedmultlistScheduleHour".$Each."_"]	=	"selected";
		}
	foreach ($TMPArrayMinute as $Each)
		{
		$ArrayReplaceList["_selectedmultlistScheduleMinute".$Each."_"]	=	"selected";
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse "recursively scheduled" options - FINISHED
	
	// Parse campaign information - STARTED

	include_once('../system/classes/class_octethfiller.php');
	$ObjectFiller = new OctethFiller();
	
		$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
		$ObjectFiller->Prefix				=	"_entered";
		$ObjectFiller->RecordContent		=	$ArrayCampaign;
	
		$ObjectFiller->ParseTemplate();
	$ObjectTemplate = $ObjectFiller->ReturnTemplate();

		$ArrayReplaceList = array(
								"_selectedradioCampaignType".$ArrayCampaign['CampaignType']."_"					=>	"checked",
								);
	$ObjectTemplate->replace($ArrayReplaceList);

	if ($ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxSendReportOptionsAdmin_"	=>	"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}

	if ($ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		$ArrayReplaceList = array(
								"_selectedcheckboxSendReportOptionsClients_"			=>	"checked",
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	// Parse campaign information - FINISHED
	
	// Parse from, reply to and return path email address selections - STARTED
	$FromType		= ($ArrayCampaign['FromEmailIDType'] == 'Administrator' ? 'A' : 'M');
	$ReplyToType	= ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator' ? 'A' : 'M');
	$ReturnPathType	= ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator' ? 'A' : 'M');
	
	$ArrayReplaceList = array(
							"_selectedlistFromEmailID".$FromType.$ArrayCampaign['RelFromEmailID']."_"					=>	"selected",
							"_selectedlistReplyToEmailID".$ReplyToType.$ArrayCampaign['RelReplyToEmailID']."_"			=>	"selected",
							"_selectedlistReturnPathEmailID".$ReturnPathType.$ArrayCampaign['RelReturnPathEmailID']."_"	=>	"selected",
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse from, reply to and return path email address selections - FINISHED

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 2 Page Parsing: Send Rules
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep2($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED
	
	// Retrieve all sending rules of the campaign - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalRules = mysql_fetch_assoc($ResultSet);
			$TotalRules = $TotalRules['TotalFound'];
		if ($TotalRules < 1)
			{
			// No sending rules exist. Display "no rules set" message
			if ($ArrayErrorMessages['PageMessage'] == '')
				{
				$ArrayErrorMessages['PageMessage'] = $ArrayLanguage["Texts"]["00210"];
				}
			}
		}
	// Retrieve all sending rules of the campaign - FINISHED
	
	// List send rules. If no rules are set, display blank rule form only - STARTED
	if ($ErrorExists == false)
		{
		// Retrieve all assigned mail lists for the campaign - STARTED
		$SQLQuery = "SELECT RelMailListID FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
			$ArrayMailLists = array();
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailLists[] = $EachRow['RelMailListID'];
			}
		// Retrieve all assigned mail lists for the campaign - FINISHED

		// Retrieve the fields of this mail list - STARTED
			// Include import library - STARTED
			include_once("../system/libraries/import.inc.php");
			// Include import library - FINISHED

			$SelectedMailListIDs = $ArrayMailLists;
		$ArrayOEMPFields = ReturnOEMPFields($SelectedMailListIDs, $ArrayLanguage, $ArrayConfig);
		$ArrayOEMPFields = array_merge($ArrayOEMPFields, ReturnOutsourcedMailListFields($SelectedMailListIDs, $ArrayLanguage, $ArrayConfig));
		// Retrieve the fields of this mail list - FINISHED

		// List rules - STARTED
		$ObjectTemplate->define_block("LIST:RULES");
		$ObjectTemplate->define_block_in_block("LIST:RULES:FIELDS", "LIST:RULES");

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayFormFields['SendRule'.$EachRow['RuleID']]				=	"TextField";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Field']		=	"DropList";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Operator1']	=	"DropList";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Value1']	=	"TextField";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Boolean1']	=	"DropList";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Operator2']	=	"DropList";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Value2']	=	"TextField";
			$ArrayFormFields['SendRule'.$EachRow['RuleID'].'Boolean2']	=	"DropList";
			
			// Select the values of the form if form is not posted - STARTED
			if (($ArrayPostVariables['FormButton_AddSendRule'] == '') && ($ArrayPostVariables['FormButton_UpdateSendRule'] == '') && ($ArrayPostVariables['FormButton_DeleteSendRule'] == ''))
				{
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Field']		=	$EachRow['Key'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Operator1']	=	$EachRow['Operator1'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Value1']	=	$EachRow['Value1'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Boolean1']	=	$EachRow['Boolean1'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Operator2']	=	$EachRow['Operator2'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Value2']	=	$EachRow['Value2'];
				$ArrayPostVariables['FormValue_SendRule'.$EachRow['RuleID'].'Boolean2']	=	$EachRow['Boolean2'];
				}
			// Select the values of the form if form is not posted - FINISHED

			// List fields - STARTED
			foreach ($ArrayOEMPFields as $Field=>$FieldName)
				{
				$ArrayReplaceList = array(
										"_RuleID_"				=>	$EachRow['RuleID'],
										"_ListField_"			=>	$Field,
										"_ListFieldName_"		=>	$FieldName,
										);
				$ObjectTemplate->duplicate_block_in_block("LIST:RULES:FIELDS", "LIST:RULES", $ArrayReplaceList);
				}
			// List fields - FINISHED
	
			$ArrayReplaceList = array(
									"_RuleID_"		=>	$EachRow['RuleID'],
									);
			$ObjectTemplate->duplicate_block("LIST:RULES", "LIST:RULES", $ArrayReplaceList);
			}
		// List rules - FINISHED
		
		// Display empty rule form at the end - STARTED
		$ObjectTemplate->define_block("ADD:NEW:RULE");
		$ObjectTemplate->define_block_in_block("ADD:NEW:RULE:FIELDS", "ADD:NEW:RULE");

		$ArrayFormFields['SendRule0']			=	"TextField";
		$ArrayFormFields['SendRule0Field']		=	"DropList";
		$ArrayFormFields['SendRule0Operator1']	=	"DropList";
		$ArrayFormFields['SendRule0Value1']		=	"TextField";
		$ArrayFormFields['SendRule0Boolean1']	=	"DropList";
		$ArrayFormFields['SendRule0Operator2']	=	"DropList";
		$ArrayFormFields['SendRule0Value2']		=	"TextField";
		$ArrayFormFields['SendRule0Boolean2']	=	"DropList";

			// List fields - STARTED
			foreach ($ArrayOEMPFields as $Field=>$FieldName)
				{
				$ArrayReplaceList = array(
										"_RuleID_"				=>	0,
										"_ListField_"			=>	$Field,
										"_ListFieldName_"		=>	$FieldName,
										);
				$ObjectTemplate->duplicate_block_in_block("ADD:NEW:RULE:FIELDS", "ADD:NEW:RULE", $ArrayReplaceList);
				}
			// List fields - FINISHED

		$ArrayReplaceList = array(
								"_RuleID_"		=>	0,
								);
		$ObjectTemplate->duplicate_block("ADD:NEW:RULE", "ADD:NEW:RULE", $ArrayReplaceList);
		// Display empty rule form at the end - FINISHED
		}	
	// List send rules. If no rules are set, display blank rule form only - FINISHED
	
	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 2 Page Parsing: Send Rules
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep2($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{

	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}
	
//########################################################################################
// STEP 1 Page Parsing: Select mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseStep1($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// List mail lists - STARTED
	$SQLQuery 			 = "SELECT tblMailLists.* FROM ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
		$SQLQuery		.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_administrators AS tblAdministrators ";
		$SQLQuery		.= "ON tblMailLists.MailListID = tblAdministrators.RelMailListID ";
		
		// Apply filtering options - STARTED
		$SQLQueryPart = array();
		
		$SQLQueryPart[] = "tblAdministrators.RelAdministratorID = '".$ArrayLoggedAdmin['AdministratorID']."'";
		// Apply filtering options - FINISHED
	
		$SQLQuery = $SQLQuery.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
		$SQLQuery .= "ORDER BY tblMailLists.Name ASC ";
	
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ObjectTemplate->define_block("LIST:MAILLISTS:LOCAL");
	$ObjectTemplate->define_block("LIST:MAILLISTS:OUTSOURCED");
	
	include_once("../system/libraries/maillists.inc.php");

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Find the total active member of the mail list - STARTED
		// $SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		// $SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		// $SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
		// $SQLQuery .= "WHERE tblAssign.RelMailListID = '".$EachRow['MailListID']."' AND (tblAssign.SubscriptionStatus='Subscribed' OR tblAssign.SubscriptionStatus='Pending for opt-out confirmation') AND tblMembers.BounceType!='Hard bounce'";
		// $ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// 	$TotalMembers = mysql_fetch_assoc($ResultSet2);
		// 		$TotalMembers = $TotalMembers['TotalFound'];
		// 
		// if ($EachRow['SourceType'] != 'Local')
		// 	{
		// 	$TotalMembers += FindTotalMemberOfOutsourcedList($EachRow, $ArrayConfig, array());
		// 	}
		// Find the total active member of the mail list - FINISHED

		$ArrayReplaceList = array(
								"_ListMailListID_"		=>	$EachRow['MailListID'],
								"_ListName_"			=>	$EachRow['Name'],
								"_ListTotalMembers_"	=>	number_format($TotalMembers),
								"_ListSourceType_"		=>	$ArrayLanguage["Texts"]["00034"][$EachRow['SourceType']],
								);

		$MailListTypeBlock = ($EachRow['SourceType'] == 'Local' ? 'LOCAL' : 'OUTSOURCED');

		$ObjectTemplate->duplicate_block("LIST:MAILLISTS:".$MailListTypeBlock, "LIST:MAILLISTS:".$MailListTypeBlock, $ArrayReplaceList);
		}
	// List mail lists - FINISHED
	
	// Select the owned mail lists - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayReplaceList = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList["_selectedlistMailListIDs".$EachRow['RelMailListID']."_"] = "selected";
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Select the owned mail lists - FINISHED
	
	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// STEP 1 Page Parsing: Select mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ShowRecordStep1($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $ArrayCampaign, $ArrayLoggedAdmin, $ArrayPostVariables, $ArrayErrorMessages, $ArrayFormFields)
	{
	// Select the owned mail lists - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayReplaceList	= array();
	$MailListType		= 'Local';

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayReplaceList["_selectedmultlistMailListIDs".$EachRow['RelMailListID']."_"] = "selected";

		// Check if mail list is local or outsourced - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachRow['RelMailListID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMailList = mysql_fetch_assoc($ResultSet2);

		if ($ArrayMailList['SourceType'] != 'Local')
			{
			$MailListType = 'Outsourced';
			}
		// Check if mail list is local or outsourced - FINISHED
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Select the owned mail lists - FINISHED
	
	// Select correct mail list group radio button - STARTED
		$ArrayReplaceList["_selectedradioMailListType".$MailListType."_"] = "checked";
	$ObjectTemplate->replace($ArrayReplaceList);
	// Select correct mail list group radio button - FINISHED
	
	return array($ObjectTemplate, $ArrayErrorMessages, $ArrayFormFields, $ArrayPostVariables);
	}

//########################################################################################
// Parses the steps to the page
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseSteps($ObjectTemplate, $ArrayLanguage, $ArrayConfig, $TotalSteps, $CurrentStep, $CampaignID)
	{	
	// Hide the entrance step (step 0) -STARTED
	if ($CurrentStep != 0)
		{
		$ObjectTemplate->remove_block("ENABLE:STEP:0");
		}
	// Hide the entrance step (step 0) -FINISHED

	for ($TMPCounter = 1; $TMPCounter <= $TotalSteps; $TMPCounter++)
		{
		// Define the class and link for the step - STARTED
		if ($CurrentStep == $TMPCounter)
			{
			$TMPClass 	= "Current";
			}
		else
			{
			$TMPClass 	= "";
			}
		$TMPLink	= "./campaign_details.php?CampaignID=".$CampaignID."&Step=".$TMPCounter;
		// Define the class and link for the step - FINISHED
	
		$ArrayReplaceList = array(
								"_Step:".$TMPCounter.":Link_"		=>	$TMPLink,
								"_Step:".$TMPCounter.":Class_"		=>	$TMPClass,
								);
		$ObjectTemplate->replace($ArrayReplaceList);

		// Hide the steps which are not the current step -STARTED
		if ($TMPCounter != $CurrentStep)
			{
			$ObjectTemplate->remove_block("ENABLE:STEP:".$TMPCounter);
			}
		// Hide the steps which are not the current step -FINISHED
		}
		
	return $ObjectTemplate;
	}

//########################################################################################
// Checks if campaign has an active sending or not
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckForActiveSending($CampaignID, $ArrayConfig)
	{	
	$SQLQueryPart  = "(";
	$SQLQueryPart .= "Status='Pending' OR ";
	$SQLQueryPart .= "Status='Sending' OR ";
	$SQLQueryPart .= "Status='Failed' OR ";
	$SQLQueryPart .= "Status='Halted'";
	$SQLQueryPart .= ")";
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$CampaignID."' AND ".$SQLQueryPart;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return $TotalFound;
		}
	else
		{
		return false;
		}
	}
?>