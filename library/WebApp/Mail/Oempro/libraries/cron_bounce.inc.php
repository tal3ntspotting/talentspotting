<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Connect to defined POP3 servers and process all bounced emails
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessBouncedEmailsIMAP($ArrayConfig, $ArrayLanguage)
	{
	// Retrieve defined mail boxes for request processing - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE UseFor LIKE '%Bounce Handling%'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	// Include the required modules - STARTED
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethemailparser.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethmimeutilities.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_mimedecode.php");
	// Include the required modules - FINISHED

	$TotalBounces = 0;
	
	
	while ($EachPOP3 = mysql_fetch_assoc($ResultSet))
		{
		// Create a connection to POP3 server and login - STARTED
		$ConnectionMail = imap_open("{".$EachPOP3["ServerHost"].":".$EachPOP3["ServerPort"]."/pop3/notls}", $EachPOP3["ServerUsername"], $EachPOP3["ServerPassword"]);
		// Create a connection to POP3 server and login- FINISHED

		// Retrieve headers - STARTED
		$TotalEmails  = imap_num_msg($ConnectionMail);
		// Retrieve headers - FINISHED

		// Analyze each email located in the mail box - STARTED
		for ($TMPCounter=1; $TMPCounter<=$TotalEmails; $TMPCounter++)
			{
			// Only process allowed number of emails per execution - STARTED
			if (($TMPCounter >= $EachPOP3['EmailsProcessAtOneTime']) && ($EachPOP3['EmailsProcessAtOneTime'] != 0))
				{
				break;
				}
			// Only process allowed number of emails per execution - FINISHED

			// Avoid time-out - STARTED
			print(' ');
			// Avoid time-out - FINISHED

			// Fetch header and body - STARTED
			$ObjectEmailHeader	= imap_header($ConnectionMail, $TMPCounter);
			$EmailHeader		= imap_fetchheader($ConnectionMail, $TMPCounter);
			$EmailBody			= imap_fetchbody($ConnectionMail, $TMPCounter, "1.1");

				if ($EmailBody == "")
					{
					$EmailBody = imap_fetchbody($ConnectionMail, $TMPCounter, "1");
					}
				//$EmailBody = trim(strip_tags($EmailBody)); ---------> Commented by Umut (strip_tags kills all email addresses.)
				$EmailBody = trim($EmailBody); 

				// Full Text Content with Attachments
				$EmailContent = imap_fetchtext($ConnectionMail, $TMPCounter, "1");
				$EmailContent = trim($EmailContent);
			// Fetch header and body - FINISHED

			// Retrieve the message ID - STARTED
			$EmailUniqueID = $ObjectEmailHeader->message_id;
				$EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
				$EmailUniqueID = str_replace('<', '', $EmailUniqueID);
				$EmailUniqueID = str_replace('>', '', $EmailUniqueID);
			// Retrieve the message ID - FINISHED

			// Parse the email content - STARTED
			$ArrayParsedHeaders = array();
			
				// Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
				$ArrayParsedHeaders['Subject']		=		strip_tags($ObjectEmailHeader->subject);
				$ArrayParsedHeaders['To']			=		$ObjectEmailHeader->to[0]->mailbox."@".$ObjectEmailHeader->to[0]->host;
				$ArrayParsedHeaders['From']			=		$ObjectEmailHeader->from[0]->mailbox."@".$ObjectEmailHeader->from[0]->host;
				// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
			// Parse the email content - FINISHED

			// Parse the email address of the sender - STARTED
			$SenderEmail = $ArrayParsedHeaders['From'];
			// Parse the email address of the sender - FINISHED

			// Prepare header and body - STARTED
			$ArrayEmailHeader	= explode("\n", $EmailHeader);
			$ArrayEmailBody 	= explode("\n", $EmailBody);
			// Prepare header and body - FINISHED

			// Decide if the email is a bounce report or not, if yes, process it - STARTED
			$IsBounceEmail = AnalyzeBody($ArrayEmailHeader, $ArrayEmailBody, $EmailContent, $EmailUniqueID, $EachPOP3, $ArrayConfig, $ArrayLanguage);
			
			if ($IsBounceEmail == true)
				{
				$TotalBounces++;
				}
			// Decide if the email is a bounce report or not, if yes, process it - FINISHED

			// Delete the email after processing - STARTED
			imap_delete($ConnectionMail, $TMPCounter);
			// Delete the email after processing - FINISHED
			}
		// Analyze each email located in the mail box - FINISHED

		// Close IMAP connection - STARTED
		imap_expunge($ConnectionMail);
		imap_close($ConnectionMail);
		// Close IMAP connection - FINISHED
		}
	// Retrieve defined mail boxes for request processing - FINISHED

	return $TotalBounces;
	}

//########################################################################################
// Connect to defined POP3 servers and process all bounced emails
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessBouncedEmails($ArrayConfig, $ArrayLanguage)
	{
	// Retrieve defined mail boxes for request processing - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE UseFor LIKE '%Bounce Handling%'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	// Include the required modules - STARTED
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_pop3.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethemailparser.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethmimeutilities.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_mimedecode.php");
	// Include the required modules - FINISHED

	$TotalBounces = 0;
	
	while ($EachPOP3 = mysql_fetch_assoc($ResultSet))
		{
		// Create a connection to POP3 server - STARTED
		$ObjectPOP3					= new pop3_class;
		$ObjectPOP3->hostname		= $EachPOP3["ServerHost"];
		$ObjectPOP3->port			= $EachPOP3["ServerPort"];

		$TMPErrorMsg = $ObjectPOP3->Open();
		// Create a connection to POP3 server - FINISHED

		// Login to POP3 server - STARTED
		if ($TMPErrorMsg == '')
			{
			$TMPErrorMsg = $ObjectPOP3->Login($EachPOP3["ServerUsername"], $EachPOP3["ServerPassword"], ($EachPOP3["ServerAPOP"] != 'No' ? 1 : 0));
			}
		// Login to POP3 server - FINISHED

		// Retrieve mail box statistics - STARTED
		if ($TMPErrorMsg == '')
			{
			$TMPErrorMsg = $ObjectPOP3->Statistics($TotalMessages, $TotalSizeBytes);
			}
		// Retrieve mail box statistics - FINISHED

		// Analyze each email located in the mail box - STARTED
		if (($TMPErrorMsg == '') && ($TotalMessages > 0))
			{
			// Start processing each email - STARTED
			for ($TMPCounter = 1; $TMPCounter <= $TotalMessages; $TMPCounter++)
				{
				// Only process allowed number of emails per execution - STARTED
				if (($TMPCounter >= $EachPOP3['EmailsProcessAtOneTime']) && ($EachPOP3['EmailsProcessAtOneTime'] != 0))
					{
					break;
					}
				// Only process allowed number of emails per execution - FINISHED

				$EmailIndex		=	$TMPCounter;

				// Retrieve the email - STARTED
				if ($TMPErrorMsg == '')
					{
					$TMPErrorMsg = $ObjectPOP3->RetrieveMessage($TMPCounter, $ArrayEmailHeaders, $ArrayEmailBody, -1);
						$EmailContent = implode("\n", $ArrayEmailHeaders)."\n\n".implode("\n", $ArrayEmailBody);
					
					foreach ($ArrayEmailHeaders as $EachHeader)
						{
						if (strtolower(substr($EachHeader, 0, 11)) == 'message-id:')
							{
							$EmailUniqueID = preg_replace('message-id:', '', $EachHeader);
							$EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
							$EmailUniqueID = str_replace('<', '', $EmailUniqueID);
							$EmailUniqueID = str_replace('>', '', $EmailUniqueID);

							break;
							}
						}
					}
				// Retrieve the email - FINISHED

				// Parse the email content - STARTED
				if ($TMPErrorMsg == '')
					{
					$ObjectEmailParser		= new OctethEmailParser();
					$OctethMIMEUtilities	= new OctethMIMEUtilities();

					$ParsedHeaders = $ObjectEmailParser->ParseHeaders($ArrayEmailHeaders);
					$ParsedBody    = $ObjectEmailParser->ParseBody($ArrayEmailBody);

					// Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
					$ParsedHeaders['Subject']		=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Subject']);
					$ParsedHeaders['To']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['To']);
					$ParsedHeaders['From']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['From']);
					$ParsedHeaders['Content-Type']	=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Content-Type']);
					// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
					}
				// Parse the email content - FINISHED

				// Parse the email address of the sender - STARTED
				if ($TMPErrorMsg == '')
					{
					$SenderEmail = GatherEmail($ParsedHeaders['From']);
					}
				// Parse the email address of the sender - FINISHED

				// Decide if the email is a bounce report or not, if yes, process it - STARTED
				if ($TMPErrorMsg == '')
					{
					$IsBounceEmail = AnalyzeBody($ParsedHeaders, $ParsedBody, implode("\n" , $ParsedBody), $EmailUniqueID, $EachPOP3, $ArrayConfig, $ArrayLanguage);
					
					if ($IsBounceEmail == true)
						{
						$TotalBounces++;
						}
					}
				// Decide if the email is a bounce report or not, if yes, process it - FINISHED

				// Delete the email after processing - STARTED
				if ($TMPErrorMsg == '')
					{
					$TMPErrorMsg = $ObjectPOP3->DeleteMessage($EmailIndex);
					}
				// Delete the email after processing - FINISHED
				}
			// Start processing each email - FINISHED
			}
		// Analyze each email located in the mail box - FINISHED

		// Close POP3 server connection - STARTED
		$ObjectPOP3->Close();
		// Close POP3 server connection - FINISHED
		}
	// Retrieve defined mail boxes for request processing - FINISHED
	
	return $TotalBounces;
	}

//########################################################################################
// Gather email address from the email header format (To, From, etc.)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GatherEmail($String)
	{
	$ReversedString = strrev($String);

		$Pattern = '/>?([^(?:>|<)]*)?<? ?"?([^"]*)/';
	preg_match_all($Pattern, $ReversedString, $ArrayMatches);

	return strrev($ArrayMatches[1][0]);
	}

//########################################################################################
// Analyze the body and decide the level of bounce
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function AnalyzeBody($ParsedHeaders, $ParsedBody, $fullContent, $EmailUniqueID, $ArrayPOP3, $ArrayConfig, $ArrayLanguage)
	{
	$TMPHeaders			= implode("\n", $ParsedHeaders);
	$TMPBody			= implode("\n", $ParsedBody);
	$TMPArrayBounce		= array();
	$IsBounceEmail		= false;

	// Try to detect if the email is a delivery report or not - STARTED
    preg_match_all("/X-Mailer-MsgId: (.*)\n/i", $TMPBody, $ArrayMatches);
    if ($ArrayMatches[1][0] != '')
        {
        $TMPEncryptedEmailAddress = trim($ArrayMatches[1][0]);
        $TMPEmail = trim(base64_decode($TMPEncryptedEmailAddress));
        }

    if ($ParsedHeaders['X-Mailer-MsgId'] != '')
        {
        $TMPEncryptedEmailAddress = trim($ParsedHeaders['X-Mailer-MsgId']);
        $TMPEmail = trim(base64_decode($TMPEncryptedEmailAddress));
        }

    if ($TMPEmail == '')
        {
        preg_match_all("/Final-Recipient: RFC822;(.*)\n/i", $TMPBody, $ArrayMatches);
        if ($ArrayMatches[1][0] != "")
            {
            $TMPEmail = $ArrayMatches[1][0];
            $TMPEmail = GatherEmail($TMPEmail);

            // Apply secondary pattern to message body if first pattern fails - STARTED
	        $ObjectUtility = new OctethUtilities();
			if (!$ObjectUtility->CheckEmailFormat($TMPEmail))
	        	{
		    	preg_match('[A-z,0-9]*@[A-z,0-9]*.[A-z,0-9]*', $TMPBody , $ArraySecondary);
		    	if (count($ArraySecondary) > 0)
			    	{
			    	$TMPEmail = trim($ArraySecondary[0]);
			    	}
            	}
	        // Apply secondary pattern to message body if first pattern fails - FINISHED
            }
        }

    if ($TMPEmail == '')
        {
        if (preg_match("/This message was created automatically by mail delivery software/i", $TMPBody) > 0)
            {
            if (preg_match("/To: (.*)/i", $TMPBody, $ArrayMatches) > 0)
                {
                if ( $ArrayMatches[1] != "")
                    {
                    $TMPEmail = $ArrayMatches[1];

                    // Apply secondary pattern to message body if first pattern fails - STARTED
	                $ObjectUtility = new OctethUtilities();
					if (!$ObjectUtility->CheckEmailFormat($TMPEmail))
	                	{
		    	       	preg_match('[A-z,0-9]*@[A-z,0-9]*.[A-z,0-9]*/i', $TMPBody , $ArraySecondary);
		    	       	if (count($ArraySecondary) > 0)
			    	       	{
			    	       	$TMPEmail = trim($ArraySecondary[0]);
			    	       	}
	                	}
	                }
	                // Apply secondary pattern to message body if first pattern fails - FINISHED
                }
            }
        }

    if ($TMPEmail == '')
        {
        if (preg_match("/-- The following addresses had permanent fatal errors --.*\n(.*)/is", $TMPBody, $ArrayMatches) == true)
            {
            if ( $ArrayMatches[1] != "")
                {
                $TMPEmail = $ArrayMatches[1];

                // Apply secondary pattern to message body if first pattern fails - STARTED
                $ObjectUtility = new OctethUtilities();
				if (!$ObjectUtility->CheckEmailFormat($TMPEmail))
                	{
	    	       	preg_match('[A-z,0-9]*@[A-z,0-9]*.[A-z,0-9]*'/i, $TMPBody , $ArraySecondary);
	    	       	if (count($ArraySecondary) > 0)
		    	       	{
		    	       	$TMPEmail = trim($ArraySecondary[0]);
		    	       	}
                	}
                // Apply secondary pattern to message body if first pattern fails - FINISHED
                }
            }
        }

    if ($TMPEmail == '')
        {
        if (preg_match_all("/Below this line is a copy of the message.*\nTo:(.*)\n.*/is", $TMPBody, $ArrayMatches) == true)
            {
            if ( $ArrayMatches[1][0] != "")
                {
                $TMPEmail = $ArrayMatches[1][0];
                $TMPEmail = explode("\n", $TMPEmail);
                $TMPEmail = trim($TMPEmail[0]);

                // Apply secondary pattern to message body if first pattern fails - STARTED
                $ObjectUtility = new OctethUtilities();
				if (!$ObjectUtility->CheckEmailFormat($TMPEmail))
                	{
	    	       	preg_match('[A-z,0-9]*@[A-z,0-9]*.[A-z,0-9]*/i', $TMPBody , $ArraySecondary);
	    	       	if (count($ArraySecondary) > 0)
		    	       	{
		    	       	$TMPEmail = trim($ArraySecondary[0]);
		    	       	}
                	}
                // Apply secondary pattern to message body if first pattern fails - FINISHED
                }
            }
        }

	if ($TMPEmail == '')
		{
		if (preg_match_all("/This is a permament error; I've given up. Sorry it didn't work out.*\n\n\<(.*)\>:\n.*/is", $TMPBody, $ArrayMatches) == true)
			{
			if ($ArrayMatches[1][0] != "")
				{
                $TMPEmail = $ArrayMatches[1][0];
                $TMPEmail = explode("\n", $TMPEmail);
                $TMPEmail = trim($TMPEmail[0]);
				}
			}
		}

    if ($TMPEmail == '')
        {
	    preg_match('[A-z,0-9]*@[A-z,0-9]*.[A-z,0-9]*'/i, $TMPBody , $ArraySecondary);
	    if (count($ArraySecondary) > 0)
			{
		    $TMPEmail = trim($ArraySecondary[0]);
		    }
        }
    // Try to detect if the email is a delivery report or not - FINISHED
    
    // Retrieve campaign ID and campaign statistics ID from the header - STARTED
	$ArrayMatches = array();
	if ((preg_match_all("/X-Mailer-CSID: (.*)\n/i", $fullContent, $ArrayMatches) == true) || ($ParsedHeaders['X-Mailer-CSID'] != ''))
		{
		if ($ArrayMatches[1][0] != '')
			{
			$PatternMatched = trim($ArrayMatches[1][0]);
			}
		else
			{
			$PatternMatched = trim($ParsedHeaders['X-Mailer-CSID']);
			}
		$TMPEncryptedCSID = $PatternMatched;
		$TMPCSID = base64_decode($TMPEncryptedCSID);
			$TMPCampaignID				= explode("_", $TMPCSID);
			$TMPCampaignStatisticsID	= $TMPCampaignID[1];
			$TMPCampaignID				= $TMPCampaignID[0];

		// Record the bounce activity to campaign statistics - STARTED
		if ($ErrorExists == false)
			{
			if (($TMPCampaignID > 0) && ($TMPCampaignStatisticsID > 0))
				{
				include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/campaigns.inc.php");
				RecordCampaignBounceActivity($TMPCampaignID, $TMPCampaignStatisticsID, $ArrayConfig);
				}
			}
		// Record the bounce activity to campaign statistics - FINISHED
		}
	// Retrieve campaign ID and campaign statistics ID from the header - FINISHED

	if ($TMPEmail != '')
		{
		// Parse the RFC bounce level code - STARTED
		preg_match_all("/Action: (.*)\n/i", $fullContent, $ArrayMatches);
			$TMPAction = trim($ArrayMatches[1][0]);
	
		preg_match_all("/Status: (.*)\n/i", $fullContent, $ArrayMatches);
			$TMPStatus = trim($ArrayMatches[1][0]);
	
		preg_match_all("/Diagnostic-Code: (.*)\n/i", $fullContent, $ArrayMatches);
			$TMPDiagnosticCode = trim($ArrayMatches[1][0]);
		// Parse the RFC bounce level code - FINISHED

		// check for another options if there is no standart Action or Status parameter in message - STARTED
		if (($TMPAction == '') && ($TMPStatus == ''))
			{
			preg_match_all("(address no longer accepts mail)", $fullContent, $ArrayMatches);
			if (count($ArrayMatches))
				{
				$TMPStatus = '5.2.1';
				$TMPAction = 'no longer accepts mail';
				}
			}

		if (($TMPAction == '') && ($TMPStatus == ''))
			{
			preg_match_all("(\d.\d.\d No such user)", $fullContent, $ArrayMatches);
			if (count($ArrayMatches))
				{
				$TMPStatus = '5.2.1';
				$TMPAction = 'no such user';
				}
			}

		if (($TMPAction == '') && ($TMPStatus == ''))
			{
			preg_match_all("(\d\d\d delivery error)", $fullContent, $ArrayMatches);
			if (count($ArrayMatches))
				{
				$TMPStatus = '5.2.1';
				$TMPAction = 'no such user';
				}
			}
		// check for another options if there is no standart Action or Status parameter in message - FINISHED

        $TMPEmail = trim($TMPEmail);
		// Check if this email address is stored in oemPro database or not - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$TMPEmail."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalMemberFound = mysql_num_rows($ResultSet);
		if ($TotalMemberFound > 0)
		{
			$ArrayMember = mysql_fetch_assoc($ResultSet);
		}
		else
		{
			$ArrayMember = false;
		}
		// Check if this email address is stored in oemPro database or not - FINISHED
		// Do not continue if member account is not found in the Oempro database. Do not track bounce reports which do not belong to Oempro subscribers - Start
		if (is_array($ArrayMember) == false)
			{
			return false;
			}
		// Do not continue if member account is not found in the Oempro database. Do not track bounce reports which do not belong to Oempro subscribers - End

		// Check if this email address has been detected before or not - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalDetections FROM ".$ArrayConfig['Database']['Prefix']."bounce_log WHERE EmailAddress='".$TMPEmail."' AND RelMemberID='".($TotalMemberFound == 0 ? 0 : $ArrayMember['MemberID'])."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalDetectionsFound = mysql_fetch_assoc($ResultSet);
			$TotalDetectionsFound = $TotalDetectionsFound['TotalDetections'];
		// Check if this email address has been detected before or not - FINISHED

		// Decide the bounce type - STARTED
		$BounceTypeID = DecideBounceType($TMPEmail, $TMPAction, $TMPStatus, $TMPDiagnosticCode, $TMPHeaders, $TMPBody, $ArrayConfig, $ArrayLanguage);
	
		if (($BounceTypeID == '') || ($BounceTypeID == 0))
			{
			$BounceTypeID = 2;
			}
		// Decide the bounce type - FINISHED

		$TMPArrayBounce["Email"] 			= $TMPEmail;
		$TMPArrayBounce["MemberID"]			= ($TotalMemberFound == 0 ? 0 : $ArrayMember['MemberID']);
		$TMPArrayBounce["BounceTypeID"]		= ($BounceTypeID == 0 ? 2 : $BounceTypeID);
		$TMPArrayBounce["MessageSubject"]	= $ParsedHeaders['Subject'];
		$TMPArrayBounce["ActionDate"]		= date("Y-m-d H:i:s");
		$TMPArrayBounce["Action"] 			= $TMPAction;
		$TMPArrayBounce["Status"] 			= $TMPStatus;
		$TMPArrayBounce["DiagnosticCode"]	= $TMPDiagnosticCode;

		// Do not process already processed bounced emails - STARTED
//		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."bounce_log WHERE MessageID='".$EmailUniqueID."' AND EmailAddress='".$TMPArrayBounce["Email"]."' AND RelMemberID='".$TMPArrayBounce["MemberID"]."'";
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."bounce_log WHERE EmailAddress='".$TMPArrayBounce["Email"]."' AND RelMemberID='".$TMPArrayBounce["MemberID"]."' AND RelCampaignID='".($TMPCampaignID == '' ? 0 : $TMPCampaignID)."' AND RelCampaignStatisticsID='".($TMPCampaignStatisticsID == '' ? 0 : $TMPCampaignStatisticsID)."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$SameDetectionsBefore = mysql_fetch_assoc($ResultSet);
			$SameDetectionsBefore = $SameDetectionsBefore['TotalFound'];

			// Fix - Start
			$SameDetectionsBefore = 0; // Disable same detection check 2008-03-15 by Cem
			// Fix - End

		if ($SameDetectionsBefore > 0)
			{
			return false;
			}			
		// Do not process already processed bounced emails - FINISHED
		
		// Apply detection thresholds - STARTED
		if (($TotalMemberFound > 0) && ($ArrayMember['BounceType'] == 'Soft bounce') && ($ArrayPOP3['DetectionAmountForHardMarking'] <= $TotalDetectionsFound + 1))
			{
			$NewBounceType = "Hard bounce";

			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."members SET BounceType='".$NewBounceType."' WHERE MemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		elseif (($TotalMemberFound > 0) && ($ArrayMember['BounceType'] == 'Hard bounce') && ($ArrayPOP3['DetectionAmountForPurging'] <= $TotalDetectionsFound + 1))
			{
			include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/delete.inc.php");
			DeleteMember($ArrayMember['MemberID'], $ArrayConfig);
			}
		elseif ($TotalMemberFound > 0)
			{
			if ($BounceTypeID == 1)
				{
				$NewBounceType = "Hard bounce";
				}
			elseif (($BounceTypeID == 2) || ($BounceTypeID == 3) || ($BounceTypeID == 4) || ($BounceTypeID == 5))
				{
				$NewBounceType = "Soft bounce";
				}
			else
				{
				$NewBounceType = "Not bounced";
				}
	
			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."members SET BounceType='".$NewBounceType."' WHERE MemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		// Apply detection thresholds - FINISHED

		// Record into bounce log - STARTED
		$IsBounceEmail = true;

		$ArrayFieldnValues = array(
								"BounceID"				=>	'',
								"EmailAddress"			=>	$TMPArrayBounce["Email"],
								"RelMemberID"			=>	$TMPArrayBounce["MemberID"],
								"RelBounceTypeID"		=>	$TMPArrayBounce["BounceTypeID"],
								"RelCampaignID"			=>	($TMPCampaignID == '' ? 0 : $TMPCampaignID),
								"RelCampaignStatisticsID"=>	($TMPCampaignStatisticsID == '' ? 0 : $TMPCampaignStatisticsID),
								"MessageSubject"		=>	mysql_escape_string($TMPArrayBounce["MessageSubject"]),
								"MessageID"				=>	$EmailUniqueID,
								"ActionDate"			=>	date("Y-m-d H:i:s"),
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."bounce_log (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Record into bounce log - FINISHED
		}

	return $IsBounceEmail;	
	}

//########################################################################################
// Decide the bounce type of the member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DecideBounceType($Email, $Action, $Status, $DiagnosticCode, $Headers, $Body, $ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ArrayStatusCode = array();
	// Initialize variables - FINISHED

	// Parse the status code - STARTED
	if (strlen($Status) == 3)
		{
		$ArrayStatusCode[0] = substr($Status, 0, 1);
		$ArrayStatusCode[1] = substr($Status, 1, 1);
		$ArrayStatusCode[2] = substr($Status, 2, 1);
		}
	elseif (strlen($Status) == 5)
		{
		$ArrayStatusCode[0] = substr($Status, 0, 1);
		$ArrayStatusCode[1] = substr($Status, 2, 1);
		$ArrayStatusCode[2] = substr($Status, 4, 1);
		}
	else
		{
		$ArrayStatusCode[0] = substr($Status, 0, 1);
		$ArrayStatusCode[1] = substr($Status, 2, 1);
		$ArrayStatusCode[2] = substr($Status, 4, 1);
		}
	// Parse the status code - FINISHED

	// Status code - STARTED
	$StatusCode = $ArrayStatusCode[0].".".$ArrayStatusCode[1].".".$ArrayStatusCode[2];
	// Status code - FINISHED
	
	// Define the bounce type - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."bounce_codes WHERE `RFCCode`='".$StatusCode."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	if (mysql_num_rows($ResultSet) < 1)
		{
		$BounceTypeID = 2;
		}
	else
		{
		$ArrayBounceCode = mysql_fetch_assoc($ResultSet);

		if ($ArrayBounceCode['BounceType'] == 'Soft Bounce')
			{
			$BounceTypeID = 2;
			}
		elseif ($ArrayBounceCode['BounceType'] == 'Hard Bounce')
			{
			$BounceTypeID = 1;
			}
		else
			{
			$BounceTypeID = 2;
			}
		}
	// Define the bounce type - FINISHED

	return $BounceTypeID;
	}
function ProcessBouncedEmailsArf($ArrayConfig, $ArrayLanguage)
{
	// Include the required modules - STARTED
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_pop3.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethemailparser.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethmimeutilities.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_mimedecode.php");
    include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/ban_functions.php");
    include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/delete.inc.php");
	// Include the required modules - FINISHED
    
	// Create a connection to POP3 server - STARTED
	$ObjectPOP3					= new pop3_class;
	$ObjectPOP3->hostname		= $ArrayConfig["ServerHost"];
	$ObjectPOP3->port			= $ArrayConfig["ServerPort"];

    $TMPErrorMsg = $ObjectPOP3->Open();
    // Create a connection to POP3 server - FINISHED
    // Login to POP3 server - STARTED
    if ($TMPErrorMsg == '')
	{
	   $TMPErrorMsg = $ObjectPOP3->Login($ArrayConfig["ServerUsername"], $ArrayConfig["ServerPassword"], ($ArrayConfig["ServerAPOP"] != 'No' ? 1 : 0));
	}
	// Login to POP3 server - FINISHED
    
	// Retrieve mail box statistics - STARTED
	if ($TMPErrorMsg == '')
	{
	   
	   $TMPErrorMsg = $ObjectPOP3->Statistics($TotalMessages, $TotalSizeBytes);
	}

	// Retrieve mail box statistics - FINISHED
    // Analyze each email located in the mail box - STARTED
	if (($TMPErrorMsg == '') && ($TotalMessages > 0))
    {
        $sql = ExecuteMySQLQuery("SELECT `date` FROM `oemp_cron_arf`", __LINE__, __FILE__);
        $lastDate = mysql_fetch_row($sql);
        
        // Start processing each email - STARTED
        for ($TMPCounter = 1; $TMPCounter <= $TotalMessages; $TMPCounter++)
        {
            $EmailIndex		=	$TMPCounter;
            // Retrieve the email - STARTED
            if ($TMPErrorMsg == '')
            {
                $TMPErrorMsg = $ObjectPOP3->RetrieveMessage($TMPCounter, $ArrayEmailHeaders, $ArrayEmailBody, -1);
                $EmailContent = implode("\n", $ArrayEmailHeaders)."\n\n".implode("\n", $ArrayEmailBody);
                
                foreach ($ArrayEmailHeaders as $EachHeader)
                {
                    if (strtolower(substr($EachHeader, 0, 11)) == 'message-id:')
                    {
                        $EmailUniqueID = preg_replace('/message-id:/i', '', $EachHeader);
                        $EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('<', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('>', '', $EmailUniqueID);
                        break;
                    }
                }
            }
			// Retrieve the email - FINISHED
            
            // Parse the email content - STARTED
            if ($TMPErrorMsg == '')
            {
                $ObjectEmailParser		= new OctethEmailParser();
                $OctethMIMEUtilities	= new OctethMIMEUtilities();
                
                $ParsedHeaders = $ObjectEmailParser->ParseHeaders($ArrayEmailHeaders);
                $ParsedBody    = $ObjectEmailParser->ParseBody($ArrayEmailBody);
                
                // Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
				$ParsedHeaders['Subject']		=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Subject']);
                $ParsedHeaders['To']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['To']);
                $ParsedHeaders['From']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['From']);
                $ParsedHeaders['Content-Type']	=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Content-Type']);
				// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
            }
			// Parse the email content - FINISHED

			// Parse the email address of the sender - STARTED
			if ($TMPErrorMsg == '')
			{
				$SenderEmail = GatherEmail($ParsedHeaders['From']);
			}
			// Parse the email address of the sender - FINISHED
            
			// Decide if the email is a bounce report or not, if yes, process it - STARTED
			if ($TMPErrorMsg == '')
			{
                //$IsBounceEmail = AnalyzeBody($ParsedHeaders, $ParsedBody, implode("\n" , $ParsedBody), $EmailUniqueID, $EachPOP3, $ArrayConfig, $ArrayLanguage);                 
                if (!empty($ParsedBody))
                {           
                    $date = strtotime($ParsedHeaders['Date']);
                    $dbDate = strtotime($lastDate[0]);
                    
                    if ($date > $dbDate)
                    {
                        foreach ($ParsedBody as $value)
                        {
                            if (strpos($value, "Original-Rcpt-To:") !== FALSE)
                            {                                
                                $email = substr($value, strlen("Original-Rcpt-To: "));
                                
                                $sql = ExecuteMySQLQuery("SELECT `MemberID` FROM `oemp_members` WHERE `Email` = '{$email}'", __LINE__, __FILE__);
                                $data = mysql_fetch_row($sql);                                
                                $memberid = $data[0];
                                                                
            					$SQLQuery = "SELECT COUNT(*) AS TotalCount, RelMailListID FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$email."'";
            					$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                                
            					while ($row = mysql_fetch_assoc($ResultSet3))
                                {
                                    if ($row['TotalCount'] == 0)
            						{
            							$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (RelMailListID,Email,DateSubmitted) VALUES (1113,'".$email."',NOW());";
            							ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
            						}
                                    else
                                    {
                                        if ($row['RelMailListID'] == 0)
                                        {
                                            $SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."maillist_blacklist SET RelMailListID = 1113, `DateSubmitted` = NOW() WHERE `Email` = '{$email}';";
                                            ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                                        }
                                    }   
                                }            					
                				//Insert into blacklist - FINISHED
                
                				// Delete the selected records as well as related records - STARTED
                				DeleteMember($memberid, $ArrayConfig);  
                                $ObjectPOP3->DeleteMessage($TMPCounter);                              
                            }
                        }    
                    }
                    
                }
                if ($IsBounceEmail == true)
                {
                    $TotalBounces++;
                }
			}
		}
        
        //ExecuteMySQLQuery("UPDATE oemp_cron_arf set `date` = '".date("Y-m-d H:i:s", $date)."'", __LINE__, __FILE__);
	}
	// Close POP3 server connection - STARTED
	$ObjectPOP3->Close();
	// Close POP3 server connection - FINISHED
		
	// Retrieve defined mail boxes for request processing - FINISHED
	
	return $TotalBounces;
}

function ProcessBouncedEmailsJmrp($ArrayConfig, $ArrayLanguage)
{
	// Include the required modules - STARTED
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_pop3.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethemailparser.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethmimeutilities.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_mimedecode.php");
    include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/delete.inc.php");
	// Include the required modules - FINISHED
    
	// Create a connection to POP3 server - STARTED
	$ObjectPOP3					= new pop3_class;
	$ObjectPOP3->hostname		= $ArrayConfig["ServerHost"];
	$ObjectPOP3->port			= $ArrayConfig["ServerPort"];

    $TMPErrorMsg = $ObjectPOP3->Open();
    // Create a connection to POP3 server - FINISHED
    // Login to POP3 server - STARTED
    if ($TMPErrorMsg == '')
	{
	   $TMPErrorMsg = $ObjectPOP3->Login($ArrayConfig["ServerUsername"], $ArrayConfig["ServerPassword"], ($ArrayConfig["ServerAPOP"] != 'No' ? 1 : 0));
	}
	// Login to POP3 server - FINISHED
    
	// Retrieve mail box statistics - STARTED
	if ($TMPErrorMsg == '')
	{
	   
	   $TMPErrorMsg = $ObjectPOP3->Statistics($TotalMessages, $TotalSizeBytes);
	}
    $TotalBounces = 0;
	// Retrieve mail box statistics - FINISHED
    // Analyze each email located in the mail box - STARTED
	if (($TMPErrorMsg == '') && ($TotalMessages > 0))
    {
        $sql = ExecuteMySQLQuery("SELECT `date` FROM `oemp_cron_jmrp`", __LINE__, __FILE__);
        $lastDate = mysql_fetch_row($sql);
        
        // Start processing each email - STARTED
        for ($TMPCounter = 1; $TMPCounter <= $TotalMessages; $TMPCounter++)
        {
            $EmailIndex		=	$TMPCounter;
            // Retrieve the email - STARTED
            if ($TMPErrorMsg == '')
            {
                $TMPErrorMsg = $ObjectPOP3->RetrieveMessage($TMPCounter, $ArrayEmailHeaders, $ArrayEmailBody, -1);                
                $EmailContent = implode("\n", $ArrayEmailHeaders)."\n\n".implode("\n", $ArrayEmailBody);
                
                foreach ($ArrayEmailHeaders as $EachHeader)
                {
                    if (strtolower(substr($EachHeader, 0, 11)) == 'message-id:')
                    {
                        $EmailUniqueID = preg_replace('/message-id:/i', '', $EachHeader);
                        $EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('<', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('>', '', $EmailUniqueID);
                        break;
                    }
                }
            }
			// Retrieve the email - FINISHED
            
            // Parse the email content - STARTED
            if ($TMPErrorMsg == '')
            {
                $ObjectEmailParser		= new OctethEmailParser();
                $OctethMIMEUtilities	= new OctethMIMEUtilities();
                
                $ParsedHeaders = $ObjectEmailParser->ParseHeaders($ArrayEmailHeaders);
                $ParsedBody    = $ObjectEmailParser->ParseBody($ArrayEmailBody);
                
                // Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
				$ParsedHeaders['Subject']		=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Subject']);
                $ParsedHeaders['To']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['To']);
                $ParsedHeaders['From']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['From']);
                $ParsedHeaders['Content-Type']	=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Content-Type']);
				// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
            }
			// Parse the email content - FINISHED

			// Parse the email address of the sender - STARTED
			if ($TMPErrorMsg == '')
			{
				$SenderEmail = GatherEmail($ParsedHeaders['From']);
			}
			// Parse the email address of the sender - FINISHED
            
			// Decide if the email is a bounce report or not, if yes, process it - STARTED
			if ($TMPErrorMsg == '')
			{                
                if (!empty($ParsedHeaders))
                {       
                    $email = $ParsedHeaders['To'];
                    
                    $sql = ExecuteMySQLQuery("SELECT `MemberID` FROM `oemp_members` WHERE `Email` = '{$email}'", __LINE__, __FILE__);
                    $data = mysql_fetch_row($sql);                                
                    $memberid = $data[0];
                    
                    $SQLQuery = "SELECT COUNT(*) AS TotalCount, RelMailListID FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$email."'";
					$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                    
					while ($row = mysql_fetch_assoc($ResultSet3))
                    {
                        if ($row['TotalCount'] == 0)
						{
							$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (RelMailListID,Email,DateSubmitted) VALUES (1112,'".$email."',NOW());";
							ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
						}
                        else
                        {
                            if ($row['RelMailListID'] == 0)
                            {
                                $SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."maillist_blacklist SET RelMailListID = 1112, `DateSubmitted` = NOW() WHERE `Email` = '{$email}';";
                                ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                            }
                        }   
                    }
                                
                                
                    DeleteMember($memberid, $ArrayConfig);  
                    $ObjectPOP3->DeleteMessage($TMPCounter);
                    
                    $TotalBounces++;          
                }
			}
		}
        
        //ExecuteMySQLQuery("UPDATE oemp_cron_arf set `date` = '".date("Y-m-d H:i:s", $date)."'", __LINE__, __FILE__);
	}
	// Close POP3 server connection - STARTED
	$ObjectPOP3->Close();
	// Close POP3 server connection - FINISHED
		
	// Retrieve defined mail boxes for request processing - FINISHED
	
	return $TotalBounces;
}

function ProcessBouncedEmailsFbl($ArrayConfig, $ArrayLanguage)
{
	// Include the required modules - STARTED
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_pop3.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethemailparser.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_octethmimeutilities.php");
	include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//classes/class_mimedecode.php");
    include_once(ABSOLUTE_PATH."/libray/WebApp/Mail/Oempro//libraries/delete.inc.php");
	// Include the required modules - FINISHED
    
	// Create a connection to POP3 server - STARTED
	$ObjectPOP3					= new pop3_class;
	$ObjectPOP3->hostname		= $ArrayConfig["ServerHost"];
	$ObjectPOP3->port			= $ArrayConfig["ServerPort"];

    $TMPErrorMsg = $ObjectPOP3->Open();
    // Create a connection to POP3 server - FINISHED
    // Login to POP3 server - STARTED
    if ($TMPErrorMsg == '')
	{
	   $TMPErrorMsg = $ObjectPOP3->Login($ArrayConfig["ServerUsername"], $ArrayConfig["ServerPassword"], ($ArrayConfig["ServerAPOP"] != 'No' ? 1 : 0));
	}
	// Login to POP3 server - FINISHED
    
	// Retrieve mail box statistics - STARTED
	if ($TMPErrorMsg == '')
	{
	   
	   $TMPErrorMsg = $ObjectPOP3->Statistics($TotalMessages, $TotalSizeBytes);
	}
    $TotalBounces = 0;
	// Retrieve mail box statistics - FINISHED
    // Analyze each email located in the mail box - STARTED
	if (($TMPErrorMsg == '') && ($TotalMessages > 0))
    {
        $sql = ExecuteMySQLQuery("SELECT `date` FROM `oemp_cron_jmrp`", __LINE__, __FILE__);
        $lastDate = mysql_fetch_row($sql);
        
        // Start processing each email - STARTED
        for ($TMPCounter = 1; $TMPCounter <= $TotalMessages; $TMPCounter++)
        {
            $EmailIndex		=	$TMPCounter;
            // Retrieve the email - STARTED
            if ($TMPErrorMsg == '')
            {
                $TMPErrorMsg = $ObjectPOP3->RetrieveMessage($TMPCounter, $ArrayEmailHeaders, $ArrayEmailBody, -1);                
                $EmailContent = implode("\n", $ArrayEmailHeaders)."\n\n".implode("\n", $ArrayEmailBody);
                
                foreach ($ArrayEmailHeaders as $EachHeader)
                {
                    if (strtolower(substr($EachHeader, 0, 11)) == 'message-id:')
                    {
                        $EmailUniqueID = preg_replace('/message-id:/i', '', $EachHeader);
                        $EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('<', '', $EmailUniqueID);
                        $EmailUniqueID = str_replace('>', '', $EmailUniqueID);
                        break;
                    }
                }
            }
			// Retrieve the email - FINISHED
            
            // Parse the email content - STARTED
            if ($TMPErrorMsg == '')
            {
                $ObjectEmailParser		= new OctethEmailParser();
                $OctethMIMEUtilities	= new OctethMIMEUtilities();
                
                $ParsedHeaders = $ObjectEmailParser->ParseHeaders($ArrayEmailHeaders);
                //$ParsedBody    = $ObjectEmailParser->ParseBody($ArrayEmailBody);
                $body  = implode("", $ArrayEmailBody);
                
                if (strpos($body, "unsubscribe.php") !== false)
                {
                    $part = substr($body, strpos($body, "&Email=3D=")+10);
                    $part2 = substr($part, 0, strpos($part, "=3D"));
                    $email = base64_decode($part2);                    
                }
                //echo "<pre>";
                //print_r($ArrayEmailBody);
                if (!isset($email))
                {
                    $TMPErrorMsg = 'nu s-a luat emailul corect';
                    $email = null;
                }
            }
			// Parse the email content - FINISHED

			// Decide if the email is a bounce report or not, if yes, process it - STARTED
			if ($TMPErrorMsg == '')
			{                
                if (isset($email))
                {       
                    $sql = ExecuteMySQLQuery("SELECT `MemberID` FROM `oemp_members` WHERE `Email` = '{$email}'", __LINE__, __FILE__);

                    $data = mysql_fetch_row($sql);                                
                    $memberid = $data[0];
                    
                    $SQLQuery = "SELECT COUNT(*) AS TotalCount, RelMailListID FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$email."'";
					$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                    
					while ($row = mysql_fetch_assoc($ResultSet3))
                    {
                        if ($row['TotalCount'] == 0)
						{
							$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (RelMailListID,Email,DateSubmitted) VALUES (1114,'".$email."',NOW());";
							ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
						}
                        else
                        {
                            if ($row['RelMailListID'] == 0)
                            {
                                $SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."maillist_blacklist SET RelMailListID = 1114, `DateSubmitted` = NOW() WHERE `Email` = '{$email}';";
                                ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
                            }
                        }   
                    }                                                                
                    DeleteMember($memberid, $ArrayConfig);  
                    $ObjectPOP3->DeleteMessage($TMPCounter);
                    
                    $TotalBounces++;          
                }
			}
		}
        
        //ExecuteMySQLQuery("UPDATE oemp_cron_arf set `date` = '".date("Y-m-d H:i:s", $date)."'", __LINE__, __FILE__);
	}
	// Close POP3 server connection - STARTED
	$ObjectPOP3->Close();
	// Close POP3 server connection - FINISHED
		
	// Retrieve defined mail boxes for request processing - FINISHED
	
	return $TotalBounces;
}
?>