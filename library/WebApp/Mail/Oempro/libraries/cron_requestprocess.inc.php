<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Connect to defined POP3 servers and process all pending requests
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessPendingRequestsIMAP($ArrayConfig, $ArrayLanguage)
	{
	// Retrieve defined mail boxes for request processing - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE UseFor LIKE '%Request Processing%'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	// Include the required modules - STARTED
	include_once("./system/classes/class_pop3.php");
	include_once("./system/classes/class_octethemailparser.php");
	include_once("./system/classes/class_octethmimeutilities.php");
	include_once("./system/classes/class_mimedecode.php");
	// Include the required modules - FINISHED

	$TotalProcessedRequests = 0;
	
	while ($EachPOP3 = mysql_fetch_assoc($ResultSet))
		{
		// Prepare the commands of this POP3 server - STARTED
		$SubscriptionCommands		= explode(",", $EachPOP3['CommandsSubscription']);
		$UnSubscriptionCommands		= explode(",", $EachPOP3['CommandsUnsubscription']);
		$PasswordCommands			= explode(",", $EachPOP3['CommandsPassword']);
		
		$ArrayCommands = array();
		foreach ($SubscriptionCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessSubscribeToMailLists';
			}
		foreach ($UnSubscriptionCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessUnsubscribeFromMailLists';
			}
		foreach ($PasswordCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessRemindLoginInformation';
			}
		// Prepare the commands of this POP3 server - FINISHED

		// Create a connection to POP3 server and login - STARTED
		$ConnectionMail = imap_open("{".$EachPOP3["ServerHost"].":".$EachPOP3["ServerPort"]."/pop3/notls}", $EachPOP3["ServerUsername"], $EachPOP3["ServerPassword"]);
		// Create a connection to POP3 server and login- FINISHED

		// Retrieve headers - STARTED
		$TotalEmails  = imap_num_msg($ConnectionMail);
		// Retrieve headers - FINISHED

		// Analyze each email located in the mail box - STARTED
		for ($TMPCounter=1; $TMPCounter<=$TotalEmails; $TMPCounter++)
			{
			// Only process allowed number of emails per execution - STARTED
			if (($TotalProcessedRequests >= $EachPOP3['EmailsProcessAtOneTime']) && ($EachPOP3['EmailsProcessAtOneTime'] != 0))
				{
				break;
				}
			// Only process allowed number of emails per execution - FINISHED

			// Avoid time-out - STARTED
			print(' ');
			// Avoid time-out - FINISHED

			// Fetch header and body - STARTED
			$ObjectEmailHeader	= imap_header($ConnectionMail, $TMPCounter);
			$EmailHeader		= imap_fetchheader($ConnectionMail, $TMPCounter);
			$EmailBody			= imap_fetchbody($ConnectionMail, $TMPCounter, "1.1");
				if ($EmailBody == "")
					{
					$EmailBody = imap_fetchbody($ConnectionMail, $TMPCounter, "1");
					}
				$EmailBody = trim(strip_tags($EmailBody)); 
			// Fetch header and body - FINISHED
			// Retrieve the message ID - STARTED
			$EmailUniqueID = $ObjectEmailHeader->message_id;
				$EmailUniqueID = str_replace(' ', '', $EmailUniqueID);
				$EmailUniqueID = str_replace('<', '', $EmailUniqueID);
				$EmailUniqueID = str_replace('>', '', $EmailUniqueID);
			// Retrieve the message ID - FINISHED

			// Parse the email content - STARTED
			$ArrayParsedHeaders = array();
			
				// Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
				$ArrayParsedHeaders['Subject']		=		strip_tags($ObjectEmailHeader->subject);
				$ArrayParsedHeaders['To']			=		$ObjectEmailHeader->to[0]->mailbox."@".$ObjectEmailHeader->to[0]->host;
				$ArrayParsedHeaders['From']			=		$ObjectEmailHeader->from[0]->mailbox."@".$ObjectEmailHeader->from[0]->host;
				// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
			// Parse the email content - FINISHED

			// Parse the email address of the sender - STARTED
			$SenderEmail = $ArrayParsedHeaders['From'];
			// Parse the email address of the sender - FINISHED

			// Prepare header and body - STARTED
			$ArrayEmailHeader	= explode("\n", $EmailHeader);
			$ArrayEmailBody 	= explode("\n", $EmailBody);
			// Prepare header and body - FINISHED
			
			// Check if this email has been sent to correct email address or not - STARTED
			$Pattern = $EachPOP3['EmailAddress'];
			
			if (eregi($Pattern, $ArrayParsedHeaders['To']) == false)
				{
				continue;
				}
			
			$TotalProcessedRequests++;
			// Check if this email has been sent to correct email address or not - FINISHED

			// Detect the request type and process - STARTED
			$Subject	=	strtolower($ArrayParsedHeaders['Subject']);
			
			if ($ArrayCommands[$Subject] != '')
				{
				call_user_func($ArrayCommands[$Subject], $ArrayConfig, $ArrayLanguage, $SenderEmail, $Subject, $EachPOP3);		
				}
			else
				{
				if ($EachPOP3['SendErrorEmail'] == 'Enabled')
					{
					SendIncorrectUsageEmail($SenderEmail, $Subject, $ArrayConfig, $ArrayLanguage);
					}
				}
			// Detect the request type and process - FINISHED

			// Delete the email after processing - STARTED
			imap_delete($ConnectionMail, $TMPCounter);
			// Delete the email after processing - FINISHED
			}
		// Analyze each email located in the mail box - FINISHED

		// Close IMAP connection - STARTED
		imap_expunge($ConnectionMail);
		imap_close($ConnectionMail);
		// Close IMAP connection - FINISHED
		}
	// Retrieve defined mail boxes for request processing - FINISHED

	return $TotalProcessedRequests;
	}

//########################################################################################
// Connect to defined POP3 servers and process all pending requests
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessPendingRequests($ArrayConfig, $ArrayLanguage)
	{
	// Retrieve defined mail boxes for request processing - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE UseFor LIKE '%Request Processing%'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	// Include the required modules - STARTED
	include_once("./system/classes/class_pop3.php");
	include_once("./system/classes/class_octethemailparser.php");
	include_once("./system/classes/class_octethmimeutilities.php");
	include_once("./system/classes/class_mimedecode.php");
	// Include the required modules - FINISHED

	$TotalProcessedRequests = 0;
	
	while ($EachPOP3 = mysql_fetch_assoc($ResultSet))
		{
		// Prepare the commands of this POP3 server - STARTED
		$SubscriptionCommands		= explode(",", $EachPOP3['CommandsSubscription']);
		$UnSubscriptionCommands		= explode(",", $EachPOP3['CommandsUnsubscription']);
		$PasswordCommands			= explode(",", $EachPOP3['CommandsPassword']);
		
		$ArrayCommands = array();
		foreach ($SubscriptionCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessSubscribeToMailLists';
			}
		foreach ($UnSubscriptionCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessUnsubscribeFromMailLists';
			}
		foreach ($PasswordCommands as $EachCommand)
			{
			$ArrayCommands[$EachCommand] = 'ProcessRemindLoginInformation';
			}
		// Prepare the commands of this POP3 server - FINISHED


		// Create a connection to POP3 server - STARTED
		$ObjectPOP3					= new pop3_class;
		$ObjectPOP3->hostname		= $EachPOP3["ServerHost"];
		$ObjectPOP3->port			= $EachPOP3["ServerPort"];

		$TMPErrorMsg = $ObjectPOP3->Open();
		// Create a connection to POP3 server - FINISHED

		// Login to POP3 server - STARTED
		if ($TMPErrorMsg == '')
			{
			$TMPErrorMsg = $ObjectPOP3->Login($EachPOP3["ServerUsername"], $EachPOP3["ServerPassword"], ($EachPOP3["ServerAPOP"] != 'No' ? 1 : 0));
			}
		// Login to POP3 server - FINISHED

		// Retrieve mail box statistics - STARTED
		if ($TMPErrorMsg == '')
			{
			$TMPErrorMsg = $ObjectPOP3->Statistics(&$TotalMessages, &$TotalSizeBytes);
			}
		// Retrieve mail box statistics - FINISHED

		// Analyze each email located in the mail box - STARTED
		if (($TMPErrorMsg == '') && ($TotalMessages > 0))
			{
			$ArrayEmailsList 	 		= $ObjectPOP3->ListMessages("", 0);
			$ArrayEmailsDetailedList	= $ObjectPOP3->ListMessages("", 1);
			
			// Start processing each email - STARTED
			for ($TMPCounter = 1; $TMPCounter <= count($ArrayEmailsList); $TMPCounter++)
				{
				// Only process allowed number of emails per execution - STARTED
				if (($TMPCounter >= $EachPOP3['EmailsProcessAtOneTime']) && ($EachPOP3['EmailsProcessAtOneTime'] != 0))
					{
					break;
					}
				// Only process allowed number of emails per execution - FINISHED

				$EmailIndex		=	$TMPCounter;
				$EmailSize		=	$ArrayEmailsList[$TMPCounter];
				$EmailUniqueID	=	$ArrayEmailsDetailedList[$TMPCounter];

				// Retrieve the email - STARTED
				if ($TMPErrorMsg == '')
					{
					$TMPErrorMsg = $ObjectPOP3->RetrieveMessage($TMPCounter, &$ArrayEmailHeaders, &$ArrayEmailBody, -1);
						$EmailContent = implode("\n", $ArrayEmailHeaders)."\n\n".implode("\n", $ArrayEmailBody);
					}
				// Retrieve the email - FINISHED

				// Parse the email content - STARTED
				if ($TMPErrorMsg == '')
					{
					$ObjectEmailParser		= new OctethEmailParser();
					$OctethMIMEUtilities	= new OctethMIMEUtilities();

					$ParsedHeaders = $ObjectEmailParser->ParseHeaders($ArrayEmailHeaders);
					$ParsedBody    = $ObjectEmailParser->ParseBody($ArrayEmailBody);

					// Decode the MIME encoding (if exists) from subject, to, from fields - STARTED
					$ParsedHeaders['Subject']		=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['Subject']);
					$ParsedHeaders['To']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['To']);
					$ParsedHeaders['From']			=		$OctethMIMEUtilities->DecodeMIMEString($ParsedHeaders['From']);
					// Decode the MIME encoding (if exists) from subject, to, from fields - FINISHED
					}
				// Parse the email content - FINISHED

				// Check if this email has been sent to correct email address or not - STARTED
				if ($TMPErrorMsg == '')
					{
					$Pattern = $EachPOP3['EmailAddress'];
					
					if (eregi($Pattern, $ParsedHeaders['To']) == false)
						{
						continue;
						}
					
					$TotalProcessedRequests++;
					}
				// Check if this email has been sent to correct email address or not - FINISHED

				// Parse the email address of the sender - STARTED
				if ($TMPErrorMsg == '')
					{
					$SenderEmail = GatherEmail($ParsedHeaders['From']);
					}
				// Parse the email address of the sender - FINISHED

				// Detect the request type and process - STARTED
				if ($TMPErrorMsg == '')
					{
					$Subject	=	strtolower($ParsedHeaders['Subject']);
					
					if ($ArrayCommands[$Subject] != '')
						{
						call_user_func($ArrayCommands[$Subject], $ArrayConfig, $ArrayLanguage, $SenderEmail, $Subject, $EachPOP3);		
						}
					else
						{
						if ($EachPOP3['SendErrorEmail'] == 'Enabled')
							{
							SendIncorrectUsageEmail($SenderEmail, $Subject, $ArrayConfig, $ArrayLanguage);
							}
						}
					}
				// Detect the request type and process - FINISHED

				// Delete the email after processing - STARTED
				$TMPErrorMsg = $ObjectPOP3->DeleteMessage($EmailIndex);
				// Delete the email after processing - FINISHED
				}
			// Start processing each email - FINISHED
			}
		// Analyze each email located in the mail box - FINISHED

		// Close POP3 server connection - STARTED
		if ($TMPErrorMsg == '')
			{
			$ObjectPOP3->Close();
			}
		// Close POP3 server connection - FINISHED
		}
	// Retrieve defined mail boxes for request processing - FINISHED


	return $TotalProcessedRequests;
	}

//########################################################################################
// Gather email address from the email header format (To, From, etc.)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GatherEmail($String)
	{
	$ReversedString = strrev($String);

		$Pattern = '/>?([^(?:>|<)]*)?<? ?"?([^"]*)/';
	preg_match_all($Pattern, $ReversedString, $ArrayMatches);

	return strrev($ArrayMatches[1][0]);
	}

//########################################################################################
// Subscribe to assigned mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessSubscribeToMailLists($ArrayConfig, $ArrayLanguage, $Email, $Subject, $ArrayPOP3)
	{
	// Initialize variables - STARTED
	$ErrorExists		=	false;
	$_POST['FormValue_Email'] = $Email;
	// Initialize variables - FINISHED

	// Include required modules - STARTED
	include_once("./system/libraries/subscribe.inc.php");
	// Include required modules - FINISHED

	// Retrieve assigned mail lists - STARTED
	if ($ErrorExists == false)
		{
		$ArrayMailListIDs = array();

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_pop3 WHERE RelPOP3ServerID='".$ArrayPOP3['POP3ServerID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListIDs[] = $EachRow['RelMailListID'];
			}
		$_POST['FormValue_MailListIDs'] = $ArrayMailListIDs;
		}
	// Retrieve assigned mail lists - FINISHED

	// Subscribe member to mail lists - STARTED
	if ($ErrorExists == false)
		{
		$ArrayResult = SubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, "./system");
		}
	// Subscribe member to mail lists - FINISHED

	// If error occured, send error email to the sender - STARTED
	if ($ErrorExists == false)
		{
		if (($ArrayResult[0] == true) && ($ArrayResult[1]) != '')
			{
				$ToEmail		=	$Email;
				$Priority		=	$ArrayLanguage["Texts"]["00312"]['Priority'];
				$Subject		=	$ArrayLanguage["Texts"]["00312"]['Subject'];
					$Subject	=	str_replace("_Email_", $Email, $Subject);
					$Subject	=	str_replace("_Error_", $ArrayResult[1], $Subject);
				$Body			=	$ArrayLanguage["Texts"]["00312"]['Body'];
					$Body		=	str_replace("_Email_", $Email, $Body);
					$Body		=	str_replace("_Error_", $ArrayResult[1], $Body);
			SendProcessEmail($ToEmail, $Priority, $Subject, $Body, $ArrayLanguage, $ArrayConfig);
			}
		}
	// If error occured, send error email to the sender - FINISHED

	return true;
	}

//########################################################################################
// Unsubscribe from selected mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessUnsubscribeFromMailLists($ArrayConfig, $ArrayLanguage, $Email, $Subject, $ArrayPOP3)
	{
	// Initialize variables - STARTED
	$ErrorExists		=	false;
	$_POST['FormValue_Email'] = $Email;
	// Initialize variables - FINISHED

	// Include required modules - STARTED
	include_once("./system/libraries/unsubscribe.inc.php");
	// Include required modules - FINISHED

	// Retrieve assigned mail lists - STARTED
	if ($ErrorExists == false)
		{
		$ArrayMailListIDs = array();

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_pop3 WHERE RelPOP3ServerID='".$ArrayPOP3['POP3ServerID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListIDs[] = $EachRow['RelMailListID'];
			}
		$_POST['FormValue_MailListIDs'] = $ArrayMailListIDs;
		}
	// Retrieve assigned mail lists - FINISHED

	// UnSubscribe member to mail lists - STARTED
	if ($ErrorExists == false)
		{
		$ArrayResult = UnSubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, "./system");
		}
	// UnSubscribe member to mail lists - FINISHED

	// If error occured, send error email to the sender - STARTED
	if ($ErrorExists == false)
		{
		if (($ArrayResult[0] == true) && ($ArrayResult[1]) != '')
			{
				$ToEmail		=	$Email;
				$Priority		=	$ArrayLanguage["Texts"]["00318"]['Priority'];
				$Subject		=	$ArrayLanguage["Texts"]["00318"]['Subject'];
					$Subject	=	str_replace("_Email_", $Email, $Subject);
					$Subject	=	str_replace("_Error_", $ArrayResult[1], $Subject);
				$Body			=	$ArrayLanguage["Texts"]["00318"]['Body'];
					$Body		=	str_replace("_Email_", $Email, $Body);
					$Body		=	str_replace("_Error_", $ArrayResult[1], $Body);
			SendProcessEmail($ToEmail, $Priority, $Subject, $Body, $ArrayLanguage, $ArrayConfig);
			}
		}
	// If error occured, send error email to the sender - FINISHED

	return true;
	}

//########################################################################################
// Send login information to the user
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessRemindLoginInformation($ArrayConfig, $ArrayLanguage, $Email, $Subject, $ArrayPOP3)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED
	
	// Retrieve the member information - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$Email."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$TotalFound = mysql_num_rows($ResultSet);
	// Retrieve the member information - FINISHED

	// If member exists in the database, send the login information, else send "error email" - STARTED
	if ($TotalFound > 0)
		{
		$ArrayMember = mysql_fetch_assoc($ResultSet);

			$ToEmail		=	$Email;
			$Priority		=	$ArrayLanguage["Texts"]["00311"]['Priority'];
			$Subject		=	$ArrayLanguage["Texts"]["00311"]['Subject'];
				$Subject	=	str_replace("_Email_", $ArrayMember['Email'], $Subject);
				$Subject	=	str_replace("_Password_", $ArrayMember['Password'], $Subject);
				$Subject	=	str_replace("_URL_", $ArrayConfig['URLs']['Software'], $Subject);
			$Body			=	$ArrayLanguage["Texts"]["00311"]['Body'];
				$Body		=	str_replace("_Email_", $ArrayMember['Email'], $Body);
				$Body		=	str_replace("_Password_", $ArrayMember['Password'], $Body);
				$Body		=	str_replace("_URL_", $ArrayConfig['URLs']['Software'], $Body);
		SendProcessEmail($ToEmail, $Priority, $Subject, $Body, $ArrayLanguage, $ArrayConfig);
		}
	else
		{
			$ToEmail		=	$Email;
			$Priority		=	$ArrayLanguage["Texts"]["00310"]['Priority'];
			$Subject		=	$ArrayLanguage["Texts"]["00310"]['Subject'];
				$Subject	=	str_replace("_Email_", $Email, $Subject);
			$Body			=	$ArrayLanguage["Texts"]["00310"]['Body'];
				$Body	=	str_replace("_Email_", $Email, $Body);
		SendProcessEmail($ToEmail, $Priority, $Subject, $Body, $ArrayLanguage, $ArrayConfig);
		}
	// If member exists in the database, send the login information, else send "error email" - FINISHED

	return true;
	}


//########################################################################################
// Sends "Command Not Recognized" email to the sender
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendIncorrectUsageEmail($ToEmail, $Request, $ArrayConfig, $ArrayLanguage)
	{
	// Load required module - STARTED
	include_once("./system/classes/class_mailer.php");
	// Load required module - FINISHED

	$ObjectMailer 			= new phpmailer();

	$ObjectMailer->IsMail();

	$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
	$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
	$ObjectMailer->AddAddress(strtolower($ToEmail), '');
	$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
	$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
	$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];
	
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayLanguage["Texts"]["00309"]['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
	$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00309"]['Subject'];
		$ObjectMailer->Subject = str_replace("_Request_", $Request, $ObjectMailer->Subject);

	$ObjectMailer->IsHTML(false);
	$ObjectMailer->Body		= $ArrayLanguage["Texts"]["00309"]['Body'];
		$ObjectMailer->Body = str_replace("_Request_", $Request, $ObjectMailer->Body);

	$ObjectMailer->Send();

	return true;
	}

//########################################################################################
// Sends email
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendProcessEmail($ToEmail, $Priority, $Subject, $Body, $ArrayLanguage, $ArrayConfig)
	{
	// Load required module - STARTED
	include_once("./system/classes/class_mailer.php");
	// Load required module - FINISHED

	$ObjectMailer 			= new phpmailer();

	$ObjectMailer->IsMail();

	$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
	$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
	$ObjectMailer->AddAddress(strtolower($ToEmail), '');
	$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
	$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
	$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];
	
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $Priority;
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
	$ObjectMailer->Subject	= $Subject;

	$ObjectMailer->IsHTML(false);
	$ObjectMailer->Body		= $Body;

	$ObjectMailer->Send();

	return true;
	}

?>