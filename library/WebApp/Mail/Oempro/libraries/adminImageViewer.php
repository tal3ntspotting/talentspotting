<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//-------------------------------------------------------------[ adminImageViewer - STARTED

include_once("./include_loggedinheader.inc.php");

if ($ArrayLoggedAdmin['Skin'] != '')
	{
	$SkinName = strtolower($ArrayLoggedAdmin['Skin']);
	}
else
	{
	$SkinName = strtolower($ArrayConfig['Settings']['Skin']);
	}
		
	
//IMAGE MUST BE JPG with width 60px!--------
$defaultImage=$ArrayConfig['Paths']['Data']."/templates/".$SkinName."/media/images/avatar/defaultAdmin.jpg";


if(isset($_GET['id']))
{

	$id    = strip_tags($_GET['id']);
	
	$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$id."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayFieldnValues = mysql_fetch_assoc($ResultSet);
	
	
	$content=$ArrayFieldnValues['Picture_content'];
	$size=$ArrayFieldnValues['Picture_size'];	
	$type=$ArrayFieldnValues['Picture_type'];	

	if($type!='' and $size!=0)		
	{
	header("Content-length: $size");
	header("Content-type: $type");
	header("Content-Disposition: attachment; filename=$name");
	
	echo $content;

	exit;
	}
	else
	{
	
	
	$defaultImageSize =	filesize($defaultImage);
		
	$fp  = fopen($defaultImage, 'r');
	
	$pictureContent = fread($fp, $defaultImageSize);
	//$pictureContent = addslashes($pictureContent);
	fclose($fp);

	header("Content-length: $defaultImageSize");
	header("Content-type: image/jpeg");
	header("Content-Disposition: attachment; filename=$name");
	
    echo $pictureContent;

	exit;	
	
	}
	
	
}
//-------------------------------------------------------------[ adminImageViewer - FINISHED
?>