<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Process mail list subscription settings
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessMailListSubSettings($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayConfig, $ArrayLanguage, $PathOfSystem)
	{
	if ($ArrayMailList['SubscriptionSettings'] == 'NA')
		{
		return;
		}
	elseif (($ArrayMailList['SubscriptionSettings'] == 'SubscribeToAnother') && ($ArrayMailList['RelSubSettingsSubscribeMailListID'] > 0))
		{
			include_once($PathOfSystem."/libraries/subscribe.inc.php");
			$Email 				= $ArrayMember['Email'];
			$ArrayMailListIDs	= array($ArrayMailList['RelSubSettingsSubscribeMailListID']);
		SubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
		}
	elseif (($ArrayMailList['SubscriptionSettings'] == 'UnSubscribeFrom') && ($ArrayMailList['RelSubSettingsUnSubscribeMailListID'] > 0))
		{
			include_once($PathOfSystem."/libraries/unsubscribe.inc.php");
			$Email 				= $ArrayMember['Email'];
			$ArrayMailListIDs	= array($ArrayMailList['RelSubSettingsUnSubscribeMailListID']);
		UnSubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
		}

	return;
	}
	
//########################################################################################
// Subscribe email address to the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem)
	{
	// Initialize variables - STARTED
	$ErrorExists		=	false;
	$ErrorMessage		=	"";
	$ArrayInformation	=	array(
								"FormValue_Email"		=>	$Email,
								);
	// Initialize variables - FINISHED
	
	// Check for required fields - STARTED
	if ($ErrorExists == false)
		{
		$ArrayRequiredFields = array(
									'Email'						=>		$ArrayLanguage["Texts"]["00016"],
									);

		// Load the required module
		include_once($PathOfSystem."/classes/class_octethreqfldchecker.php");
		
		// Initialize the object
		$ObjectReqFieldChecker = new OctethReqFieldChecker($ArrayRequiredFields, $ArrayErrorMessages, $ArrayInformation, 'FormValue_');
			$ArrayErrorMessages = $ObjectReqFieldChecker->CheckForRequiredFields();
		if (count($ArrayErrorMessages) > 0)
			{
			$ErrorExists = true;
			$ErrorMessage = $ArrayLanguage["Texts"]["00314"];
			}
		}
	// Check for required fields - FINISHED

	// Check for unique fields - STARTED
	if ($ErrorExists == false)
		{
		// Include the required module - STARTED
		include_once($PathOfSystem."/libraries/membership.inc.php");
		// Include the required module - FINISHED

		// Check email address - STARTED
		if ($ErrorExists == false)
			{
			if ($ArrayInformation['FormValue_Email'] != '')
				{
				// Load the required module - STARTED
				include_once($PathOfSystem."/classes/class_utilities1.php");
				// Load the required module - FINISHED
		
				$ObjectUtilities = new OctethUtilities();
		
				if ($ObjectUtilities->CheckEmailFormat(strtolower($ArrayInformation['FormValue_Email'])) == false)
					{
					$ErrorExists = true;
					$ErrorMessage = $ArrayLanguage["Texts"]["00313"];
					}
				}
			}
		// Check email address - FINISHED

		// Check if email address is banned or not - STARTED
		if ($ErrorExists == false)
			{
				$Email 				= $ArrayInformation['FormValue_Email'];
				$ArrayMailLists 	= $ArrayMailListIDs;
			$EmailBanned = CheckEmailIfBanned($Email, $ArrayMailLists, $ArrayConfig);

			if ($EmailBanned == true)
				{
				$ErrorExists = true;
				$ErrorMessage = $ArrayLanguage["Texts"]["00315"];
				}
			}
		// Check if email address is banned or not - FINISHED

		// Check if email address is black listed or not - STARTED
		if ($ErrorExists == false)
			{
				$Email 				= $ArrayInformation['FormValue_Email'];
				$ArrayMailLists 	= $ArrayMailListIDs;
			$EmailBlackListed = CheckEmailIfBlackListed($Email, $ArrayMailLists, $ArrayConfig);
			
			if ($EmailBlackListed == true)
				{
				$ErrorExists = true;
				$ErrorMessage = $ArrayLanguage["Texts"]["00316"];
				}
			}
		// Check if email address is black listed or not - FINISHED
		}
	// Check for unique fields - FINISHED

	// Field validations - STARTED
	if ($ErrorExists == false)
		{
		}
	// Field validations - FINISHED

	// Add record - STARTED
	if ($ErrorExists == false)
		{
		// Check if email exists or not. If exists, do not add member account, just retrieve it - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$ArrayInformation['FormValue_Email']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$AddRecord = false;

			$ArrayMember = mysql_fetch_assoc($ResultSet);
				$NewMemberID = $ArrayMember['MemberID'];
			}
		else
			{
			$AddRecord = true;
			}
		// Check if email exists or not. If exists, do not add member account, just retrieve it - FINISHED

		// Add member account if this member does not exist in the system - STARTED
		if ($AddRecord == true)
			{
			// Generate new password for the member - STARTED
			include_once($PathOfSystem."/classes/class_utilities1.php");
			$ObjectUtilities = new OctethUtilities();
				$NewPassword = $ObjectUtilities->GeneratePassword(5);
			// Generate new password for the member - FINISHED

			$ArrayFieldnValues = array(
									"MemberID"			=>	'',
									"Email"				=>	$ArrayInformation['FormValue_Email'],
									"BounceType"		=>	'Not bounced',
									"Password"			=>	$NewPassword,
									);
	
			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList = "'".$ValueList."'";
			
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$NewMemberID = mysql_insert_id();
			}
		// Add member account if this member does not exist in the system - FINISHED
		}
	// Add record - FINISHED

	// Subscribe to selected mail lists - STARTED
	if ($ErrorExists == false)
		{
		$TotalSubscribedMailLists = 0;
		
		foreach ($ArrayMailListIDs as $EachMailListID)
			{
			// Retrieve mail list information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachMailListID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMailList = mysql_fetch_assoc($ResultSet);
			// Retrieve mail list information - FINISHED

			// Check if member has subscribed to this mail list or not - STARTED
			$UpdateExistingSubscription = false;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMemberID='".$NewMemberID."' AND RelMailListID='".$EachMailListID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				$UpdateExistingSubscription = true;
				
				$ArrayMembership = mysql_fetch_assoc($ResultSet);
				}
			// Check if member has subscribed to this mail list or not - FINISHED

			// Add new membership or update existing membership - STARTED
			if ($UpdateExistingSubscription == false)
				{
				$ArrayFieldnValues = array(
										"MembershipID"			=>	'',
										"RelMailListID"			=>	$EachMailListID,
										"RelMemberID"			=>	$NewMemberID,
										"OptInConfirmed"		=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? 'Yes' : 'No'),
										"OptOutConfirmed"		=>	'No',
										"SubscriptionStatus"	=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? 'Subscribed' : 'Pending for opt-in confirmation'),
										"SubscriptionDate"		=>	date("Y-m-d H:i:s"),
										"UnsubscriptionDate"	=>	'0000-00-00 00:00:00',
										"OptInConfirmDate"		=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? date("Y-m-d H:i:s") : '0000-00-00 00:00:00'),
										"OptOutConfirmDate"		=>	'0000-00-00 00:00:00',
										"IPOnSubscription"		=>	'0.0.0.0',
										"IPOnUnSubscription"	=>	'0.0.0.0',
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList = "'".$ValueList."'";
				
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_members (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$NewMembershipID = mysql_insert_id();

				// Retrieve mail list and member information - STARTED
				$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$NewMemberID."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);
	
				$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$EachMailListID."' AND RelMemberID='".$NewMemberID."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet);
				// Retrieve mail list and member information - FINISHED

				// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - STARTED
				if ($ArrayMailList['SubscriptionType'] == 'Opt-in')
					{
					// Load required module - STARTED
					include_once($PathOfSystem."/classes/class_mailer.php");
					include_once($PathOfSystem."/libraries/membership.inc.php");
					include_once($PathOfSystem."/libraries/personalization.inc.php");
					// Load required module - FINISHED

						$ArrayTMPMailListIDs		=	array($EachMailListID);
						$EmailType				=	'OptInConfirmationRequest';
					SendMailListEmails($ArrayTMPMailListIDs, $EmailType, $ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig);
					}
				// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - FINISHED

				// Register member to receive auto responders of the mail list as follow up - STARTED
				if ($ArrayMailList['SubscriptionType'] != 'Opt-in')
					{
						include_once("./system/libraries/followup.inc.php");
					$TotalRegistered = RegisterFollowUpAutoResponders($EachMailListID, $NewMemberID, $ArrayConfig, $ArrayLanguage);
					}
				// Register member to receive auto responders of the mail list as follow up - FINISHED
				}
			else
				{
				if (($ArrayMembership['SubscriptionStatus'] == 'Pending for opt-in confirmation') || ($ArrayMembership['SubscriptionStatus'] == 'Subscribed'))
					{
					continue;
					}

				$ArrayFieldnValues = array(
										"MembershipID"			=>	$ArrayMembership['MembershipID'],
										"RelMailListID"			=>	$EachMailListID,
										"RelMemberID"			=>	$NewMemberID,
										"OptInConfirmed"		=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? 'Yes' : 'No'),
										"OptOutConfirmed"		=>	'No',
										"SubscriptionStatus"	=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? 'Subscribed' : 'Pending for opt-in confirmation'),
										"SubscriptionDate"		=>	date("Y-m-d H:i:s"),
										"UnsubscriptionDate"	=>	'0000-00-00 00:00:00',
										"OptInConfirmDate"		=>	($ArrayMailList['SubscriptionType'] != 'Opt-in' ? date("Y-m-d H:i:s") : '0000-00-00 00:00:00'),
										"OptOutConfirmDate"		=>	'0000-00-00 00:00:00',
										"IPOnSubscription"		=>	$ArrayMembership["IPOnSubscription"],
										"IPOnUnSubscription"	=>	$ArrayMembership["IPOnUnSubscription"],
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList = "'".$ValueList."'";
				
				$SQLQuery = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."maillist_members (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$NewMembershipID = mysql_insert_id();

				// Retrieve mail list and member information - STARTED
				$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$NewMemberID."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);
	
				$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$EachMailListID."' AND RelMemberID='".$NewMemberID."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet);
				// Retrieve mail list and member information - FINISHED

				// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - STARTED
				if ($ArrayMailList['SubscriptionType'] == 'Opt-in')
					{
					// Load required module - STARTED
					include_once($PathOfSystem."/classes/class_mailer.php");
					include_once($PathOfSystem."/libraries/membership.inc.php");
					include_once($PathOfSystem."/libraries/personalization.inc.php");
					// Load required module - FINISHED

						$ArrayTMPMailListIDs		=	array($EachMailListID);
						$EmailType				=	'OptInConfirmationRequest';
					SendMailListEmails($ArrayTMPMailListIDs, $EmailType, $ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig);
					}
				// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - FINISHED

				// Register member to receive auto responders of the mail list as follow up - STARTED
				if ($ArrayMailList['SubscriptionType'] != 'Opt-in')
					{
						include_once("./system/libraries/followup.inc.php");
					$TotalRegistered = RegisterFollowUpAutoResponders($EachMailListID, $NewMemberID, $ArrayConfig, $ArrayLanguage);
					}
				// Register member to receive auto responders of the mail list as follow up - FINISHED
				}
			// Add new membership or update existing membership - STARTED

			// If opt-in confirmation is not enabled for this mail list, process subscription settings - STARTED
			if ($ArrayMailList['SubscriptionType'] == 'Free')
				{
				ProcessMailListSubSettings($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
				}
			// If opt-in confirmation is not enabled for this mail list, process subscription settings - FINISHED

			// Update mail list activity statistics - STARTED
			if (($ArrayMailList['SubscriptionType'] == 'Free') && ($ErrorExists == false))
				{
				$ArrayFieldnValues = array(
										"ActivityID"		=> '',
										"RelMemberID"		=> $ArrayMember['MemberID'],
										"RelMailListID"		=> $EachMailListID,
										"Activity"			=> 'Subscription',
										"ActivityDateTime"	=> date('Y-m-d H:i:s'),
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList ="'".$ValueList."'";
		
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members_activities_detailed (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSetActivityDetails = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				}
			// Update mail list activity statistics - FINISHED
				
			$TotalSubscribedMailLists++;
			}
		
		// If couldn't subscribe to at least one mail list, display error message - STARTED
		if ($TotalSubscribedMailLists == 0)
			{
			$ErrorExists = true;
			$ErrorMessage = $ArrayLanguage["Texts"]["00317"];
			}
		// If couldn't subscribe to at least one mail list, display error message - FINISHED
		}
	// Subscribe to selected mail lists - FINISHED

	// Update activity statistics - STARTED
	if ($ErrorExists == false)
		{
		$ArrayFieldnValues = array(
								"ActivityID"		=>	'',
								"RelMemberID"		=>	$ArrayMember['MemberID'],
								"Activity"			=>	'Subscription',
								"ActivityDateTime"	=>	date("Y-m-d H:i:s"),
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members_activities (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Update activity statistics - FINISHED

	return array($ErrorExists, $ErrorMessage);
	}

?>