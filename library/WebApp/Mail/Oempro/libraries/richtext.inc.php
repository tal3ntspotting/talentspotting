<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Generate the correct rich text editor code and return it's HTML code
function GenerateRichText($ArrayParameters, $EditorPath, $ArrayLanguage, $ArrayConfig)
	{
	include_once($EditorPath."/fckeditor/fckeditor.php");
	
	$ObjectEditor				= new FCKeditor($ArrayParameters['FieldName']);

	$ObjectEditor->BasePath							= $ArrayParameters['BasePath'];
	$ObjectEditor->Value							= $ArrayParameters['Value'];
	$ObjectEditor->Width							= $ArrayParameters['Width'];
	$ObjectEditor->Height							= $ArrayParameters['Height'];
	$ObjectEditor->ToolbarSet						= $ArrayParameters['ToolbarSet'];
	$ObjectEditor->Config['FullPage']				= $ArrayParameters['FullPage'];
	$ObjectEditor->Config['AutoDetectLanguage']		= $ArrayParameters['AutoDetectLanguage'];
	$ObjectEditor->Config['DefaultLanguage']		= $ArrayParameters['DefaultLanguage'];
	$ObjectEditor->Config['StartUpFocus']			= $ArrayParameters['StartUpFocus'];
	$ObjectEditor->Config['UseBROnCarriageReturn']	= $ArrayParameters['UseBROnCarriageReturn'];
	$ObjectEditor->Config['EnableXHTML']			= $ArrayParameters['EnableXHTML'];
	$ObjectEditor->Config['EnableSourceXHTML']		= $ArrayParameters['EnableSourceXHTML'];

	$HTMLOutput 				= $ObjectEditor->CreateHtml() ;

	return $HTMLOutput;
	}


//########################################################################################
// Return Rich Text Editor (For rich text editor: Rich Area)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GenerateRichTextEditor($RECaption, $REFormItemName, $REDefaultValue, $REWidth, $REHeight, $REFilesPath, $REFilesURL, $REPageMode, $REAbsolutePaths, $ArrayLoggedAdmin)
	{	
	$ObjectRichText = new rich($RECaption, $REFormItemName, $REDefaultValue, $REWidth, $REHeight, $REFilesPath, $REFilesURL, $REPageMode, $REAbsolutePaths);

	$ObjectRichText->active_mode();
	$ObjectRichText->set_lang('en');
	$ObjectRichText->set_doc_lang('en');
	$ObjectRichText->set_br_on_enter(false);
	$ObjectRichText->set_doc_charset(($ArrayLoggedAdmin['CharSet'] != '' ? substr($ArrayLoggedAdmin['CharSet'], 0, strlen($ArrayLoggedAdmin['CharSet']) - 5) : $ArrayLanguage["Config"]["CharSet"]));

	$ObjectRichText->hide_tb('fullscreen', false);
	$ObjectRichText->hide_tb('preview', false);
	$ObjectRichText->hide_tb('find', false);
	$ObjectRichText->hide_tb('clipboard', false);
	$ObjectRichText->hide_tb('history', false);
	$ObjectRichText->hide_tb('text', false);
	$ObjectRichText->hide_tb('align', false);
	$ObjectRichText->hide_tb('list', false);
	$ObjectRichText->hide_tb('indent', false);
	$ObjectRichText->hide_tb('font', false);
	$ObjectRichText->hide_tb('size', false);
	$ObjectRichText->hide_tb('color', false);
	$ObjectRichText->hide_tb('image', false);
	$ObjectRichText->hide_tb('page_properties', false);
	$ObjectRichText->hide_tb('source', false);
	$ObjectRichText->hide_tb('link', false);
	$ObjectRichText->hide_tb('hr', false);
	$ObjectRichText->hide_tb('remove_format', false);
	$ObjectRichText->hide_tb('form', false);
	$ObjectRichText->hide_tb('table', false);
	$ObjectRichText->hide_tb('adv_table', false);
	$ObjectRichText->hide_tb('paragraph', false);
	$ObjectRichText->hide_tb('style', true);
	$ObjectRichText->hide_tb('flash', true);
	$ObjectRichText->hide_tb('paste_word', false);
	$ObjectRichText->hide_tb('switch_borders', true);
	$ObjectRichText->hide_tb('special_chars', false);
	$ObjectRichText->hide_tb('snippets', true);
	$ObjectRichText->hide_tb('help', true);
	$ObjectRichText->hide_tb('absolute_position', true);

	$RichTextContent =  $ObjectRichText->get_rich();

	return $RichTextContent;
	}
	
?>