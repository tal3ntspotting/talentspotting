<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Apply time zone to the entered time
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ApplyTimeZone($DateTime, $TimeZone, $ApplyTimeZone, $ArrayConfig)
	{
	if ($ApplyTimeZone == false)
		{
		return $DateTime;
		}
	else
		{
		$RawTimeZone	= str_replace('0', '', $TimeZone);
		$RawTimeZone	= intval($RawTimeZone) * 3600;
		$ServerTimeZone = date('Z');
		$ServerDST		= date('I');
		if ($ServerDST == 1)
			{
			$ServerTimeZone = $ServerTimeZone - 3600;
			}
		$SecondsToAdd	= -1 * ($ServerTimeZone - $RawTimeZone);

		$DateTime = strtotime($DateTime);
		$AppliedDateTime = date('Y-m-d H:i:s', strtotime(date('Y-m-d', $DateTime).($SecondsToAdd > 0 ? ' +'.$SecondsToAdd : ' '.$SecondsToAdd).' seconds '.date('H:i:s', $DateTime)));
		}
	
	return $AppliedDateTime;
	}

//########################################################################################
// Generates an array of time zones
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GenerateTimeZoneList($ArrayConfig)
	{
	$ArrayTimeZones = array();
	
	$TMPLoopCounter = 1;

	$ServerTimeZone = date('Z');
	$ServerDST		= date('I');

	if ($ServerDST == 1)
		{
		$ServerTimeZone = $ServerTimeZone - 3600;
		}

	for ($TMPCounter = -12; $TMPCounter <= 14; $TMPCounter++)
		{
		$OffsetSeconds	= $TMPCounter * 3600;
		$SecondsToAdd	= -1 * ($ServerTimeZone - $OffsetSeconds);
		$TimeZone		= ($TMPCounter < 0 ? sprintf("%03d", $TMPCounter) : sprintf("%02d", $TMPCounter)).'00';

		$ArrayTimeZones[$TMPLoopCounter]['Prefix']		= ($TMPCounter < 0 ? '' : '+');
		$ArrayTimeZones[$TMPLoopCounter]['TimeZone']	= $TimeZone;
		$ArrayTimeZones[$TMPLoopCounter]['CurrentTime']	= date('H:i', strtotime(date('Y-m-d').' '.$SecondsToAdd.' seconds '.date('H:i:s')));

		$TMPLoopCounter++;
		}

	return $ArrayTimeZones;
	}
	
//########################################################################################
// Returns the difference in formatted
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TimeDifference($DurationSeconds, $TextDays, $TextHours, $TextMinutes, $TextSeconds)
	{
		$TotalDurationDays		= floor($DurationSeconds / 86400); // Days
		$TotalDurationHours		= sprintf("%02d", ($DurationSeconds / 3600) % 24); // Hours
		$TotalDurationMinutes	= sprintf("%02d", ($DurationSeconds / 60) % 60); // Minutes
		$TotalDurationSeconds	= sprintf("%02d", ($DurationSeconds % 60)); // Seconds
	
	$TotalDuration				= array();
	if ($TotalDurationDays != '00')
		{
		$TotalDuration[] = $TotalDurationDays." ".$TextDays;
		}
	if ($TotalDurationHours != '00')
		{
		$TotalDuration[] = $TotalDurationHours." ".$TextHours;
		}
	if ($TotalDurationMinutes != '00')
		{
		$TotalDuration[] = $TotalDurationMinutes." ".$TextMinutes;
		}
	if ($TotalDurationSeconds != '00')
		{
		$TotalDuration[] = $TotalDurationSeconds." ".$TextSeconds;
		}
	
	if (count($TotalDuration) == 0)
		{
		$TotalDuration[] = "0 ".$TextSeconds;
		}
	
	$TotalDuration = implode(" ", $TotalDuration);
	
	return $TotalDuration;
	}

?>