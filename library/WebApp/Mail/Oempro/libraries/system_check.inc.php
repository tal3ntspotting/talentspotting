<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Check system settings
function CheckSystemSettings($ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists	= false;
	$ArrayErrors	= array();
	// Initialize variables - FINISHED

	// Compare defined URL and running URL - STARTED
		$ArrayParsedURL = parse_url($ArrayConfig['URLs']['Software']);
	if ($_SERVER['HTTP_HOST'] != $ArrayParsedURL['host'])
		{
		$ErrorExists	= true;
			$TMPLanguageText = $ArrayLanguage["Texts"]["00532"];
			$TMPLanguageText = str_replace('_oemProURL_', $ArrayConfig['URLs']['Software'], $TMPLanguageText);
		$ArrayErrors[]	= $TMPLanguageText;
		}
	// Compare defined URL and running URL - FINISHED

	// Check PHP settings - STARTED
	$Result = ini_get('register_globals');
	if (($Result == true) || ($Result == 'on') || ($Result == '1'))
		{
		$ErrorExists	= true;
		$ArrayErrors[]	= $ArrayLanguage["Texts"]["00519"];
		}

	$Result = ini_get('magic_quotes_runtime');
	if (($Result == true) || ($Result == 'on') || ($Result == '1'))
		{
		$ErrorExists	= true;
		$ArrayErrors[]	= $ArrayLanguage["Texts"]["00533"];
		}

	$Result = ini_get('max_execution_time');
	if ($Result < 1800)
		{
		$ErrorExists	= true;
		$ArrayErrors[]	= $ArrayLanguage["Texts"]["00521"];
		}
	// Check PHP settings - FINISHED
	
	// Check MySQL settings - STARTED
	$SQLQuery = "SHOW VARIABLES LIKE 'sql_mode'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayResult = mysql_fetch_assoc($ResultSet);
	
	$SQLMode = $ArrayResult['sql_mode'];
	
	if (preg_match('STRICT', $SQLMode) == true)
		{
		$ErrorExists = true;
		$ArrayErrors[] = $ArrayLanguage["Texts"]["00523"];
		}
	// Check MySQL settings - FINISHED
	
	// Check file and directory permissions - STARTED
	$ArrayDirectories = array(
							$ArrayConfig['Paths']['Software'].'/system/data/campaign_attachments',
							$ArrayConfig['Paths']['Software'].'/system/data/campaign_processing',
							$ArrayConfig['Paths']['Software'].'/system/data/config',
							$ArrayConfig['Paths']['Software'].'/system/data/db',
							$ArrayConfig['Paths']['Software'].'/system/data/import',
							$ArrayConfig['Paths']['Software'].'/system/data/export',
							$ArrayConfig['Paths']['Software'].'/system/data/newsletter_templates',
							$ArrayConfig['Paths']['Software'].'/system/data/user_uploads',
							);

	foreach ($ArrayDirectories as $EachDirectory)
		{
		if (CheckDirectoryPermissions($EachDirectory, $ArrayConfig) == true)
			{
			$ErrorExists	= true;
			$ArrayErrors[]	= $EachDirectory.$ArrayLanguage["Texts"]["00522"];
			}
		}
	// Check file and directory permissions - FINISHED
	
	return $ArrayErrors;
	}

//########################################################################################
// Check system settings
function CheckDirectoryPermissions($Path, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists	= false;
	// Initialize variables - FINISHED

	// Check if path has read/write access - STARTED
	if ($ErrorExists == false)
		{
		$FileHandler = fopen($Path.'/_test_oempro.txt', "w");
			fwrite($FileHandler, 'Testing read/write permissions');
		fclose($FileHandler);
	
		if (file_exists($Path.'/_test_oempro.txt') == true)
			{
			unlink($Path.'/_test_oempro.txt');
			}
		else
			{
			$ErrorExists = true;
			}
		}
	// Check if path has read/write access - FINISHED
	
	return $ErrorExists;
	}


?>