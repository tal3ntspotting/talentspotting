<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Execute each cron module registered in oemPro
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ExecuteCRONModules()
	{
	global $ArrayConfig;
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."cron";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachCron = mysql_fetch_assoc($ResultSet))
		{
		$FileName = $ArrayConfig['URLs']['Software']."/".$EachCron['CronFileName'];

		$Return = implode('', file($FileName));
		}

	return;
	}

?>