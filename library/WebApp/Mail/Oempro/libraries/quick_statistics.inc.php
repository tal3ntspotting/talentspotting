<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Retrieves the statistics
function TotalSendingCampaigns($ArrayAdministrator, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns AS tblCampaign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."campaigns_statistics AS tblStats ";
	$SQLQuery .= "ON tblCampaign.CampaignID = tblStats.RelCampaignID ";
	$SQLQuery .= "WHERE tblStats.Status = 'Sending' ";
	
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery .= "AND tblCampaign.RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		}

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
	
	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalSentCampaigns($ArrayAdministrator, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns AS tblCampaign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."campaigns_statistics AS tblStats ";
	$SQLQuery .= "ON tblCampaign.CampaignID = tblStats.RelCampaignID ";
	$SQLQuery .= "WHERE tblStats.Status = 'Completed' ";
	
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery .= "AND tblCampaign.RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
	
	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalReads($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TotalReads = 0;
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery = "SELECT SUM(ReadAmount) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_read WHERE RelCampaignID='".$EachRow['CampaignID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalReads += $TotalFound['TotalFound'];
			}
		$TotalFound = $TotalReads;
		}
	else
		{
		$SQLQuery = "SELECT SUM(ReadAmount) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_read";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		}


	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalClicks($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TotalClicks = 0;
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery = "SELECT SUM(ClickAmount) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_linkclicks WHERE RelCampaignID='".$EachRow['CampaignID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalClicks += $TotalFound['TotalFound'];
			}
		$TotalFound = $TotalClicks;
		}
	else
		{
		$SQLQuery = "SELECT SUM(ClickAmount) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_linkclicks";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		}


	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalBanned($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_administrators WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TotalBanned = 0;

		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE RelMailListID='0'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalBanned += $TotalFound['TotalFound'];

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE RelMailListID='".$EachRow['RelMailListID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalBanned += $TotalFound['TotalFound'];
			}
		$TotalFound = $TotalBanned;
		}
	else
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		}


	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalBlackListed($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_administrators WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TotalBlackListed = 0;

		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE RelMailListID='0'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalBlackListed += $TotalFound['TotalFound'];

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE RelMailListID='".$EachRow['RelMailListID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalBlackListed += $TotalFound['TotalFound'];
			}
		$TotalFound = $TotalBlackListed;
		}
	else
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		}


	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalBounced($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		return TotalLocalBounced($ArrayAdministrator, $ArrayConfig);
		}
	else
		{
		return TotalLocalBounced($ArrayAdministrator, $ArrayConfig) + TotalOutsourcedBounced($ArrayAdministrator, $ArrayConfig);
		}
	}

//########################################################################################
// Retrieves the statistics
function TotalOutsourcedBounced($ArrayAdministrator, $ArrayConfig)
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."bounce_log WHERE RelMemberID='0' GROUP BY EmailAddress";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_num_rows($ResultSet);

	return $TotalFound;
	}

//########################################################################################
// Retrieves the statistics
function TotalLocalBounced($ArrayAdministrator, $ArrayConfig)
	{
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_administrators AS tblAssign ";
		$SQLQuery .= "ON tblMailLists.MailListID = tblAssign.RelMailListID ";
		$SQLQuery .= "WHERE tblAssign.RelAdministratorID = '".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$ArrayMailListIDs = array();

		while ($EachMailList = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListIDs[] = $EachMailList['MailListID'];
			}

		$SQLQueryMailListIDs = "tblAssign.RelMailListID= '".implode("' OR tblAssign.RelMailListID= '", $ArrayMailListIDs)."'";

		$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
		$SQLQuery .= "WHERE (".$SQLQueryMailListIDs.") AND tblMembers.BounceType!='Not bounced' ";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TMPTotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalFound = $TMPTotalFound['TotalFound'];
		}
	else
		{	
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members WHERE BounceType!='Not bounced'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		}
		
	return $TotalFound;
	}


?>