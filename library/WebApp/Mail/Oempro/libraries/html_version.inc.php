<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Retrieve the content of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveContent($ArrayCampaign, $ArrayCampaignStatistics, $ArrayMember, $Email, $ArrayConfig, $ArrayLanguage, $PublicDisplay)
	{
	// Initialize variables - STARTED
	$ArrayStatus = array();
	// Initialize variables - FINISHED

	// Include the required modules - STARTED
	include_once("./system/libraries/plugin_functions.inc.php");
	include_once("./system/libraries/personalization.inc.php");
	include_once("./system/libraries/campaign_send.inc.php");
	include_once("./system/libraries/campaign_preview.inc.php");
	// Include the required modules - FINISHED

	// Retrieve campaign SMTP settings - STARTED
	// Retrieve campaign SMTP settings - FINISHED

	// Retrieve attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayAttachments[] = array(
									"AttachmentID"		=>	$EachRow['AttachmentID'],
									"FileName"			=>	$EachRow['FileName'],
									"FileSize"			=>	$EachRow['FileSize'],
									);
		}
	// Retrieve attachments - FINISHED

	// Retrieve content - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentMainURL']));
			$ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentAlternateURL']));
			$ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve content - FINISHED

	// Add administrator header and footer to the content - STARTED
		include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$ArrayContent = AddAdministratorHeaderFooter($ArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
	// Add administrator header and footer to the content - FINISHED


	// Retrieve from, reply to and return path emails - STARTED
	// Retrieve from, reply to and return path emails - FINISHED

	// Check if the campaign owner administrator has a pre-defined return-path email address - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayCampaignAdministrator = mysql_fetch_assoc($ResultSet);
	// Check if the campaign owner administrator has a pre-defined return-path email address - FINISHED

	// Define email properties - STARTED
		// Define subject - STARTED
		$TMPSubject = $ArrayContent['Subject'];
		// Define subject - FINISHED
		
		// Define content - STARTED
		if (count($ArrayNewsletter) > 0)
			{
			// CMS based content
			$TMPPriority 			= $ArrayNewsletter['Priority'];
			$TMPCharSet				= $ArrayNewsletter['CharSet'];
			$TMPContentType			= $ArrayNewsletter['ContentType'];
			if ($TMPContentType == 'Html')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}
			elseif ($TMPContentType == 'Both')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				}
			elseif ($TMPContentType == 'Plain')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}

			// Add administrator header and footer to the content - STARTED
				include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
				$TMPArrayContent = array(
										"ContentMain"		=> $TMPContentMain,
										"ContentAlternate"	=> $TMPContentAlternate,
										"ContentType"		=> $TMPContentType,
										);
			$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
				$TMPContentMain = $TMPArrayContent['ContentMain'];
				$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
			// Add administrator header and footer to the content - FINISHED
			}
		else
			{
			// Traditional template or quick mode
			$TMPPriority 			= $ArrayContent['Priority'];
			$TMPCharSet				= $ArrayContent['CharSet'];
			$TMPContentType			= $ArrayContent['ContentType'];
			$TMPContentMain			= $ArrayContent['ContentMain'];
			$TMPContentAlternate	= $ArrayContent['ContentAlternate'];
			}
		// Define content - FINISHED

		// Run external plug-ins which are content specific - Start
		$ArrayPlugInReturn = RunPlugInContentProcesses($TMPContentMain, $TMPContentAlternate, array(), $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig);
			$TMPContentMain		=	$ArrayPlugInReturn[0];
			$TMPContentAlternate=	$ArrayPlugInReturn[1];
		// Run external plug-ins which are content specific - End

		// Process on email content (personalization, tracking links, tracking reads) - STARTED
		if ($PublicDisplay == false)
			{
			if ($ArrayContent['Personalize'] == 'Yes')
				{
					$ArrayPrefixes		=	array(
												"Sender",
												"Member",
												"Membership",
												"Campaign"
												);
					$ArrayInformation	=	array(
												$ArrayCampaignAdministrator,
												$ArrayMember,
												$ArrayMembership,
												$ArrayCampaign,
												);											
				$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
				$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
				$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
				}
			}

			$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
			$TMPContentMain			=	PersonalizeCampaignTagsForPublic($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, false, $PublicDisplay, '%');
			$TMPContentAlternate	=	PersonalizeCampaignTagsForPublic($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, false, $PublicDisplay, '%');

			// Perform global personalization - STARTED
			$TMPSubject				= PerformGlobalCampaignPersonalization($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			$TMPContentMain			= PerformGlobalCampaignPersonalization($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			$TMPContentAlternate	= PerformGlobalCampaignPersonalization($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			// Perform global personalization - FINISHED

		// Process on email content (personalization, tracking links, tracking reads) - FINISHED

	$ArrayEmailProperties = array();
		$ArrayEmailProperties	= array(
										"CampaignID"		=>	$ArrayCampaign['CampaignID'],
										"EmbedObjects"		=>	($ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
										"FromEmail"			=>	$ArrayFromEmail['EmailAddress'],
										"FromName"			=>	$ArrayFromEmail['Name'],
										"ReplyToEmail"		=>	$ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"		=>	$ArrayReplyToEmail['Name'],
										"ReturnPathEmail"	=>	($ArrayCampaignAdministrator['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayCampaignAdministrator['ReturnPathEmail']),
										"ToName"			=>	$ArrayMember['Email'],
										"ToEmail"			=>	$ArrayMember['Email'],
										"Priority"			=>	$TMPPriority,
										"CharSet"			=>	$TMPCharSet,
										"Subject"			=>	$TMPSubject,
										"ContentType"		=>	$TMPContentType,
										"ContentMain"		=>	$TMPContentMain,
										"ContentAlternate"	=>	$TMPContentAlternate,
										"Attachments"		=>	$ArrayAttachments,
										"SendEngines"		=>	$ArraySMTP,
										);
	// Define email properties - FINISHED

	return $ArrayEmailProperties;
	}
?>