<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Fixes FCKEditor image source bug
function FixFCKEditorImageSourceBug($Content, $ArrayConfig)
	{
	preg_match_all("/src\=\"(.*)\/data\/user_uploads(.*)\"/iU", $Content, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $EachMatch)
		{
		$TMPImageURL = $EachMatch[1].'/data/user_uploads'.$EachMatch[2];
		$TMPCorrectImageURL = $ArrayConfig['URLs']['Software'].$TMPImageURL;

		$Content = str_replace('src="'.$TMPImageURL.'"', 'src="'.$TMPCorrectImageURL.'"', $Content);
		}

	preg_match_all("/src\=\"(.*)\/data\/editors\/fckeditor(.*)\"/iU", $Content, $ArrayMatches, PREG_SET_ORDER);
	foreach ($ArrayMatches as $EachMatch)
		{
		$TMPImageURL = $EachMatch[1].'/data/editors/fckeditor'.$EachMatch[2];
		$TMPCorrectImageURL = $ArrayConfig['URLs']['Software'].$TMPImageURL;

		$Content = str_replace('src="'.$TMPImageURL.'"', 'src="'.$TMPCorrectImageURL.'"', $Content);
		}

	return $Content;
	}
	
//########################################################################################
// Records bounce activity to campaign statistics
function RecordCampaignBounceActivity($CampaignID, $CampaignStatisticsID, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists	= false;
	// Initialize variables - FINISHED
	
	// Check if campaign and campaign statistics exists - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
			
		if ($TotalFound != 1)
			{
			$ErrorExists = true;
			}
		}
	// Check if campaign and campaign statistics exists - FINISHED

	// Record the activity - STARTED
	if (($ErrorExists == false) && ($TotalFound > 0))
		{
		// Retrieve the activity log for the campaign - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_tracks WHERE RelCampaignStatisticsID='".$CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayTrack = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$ArrayTrack = array();
			}
		// Retrieve the activity log for the campaign - FINISHED
		
		$ArrayFieldnValues = array(
									"TrackID"						=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TrackID'] : ''),
									"RelCampaignID"					=>	$CampaignID,
									"RelCampaignStatisticsID"		=>	$CampaignStatisticsID,
									"TotalBounces"					=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TotalBounces'] + 1 : 1),
									"TotalUnsubscriptions"			=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TotalUnsubscriptions'] : 0),
									);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		if (count($ArrayTrack) > 0)
			{
			$SQLQuery = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."campaigns_tracks (".$FieldList.") VALUES (".$ValueList.")";
			}
		else
			{
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."campaigns_tracks (".$FieldList.") VALUES (".$ValueList.")";
			}
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Record the activity - FINISHED

	return true;
	}
	
//########################################################################################
// Records unsubscription activity to campaign statistics
function RecordCampaignUnSubscriptionActivity($CampaignID, $CampaignStatisticsID, $MemberID, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists	= false;
	// Initialize variables - FINISHED
	
	// Check if campaign and campaign statistics exists - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
			
		if ($TotalFound != 1)
			{
			$ErrorExists = true;
			}
		}
	// Check if campaign and campaign statistics exists - FINISHED

	// Record the activity - STARTED
	if (($ErrorExists == false) && ($TotalFound > 0))
		{
		// Retrieve the activity log for the campaign - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_tracks WHERE RelCampaignStatisticsID='".$CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayTrack = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$ArrayTrack = array();
			}
		// Retrieve the activity log for the campaign - FINISHED
		
		$ArrayFieldnValues = array(
									"TrackID"						=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TrackID'] : ''),
									"RelCampaignID"					=>	$CampaignID,
									"RelCampaignStatisticsID"		=>	$CampaignStatisticsID,
									"TotalBounces"					=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TotalBounces'] : 0),
									"TotalUnsubscriptions"			=>	(count($ArrayTrack) > 0 ? $ArrayTrack['TotalUnsubscriptions'] + 1 : 1),
									);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		if (count($ArrayTrack) > 0)
			{
			$SQLQuery = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."campaigns_tracks (".$FieldList.") VALUES (".$ValueList.")";
			}
		else
			{
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."campaigns_tracks (".$FieldList.") VALUES (".$ValueList.")";
			}
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Record the activity - FINISHED

	// Record unsubscription more detailed - STARTED
	if (($ErrorExists == false) && ($TotalFound > 0))
		{
		$ArrayFieldnValues = array(
									"UnsubscriptionTrackID"			=>	'',
									"RelCampaignID"					=>	$CampaignID,
									"RelCampaignStatisticsID"		=>	$CampaignStatisticsID,
									"RelMemberID"					=>	$MemberID,
									"UnsubscriptionDate"			=>	date('Y-m-d H:i:s'),
									);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."campaigns_unsubscriptions (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}	
	// Record unsubscription more detailed - FINISHED

	return true;
	}

//########################################################################################
// Add administrator header and footer to the campaign contents
function AddAdministratorHeaderFooter($ArrayContent, $AdministratorID, $ArrayConfig)
	{
	// Retrieve administrator info - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$AdministratorID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdministrator = mysql_fetch_assoc($ResultSet);
	// Retrieve administrator info - FINISHED

	if ($ArrayContent['ContentType'] == 'Plain')
		{
		// Add Header - STARTED
		if ($ArrayAdministrator['HeaderPlain'] != '')
			{
			$ArrayContent['ContentMain'] = $ArrayAdministrator['HeaderPlain'].$ArrayContent['ContentMain'];
			}
		// Add Header - FINISHED

		// Add Footer - STARTED
		if ($ArrayAdministrator['FooterPlain'] != '')
			{
			$ArrayContent['ContentMain'] = $ArrayContent['ContentMain'].$ArrayAdministrator['FooterPlain'];
			}
		// Add Footer - FINISHED
		}
	elseif ($ArrayContent['ContentType'] == 'Html')
		{
		// Add Header - STARTED
		if (($ArrayAdministrator['HeaderHTML'] != '') && (strlen($ArrayAdministrator['HeaderHTML']) > 10))
			{
			// Locate <body> tag - STARTED
			preg_match_all('/<(body)(.*)>/iU', $ArrayContent['ContentMain'], $ArrayMatches, PREG_SET_ORDER);
			$TargetTag = $ArrayMatches[0][0];
			// Locate <body> tag - FINISHED

			$ArrayContent['ContentMain'] = str_replace($TargetTag, $TargetTag."\n".$ArrayAdministrator['HeaderHTML'], $ArrayContent['ContentMain']);
			}
		// Add Header - FINISHED

		// Add Footer - STARTED
		if (($ArrayAdministrator['FooterHTML'] != '') && (strlen($ArrayAdministrator['FooterHTML']) > 10))
			{
			// Locate </body> tag - STARTED
			preg_match_all('/<(\/body)(.*)>/iU', $ArrayContent['ContentMain'], $ArrayMatches, PREG_SET_ORDER);
			$TargetTag = $ArrayMatches[0][0];
			// Locate </body> tag - FINISHED

			$ArrayContent['ContentMain'] = str_replace($TargetTag, $ArrayAdministrator['FooterHTML']."\n".$TargetTag, $ArrayContent['ContentMain']);
			}
		// Add Footer - FINISHED
		}
	elseif ($ArrayContent['ContentType'] == 'Both')
		{
		// Add Header - STARTED
		if (($ArrayAdministrator['HeaderHTML'] != '') && (strlen($ArrayAdministrator['HeaderHTML']) > 10))
			{
			// Locate <body> tag - STARTED
			preg_match_all('/<(body)(.*)>/iU', $ArrayContent['ContentMain'], $ArrayMatches, PREG_SET_ORDER);
			$TargetTag = $ArrayMatches[0][0];
			// Locate <body> tag - FINISHED

			$ArrayContent['ContentMain'] = str_replace($TargetTag, $TargetTag."\n".$ArrayAdministrator['HeaderHTML'], $ArrayContent['ContentMain']);
			}
		if ($ArrayAdministrator['HeaderPlain'] != '')
			{
			$ArrayContent['ContentAlternate'] = $ArrayAdministrator['HeaderPlain'].$ArrayContent['ContentAlternate'];
			}
		// Add Header - FINISHED

		// Add Footer - STARTED
		if (($ArrayAdministrator['FooterHTML'] != '') && (strlen($ArrayAdministrator['FooterHTML']) > 10))
			{
			// Locate </body> tag - STARTED
			preg_match_all('/<(\/body)(.*)>/iU', $ArrayContent['ContentMain'], $ArrayMatches, PREG_SET_ORDER);
			$TargetTag = $ArrayMatches[0][0];
			// Locate </body> tag - FINISHED

			$ArrayContent['ContentMain'] = str_replace($TargetTag, $ArrayAdministrator['FooterHTML']."\n".$TargetTag, $ArrayContent['ContentMain']);
			}
		if ($ArrayAdministrator['FooterPlain'] != '')
			{
			$ArrayContent['ContentAlternate'] = $ArrayContent['ContentAlternate'].$ArrayAdministrator['FooterPlain'];
			}
		// Add Footer - FINISHED
		}

	return $ArrayContent;
	}

//########################################################################################
// Returns the content of the campaign
function CampaignContent($ArrayCampaign, $OverridePersonalization, $ArrayConfig, $ArrayLanguage)
	{
	
	// Retrieve campaign SMTP settings - STARTED
	$ArraySMTP = array();
	if ($ArrayCampaign['SendEngine'] == 'SMTP')
		{
		$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
		$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
		$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."' LIMIT 0,1";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TMPArraySMTP = mysql_fetch_assoc($ResultSet);

		$ArraySMTP = array(
						'Type'		=>	'SMTP',
						'Host'		=>	$TMPArraySMTP['ServerHost'],
						'Port'		=>	$TMPArraySMTP['ServerPort'],
						'Auth'		=>	$TMPArraySMTP['ServerAuth'],
						'Username'	=>	$TMPArraySMTP['ServerUsername'],
						'Password'	=>	$TMPArraySMTP['ServerPassword'],
						'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'MTA')
		{
		$ArraySMTP = array(
						'Type'		=>	'MTA',
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'QMail')
		{
		$ArraySMTP = array(
						'Type'		=>	'QMail',
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'Sendmail')
		{
		$ArraySMTP = array(
						'Type'		=>	'Sendmail',
						);
		}
	// Retrieve campaign SMTP settings - FINISHED

	// Retrieve attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayAttachments = array();

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayAttachments[] = array(
									"AttachmentID"		=>	$EachRow['AttachmentID'],
									"FileName"			=>	$EachRow['FileName'],
									"FileSize"			=>	$EachRow['FileSize'],
									);
		}
	// Retrieve attachments - FINISHED

	// Retrieve content - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentMainURL']));
			$ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($ArrayContent['ContentAlternateURL'] != '' && $ArrayContent['ContentAlternateURL'] != '_enteredContentAlternateURL_')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentAlternateURL']));
			$ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve content - FINISHED

	// Add administrator header and footer to the content - STARTED
		include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$ArrayContent = AddAdministratorHeaderFooter($ArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
	// Add administrator header and footer to the content - FINISHED

	// Retrieve from, reply to and return path emails - STARTED
	if ($ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);
	// Retrieve from, reply to and return path emails - FINISHED
	
	// Retrieve a random member account (if enabled) - STARTED
	if ($ArrayContent['Personalize'] == 'Yes')
		{
		// Retrieve the mail lists of the campaign and check if any outsourced list exists - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblLists ON tblAssign.RelMailListID = tblLists.MailListID WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblLists.SourceType='Outsourced - MySQL'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Retrieve the mail lists of the campaign and check if any outsourced list exists - FINISHED
		
		// If all lists are local sourced, then retrieve a random member - STARTED
		if ($TotalFound < 1)
			{
			// Retrieve mail lists - STARTED
			$ArrayMailLists = array();
			
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
			$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblMailLists.SourceType='Local'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayMailLists[]	=	$EachRow['MailListID'];
				}
			// Retrieve mail lists - FINISHED

			$SQLQuery  = "SELECT tblMembers.* FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
			$SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
			$SQLQuery .= "WHERE tblAssign.RelMailListID = '".$ArrayMailLists[0]."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblMembership WHERE RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet);
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailList WHERE MailListID='".$ArrayMembership['RelMailListID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblLists ON tblAssign.RelMailListID = tblLists.MailListID WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblLists.SourceType='Outsourced - MySQL' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet);

			// Connect to outsourced mail list database - STARTED
			mysql_close();
		
			$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
				mysql_select_db($ArrayMailList['OutSourceDBName']);
			// Connect to outsourced mail list database - FINISHED
			
				$SQLQueryPart = ($ArrayMailList['OutSourceDBWhereQuery'] != '' ? ' WHERE '.$ArrayMailList['OutSourceDBWhereQuery'] : '');
			$SQLQuery = "SELECT * FROM ".$ArrayMailList['OutSourceDBTable'].$SQLQueryPart." LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);

			// For outsourced members, the email address field may be different. Make a new field with "Email" name - STARTED
			$ArrayMember['Email']	=	$ArrayMember[$ArrayMailList['OutSourceDBEmailFieldName']];
			// For outsourced members, the email address field may be different. Make a new field with "Email" name - FINISHED
			
			// Connect to oemPro database - STARTED
			mysql_close();
		
				// Connect to oemPro MySQL server and database
				$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
				// Connect to outsourced MySQL list - FINISHED
			// Connect to oemPro database - FINISHED
			}
		// If all lists are local sourced, then retrieve a random member - FINISHED
		}
	// Retrieve a random member account (if enabled) - FINISHED

	// Check if the campaign owner administrator has a pre-defined return-path email address - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayCampaignAdministrator = mysql_fetch_assoc($ResultSet);
	// Check if the campaign owner administrator has a pre-defined return-path email address - FINISHED

	// Define email properties - STARTED
		// Define subject - STARTED
		$TMPSubject = $ArrayContent['Subject'];
		// Define subject - FINISHED
		
		// Define content - STARTED
		if (count($ArrayNewsletter) > 0)
			{
			// CMS based content
			$TMPPriority 			= $ArrayNewsletter['Priority'];
			$TMPCharSet				= $ArrayNewsletter['CharSet'];
			$TMPContentType			= $ArrayNewsletter['ContentType'];

			if ($TMPContentType == 'Html')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}
			elseif ($TMPContentType == 'Both')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				}
			elseif ($TMPContentType == 'Plain')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}

			// Add administrator header and footer to the content - STARTED
				include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
				$TMPArrayContent = array(
										"ContentMain"		=> $TMPContentMain,
										"ContentAlternate"	=> $TMPContentAlternate,
										"ContentType"		=> $TMPContentType,
										);
			$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
				$TMPContentMain = $TMPArrayContent['ContentMain'];
				$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
			// Add administrator header and footer to the content - FINISHED
			}
		else
			{
			// Traditional template or quick mode
			$TMPPriority 			= $ArrayContent['Priority'];
			$TMPCharSet				= $ArrayContent['CharSet'];
			$TMPContentType			= $ArrayContent['ContentType'];
			$TMPContentMain			= $ArrayContent['ContentMain'];
			$TMPContentAlternate	= $ArrayContent['ContentAlternate'];
			}
		// Define content - FINISHED

		// Run external plug-ins which are content specific - Start
		$ArrayPlugInReturn = RunPlugInContentProcesses($TMPContentMain, $TMPContentAlternate, array(), $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig);
			$TMPContentMain		=	$ArrayPlugInReturn[0];
			$TMPContentAlternate=	$ArrayPlugInReturn[1];
		// Run external plug-ins which are content specific - End

		// Process on email content (personalization, tracking links, tracking reads) - STARTED
		if (($ArrayContent['Personalize'] == 'Yes') && ($OverridePersonalization == false))
			{
				$ArrayPrefixes		=	array(
											"Sender",
											"Member",
											"Membership",
											"Campaign"
											);
				$ArrayInformation	=	array(
											$ArrayCampaignAdministrator,
											$ArrayMember,
											$ArrayMembership,
											$ArrayCampaign,
											);											
			$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
			}
	
		if ($OverridePersonalization == false)
			{
				$ArrayCampaignStatistics = array();
			$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
			$TMPContentMain			=	PersonalizeCampaignTags($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
			$TMPContentAlternate	=	PersonalizeCampaignTags($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
	
			// Perform global personalization - STARTED
			$TMPSubject				= PerformGlobalCampaignPersonalization($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			$TMPContentMain			= PerformGlobalCampaignPersonalization($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			$TMPContentAlternate	= PerformGlobalCampaignPersonalization($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
			// Perform global personalization - FINISHED
			}
		// Process on email content (personalization, tracking links, tracking reads) - FINISHED

	$ArrayEmailProperties = array();
		$ArrayEmailProperties	= array(
										"CampaignID"		=>	$ArrayCampaign['CampaignID'],
										"EmbedObjects"		=>	($ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
                                        "CampaignName"      =>  $ArrayCampaign['CampaignName'],
										"FromEmail"			=>	$ArrayFromEmail['EmailAddress'],
										"FromName"			=>	$ArrayFromEmail['Name'],
										"ReplyToEmail"		=>	$ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"		=>	$ArrayReplyToEmail['Name'],
										"ReturnPathEmail"	=>	($ArrayCampaignAdministrator['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayCampaignAdministrator['ReturnPathEmail']),
										"ToName"			=>	$ArrayMember['Email'],
										"ToEmail"			=>	$ArrayMember['Email'],
										"Priority"			=>	$TMPPriority,
										"CharSet"			=>	$TMPCharSet,
										"Subject"			=>	$TMPSubject,
										"ContentType"		=>	$TMPContentType,
										"ContentMain"		=>	$TMPContentMain,
										"ContentAlternate"	=>	$TMPContentAlternate,
										"Attachments"		=>	$ArrayAttachments,
										"SendEngines"		=>	$ArraySMTP,
										);
	// Define email properties - FINISHED

	return $ArrayEmailProperties;
	}


//########################################################################################
// Duplicates the campaign
function DuplicateCampaign($CampaignID, $ArrayTables, $Prefix, $ArrayConfig)
	{
	foreach ($ArrayTables as $EachTable)
		{
		if ($EachTable == 'campaigns')
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachTable." WHERE CampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				$ArrayRow = mysql_fetch_assoc($ResultSet);
	
				$ArrayRow['CampaignName'] = $Prefix." ".$ArrayRow['CampaignName'];
	
				$ArrayRow = array_slice($ArrayRow, 1);

				foreach ($ArrayRow as $Field=>$Value)
					{
					$ArrayRow[$Field] = mysql_escape_string($Value);
					}
				
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$NewCampaignID = mysql_insert_id();
				}
			}
		elseif ($EachTable == 'campaigns_schedule')
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachTable." WHERE RelCampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				$ArrayRow = mysql_fetch_assoc($ResultSet);
	
				$ArrayRow['RelCampaignID'] = $NewCampaignID;
				$ArrayRow['ScheduleType'] = 'Not scheduled';

				$ArrayRow = array_slice($ArrayRow, 1);

				foreach ($ArrayRow as $Field=>$Value)
					{
					$ArrayRow[$Field] = mysql_escape_string($Value);
					}
				
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$NewScheduleID = mysql_insert_id();
				}
			}
		elseif ($EachTable == 'campaigns_cms')
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachTable." WHERE RelCampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				$ArrayRow = mysql_fetch_assoc($ResultSet);
	
				$ArrayRow['RelCampaignID'] = $NewCampaignID;
				$OldContentCMSID = $ArrayRow['ContentCMSID'];
	
				$ArrayRow = array_slice($ArrayRow, 1);

				foreach ($ArrayRow as $Field=>$Value)
					{
					$ArrayRow[$Field] = mysql_escape_string($Value);
					}
	
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$NewContentCMSID = mysql_insert_id();
				}
			}
		elseif ($EachTable == 'campaigns_cms_refs')
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$OldContentCMSID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				while ($ArrayRow = mysql_fetch_assoc($ResultSet))
					{
					$ArrayRow['RelContentCMSID'] = $NewContentCMSID;
		
					$ArrayRow = array_slice($ArrayRow, 1);

					foreach ($ArrayRow as $Field=>$Value)
						{
						$ArrayRow[$Field] = mysql_escape_string($Value);
						}
		
					$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
					$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					}
				}
			}
		elseif ($EachTable == 'campaigns_attachments')
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachTable." WHERE RelCampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				while ($ArrayRow = mysql_fetch_assoc($ResultSet))
					{
					$OldAttachmentID = $ArrayRow['AttachmentID'];
					
					$ArrayRow['RelCampaignID'] = $NewCampaignID;
		
					$ArrayRow = array_slice($ArrayRow, 1);

					foreach ($ArrayRow as $Field=>$Value)
						{
						$ArrayRow[$Field] = mysql_escape_string($Value);
						}
		
					$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
					$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
						$NewAttachmentID = mysql_insert_id();
					
					// Copy attachments - STARTED
					$PathCopyFrom	= $ArrayConfig['Paths']['Data']."/campaign_attachments/".$CampaignID;
					$PathCopyTo		= $ArrayConfig['Paths']['Data']."/campaign_attachments/".$NewCampaignID;

					if (is_dir($PathCopyTo) == false)
						{
						mkdir($PathCopyTo,0777);
						}

					copy($PathCopyFrom.'/'.$OldAttachmentID.'.att', $PathCopyTo.'/'.$NewAttachmentID.'.att');
					// Copy attachments - FINISHED
					}
				}
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachTable." WHERE RelCampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet) > 0)
				{
				while ($ArrayRow = mysql_fetch_assoc($ResultSet))
					{
					$ArrayRow['RelCampaignID'] = $NewCampaignID;
		
					$ArrayRow = array_slice($ArrayRow, 1);

					foreach ($ArrayRow as $Field=>$Value)
						{
						$ArrayRow[$Field] = mysql_escape_string($Value);
						}
		
					$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix'].$EachTable." VALUES ('', '".implode("','", array_values($ArrayRow))."')";
					$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					}
				}
			}
		}
		
	return $ArrayCMSParts;
	}

function getCampaignDetails($CampaignID)
{
    $query = "SELECT * FROM `oemp_campaigns` WHERE `CampaignID` = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);

    $campaign = mysql_fetch_assoc($sql);
    
    $query = "SELECT * FROM `oemp_campaigns_maillists` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    while ($row = mysql_fetch_assoc($sql))
    {
        $campaign['mailists'][$row['RelMailListID']] = $row['RelMailListID'];    
    }
    
    $query = "SELECT * FROM `oemp_campaigns_schedule` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    $campaign['schedule'] = mysql_fetch_assoc($sql);    
    
    $query = "SELECT * FROM `oemp_campaigns_content` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    $campaign['content'] = mysql_fetch_assoc($sql);
    
    $query = "SELECT RelArchiveListID FROM `oemp_archive_list_campaigns` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    while ($row = mysql_fetch_assoc($sql))
    {
        $campaign['archive'][$row['RelArchiveListID']] = $row['RelArchiveListID'];    
    }
    
    $query = "SELECT * FROM `oemp_campaigns_social` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    $campaign['social'] = mysql_fetch_assoc($sql);
    
    $query = "SELECT RelRSSFeedID FROM `oemp_archive_rss_campaigns` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    while ($row = mysql_fetch_assoc($sql))
    {
        $campaign['rss'][$row['RelRSSFeedID']] = $row['RelRSSFeedID'];    
    }
    
    return $campaign;
}


//########################################################################################
// Retrieve the email addresses of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GetMaillistEmailAddresses()
{
    global $ArrayConfig;
	$ArrayEmails = array();
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE 1";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
    
    while ($EachRow = mysql_fetch_assoc($ResultSet))
    {
        $ArrayEmails[$EachRow['EmailAddressID']]['email'] = $EachRow['EmailAddress'];
        $ArrayEmails[$EachRow['EmailAddressID']]['name'] =	$EachRow['Name'];
    }
	// Retrieve the email addresses of the mail lists - FINISHED

	return $ArrayEmails;
}

//########################################################################################
// Retrieve the email addresses of administrator
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GetAdministratorEmailAddresses()
{
    global $ArrayConfig;
	$ArrayEmails = array();
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE 1";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
    
    while ($EachRow = mysql_fetch_assoc($ResultSet))
    {
        $ArrayEmails[$EachRow['EmailID']]['email'] = $EachRow['EmailAddress'];
        $ArrayEmails[$EachRow['EmailID']]['name'] =	$EachRow['Name'];
    }
	// Retrieve the email addresses of the mail lists - FINISHED

	return $ArrayEmails;
}

function getArchiveList()
{
    global $ArrayConfig;
    
    $SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_list WHERE 1";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
    
    while ($EachRow = mysql_fetch_assoc($ResultSet))
    {
        $ArrayEmails[$EachRow['ArchiveListID']] = $EachRow['ListName'];
    }
    
	return $ArrayEmails;
}

function getRssList()
{
    global $ArrayConfig;
    
    $SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_rss WHERE 1";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
    
    while ($EachRow = mysql_fetch_assoc($ResultSet))
    {
        $ArrayEmails[$EachRow['RSSFeedID']] = $EachRow['RSSFeedName'];
    }
    
	return $ArrayEmails;
}

function updateCampaign($data)
{
    global $ArrayConfig;

    if ($data['CampaignName'] != "")
    {
        $RelFromEmailID = substr($data['FromEmailID'], 1);
        $RelReplyToEmailID =  substr($data['ReplyToEmailID'], 1);
        $RelReturnPathEmailID = substr($data['ReturnPathEmailID'], 1);
        
        $FromEmailIDType = "Administrator";
        if ($data['FromEmailID']{0} == 'M')
        {
            $FromEmailIDType = 'MailList';
        }
        
        $ReplyToEmailIDType = "Administrator";
        if ($data['ReplyToEmailID']{0} == 'M')
        {
            $ReplyToEmailIDType = 'MailList';
        }
        
        $ReturnPathEmailIDType = "Administrator";
        if ($data['ReturnPathEmailID']{0} == 'M')
        {
            $ReturnPathEmailIDType = 'MailList';
        }
        
        $query = "UPDATE `oemp_campaigns` SET 
                                        `RelAdministratorID` = 1, 
                                        `Status` = 'Active', 
                                        `IntegrityCheck` = 'Ready', 
                                        `IntegrityLastStep` = 7, 
                                        `CampaignName` = '".addslashes($data['CampaignName'])."',
                                        `CampaignType` = 'Quick Campaign',
                                        `RelFromEmailID` = $RelFromEmailID,
                                        `FromEmailIDType` = '$FromEmailIDType',
                                        `RelReplyToEmailID` = $RelReplyToEmailID,
                                        `ReplyToEmailIDType` = '$ReplyToEmailIDType',
                                        `RelReturnPathEmailID` = $RelReturnPathEmailID,
                                        `ReturnPathEmailIDType` = '$ReturnPathEmailIDType',
                                        `SendEngine` = 'SMTP',
                                        `SendReportToAdmin` = 'No',
                                        `SendReportToClients` = 'No'
                        WHERE `CampaignID` = {$data['CampaignID']}";
                     
        ExecuteMySQLQuery($query, __LINE__, __LINE__);
        
        ExecuteMySQLQuery("DELETE FROM oemp_campaigns_schedule WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
        
        $data['ScheduleSendMaxInstance'] = ($data['ScheduleSendMaxInstance'] > 0) ? $data['ScheduleSendMaxInstance'] : 0;
               
        $query = "INSERT INTO `oemp_campaigns_schedule` SET 
                                        `RelCampaignID` = {$data['CampaignID']}, 
                                        `ScheduleType` = '{$data['ScheduleType']}', 
                                        `ScheduleSendDate` = '".date("Y-m-d H:i", strtotime($data['ScheduleSendDateYear']."-".$data['ScheduleSendDateMonth']."-".$data['ScheduleSendDateDay']." ".$data['ScheduleSendDateHour'].":".$data['ScheduleSendDateMinute'].":00"))."', 
                                        `ScheduleMinute` = '{$data['ScheduleSendDateMinute']}', 
                                        `ScheduleHour` = '{$data['ScheduleSendDateHour']}',
                                        `ScheduleDayOfMonth` = '{$data['ScheduleSendDateDay']}',
                                        `ScheduleDayOfWeek` = '{$data['ScheduleSendDateDay']}',
                                        `ScheduleMonth` = '{$data['ScheduleSendDateMonth']}',
                                        `ScheduleSendMaxInstance` = {$data['ScheduleSendMaxInstance']}";
                                        
        ExecuteMySQLQuery($query, __LINE__, __LINE__);
        
        ExecuteMySQLQuery("DELETE FROM oemp_campaigns_maillists WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
        
        if (!empty($data['MailListIDs']))
        {
            foreach($data['MailListIDs'] as $key=>$value)
            {
                $query = "INSERT INTO `oemp_campaigns_maillists` SET `RelCampaignID` = {$data['CampaignID']}, `RelMailListID` = $value";
                ExecuteMySQLQuery($query, __LINE__, __LINE__);
            }
        }
              
        ExecuteMySQLQuery("DELETE FROM oemp_campaigns_social WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
        
        $SocialPostLinkedin = 0;
        $SocialPostTwitter = 0;
        $SocialPostFacebook = 0;
                
        if (!empty($data['social_post']))
        {
            foreach ($data['social_post'] as $key=>$value)
            {
                ${'SocialPost'.ucwords($key)} = 1; 
            }
        }
        
        $query = "INSERT INTO `oemp_campaigns_social` SET 
                                        `RelCampaignID` = {$data['CampaignID']}, 
                                        `SocialPostLinkedin` = {$SocialPostLinkedin},  
                                        `SocialPostTwitter` = {$SocialPostTwitter}, 
                                        `SocialPostFacebook` = {$SocialPostFacebook}";                                       
        ExecuteMySQLQuery($query, __LINE__, __LINE__);
        
        ExecuteMySQLQuery("DELETE FROM oemp_campaigns_content WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
        
        $Personalize = 'Yes';
        $TrackReads = 'Yes';
        $TrackLinkClicks = 'Yes';
        
        $data['ContentAlternateURL'] = isset($data['ContentAlternateURL']) && $data['ContentAlternateURL'] != '_enteredContentAlternateURL_' ? $data['ContentAlternateURL'] : '';
        
        $query = "INSERT INTO `oemp_campaigns_content` SET 
                                        `RelCampaignID` = {$data['CampaignID']}, 
                                        `Subject` = '{$data['Subject']}', 
                                        `ContentMain` = '".trim($data['ContentMain'])."', 
                                        `ContentMainURL` = '".$data['ContentMainURL']."', 
                                        `ContentAlternate` = '".replace_all_urls($data['ContentAlternate'])."',
                                        `ContentAlternateURL` = '".$data['ContentAlternateURL']."',
                                        `ContentType` = '{$data['ContentType']}',
                                        `CharSet` = '{$data['CharSet']}',
                                        `Priority` = '3',
                                        `TrackLinkClicks` = '$TrackLinkClicks', 
                                        `TrackReads` = '$TrackReads',
                                        `Personalize` = '$Personalize',
                                        `EmbedObjects` = 'No'";
                                        
        ExecuteMySQLQuery($query, __LINE__, __LINE__);
              
        if (!empty($data['ArchiveID']) && ($data['ScheduleType'] == 'Send Now' ||$data['ScheduleType'] == 'One time scheduled'))
        {
            ExecuteMySQLQuery("DELETE FROM oemp_archive_list_campaigns WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
            foreach ($data['ArchiveID'] as $key=>$value)
            {
                $query = "INSERT INTO `oemp_archive_list_campaigns` SET `RelCampaignID` = {$data['CampaignID']}, `RelArchiveListID` = $value, `RelAdministratorID` = 1";
                ExecuteMySQLQuery($query, __LINE__, __LINE__);
            }
        }
       
        if (!empty($data['RssID']) && ($data['ScheduleType'] == 'Send Now' ||$data['ScheduleType'] == 'One time scheduled'))
        {
            ExecuteMySQLQuery("DELETE FROM oemp_archive_rss_campaigns WHERE `RelCampaignID` = {$data['CampaignID']}", __LINE__, __LINE__);
            
            foreach ($data['RssID'] as $key=>$value)
            {
                $query = "INSERT INTO `oemp_archive_rss_campaigns` SET `RelCampaignID` = {$data['CampaignID']}, `RelRSSFeedID` = $value, `RelAdministratorID` = 1";
                ExecuteMySQLQuery($query, __LINE__, __LINE__);
            }
        }
        
        $query = "SELECT * FROM oemp_campaigns_statistics WHERE RelCampaignID = {$data['CampaignID']}";
        $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
        
        if (mysql_num_rows($sql) == 0 && $data['ScheduleType'] == 'Send Now')
        {
            
            $query = "INSERT INTO `oemp_campaigns_statistics` SET 
                                        `RelCampaignID` = {$data['CampaignID']}, 
                                        `Sleeping` = 0, 
                                        `Status` = 'Pending',
                                        `StatusDescription` = '',
                                        `SendStartDateTime` = NOW()";
            ExecuteMySQLQuery($query, __LINE__, __LINE__) or die(mysql_error());   
        }

        $query = "SELECT * FROM oemp_social_statistics WHERE RelCampaignID = {$data['CampaignID']}";
        $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);
        
        if (mysql_num_rows($sql) == 0)
        {
            $query = "INSERT INTO `oemp_social_statistics` SET 
                                        `RelCampaignID` = {$data['CampaignID']}";
            ExecuteMySQLQuery($query, __LINE__, __LINE__) or die(mysql_error());   
        }
         
        
        header("Location: ./campaign_new.php?CampaignID={$data['CampaignID']}&saved=1#saved");
    }
    else
    {
        header("Location: ./campaign_new.php?CampaignID={$data['CampaignID']}&saved=2");
    }
    //echo $data['FromEmailID']{0};
}

function updateCampaignRules($data)
{
    $query = "INSERT INTO oemp_campaigns_rules SET
                          `RelCampaignID` = {$data['CampaignID']},
                          `Key` = '{$data['field']}',
                          `Operator1` = '{$data['operator1']}',
                          `Value1` = '{$data['value1']}',
                          `Boolean1` = '{$data['boolean1']}',
                          `Operator2` = '{$data['operator2']}',
                          `Value2` = '{$data['value2']}',
                          `Boolean2` = '{$data['boolean2']}',
                          `AdvancedSQLQuery` = ''";
                          
    ExecuteMySQLQuery($query, __LINE__, __LINE__);
    
    if ($data['CampaignTemplateID'] > 0)
    {
        header("Location: ./campaign_new.php?CampaignID={$data['CampaignID']}&CampaignTemplateID={$data['CampaignTemplateID']}");    
    }
    else
    {
        header("Location: ./campaign_new.php?CampaignID={$data['CampaignID']}&saved=1#saved");
    }
}

function getCampaignSocialDetails($CampaignID)
{
    $query = "SELECT * FROM `oemp_campaigns_social` WHERE RelCampaignID = $CampaignID";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);

    return mysql_fetch_assoc($sql);
}	

function getCampaignSocialData($CampaignID)
{
    $query = "SELECT * FROM `oemp_social` WHERE 1";
    $sql = ExecuteMySQLQuery($query, __LINE__, __LINE__);

    return mysql_fetch_assoc($sql);
}

function replace_all_urls($text)
{
	$elemente=preg_split('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\-:!&~;%\.]*(\?\S+)?)?)?)@', $text);
	
	preg_match_all('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\-:!&~;%\.]*(\?\S+)?)?)?)@', $text, $matches);
	
	$de_cautat=array();
	$de_inlocuit=array();
	
	foreach ($matches[0] as $cheie=>$valoare){

		$de_cautat[]=$valoare;

		$de_inlocuit[]='%LinkTrack='.$valoare.'%';
	}
	
	$text_nou.='';
	
	foreach ($elemente as $key=>$val){
		
		$text_nou.=$val.$de_inlocuit[$key];
	}

	$text_nou=str_replace("%%","%",$text_nou);
	$text_nou=str_replace("%LinkTrack=%LinkTrack=","%LinkTrack=",$text_nou);
	
	return $text_nou;
}
?>
