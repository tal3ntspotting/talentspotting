<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Optimizes all oemPro database tables
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function OptimizeDB($ArrayConfig, $SetupValue1, $SetupValue2, $SetupValue3, $SetupValue4, $SetupValue5, $SetupValue6, $SetupValue7, $SetupValue8, $SetupValue9, $SetupValue10)
	{
	$SQLQuery = "SHOW TABLES";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayTables = array();
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$Table = array_values($EachRow);
			$Table = $Table[0];

		if (substr($Table, 0, strlen($ArrayConfig['Database']['Prefix'])) == $ArrayConfig['Database']['Prefix'])
			{
			$ArrayTables[] = $Table;
			}
		}

	$SQLQuery = "OPTIMIZE TABLE ".implode(",", $ArrayTables);
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	return;	
	}

?>