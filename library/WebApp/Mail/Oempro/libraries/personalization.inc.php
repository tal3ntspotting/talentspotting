<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Performs conditional personalization
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ConditionalPersonalization($String, $ArrayPrefixes, $ArrayInformation)
	{
	// Find all conditional personalization tags - STARTED
	$TMPPattern = "/%\{(.*)\}%/iU";
	preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER);
	// Find all conditional personalization tags - FINISHED

	// Loop each condition - STARTED
	foreach ($ArrayMatches as $EachArrayFound)
		{
		$ConditionSet		= $EachArrayFound[1];
		$TMPConditionSet	= $ConditionSet;

		if (eregi(':EQUALS:', $TMPConditionSet) == true)
			{
			$ConditionLeft		= explode(':EQUALS:', $TMPConditionSet);
				$ConditionLeft	= $ConditionLeft[0];

				preg_match('/:EQUALS:(.*)\?\?/i', $TMPConditionSet, $ArrayConditionRightMatches);
			$ConditionRight 	= ($ArrayConditionRightMatches[1] == '' ? '' : $ArrayConditionRightMatches[1]);

			$ConditionOperator	= 'EQUALS';
			}
		elseif (eregi(':GREATER:', $TMPConditionSet) == true)
			{
			$ConditionLeft		= explode(':GREATER:', $TMPConditionSet);
				$ConditionLeft	= $ConditionLeft[0];

				preg_match('/:GREATER:(.*)\?\?/i', $TMPConditionSet, $ArrayConditionRightMatches);
			$ConditionRight 	= ($ArrayConditionRightMatches[1] == '' ? '' : $ArrayConditionRightMatches[1]);

			$ConditionOperator	= 'GREATER';
			}
		elseif (eregi(':SMALLER:', $TMPConditionSet) == true)
			{
			$ConditionLeft		= explode(':SMALLER:', $TMPConditionSet);
				$ConditionLeft	= $ConditionLeft[0];

				preg_match('/:SMALLER:(.*)\?\?/i', $TMPConditionSet, $ArrayConditionRightMatches);
			$ConditionRight 	= ($ArrayConditionRightMatches[1] == '' ? '' : $ArrayConditionRightMatches[1]);

			$ConditionOperator	= 'SMALLER';
			}

		$TMPConditionSet	= explode('??', $TMPConditionSet);
		$TMPConditionSet	= explode('::', $TMPConditionSet[1]);
		$ConditionTrue		= $TMPConditionSet[0];
		$ConditionFalse		= $TMPConditionSet[1];

		if ($ConditionOperator == 'EQUALS')
			{
			if ($ConditionLeft == $ConditionRight)
				{
				$String = str_replace($EachArrayFound[0], $ConditionTrue, $String);
				}
			else
				{
				$String = str_replace($EachArrayFound[0], $ConditionFalse, $String);
				}
			}
		elseif ($ConditionOperator == 'GREATER')
			{
			if ($ConditionLeft > $ConditionRight)
				{
				$String = str_replace($EachArrayFound[0], $ConditionTrue, $String);
				}
			else
				{
				$String = str_replace($EachArrayFound[0], $ConditionFalse, $String);
				}
			}
		elseif ($ConditionOperator == 'SMALLER')
			{
			if ($ConditionLeft < $ConditionRight)
				{
				$String = str_replace($EachArrayFound[0], $ConditionTrue, $String);
				}
			else
				{
				$String = str_replace($EachArrayFound[0], $ConditionFalse, $String);
				}
			}
		}
	// Loop each condition - FINISHED

	return $String;
	}

//########################################################################################
// Checks if the link is a system link such as forward, html version, unsubscribe, rate, etc.
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function IsSystemLink($Link, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$Result = false;
	// Initialize variables - FINISHED
	
	$ArraySystemLinks = array(
							'/member/index.php',
							'/html_version.php',
							'/unsubscribe.php',
							'/rate_campaign.php',
							'/forward_campaign.php'
							);

	foreach ($ArraySystemLinks as $EachLink)
		{
		if (substr($Link, 0, strlen($ArrayConfig['URLs']['Software'].$EachLink)) == $ArrayConfig['URLs']['Software'].$EachLink)
			{
			$Result = true;
			break;
			}
		}

	return $Result;
	}

//########################################################################################
// Finds all href and name parameteres in a link
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function FindAllLinksInHTML($String, $ArrayConfig, $ArrayLanguage)
	{
	   $String = stripslashes($String);
	// Initialize variables - STARTED
	$ArrayLinks			= array();
	// Initialize variables - FINISHED
	
	// Insert each <a> into new line - STARTED
	$NewString = str_replace("</a>", "</a>\n", $String);
	// Insert each <a> into new line - FINISHED
	
	// Find all links - STARTED
	$TMPPattern = "/<a(.*)>.*<\/a>/isU";
	preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);
	// Find all links - FINISHED
    
	// Find "href" and "name" parameters inside links - STARTED
	$TMPCounter = 0;
	foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
	{	    
		$SearchOn = $ArrayEachMatch[1];
        
        if (strpos($SearchOn, 'post_social.php') === false)
        {
            $TMPPattern = "/name=\"([^\r\n]*)\"/iU";
    		preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
    
    		$ArrayLinks[$TMPCounter]['Name']	= $ArraySubMatches[0][1];
    		$ArrayLinks[$TMPCounter]['FullName']= $ArraySubMatches[0][0];
    
    		$TMPPattern = "/href=\"([^\r\n]*)\"/isU";
    		preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
    
    		$ArrayLinks[$TMPCounter]['Link']	= $ArraySubMatches[0][1];
    		$ArrayLinks[$TMPCounter]['FullLink']= $ArraySubMatches[0][0];
    
    		$ArrayLinks[$TMPCounter]['AllLink']	= $ArrayEachMatch[0];
    
    		$TMPCounter++;    
        }
        
		
	}
	// Find "href" and "name" parameters inside links - FINISHED

	// Find map links - STARTED
	$TMPPattern = "/<area(.*)>/isU";
	preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);

	foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
		{
		$SearchOn = $ArrayEachMatch[1];
		$TMPPattern = "/name=\"([^\r\n]*)\"/iU";
		preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

		$ArrayLinks[$TMPCounter]['Name']	= $ArraySubMatches[0][1];
		$ArrayLinks[$TMPCounter]['FullName']= $ArraySubMatches[0][0];

		$TMPPattern = "/href=\"([^\r\n]*)\"/iU";
		preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);

		$ArrayLinks[$TMPCounter]['Link']	= $ArraySubMatches[0][1];
		$ArrayLinks[$TMPCounter]['FullLink']= $ArraySubMatches[0][0];

		$ArrayLinks[$TMPCounter]['AllLink']	= $ArrayEachMatch[0];

		$TMPCounter++;
		}
        
	// Find map links - FINISHED
	return $ArrayLinks;
	}
//########################################################################################
// Detect re-brandable opt-in/out links and personalize them
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RenderOptInOutLinksReBrandable($String, $EncryptedMailListMembershipID, $ArrayMembership, $ArrayMember, $ArrayMailList, $Type, $ArrayConfig, $ArrayLanguage)
	{
	if ($Type == 'OptIn')
		{
			$TMPPattern = "/%Link=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
				{
				foreach ($ArrayMatches as $EachMatch) 
					{
						
						$TMPPattern = "%Link=".$EachMatch[1]."%";
							$TMPWhichURL	  = base64_encode($EachMatch[1]);
						$TMPReplace = $ArrayConfig['URLs']['Software']."/optin_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Rbrnd=".$TMPWhichURL;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}

			$TMPPattern = "/%RejectLink=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
				{
				foreach ($ArrayMatches as $EachMatch) 
					{
						$TMPPattern = "%RejectLink=".$EachMatch[1]."%";
							$TMPWhichURL	  = base64_encode($EachMatch[1]);
						$TMPReplace = $ArrayConfig['URLs']['Software']."/optin_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Action=Rejected"."&Rbrnd=".$TMPWhichURL;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}
		}
	elseif ($Type == 'OptOut')
		{
			$TMPPattern = "/%Link=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
				{
				foreach ($ArrayMatches as $EachMatch) 
					{
						$TMPPattern = "%Link=".$EachMatch[1]."%";
							$TMPWhichURL	  = base64_encode($EachMatch[1]);
						$TMPReplace = $ArrayConfig['URLs']['Software']."/optout_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Rbrnd=".$TMPWhichURL;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}

			$TMPPattern = "/%RejectLink=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
				{
				foreach ($ArrayMatches as $EachMatch) 
					{
						$TMPPattern = "%RejectLink=".$EachMatch[1]."%";
							$TMPWhichURL	  = base64_encode($EachMatch[1]);
						$TMPReplace = $ArrayConfig['URLs']['Software']."/optout_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Action=Rejected"."&Rbrnd=".$TMPWhichURL;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}
		}

	return $String;
	}
//########################################################################################
// Perform global campaign personalization
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PerformGlobalCampaignPersonalization($String, $CampaignContentType, $ThisContentType, $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%')
	{
	// %GetContentOnce=.....% tag - STARTED
		$TMPPattern = "/%GetContentOnce=(.*)%/iU";
	if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
				$TMPPattern = "%GetContentOnce=".$EachMatch[1]."%";
				$TMPReplace = RetrieveRemoteContent($EachMatch[1], $ArrayConfig);
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		}
	// %GetContentOnce=.....% tag - FINISHED

	return $String;
	}
	
//########################################################################################
// Perform personalization of campaign tags such as read tracking, link tracking, etc.
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PersonalizeCampaignTagsForPublic($String, $CampaignContentType, $ThisContentType, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $PublicDisplay, $Tag = '%')
	{
	// Load required module - STARTED
	include_once("../system/libraries/membership.inc.php");
	include_once("./system/libraries/membership.inc.php");
	// Load required module - FINISHED

	// Set variables - STARTED
	$Demo		= ($IsDemo == true ? '1' : '0');
	// Set variables - FINISHED

	// Encrypt member ID - STARTED
	$EncryptedMemberID = Encrypt($ArrayMember['MemberID'], $ArrayConfig['Settings']['EncryptionKey']);
	// Encrypt member ID - FINISHED

	// Perform plug-in tag personalizations - STARTED
		$Mode = 'Campaigns';
	$String = PersonalizePlugInTags($String, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $Mode);
	// Perform plug-in tag personalizations - FINISHED

	// Perform global personalizations - STARTED
		// %GetContent=.....% tag - STARTED
			$TMPPattern = "/%GetContent=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%GetContent=".$EachMatch[1]."%";
					$TMPReplace = RetrieveRemoteContent($EachMatch[1], $ArrayConfig);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %GetContent=.....% tag - FINISHED

		// %Date% tag - STARTED
		$TMPPattern = "%Date%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["DateTime"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %Date% tag - FINISHED
        
        // %HumanDate% tag - STARTED
		$TMPPattern = "%HumanDate%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["HumanDate"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %Date% tag - FINISHED
		
		// %Date=....% tag - STARTED
			$TMPPattern = "/%Date=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%Date=".$EachMatch[1]."%";
					$TMPReplace = date($EachMatch[1]);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %Date=....% tag - FINISHED
		
		// %AccountArea% tag - STARTED
		if ($PublicDisplay == false)
			{
			$TMPPattern = "%AccountArea%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/member/index.php?EncryptedMemberID=".$EncryptedMemberID;
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%AccountArea%";
			$TMPReplace = '#';
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %AccountArea% tag - FINISHED

		// %HTMLVersion% tag - STARTED
		if ($PublicDisplay == false)
			{
			$TMPPattern = "%HTMLVersion%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/html_version.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".$ArrayMember['Email'];
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%HTMLVersion%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/html_version.php?ECID=".rawurlencode(base64_encode($ArrayCampaign['CampaignID']));
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %HTMLVersion% tag - FINISHED


		// %SubscribedMailListIDs% tag - STARTED
		$TMPPattern = "%CampaignMailListIDs%";
		$TMPReplace = implode(",", ReturnCampaignMailListIDs($ArrayCampaign, $ArrayConfig));
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %SubscribedMailListIDs% tag - FINISHED

		// %UnSubscribe% tag - STARTED
		if ($PublicDisplay == false)
			{
			$TMPPattern = "%UnSubscribe%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%UnSubscribe%";
			$TMPReplace = "#";
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %UnSubscribe% tag - FINISHED

		// %UnSubscribeAdmin% tag - STARTED
		if ($PublicDisplay == false)
			{
			$EncryptedAdministratorID = rawurlencode(base64_encode($ArrayCampaign['RelAdministratorID']));
			
			$TMPPattern = "%UnSubscribeAdmin%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?EAID=".$EncryptedAdministratorID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%UnSubscribeAdmin%";
			$TMPReplace = "#";
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %UnSubscribeAdmin% tag - FINISHED

		// %UnSubscribe=.....% tag - STARTED
			$TMPPattern = "/%UnSubscribe=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
				if ($PublicDisplay == false)
					{
						$TMPPattern = "%UnSubscribe=".$EachMatch[1]."%";
						$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&MailLists=".$EachMatch[1]."&Email=".base64_encode($ArrayMember['Email']);
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				elseif ($PublicDisplay == true)
					{
						$TMPPattern = "%UnSubscribe=".$EachMatch[1]."%";
						$TMPReplace = '#';
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}
			}
		// %UnSubscribe=.....% tag - FINISHED

		// %UnSubscribeCustom=X,Y% tag - STARTED
			$TMPPattern = "/%UnSubscribeCustom=\[(.*)](.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
				$TMPResultScreenSuccess	= explode(",", trim($EachMatch[2],','));
					$TMPResultScreenFailure	= rawurlencode(base64_encode($TMPResultScreenSuccess[1]));
					$TMPResultScreenSuccess = rawurlencode(base64_encode($TMPResultScreenSuccess[0]));
				$TMPResultMailLists = $EachMatch[1];
				if ($PublicDisplay == false)
					{
						$TMPPattern = $EachMatch[0];
						$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email'])."&CRSSID=".$TMPResultScreenSuccess."&CRFSID=".$TMPResultScreenFailure."&MailLists=".$TMPResultMailLists;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				elseif ($PublicDisplay == true)
					{
						$TMPPattern = $EachMatch[0];
						$TMPReplace = '#';
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}
			}
		// %UnSubscribeCustom=X,Y% tag - FINISHED

		// %Rate% tag - STARTED
		if ($PublicDisplay == false)
			{
			$TMPPattern = "%Rate%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/rate_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%Rate%";
			$TMPReplace = "#";
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %Rate% tag - FINISHED
		
		// %ForwardTo% tag - STARTED
		if ($PublicDisplay == false)
			{
			$TMPPattern = "%ForwardTo%";
			$TMPReplace = $ArrayConfig['URLs']['Software']."/forward_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		elseif ($PublicDisplay == true)
			{
			$TMPPattern = "%ForwardTo%";
			$TMPReplace = "#";
			$String = str_replace($TMPPattern, $TMPReplace, $String);
			}
		// %ForwardTo% tag - FINISHED
		
		//%ForwardTo=% tag - STARTED
		$TMPPattern = "/%ForwardTo=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
				$TMPPattern = "%ForwardTo=".$EachMatch[1]."%";
				$TMPWhichURL = base64_encode($EachMatch[1]);
					
				if ($PublicDisplay == false)
					{
					$TMPReplace = $ArrayConfig['URLs']['Software']."/forward_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email'])."&URL=".$TMPWhichURL;
					}
				else
					{
					$TMPReplace="#";
					}
							
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		//%ForwardTo=% tag - FINISHED
		
			
	// Perform global personalizations - FINISHED

	// Perform content type specific personalizations (ex: read tracking, link tracking) - STARTED
	if (($CampaignContentType == 'Html') || ($CampaignContentType == "Both"))
		{
		if ($ThisContentType == 'Subject')
			{
			}
		elseif ($ThisContentType == 'Main')
			{
			// link tracking: %LinkTrack=.....% - STARTED
			if (($ArrayContent['TrackLinkClicks'] == 'Yes') && ($PublicDisplay == false))
				{
				$ArrayLinks = FindAllLinksInHTML($String, $ArrayConfig, $ArrayLanguage);

				foreach ($ArrayLinks as $EachIndex=>$ArrayEachLink)
					{
					if ((substr($ArrayEachLink['Link'], 0, 1) != '#') && (substr($ArrayEachLink['Link'], 0, 7) != 'mailto:'))
						{
						// Do not track system links such as unsubscription, forward, rate, html version, account area - STARTED
						if (IsSystemLink($ArrayEachLink['Link'], $ArrayConfig) == true)
							{
							continue;
							}
						// Do not track system links such as unsubscription, forward, rate, html version, account area - FINISHED
						
							 $TMPLinkName		= rawurlencode(base64_encode(rawurldecode($ArrayEachLink['Name'])));
			                 $TMPPattern 	= $ArrayEachLink['FullLink'];
							 $TMPWhichURL	= base64_encode($ArrayEachLink['Link']);
							 $TMPReplace = "href=\"".$ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&Name=".$TMPLinkName."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email'])."\"";
                             $TMPAllLink = str_replace($TMPPattern, $TMPReplace, $ArrayEachLink['AllLink']);

							$TMPPattern 	= $ArrayEachLink['AllLink'];
							$TMPReplace 	= $TMPAllLink;
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
				}
			else
				{
					$TMPPattern = "/%LinkTrack=(.*)%/iU";
				if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
					{
					foreach ($ArrayMatches as $EachMatch) 
						{
							$TMPPattern = "%LinkTrack=".$EachMatch[1]."%";
								$TMPWhichURL	  = base64_encode($EachMatch[1]);
							$TMPReplace = $ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email']);
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
				}
			// link tracking: %LinkTrack=.....% - FINISHED
			
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignatureHTML'];
				}
			// oemPro signature - FINISHED

			// read tracking - STARTED
			if (($ArrayContent['TrackReads'] == 'Yes') && ($PublicDisplay == false))
				{
				$ReadTrackURL = '<img src="'.$ArrayConfig['URLs']['Software'].'/read.php?EncryptedMemberID='.$EncryptedMemberID.'&CampaignID='.$ArrayCampaign['CampaignID'].'&CampaignStatisticsID='.$ArrayCampaignStatistics['CampaignStatisticsID'].'&Demo='.$Demo.'&Email='.base64_encode($ArrayMember['Email']).'" width="10" height="10" alt="---">';
				$NewString = str_replace('</body>', "\n".$ReadTrackURL."\n\n</body>", $String);
				if ($NewString == $String)
					{
					$String .= "\n\n".$ReadTrackURL;
					}
				else
					{
					$String = $NewString;
					unset($NewString);
					}

				}
			// read tracking - FINISHED
			}
		elseif ($ThisContentType == 'Alternate')
			{
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignaturePlain'];
				}
			// oemPro signature - FINISHED
			}
		}
	elseif ($CampaignContentType == 'Plain')
		{
		if ($ThisContentType == 'Subject')
			{
			}
		elseif ($ThisContentType == 'Main')
			{
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignaturePlain'];
				}
			// oemPro signature - FINISHED
			}
		elseif ($ThisContentType == 'Alternate')
			{
			// No alternate content
			}
		}
	// Perform content type specific personalizations (ex: read tracking, link tracking) - FINISHED
	
	return $String;
	}
	
	
	
//########################################################################################
// Perform personalization of plug-in tags
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PersonalizePlugInTags($String, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $Mode)
	{
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."plugins_tags AS tblTags ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."plugins AS tblPlugIns ";
	$SQLQuery .= "ON tblTags.RelPlugInID = tblPlugIns.PlugInID ";
	$SQLQuery .= "WHERE (tblTags.AvailableFor='".$Mode."' OR tblTags.AvailableFor='All')";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		if (file_exists($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']) == true)
			{
			include_once($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']);
			$String = call_user_func($EachRow['FunctionName'], $String, $ArrayConfig, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics);
			}
		}

	return $String;
	}
	
//########################################################################################
// Perform personalization of system emails such as auto responders (for unsubscription link, etc.)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PersonalizeTagsForSystemEmails($String, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayEmailInfo, $ContentPart, $EmailType, $Tag = '%')
	{
	// Load required module - STARTED
	include_once("../system/libraries/membership.inc.php");
	include_once("./system/libraries/membership.inc.php");
	// Load required module - FINISHED

	// Set variables - STARTED
	$Demo		= 0;
	// Set variables - FINISHED

	// Encrypt member ID - STARTED
	$EncryptedMemberID = Encrypt($ArrayMember['MemberID'], $ArrayConfig['Settings']['EncryptionKey']);

	// Encrypt member ID - FINISHED

	// Perform plug-in tag personalizations - STARTED
		$Mode 						= 'System Emails';
		$ArrayCampaign 				= array();
		$ArrayCampaignStatistics 	= array();
	$String = PersonalizePlugInTags($String, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $Mode);
	// Perform plug-in tag personalizations - FINISHED

	// Perform global personalizations - STARTED
		// %GetContent=.....% tag - STARTED
			$TMPPattern = "/%GetContent=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
    			foreach ($ArrayMatches as $EachMatch) 
    			{
    				$TMPPattern = "%GetContent=".$EachMatch[1]."%";
    				$TMPReplace = RetrieveRemoteContent($EachMatch[1], $ArrayConfig);
                    $String = str_replace($TMPPattern, $TMPReplace, $String);
    			}
			}
		// %GetContent=.....% tag - FINISHED
        
		// %Date% tag - STARTED
		$TMPPattern = "%Date%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["DateTime"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %Date% tag - FINISHED
		
        // %HumanDate% tag - STARTED
		$TMPPattern = "%HumanDate%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["HumanDate"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
        
		// %Date=....% tag - STARTED
			$TMPPattern = "/%Date=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%Date=".$EachMatch[1]."%";
					$TMPReplace = date($EachMatch[1]);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %Date=....% tag - FINISHED
		
		// %AccountArea% tag - STARTED
		$TMPPattern = "%AccountArea%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/member/index.php?EncryptedMemberID=".$EncryptedMemberID;
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %AccountArea% tag - FINISHED
		
		// %UnSubscribe% tag - STARTED
		$TMPPattern = "%UnSubscribe%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %UnSubscribe% tag - FINISHED

		// %UnSubscribeAdmin% tag - STARTED
		$EncryptedAdministratorID = rawurlencode(base64_encode($ArrayCampaign['RelAdministratorID']));

		$TMPPattern = "%UnSubscribeAdmin%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?EAID=".$EncryptedAdministratorID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %UnSubscribeAdmin% tag - FINISHED

		// %UnSubscribe=.....% tag - STARTED
			$TMPPattern = "/%UnSubscribe=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%UnSubscribe=".$EachMatch[1]."%";
					$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&MailLists=".$EachMatch[1]."&Email=".base64_encode($ArrayMember['Email']);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %UnSubscribe=.....% tag - FINISHED
        
		// %UnSubscribeCustom=X,Y% tag - STARTED
			$TMPPattern = "/%UnSubscribeCustom=\[(.*)](.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
				$TMPResultScreenSuccess	= explode(",", trim($EachMatch[2],','));
					$TMPResultScreenFailure	= rawurlencode(base64_encode($TMPResultScreenSuccess[1]));
					$TMPResultScreenSuccess = rawurlencode(base64_encode($TMPResultScreenSuccess[0]));
				$TMPResultMailLists = $EachMatch[1];
					$TMPPattern = $EachMatch[0];
					$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email'])."&CRSSID=".$TMPResultScreenSuccess."&CRFSID=".$TMPResultScreenFailure."&MailLists=".$TMPResultMailLists;
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %UnSubscribeCustom=X,Y% tag - FINISHED
        
		// link tracking: %LinkTrack=.....% - STARTED
		if ($ArrayConfig['Settings']['TrackSystemEmailLinkClicks'] == 'Yes')
			{
			$ArrayLinks = FindAllLinksInHTML($String, $ArrayConfig, $ArrayLanguage);

			foreach ($ArrayLinks as $EachIndex=>$ArrayEachLink)
				{
				if ((substr($ArrayEachLink['Link'], 0, 1) != '#') && (substr($ArrayEachLink['Link'], 0, 7) != 'mailto:'))
					{
					// Do not track system links such as unsubscription, forward, rate, html version, account area - STARTED
					if (IsSystemLink($ArrayEachLink['Link'], $ArrayConfig) == true)
						{
						continue;
						}
					// Do not track system links such as unsubscription, forward, rate, html version, account area - FINISHED

						$TMPLinkName		= rawurlencode(base64_encode(rawurldecode($ArrayEachLink['Name'])));
							$TMPPattern 	= $ArrayEachLink['FullLink'];
							$TMPWhichURL	= base64_encode($ArrayEachLink['Link']);
						$TMPReplace = "href=\"".$ArrayConfig['URLs']['Software']."/link_generic.php?URL=".$TMPWhichURL."&Name=".$TMPLinkName."&EncryptedMemberID=".$EncryptedMemberID."&AutoResponderID=".$ArrayEmailInfo['AutoResponderID']."&EmailID=".$ArrayEmailInfo['EmailID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email'])."\"";
					$TMPAllLink = str_replace($TMPPattern, $TMPReplace, $ArrayEachLink['AllLink']);

						$TMPPattern 	= $ArrayEachLink['AllLink'];
						$TMPReplace 	= $TMPAllLink;
					$String = str_replace($TMPPattern, $TMPReplace, $String);
					}
				}
			}
		// link tracking: %LinkTrack=.....% - FINISHED
		
		// read tracking - STARTED
		if (($ArrayConfig['Settings']['TrackSystemEmailOpens'] == 'Yes') && ($ContentPart == 'Main') && (($ArrayEmailInfo['ContentType'] == 'Html') || ($ArrayEmailInfo['ContentType'] == 'Both')))
			{
			$ReadTrackURL = '<img src="'.$ArrayConfig['URLs']['Software'].'/read_generic.php?EncryptedMemberID='.$EncryptedMemberID.'&AutoResponderID='.$ArrayEmailInfo['AutoResponderID'].'&EmailID='.$ArrayEmailInfo['EmailID'].'&Demo='.$Demo.'&Email='.$ArrayMember['Email'].'" width="10" height="10" alt="---">';
            $String = str_replace("</body>", "\n{$ReadTrackURL}\n\n</body>", $String);
			}
		// read tracking - FINISHED
	// Perform global personalizations - FINISHED

	return $String;
	}

	//########################################################################################
	// Perform personalization for outsourced members
	// ---
	// Void		IN		N/A				- N/A
	// Void		OUT		N/A				- N/A
	// ---
	function PersonalizeOutsourcedMember($String, $ArrayPrefixes, $ArrayInformation, $ArrayConfig)
		{
		// Get Maillist ID array - STARTED
		$RelCampaignID=$ArrayInformation[2]["RelCampaignID"];

		$ArrayMailListIDs = array();

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$RelCampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListIDs[] = $EachRow['RelMailListID'];
			}
		// Get Maillist ID array - FINISHED	
							
		// Check if it is outsourced then go - STARTED
		if($ArrayInformation["1"]["Source"]=="Outsourced")
			{
			$TMPPattern = "/%Member:(.*)%/iU";
			preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER);

			$ArrayOutsourcedDbFields= array();

			foreach ($ArrayMatches as $key => $value)
				{
				$ArrayOutsourcedDbFields[]=$value[1];
				}
			
			//GET DB Fields - STARTED
			$ArrayOutsourcedDbFields = array_unique($ArrayOutsourcedDbFields);
			//GET DB Fields - FINISHED
			}
		// Check if it is outsourced then go - FINISHED
			
		// Connect Out DB and get Fields - STARTED
		$ArrayMailList = array();

		foreach ($ArrayMailListIDs as $key => $EachMailListID) 
			{
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachMailListID."' ";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayMailList["OutSourceDBHost"] 		= $EachRow['OutSourceDBHost'];
				$ArrayMailList["OutSourceDBUsername"] 	= $EachRow['OutSourceDBUsername'];
				$ArrayMailList["OutSourceDBPassword"] 	= $EachRow['OutSourceDBPassword'];
				$ArrayMailList["OutSourceDBTable"] 		= $EachRow['OutSourceDBTable'];
				$ArrayMailList["OutSourceDBName"] 		= $EachRow['OutSourceDBName'];
				$ArrayMailList["OutSourceDBEmailFieldName"] = $EachRow['OutSourceDBEmailFieldName'];
				}	
						
			// Connect to outsourced mail list database - STARTED
			mysql_close();

			$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
				mysql_select_db($ArrayMailList['OutSourceDBName']);
			// Connect to outsourced mail list database - FINISHED

			$SQLQuery = "SELECT * FROM ".$ArrayMailList['OutSourceDBTable']." WHERE ".$ArrayMailList["OutSourceDBEmailFieldName"]."='".$ArrayInformation[2]["Email"]."'  LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
			$ArrayOutsourcedMember = mysql_fetch_assoc($ResultSet);

			$ArrayCustomField = array();

			foreach ($ArrayOutsourcedDbFields as $key => $EachCustomField) 
				{
				$ArrayCustomField[$EachCustomField] = $ArrayOutsourcedMember[$EachCustomField];
				}	

			// Connect to oemPro database - STARTED
			mysql_close();
			// Connect to oemPro MySQL server and database
			$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
			// Connect to outsourced MySQL list - FINISHED
			// Connect to oemPro database - FINISHED	
						
			//Replace String with ArrayCustomField - STARTED					
			foreach ($ArrayCustomField as $key => $Value) 
				{
				$Replacement="%Member:".$key."%";
			
				$String=str_replace($Replacement,$Value,$String);
				}						
			//Replace String with ArrayCustomField - FINISHED
			}
		// Connect Out DB and get Fields - STARTED			

		return $String;
		}

	//########################################################################################


//########################################################################################
// Perform personalization
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function Personalize($String, $ArrayPrefixes, $ArrayInformation, $Tag = '%')
	{
	
	$TMPCounter = 0;

	// Personalize the string according to defined information - STARTED
	foreach ($ArrayPrefixes as $Prefix)
		{
		// Perform personalization - STARTED
		foreach ($ArrayInformation[$TMPCounter] as $Key=>$Val)
			{
			$String = str_replace($Tag.$Prefix.":".$Key.$Tag, $Val, $String);
			}
		// Perform personalization - FINISHED

		$TMPCounter++;
		}
	// Personalize the string according to defined information - FINISHED

	// Perform global personalizations - STARTED
		// %GetContent=.....% tag - STARTED
			$TMPPattern = "/%GetContent=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%GetContent=".$EachMatch[1]."%";
					$TMPReplace = RetrieveRemoteContent($EachMatch[1], $ArrayConfig);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %GetContent=.....% tag - FINISHED
	// Perform global personalizations - FINISHED

	// Conditional personalization - STARTED
	$String = ConditionalPersonalization($String, $ArrayPrefixes, $ArrayInformation);
	// Conditional personalization - FINISHED


	return $String;
	}

//########################################################################################
// Perform personalization of campaign tags such as read tracking, link tracking, etc.
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PersonalizeCampaignTags($String, $CampaignContentType, $ThisContentType, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%')
	{
	// Load required module - STARTED
	include_once("../system/libraries/membership.inc.php");
	include_once("./system/libraries/membership.inc.php");
	// Load required module - FINISHED

	// Set variables - STARTED
	$Demo		= ($IsDemo == true ? '1' : '0');
	// Set variables - FINISHED

	// Encrypt member ID - STARTED

	$EncryptedMemberID = Encrypt($ArrayMember['MemberID'], $ArrayConfig['Settings']['EncryptionKey']);	

	// Encrypt member ID - FINISHED

	// Perform plug-in tag personalizations - STARTED
	$Mode = 'Campaigns';
	$String = PersonalizePlugInTags($String, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $Mode);
	// Perform plug-in tag personalizations - FINISHED

	// Perform global personalizations - STARTED
		// %GetContent=.....% tag - STARTED
			$TMPPattern = "/%GetContent=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%GetContent=".$EachMatch[1]."%";
					$TMPReplace = RetrieveRemoteContent($EachMatch[1], $ArrayConfig);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %GetContent=.....% tag - FINISHED

		// %Date% tag - STARTED
		$TMPPattern = "%Date%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["DateTime"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %Date% tag - FINISHED
        
        // %HumanDate% tag - STARTED
		$TMPPattern = "%HumanDate%";
		$TMPReplace = date($ArrayLanguage["Config"]["DateFormat"]["HumanDate"]);
		$String = str_replace($TMPPattern, $TMPReplace, $String);

		// %Date=....% tag - STARTED
			$TMPPattern = "/%Date=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%Date=".$EachMatch[1]."%";
					$TMPReplace = date($EachMatch[1]);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %Date=....% tag - FINISHED
		
		// %AccountArea% tag - STARTED
		$TMPPattern = "%AccountArea%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/member/index.php?EncryptedMemberID=".$EncryptedMemberID;
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %AccountArea% tag - FINISHED

		// %HTMLVersion% tag - STARTED
		$TMPPattern = "%HTMLVersion%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/html_version.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode(rawurlencode($ArrayMember['Email']));
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %HTMLVersion% tag - FINISHED

		// %SubscribedMailListIDs% tag - STARTED
		$TMPPattern = "%CampaignMailListIDs%";
		$TMPReplace = implode(",", ReturnCampaignMailListIDs($ArrayCampaign, $ArrayConfig));
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %SubscribedMailListIDs% tag - FINISHED

		// %UnSubscribe% tag - STARTED
		$TMPPattern = "%UnSubscribe%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %UnSubscribe% tag - FINISHED

		// %UnSubscribeAdmin% tag - STARTED
		$EncryptedAdministratorID = rawurlencode(base64_encode($ArrayCampaign['RelAdministratorID']));

		$TMPPattern = "%UnSubscribeAdmin%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?EAID=".$EncryptedAdministratorID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email']);
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %UnSubscribeAdmin% tag - FINISHED

		// %UnSubscribe=.....% tag - STARTED
			$TMPPattern = "/%UnSubscribe=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
					$TMPPattern = "%UnSubscribe=".$EachMatch[1]."%";
					$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&MailLists=".$EachMatch[1]."&Email=".base64_encode($ArrayMember['Email']);
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %UnSubscribe=.....% tag - FINISHED

		// %UnSubscribeCustom=X,Y% tag - STARTED
			$TMPPattern = "/%UnSubscribeCustom=\[(.*)](.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
            foreach ($ArrayMatches as $EachMatch) 
				{
				$TMPResultScreenSuccess	= explode(",", trim($EachMatch[2],','));
					$TMPResultScreenFailure	= rawurlencode(base64_encode($TMPResultScreenSuccess[1]));
					$TMPResultScreenSuccess = rawurlencode(base64_encode($TMPResultScreenSuccess[0]));
				$TMPResultMailLists = $EachMatch[1];
					$TMPPattern = $EachMatch[0];
					$TMPReplace = $ArrayConfig['URLs']['Software']."/unsubscribe.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".base64_encode($ArrayMember['Email'])."&CRSSID=".$TMPResultScreenSuccess."&CRFSID=".$TMPResultScreenFailure."&MailLists=".$TMPResultMailLists;
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		// %UnSubscribeCustom=X,Y% tag - FINISHED
		
		// %Rate% tag - STARTED
		$TMPPattern = "%Rate%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/rate_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".$ArrayMember['Email'];
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %Rate% tag - FINISHED
		
		// %ForwardTo% tag - STARTED
		$TMPPattern = "%ForwardTo%";
		$TMPReplace = $ArrayConfig['URLs']['Software']."/forward_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".$ArrayMember['Email'];
		$String = str_replace($TMPPattern, $TMPReplace, $String);
		// %ForwardTo% tag - FINISHED

		//%ForwardTo=% tag - STARTED
		$TMPPattern = "/%ForwardTo=(.*)%/iU";
		if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
			{
			foreach ($ArrayMatches as $EachMatch) 
				{
				$TMPPattern = "%ForwardTo=".$EachMatch[1]."%";
				$TMPWhichURL	  = base64_encode($EachMatch[1]);
					
				if ($PublicDisplay == false)
					{
					$TMPReplace = $ArrayConfig['URLs']['Software']."/forward_campaign.php?CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&EncryptedMemberID=".$EncryptedMemberID."&Email=".$ArrayMember['Email']."&URL=".$TMPWhichURL;
					}
				else
					{
					$TMPReplace="#";
					}
							
				$String = str_replace($TMPPattern, $TMPReplace, $String);
				}
			}
		//%ForwardTo=% tag - FINISHED

	// Perform global personalizations - FINISHED

	// Perform content type specific personalizations (ex: read tracking, link tracking) - STARTED
	if (($CampaignContentType == 'Html') || ($CampaignContentType == "Both"))
		{
		  
		if ($ThisContentType == 'Subject')
			{
			}
		elseif ($ThisContentType == 'Main')
			{
			
			// link tracking: %LinkTrack=.....% - STARTED
			if ($ArrayContent['TrackLinkClicks'] == 'Yes')
				{
				    
				$ArrayLinks = FindAllLinksInHTML($String, $ArrayConfig, $ArrayLanguage);

				foreach ($ArrayLinks as $EachIndex=>$ArrayEachLink)
					{
					if ((substr($ArrayEachLink['Link'], 0, 1) != '#') && (substr($ArrayEachLink['Link'], 0, 7) != 'mailto:'))
						{
						// Do not track system links such as unsubscription, forward, rate, html version, account area - STARTED
						if (IsSystemLink($ArrayEachLink['Link'], $ArrayConfig) == true)
							{
							continue;
							}
						// Do not track system links such as unsubscription, forward, rate, html version, account area - FINISHED
						
								$TMPLinkName	= rawurlencode(base64_encode(rawurldecode($ArrayEachLink['Name'])));
								$TMPPattern 	= $ArrayEachLink['FullLink'];
								$TMPWhichURL	= base64_encode($ArrayEachLink['Link']);
								$TMPReplace = "href=\"".$ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&Name=".$TMPLinkName."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email'])."\"";
								$TMPAllLink = str_replace($TMPPattern, $TMPReplace, $ArrayEachLink['AllLink']);

							$TMPPattern 	= $ArrayEachLink['AllLink'];
							$TMPReplace 	= $TMPAllLink;
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
				}
			else
				{
					$TMPPattern = "/%LinkTrack=(.*)%/iU";
				if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
					{
					foreach ($ArrayMatches as $EachMatch) 
						{
							$TMPPattern = "%LinkTrack=".$EachMatch[1]."%";
							$TMPWhichURL = base64_encode($EachMatch[1]);
							$TMPReplace = $ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email']);
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
				}
			// link tracking: %LinkTrack=.....% - FINISHED
			
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignatureHTML'];
				}
			// oemPro signature - FINISHED

			// read tracking - STARTED
			if ($ArrayContent['TrackReads'] == 'Yes')
				{
				$ReadTrackURL = '<img src="'.$ArrayConfig['URLs']['Software'].'/read.php?EncryptedMemberID='.$EncryptedMemberID.'&CampaignID='.$ArrayCampaign['CampaignID'].'&CampaignStatisticsID='.$ArrayCampaignStatistics['CampaignStatisticsID'].'&Demo='.$Demo.'&Email='.base64_encode($ArrayMember['Email']).'" width="10" height="10" alt="---">';
				$NewString = str_replace('</body>', "\n".$ReadTrackURL."\n\n</body>", $String);
				if ($NewString == $String)
					{
					$String .= "\n\n".$ReadTrackURL;
					}
				else
					{
					$String = $NewString;
					unset($NewString);
					}
				}
			// read tracking - FINISHED
			}
		elseif ($ThisContentType == 'Alternate')
			{
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignaturePlain'];
				}
			// oemPro signature - FINISHED

			// link tracking (%LinkTrack%) - STARTED
				$TMPPattern = "/%LinkTrack=(.*)%/iU";
				if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
					{
					foreach ($ArrayMatches as $EachMatch) 
						{
							$TMPPattern = "%LinkTrack=".$EachMatch[1]."%";
							$TMPWhichURL = base64_encode($EachMatch[1]);
							$TMPReplace = $ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email']);
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
			// link tracking (%LinkTrack%) - FINISHED
			}
		}
	elseif ($CampaignContentType == 'Plain')
		{
			
		if ($ThisContentType == 'Subject')
			{
			}
		elseif ($ThisContentType == 'Main')
			{
			// oemPro signature - STARTED
			if ($ArrayConfig['GlobalSettings']['IncludeSignature'] == true)
				{
				$String .= $ArrayConfig['GlobalSettings']['SignaturePlain'];
				}
			// oemPro signature - FINISHED

			// link tracking (%LinkTrack%) - STARTED
				$TMPPattern = "/%LinkTrack=(.*)%/iU";
				if (preg_match_all($TMPPattern, $String, $ArrayMatches, PREG_SET_ORDER))
					{
					foreach ($ArrayMatches as $EachMatch) 
						{
							$TMPPattern = "%LinkTrack=".$EachMatch[1]."%";
							$TMPWhichURL = base64_encode($EachMatch[1]);
							$TMPReplace = $ArrayConfig['URLs']['Software']."/link.php?URL=".$TMPWhichURL."&EncryptedMemberID=".$EncryptedMemberID."&CampaignID=".$ArrayCampaign['CampaignID']."&CampaignStatisticsID=".$ArrayCampaignStatistics['CampaignStatisticsID']."&Demo=".$Demo."&Email=".base64_encode($ArrayMember['Email']);
						$String = str_replace($TMPPattern, $TMPReplace, $String);
						}
					}
			// link tracking (%LinkTrack%) - FINISHED
			}
		elseif ($ThisContentType == 'Alternate')
			{
			// No alternate content
			}
		}
	// Perform content type specific personalizations (ex: read tracking, link tracking) - FINISHED
	return $String;
	}

//########################################################################################
// Returns the campaign mail list IDs in array
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnCampaignMailListIDs($ArrayCampaign, $ArrayConfig)
	{
	$ArrayMailListIDs = array();
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayMailListIDs[] = $EachRow['RelMailListID'];
		}
	
	return $ArrayMailListIDs;
	}


?>
