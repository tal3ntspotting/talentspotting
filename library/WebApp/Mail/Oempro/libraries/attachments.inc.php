<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Returns the type of the file according to file extension
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnFileType($FilePath)
	{
	$Extension = pathinfo($FilePath);
		$Extension = strtolower($Extension['extension']);

	if ($Extension == 'xls')
		{
		$Type = "Excel";
		}
	elseif (($Extension == 'gif') || ($Extension == 'jpg') || ($Extension == 'tif') || ($Extension == 'bmp') || ($Extension == 'png'))
		{
		$Type = "Image";
		}
	elseif ($Extension == 'pdf')
		{
		$Type = "PDF";
		}
	elseif ($Extension == 'txt')
		{
		$Type = "Text";
		}
	elseif ($Extension == 'doc')
		{
		$Type = "DOC";
		}
	elseif (($Extension == 'zip') || ($Extension == 'rar'))
		{
		$Type = "ZIP";
		}
	else
		{
		$Type = "Generic";
		}
	
	return $Type;
	}

?>