<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Imports from another oemPro mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ImportFromOtherMailList($ArrayMember, $TargetMailListID, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists			= 	false;
	$ImportStatus			=	"";
	// Initialize variables - FINISHED

	// Check if the email address exists in database or not (for the same mail list) - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT MemberID FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$ArrayMember['Email']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalMemberFound = mysql_num_rows($ResultSet);

		$ArrayIgnoreMailListIDs = array();

		if ($TotalMemberFound > 0)
			{
			$SQLQueryPart = "RelMailListID='".$TargetMailListID."'";

			$SQLQuery = "SELECT RelMailListID, RelMemberID FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMemberID='".$ArrayMember['MemberID']."' AND (".$SQLQueryPart.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_num_rows($ResultSet);
				
			if ($TotalFound > 0)
				{
				$ErrorExists	= true;
				$ImportStatus	= "Duplicate";
				}
			}
		}
	// Check if the email address exists in database or not (for the same mail list) - FINISHED

	// Subscribe to mail list - STARTED
	if ($ErrorExists == false)
		{
		$TMPSubscribedAtLeastOne = false;
		 
		// Retrieve mail list opt-in confirmation setting - STARTED
		$SQLQuery = "SELECT SubscriptionType FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$TargetMailListID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$SubscriptionType = mysql_fetch_assoc($ResultSet);
			$SubscriptionType = $SubscriptionType['SubscriptionType'];
		// Retrieve mail list opt-in confirmation setting - FINISHED

		// Decide opt-in status - STARTED
		if ($SubscriptionType == 'Opt-in')
			{
			if ($ArrayPostValues['FormValue_MakeOptedIn'] != '')
				{
				$OptInStatus = "Yes";
				}
			else
				{
				if ($ArrayPostValues['FormValue_RequestOptIn'] != '')
					{
					$OptInStatus = "No";
					}
				elseif ($ArrayLine[$ArrayMailListFields['OptInConfirmed']] != '')
					{
					$OptInStatus = $ArrayLine[$ArrayMailListFields['OptInConfirmed']];
					}
				else
					{
					$OptInStatus = "No";
					}
				}
			}
		else
			{
			$OptInStatus = "Yes";
			}
		// Decide opt-in status - FINISHED

		$ArrayFieldnValues = array(
									"MembershipID"			=>	'',
									"RelMailListID"			=>	$TargetMailListID,
									"RelMemberID"			=>	$ArrayMember['MemberID'],
									"OptInConfirmed"		=>	$OptInStatus,
									"OptOutConfirmed"		=>	($ArrayLine[$ArrayMailListFields['OptOutConfirmed']] == '' ? 'No' : $ArrayLine[$ArrayMailListFields['OptOutConfirmed']]),
									"SubscriptionStatus"	=>	($ArrayLine[$ArrayMailListFields['SubscriptionStatus']] == '' ? ($OptInStatus == 'Yes' ? 'Subscribed'  : 'Pending for opt-in confirmation') : $ArrayLine[$ArrayMailListFields['SubscriptionStatus']]),
									"SubscriptionDate"		=>	($ArrayLine[$ArrayMailListFields['SubscriptionDate']] == '' ? date("Y-m-d H:i:s") : $ArrayLine[$ArrayMailListFields['SubscriptionDate']]),
									"UnSubscriptionDate"	=>	($ArrayLine[$ArrayMailListFields['UnSubscriptionDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['UnSubscriptionDate']]),
									"OptInConfirmDate"		=>	($ArrayLine[$ArrayMailListFields['OptInConfirmDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['OptInConfirmDate']]),
									"OptOutConfirmDate"		=>	($ArrayLine[$ArrayMailListFields['OptOutConfirmDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['OptOutConfirmDate']]),
									"IPOnSubscription"		=>	($ArrayLine[$ArrayMailListFields['IPOnSubscription']] == '' ? '0.0.0.0' : $ArrayLine[$ArrayMailListFields['IPOnSubscription']]),
									"IPOnUnsubscription"	=>	($ArrayLine[$ArrayMailListFields['IPOnUnsubscription']] == '' ? '0.0.0.0' : $ArrayLine[$ArrayMailListFields['IPOnUnsubscription']]),
									);
	
		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList = "'".$ValueList."'";
		
		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_members (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$MailListMembershipID = mysql_insert_id();

		// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - STARTED
		if ($ArrayPostValues['FormValue_RequestOptIn'] != '')
			{
			// Load required module - STARTED
			include_once("../system/classes/class_mailer.php");
			include_once("../system/libraries/membership.inc.php");
			include_once("../system/libraries/personalization.inc.php");
			// Load required module - FINISHED

			// Retrieve membership and mail list information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$TargetMailListID."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet2);

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE MembershipID='".$MailListMembershipID."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet2);
			// Retrieve membership and mail list information - FINISHED

				$ArrayMailListIDs		=	array($TargetMailListID);
				$EmailType				=	'OptInConfirmationRequest';
			SendMailListEmails($ArrayMailListIDs, $EmailType, $ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig);
			}
		// If enabled for this mail list subscription, send the opt-in confirmation email to the new member - FINISHED

		// Register member to receive auto responders of the mail list as follow up - STARTED
		if (($OptInStatus == 'Yes') && ($ArrayPostValues['FormValue_DisableAutoResponders'] == ''))
			{
				include_once("../system/libraries/followup.inc.php");
			$TotalRegistered = RegisterFollowUpAutoResponders($EachMailListID, ($MemberID == '' ? $NewMemberID : $MemberID), $ArrayConfig, $ArrayLanguage);
			}
		// Register member to receive auto responders of the mail list as follow up - FINISHED
		}
	// Subscribe to mail list - FINISHED

	// If imported without any problem, set the status - STARTED
	if ($ErrorExists == false)
		{
		$ImportStatus = "Imported";
		}
	// If imported without any problem, set the status - FINISHED

	return $ImportStatus;
	}

//########################################################################################
// Imports black listed members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ImportBlackListedMembers($ArrayLine, $ArrayMailLists, $ArrayConfig, $ArrayPostValues)
	{
	// Initialize variables - STARTED
	$ErrorExists			= 	false;
	$ImportStatus			=	"";
	// Initialize variables - FINISHED

	// Create banned item record - STARTED
	if ($ErrorExists == false)
		{
		foreach ($ArrayMailLists as $EachMailListID)
			{
			$ArrayFieldnValues = array(
										"BlackListID"		=>	'',
										"RelMailListID"		=>	$EachMailListID,
										"Email"				=>	$ArrayLine[0],
										"DateSubmitted"		=>	date('Y-m-d H:i:s'),
										);
	
			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList = "'".$ValueList."'";
	
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$NewBlackListedID = mysql_insert_id();
			}
		}
	// Create banned item record - FINISHED

	// If imported without any problem, set the status - STARTED
	if ($ErrorExists == false)
		{
		$ImportStatus = "Imported";
		}
	// If imported without any problem, set the status - FINISHED

	return $ImportStatus;
	}
	
//########################################################################################
// Imports banned members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ImportBannedMembers($ArrayLine, $ArrayMailLists, $BanType, $ArrayConfig, $ArrayPostValues)
	{
	// Initialize variables - STARTED
	$ErrorExists			= 	false;
	$ImportStatus			=	"";
	// Initialize variables - FINISHED

	// Create banned item record - STARTED
	if ($ErrorExists == false)
		{
		foreach ($ArrayMailLists as $EachMailListID)
			{
			$ArrayFieldnValues = array(
										"BanID"				=> '',
										"RelMailListID"		=> $EachMailListID,
										"Type"				=> $BanType,
										"Banned"			=> $ArrayLine[0],
										);
	
			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList = "'".$ValueList."'";
	
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_banned (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$NewBannedID = mysql_insert_id();
			}
		}
	// Create banned item record - FINISHED

	// If imported without any problem, set the status - STARTED
	if ($ErrorExists == false)
		{
		$ImportStatus = "Imported";
		}
	// If imported without any problem, set the status - FINISHED

	return $ImportStatus;
	}
	
//########################################################################################
// Imports new member information to oemPro database (for Flat Files)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ImportNewMember($ArrayLine, $ArrayCorrespondancy, $ArrayMailLists, $ArrayConfig, $ArrayBanned, $ArrayDefaultValues, $ArrayCustomFields, $ArrayPostValues)
	{
	// Initialize variables - STARTED
	$ErrorExists			= 	false;
	$ArrayMemberFields		=	array();
	$ArrayMailListFields	=	array();
	$ArrayInvCorrespondancy	=	array_flip($ArrayCorrespondancy);
	$ImportStatus			=	"";
	// Initialize variables - FINISHED

	// Specify the fields for member table - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."members";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		while ($EachField = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMemberFields[$EachField['Field']]	=	$ArrayInvCorrespondancy[$EachField['Field']];
			}
		}
	// Specify the fields for member table - FINISHED
	
	// Specify the fields for mail list membership table - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."maillist_members";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		while ($EachField = mysql_fetch_assoc($ResultSet))
			{
			$ArrayMailListFields[$EachField['Field']]	=	$ArrayInvCorrespondancy[$EachField['Field']];
			}
		}
	// Specify the fields for mail list membership table - FINISHED

	// Define the email address - STARTED
	if ($ErrorExists == false)
		{
		$EmailAddress	= $ArrayLine[$ArrayMemberFields['Email']];
		$EmailUser		= explode("@", $EmailAddress);
			$EmaliUser	= $EmailUser[0];
		$EmailDomain		= explode("@", $EmailAddress);
			$EmailDomain	= $EmailDomain[1];

		if ($EmailAddress == '')
			{
			$ErrorExists	= true;
			$ImportStatus	= "Failed";
			}
		}
	// Define the email address - FINISHED

	// Check if the email address is banned or not - STARTED
	if (($ErrorExists == false) && ($ArrayPostValues['FormValue_CheckBanned'] != ''))
		{
		// Check in banned items (both global and local) - STARTED
		if ($ErrorExists == false)
			{
			foreach ($ArrayBanned as $Type=>$Banned)
				{
				if ($Type == 'Domain')
					{
					foreach ($Banned as $EachBanned)
						{
						if ($EachBanned == $EmailDomain)
							{
							$ErrorExists	= true;
							$ImportStatus 	= "Banned";
							}
						}
					}
				elseif ($Type == 'Email address')
					{
					foreach ($Banned as $EachBanned)
						{
						$TMPPattern = str_replace("*", "(.*)", $EachBanned);

						if (eregi($TMPPattern, $EmailAddress) == true)
							{
							$ErrorExists	= true;
							$ImportStatus 	= "Banned";
							}
						}
					}
				}
			}
		// Check in banned items (both global and local) - FINISHED
		}
	// Check if the email address is banned or not - FINISHED

	// Check if the email address is in the mail list black list - STARTED
	if (($ErrorExists == false) && ($ArrayPostValues['FormValue_CheckBlackListed'] != ''))
		{
		$SQLQueryPart = array();

		foreach ($ArrayPostValues['FormValue_MailLists'] as $EachMailListID)
			{
			$SQLQueryPart[] = "RelMailListID='".$EachMailListID."'";
			}
		$SQLQueryPart[] = "RelMailListID='0'";

		$SQLQueryPart = implode(" OR ", $SQLQueryPart);
        
        //$SQLQueryPart  = "1";
		
		//$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE (".$SQLQueryPart.") AND Email='".$ArrayLine[$ArrayMemberFields['Email']]."'";
        $SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$ArrayLine[$ArrayMemberFields['Email']]."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		
		if ($TotalFound > 0)
			{
			$ErrorExists = true;
			$ImportStatus = "Black listed";
			}
		}
	// Check if the email address is in the mail list black list - FINISHED
	
	// Check for email address format - STARTED
	if (($ErrorExists == false) && ($ArrayPostValues['FormValue_ValidateEmail'] != ''))
		{
		// Load the required module - STARTED
		include_once("../system/classes/class_utilities1.php");

		$ObjectUtilities = new OctethUtilities();

		if ($ObjectUtilities->CheckEmailFormat(strtolower($EmailAddress)) == false)
			{
			$ErrorExists	= true;
			$ImportStatus	= "Failed";
			}
		// Load the required module - FINISHED
		}
	// Check for email address format - FINISHED

	// Check if the email address exists in database or not (for the same mail list) - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT MemberID FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$EmailAddress."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalMemberFound = mysql_num_rows($ResultSet);

		$ArrayIgnoreMailListIDs = array();

		if ($TotalMemberFound > 0)
			{
			$SQLQueryPart = array();
	
			foreach ($ArrayPostValues['FormValue_MailLists'] as $EachMailListID)
				{
				$SQLQueryPart[] = "RelMailListID='".$EachMailListID."'";
				}
			$SQLQueryPart[] = "RelMailListID='0'";
	
			$SQLQueryPart = implode(" OR ", $SQLQueryPart);
			
			$MemberID = mysql_fetch_assoc($ResultSet);
				$MemberID = $MemberID['MemberID'];
				
			$SQLQuery = "SELECT RelMailListID, RelMemberID FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMemberID='".$MemberID."' AND (".$SQLQueryPart.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_num_rows($ResultSet);
				
			if ($TotalFound > 0)
				{
				while ($EachRow = mysql_fetch_assoc($ResultSet))
					{
					$ArrayIgnoreMailListIDs[$EachRow['RelMailListID']] = true;
					}
				}
			}
		}
	// Check if the email address exists in database or not (for the same mail list) - FINISHED

	// Create member record - STARTED
	if ($ErrorExists == false)
		{
		if ($TotalMemberFound < 1)
			{
			// Generate a new random password if password field is not synchronized - STARTED
			if ($ArrayLine[$ArrayMemberFields['Password']] == '')
				{
				include_once("../system/classes/class_utilities1.php");
				$ObjectUtilities = new OctethUtilities();
					$NewAutoPassword = $ObjectUtilities->GeneratePassword(5);
				}
			// Generate a new random password if password field is not synchronized - FINISHED
			
			$ArrayFieldnValues = array(
										"MemberID"		=>	'',
										"Email"			=>	addslashes(strtolower($ArrayLine[$ArrayMemberFields['Email']])),
										"BounceType"	=>	addslashes(($ArrayLine[$ArrayMemberFields['BounceType']] == '' ? $ArrayDefaultValues['BounceType'] : $ArrayLine[$ArrayMemberFields['BounceType']])),
										"Password"		=>	addslashes(($ArrayLine[$ArrayMemberFields['Password']] == '' ? $NewAutoPassword : $ArrayLine[$ArrayMemberFields['Password']])),
										);
	
			// Add custom fields into the array - STARTED
			foreach ($ArrayCustomFields as $EachCustomFieldID)
				{
				$ArrayFieldnValues['CustomField'.$EachCustomFieldID]	=	addslashes($ArrayLine[$ArrayMemberFields['CustomField'.$EachCustomFieldID]]);
				}
			// Add custom fields into the array - FINISHED

			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList = "'".$ValueList."'";

			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$NewMemberID = mysql_insert_id();
			}
		else
			{
			// If member exists, only update the custom fields which are being imported - STARTED
			if ($ArrayPostValues['FormValue_CheckDuplicate'] == 'Update')
				{
				$ArrayFieldnValues = array(
											);
		
				$SQLQueryPart = array();
				foreach ($ArrayCustomFields as $EachCustomFieldID)
					{
					if ($ArrayCorrespondancy[$ArrayMemberFields['CustomField'.$EachCustomFieldID]] != '')
						{
						$SQLQueryPart[] = 'CustomField'.$EachCustomFieldID." = '".addslashes($ArrayLine[$ArrayMemberFields['CustomField'.$EachCustomFieldID]])."'";
						}
					}
				$SQLQueryPart = implode(",", $SQLQueryPart);
	
				$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."members SET ".$SQLQueryPart." WHERE MemberID='".$MemberID."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				}
			// If member exists, only update the custom fields which are being imported - FINISHED
			}
		}
	// Create member record - FINISHED

	// Subscribe to mail list - STARTED
	if ($ErrorExists == false)
		{
		$TMPSubscribedAtLeastOne = false;
		 
		foreach ($ArrayPostValues['FormValue_MailLists'] as $EachMailListID)
			{
			// Skip this membership if this record exists - STARTED
			if ($ArrayIgnoreMailListIDs[$EachMailListID] == true)
				{
				// Update opt-in confirmation status if "opt-in confirmed" checkbox and "Update member information" radio button is checked - STARTED
				if (($ArrayPostValues['FormValue_CheckDuplicate'] == 'Update') && ($ArrayPostValues['FormValue_MakeOptedIn'] != ''))
					{
					// Change membership status to "subscribed" - STARTED
					$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."maillist_members SET OptInConfirmed='Yes', OptOutConfirmed='No', SubscriptionStatus='Subscribed', UnsubscriptionDate='0000-00-00 00:00:00', OptOutConfirmDate='0000-00-00 00:00:00', IPOnUnSubscription='0.0.0.0' WHERE RelMailListID='".$EachMailListID."' AND RelMemberID='".($MemberID == '' ? $NewMemberID : $MemberID)."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					// Change membership status to "subscribed" - FINISHED

					continue;
					}
				else
					{
					continue;
					}
				// Update opt-in confirmation status if "opt-in confirmed" checkbox and "Update member information" radio button is checked - FINISHED
				}
			// Skip this membership if this record exists - FINISHED
			
			$TMPSubscribedAtLeastOne = true;
			
			// Retrieve mail list opt-in confirmation setting - STARTED
			$SQLQuery = "SELECT SubscriptionType FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachMailListID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$SubscriptionType = mysql_fetch_assoc($ResultSet);
				$SubscriptionType = $SubscriptionType['SubscriptionType'];
			// Retrieve mail list opt-in confirmation setting - FINISHED

			// Decide opt-in status - STARTED
			if ($SubscriptionType == 'Opt-in')
				{
				if ($ArrayPostValues['FormValue_MakeOptedIn'] != '')
					{
					$OptInStatus = "Yes";
					}
				else
					{
					if ($ArrayPostValues['FormValue_RequestOptIn'] != '')
						{
						$OptInStatus = "No";
						}
					elseif ($ArrayLine[$ArrayMailListFields['OptInConfirmed']] != '')
						{
						$OptInStatus = $ArrayLine[$ArrayMailListFields['OptInConfirmed']];
						}
					else
						{
						$OptInStatus = "No";
						}
					}
				}
			else
				{
				$OptInStatus = "Yes";
				}
			// Decide opt-in status - FINISHED

			$ArrayFieldnValues = array(
										"MembershipID"			=>	'',
										"RelMailListID"			=>	$EachMailListID,
										"RelMemberID"			=>	($MemberID == '' ? $NewMemberID : $MemberID),
										"OptInConfirmed"		=>	$OptInStatus,
										"OptOutConfirmed"		=>	($ArrayLine[$ArrayMailListFields['OptOutConfirmed']] == '' ? 'No' : $ArrayLine[$ArrayMailListFields['OptOutConfirmed']]),
										"SubscriptionStatus"	=>	($ArrayLine[$ArrayMailListFields['SubscriptionStatus']] == '' ? ($OptInStatus == 'Yes' ? 'Subscribed'  : 'Pending for opt-in confirmation') : $ArrayLine[$ArrayMailListFields['SubscriptionStatus']]),
										"SubscriptionDate"		=>	($ArrayLine[$ArrayMailListFields['SubscriptionDate']] == '' ? date("Y-m-d H:i:s") : $ArrayLine[$ArrayMailListFields['SubscriptionDate']]),
										"UnSubscriptionDate"	=>	($ArrayLine[$ArrayMailListFields['UnSubscriptionDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['UnSubscriptionDate']]),
										"OptInConfirmDate"		=>	($ArrayLine[$ArrayMailListFields['OptInConfirmDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['OptInConfirmDate']]),
										"OptOutConfirmDate"		=>	($ArrayLine[$ArrayMailListFields['OptOutConfirmDate']] == '' ? '0000-00-00 00:00:00' : $ArrayLine[$ArrayMailListFields['OptOutConfirmDate']]),
										"IPOnSubscription"		=>	($ArrayLine[$ArrayMailListFields['IPOnSubscription']] == '' ? '0.0.0.0' : $ArrayLine[$ArrayMailListFields['IPOnSubscription']]),
										"IPOnUnsubscription"	=>	($ArrayLine[$ArrayMailListFields['IPOnUnsubscription']] == '' ? '0.0.0.0' : $ArrayLine[$ArrayMailListFields['IPOnUnsubscription']]),
										);
		
			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList = "'".$ValueList."'";
			
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_members (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$MailListMembershipID = mysql_insert_id();

			// Register member to receive auto responders of the mail list as follow up - STARTED
			if (($OptInStatus == 'Yes') && ($ArrayPostValues['FormValue_DisableAutoResponders'] == ''))
				{
					include_once("../system/libraries/followup.inc.php");
				$TotalRegistered = RegisterFollowUpAutoResponders($EachMailListID, ($MemberID == '' ? $NewMemberID : $MemberID), $ArrayConfig, $ArrayLanguage);
				}
			// Register member to receive auto responders of the mail list as follow up - FINISHED
			}
		}
	// Subscribe to mail list - FINISHED

	// If member has not been subscribed to any mail list due to duplicate, return "duplicate" error - STARTED
	if (($ErrorExists == false) && ($TMPSubscribedAtLeastOne == false))
		{
		$ErrorExists	= true;
		$ImportStatus	= "Duplicate";
		}
	// If member has not been subscribed to any mail list due to duplicate, return "duplicate" error - FINISHED

	// Send opt-in confirmation email (if enabled) - STARTED
	if (($ErrorExists == false) && ($ArrayPostValues['FormValue_RequestOptIn'] != ''))
		{
		// Retrieve member account information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".($MemberID == '' ? $NewMemberID : $MemberID)."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMember = mysql_fetch_assoc($ResultSet);
		// Retrieve member account information - FINISHED
		
		// Retrieve all opt-in request emails of the mail lists - STARTED
		$SQLQueryPart = array();
		foreach ($ArrayPostValues['FormValue_MailLists'] as $EachMailListID)
			{
			$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
			}

		$SQLQueryPart = implode(" OR ", $SQLQueryPart);

		$SQLQuery  = "SELECT * ";
		$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."maillist_messages AS tblAssign ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."emails AS tblEmails ";
		$SQLQuery .= "ON tblAssign.RelEmailID = tblEmails.EmailID ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
		$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
		$SQLQuery .= "WHERE (".$SQLQueryPart.") AND tblAssign.UseFor='OptInConfirmationRequest'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Retrieve all opt-in request emails of the mail lists - FINISHED

		// Send all opt-in confirmation emails to the member - STARTED
			// Load required module - STARTED
			include_once("../system/classes/class_mailer.php");
			include_once("../system/libraries/membership.inc.php");
			include_once("../system/libraries/personalization.inc.php");
			// Load required module - FINISHED

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// Retrieve membership and mail list information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachRow['MailListID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet2);

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$EachRow['MailListID']."' AND RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet2);
			// Retrieve membership and mail list information - FINISHED
			
			$ObjectMailer 			= new phpmailer();
	
			$ObjectMailer->IsMail();

			$ObjectMailer->From		= $EachRow['FromEmail'];
			$ObjectMailer->FromName	= $EachRow['FromName'];
			$ObjectMailer->AddAddress(strtolower($ArrayLine[$ArrayMemberFields['Email']]), '');
			$ObjectMailer->AddReplyTo($EachRow['ReplyToEmail'], $EachRow['ReplyToName']);
			$ObjectMailer->ReturnPath= $EachRow['ReturnPathEmail'];
			$ObjectMailer->Sender= $EachRow['ReturnPathEmail'];
			
			$ObjectMailer->WordWrap	= 70;
			$ObjectMailer->Priority	= $EachRow['Priority'];
			$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
			$ObjectMailer->CharSet	= substr($EachRow["CharSet"], 0, strlen($EachRow["CharSet"]) - 5);
			$ObjectMailer->Subject	= stripslashes($EachRow['Subject']);

			if (($EachRow['ContentType'] == 'Html') || ($EachRow['ContentType'] == 'Both'))
				{
				$ObjectMailer->IsHTML(true);
				$ObjectMailer->Body		= $EachRow['Content'];
				$ObjectMailer->AltBody	= $EachRow['ContentAlternate'];
				}
			elseif ($EachRow['ContentType'] == 'Plain')
				{
				$ObjectMailer->IsHTML(false);
				$ObjectMailer->Body		= $EachRow['Content'];
				}

			// Parse the confirmation link - STARTED
			$EncryptedMailListMembershipID = Encrypt($MailListMembershipID, $ArrayConfig['Settings']['EncryptionKey']);
			
					$ArrayReplaceList = array(
											"%Link%"			=>	$ArrayConfig['URLs']['Software']."/optin_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID'],
											);
				$ObjectMailer->Body	=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
			// Parse the confirmation link - FINISHED
			
			// Perform personalization - STARTED
				$ArrayPrefixes		=	array(
											"Sender",
											"Member",
											"Membership",
											"MailList",
											);
				$ArrayInformation	=	array(
											$ArrayMember,
											$ArrayMember,
											$ArrayMembership,
											$ArrayMailList,
											);											
			$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
			$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
			$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
			// Perform personalization - FINISHED
	
			if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
				{
				$ObjectMailer->IsMail();
				}
			else
				{
				include_once('../system/libraries/send_engines.inc.php');
				include_once("../system/classes/class_smtp.php");
				
				$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
				
				if ($ArraySendEngine == false)
					{
					$ObjectMailer->IsMail();
					}
				else
					{
						$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
						$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
						$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
						$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
						$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
						$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
						$ObjectMailer->SMTPKeepAlive	=	false;
					$ObjectMailer->IsSMTP();
					}
				}

			$ObjectMailer->Send();
			}
		// Send all opt-in confirmation emails to the member - FINISHED
		}
	// Send opt-in confirmation email (if enabled) - FINISHED

	// If imported without any problem, set the status - STARTED
	if ($ErrorExists == false)
		{
		$ImportStatus = "Imported";
		}
	// If imported without any problem, set the status - FINISHED

	return $ImportStatus;
	}
	
//########################################################################################
// Returns an array which includes database table custom fields a mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnOEMPCustomFields($MailListIDs, $ArrayLanguage, $ArrayConfig)
	{	
	$ArrayOEMPFields = array();

	$ArrayHideFields = array();

	// Retrieve custom fields - STARTED
	$SQLQueryPart = array();
	foreach ($MailListIDs as $EachMailListID)
		{
		$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
		}
	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_customfields as tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."customfields as tblFields ";
	$SQLQuery .= "ON tblAssign.RelCustomFieldID = tblFields.CustomFieldID ";
	if ($SQLQueryPart != '')
		{
		$SQLQuery .= "WHERE (".$SQLQueryPart.") ";
		}
	$SQLQuery .= "GROUP BY tblAssign.RelCustomFieldID ";
	$SQLQuery .= "ORDER BY tblFields.Title ASC";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if (($EachField['FieldType'] != 'Hidden field') && ($EachField['FieldType'] != 'Notice'))
			{
			$CustomFieldTitle = (strlen($EachField['Title']) > 40 ? substr($EachField['Title'], 0, 40)."..." : $EachField['Title']);
			}
		else
			{
			$CustomFieldTitle = (strlen($EachField['Extra1']) > 40 ? substr($EachField['Extra1'], 0, 40)."..." : $EachField['Extra1']);
			}

		$ArrayOEMPFields["CustomField".$EachField['CustomFieldID']] = $CustomFieldTitle;
		}
	// Retrieve custom fields - FINISHED

	return $ArrayOEMPFields;
	}
	
//########################################################################################
// Returns an array which includes outsourced mail list fields
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnOutsourcedMailListFields($MailListIDs, $ArrayLanguage, $ArrayConfig)
	{	
	// Initialize variables - STARTED
	$ArrayOutsourcedFields	= array();
	$DoesAllOutsourcedLists = true;
	// Initialize variables - FINISHED

	// Loop each selected mail list - STARTED
	foreach ($MailListIDs as $EachMailListID)
		{
		// Retrieve mail list information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachMailListID."' AND SourceType!='Local'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		if (mysql_num_rows($ResultSet) == 0)
			{
			$DoesAllOutsourcedLists = false;
			continue;
			}
		else
			{
			$ArrayMailList = mysql_fetch_assoc($ResultSet);
			}
		// Retrieve mail list information - FINISHED

		// Retrieve the outsourced mail list aliases - STARTED
		include_once("../system/libraries/outsourced_lists.inc.php");
		$ArrayMailListAliases = ReturnMailListAliases($ArrayMailList, $ArrayConfig);
		// Retrieve the outsourced mail list aliases - FINISHED
		
		// Retrieve fields of this mail list - STARTED
		mysql_close();

		$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
			mysql_select_db($ArrayMailList['OutSourceDBName']);

			// Retrieve the list of fields - STARTED
			$SQLQueryFields = "DESCRIBE ".$ArrayMailList['OutSourceDBTable'];
			$ResultSet = ExecuteMySQLQuery($SQLQueryFields, __LINE__, __FILE__);
			
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayOutsourcedFields[$EachRow['Field']] = $ArrayMailListAliases[$EachRow['Field']];
				}
			// Retrieve the list of fields - FINISHED
		
		mysql_close();
		
		// Connect to oemPro MySQL server and database - STARTED
		$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
		// Connect to oemPro MySQL server and database - FINISHED
		
		// Retrieve fields of this mail list - FINISHED
		}
	// Loop each selected mail list - FINISHED

	// If there is at least one local mail list selected, then do not return fields of outsourced lists to avoid any problems on send rules - STARTED
	if ($DoesAllOutsourcedLists == true)
		{
		return $ArrayOutsourcedFields;
		}
	else
		{
		return array();
		}
	// If there is at least one local mail list selected, then do not return fields of outsourced lists to avoid any problems on send rules - FINISHED
	}
	
//########################################################################################
// Returns an array which includes database table fields, names and custom fields of a mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnOEMPFields($MailListIDs, $ArrayLanguage, $ArrayConfig)
	{	
	$ArrayOEMPFields = array();

	$ArrayHideFields = array();
		$ArrayHideFields["MemberID"]		=	true;
		$ArrayHideFields["ID"]				=	true;
		$ArrayHideFields["RelMailListID"]	=	true;
		$ArrayHideFields["RelMemberID"]		=	true;
		$ArrayHideFields["MembershipID"]	=	true;

	// Retrieve "oemp_members" fields (except custom fields) - STARTED
	$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."members";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if ((substr($EachField['Field'], 0, 11) != 'CustomField') && ($ArrayHideFields[$EachField['Field']] != true))
			{
			$ArrayOEMPFields[$EachField['Field']] = ($ArrayLanguage["Texts"]["00081"][$EachField['Field']] == '' ? $EachField['Field'] : $ArrayLanguage["Texts"]["00081"][$EachField['Field']]);
			}
		}
	// Retrieve "oemp_members" fields (except custom fields) - FINISHED

	// Retrieve custom fields - STARTED
	$SQLQueryPart = array();
	foreach ($MailListIDs as $EachMailListID)
		{
		$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
		}
	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_customfields as tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."customfields as tblFields ";
	$SQLQuery .= "ON tblAssign.RelCustomFieldID = tblFields.CustomFieldID ";
	$SQLQuery .= "WHERE (".$SQLQueryPart.") ";
	$SQLQuery .= "GROUP BY tblAssign.RelCustomFieldID ";
	$SQLQuery .= "ORDER BY tblFields.Title ASC";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if (($EachField['FieldType'] != 'Hidden field') && ($EachField['FieldType'] != 'Notice'))
			{
			$CustomFieldTitle = (strlen($EachField['Title']) > 40 ? substr($EachField['Title'], 0, 40)."..." : $EachField['Title']);
			}
		else
			{
			$CustomFieldTitle = (strlen($EachField['Extra1']) > 40 ? substr($EachField['Extra1'], 0, 40)."..." : $EachField['Extra1']);
			}

		$ArrayOEMPFields["CustomField".$EachField['CustomFieldID']] = $CustomFieldTitle;
		}
	// Retrieve custom fields - FINISHED

	// Retrieve "oemp_maillist_members" fields - STARTED
	$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."maillist_members";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if ((substr($EachField['Field'], 0, 11) != 'CustomField') && ($ArrayHideFields[$EachField['Field']] != true))
			{
			$ArrayOEMPFields[$EachField['Field']] = ($ArrayLanguage["Texts"]["00082"][$EachField['Field']] == '' ? $EachField['Field'] : $ArrayLanguage["Texts"]["00082"][$EachField['Field']]);
			}
		}
	// Retrieve "oemp_maillist_members" fields - FINISHED
	
	return $ArrayOEMPFields;
	}
	
//########################################################################################
// Parses a string into array according to qualifier, delimeter and EOL character
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseString2Array($String, $Delimeter, $Qualifier, $EOL)
	{	
	$CharTab = "\t";
	// Re-init variables - STARTED
	if ($Delimeter == "T")
		{
			$Delimeter = $CharTab;
		}
	else 
		{
			$Delimeter = stripslashes($Delimeter);
		}
	$Qualifier = stripslashes($Qualifier);
	// Re-init variables - FINISHED

	// Remove line breaks - STARTED
	$String = trim($String);
	// Remove line breaks - FINISHED

	// Remove delimeters - STARTED
	$ArrayString = array();
	if ($Delimeter != '')
		{
		$ArrayString = explode($Qualifier.$Delimeter.$Qualifier, $String);
		}
	else
		{
		$ArrayString[] = $String;
		}
	// Remove delimeters - FINISHED
		
	// Remove the beginning and ending qualifiers - STARTED
	if ($Qualifier != '')
		{
		if (substr($ArrayString[0], 0, strlen($Qualifier)) == $Qualifier)
			{
			$ArrayString[0]		 					= substr($ArrayString[0], 1, strlen($ArrayString[0]));
			}
		if (substr($ArrayString[count($ArrayString) - 1], strlen($ArrayString[count($ArrayString) - 1]) - strlen($Qualifier), strlen($ArrayString[count($ArrayString) - 1])) == $Qualifier)
			{
			$ArrayString[count($ArrayString) - 1]	= substr($ArrayString[count($ArrayString) - 1], 0, strlen($ArrayString[count($ArrayString) - 1]) - 1);
			}
		}
	// Remove the beginning and ending qualifiers - FINISHED

	return $ArrayString;
	}

//########################################################################################
// Converts line breaks (CR LF, LF CR, CR) to LF (Unix - \n)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ConvertLineBreaks($String)
	{
	return preg_replace('/\r\n|\n\r|\r/', "\n", $String);
	}

?>