<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Delete news
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteNews($NewsID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."news WHERE NewsID='".$NewsID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayNews = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED
	
	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "news";
		$SuperTableFieldName	= "NewsID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $NewsID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."news WHERE NewsID='".$NewsID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete arhcive RSS feed
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteCampaignRSS($RSSFeedID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_rss WHERE RSSFeedID='".$RSSFeedID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayArchiveRSS = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED
	
	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "archive_rss";
		$SuperTableFieldName	= "RSSFeedID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $RSSFeedID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."archive_rss WHERE RSSFeedID='".$RSSFeedID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete arhcive list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteCampaignArchive($ArchiveListID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."archive_list WHERE ArchiveListID='".$ArchiveListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayArchiveList = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED
	
	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "archive_list";
		$SuperTableFieldName	= "ArchiveListID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $ArchiveListID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."archive_list WHERE ArchiveListID='".$ArchiveListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteIntegrationScreen($ScreenID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."integration_screens WHERE ScreenID='".$ScreenID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayScreen = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Avoid removing default screens - STARTED
	if ($ArrayScreen['RelAdministratorID'] == '0')
		{
		return;
		}
	// Avoid removing default screens - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "integration_screens";
		$SuperTableFieldName	= "ScreenID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $ScreenID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."integration_screens WHERE ScreenID='".$ScreenID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteMembership($MembershipID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE MembershipID='".$MembershipID."' AND SubscriptionStatus = 'Subscribed'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayMembership = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "maillist_members";
		$SuperTableFieldName	= "MembershipID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $MembershipID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE MembershipID='".$MembershipID."' AND SubscriptionStatus = 'Subscribed'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete campaigns
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteCampaigns($CampaignID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE CampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCampaign = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete campaign files and directories - STARTED
	$TemplatePath = $ArrayConfig['Paths']['Data']."/campaign_attachments/".$CampaignID;

	if (is_dir($TemplatePath) == true)
		{
		$DirHandle = opendir($TemplatePath);
		while ($EachFile = readdir($DirHandle))
			{
			if (($EachFile != '.') && ($EachFile != '..'))
				{
				unlink($TemplatePath."/".$EachFile);
				}
			}
		closedir($DirHandle);
	
		rmdir($TemplatePath);
		}
		
	$TemplatePath = $ArrayConfig['Paths']['Data']."/campaign_processing/".$CampaignID;

	if (is_dir($TemplatePath) == true)
		{
		$DirHandle = opendir($TemplatePath);
		while ($EachFile = readdir($DirHandle))
			{
			if (($EachFile != '.') && ($EachFile != '..'))
				{
				unlink($TemplatePath."/".$EachFile);
				}
			}
		closedir($DirHandle);
	
		rmdir($TemplatePath);
		}
	// Delete campaign files and directories - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "campaigns";
		$SuperTableFieldName	= "CampaignID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $CampaignID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE CampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete newsletter templates
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteTemplates($NewsletterTemplateID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$NewsletterTemplateID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete newsletter template files and objects - STARTED
	$TemplatePath = $ArrayConfig['Paths']['Data']."/newsletter_templates/".$NewsletterTemplateID;
	unlink($TemplatePath."/content.htm");
	unlink($TemplatePath."/content.txt");

	$DirHandle = opendir($TemplatePath."/objects");
	while ($EachFile = readdir($DirHandle))
		{
		if (($EachFile != '.') && ($EachFile != '..'))
			{
			unlink($TemplatePath."/objects/".$EachFile);
			}
		}
	closedir($DirHandle);

	rmdir($TemplatePath."/objects");
	rmdir($TemplatePath);
	// Delete newsletter template files and objects - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "newsletter_templates";
		$SuperTableFieldName	= "NewsletterTemplateID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $NewsletterTemplateID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$NewsletterTemplateID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteMailList($MailListID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "maillists";
		$SuperTableFieldName	= "MailListID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $MailListID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteMember($MemberID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$MemberID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "members";
		$SuperTableFieldName	= "MemberID";
		$ArrayExcludedSubTables	= array("campaigns_linkclicks_detailed");
		$RelatedID				= $MemberID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$MemberID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete auto responder
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteAutoResponder($AutoResponderID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."autoresponders WHERE AutoResponderID='".$AutoResponderID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "autoresponders";
		$SuperTableFieldName	= "AutoResponderID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $AutoResponderID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."autoresponders WHERE AutoResponderID='".$AutoResponderID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete follow up groups
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteFollowUps($FollowUpGroupID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE FollowUpGroupID='".$FollowUpGroupID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "maillist_followups";
		$SuperTableFieldName	= "FollowUpGroupID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $FollowUpGroupID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE FollowUpGroupID='".$FollowUpGroupID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete mail list emails
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteEmails($EmailID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."emails WHERE EmailID='".$EmailID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "emails";
		$SuperTableFieldName	= "EmailID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $EmailID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."emails WHERE EmailID='".$EmailID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete custom field
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteCustomField($CustomFieldID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."customfields WHERE CustomFieldID='".$CustomFieldID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCustomField = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Remove the table field from members table - STARTED
	$SQLQuery = "ALTER TABLE oemp_members DROP `CustomField".$CustomFieldID."`";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the table field from members table - FINISHED
	
	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "customfields";
		$SuperTableFieldName	= "CustomFieldID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $CustomFieldID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."customfields WHERE CustomFieldID='".$CustomFieldID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete client
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteClient($ClientID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."clients WHERE ClientID='".$ClientID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayBan = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED
	
	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "clients";
		$SuperTableFieldName	= "ClientID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $ClientID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."clients WHERE ClientID='".$ClientID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete send engine (SMTP server)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteSendEngine($SMTPServerID, $ArrayLoggedAdmin, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."smtp_servers WHERE SMTPServerID='".$SMTPServerID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayBan = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "smtp_servers";
		$SuperTableFieldName	= "SMTPServerID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $SMTPServerID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."smtp_servers WHERE SMTPServerID='".$SMTPServerID."' AND RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}
	
//########################################################################################
// Delete mail box (POP3 server)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteMailBox($POP3ServerID, $ArrayLoggedAdmin, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE POP3ServerID='".$POP3ServerID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayBan = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "pop3_servers";
		$SuperTableFieldName	= "POP3ServerID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $POP3ServerID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."pop3_servers WHERE POP3ServerID='".$POP3ServerID."' AND RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete black listed member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteBlackListedMember($BlackListID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE BlackListID='".$BlackListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayBan = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "maillist_blacklist";
		$SuperTableFieldName	= "BlackListID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $BlackListID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE BlackListID='".$BlackListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}

//########################################################################################
// Delete banned member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DeleteBannedMember($BanID, $ArrayConfig)
	{	
	// Retrieve the selected record - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE BanID='".$BanID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayBan = mysql_fetch_assoc($ResultSet);
	// Retrieve the selected record - FINISHED

	// Delete assigned records from other tables - STARTED
		$SuperTableName			= "maillist_banned";
		$SuperTableFieldName	= "BanID";
		$ArrayExcludedSubTables	= array();
		$RelatedID				= $BanID;
	RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
	// Delete assigned records from other tables - FINISHED

	// Remove the record - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE BanID='".$BanID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Remove the record - FINISHED
	
	return;
	}


?>