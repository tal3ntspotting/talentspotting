<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Check for the possible "halted" send processes and resume them
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckAndResumeCampaigns($ArrayConfig, $ArrayLanguage)
	{
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE Status='Sending' OR Sleeping='1'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$TotalRecovered = 0;

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Calculate the different between now and last action time - STARTED
		$Now			= strtotime(date("Y-m-d H:i:s"));
		$LastActionTime	= strtotime($EachRow['SendLastActionDateTime']);
		$Difference		= $Now - $LastActionTime;
		// Calculate the different between now and last action time - FINISHED

		// Decide if the sending process is halted or not - STARTED
		if (($Difference > $ArrayConfig['Settings']['AssumeCampaignHaltedAfterSeconds']) && ($EachRow['SendLastActionDateTime'] != '0000-00-00 00:00:00'))
			{
			$TotalRecovered++;
			
			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Pending', Sleeping='0' SendLastActionDateTime='".date("Y-m-d H:i:s")."' WHERE CampaignStatisticsID='".$EachRow['CampaignStatisticsID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		// Decide if the sending process is halted or not - FINISHED
		
		}

	return $TotalRecovered;
	}


?>