<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Send a preview email to the defined email address of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
include_once("../system/libraries/plugin_functions.inc.php");


function CampaignPreview($Email, $CampaignID, $ArrayConfig, $ArrayLanguage, $ArrayPostVariables, $ArrayLoggedAdmin)
	{
	// Initialize variables - STARTED
	$ArrayStatus = array();
	// Initialize variables - FINISHED

	// Include the required modules - STARTED
	include_once("../system/libraries/personalization.inc.php");
	include_once("../system/libraries/campaign_send.inc.php");
	// Include the required modules - FINISHED

	// Retrieve campaign information - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE CampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayCampaign = mysql_fetch_assoc($ResultSet);
	// Retrieve campaign information - FINISHED
	
	// Retrieve campaign SMTP settings - STARTED
	$ArraySMTP = array();
	if ($ArrayLoggedAdmin['ForcedSendingMethod'] != 'N/A')
		{
		if ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Qmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'QMail',
							);
			}
		elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Sendmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'Sendmail',
							);
			}
		elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'Local MTA')
			{
			$ArraySMTP = array(
							'Type'		=>	'MTA',
							);
			}
		elseif ($ArrayLoggedAdmin['ForcedSendingMethod'] == 'SMTP Server')
			{
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "WHERE tblSMTP.SMTPServerID = '".$ArrayLoggedAdmin['ForcedSendingMethodID']."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TMPArraySMTP = mysql_fetch_assoc($ResultSet);
	
			$ArraySMTP = array(
							'Type'		=>	'SMTP',
							'Host'		=>	$TMPArraySMTP['ServerHost'],
							'Port'		=>	$TMPArraySMTP['ServerPort'],
							'Auth'		=>	$TMPArraySMTP['ServerAuth'],
							'Username'	=>	$TMPArraySMTP['ServerUsername'],
							'Password'	=>	$TMPArraySMTP['ServerPassword'],
							'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
							);
			}
		}
	else
		{
		if ($ArrayCampaign['SendEngine'] == 'SMTP')
			{
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TMPArraySMTP = mysql_fetch_assoc($ResultSet);
	
			$ArraySMTP = array(
							'Type'		=>	'SMTP',
							'Host'		=>	$TMPArraySMTP['ServerHost'],
							'Port'		=>	$TMPArraySMTP['ServerPort'],
							'Auth'		=>	$TMPArraySMTP['ServerAuth'],
							'Username'	=>	$TMPArraySMTP['ServerUsername'],
							'Password'	=>	$TMPArraySMTP['ServerPassword'],
							'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'MTA')
			{
			$ArraySMTP = array(
							'Type'		=>	'MTA',
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'QMail')
			{
			$ArraySMTP = array(
							'Type'		=>	'QMail',
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'Sendmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'Sendmail',
							);
			}
		}
	// Retrieve campaign SMTP settings - FINISHED

	// Retrieve attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayAttachments[] = array(
									"AttachmentID"		=>	$EachRow['AttachmentID'],
									"FileName"			=>	$EachRow['FileName'],
									"FileSize"			=>	$EachRow['FileSize'],
									);
		}
	// Retrieve attachments - FINISHED

	// Retrieve content - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentMainURL']));
			$ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentAlternateURL']));
			$ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve content - FINISHED

	// Add administrator header and footer to the content - STARTED
		include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$ArrayContent = AddAdministratorHeaderFooter($ArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
	// Add administrator header and footer to the content - FINISHED

	// Retrieve from, reply to and return path emails - STARTED
	if ($ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);
	// Retrieve from, reply to and return path emails - FINISHED

	// Retrieve a random member account (if enabled) - STARTED
	if ($ArrayContent['Personalize'] == 'Yes')
		{
		// Retrieve the mail lists of the campaign and check if any outsourced list exists - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblLists ON tblAssign.RelMailListID = tblLists.MailListID WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblLists.SourceType='Outsourced - MySQL'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Retrieve the mail lists of the campaign and check if any outsourced list exists - FINISHED
		
		// If all lists are local sourced, then retrieve a random member - STARTED
		if ($TotalFound < 1)
			{
			// Retrieve mail lists - STARTED
			$ArrayMailLists = array();
			
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
			$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblMailLists.SourceType='Local'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayMailLists[]	=	$EachRow['MailListID'];
				}
			// Retrieve mail lists - FINISHED

			$SQLQuery  = "SELECT tblMembers.* FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
			$SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
			$SQLQuery .= "WHERE tblAssign.RelMailListID = '".$ArrayMailLists[0]."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);
				
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblMembership WHERE RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMembership = mysql_fetch_assoc($ResultSet);
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailList WHERE MailListID='".$ArrayMembership['RelMailListID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblLists ON tblAssign.RelMailListID = tblLists.MailListID WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."' AND tblLists.SourceType='Outsourced - MySQL' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet);

			// Connect to outsourced mail list database - STARTED
			mysql_close();
			
			$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
				mysql_select_db($ArrayMailList['OutSourceDBName']);
			// Connect to outsourced mail list database - FINISHED
			
				$SQLQueryPart = ($ArrayMailList['OutSourceDBWhereQuery'] != '' ? ' WHERE '.$ArrayMailList['OutSourceDBWhereQuery'] : '');
			$SQLQuery = "SELECT * FROM ".$ArrayMailList['OutSourceDBTable'].$SQLQueryPart." LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet);

			// For outsourced members, the email address field may be different. Make a new field with "Email" name - STARTED
			$ArrayMember['Email']	=	$ArrayMember[$ArrayMailList['OutSourceDBEmailFieldName']];
			// For outsourced members, the email address field may be different. Make a new field with "Email" name - FINISHED
			
			// Connect to oemPro database - STARTED
			mysql_close();
		
				// Connect to oemPro MySQL server and database
				$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
				// Connect to outsourced MySQL list - FINISHED
			// Connect to oemPro database - FINISHED
			}
		// If all lists are local sourced, then retrieve a random member - FINISHED
		}
	// Retrieve a random member account (if enabled) - FINISHED

	// Define email properties - STARTED
		// Define subject - STARTED
		$TMPSubject = $ArrayContent['Subject'];
		// Define subject - FINISHED

			// Analyze subject. If it is in "multi-mode", explode into array - STARTED
			if (count(explode("\n", $TMPSubject)) > 1)
				{
				$TMPSubject = explode("\n", $TMPSubject);
				}
			// Analyze subject. If it is in "multi-mode", explode into array - FINISHED

			// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - STARTED
			if (strtolower(gettype($TMPSubject)) == 'array')
				{
				$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
				}
			// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - FINISHED
		
		// Define content - STARTED
		if (count($ArrayNewsletter) > 0)
			{
			// CMS based content
			$TMPPriority 			= $ArrayNewsletter['Priority'];
			$TMPCharSet				= $ArrayNewsletter['CharSet'];
			$TMPContentType			= $ArrayNewsletter['ContentType'];

			if ($TMPContentType == 'Html')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}
			elseif ($TMPContentType == 'Both')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				}
			elseif ($TMPContentType == 'Plain')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}

			// Add administrator header and footer to the content - STARTED
				include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
				$TMPArrayContent = array(
										"ContentMain"		=> $TMPContentMain,
										"ContentAlternate"	=> $TMPContentAlternate,
										"ContentType"		=> $TMPContentType,
										);
			$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
				$TMPContentMain = $TMPArrayContent['ContentMain'];
				$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
			// Add administrator header and footer to the content - FINISHED
			}
		else
			{
			// Traditional template or quick mode
			$TMPPriority 			= $ArrayContent['Priority'];
			$TMPCharSet				= $ArrayContent['CharSet'];
			$TMPContentType			= $ArrayContent['ContentType'];
			$TMPContentMain			= $ArrayContent['ContentMain'];
			$TMPContentAlternate	= $ArrayContent['ContentAlternate'];
			}
		// Define content - FINISHED

		// Run external plug-ins which are content specific - Start
		$ArrayPlugInReturn = RunPlugInContentProcesses($TMPContentMain, $TMPContentAlternate, $ArrayLoggedAdmin, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig);
			$TMPContentMain		=	$ArrayPlugInReturn[0];
			$TMPContentAlternate=	$ArrayPlugInReturn[1];
		// Run external plug-ins which are content specific - End

		// Process on email content (personalization, tracking links, tracking reads) - STARTED
		if ($ArrayContent['Personalize'] == 'Yes')
			{
				$ArrayPrefixes		=	array(
											"Sender",
											"Member",
											"Membership",
											"Campaign"
											);
				$ArrayInformation	=	array(
											$ArrayLoggedAdmin,
											$ArrayMember,
											$ArrayMembership,
											$ArrayCampaign,
											);											
			$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
			}
	
			$ArrayCampaignStatistics = array();
		$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
		$TMPContentMain			=	PersonalizeCampaignTags($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
		$TMPContentAlternate	=	PersonalizeCampaignTags($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');

		// Perform global personalization - STARTED
		$TMPSubject				= PerformGlobalCampaignPersonalization($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		$TMPContentMain			= PerformGlobalCampaignPersonalization($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		$TMPContentAlternate	= PerformGlobalCampaignPersonalization($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		// Perform global personalization - FINISHED

		// Process on email content (personalization, tracking links, tracking reads) - FINISHED

		// Remove all unwanted HTML codes from email body - STARTED
		if (($ArrayContent['ContentType'] == 'Html') || ($ArrayContent['ContentType'] == 'Both'))
			{
			include_once("./system/libraries/campaign_send.inc.php");
			$TMPContentMain = RemoveUnwantedHTMLCodes($TMPContentMain, $ArrayConfig);
			}
		// Remove all unwanted HTML codes from email body - FINISHED

	$ArrayEmailProperties = array();
		$ArrayEmailProperties	= array(
										"CampaignID"		=>	$ArrayCampaign['CampaignID'],
										"EmbedObjects"		=>	($ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
										"FromEmail"			=>	$ArrayFromEmail['EmailAddress'],
										"FromName"			=>	$ArrayFromEmail['Name'],
										"ReplyToEmail"		=>	$ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"		=>	$ArrayReplyToEmail['Name'],
										"ReturnPathEmail"	=>	($ArrayLoggedAdmin['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayLoggedAdmin['ReturnPathEmail']),
										"ToName"			=>	'',
										"ToEmail"			=>	$ArrayPostVariables['FormValue_Email'],
										"Priority"			=>	$TMPPriority,
										"CharSet"			=>	$TMPCharSet,
										"Subject"			=>	$ArrayLanguage["Texts"]["00159"]." ".$TMPSubject,
										"ContentType"		=>	$TMPContentType,
										"ContentMain"		=>	$TMPContentMain,
										"ContentAlternate"	=>	$TMPContentAlternate,
										"Attachments"		=>	$ArrayAttachments,
										"SendEngines"		=>	$ArraySMTP,
										);
	// Define email properties - FINISHED    
	// Prepare and send mailing engine - STARTED
	$ArrayStatus = SendPreviewEmail($ArrayEmailProperties, $ArrayConfig, $ArrayLoggedAdmin, $ArrayLanguage);
	// Prepare and send mailing engine - FINISHED
    
	return $ArrayStatus;
	}

//########################################################################################
// Send preview email
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendPreviewEmail($ArrayEmailProperties, $ArrayConfig, $ArrayLoggedAdmin, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists 	= false;
	$ArrayError		= array();
	// Initialize variables - FINISHED

	// Include the required modules - STARTED
	include_once("../system/classes/class_mailer.php");
	include_once("../system/classes/class_smtp.php");
	// Include the required modules - FINISHED

	$ObjectMailer 			= new phpmailer();

	// Define send engine - STARTED
	if ($ArrayEmailProperties['SendEngines']['Type'] == 'Sendmail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['SendmailPath'];
	
		$ObjectMailer->IsSendmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'QMail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['QMailPath'];
		$ObjectMailer->IsQmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'MTA')
		{
		$ObjectMailer->IsMail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'SMTP')
		{
			$ObjectMailer->Host		=	$ArrayEmailProperties['SendEngines']['Host'];
			$ObjectMailer->Port		=	$ArrayEmailProperties['SendEngines']['Port'];
			$ObjectMailer->SMTPAuth	=	($ArrayEmailProperties['SendEngines']['Auth'] == 'Authorization' ? true : false);
			$ObjectMailer->Username	=	$ArrayEmailProperties['SendEngines']['Username'];
			$ObjectMailer->Password	=	$ArrayEmailProperties['SendEngines']['Password'];
			$ObjectMailer->Timeout	=	$ArrayEmailProperties['SendEngines']['TimeOutSec'];
			$ObjectMailer->SMTPKeepAlive	=	false;
		$ObjectMailer->IsSMTP();
		}
	// Define send engine - FINISHED

	// Set email addresses - STARTED
	$ObjectMailer->From			= $ArrayEmailProperties['FromEmail'];
	$ObjectMailer->FromName		= $ArrayEmailProperties['FromName'];
	$ObjectMailer->ReturnPath	= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->Sender		= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->AddReplyTo($ArrayEmailProperties['ReplyToEmail'], $ArrayEmailProperties['ReplyToName']);
	$ObjectMailer->AddAddress(strtolower($ArrayEmailProperties['ToEmail']), strtolower($ArrayEmailProperties['ToName']));
	// Set email addresses - FINISHED
	
	// Set email properties - STARTED
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayEmailProperties['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= substr($ArrayEmailProperties["CharSet"], 0, strlen($ArrayEmailProperties["CharSet"]) - 5);
	$ObjectMailer->Subject	= $ArrayEmailProperties['Subject'];
	$link_start = substr($ArrayEmailProperties['ContentMain'], strpos($ArrayEmailProperties['ContentMain'], "http://".$_SERVER['HTTP_HOST'].'/unsubscribe.php?'));
    $unsubscribe = substr($link_start, 0, strpos($link_start,'"'));
    $ObjectMailer->listUnsubscribe = $unsubscribe;

    $ObjectMailer->AddCustomHeader("X-Mailer-MsgId: ".base64_encode(strtolower($ArrayEmailProperties['ToEmail'])));
	$ObjectMailer->AddCustomHeader("X-Mailer-CSID: ".base64_encode($ArrayEmailProperties['CampaignID'].'_0'));
	// Set email properties - FINISHED

	// Set email body - STARTED
	if ($ArrayEmailProperties["ContentType"] == 'Plain')
		{
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= stripslashes($ArrayEmailProperties['ContentMain']);
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Html')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= stripslashes($ArrayEmailProperties['ContentMain']);
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Both')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= stripslashes($ArrayEmailProperties['ContentMain']);
		$ObjectMailer->AltBody	= stripslashes($ArrayEmailProperties['ContentAlternate']);
		}
	// Set email body - FINISHED

	// If enabled, embed images to the email - STARTED
	if (($ArrayEmailProperties["ContentType"] == 'Html') || ($ArrayEmailProperties["ContentType"] == 'Both'))
		{
		if ($ArrayEmailProperties['EmbedObjects'] == true)
			{
				$Content = $ObjectMailer->Body;
			$ArrayReturn = EmbedObjects($ObjectMailer, $Content, $ArrayConfig);
			
			$ObjectMailer 			= $ArrayReturn[0];
			$ObjectMailer->Body		= $ArrayReturn[1];
			}
		}
	// If enabled, embed images to the email - FINISHED
	// Insert attachments - STARTED

	foreach ($ArrayEmailProperties['Attachments'] as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$ArrayConfig['Paths']['Data']."/campaign_attachments/".$ArrayEmailProperties['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$ObjectMailer->AddAttachment($AttachmentPath, $AttachmentName);
		}
	// Insert attachments - FINISHED
	
	// Send the email - STARTED
	$ObjectMailer->Send();
	// Send the email - FINISHED
	if (strpos($_SERVER['HTTP_REFERER'], 'list-campaigns') !== false || strpos($_SERVER['HTTP_REFERER'], 'campaign-new') !== false)
    {
        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit();
    }
	// Detect if any error occured or not - STARTED
	if ($ObjectMailer->ErrorInfo != '')
		{
		$ArrayError = array(true, $ObjectMailer->ErrorInfo);
		}
	// Detect if any error occured or not - FINISHED

	return $ArrayError;
	}
	
//########################################################################################
// Integrates CMS parts into content body
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function IntegrateCMSPartsToContent($Content, $ArrayCMSParts)
	{
	$ArrayPattern	= array();
	$ArrayReplace	= array();

	foreach ($ArrayCMSParts as $Key => $ArrayValues)
		{
		$ArrayPattern[]	= "%INSERT:CMS:".$ArrayValues['Key'].":PLAIN%";
		$ArrayPattern[]	= "%INSERT:CMS:".$ArrayValues['Key'].":HTML%";

		$ArrayReplace[]	= $ArrayValues['Content'];
		$ArrayReplace[]	= $ArrayValues['Content'];
		}

	$Content = str_replace($ArrayPattern, $ArrayReplace, $Content);
	
	return $Content;
	}
	
?>