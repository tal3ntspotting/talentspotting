<?php

function getOpenings($array, $start, $end)
{
    $startArray = explode("-", $start);
    $endArray = explode("-", $end);
    
    $start_date = gregoriantojd($startArray[1], $startArray[2], $startArray[0]);
    $end_date = gregoriantojd($endArray[1], $endArray[2], $endArray[0]);

    $difference = $end_date - $start_date;
    
    $weekDays = array(1,2,3,4,5,6,7);
    
    $campaigns = array_keys($array);
    foreach ($campaigns as $key=>$value)
    {
        for ($i=0; $i<=$difference;$i++)
        {
            $date = date("Y-m-d", strtotime("+ $i day",strtotime($start)));
            $report['campaigns'][$value][date("N", strtotime($date))] += $array[$value][$date];
        }
        ksort($report['campaigns'][$value]);
    }
    
    for ($i=0; $i<=$difference;$i++)
    {
        $date = date("Y-m-d", strtotime("+ $i day",strtotime($start)));
        $occurences[date("N", strtotime($date))] += 1;
    }
    
    foreach ($report['campaigns'] as $key=>$value)
    {
        foreach ($value as $k=>$v)
        {
            $report['totals'][$k] += $v;     
        }
        
    }
    
    foreach($report['totals'] as $key=>$value)
    {
        $report['totals'][$key] = number_format($value / $occurences[$key], 2);
    }

    return $report;
}

?>
