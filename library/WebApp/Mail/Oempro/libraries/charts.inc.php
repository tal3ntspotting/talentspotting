<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Octeth Chart Engine: Generates the required XML code for generating the charts (Bar Chart)
function GenerateBarChartXML($ArrayCategories, $ArrayData, $Caption, $MultiSeries, $ShowTitle, $Style, $HighDetail, $LegendDisplay, $XAxisShowLabels, $YAxisShowLabels, $YAxisMaxValue, $YAxisShowMinOrMaxValues, $LegendFontSize, $XAxisFontSize, $YAxisFontSize, $ArrayConfig, $ArrayLanguage)
	{
	header("Content-type: text/xml");
	
	$XML = '';

	$XML = '<?xml version="1.0" encoding="UTF-8"?>'."\n";

	$XML .= '<octethgrapher>'."\n";
	$XML .= '	<settings>'."\n";
	$XML .= '		<graph name="'.$Caption.'" showtitle="'.$ShowTitle.'" style="'.$Style.'" highdetail="'.$HighDetail.'" />'."\n";
	$XML .= '		<legend display="'.$LegendDisplay.'"  fontsize="'.$LegendFontSize.'" />'."\n";

	if ($Style == 'pie')
		{
		$XML .= '<nodevalue display="true" style="onmouseover" />'."\n";
		}

	$XML .= '	</settings>'."\n";
	$XML .= '	<data>'."\n";

	if ($Style != 'pie')
		{
		$XML .= '		<properties>'."\n";
		$XML .= '			<xaxis showlabels="'.$XAxisShowLabels.'" fontsize="'.$XAxisFontSize.'">'."\n";
	
		foreach ($ArrayCategories as $Index=>$Name)
			{
			$XML .= '				<node>'.$Name.'</node>'."\n";
			}
	
		$XML .= '			</xaxis>'."\n";
		$XML .= '			<yaxis showlabels="'.$YAxisShowLabels.'" fontsize="'.$YAxisFontSize.'" maxvalue="'.$YAxisMaxValue.'" steps="10" showminorvalues="'.$YAxisShowMinOrMaxValues.'" />'."\n";
		$XML .= '		</properties>'."\n";
		}
		
	foreach ($ArrayData as $Index=>$ArrayValues)
		{
		$XML .= '		<node label="'.$ArrayValues['Series1Name'].'" color="'.$ArrayValues['Color1'].'" value="'.$ArrayValues['Value1'].'" xaxis="'.$ArrayValues['Name'].'" />'."\n";
		if ($MultiSeries == true)
			{
			$XML .= '		<node label="'.$ArrayValues['Series2Name'].'" color="'.$ArrayValues['Color2'].'" value="'.$ArrayValues['Value2'].'" xaxis="'.$ArrayValues['Name'].'" />'."\n";
			}
		}

	$XML .= '	</data>'."\n";
	$XML .= '</octethgrapher>'."\n";

	return $XML;
	}

//########################################################################################
// Generates the required XML code for generating the charts (AF Area chart) (with multiple series)
function GenerateAreaXMLMulti($ArrayCategories, $ArrayData, $Caption, $SubCaption, $YAxisName, $XAxisName, $ArrayConfig, $ArrayLanguage)
	{
	$XML  = "";
	$XML .= "<graph bgcolor='FFFFFF' caption='".$Caption."' subCaption='".$SubCaption."' yaxisname='".$YAxisName."' xaxisname='".$XAxisName."' canvasbgcolor='FFFFFF' gridbgcolor='CCCCCC' hovercapbg='FFCC00' hovercapborder='FF0000' divlinecolor='FFFFFF' thousandSeparator=',' showCanvas='0' animation='0' decimalPrecision='0'>\n";

	$XML .= "<categories>\n";
	foreach ($ArrayCategories as $Index=>$Name)
		{
		$XML .= "<category name='".$Name."' />\n";
		}
	$XML .= "</categories>\n";

	$XML .= "<dataset seriesname='".$ArrayData[0]['Series1Name']."' color='660000' showValues='1'>\n";
	foreach ($ArrayData as $Index=>$ArrayEach)
		{
		$XML .= "<set value='".$ArrayEach['Value1']."' />\n";
		}
	$XML .= "</dataset>\n";

	$XML .= "<dataset seriesname='".$ArrayData[0]['Series2Name']."' color='003399' showValues='1'>\n";
	foreach ($ArrayData as $Index=>$ArrayEach)
		{
		$XML .= "<set value='".$ArrayEach['Value2']."' />\n";
		}
	$XML .= "</dataset>\n";

	$XML .= "</graph>\n";

	return $XML;
	}

//########################################################################################
// Generates the required XML code for generating the charts (AF Area chart) (with two series)
function GenerateAreaXML($ArrayCategories, $ArrayData, $Caption, $SubCaption, $YAxisName, $XAxisName, $ArrayConfig, $ArrayLanguage)
	{
	$XML  = "";
	$XML .= "<graph bgcolor='FFFFFF' caption='".$Caption."' subCaption='".$SubCaption."' yaxisname='".$YAxisName."' xaxisname='".$XAxisName."' canvasbgcolor='FFFFFF' gridbgcolor='CCCCCC' hovercapbg='FFCC00' hovercapborder='FF0000' divlinecolor='FFFFFF' thousandSeparator=',' showCanvas='0' animation='0' decimalPrecision='0'>\n";

	$XML .= "<categories>\n";
	foreach ($ArrayCategories as $Index=>$Name)
		{
		$XML .= "<category name='".$Name."' />\n";
		}
	$XML .= "</categories>\n";

	$XML .= "<dataset seriesname='".$ArrayData[0]['Series1Name']."' color='660000' showValues='1'>\n";
	foreach ($ArrayData as $Index=>$ArrayEach)
		{
		$XML .= "<set value='".$ArrayEach['Value1']."' />\n";
		}
	$XML .= "</dataset>\n";

	$XML .= "<dataset seriesname='".$ArrayData[0]['Series2Name']."' color='003399' showValues='1'>\n";
	foreach ($ArrayData as $Index=>$ArrayEach)
		{
		$XML .= "<set value='".$ArrayEach['Value2']."' />\n";
		}
	$XML .= "</dataset>\n";

	$XML .= "</graph>\n";

	return $XML;
	}
	
//########################################################################################
// Generates the required XML code for generating the charts (Line chart)
function GenerateLineXML($ArrayData, $Caption, $SubCaption, $YAxisName, $XAxisName, $ArrayConfig, $ArrayLanguage)
	{
	$XML  = "";
	$XML .= "<graph bgcolor='FFFFFF' caption='".$Caption."' subCaption='".$SubCaption."' yaxisname='".$YAxisName."' xaxisname='".$XAxisName."' canvasbgcolor='FFFFFF' gridbgcolor='CCCCCC' hovercapbg='FFCC00' hovercapborder='FF0000' divlinecolor='FFFFFF' thousandSeparator=',' showCanvas='0' animation='0' rotateNames='1' decimalPrecision='0'>\n";

	foreach ($ArrayData as $Index=>$ArrayEach)
		{
		$XML .= "<set name='".$ArrayEach['Name']."' value='".$ArrayEach['Value']."' color='000000' link='' />\n";
		}
	$XML .= "</graph>\n";

	return $XML;
	}

?>