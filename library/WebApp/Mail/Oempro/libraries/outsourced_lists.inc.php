<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Returns the mail list aliases in array
function ReturnMailListAliases($ArrayMailList, $ArrayConfig)
	{
	// Connect to outsourced MySQL list - STARTED
	mysql_close();
	
	$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
		mysql_select_db($ArrayMailList['OutSourceDBName']);
	// Connect to outsourced MySQL list - FINISHED

	// Retrieve the fields and list them - STARTED
	if ($ArrayMailList['OutSourceDBQueryType'] == 'Advanced')
		{
		$SQLQuery = $ArrayMailList['OutSourceDBFullSQLQuery'];
		$ResultSet = mysql_query($SQLQuery);
		}
	else
		{
		$SQLQuery = "DESCRIBE ".$ArrayMailList['OutSourceDBTable'];
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Connect to oemPro MySQL server and database
	mysql_close();

	$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
	// Connect to outsourced MySQL list - FINISHED

	$ArrayMailListAliases = array();

	if ($ArrayMailList['OutSourceDBQueryType'] == 'Advanced')
		{
		for ($TMPCounter = 0; $TMPCounter < mysql_num_fields($ResultSet); $TMPCounter++)
			{
			$ObjectColumns = mysql_fetch_field($ResultSet, $TMPCounter);
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_aliases WHERE RelMailListID='".$ArrayMailList['MailListID']."' AND FieldName='".$ObjectColumns->name."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayAlias = mysql_fetch_assoc($ResultSet2);
		
			if ($ArrayAlias['Alias'] == '')
				{
				$ArrayMailListAliases[$ObjectColumns->name] = $EachRow['Field'];
				}
			else
				{
				$ArrayMailListAliases[$ObjectColumns->name] = $ArrayAlias['Alias'];
				}
			}
		}
	else
		{
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_aliases WHERE RelMailListID='".$ArrayMailList['MailListID']."' AND FieldName='".$EachRow['Field']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayAlias = mysql_fetch_assoc($ResultSet2);
		
			if ($ArrayAlias['Alias'] == '')
				{
				$ArrayMailListAliases[$EachRow['Field']] = $EachRow['Field'];
				}
			else
				{
				$ArrayMailListAliases[$EachRow['Field']] = $ArrayAlias['Alias'];
				}
			}
		}
	
	return $ArrayMailListAliases;
	}
	
?>