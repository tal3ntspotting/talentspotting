<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Forward (send) the campaigns to entered email addresses
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ForwardCampaign($ArrayRecipients, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayMember, $Email, $ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ArrayStatus = array();
	// Initialize variables - FINISHED

	// Include the required modules - STARTED
	include_once("./system/libraries/personalization.inc.php");
	include_once("./system/libraries/campaign_send.inc.php");
	include_once("./system/libraries/campaign_preview.inc.php");
	// Include the required modules - FINISHED

	// Retrieve campaign SMTP settings - STARTED
	$ArraySMTP = array();
	if ($ArrayCampaign['SendEngine'] == 'SMTP')
		{
		$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
		$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
		$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."' LIMIT 0,1";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TMPArraySMTP = mysql_fetch_assoc($ResultSet);

		$ArraySMTP = array(
						'Type'		=>	'SMTP',
						'Host'		=>	$TMPArraySMTP['ServerHost'],
						'Port'		=>	$TMPArraySMTP['ServerPort'],
						'Auth'		=>	$TMPArraySMTP['ServerAuth'],
						'Username'	=>	$TMPArraySMTP['ServerUsername'],
						'Password'	=>	$TMPArraySMTP['ServerPassword'],
						'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'MTA')
		{
		$ArraySMTP = array(
						'Type'		=>	'MTA',
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'QMail')
		{
		$ArraySMTP = array(
						'Type'		=>	'QMail',
						);
		}
	elseif ($ArrayCampaign['SendEngine'] == 'Sendmail')
		{
		$ArraySMTP = array(
						'Type'		=>	'Sendmail',
						);
		}
	// Retrieve campaign SMTP settings - FINISHED

	// Retrieve attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayAttachments[] = array(
									"AttachmentID"		=>	$EachRow['AttachmentID'],
									"FileName"			=>	$EachRow['FileName'],
									"FileSize"			=>	$EachRow['FileSize'],
									);
		}
	// Retrieve attachments - FINISHED

	// Retrieve content - STARTED
	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentMainURL']));
			$ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($ArrayContent['ContentAlternateURL']));
			$ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);
			}
		else
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}
	// Retrieve content - FINISHED

	// Add administrator header and footer to the content - STARTED
		include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$ArrayContent = AddAdministratorHeaderFooter($ArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
	// Add administrator header and footer to the content - FINISHED

	// Retrieve from, reply to and return path emails - STARTED
	if ($ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);
	// Retrieve from, reply to and return path emails - FINISHED

	// Check if the campaign owner administrator has a pre-defined return-path email address - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayCampaignAdministrator = mysql_fetch_assoc($ResultSet);
	// Check if the campaign owner administrator has a pre-defined return-path email address - FINISHED

	// Define email properties - STARTED
		// Define subject - STARTED
		$TMPSubject = $ArrayContent['Subject'];
		
			// Analyze subject. If it is in "multi-mode", explode into array - STARTED
			if (count(explode("\n", $TMPSubject)) > 1)
				{
				$TMPSubject = explode("\n", $TMPSubject);
				}
			// Analyze subject. If it is in "multi-mode", explode into array - FINISHED

			// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - STARTED
			if (strtolower(gettype($TMPSubject)) == 'array')
				{
				$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
				}
			// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - FINISHED
		
		// Define subject - FINISHED

		// Define content - STARTED
		if (count($ArrayNewsletter) > 0)
			{
			// CMS based content
			$TMPPriority 			= $ArrayNewsletter['Priority'];
			$TMPCharSet				= $ArrayNewsletter['CharSet'];
			$TMPContentType			= $ArrayNewsletter['ContentType'];

			if ($TMPContentType == 'Html')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}
			elseif ($TMPContentType == 'Both')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				}
			elseif ($TMPContentType == 'Plain')
				{
				$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
				$TMPContentAlternate	= '';
				}

			// Add administrator header and footer to the content - STARTED
				include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
				$TMPArrayContent = array(
										"ContentMain"		=> $TMPContentMain,
										"ContentAlternate"	=> $TMPContentAlternate,
										"ContentType"		=> $TMPContentType,
										);
			$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
				$TMPContentMain = $TMPArrayContent['ContentMain'];
				$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
			// Add administrator header and footer to the content - FINISHED
			}
		else
			{
			// Traditional template or quick mode
			$TMPPriority 			= $ArrayContent['Priority'];
			$TMPCharSet				= $ArrayContent['CharSet'];
			$TMPContentType			= $ArrayContent['ContentType'];
			$TMPContentMain			= $ArrayContent['ContentMain'];
			$TMPContentAlternate	= $ArrayContent['ContentAlternate'];
			}
		// Define content - FINISHED

		// If top messages are defined for both html and plain parts of the forwarded email, insert them - STARTED
		if (($TMPContentType == 'Html') || ($TMPContentType == 'Both'))
			{
			if ($ArrayConfig['Settings']['ForwardNoticeHTML'] != '')
				{
				$TMPContentMain		= "<div>".$ArrayConfig['Settings']['ForwardNoticeHTML']."</div><br><br>".$TMPContentMain;
				}
			if ($ArrayConfig['Settings']['ForwardNoticePlain'] != '')
				{
				$TMPContentAlternate	= $ArrayConfig['Settings']['ForwardNoticePlain']."\n\n".$TMPContentAlternate;
				}
			}
		else
			{
			if ($ArrayConfig['Settings']['ForwardNoticePlain'] != '')
				{
				$TMPContentMain	= $ArrayConfig['Settings']['ForwardNoticePlain']."\n\n".$TMPContentMain;
				}
			}
		// If top messages are defined for both html and plain parts of the forwarded email, insert them - FINISHED

		// Run external plug-ins which are content specific - Start
		$ArrayPlugInReturn = RunPlugInContentProcesses($TMPContentMain, $TMPContentAlternate, array(), $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig);
			$TMPContentMain		=	$ArrayPlugInReturn[0];
			$TMPContentAlternate=	$ArrayPlugInReturn[1];
		// Run external plug-ins which are content specific - End

		// Process on email content (personalization, tracking links, tracking reads) - STARTED
		if ($ArrayContent['Personalize'] == 'Yes')
			{
				$ArrayPrefixes		=	array(
											"Sender",
											"Member",
											"Membership",
											"Campaign"
											);
				$ArrayInformation	=	array(
											$ArrayCampaignAdministrator,
											$ArrayMember,
											$ArrayMembership,
											$ArrayCampaign,
											);											
			$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
			$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
			}
	
		$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, true, '%');
		$TMPContentMain			=	PersonalizeCampaignTagsForPublic($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, false, true, '%');
		$TMPContentAlternate	=	PersonalizeCampaignTagsForPublic($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, false, true, '%');

		// Perform global personalization - STARTED
		$TMPSubject				= PerformGlobalCampaignPersonalization($TMPSubject, $TMPContentType, 'Subject', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		$TMPContentMain			= PerformGlobalCampaignPersonalization($TMPContentMain, $TMPContentType, 'Main', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		$TMPContentAlternate	= PerformGlobalCampaignPersonalization($TMPContentAlternate, $TMPContentType, 'Alternate', $ArrayConfig, $ArrayLanguage, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $IsDemo, $Tag = '%');
		// Perform global personalization - FINISHED

		// Process on email content (personalization, tracking links, tracking reads) - FINISHED

	// Check if the campaign owner administrator has a pre-defined return-path email address - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayCampaignAdministrator = mysql_fetch_assoc($ResultSet);
	// Check if the campaign owner administrator has a pre-defined return-path email address - FINISHED

	$ArrayEmailProperties = array();
		$ArrayEmailProperties	= array(
										"CampaignID"		=>	$ArrayCampaign['CampaignID'],
										"CampaignStatisticsID"		=>	$ArrayCampaignStatistics['CampaignStatisticsID'],
										"EmbedObjects"		=>	($ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
										"FromEmail"			=>	$ArrayFromEmail['EmailAddress'],
										"FromName"			=>	$ArrayFromEmail['Name'],
										"ReplyToEmail"		=>	$ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"		=>	$ArrayReplyToEmail['Name'],
										"ReturnPathEmail"	=>	($ArrayCampaignAdministrator['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayCampaignAdministrator['ReturnPathEmail']),
										"ToName"			=>	$ArrayMember['Email'],
										"ToEmail"			=>	$ArrayMember['Email'],
										"Priority"			=>	$TMPPriority,
										"CharSet"			=>	$TMPCharSet,
										"Subject"			=>	$ArrayLanguage["Texts"]["00260"]." ".$TMPSubject." ".$ArrayLanguage["Texts"]["00261"],
										"ContentType"		=>	$TMPContentType,
										"ContentMain"		=>	$TMPContentMain,
										"ContentAlternate"	=>	$TMPContentAlternate,
										"Attachments"		=>	$ArrayAttachments,
										"SendEngines"		=>	$ArraySMTP,
										);

	$ArrayEmailProperties['Subject'] = str_replace('_ForwardedBy_', ($ArrayMember['Email'] == '' ? $Email : $ArrayMember['Email']), $ArrayEmailProperties['Subject']);
	// Define email properties - FINISHED

	// Prepare and forward to each recipient - STARTED
	foreach ($ArrayRecipients as $ArrayEachRecipient)
		{
			$ArrayEmailProperties['ToName']		=	$ArrayEachRecipient['Name'];
			$ArrayEmailProperties['ToEmail']	=	$ArrayEachRecipient['Email'];
		$ArrayStatus = ForwardEmail($ArrayEmailProperties, $ArrayConfig, $ArrayMember, $ArrayLanguage);
		}
	// Prepare and send mailing engine - FINISHED

	return array();
	}

//########################################################################################
// Send forwarded email
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ForwardEmail($ArrayEmailProperties, $ArrayConfig, $ArrayMember, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists 	= false;
	$ArrayError		= array();
	// Initialize variables - FINISHED

	// Include the required modules - STARTED
	include_once("./system/classes/class_mailer.php");
	include_once("./system/classes/class_smtp.php");
	// Include the required modules - FINISHED

	$ObjectMailer 			= new phpmailer();

	// Define send engine - STARTED
	if ($ArrayEmailProperties['SendEngines']['Type'] == 'Sendmail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['SendmailPath'];
		$ObjectMailer->IsSendmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'QMail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['QMailPath'];
		$ObjectMailer->IsQmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'MTA')
		{
		$ObjectMailer->IsMail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'SMTP')
		{
			$ObjectMailer->Host		=	$ArrayEmailProperties['SendEngines']['Host'];
			$ObjectMailer->Port		=	$ArrayEmailProperties['SendEngines']['Port'];
			$ObjectMailer->SMTPAuth	=	($ArrayEmailProperties['SendEngines']['Auth'] == 'Authorization' ? true : false);
			$ObjectMailer->Username	=	$ArrayEmailProperties['SendEngines']['Username'];
			$ObjectMailer->Password	=	$ArrayEmailProperties['SendEngines']['Password'];
			$ObjectMailer->Timeout	=	$ArrayEmailProperties['SendEngines']['TimeOutSec'];
			$ObjectMailer->SMTPKeepAlive	=	false;
		$ObjectMailer->IsSMTP();
		}
	// Define send engine - FINISHED

	// Set email addresses - STARTED
	$ObjectMailer->From			= $ArrayEmailProperties['FromEmail'];
	$ObjectMailer->FromName		= $ArrayEmailProperties['FromName'];
	$ObjectMailer->ReturnPath	= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->Sender		= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->AddReplyTo($ArrayAutoResponder['ReplyToEmail'], $ArrayAutoResponder['ReplyToName']);
	$ObjectMailer->AddAddress(strtolower($ArrayEmailProperties['ToEmail']), $ArrayEmailProperties['ToName']);
	// Set email addresses - FINISHED

	// Set email properties - STARTED
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayEmailProperties['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= substr($ArrayEmailProperties["CharSet"], 0, strlen($ArrayEmailProperties["CharSet"]) - 5);
	$ObjectMailer->Subject	= $ArrayEmailProperties['Subject'];
	$ObjectMailer->AddCustomHeader("X-Mailer-MsgId: ".base64_encode(strtolower($ArrayMember['Email'])));
	$ObjectMailer->AddCustomHeader("X-Mailer-CSID: ".base64_encode($ArrayEmailProperties['CampaignID'].'_'.$ArrayEmailProperties['CampaignStatisticsID']));
	// Set email properties - FINISHED

	// Set email body - STARTED
	if ($ArrayEmailProperties["ContentType"] == 'Plain')
		{
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $ArrayEmailProperties['ContentMain'];
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Html')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $ArrayEmailProperties['ContentMain'];
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Both')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $ArrayEmailProperties['ContentMain'];
		$ObjectMailer->AltBody	= $ArrayEmailProperties['ContentAlternate'];
		}
	// Set email body - FINISHED

	// If enabled, embed images to the email - STARTED
	if (($ArrayEmailProperties["ContentType"] == 'Html') || ($ArrayEmailProperties["ContentType"] == 'Both'))
		{
		if ($ArrayEmailProperties['EmbedObjects'] == true)
			{
				$Content = $ObjectMailer->Body;
			$ArrayReturn = EmbedObjects($ObjectMailer, $Content, $ArrayConfig);
			
			$ObjectMailer 			= $ArrayReturn[0];
			$ObjectMailer->Body		= $ArrayReturn[1];
			}
		}
	// If enabled, embed images to the email - FINISHED
	
	// Insert attachments - STARTED
	foreach ($ArrayEmailProperties['Attachments'] as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$ArrayConfig['Paths']['Data']."/campaign_attachments/".$ArrayEmailProperties['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$ObjectMailer->AddAttachment($AttachmentPath, $AttachmentName);
		}
	// Insert attachments - FINISHED

	// Send the email - STARTED
	$ObjectMailer->Send();
	// Send the email - FINISHED
	
	// Detect if any error occured or not - STARTED
	if ($ObjectMailer->ErrorInfo != '')
		{
		$ArrayError = array(true, $ObjectMailer->ErrorInfo);
		}
	// Detect if any error occured or not - FINISHED

	return $ArrayError;
	}
?>