<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Inserts member email address into ban list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function BanMember($ArrayMember, $ArrayMailList, $ArrayConfig, $ArrayLanguage)
	{
	// Skip if this rule exists in the database - STARTED
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE RelMailListID='".$ArrayMailList['MailListID']."' AND Banned='".$ArrayMember['Email']."' AND Type='Email address'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
						
	if ($TotalFound > 0)
		{
		return;
		}
	// Skip if this rule exists in the database - FINISHED

	$ArrayFieldnValues = array(
							"BanID"			=>	'',
							"RelMailListID"	=>	$ArrayMailList['MailListID'],
							"Type"			=>	'Email address',
							"Banned"		=>	$ArrayMember['Email'],
							);

	$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
		$FieldList = "`".$FieldList."`";
	$ValueList = implode("','", array_values($ArrayFieldnValues));
		$ValueList ="'".$ValueList."'";

	$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_banned (".$FieldList.") VALUES (".$ValueList.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;
	}
	
//########################################################################################
// Inserts member email address into black list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function BlackListMember($ArrayMember, $ArrayMailList, $ArrayConfig, $ArrayLanguage)
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$ArrayMember['Email']."' AND RelMailListID='".$ArrayMailList['MailListID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound < 1)
		{					
		$ArrayFieldnValues = array(
									"BlackListID"		=>	'',
									"RelMailListID"		=>	$ArrayMailList['MailListID'],
									"Email"				=>	$ArrayMember['Email'],
									"DateSubmitted"		=>	date('Y-m-d H:i:s'),
									);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList = "'".$ValueList."'";
		
		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);				
		}

	return;
	}


?>