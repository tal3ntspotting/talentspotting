<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/
    header("Content-Type: image/jpeg");
    //create_image();
    //die();

    function GenerateCAPTCHA($strLength, $Width, $Height, $TTFFontPath, $BackgroundColorR, $BackgroundColorG, $BackgroundColorB, $TXTColorR, $TXTColorG, $TXTColorB, $SessionID, $SessionKeyName, $ArrayConfig)
	{
        $md5 = sha1(md5(rand(0,9999)));
        $pass = substr($md5, 10, 5);
        
        $_SESSION[$SessionKeyName] = $pass;
        
        $image = ImageCreatetruecolor(132, 20);
        
        $clr_white = ImageColorAllocate($image, 255, 255, 255);
        $clr_black = ImageColorAllocate($image, 0, 0, 0);
        $clr_sea_blue = ImageColorAllocate($image, 89, 153, 190);
        $clr_portocaliu = ImageColorAllocate($image, 255, 195, 146);
        
        imagefill($image, 0, 0, $clr_black);
        
        imagefontheight(15);
        imagefontwidth(15);
        
        imagestring($image, 5, 30, 3, $pass, $clr_white);
        
        imageline($image, 130, 1, 1, 11, $clr_sea_blue);
        imageline($image, 130, 1, 50, 20, $clr_sea_blue);
        imageline($image, 5, 1, 50, 20, $clr_sea_blue);
        imageline($image, 60, 1, 96, 20, $clr_sea_blue);
        imageline($image, 50, 1, 1, 20, $clr_sea_blue);
        
        // *** Afisam noua imagine creata in format jpg ***
        imagejpeg($image);
        
        // *** Eliberam resursele ***
        imagedestroy($image);
	}
	
?>
