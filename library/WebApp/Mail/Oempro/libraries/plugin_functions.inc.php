<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Executes each plug-in processes and returns their results
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RunPlugInContentProcesses($MainContent, $AlternateContent, $ArrayAdministrator, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig)
	{
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."plugins_contentprocesses AS tblTags ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."plugins AS tblPlugIns ";
	$SQLQuery .= "ON tblTags.RelPlugInID = tblPlugIns.PlugInID ";
	$ResultSet = mysql_query($SQLQuery);

	$ArrayResult = array($MainContent, $AlternateContent);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{

		if (file_exists($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']) == true)
			{
			include_once($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']);
			$ArrayResult = call_user_func($EachRow['FunctionName'], $MainContent, $AlternateContent, $ArrayAdministrator, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayMember, $ArrayConfig);
			}
		}

	return $ArrayResult;
	}

//########################################################################################
// Executes each plug-in processes and returns their results
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RunPlugInProcesses($SendCompleted, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics)
	{
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."plugins_processes AS tblTags ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."plugins AS tblPlugIns ";
	$SQLQuery .= "ON tblTags.RelPlugInID = tblPlugIns.PlugInID ";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayPlugInProcessResults = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		if (file_exists($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']) == true)
			{
			include_once($ArrayConfig['Paths']['Software']."/".$EachRow['ModuleFileName']);
			$ArrayResult = call_user_func($EachRow['FunctionName'], $SendCompleted, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayCampaign, $ArrayCampaignStatistics);
				$TMPCommand = $ArrayResult[0];
				$TMPMessage = $ArrayResult[1];
			
			$ArrayPlugInProcessResults[] = array(
												"Command"		=>	$TMPCommand,
												"Message"		=>	$TMPMessage,
												);
			}
		}

	return $ArrayPlugInProcessResults;
	}


?>