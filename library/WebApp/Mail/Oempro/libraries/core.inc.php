<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//---------------------------=[ GLOBAL SYSTEM CONFIGURATION ]=--------------------------- - STARTED
$ArrayConfig['GlobalSettings']['IncludeSignature']			=	false;
$ArrayConfig['GlobalSettings']['SignatureHTML']				=	'<br><br><b><style type="text/css">#oemProSignature { text-align:center; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:9px; color:#FF0000; text-decoration:none;}</style><a href="http://www.octeth.com" target="_blank" id="oemProSignature">This email is sent by oemPro - www.octeth.com<br>&quot;the most trusted name in email marketing&quot;</a>';
$ArrayConfig['GlobalSettings']['SignaturePlain']			=	"\n\n\n________________________________________\nThis email is sent by oemPro - www.octeth.com";
$ArrayConfig['GlobalSettings']['PrintPageSignature']		=	false;
$ArrayConfig['GlobalSettings']['PageSignature']				=	'<br><br><b><style type="text/css">#oemProSignature { text-align:center; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:9px; color:#000000; text-decoration:none; font-weight: normal;}</style><a href="http://www.octeth.com" target="_blank" id="oemProSignature"><center>©Copyright Octeth Technologies. All rights reserved.<br>oemPro, oemPro logo and all other related logos, names are <br>the registered trademark of Octeth Technologies<br>&quot;The Most Trusted Name In Email Marketing&quot;</center></a>';
$ArrayConfig['GlobalSettings']['SystemSelfCheckError']		=	'<table align="center"><tr><td><div id="SelfCheck" style="width: 750px; text-align: left; font-family: Courier New, Arial; font-weight: bold; font-size: 11px; margin: 0 auto; color: #006600; border-width: 1px; border-color: #006600;border-style: dotted;"><strong><u>SYSTEM SELF CHECK FAILURE! </u></strong><br />_Message_<br />Note: This error message is displayed to super administrators only.</div></td></tr></table>';
$ArrayConfig['GlobalSettings']['URL']['QuickTips']			=	'http://www.octeth.com/agents/quick_tips.php';
$ArrayConfig['GlobalSettings']['URL']['OctethNewsServer']	=	"http://www.octeth.com/agents/show_product_news.php?License=_RegistrationLicense_&Content=1&Product=OEMP";
$ArrayConfig['GlobalSettings']['URL']['OctethQuickHelp']	=	"http://www.octeth.com/agents/quick_help.php?License=_RegistrationLicense_&Module=_ModuleName_&Product=OEMP";
$ArrayConfig['GlobalSettings']['URL']['OctethPlugIns']		=	"http://www.octeth.com/agents/download_plugins.php?License=_RegistrationLicense_&Product=OEMP";
$ArrayConfig['GlobalSettings']['URL']['OctethVersionCheck']	=	"http://www.octeth.com/agents/version_check.php?License=_RegistrationLicense_&Product=OEMP";
//---------------------------=[ GLOBAL SYSTEM CONFIGURATION ]=--------------------------- - FINISHED

/**
 * Deletes membership database table of the mail list
 *
 * @return boolean
 * @author Cem Hurturk
 **/
function DeleteMembershipTable($MailListID, $ArrayConfig)
	{
	$SQLQuery = "DROP TABLE `oemp_maillist_members_".$MailListID."`";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	return ((mysql_error($ResultSet) != '') ? false : true);
	}

/**
 * Creates membership database table for storing members of the mail list
 *
 * @return boolean
 * @author Cem Hurturk
 **/
function CreateMembershipTable($MailListID, $ArrayConfig)
	{
	$SQLQuery = "CREATE TABLE `oemp_maillist_members_".$MailListID."` (
				  `MembershipID` int(11) NOT NULL auto_increment,
				  `RelMailListID` int(11) NOT NULL default '0',
				  `RelMemberID` int(11) NOT NULL default '0',
				  `OptInConfirmed` enum('Yes','No') NOT NULL default 'Yes',
				  `OptOutConfirmed` enum('Yes','No') NOT NULL default 'Yes',
				  `SubscriptionStatus` enum('Pending for opt-in confirmation','Subscribed','Pending for opt-out confirmation','Unsubscribed') NOT NULL default 'Pending for opt-in confirmation',
				  `SubscriptionDate` datetime NOT NULL default '0000-00-00 00:00:00',
				  `UnsubscriptionDate` datetime NOT NULL default '0000-00-00 00:00:00',
				  `OptInConfirmDate` datetime NOT NULL default '0000-00-00 00:00:00',
				  `OptOutConfirmDate` datetime NOT NULL default '0000-00-00 00:00:00',
				  `IPOnSubscription` varchar(250) NOT NULL default '',
				  `IPOnUnSubscription` varchar(250) NOT NULL default '',
				  PRIMARY KEY  (`MembershipID`),
				  KEY `RelMailListID` (`RelMailListID`,`RelMemberID`),
				  KEY `RelMemberID` (`RelMemberID`),
				  KEY `SubscriptionStatus` (`SubscriptionStatus`),
				  KEY `SubscriptionDate` (`SubscriptionDate`),
				  KEY `UnsubscriptionDate` (`UnsubscriptionDate`)
				) ENGINE=MyISAM;
				";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	return ((mysql_error($ResultSet) != '') ? false : true);
	}

//########################################################################################
// Retrieves the content from a remote URL
function RetrieveRemoteContent($URL, $ArrayConfig)
	{
	if (extension_loaded('curl') == true)
		{
		$CurlHandler = curl_init();
			curl_setopt($CurlHandler, CURLOPT_URL, $URL);
			curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CurlHandler, CURLOPT_USERAGENT, $ArrayConfig['Branding']['ProductName']);

		$RemoteContent = curl_exec($CurlHandler);
		curl_close($CurlHandler);
		}
	else
		{
		$RemoteContent = implode("", file($URL));
		}

	return $RemoteContent;
	}


//########################################################################################
// Executes MySQL queries
function ExecuteMySQLQuery($SQLQuery, $Line = 0, $File = '')
	{
	global $ArrayConfig;
	
	$ResultSet = mysql_query($SQLQuery);

	if ($ArrayConfig['Settings']['SystemDebugMode'] == 'Enabled')
		{
		print('<span style="font-family: Courier New; font-size: 13px; color: #000000;">'.$SQLQuery.'</span><br /><br />');
		if (mysql_error() != '')
			{
			print('<span style="font-family: Courier New; font-size: 11px; color: #ff0000;">Error: '.mysql_error().'</span><br>');
			}
		}
	elseif (mysql_error() != '')
		{
		$FileHandler = fopen($ArrayConfig['Paths']['Data'].'/db/error.log', 'a');
			fwrite($FileHandler, date('r')."\n".$SQLQuery."\n".mysql_error()."\nLine #".$Line." on ".$File."\n------------------------\n");
		fclose($FileHandler);
		}
	
	return $ResultSet;
	}

//########################################################################################
// Load language module
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function LoadLanguageModules($PlugInCode, $ArrayConfig)
	{
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."plugins WHERE Code='".$PlugInCode."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	if (mysql_num_rows($ResultSet) < 1)
		{
		include_once($ArrayConfig['Paths']['Data']."/languages/".strtolower($PlugInCode)."_lang_standard.inc.php");
		}
	else
		{
		$ArrayPlugIn = mysql_fetch_assoc($ResultSet);

		include_once($ArrayConfig['Paths']['Data']."/languages/".strtolower($ArrayPlugIn['Code'])."_lang_standard.inc.php");
		}
		
	return $ArrayLanguage;
	}
	
//########################################################################################
// Returns parsed oemPro version
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseOEMPVersion($Version, $VersionType)
	{
	$NewVersion = array();

	// Major version - STARTED
	$MajorPart		= substr($Version, 0, 2);
	if (substr($MajorPart, 0, 1) == '0')
		{
		$MajorPart = substr($MajorPart, 1, 2);
		}

	$NewVersion[] = $MajorPart;
	// Major version - FINISHED

	// Minor version - STARTED
	$MinorPart		= substr($Version, 2, 2);
	if (substr($MinorPart, 0, 1) == '0')
		{
		$MinorPart = substr($MinorPart, 1, 2);
		}

	$NewVersion[] = $MinorPart;
	// Minor version - FINISHED

	// Build version - STARTED
	$BuildPart		= substr($Version, 4, 2);
	if (substr($BuildPart, 0, 1) == '0')
		{
		$BuildPart = substr($BuildPart, 1, 2);
		}

	$NewVersion[] = $BuildPart;
	// Build version - FINISHED

	// Release version - STARTED
	$ReleasePart		= substr($Version, 6, 2);
	if (substr($ReleasePart, 0, 1) == '0')
		{
		$ReleasePart = substr($ReleasePart, 1, 2);
		}

	$NewVersion[] = $ReleasePart;
	// Release version - FINISHED

	// Generate readable version - STARTED
	$TMPNewVersion = $NewVersion;
	$NewVersion = "";

	if ($TMPNewVersion[0] != '0')
		{
		$NewVersion .= $TMPNewVersion[0];
		}

	if ($TMPNewVersion[1] != '0')
		{
		$NewVersion .= ".".$TMPNewVersion[1];
		}
	elseif (($TMPNewVersion[1] == '0') && (($TMPNewVersion[2] != '0') || ($TMPNewVersion[3] != '0')))
		{
		$NewVersion .= ".".$TMPNewVersion[1];
		}
	
	if ($TMPNewVersion[2] != '0')
		{
		$NewVersion .= ".".$TMPNewVersion[2];
		}
	elseif (($TMPNewVersion[2] == '0') && ($TMPNewVersion[3] != '0'))
		{
		$NewVersion .= ".".$TMPNewVersion[2];
		}

	if ($TMPNewVersion[3] != '0')
		{
//		$NewVersion .= " Release ".$TMPNewVersion[3];
		$NewVersion .= ".".$TMPNewVersion[3];
		}
	// Generate readable version - FINISHED
	
	// Insert the version type - STARTED
	if ($VersionType != 'fr')
		{
		$NewVersion .= $VersionType;
		}
	// Insert the version type - FINISHED
	
	return $NewVersion;
	}
	
//########################################################################################
// Returns true if the entered extension is loaded in PHP
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function IsExtensionLoaded($ExtensionName)
	{
	$ArrayLoadedExtensions = get_loaded_extensions();
	
	if (in_array(strtolower($ExtensionName), $ArrayLoadedExtensions))
		{
		return true;
		}
	else
		{
		return false;
		}
	}

//########################################################################################
// Removes related tables according to table dependencies
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID, $ArrayProcessLog = array(), $LastTableField = "")
	{
	$ArrayExcludedSubTables = array_flip($ArrayExcludedSubTables);

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."tabledependencies WHERE SuperTableName='".$SuperTableName."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	// Loop each sub table - STARTED
	while ($EachSuperTable = mysql_fetch_assoc($ResultSet))
		{
		// Skip this loop if this super table and sub table for the mentioned fields processed before - STARTED
		if ($ArrayProcessLog[$EachSuperTable['SuperTableName']][$EachSuperTable['SubTableName']][$EachSuperTable['SuperTableFieldName']][$EachSuperTable['SubTableFieldName']] == true)
			{
			continue;
			}
		else
			{
			$ArrayProcessLog[$EachSuperTable['SuperTableName']][$EachSuperTable['SubTableName']][$EachSuperTable['SuperTableFieldName']][$EachSuperTable['SubTableFieldName']] = true;
			}
		// Skip this loop if this super table and sub table for the mentioned fields processed before - FINISHED

		// Check if this table is excluded - STARTED
		if (isset($ArrayExcludedSubTables[$EachDependency['SubTableName']]) == true)
			{
			continue;
			}
		// Check if this table is excluded - FINISHED

		// Retrieve the source record - STARTED
			$LastTableField = ($LastTableField == '' ? $EachSuperTable['SuperTableFieldName'] : $LastTableField);
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachSuperTable['SuperTableName']." WHERE ".$LastTableField."='".$RelatedID."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArraySourceRecord = mysql_fetch_assoc($ResultSet2);
		// Retrieve the source record - FINISHED

		// Retrieve records - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix'].$EachSuperTable['SubTableName']." WHERE ".$EachSuperTable['SubTableFieldName']."='".$ArraySourceRecord[$EachSuperTable['SuperTableFieldName']]."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRecord = mysql_fetch_assoc($ResultSet2))
			{
			// Check for the sub-tables - STARTED
				$TMPSuperTableName			=	$EachSuperTable['SubTableName'];
				$TMPSuperTableFieldName		=	'';
				$TMPRelatedID				=	$EachRecord[$EachSuperTable['SubTableFieldName']];
				$TMPLastTableField			=	$EachSuperTable['SubTableFieldName'];
			RemoveTablesWithDependencies($ArrayConfig, $TMPSuperTableName, $TMPSuperTableFieldName, $ArrayExcludedSubTables, $TMPRelatedID, $ArrayProcessLog, $TMPLastTableField);
			// Check for the sub-tables - FINISHED
			}
		// Retrieve records - FINISHED

		// Remove the related sub table rows - STARTED
		$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix'].$EachSuperTable['SubTableName']." WHERE ".$EachSuperTable['SubTableFieldName']."='".$ArraySourceRecord[$EachSuperTable['SuperTableFieldName']]."'";
		$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Remove the related sub table rows - FINISHED
		}
	// Loop each sub table - FINISHED

		
	return true;
	}
	
//########################################################################################
// Do the first processes
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function FirstProcesses()
	{
	global $ArrayConfig;
	
	// Check for periodical processes and run them - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."periodicprocesses WHERE NOW() > DATE_ADD(LastRunDateTime, INTERVAL IntervalMinutesToCheck HOUR) OR LastRunDateTime='0000-00-00 00:00:00'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachProcess = mysql_fetch_assoc($ResultSet))
		{
		}
	// Check for periodical processes and run them - FINISHED

	return true;
	}

//########################################################################################
// Do the final processes
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function FinalProcesses()
	{
	global $ArrayConfig;
	
	DatabaseClose();
	
	// Display product signature if enabled - STARTED
	if ($ArrayConfig['GlobalSettings']['PrintPageSignature'] == true)
		{
		print ($ArrayConfig['GlobalSettings']['PageSignature']);
		}
	// Display product signature if enabled - FINISHED
	
	return true;
	}

//########################################################################################
// Prints raw error to the screen (fatal errors)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PrintRawError($ErrorMessage)
	{
	print("<hr>");
	print("<b>ERROR OCCURED</b><br>");
	print("Description: ".$ErrorMessage."<br>");
	print("Module: ".$_SERVER['PHP_SELF']."<br>");
	print("Time occured: ".date("r")."<br>");	
	print("<hr>");
	
	return true;
	}


?>