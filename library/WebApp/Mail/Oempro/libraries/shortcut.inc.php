<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Update shortcut of the administrator
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function UpdateShortcut($ArrayConfig, $ArrayAdministrator, $Page)
	{
	$Page = parse_url($Page);
		$Page = $Page['path'];
		
	// Check if this page exists for the administrator - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_shortcuts WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."' AND FileName='".$Page."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	if (mysql_num_rows($ResultSet) > 0)
		{
		$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."administrator_shortcuts SET ClickAmount = ClickAmount + 1 WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."' AND FileName='".$Page."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	else
		{
		$ArrayFieldnValues = array(
								"ShortCutID"				=>	'',
								"RelAdministratorID"		=>	$ArrayAdministrator['AdministratorID'],
								"FileName"					=>	$Page,
								"ClickAmount"				=>	1,
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."administrator_shortcuts (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Check if this page exists for the administrator - FINISHED

	return;
	}


?>