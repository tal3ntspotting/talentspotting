<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Process each follow up track and send auto responders to the members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendAutoResponders($ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	$TotalEmailsSent	= 0;
	// Initialize variables - FINISHED

	// Retrieve all "PENDING" follow up tracks - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."followups_track WHERE ActionDate<='".date("Y-m-d H:i:s")."' AND Status='Pending'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachTrack = mysql_fetch_assoc($ResultSet))
		{
		// Retrieve member information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$EachTrack['RelMemberID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMember = mysql_fetch_assoc($ResultSet2);
		// Retrieve member information - FINISHED

		// Retrieve mail list information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."followups WHERE FollowUpID='".$EachTrack['RelFollowUpID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayFollowUp = mysql_fetch_assoc($ResultSet2);

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE FollowUpGroupID='".$ArrayFollowUp['RelFollowUpGroupID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayFollowUpGroup = mysql_fetch_assoc($ResultSet2);

		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$ArrayFollowUpGroup['RelMailListID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMailList = mysql_fetch_assoc($ResultSet2);
		// Retrieve mail list information - FINISHED

		// Retrieve membership information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$ArrayMailList['MailListID']."' AND RelMemberID='".$ArrayMember['MemberID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMembership = mysql_fetch_assoc($ResultSet2);
		// Retrieve membership information - FINISHED

		// Retrieve auto responder information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."autoresponders WHERE AutoResponderID='".$ArrayFollowUp['RelAutoResponderID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayAutoResponder = mysql_fetch_assoc($ResultSet2);
		// Retrieve auto responder information - FINISHED

		// Send auto responder to the member - STARTED
		$SendStatus = SendAutoResponderToMember($ArrayMember, $ArrayMailList, $ArrayMembership, $ArrayAutoResponder, $ArrayConfig, $ArrayLanguage);
		// Send auto responder to the member - FINISHED

		// Update the status of the track - STARTED
			$TMPSendStatus = ($SendStatus == false ? 'Failed' : 'Sent');
		$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."followups_track SET Status='".$TMPSendStatus."', ProcessedOn='".date("Y-m-d H:i:s")."' WHERE ID='".$EachTrack['ID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Update the status of the track - FINISHED

		$TotalEmailsSent++;
		}
	// Retrieve all "PENDING" follow up tracks - FINISHED

	return $TotalEmailsSent;
	}

//########################################################################################
// Send auto responder to the member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendAutoResponderToMember($ArrayMember, $ArrayMailList, $ArrayMembership, $ArrayAutoResponder, $ArrayConfig, $ArrayLanguage)
	{
	// Load required module - STARTED
	include_once("./system/classes/class_mailer.php");
	include_once("./system/libraries/personalization.inc.php");
	// Load required module - FINISHED

	// Analyze subject. If it is in "multi-mode", explode into array - STARTED
	if (count(explode("\n", $ArrayAutoResponder['Subject'])) > 1)
		{
		$TMPSubject = explode("\n", $ArrayAutoResponder['Subject']);
		}
	// Analyze subject. If it is in "multi-mode", explode into array - FINISHED

	// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - STARTED
	if (strtolower(gettype($TMPSubject)) == 'array')
		{
		$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
		}
	else
		{
		$TMPSubject			=	$ArrayAutoResponder['Subject'];
		}
	// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - FINISHED

	$ObjectMailer 			= new phpmailer();

	$ObjectMailer->IsMail();

	$ObjectMailer->From		= $ArrayAutoResponder['FromEmail'];
	$ObjectMailer->FromName	= $ArrayAutoResponder['FromName'];
	$ObjectMailer->AddAddress(strtolower($ArrayMember['Email']), '');
	$ObjectMailer->AddReplyTo($ArrayAutoResponder['ReplyToEmail'], $ArrayAutoResponder['ReplyToName']);
	$ObjectMailer->ReturnPath= $ArrayAutoResponder['ReturnPathEmail'];
	$ObjectMailer->Sender= $ArrayAutoResponder['ReturnPathEmail'];
	
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayAutoResponder['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= substr($ArrayAutoResponder["CharSet"], 0, strlen($ArrayAutoResponder["CharSet"]) - 5);
	$ObjectMailer->Subject	= stripslashes($TMPSubject);

	if (($ArrayAutoResponder['ContentType'] == 'Html') || ($ArrayAutoResponder['ContentType'] == 'Both'))
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $ArrayAutoResponder['ContentMain'];
		$ObjectMailer->AltBody	= $ArrayAutoResponder['ContentAlternate'];
		}
	elseif ($ArrayAutoResponder['ContentType'] == 'Plain')
		{
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $ArrayAutoResponder['ContentMain'];
		}

	// Insert attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."autoresponders_attachments WHERE RelAutoResponderID='".$ArrayAutoResponder['AutoResponderID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachAttachment = mysql_fetch_assoc($ResultSet))
		{
		$ObjectMailer->AddStringAttachment($EachAttachment['File'], $EachAttachment['FileName']);
		}
	// Insert attachments - FINISHED

	// Get info of owner administrator - Start
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayAutoResponder['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayOwnerAdministrator = mysql_fetch_assoc($ResultSet);
	// Get info of owner administrator - End

	// Perform personalization - STARTED
		$ArrayPrefixes		=	array(
									"Sender",
									"Member",
									"Membership",
									"MailList",
									);
		$ArrayInformation	=	array(
									$ArrayOwnerAdministrator,
									$ArrayMember,
									$ArrayMembership,
									$ArrayMailList,
									);											
	$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');

	$ObjectMailer->Subject	=	PersonalizeTagsForSystemEmails($ObjectMailer->Subject, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Subject', 'AutoResponder', '%');
	$ObjectMailer->Body		=	PersonalizeTagsForSystemEmails($ObjectMailer->Body, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Main', 'AutoResponder', '%');
	$ObjectMailer->AltBody	=	PersonalizeTagsForSystemEmails($ObjectMailer->AltBody, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Alternate', 'AutoResponder', '%');

	$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
        
    if (strpos($ObjectMailer->Body, "CampaignID=&CampaignStatisticsID=&") !== false)
    {
        $ObjectMailer->Body = str_replace("CampaignID=&CampaignStatisticsID=&", "CampaignID=1&CampaignStatisticsID=1&", $ObjectMailer->Body);
        $ObjectMailer->AltBody = str_replace("CampaignID=&CampaignStatisticsID=&", "CampaignID=1&CampaignStatisticsID=1&", $ObjectMailer->AltBody);
    }
	// Perform personalization - FINISHED

	if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
		{
		$ObjectMailer->IsMail();
		}
	else
		{
		include_once('./system/libraries/send_engines.inc.php');
		include_once("./system/classes/class_smtp.php");
		
		$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
		
		if ($ArraySendEngine == false)
			{
			$ObjectMailer->IsMail();
			}
		else
			{
				$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
				$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
				$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
				$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
				$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
				$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
				$ObjectMailer->SMTPKeepAlive	=	false;
			$ObjectMailer->IsSMTP();
			}
		}
		
	$ObjectMailer->Send();
	
	if ($ObjectMailer->ErrorInfo != '')
		{
		$SendStatus = false;
		}
	else
		{
		$SendStatus = true;
		}

	return $SendStatus;
	}

?>