<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Gather email address from the email header format (To, From, etc.)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GatherEmail($String)
	{
	$ReversedString = strrev($String);

		$Pattern = '/>?([^(?:>|<)]*)?<? ?"?([^"]*)/';
	preg_match_all($Pattern, $ReversedString, $ArrayMatches);

	$Email	= strrev($ArrayMatches[1][0]);
	$Name	= strrev($ArrayMatches[2][0]);

	return array($Name, $Email);
	}

?>