<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Purge not confirmed administrators
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PurgeNotConfirmedAdministrators($ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	$TotalPurged = 0;
	// Initialize variables - FINISHED

	// Retrieve the list of administrators that are not confirmed and passed defined time limit - STARTED
	$TimeLimit = strtotime(date('Y-m-d').' -'.$ArrayConfig['Settings']['NewAdminExpireAfterDays'].' days '.date('H:i:s'));

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE IsConfirmed='No' AND AccountSetupDate<='".$TimeLimit."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalPurged = mysql_num_rows($ResultSet);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Delete assigned records from other tables - STARTED
			$SuperTableName			= "administrators";
			$SuperTableFieldName	= "AdministratorID";
			$ArrayExcludedSubTables	= array();
			$RelatedID				= $EachRow['AdministratorID'];
		RemoveTablesWithDependencies($ArrayConfig, $SuperTableName, $SuperTableFieldName, $ArrayExcludedSubTables, $RelatedID);
		// Delete assigned records from other tables - FINISHED

		// Remove the administrator account - STARTED
		$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$EachRow['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Remove the administrator account - FINISHED

		// Remove all user file uploads - STARTED
		include_once('./system/libraries/file.inc.php');
			$Path = $ArrayConfig['Paths']['Data'].'/user_uploads/'.$EachRow['AdministratorID'];
		RemoveDirectory($Path, $ArrayConfig);
		// Remove all user file uploads - FINISHED
		}
	// Retrieve the list of administrators that are not confirmed and passed defined time limit - FINISHED

	return $TotalPurged;
	}


?>