<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Unregister member from scheduled auto responders
// ---
function UnRegisterFromAutoResponders($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig)
	{
	// Retrive all follow-up groups of this mail list - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE RelMailListID='".$ArrayMailList['MailListID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachFollowUpGroup = mysql_fetch_assoc($ResultSet))
		{
		// Retrieve follow-up auto responders of this group - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."followups WHERE  RelFollowUpGroupID='".$EachFollowUpGroup['FollowUpGroupID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachFollowUp = mysql_fetch_assoc($ResultSet2))
			{
			$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."followups_track WHERE RelMemberID='".$ArrayMember['MemberID']."' AND RelFollowUpID='".$EachFollowUp['FollowUpID']."'";
			$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		// Retrieve follow-up auto responders of this group - FINISHED
		}
	// Retrive all follow-up groups of this mail list - FINISHED
	
	return;
	}

//########################################################################################
// Sends auto responder to the entered member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendAutoResponder($ArrayMember, $ArrayAutoResponder, $ArrayMailList, $ArrayMemberShip, $ApplyPersonalization, $IsPreview, $PreviewEmailAddress, $ArrayConfig, $ArrayLanguage)
	{	
	// Load required module - STARTED
	include_once("../system/classes/class_mailer.php");
	include_once("../system/libraries/personalization.inc.php");
	// Load required module - FINISHED

	$ObjectMailer 			= new phpmailer();

	$ObjectMailer->IsMail();

	$ObjectMailer->From		= $ArrayAutoResponder['FromEmail'];
	$ObjectMailer->FromName	= $ArrayAutoResponder['FromName'];
	$ObjectMailer->AddReplyTo($ArrayAutoResponder['ReplyToEmail'], $ArrayAutoResponder['ReplyToName']);
	$ObjectMailer->ReturnPath= $ArrayAutoResponder['ReturnPathEmail'];
	$ObjectMailer->Sender= $ArrayAutoResponder['ReturnPathEmail'];

	if ($IsPreview == false)	
		{
		$ObjectMailer->AddAddress(strtolower($ArrayMember['Email']), '');
		}
	else
		{
		$ObjectMailer->AddAddress(strtolower($PreviewEmailAddress), '');
		}
	
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayAutoResponder['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= substr($ArrayAutoResponder["CharSet"], 0, strlen($ArrayAutoResponder["CharSet"]) - 5);
	$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00159"]." ".stripslashes($ArrayAutoResponder['Subject']);
	
	if (($ArrayAutoResponder['ContentType'] == 'Html') || ($ArrayAutoResponder['ContentType'] == 'Both'))
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $ArrayAutoResponder['ContentMain'];
		$ObjectMailer->AltBody	= $ArrayAutoResponder['ContentAlternate'];
		}
	elseif ($ArrayAutoResponder['ContentType'] == 'Plain')
		{
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $ArrayAutoResponder['ContentMain'];
		}
    
	// Insert attachments - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."autoresponders_attachments WHERE RelAutoResponderID='".$ArrayAutoResponder['AutoResponderID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachAttachment = mysql_fetch_assoc($ResultSet))
		{
		$ObjectMailer->AddStringAttachment($EachAttachment['File'], $EachAttachment['FileName']);
		}
	// Insert attachments - FINISHED

	// Get info of owner administrator - Start
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayAutoResponder['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayAdministrator = mysql_fetch_assoc($ResultSet);
	// Get info of owner administrator - End

	// Perform personalization - STARTED
		$ArrayPrefixes		=	array(
									"Sender",
									"Member",
									"Membership",
									"MailList",
									);
		$ArrayInformation	=	array(
									$ArrayAdministrator,
									$ArrayMember,
									$ArrayMembership,
									$ArrayMailList,
									);											
	$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');

	$ObjectMailer->Subject	=	PersonalizeTagsForSystemEmails($ObjectMailer->Subject, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Subject', 'AutoResponder', '%');
	$ObjectMailer->Body		=	PersonalizeTagsForSystemEmails($ObjectMailer->Body, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Main', 'AutoResponder', '%');
	$ObjectMailer->AltBody	=	PersonalizeTagsForSystemEmails($ObjectMailer->AltBody, $ArrayConfig, $ArrayLanguage, $ArrayMember, $ArrayAutoResponder, 'Alternate', 'AutoResponder', '%');

    //die($ObjectMailer->Body);
	$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
	$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
    
	// Perform personalization - FINISHED
    
	if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
		{
		$ObjectMailer->IsMail();
		}
	else
		{
		include_once('../system/libraries/send_engines.inc.php');
		include_once("../system/classes/class_smtp.php");
		
		$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
		
		if ($ArraySendEngine == false)
			{
			$ObjectMailer->IsMail();
			}
		else
			{
				$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
				$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
				$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
				$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
				$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
				$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
				$ObjectMailer->SMTPKeepAlive	=	false;
			$ObjectMailer->IsSMTP();
			}
		}

	$ObjectMailer->Send();
	
	if ($ObjectMailer->ErrorInfo == true)
		{
		return true;
		}
	else
		{
		return false;
		}
	}
	
?>