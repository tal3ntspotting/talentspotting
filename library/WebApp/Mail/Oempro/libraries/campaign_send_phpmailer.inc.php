<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Removes all unwanted HTML code from email body
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RemoveUnwantedHTMLCodes($String, $ArrayConfig)
	{
	$ArrayUnwantedHTMLCodes = array(
									'<tbody>'		=>	'',
									'</tbody>'		=>	'',
									);
	
	foreach ($ArrayUnwantedHTMLCodes as $EachPattern=>$EachReplace)
		{
		$String = str_replace($EachPattern, $EachReplace, $String);
		}

	return $String;
	}
	
//########################################################################################
// Embeds objects found in the email body to the mailer object (phpmailer)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function EmbedObjects($ObjectMailer, $Content, $ArrayConfig)
	{
	$ArrayObjects = array();
	
	// src= - STARTED
		$Pattern = "/src\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - STARTED
			if (substr($EachMatch[2], 0 , strlen($ArrayConfig['URLs']['Software']) + 10) == $ArrayConfig['URLs']['Software'].'/read.php?')
				{
				continue;
				}
			else
				{
				$ArrayObjects[] = $EachMatch[2];
				}
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - FINISHED
			}
		}
	// src= - FINISHED

	// background-image:url( - STARTED
		$Pattern = "/background\-image\:url\((.*)\)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayImages[] = $EachMatch[1];
			}
		}
	// background-image:url( - FINISHED

	// background=" - STARTED
		$Pattern = "/background\=(\"|\'|)(.*)(\"|\'|\>)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - FINISHED

	// <link href=" - STARTED
		$Pattern = "/\<link href\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// <link href=" - FINISHED

	$ArrayObjects = array_flip(array_flip($ArrayObjects));

	$ArrayParsedURL = parse_url($ArrayConfig['URLs']['Software']);

	foreach ($ArrayObjects as $EachObject)
		{
		// If the object path is defined starting with '/', add website URL to the beginning - STARTED
		if (substr($EachObject, 0, 1) == '/')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].$EachObject;
			}
		elseif (substr($EachObject, 0, 2) == './')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 1, strlen($EachObject));
			}
		elseif (substr($EachObject, 0, 3) == '../')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 2, strlen($EachObject));
			}
		else
			{
			$EachObjectRenamed = $EachObject;
			}
		// If the object path is defined starting with '/', add website URL to the beginning - FINISHED

		$ArrayReplacePattern = array();
		$ArrayReplaceReplace = array();

			$CID = rand(10000000,20000000)."-".rand(10000,20000);
		$ObjectMailer->AddEmbeddedImage($EachObjectRenamed, $CID);

		$ArrayReplacePattern[]	=	'src="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'background-image:url('.$EachObject.');';
		$ArrayReplacePattern[]	=	'background="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'<link href="'.$EachObject.'"';
	
		$ArrayReplaceReplace[]	=	'src="cid:'.$CID.'"';
		$ArrayReplaceReplace[]	=	'background-image:url(cid:'.$CID.');';
		$ArrayReplaceReplace[]	=	'background="cid:'.$CID.'"';
		$ArrayReplaceReplace[]	=	'<link href="cid:'.$CID.'"';

		$Content = str_replace($ArrayReplacePattern, $ArrayReplaceReplace, $Content);
		$Content=quoted_printable_decode($Content);
		}

	return array($ObjectMailer, $Content);
	}
	
?>