<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Retrieves the quick tips from Octeth Server
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetriveQuickTip($URL, $LicenseKey)
	{
	include_once("../system/classes/class_octethhttp.php");
	
	$Parameters				 = "License=".$LicenseKey;
	
	$CurlHandler = curl_init();
		curl_setopt($CurlHandler, CURLOPT_URL, $URL);
		curl_setopt($CurlHandler, CURLOPT_POST, 1);
		curl_setopt($CurlHandler, CURLOPT_POSTFIELDS, $Parameters);
		curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($CurlHandler, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($CurlHandler, CURLOPT_USERAGENT, $ArrayConfig['Branding']['ProductName']);

	$QuickTip = curl_exec($CurlHandler);
	curl_close($CurlHandler);

	preg_match_all('/^(.*)\r\n\r\n/is', $QuickTip, $ArrayMatches);
	$QuickTip = str_replace($ArrayMatches[0][0], '', $QuickTip);

	if ($QuickTip == '')
		{
		return false;
		}
	else
		{
		return $QuickTip;
		}
	}

?>