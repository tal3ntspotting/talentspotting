<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Sends mail list emails to the member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendMailListEmails($ArrayMailListIDs, $EmailType, $ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig)
	{
	// Retrieve all request emails of the mail lists - STARTED
	$SQLQueryPart = array();
	foreach ($ArrayMailListIDs as $EachMailListID)
		{
		$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
		}

	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery  = "SELECT * ";
	$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."maillist_messages AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."emails AS tblEmails ";
	$SQLQuery .= "ON tblAssign.RelEmailID = tblEmails.EmailID ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE (".$SQLQueryPart.") AND tblAssign.UseFor='".$EmailType."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Retrieve all request emails of the mail lists - FINISHED

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ObjectMailer 			= new phpmailer();

		$ObjectMailer->IsMail();

		$ObjectMailer->From		= $EachRow['FromEmail'];
		$ObjectMailer->FromName	= $EachRow['FromName'];
		$ObjectMailer->AddAddress(strtolower($ArrayMember['Email']), '');
		$ObjectMailer->AddReplyTo($EachRow['ReplyToEmail'], $EachRow['ReplyToName']);
		$ObjectMailer->ReturnPath= $EachRow['ReturnPathEmail'];
		$ObjectMailer->Sender= $EachRow['ReturnPathEmail'];
		
		$ObjectMailer->WordWrap	= 70;
		$ObjectMailer->Priority	= $EachRow['Priority'];
		$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
		$ObjectMailer->CharSet	= substr($EachRow["CharSet"], 0, strlen($EachRow["CharSet"]) - 5);
		$ObjectMailer->Subject	= stripslashes($EachRow['Subject']);
		
		if (($EachRow['ContentType'] == 'Html') || ($EachRow['ContentType'] == 'Both'))
			{
			$ObjectMailer->IsHTML(true);
			$ObjectMailer->Body		= $EachRow['Content'];
			$ObjectMailer->AltBody	= $EachRow['ContentAlternate'];
			}
		elseif ($EachRow['ContentType'] == 'Plain')
			{
			$ObjectMailer->IsHTML(false);
			$ObjectMailer->Body		= $EachRow['Content'];
			}

		// Parse the confirmation link - STARTED
		if (($EmailType == 'OptInConfirmationRequest') || ($EmailType == 'OptInNotConfirmed'))
			{
			$EncryptedMailListMembershipID = Encrypt($ArrayMembership['MembershipID'], $ArrayConfig['Settings']['EncryptionKey']);
			
				$ArrayReplaceList = array(
										"%Link%"			=>	$ArrayConfig['URLs']['Software']."/optin_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Output=",
										"%RejectLink%"		=>	$ArrayConfig['URLs']['Software']."/optin_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Action=Rejected",
										);
			$ObjectMailer->Body		=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
			$ObjectMailer->AltBody	=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->AltBody);
			
			// Parse re-brandable confirmation links - STARTED
				$Type = 'OptIn';
			$ObjectMailer->Body		= RenderOptInOutLinksReBrandable($ObjectMailer->Body, $EncryptedMailListMembershipID, $ArrayMembership, $ArrayMember, $ArrayMailList, $Type, $ArrayConfig, $ArrayLanguage);
			$ObjectMailer->AltBody	= RenderOptInOutLinksReBrandable($ObjectMailer->AltBody, $EncryptedMailListMembershipID, $ArrayMembership, $ArrayMember, $ArrayMailList, $Type, $ArrayConfig, $ArrayLanguage);
			// Parse re-brandable confirmation links - FINISHED
			}
		elseif (($EmailType == 'OptOutConfirmationRequest') || ($EmailType == 'OptOutNotConfirmed'))
			{
			$EncryptedMailListMembershipID = Encrypt($ArrayMembership['MembershipID'], $ArrayConfig['Settings']['EncryptionKey']);
			
				$ArrayReplaceList = array(
										"%Link%"			=>	$ArrayConfig['URLs']['Software']."/optout_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID'],
										"%RejectLink%"		=>	$ArrayConfig['URLs']['Software']."/optout_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID']."&Action=Rejected",
										);
			$ObjectMailer->Body		=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
			$ObjectMailer->AltBody	=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->AltBody);

			// Parse re-brandable confirmation links - STARTED
				$Type = 'OptOut';
			$ObjectMailer->Body		= RenderOptInOutLinksReBrandable($ObjectMailer->Body, $EncryptedMailListMembershipID, $ArrayMembership, $ArrayMember, $ArrayMailList, $Type, $ArrayConfig, $ArrayLanguage);
			$ObjectMailer->AltBody	= RenderOptInOutLinksReBrandable($ObjectMailer->AltBody, $EncryptedMailListMembershipID, $ArrayMembership, $ArrayMember, $ArrayMailList, $Type, $ArrayConfig, $ArrayLanguage);
			// Parse re-brandable confirmation links - FINISHED
			}
		// Parse the confirmation link - FINISHED
		
		// Perform personalization - STARTED
			$ArrayPrefixes		=	array(
										"Member",
										"Membership",
										"MailList",
										);
			$ArrayInformation	=	array(
										$ArrayMember,
										$ArrayMembership,
										$ArrayMailList,
										);											
		$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
		$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
		$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
		
		$ObjectMailer->Subject	=	PersonalizeTagsForSystemEmails($ObjectMailer->Subject, $ArrayConfig, $ArrayLanguage, $ArrayMember, $EachRow, 'Subject', 'OptInOut', '%');
		$ObjectMailer->Body		=	PersonalizeTagsForSystemEmails($ObjectMailer->Body, $ArrayConfig, $ArrayLanguage, $ArrayMember, $EachRow, 'Main', 'OptInOut', '%');
		$ObjectMailer->AltBody	=	PersonalizeTagsForSystemEmails($ObjectMailer->AltBody, $ArrayConfig, $ArrayLanguage, $ArrayMember, $EachRow, 'Alternate', 'OptInOut', '%');

		$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
		$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
		$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
		// Perform personalization - FINISHED
			
		if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
			{
			$ObjectMailer->IsMail();
			}
		else
			{
			include_once($ArrayConfig['Paths']['Software'].'/system/libraries/send_engines.inc.php');
			include_once($ArrayConfig['Paths']['Software']."/system/classes/class_smtp.php");
			
			$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
			
			if ($ArraySendEngine == false)
				{
				$ObjectMailer->IsMail();
				}
			else
				{
					$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
					$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
					$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
					$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
					$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
					$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
					$ObjectMailer->SMTPKeepAlive	=	false;
				$ObjectMailer->IsSMTP();
				}
			}

		$ObjectMailer->Send();
		}

	return true;
	}


//########################################################################################
// Check if the email address is black listed or not
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckEmailIfBlackListed($Email, $ArrayMailLists, $ArrayConfig)
	{
	$SQLQueryPart = array();
	foreach ($ArrayMailLists as $EachMailListID)
		{
		$SQLQueryPart[] = "RelMailListID='".$EachMailListID."'";
		}
	$SQLQueryPart[] = "RelMailListID='0'";
	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$Email."' AND (".$SQLQueryPart.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
	
	if ($TotalFound > 0)
		{
		return true;
		}
	else
		{
		return false;
		}
	}

//########################################################################################
// Check if the email address exists or not 
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckEmailIfExists($Email, $ArrayConfig, $IgnoreMemberID)
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$Email."'";
	
	if ($IgnoreMemberID != '')
		{
		$SQLQuery .= " AND MemberID != '".$IgnoreMemberID."'";
		}
		
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return true;
		}
	else
		{
		return false;
		}
	}

//########################################################################################
// Check if the email address is banned or not
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckEmailIfBanned($Email, $ArrayMailLists, $ArrayConfig)
	{
	$EmailAddress	=	$Email;
	$EmailDomain	=	$Email;
		$EmailDomain=	explode("@", $EmailDomain);
		$EmailDomain=	$EmailDomain[1];
	
	$SQLQueryPart = array();
	foreach ($ArrayMailLists as $EachMailListID)
		{
		$SQLQueryPart[] = "RelMailListID='".$EachMailListID."'";
		}
	$SQLQueryPart[] = "RelMailListID='0'";
	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE (Banned='".$EmailAddress."' OR Banned='".$EmailDomain."') AND (Type='Domain' OR Type='Email address') AND (".$SQLQueryPart.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
	
	if ($TotalFound > 0)
		{
		return true;
		}
	else
		{
		return false;
		}
	}
	
//########################################################################################
// Decrypts the entered number
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function Decrypt($Number, $Key)
	{
	
	return rawurldecode(base64_decode(($Number)))/$Key;
	}


//########################################################################################
// Encrypts the entered number
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function Encrypt($Number, $Key)
	{
	return rawurlencode(base64_encode(($Number*$Key)));
	}


?>