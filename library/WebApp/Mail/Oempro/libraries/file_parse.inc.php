<?php

function findOccurence(&$result, $occurence)
{
    if (stripos($result, $occurence) !== FALSE)
    {
        $result = substr($result, stripos($result, $occurence)+strlen($occurence), strlen($result));
        return true;
    }
    else
    {
        return false;
    }
}

function fetchData($result, $occurence)
{
    return substr($result, 0, stripos($result, $occurence));
}
