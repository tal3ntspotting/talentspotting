<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Parses screen language texts
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParseScreenLanguage($ObjectTemplate, $ArrayLanguage, $ArrayConfig)
	{
	$PageContent = $ObjectTemplate->parseandoutput();
	
		$TMPPattern = "/\_\{LANGUAGE\:(.*)\}\_/iU";
	if (preg_match_all($TMPPattern, $PageContent, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArraySubParts = explode(":", $EachMatch[1]);
			if ($ArraySubParts[1] == '')
				{
				$PageContent = str_replace('_{LANGUAGE:'.$EachMatch[1].'}_', $ArrayLanguage["Screens"][$EachMatch[1]], $PageContent);
				}
			else
				{
				$PageContent = str_replace('_{LANGUAGE:'.$EachMatch[1].'}_', $ArrayLanguage[$ArraySubParts[0]]["Screens"][$ArraySubParts[1]], $PageContent);
				}
			}
		}

	$ObjectTemplate = new OctethTemplate($PageContent, 'string');	
	
	return $ObjectTemplate;
	}

?>