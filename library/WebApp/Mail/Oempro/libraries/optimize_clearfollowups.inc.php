<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Clear followups_tracks table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ClearFollowUps($ArrayConfig, $SetupValue1, $SetupValue2, $SetupValue3, $SetupValue4, $SetupValue5, $SetupValue6, $SetupValue7, $SetupValue8, $SetupValue9, $SetupValue10)
	{
	// Initialize variables - STARTED
	$FilterDateTime = date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -".$SetupValue1." days ".date("H:i:s")));
	// Initialize variables - FINISHED

	// Clear the follow up tracks - STARTED
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."followups_track WHERE ActionDate<='".$FilterDateTime."' AND (Status='Sent' OR Status='Cancelled' OR Status='Failed')";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Clear the follow up tracks - FINISHED
	
	return;	
	}

?>