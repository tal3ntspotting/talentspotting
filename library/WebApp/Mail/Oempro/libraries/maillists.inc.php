<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Finds active members (not bounced and not unsubscribed)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function FindMailListActiveMembers($MailListID, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
	$SQLQuery .= "WHERE (tblAssign.SubscriptionStatus='Subscribed' OR tblAssign.SubscriptionStatus='Pending for opt-out confirmation') ";
	$SQLQuery .= "AND (tblMembers.BounceType='Not bounced' OR tblMembers.BounceType='Soft bounce')";
	$SQLQuery .= "AND tblAssign.RelMailListID = '".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	return $TotalFound;
	}

//########################################################################################
// Generates selected send rules as an array
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GenerateSendRulesArray($ArrayConfig, $ArrayCampaign)
	{
	$TMPArraySQLQueryPartSendRules = array();
	
	$TMPSQLQuerySendRules = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$TMPResultSetSendRules = ExecuteMySQLQuery($TMPSQLQuerySendRules, __LINE__, __FILE__);
		$TotalSendRules = mysql_num_rows($TMPResultSetSendRules);
		
	$TMPArraySendRule = array();
	$TMPCounter = 1;
	while ($EachRule = mysql_fetch_assoc($TMPResultSetSendRules))
		{
		$TMPSendRule = "";

		if ($EachRule['Operator1'] == 'equals to')
			{
			$TMPOperator1 = '=';
			}
		elseif ($EachRule['Operator1'] == 'greater than')
			{
			$TMPOperator1 = '>';
			}
		elseif ($EachRule['Operator1'] == 'smaller than')
			{
			$TMPOperator1 = '<';
			}
		elseif ($EachRule['Operator1'] == 'not equals to')
			{
			$TMPOperator1 = '!=';
			}
		elseif ($EachRule['Operator1'] == 'includes')
			{
			$TMPOperator1 = 'LIKE';
			}
		elseif ($EachRule['Operator1'] == 'not includes')
			{
			$TMPOperator1 = 'NOT LIKE';
			}
		else
			{
			$TMPOperator1 = '=';
			}

		if (($TMPOperator1 == 'LIKE') || ($TMPOperator1 == 'NOT'))
			{
			$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '%".$EachRule['Value1']."%'";
			}
		else
			{
			$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '".$EachRule['Value1']."'";
			}

		if ($EachRule['Boolean1'] != '')
			{
			if ($EachRule['Operator2'] == 'equals to')
				{
				$TMPOperator2 = '=';
				}
			elseif ($EachRule['Operator2'] == 'greater than')
				{
				$TMPOperator2 = '>';
				}
			elseif ($EachRule['Operator2'] == 'smaller than')
				{
				$TMPOperator2 = '<';
				}
			elseif ($EachRule['Operator2'] == 'not equals to')
				{
				$TMPOperator2 = '!=';
				}
			elseif ($EachRule['Operator2'] == 'includes')
				{
				$TMPOperator2 = 'LIKE';
				}
			elseif ($EachRule['Operator2'] == 'not includes')
				{
				$TMPOperator2 = 'NOT LIKE';
				}
			else
				{
				$TMPOperator2 = '=';
				}

			if (($TMPOperator2 == 'LIKE') || ($TMPOperator2 == 'NOT'))
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '%".$EachRule['Value2']."%'";
				}
			else
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '".$EachRule['Value2']."'";
				}
			}
		
		if ($TotalSendRules > $TMPCounter)
			{
			$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean2']." ";
			}
		
		$TMPCounter++;

		$TMPArraySendRule[] = $TMPSendRule;
		}

	if ($TotalSendRules > 0)
		{
		$TMPSendRule = implode("", $TMPArraySendRule);
			$TMPSendRule = "(".$TMPSendRule.")";

		$TMPArraySQLQueryPartSendRules[] = $TMPSendRule;
		}

	return $TMPArraySQLQueryPartSendRules;
	}
	
//########################################################################################
// Retrieve the SMTP servers of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function FindTotalMemberOfOutsourcedList($ArrayMailList, $ArrayConfig, $TMPArraySQLQueryPartSendRules = array())
	{
	// If mail list is not outsourced, return - STARTED
	if ($ArrayMailList['SourceType'] != 'Outsourced - MySQL')
		{
		return 0;
		}
	// If mail list is not outsourced, return - FINISHED

	// Connect to outsourced MySQL list - STARTED
	mysql_close();

	$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
		mysql_select_db($ArrayMailList['OutSourceDBName']);
	// Connect to outsourced MySQL list - FINISHED

	// Retrieve the total rows in the outsourced table - STARTED
	if ($ArrayMailList['OutSourceDBQueryType'] == 'Basic')
		{
		if ($ArrayMailList['OutSourceDBTable'] != '')
			{
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayMailList['OutSourceDBTable'];
	
			if ($ArrayMailList['OutSourceDBWhereQuery'] != '')
				{
				$SQLQuery .= " WHERE ".$ArrayMailList['OutSourceDBWhereQuery'];
				$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " AND ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
				}
			else
				{
				$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " WHERE ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
				}
	
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TotalFound = mysql_fetch_assoc($ResultSet);
				$TotalFound = $TotalFound['TotalFound'];
			}
		else
			{
			$TotalFound = 0;
			}
		}
	else
		{
		$SQLQuery		= $ArrayMailList['OutSourceDBFullSQLQuery'];

		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_num_rows($ResultSet);
		}
	// Retrieve the total rows in the outsourced table - FINISHED

	// Connect to oemPro MySQL server and database
	mysql_close();

	$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
	// Connect to outsourced MySQL list - FINISHED
	
	return $TotalFound;
	}
	
//########################################################################################
// Retrieve the SMTP servers of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GetSMTPServersOfMailLists($ArrayMailListIDs, $ArrayConfig)
	{
	$ArraySMTP = array();
	
	// Retrieve the SMTP servers of the mail lists - STARTED
	foreach ($ArrayMailListIDs as $EachMailListID)
		{
		$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_smtp AS tblAssign ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
		$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
		$SQLQuery .= "WHERE tblAssign.RelMailListID='".$EachMailListID."' ORDER BY tblSMTP.ServerName ASC";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArraySMTP[$EachRow['SMTPServerID']] = array(
														"SMTPServerID"	=>	$EachRow['SMTPServerID'],
														"Name"			=>	$EachRow['ServerName'],
														"Host"			=>	$EachRow['ServerHost'],
														);
			}
		}
	// Retrieve the SMTP servers of the mail lists - FINISHED

	return $ArraySMTP;
	}
	
//########################################################################################
// Retrieve the email addresses of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GetEmailAddressesOfMailLists($ArrayMailListIDs, $ArrayConfig)
	{
	$ArrayEmails = array();
	
	// Retrieve the email addresses of the mail lists - STARTED
	foreach ($ArrayMailListIDs as $EachMailListID)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE RelMailListID='".$EachMailListID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayEmails[$EachRow['Name']."-".$EachRow['EmailAddress']] = array(
																			"EmailID"	=>	$EachRow['EmailAddressID'],
																			"Name"		=>	$EachRow['Name'],
																			"Email"		=>	$EachRow['EmailAddress'],
																			);
			}
		}
	// Retrieve the email addresses of the mail lists - FINISHED

	return $ArrayEmails;
	}

//########################################################################################
// Retrieve the email addresses of administrator
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GetEmailAddressesOfAdministrator($AdministratorID, $ArrayConfig)
	{
	$ArrayEmails = array();
	
	// Retrieve the email addresses of the administrator - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE RelAdministratorID='".$AdministratorID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayEmails[$EachRow['Name']."-".$EachRow['EmailAddress']] = array(
																		"EmailID"	=>	$EachRow['EmailID'],
																		"Name"		=>	$EachRow['Name'],
																		"Email"		=>	$EachRow['EmailAddress'],
																		);
		}
	// Retrieve the email addresses of the administrator - FINISHED

	return $ArrayEmails;
	}
?>