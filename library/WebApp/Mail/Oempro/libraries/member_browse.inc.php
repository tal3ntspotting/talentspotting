<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Returns an array which includes database table fields, names and custom fields of a mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnMembershipFields($MailListIDs, $ArrayLanguage, $ArrayConfig, $ArrayHideFields)
	{	
	$ArrayOEMPFields = array();

		$ArrayHideFields["MemberID"]		=	true;
		$ArrayHideFields["ID"]				=	true;
		$ArrayHideFields["RelMailListID"]	=	true;
		$ArrayHideFields["RelMemberID"]		=	true;
		$ArrayHideFields["MembershipID"]	=	true;

	// Retrieve "oemp_maillist_members" fields - STARTED
	$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."maillist_members";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if ((substr($EachField['Field'], 0, 11) != 'CustomField') && ($ArrayHideFields[$EachField['Field']] != true))
			{
			$ArrayOEMPFields[$EachField['Field']] = ($ArrayLanguage["Texts"]["00082"][$EachField['Field']] == '' ? $EachField['Field'] : $ArrayLanguage["Texts"]["00082"][$EachField['Field']]);
			}
		}
	// Retrieve "oemp_maillist_members" fields - FINISHED

	return $ArrayOEMPFields;
	}

//########################################################################################
// Returns an array which includes database table fields, names and custom fields of a mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnMemberFields($MailListIDs, $ArrayLanguage, $ArrayConfig, $ArrayHideFields, $ArrayLoggedAdmin)
	{	
	$ArrayOEMPFields = array();

		$ArrayHideFields["MemberID"]		=	true;
		$ArrayHideFields["ID"]				=	true;
		$ArrayHideFields["RelMailListID"]	=	true;
		$ArrayHideFields["RelMemberID"]		=	true;
		$ArrayHideFields["MembershipID"]	=	true;

	// Retrieve "oemp_members" fields (except custom fields) - STARTED
	$SQLQuery = "DESCRIBE ".$ArrayConfig['Database']['Prefix']."members";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if ((substr($EachField['Field'], 0, 11) != 'CustomField') && ($ArrayHideFields[$EachField['Field']] != true))
			{
			$ArrayOEMPFields[$EachField['Field']] = ($ArrayLanguage["Texts"]["00081"][$EachField['Field']] == '' ? $EachField['Field'] : $ArrayLanguage["Texts"]["00081"][$EachField['Field']]);
			}
		}
	// Retrieve "oemp_members" fields (except custom fields) - FINISHED

	// Retrieve custom fields - STARTED
	$SQLQueryPart = array();
	foreach ($MailListIDs as $EachMailListID)
		{
		$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
		}
	$SQLQueryPart = implode(" OR ", $SQLQueryPart);

	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_customfields as tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."customfields as tblFields ";
	$SQLQuery .= "ON tblAssign.RelCustomFieldID = tblFields.CustomFieldID ";
	if ($MailListIDs[0] > 0)
		{
		$SQLQuery .= "WHERE (".$SQLQueryPart.") ";
		}
	$SQLQuery .= "AND tblFields.RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' ";
	$SQLQuery .= "GROUP BY tblAssign.RelCustomFieldID ";
	$SQLQuery .= "ORDER BY tblFields.Title ASC";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		if (($EachField['FieldType'] != 'Hidden field') && ($EachField['FieldType'] != 'Notice'))
			{
			$CustomFieldTitle = (strlen($EachField['Title']) > 15 ? substr($EachField['Title'], 0, 15)."..." : $EachField['Title']);
			}
		else
			{
			$CustomFieldTitle = (strlen($EachField['Extra1']) > 15 ? substr($EachField['Extra1'], 0, 15)."..." : $EachField['Extra1']);
			}

		$ArrayOEMPFields["CustomField".$EachField['CustomFieldID']] = $CustomFieldTitle;
		}
	// Retrieve custom fields - FINISHED

	return $ArrayOEMPFields;
	}
?>