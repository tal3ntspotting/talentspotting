<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Displays transparent image
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DisplayTransImage($ArrayConfig)
	{
	// Define which skin file to use - STARTED
	$SkinName = strtolower($ArrayConfig['Settings']['Skin']);
	// Define which skin file to use - FINISHED

	header("Content-type: image/gif");
	
	$FileHandler = fopen($ArrayConfig['Paths']['Data'].'/templates/'.$SkinName.'/media/images/theme/trans.gif', "rb");
		fpassthru($FileHandler);
	fclose($FileHandler);
	
	exit;
	}

?>