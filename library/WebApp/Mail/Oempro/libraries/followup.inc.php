<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Register follow up auto responders for the member
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RegisterFollowUpAutoResponders($MailListID, $MemberID, $ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	$TotalRegistered	= 0;
	$StartDate			= date("Y-m-d");
	$StartTime			= date("H:i:s");
		$LastDate			= $StartDate;
		$LastTime			= $StartTime;
	// Initialize variables - FINISHED

	// Retrieve follow groups of the mail list - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE RelMailListID='".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	if (mysql_num_rows($ResultSet) > 0)
		{
		// Loop each follow group - STARTED
		while ($EachFollowUp = mysql_fetch_assoc($ResultSet))
			{
			// Retrieve the list of auto responders for the follow up group - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."followups WHERE RelFollowUpGroupID='".$EachFollowUp['FollowUpGroupID']."' ORDER BY ProcessOrder ASC";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachAutoResponder = mysql_fetch_assoc($ResultSet2))
				{
				// Set the last process date and time - STARTED
				$TMPLastProcessDateTime = date("Y-m-d H:i:s", strtotime($LastDate." + ".$EachAutoResponder['SecondsToFollowUp']." second ".$LastTime));
					$LastDate = date("Y-m-d", strtotime($TMPLastProcessDateTime));
					$LastTime = date("H:i:s", strtotime($TMPLastProcessDateTime));
				// Set the last process date and time - FINISHED

				// Register the auto responder to the member - STARTED
				$ArrayFieldnValues = array(
										"ID"					=>	'',
										"RelMemberID"			=>	$MemberID,
										"RelFollowUpID"			=>	$EachAutoResponder['FollowUpID'],
										"ActionDate"			=>	$TMPLastProcessDateTime,
										"ProcessedOn"			=>	'0000-00-00 00:00:00',
										"Status"				=>	'Pending',
										"Extra1"				=>	'',
										"Extra2"				=>	'',
										"Extra3"				=>	'',
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList = "'".$ValueList."'";
				
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."followups_track (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				// Register the auto responder to the member - FINISHED
				
				$TotalRegistered++;
				}
			// Retrieve the list of auto responders for the follow up group - FINISHED
			}
		// Loop each follow group - FINISHED
		}
	// Retrieve follow groups of the mail list - FINISHED

	return $TotalRegistered;
	}

?>