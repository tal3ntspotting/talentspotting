<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Checks whether the campaign is being sent or not
function IsCampaignBeingSent($CampaignID, $ArrayConfig)
	{
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$CampaignID."' AND Status!='Pending'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	if (mysql_num_rows($ResultSet) > 0)
		{
		return false;
		}
	else
		{
		return true;
		}
	}
	
//########################################################################################
// Skips email sending to the member if the account is banned or black listed
function IsMemberBannedOrBlackListed($ArrayMember, $ArrayCampaign, $ArrayConfig)
	{
	   return false;
	$EmailAddress	=	$ArrayMember['Email'];
	$EmailDomain	=	explode('@', $EmailAddress);
		$EmailDomain=	"*@".$EmailDomain[1];

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$EmailAddress."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return true;
		}

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_banned WHERE (Banned='".$EmailAddress."' AND Type='Email address') OR (Banned='".$EmailDomain."' AND Type='Domain')";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return true;
		}

	return false;
	}

//########################################################################################
// Calculate the total recipients during campaign sending
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function InstantCalculateTotalRecipients($ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $IsResumed)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Retrieve mail lists - STARTED
	$ArrayMailLists = array();
	
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayMailLists[]	=	array(
									"MailListID"		=>	$EachRow['MailListID'],
									"SourceType"		=>	$EachRow['SourceType'],
									);
		}
	// Retrieve mail lists - FINISHED

	// Retrieve and prepare recipients (local) - STARTED
	$TMPSQLQuery = array();
	foreach ($ArrayMailLists as $ArrayVals)
		{
		$TMPSQLQuery[] = "tblAssign.RelMailListID = '".$ArrayVals['MailListID']."'";
		}
	$TMPSQLQuery = implode(" OR ", $TMPSQLQuery);
	$TMPSQLQuery = "(".$TMPSQLQuery.")";

	// Query for retrieving members - STARTED
	$SQLQuery 			 = "SELECT tblMembers.*, tblAssign.* ";
		$SQLQuery		.= "FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery		.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQuery		.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";

	// Query for calculating the amount of recipiens - STARTED
	$SQLQueryTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryTotal	.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of recipiens - FINISHED

	// Query for calculating the amount of ALL recipiens (ignoring resume) - STARTED
	$SQLQueryOverAllTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryOverAllTotal	.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryOverAllTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of ALL recipiens (ignoring resume) - FINISHED

	$SQLQueryPart = array();
		$SQLQueryPart[] = $TMPSQLQuery;
		$SQLQueryPart[] = "tblMembers.BounceType!='Hard bounce'";
		$SQLQueryPart[] = "(tblAssign.SubscriptionStatus='Subscribed' OR tblAssign.SubscriptionStatus='Pending for opt-out confirmation')";
		
		// Resume if paused - STARTED
		if ($IsResumed == true)
			{
			$SQLQueryPart[] = "tblMembers.MemberID > '".$ArrayCampaignStatistics['QueuePointer']."'";
			}
		// Resume if paused - FINISHED

		// Apply sending rules - STARTED
		$TMPArraySQLQueryPartSendRules = array();
		
		$TMPSQLQuerySendRules = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$TMPResultSetSendRules = ExecuteMySQLQuery($TMPSQLQuerySendRules, __LINE__, __FILE__);
			$TotalSendRules = mysql_num_rows($TMPResultSetSendRules);
			
		$TMPArraySendRule = array();
		$TMPCounter = 1;
		while ($EachRule = mysql_fetch_assoc($TMPResultSetSendRules))
			{
			$TMPSendRule = "";

			if ($EachRule['Operator1'] == 'equals to')
				{
				$TMPOperator1 = '=';
				}
			elseif ($EachRule['Operator1'] == 'greater than')
				{
				$TMPOperator1 = '>';
				}
			elseif ($EachRule['Operator1'] == 'smaller than')
				{
				$TMPOperator1 = '<';
				}
			elseif ($EachRule['Operator1'] == 'not equals to')
				{
				$TMPOperator1 = '!=';
				}
			elseif ($EachRule['Operator1'] == 'includes')
				{
				$TMPOperator1 = 'LIKE';
				}
			elseif ($EachRule['Operator1'] == 'not includes')
				{
				$TMPOperator1 = 'NOT LIKE';
				}
			else
				{
				$TMPOperator1 = '=';
				}

			if (($TMPOperator1 == 'LIKE') || ($TMPOperator1 == 'NOT LIKE'))
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '%".$EachRule['Value1']."%'";
				}
			else
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '".$EachRule['Value1']."'";
				}

			if ($EachRule['Boolean1'] != '')
				{
				if ($EachRule['Operator2'] == 'equals to')
					{
					$TMPOperator2 = '=';
					}
				elseif ($EachRule['Operator2'] == 'greater than')
					{
					$TMPOperator2 = '>';
					}
				elseif ($EachRule['Operator2'] == 'smaller than')
					{
					$TMPOperator2 = '<';
					}
				elseif ($EachRule['Operator2'] == 'not equals to')
					{
					$TMPOperator2 = '!=';
					}
				elseif ($EachRule['Operator2'] == 'includes')
					{
					$TMPOperator2 = 'LIKE';
					}
				elseif ($EachRule['Operator2'] == 'not includes')
					{
					$TMPOperator2 = 'NOT LIKE';
					}
				else
					{
					$TMPOperator2 = '=';
					}

				if (($TMPOperator2 == 'LIKE') || ($TMPOperator2 == 'NOT LIKE'))
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '%".$EachRule['Value2']."%'";
					}
				else
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '".$EachRule['Value2']."'";
					}
				}
			
			if ($TotalSendRules > $TMPCounter)
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean2']." ";
				}
			
			$TMPCounter++;

			$TMPArraySendRule[] = $TMPSendRule;
			}

		if ($TotalSendRules > 0)
			{
			$TMPSendRule = implode("", $TMPArraySendRule);
				$TMPSendRule = "(".$TMPSendRule.")";

			$SQLQueryPart[] = $TMPSendRule;
			$TMPArraySQLQueryPartSendRules[] = $TMPSendRule;
			}
		// Apply sending rules - FINISHED

	$SQLQuery = $SQLQuery.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
	$SQLQueryTotal = $SQLQueryTotal.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";

	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - STARTED
	if ($IsResumed == true)
		{
			$TMPOverAllSQLQueryPart = $SQLQueryPart;
			$TMPOverAllSQLQueryPart[3] = "tblMembers.MemberID > '0'";
		$SQLQueryOverAllTotal = $SQLQueryOverAllTotal.(count($TMPOverAllSQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $TMPOverAllSQLQueryPart) : '')." ";
		}
	else
		{
		$SQLQueryOverAllTotal = $SQLQueryTotal;
		}
	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - FINISHED

	$SQLQuery .= "GROUP BY tblMembers.MemberID ";
	$SQLQuery .= "ORDER BY tblMembers.MemberID ";
	// Query for retrieving members - FINISHED

	$ResultSetOverAllTotal = ExecuteMySQLQuery($SQLQueryOverAllTotal, __LINE__, __FILE__);

	$OverAllTotalRecords = mysql_fetch_assoc($ResultSetOverAllTotal);
		$OverAllTotalRecords = $OverAllTotalRecords['TotalFound'];

	mysql_free_result($ResultSetOverAllTotal);
	// Retrieve and prepare recipients (local) - FINISHED

	// Retrieve the total amount of outsourced recipients - STARTED
	include_once("./system/libraries/maillists.inc.php");

	$TotalOutsourcedRecipients = 0;

	$ArrayOutsourcedMailLists = array();

	foreach ($ArrayMailLists as $Index => $ArrayValues)
		{
		if ($ArrayValues['SourceType'] == 'Outsourced - MySQL')
			{
			$ArrayOutsourcedMailLists[] = $ArrayValues['MailListID'];
			
			$SQLQueryMailList = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$ArrayValues['MailListID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQueryMailList, __LINE__, __FILE__);
				$ArrayOutsourcedMailList = mysql_fetch_assoc($ResultSet);

			$TotalOutsourcedRecipients += FindTotalMemberOfOutsourcedList($ArrayOutsourcedMailList, $ArrayConfig, $TMPArraySQLQueryPartSendRules);
			}
		}
	// Retrieve the total amount of outsourced recipients - FINISHED

	// Re-define the total amounts - STARTED
	$OverAllTotalRecords		= $OverAllTotalRecords + $TotalOutsourcedRecipients;
	$TotalOutsourcedRecipients	= $TotalOutsourcedRecipients;
	// Re-define the total amounts - FINISHED

	return array($OverAllTotalRecords, $TotalOutsourcedRecipients);
	}

//########################################################################################
// Drop temporary MySQL table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CalculateTotalRecipientsOnTemporaryMySQLTable($TemporaryMySQLTableName, $ArrayConfig)
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$TemporaryMySQLTableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalAllRecipients = mysql_fetch_assoc($ResultSet);
		$TotalAllRecipients = $TotalAllRecipients['TotalFound'];

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$TemporaryMySQLTableName." WHERE Status='NA'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalRecipients = mysql_fetch_assoc($ResultSet);
		$TotalRecipients = $TotalRecipients['TotalFound'];

	return array($TotalAllRecipients, $TotalRecipients);	
	}
	
//########################################################################################
// Drop temporary MySQL table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DropTemporaryMySQLTable($TemporaryMySQLTableName, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayConfig)
	{
	$SQLQuery = "DROP TABLE `".$TemporaryMySQLTableName."`;";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;	
	}
	
//########################################################################################
// Records member information into temporary MySQL table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SaveIntoTemporaryMySQLTable($RecipientSQLQuery, $RecipientsPerInterval, $TMPIntervalCounter, $TemporaryMySQLTableName, $ArrayLoggedAdmin, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayConfig)
	{
	$TMPResultSet = ExecuteMySQLQuery($RecipientSQLQuery, __LINE__, __FILE__);
		$TotalMembers = mysql_num_rows($TMPResultSet);
		mysql_free_result($TMPResultSet);
		
		$StartFrom = ($TMPIntervalCounter - 1) * $RecipientsPerInterval;
	$TMPSQLQuery = $RecipientSQLQuery." LIMIT ".$StartFrom.",".$RecipientsPerInterval;
	$TMPResultSet = ExecuteMySQLQuery($TMPSQLQuery, __LINE__, __FILE__);

	while ($EachMember = mysql_fetch_assoc($TMPResultSet))
		{
		// Avoid time-out - STARTED
		print(" ");
		// Avoid time-out - FINISHED

		// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
		if ($ArrayCampaignStatistics['Status'] != 'Preparing')
			{
			$ProcessStopped = true;
			break;
			}
		else
			{
			$ProcessStopped = false;
			}
		// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED
		
		// Add into temporary table - STARTED
		$ArrayFieldnValues = array(
								'ID'			=> '',
								'RelMemberID'	=> $EachMember['MemberID'],
								'Email'			=> $EachMember['Email'],
								'Status'		=> 'NA',
								'Pointer'		=> '0',
								);
		
		$FieldList = '`'.implode('`,`', array_keys($ArrayFieldnValues)).'`';
		$ValueList = '\''.implode('\',\'', array_values($ArrayFieldnValues)).'\'';
		
		$SQLQuery = "INSERT INTO ".$TemporaryMySQLTableName." (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Add into temporary table - FINISHED
		}	
	
	return array($TotalMembers, $TotalMembers);			
	}
	
//########################################################################################
// Create temporary MySQL table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CreateTemporaryMySQLTable($RecipientSQLQuery, $RecipientsPerInterval, $TotalIntervals, $TotalLocalRecipients, $ArrayLoggedAdmin, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayConfig)
	{
	$TemporaryMySQLTableName = $ArrayConfig['Database']['Prefix'].'tmp_'.$ArrayCampaign['CampaignID'].'_'.$ArrayCampaignStatistics['CampaignStatisticsID'];

	// Check if this table exists (this means this is a resume) - STARTED
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$TemporaryMySQLTableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	if (mysql_error() != '')
		{
		$IsResumed = false;
		}
	else
		{
		$IsResumed = true;
		}
	// Check if this table exists (this means this is a resume) - FINISHED

	// Create table and members inside if this is not a resume - STARTED	
	if ($IsResumed == false)
		{
		$SQLQuery = "CREATE TABLE `".$TemporaryMySQLTableName."` (`ID` INT NOT NULL AUTO_INCREMENT , `RelMemberID` INT NOT NULL , `Email` VARCHAR( 250 ) NOT NULL , `Status` ENUM( 'NA', 'Sent', 'Failed' ) NOT NULL , `Pointer` TINYINT NOT NULL , PRIMARY KEY ( `ID` ) , INDEX ( `RelMemberID` )) CHARACTER SET = latin1;";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
		for ($TMPIntervalCounter = 1; $TMPIntervalCounter <= $TotalIntervals; $TMPIntervalCounter++)
			{
			// If total number of local recipients is 0, then skip sending - STARTED
			if ($TotalLocalRecipients == 0)
				{
				break;
				}
			// If total number of local recipients is 0, then skip sending - FINISHED
	
			// Retrieve campaign statistics every interval for command checking - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
			// Retrieve campaign statistics every interval for command checking - FINISHED
	
			// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
			if ($ArrayCampaignStatistics['Status'] != 'Preparing')
				{
				$ProcessStopped = true;

				// Remove temporary MySQL table if sending has been cancelled - STARTED
				DropTemporaryMySQLTable($TemporaryMySQLTableName, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayConfig);
				// Remove temporary MySQL table if sending has been cancelled - FINISHES
	
				break;
				}
			else
				{
				$ProcessStopped = false;
				}
			// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED
	
			// Save members into temporary table - STARTED
			$ArrayReturn = SaveIntoTemporaryMySQLTable($RecipientSQLQuery, $RecipientsPerInterval, $TMPIntervalCounter, $TemporaryMySQLTableName, $ArrayLoggedAdmin, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayLanguage, $ArrayConfig);
				$TotalOverAllRecipients = $ArrayReturn[0];
				$TotalRecipients = $ArrayReturn[1];
			// Save members into temporary table - FINISHED
			}
		}
	else
		{
		$ProcessStopped = false;

		$ArrayReturn = CalculateTotalRecipientsOnTemporaryMySQLTable($TemporaryMySQLTableName, $ArrayConfig);
			$TotalOverAllRecipients	= $ArrayReturn[0];
			$TotalRecipients		= $ArrayReturn[1];
		}
	// Create table and members inside if this is not a resume - FINISHED

	return array($TemporaryMySQLTableName, $ProcessStopped, $TotalOverAllRecipients, $TotalRecipients, $IsResumed);	
	}
	
//########################################################################################
// Generates the SQL query for retrieving the outsourced members from outsourced mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function GenerateOutsourceSQLQuery($ArrayMailList, $ArrayConfig, $TMPArraySQLQueryPartSendRules = array())
	{
	if ($ArrayMailList['OutSourceDBQueryType'] == 'Basic')
		{
		$SQLQuery			 = "SELECT * ";
			$SQLQuery		.= "FROM ".$ArrayMailList['OutSourceDBTable']." AS tblMembers ";

		// Apply filtering options - STARTED
		$SQLQueryPart = array();
		if ($ArrayMailList['OutSourceDBWhereQuery'] != '')
			{
			$SQLQueryPart[] = $ArrayMailList['OutSourceDBWhereQuery'];
			}
		// Apply filtering options - FINISHED

		// Apply send rules - STARTED
		if (count($SQLQueryPart) > 0)
			{
			$SQLQuery = $SQLQuery." WHERE ".implode(" AND ", $SQLQueryPart)." ";
			$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " AND ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
			}
		else
			{
			$SQLQuery = $SQLQuery." ";
			$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " WHERE ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
			}
		// Apply send rules - FINISHED

		$SQLQuery .= "GROUP BY tblMembers.".$ArrayMailList['OutSourceDBEmailFieldName']." ";
		}
	else
		{
		$SQLQuery		= $ArrayMailList['OutSourceDBFullSQLQuery'];
		}

	return $SQLQuery;
	}
	
//########################################################################################
// Send final status to the administrator and assigned clients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendFinalReport($ArrayCampaign, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $ArrayLanguage, $ArrayClients)
	{
	// Initialize variables - STARTED
	$ErrorExists 	= false;
	// Initialize variables - FINISHED

	// Send report to administrator - STARTED
	if ($ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		// Retrieve administrator info - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdministrator = mysql_fetch_assoc($ResultSet);
		// Retrieve administrator info - FINISHED

		$ObjectMailer 			= new phpmailer();

		$ObjectMailer->IsMail();

		$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->AddAddress(strtolower($ArrayAdministrator['Email']), '');
		$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
		$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];

		$ObjectMailer->WordWrap	= 70;
		$ObjectMailer->Priority	= 5;
		$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
		$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
		$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00247"];

		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $ArrayLanguage["Texts"]["00248"];

			$ArrayReplaceList = array(
									"%Name%"					=>		$ArrayAdministrator['Name'],
									"%Campaign:CampaignName%"	=>		$ArrayCampaign['CampaignName'],
									"%TotalRecipients%"			=>		number_format($ArrayCampaignStatistics['TotalRecipients']),
									"%SentRecipients%"			=>		number_format($ArrayCampaignStatistics['SentRecipients']),
									"%FailedRecipients%"		=>		number_format($ArrayCampaignStatistics['FailedRecipients']),
									);
			$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);

		$ObjectMailer->Send();
		}
	// Send report to administrator - FINISHED
	
	// Send report to assigned clients - STARTED
	if ($ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		foreach ($ArrayClients as $Key => $ArrayEachClient)
			{
			$ObjectMailer 			= new phpmailer();
	
			$ObjectMailer->IsMail();
	
			$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
			$ObjectMailer->AddAddress(strtolower($ArrayEachClient['Email']), '');
			$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
			$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];
	
			$ObjectMailer->WordWrap	= 70;
			$ObjectMailer->Priority	= 5;
			$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
			$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
			$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00247"];
	
			$ObjectMailer->IsHTML(false);
			$ObjectMailer->Body		= $ArrayLanguage["Texts"]["00248"];
	
				$ArrayReplaceList = array(
										"%Name%"					=>		$ArrayEachClient['Name'],
										"%Campaign:CampaignName%"	=>		$ArrayCampaign['CampaignName'],
										"%TotalRecipients%"			=>		number_format($ArrayCampaignStatistics['TotalRecipients']),
										"%SentRecipients%"			=>		number_format($ArrayCampaignStatistics['SentRecipients']),
										"%FailedRecipients%"		=>		number_format($ArrayCampaignStatistics['FailedRecipients']),
										);
				$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
	
			$ObjectMailer->Send();
			}
		}
	// Send report to assigned clients - FINISHED
	
	return;
	}
	
//########################################################################################
// Send instant status to the administrator and assigned clients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendInstantReport($ArrayCampaign, $ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $ArrayLanguage, $ArrayClients)
	{
	// Initialize variables - STARTED
	$ErrorExists 	= false;
	// Initialize variables - FINISHED

	// Send report to administrator - STARTED
	if ($ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		// Retrieve administrator info - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdministrator = mysql_fetch_assoc($ResultSet);
		// Retrieve administrator info - FINISHED

		$ObjectMailer 			= new phpmailer();

		$ObjectMailer->IsMail();

		$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->AddAddress(strtolower($ArrayAdministrator['Email']), '');
		$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
		$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];

		$ObjectMailer->WordWrap	= 70;
		$ObjectMailer->Priority	= 5;
		$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
		$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
		$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00245"];

		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $ArrayLanguage["Texts"]["00246"];

			$ArrayReplaceList = array(
									"%Name%"					=>		$ArrayAdministrator['Name'],
									"%Campaign:CampaignName%"	=>		$ArrayCampaign['CampaignName'],
									"%TotalRecipients%"			=>		number_format($ArrayCampaignStatistics['TotalRecipients']),
									"%SentRecipients%"			=>		number_format($ArrayCampaignStatistics['SentRecipients']),
									"%FailedRecipients%"		=>		number_format($ArrayCampaignStatistics['FailedRecipients']),
									);
			$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);

		$ObjectMailer->Send();
		}
	// Send report to administrator - FINISHED
	
	// Send report to assigned clients - STARTED
	if ($ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		foreach ($ArrayClients as $Key => $ArrayEachClient)
			{
			$ObjectMailer 			= new phpmailer();
	
			$ObjectMailer->IsMail();
	
			$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
			$ObjectMailer->AddAddress(strtolower($ArrayEachClient['Email']), '');
			$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
			$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];
	
			$ObjectMailer->WordWrap	= 70;
			$ObjectMailer->Priority	= 5;
			$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
			$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
			$ObjectMailer->Subject	= $ArrayLanguage["Texts"]["00245"];
	
			$ObjectMailer->IsHTML(false);
			$ObjectMailer->Body		= $ArrayLanguage["Texts"]["00246"];
	
				$ArrayReplaceList = array(
										"%Name%"					=>		$ArrayEachClient['Name'],
										"%Campaign:CampaignName%"	=>		$ArrayCampaign['CampaignName'],
										"%TotalRecipients%"			=>		number_format($ArrayCampaignStatistics['TotalRecipients']),
										"%SentRecipients%"			=>		number_format($ArrayCampaignStatistics['SentRecipients']),
										"%FailedRecipients%"		=>		number_format($ArrayCampaignStatistics['FailedRecipients']),
										);
				$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
	
			$ObjectMailer->Send();
			}
		}
	// Send report to assigned clients - FINISHED
	
	return;
	}

//########################################################################################
// Send email (Using SwiftMailer)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendEmailSwift($ArrayEmailProperties, $ArrayConfig, $ArrayLanguage, $TMPSubject, $TMPContentMain, $TMPContentAlternate, $ArrayMember, $ArraySMTPProcessLog, $TotalLoopedMembers)
	{
		
	global $ObjectMailer;

	// Initialize variables - STARTED
	$ErrorExists 	= false;
	$ArrayError		= array();
	// Initialize variables - FINISHED

	// Construct the send engine object - Start
	$ObjectMailer = new OctethSwiftEngine($ArrayConfig['Paths']['Software'].'/system/classes');
	
	if ($ObjectMailer->ErrorStatus == true)
		{
		print ($ObjectMailer->ErrorInfo[0]);
		exit;
		}
	// Construct the send engine object - End

	// Define send engine - STARTED
	if ($ArrayEmailProperties['SendEngines']['Type'] == 'Sendmail')
		{
		$ObjectMailer->SendEngine		= 'SendMail';
		$ObjectMailer->TimeOut			= 3;
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'QMail')
		{
		$ObjectMailer->SendEngine		= 'SendMail';
		$ObjectMailer->TimeOut			= 3;
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'MTA')
		{
		$ObjectMailer->SendEngine		= 'NativeMail';
		$ObjectMailer->TimeOut			= 3;
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
		{
		$ObjectMailer->SendEngine		= 'NativeMail';
		$ObjectMailer->TimeOut			= 3;
		}
	elseif (($ArrayEmailProperties['SendEngines']['Type'] == 'SMTP') or ($ArrayEmailProperties['SendEngines'][0]['Type'] == 'SMTP'))
		{
		// Learn which SMTP server to process - STARTED
		$SMTPIndex = $ArraySMTPProcessLog['WhichSMTP'];
		if ($SMTPIndex == '')
			{
			$SMTPIndex = 0;
			$ArraySMTPProcessLog['WhichSMTP'] = 0;
			}
		// Learn which SMTP server to process - FINISHED

		// SMTP server load balancing - STARTED
		if ($TotalLoopedMembers % $ArrayEmailProperties['SendEngines'][$SMTPIndex]['MaxEmailsPerInterval'] == 0)
			{
			// Wait for the defined seconds - STARTED
			if ($ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval'] != 0)
				{
				
				sleep($ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval']);
				}
			// Wait for the defined seconds - FINISHED

			// Proceed to next SMTP server (if exists) - STARTED
			if ($ArrayEmailProperties['SendEngines'][$SMTPIndex + 1]['Type'] == 'SMTP')
				{
				$SMTPIndex = $SMTPIndex + 1;
				$ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			else
				{
				$SMTPIndex = 0;
				$ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			// Proceed to next SMTP server (if exists) - FINISHED
			}
		// SMTP server load balancing - FINISHED

		$ObjectMailer->SendEngine		= 'SMTP';
		$ObjectMailer->SMTPHost			= $ArrayEmailProperties['SendEngines'][$SMTPIndex]['Host'];
		$ObjectMailer->SMTPAuth			= ($ArrayEmailProperties['SendEngines'][$SMTPIndex]['Auth'] == 'Authorization' ? true : false);
		$ObjectMailer->SMTPUsername		= $ArrayEmailProperties['SendEngines'][$SMTPIndex]['Username'];
		$ObjectMailer->SMTPPassword		= $ArrayEmailProperties['SendEngines'][$SMTPIndex]['Password'];
		$ObjectMailer->SMTPPort			= $ArrayEmailProperties['SendEngines'][$SMTPIndex]['Port'];
		$ObjectMailer->SMTPSSL			= false;
		$ObjectMailer->TimeOut			= $ArrayEmailProperties['SendEngines'][$SMTPIndex]['TimeOutSec'];
		}

	$ObjectMailer->SetConnection();
	// Define send engine - FINISHED

	// Message content settings - Start
	$ObjectMailer->EmailHeaderList	= array(
											'X-Mailer'			=> $ArrayConfig['Settings']['XMailer'],
											'X-Mailer-MsgId'	=> base64_encode(strtolower($ArrayMember['Email'])),
											'X-Mailer-CSID'		=> base64_encode($ArrayEmailProperties['CampaignID'].'_'.$ArrayEmailProperties['CampaignStatisticsID']),
											);
	$ObjectMailer->Subject			= $TMPSubject;
	$ObjectMailer->FromName			= $ArrayEmailProperties['FromName'];
	$ObjectMailer->FromEmail		= $ArrayEmailProperties['FromEmail'];
	$ObjectMailer->ReplyToName		= $ArrayEmailProperties['ReplyToName'];
	$ObjectMailer->ReplyToEmail		= $ArrayEmailProperties['ReplyToEmail'];
	$ObjectMailer->ReturnPath		= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->CharSet			= substr($ArrayEmailProperties["CharSet"], 0, strlen($ArrayEmailProperties["CharSet"]) - 5);
	$ObjectMailer->Priority			= $ArrayEmailProperties['Priority'];
	$ObjectMailer->ToName			= '';
	$ObjectMailer->ToEmail			= $ArrayMember['Email'];
	// Message content settings - End

	// Set email body - STARTED
	if ($ArrayEmailProperties["ContentType"] == 'Plain')
		{
		$ObjectMailer->ContentType		= 'Plain';
		$ObjectMailer->MainContent		= $TMPContentMain;
		$ObjectMailer->AlternateContent	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Html')
		{
		$ObjectMailer->ContentType		= 'HTML';
		$ObjectMailer->MainContent		= $TMPContentMain;
		$ObjectMailer->AlternateContent	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Both')
		{
		$ObjectMailer->ContentType		= 'MultiPart';
		$ObjectMailer->MainContent		= $TMPContentMain;
		$ObjectMailer->AlternateContent	= $TMPContentAlternate;
		}
	// Set email body - FINISHED

	// If enabled, embed images to the email - STARTED
	if (($ArrayEmailProperties["ContentType"] == 'Html') || ($ArrayEmailProperties["ContentType"] == 'Both'))
		{
		if ($ArrayEmailProperties['EmbedObjects'] == true)
			{
			$ObjectMailer->EmbedImages		= false;
			}
		}
	// If enabled, embed images to the email - FINISHED

	// Insert attachments - STARTED
	$ObjectMailer->AttachmentList = array();
	
	foreach ($ArrayEmailProperties['Attachments'] as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$ArrayConfig['Paths']['Data']."/campaign_attachments/".$ArrayEmailProperties['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$ObjectMailer->AttachmentList[$AttachmentName] = $AttachmentPath;
		}
	// Insert attachments - FINISHED

	// Build the message - Start
	$ObjectMailer->BuildMessage();

	if ($ObjectMailer->ErrorStatus == true)
		{
		print ($ObjectMailer->ErrorInfo[0]);
		exit;
		}
	// Build the message - End

	// Deliver the email - Start
	if ($ArrayConfig['Settings']['SystemDebugMode'] != 'Enabled')
		{
		if ($ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
			{
			$TMPSendEmail = false;
			$TMPSaveEmail = true;
			}
		else
			{
			$TMPSendEmail = true;
			$TMPSaveEmail = false;
			}
		
		$EmailContent = $ObjectMailer->SendMessage($TMPSendEmail, $TMPSaveEmail);

		// Save the returned raw email to the defined directory - STARTED
		if ($ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
			{
				$TMPFileName = strtotime(date('Y-m-d H:i:s')).rand(1000000, 9999999);
			if (($TMPFileHandler = fopen($ArrayConfig['Settings']['SaveRawEmailPath'].$TMPFileName, 'w')) == true)
				{
					fwrite($TMPFileHandler, $EmailContent);
				fclose($TMPFileHandler);
				}
			else
				{
				$ArrayError = array(true, $ArrayLanguage["Texts"]["00432"]);
				}
			}
		// Save the returned raw email to the defined directory - FINISHED

		if ($ObjectMailer->ErrorStatus == true)
			{
			print_r($ObjectMailer->ErrorInfo);
			exit;
			}
		}
	// Deliver the email - End

	// Detect if any error occured or not - STARTED
	if ($ObjectMailer->ErrorStatus == true)
		{
		$ArrayError = array(true, $ObjectMailer->ErrorInfo);
		}
	// Detect if any error occured or not - FINISHED

	$ObjectMailer->ObjectSwift->disconnect();
	
	return array($ArraySMTPProcessLog, $ArrayError);
	}

//########################################################################################
// Send email
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function SendEmail($ArrayEmailProperties, $ArrayConfig, $ArrayLanguage, $TMPSubject, $TMPContentMain, $TMPContentAlternate, $ArrayMember, $ArraySMTPProcessLog, $TotalLoopedMembers)
	{
	// Initialize variables - STARTED
	$ErrorExists 	= false;
	$ArrayError		= array();
	// Initialize variables - FINISHED

	$ObjectMailer 			= new phpmailer();

	// Define send engine - STARTED
	if ($ArrayEmailProperties['SendEngines']['Type'] == 'Sendmail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['SendmailPath'];
		$ObjectMailer->IsSendmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'QMail')
		{
			$ObjectMailer->Sendmail	= $ArrayConfig['Settings']['QMailPath'];
		$ObjectMailer->IsQmail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'MTA')
		{
		$ObjectMailer->IsMail();
		}
	elseif ($ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
		{
		$ObjectMailer->IsSaveAsFile();
		}
	elseif (($ArrayEmailProperties['SendEngines']['Type'] == 'SMTP') or ($ArrayEmailProperties['SendEngines'][0]['Type'] == 'SMTP'))
		{
		// Learn which SMTP server to process - STARTED
		$SMTPIndex = $ArraySMTPProcessLog['WhichSMTP'];
		if ($SMTPIndex == '')
			{
			$SMTPIndex = 0;
			$ArraySMTPProcessLog['WhichSMTP'] = 0;
			}
		// Learn which SMTP server to process - FINISHED

		// SMTP server load balancing - STARTED
		if ($TotalLoopedMembers % $ArrayEmailProperties['SendEngines'][$SMTPIndex]['MaxEmailsPerInterval'] == 0)
			{
			// Wait for the defined seconds - STARTED
			if ($ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval'] != 0)
				{
				sleep($ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval']);
				}
			// Wait for the defined seconds - FINISHED

			// Proceed to next SMTP server (if exists) - STARTED
			if ($ArrayEmailProperties['SendEngines'][$SMTPIndex + 1]['Type'] == 'SMTP')
				{
				$SMTPIndex = $SMTPIndex + 1;
				$ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			else
				{
				$SMTPIndex = 0;
				$ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			// Proceed to next SMTP server (if exists) - FINISHED
			}
		// SMTP server load balancing - FINISHED

			$ObjectMailer->Host		=	$ArrayEmailProperties['SendEngines'][$SMTPIndex]['Host'];
			$ObjectMailer->Port		=	$ArrayEmailProperties['SendEngines'][$SMTPIndex]['Port'];
			$ObjectMailer->SMTPAuth	=	($ArrayEmailProperties['SendEngines'][$SMTPIndex]['Auth'] == 'Authorization' ? true : false);
			$ObjectMailer->Username	=	$ArrayEmailProperties['SendEngines'][$SMTPIndex]['Username'];
			$ObjectMailer->Password	=	$ArrayEmailProperties['SendEngines'][$SMTPIndex]['Password'];
			$ObjectMailer->Timeout	=	$ArrayEmailProperties['SendEngines'][$SMTPIndex]['TimeOutSec'];
			$ObjectMailer->SMTPKeepAlive	=	false;
		$ObjectMailer->IsSMTP();
		}
	// Define send engine - FINISHED

	// Set email addresses - STARTED
	$ObjectMailer->From			= $ArrayEmailProperties['FromEmail'];
	$ObjectMailer->FromName		= $ArrayEmailProperties['FromName'];
	$ObjectMailer->ReturnPath	= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->Sender		= $ArrayEmailProperties['ReturnPathEmail'];
	$ObjectMailer->AddReplyTo($ArrayEmailProperties['ReplyToEmail'], $ArrayEmailProperties['ReplyToName']);
	$ObjectMailer->AddAddress(strtolower($ArrayMember['Email']), '');
	// Set email addresses - FINISHED
	
	// Set email properties - STARTED
	$ObjectMailer->WordWrap	= 70;
	$ObjectMailer->Priority	= $ArrayEmailProperties['Priority'];
	$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
	$ObjectMailer->CharSet	= substr($ArrayEmailProperties["CharSet"], 0, strlen($ArrayEmailProperties["CharSet"]) - 5);
	$ObjectMailer->Subject	= $TMPSubject;

	$link_start = substr($TMPContentMain, strpos($TMPContentMain, $ArrayConfig['URLs']['Software'].'/unsubscribe.php?'));
    $unsubscribe = substr($link_start, 0, strpos($link_start,'"'));
    $ObjectMailer->listUnsubscribe = $unsubscribe;
    $ObjectMailer->subscriptionDate = $ArrayMember['SubscriptionDate'];
    $ObjectMailer->AddCustomHeader("X-Mailer-MsgId: ".base64_encode(strtolower($ArrayMember['Email'])));
	$ObjectMailer->AddCustomHeader("X-Mailer-CSID: ".base64_encode($ArrayEmailProperties['CampaignID'].'_'.$ArrayEmailProperties['CampaignStatisticsID']));
	// Set email properties - FINISHED
    
    
	// Set email body - STARTED
	if ($ArrayEmailProperties["ContentType"] == 'Plain')
		{
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $TMPContentMain;
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Html')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $TMPContentMain;
		$ObjectMailer->AltBody	= '';
		}
	elseif ($ArrayEmailProperties["ContentType"] == 'Both')
		{
		$ObjectMailer->IsHTML(true);
		$ObjectMailer->Body		= $TMPContentMain;
		$ObjectMailer->AltBody	= $TMPContentAlternate;
		}
	// Set email body - FINISHED

	// If enabled, embed images to the email - STARTED
	if (($ArrayEmailProperties["ContentType"] == 'Html') || ($ArrayEmailProperties["ContentType"] == 'Both'))
		{
		if ($ArrayEmailProperties['EmbedObjects'] == true)
			{
				$Content = $ObjectMailer->Body;
			$ArrayReturn = EmbedObjects($ObjectMailer, $Content, $ArrayConfig);
			
			$ObjectMailer 			= $ArrayReturn[0];
			$ObjectMailer->Body		= $ArrayReturn[1];
			}
		}
	// If enabled, embed images to the email - FINISHED
	
	// Insert attachments - STARTED
	foreach ($ArrayEmailProperties['Attachments'] as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$ArrayConfig['Paths']['Data']."/campaign_attachments/".$ArrayEmailProperties['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$ObjectMailer->AddAttachment($AttachmentPath, $AttachmentName);
		}
	// Insert attachments - FINISHED

	// Send the email - STARTED
	if ($ArrayConfig['Settings']['SystemDebugMode'] != 'Enabled')
		{
		$TMPReturn = $ObjectMailer->Send();

		// Save the returned raw email to the defined directory - STARTED
		if ($ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
			{
				$TMPFileName = strtotime(date('Y-m-d H:i:s')).rand(1000000, 9999999);
			if (($TMPFileHandler = fopen($ArrayConfig['Settings']['SaveRawEmailPath'].$TMPFileName, 'w')) == true)
				{
					fwrite($TMPFileHandler, $TMPReturn);
				fclose($TMPFileHandler);
				}
			else
				{
				$ArrayError = array(true, $ArrayLanguage["Texts"]["00432"]);
				}
			}
		// Save the returned raw email to the defined directory - FINISHED
		}
	// Send the email - FINISHED
	
	// Detect if any error occured or not - STARTED
	if ($ObjectMailer->ErrorInfo != '')
		{
		$ArrayError = array(true, $ObjectMailer->ErrorInfo);
		}
	// Detect if any error occured or not - FINISHED

	return array($ArraySMTPProcessLog, $ArrayError);
	}
	
//########################################################################################
// Calculate the total recipients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CalculateTotalRecipients($ArrayCampaign, $ArrayCampaignStatistics, $ArrayConfig, $IsResumed)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Retrieve mail lists - STARTED
	$ArrayMailLists = array();
	
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayMailLists[]	=	array(
									"MailListID"		=>	$EachRow['MailListID'],
									"SourceType"		=>	$EachRow['SourceType'],
									);
		}
	// Retrieve mail lists - FINISHED

	// Retrieve and prepare recipients (local) - STARTED
	$TMPSQLQuery = array();
	foreach ($ArrayMailLists as $ArrayVals)
		{
		$TMPSQLQuery[] = "tblAssign.RelMailListID = '".$ArrayVals['MailListID']."'";
		}
	$TMPSQLQuery = implode(" OR ", $TMPSQLQuery);
	$TMPSQLQuery = "(".$TMPSQLQuery.")";

	// Query for retrieving members - STARTED
	$SQLQuery 			 = "SELECT tblMembers.*, tblAssign.* ";
		$SQLQuery		.= "FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery		.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQuery		.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";

	// Query for calculating the amount of recipiens - STARTED
	$SQLQueryTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryTotal	.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of recipiens - FINISHED

	// Query for calculating the amount of ALL recipiens (ignoring resume) - STARTED
	$SQLQueryOverAllTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryOverAllTotal	.= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryOverAllTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of ALL recipiens (ignoring resume) - FINISHED

	$SQLQueryPart = array();
		$SQLQueryPart[] = $TMPSQLQuery;
		$SQLQueryPart[] = "tblMembers.BounceType!='Hard bounce'";
		$SQLQueryPart[] = "(tblAssign.SubscriptionStatus='Subscribed')";
		
		// Resume if paused - STARTED
		if ($IsResumed == true)
			{
			$SQLQueryPart[] = "tblMembers.MemberID > '".$ArrayCampaignStatistics['QueuePointer']."'";
			}
		// Resume if paused - FINISHED

		// Apply sending rules - STARTED
		$TMPArraySQLQueryPartSendRules = array();
		
		$TMPSQLQuerySendRules = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$TMPResultSetSendRules = ExecuteMySQLQuery($TMPSQLQuerySendRules, __LINE__, __FILE__);
			$TotalSendRules = mysql_num_rows($TMPResultSetSendRules);
			
		$TMPArraySendRule = array();
		$TMPCounter = 1;
		while ($EachRule = mysql_fetch_assoc($TMPResultSetSendRules))
			{
			$TMPSendRule = "";

			if ($EachRule['Operator1'] == 'equals to')
				{
				$TMPOperator1 = '=';
				}
			elseif ($EachRule['Operator1'] == 'greater than')
				{
				$TMPOperator1 = '>';
				}
			elseif ($EachRule['Operator1'] == 'smaller than')
				{
				$TMPOperator1 = '<';
				}
			elseif ($EachRule['Operator1'] == 'not equals to')
				{
				$TMPOperator1 = '!=';
				}
			elseif ($EachRule['Operator1'] == 'includes')
				{
				$TMPOperator1 = 'LIKE';
				}
			elseif ($EachRule['Operator1'] == 'not includes')
				{
				$TMPOperator1 = 'NOT LIKE';
				}
			else
				{
				$TMPOperator1 = '=';
				}

			if (($TMPOperator1 == 'LIKE') || ($TMPOperator1 == 'NOT LIKE'))
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '%".$EachRule['Value1']."%'";
				}
			else
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '".$EachRule['Value1']."'";
				}

			if ($EachRule['Boolean1'] != '')
				{
				if ($EachRule['Operator2'] == 'equals to')
					{
					$TMPOperator2 = '=';
					}
				elseif ($EachRule['Operator2'] == 'greater than')
					{
					$TMPOperator2 = '>';
					}
				elseif ($EachRule['Operator2'] == 'smaller than')
					{
					$TMPOperator2 = '<';
					}
				elseif ($EachRule['Operator2'] == 'not equals to')
					{
					$TMPOperator2 = '!=';
					}
				elseif ($EachRule['Operator2'] == 'includes')
					{
					$TMPOperator2 = 'LIKE';
					}
				elseif ($EachRule['Operator2'] == 'not includes')
					{
					$TMPOperator2 = 'NOT LIKE';
					}
				else
					{
					$TMPOperator2 = '=';
					}

				if (($TMPOperator2 == 'LIKE') || ($TMPOperator2 == 'NOT LIKE'))
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '%".$EachRule['Value2']."%'";
					}
				else
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '".$EachRule['Value2']."'";
					}
				}
			
			if ($TotalSendRules > $TMPCounter)
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean2']." ";
				}
			
			$TMPCounter++;

			$TMPArraySendRule[] = $TMPSendRule;
			}

		if ($TotalSendRules > 0)
			{
			$TMPSendRule = implode("", $TMPArraySendRule);
				$TMPSendRule = "(".$TMPSendRule.")";

			$SQLQueryPart[] = $TMPSendRule;
			$TMPArraySQLQueryPartSendRules[] = $TMPSendRule;
			}
		// Apply sending rules - FINISHED

	$SQLQuery = $SQLQuery.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
	$SQLQueryTotal = $SQLQueryTotal.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";

	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - STARTED
	if ($IsResumed == true)
		{
			$TMPOverAllSQLQueryPart = $SQLQueryPart;
			$TMPOverAllSQLQueryPart[3] = "tblMembers.MemberID > '0'";
		$SQLQueryOverAllTotal = $SQLQueryOverAllTotal.(count($TMPOverAllSQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $TMPOverAllSQLQueryPart) : '')." ";
		}
	else
		{
		$SQLQueryOverAllTotal = $SQLQueryTotal;
		}
	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - FINISHED

	$SQLQuery .= "GROUP BY tblMembers.MemberID ";
	$SQLQuery .= "ORDER BY tblMembers.MemberID ";
	// Query for retrieving members - FINISHED

	$ResultSetTotal = ExecuteMySQLQuery($SQLQueryTotal, __LINE__, __FILE__);
	$ResultSetOverAllTotal = ExecuteMySQLQuery($SQLQueryOverAllTotal, __LINE__, __FILE__);
	
	$TotalRecords	= mysql_fetch_assoc($ResultSetTotal);
		$TotalRecords = $TotalRecords['TotalFound'];
	$TotalIntervals		= ceil($TotalRecords / $EmailsPerInterval);
		$TotalIntervals = ($TotalIntervals == 0 ? 1 : $TotalIntervals);

	$OverAllTotalRecords = mysql_fetch_assoc($ResultSetOverAllTotal);
		$OverAllTotalRecords = $OverAllTotalRecords['TotalFound'];
	
	mysql_free_result($ResultSetTotal);
	// Retrieve and prepare recipients (local) - FINISHED

	// Retrieve the total amount of outsourced recipients - STARTED
	include_once("./system/libraries/maillists.inc.php");

	$TotalOutsourcedRecipients = 0;

	$ArrayOutsourcedMailLists = array();

	foreach ($ArrayMailLists as $Index => $ArrayValues)
		{
		if ($ArrayValues['SourceType'] == 'Outsourced - MySQL')
			{
			$ArrayOutsourcedMailLists[] = $ArrayValues['MailListID'];
			
			$SQLQueryMailList = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$ArrayValues['MailListID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQueryMailList, __LINE__, __FILE__);
				$ArrayOutsourcedMailList = mysql_fetch_assoc($ResultSet);

			$TotalOutsourcedRecipients += FindTotalMemberOfOutsourcedList($ArrayOutsourcedMailList, $ArrayConfig, $TMPArraySQLQueryPartSendRules);
			}
		}
	// Retrieve the total amount of outsourced recipients - FINISHED

	// Re-define the total amounts - STARTED
	$OverAllTotalRecords		= $OverAllTotalRecords + $TotalOutsourcedRecipients;
	$TotalOutsourcedRecipients	= $TotalOutsourcedRecipients;
	$TotalRecords				= $TotalRecords + $TotalOutsourcedRecipients;
	$TotalIntervals				= $TotalIntervals;
	// Re-define the total amounts - FINISHED

	return array($SQLQuery, $TotalRecords, $TotalIntervals, $OverAllTotalRecords, $TotalOutsourcedRecipients, $ArrayOutsourcedMailLists);
	}

//########################################################################################
// Defines the email properties
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DefineEmailProperties($ArrayCampaign, $ArrayCampaignStatistics, $ArrayContent, $ArrayNewsletter, $ArrayFromEmail, $ArrayReplyToEmail, $ArrayReturnPathEmail, $ArrayAttachments, $ArraySMTP, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Define subject - STARTED
	$TMPSubject = $ArrayContent['Subject'];
	// Define subject - FINISHED
		
	// Define content - STARTED
	if (count($ArrayNewsletter) > 0)
		{
		// CMS based content
		$TMPPriority 			= $ArrayNewsletter['Priority'];
		$TMPCharSet				= $ArrayNewsletter['CharSet'];
		$TMPContentType			= $ArrayNewsletter['ContentType'];
		if ($TMPContentType == 'Html')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}
		elseif ($TMPContentType == 'Both')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentHTML'], $ArrayContent['CMSParts']);
			$TMPContentAlternate	= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
			}
		elseif ($TMPContentType == 'Plain')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($ArrayNewsletter['ContentPlain'], $ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}

		// Add administrator header and footer to the content - STARTED
			include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
			$TMPArrayContent = array(
									"ContentMain"		=> $TMPContentMain,
									"ContentAlternate"	=> $TMPContentAlternate,
									"ContentType"		=> $TMPContentType,
									);
		$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
			$TMPContentMain = $TMPArrayContent['ContentMain'];
			$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
		// Add administrator header and footer to the content - FINISHED
		}
	else
		{
		// Traditional template or quick mode
		$TMPPriority 			= $ArrayContent['Priority'];
		$TMPCharSet				= $ArrayContent['CharSet'];
		$TMPContentType			= $ArrayContent['ContentType'];
		$TMPContentMain			= $ArrayContent['ContentMain'];
		$TMPContentAlternate	= $ArrayContent['ContentAlternate'];
		}
	// Define content - FINISHED

	// Analyze subject. If it is in "multi-mode", explode into array - STARTED
	if (count(explode("\n", $TMPSubject)) > 1)
		{
		$TMPSubject = explode("\n", $TMPSubject);
		}
	// Analyze subject. If it is in "multi-mode", explode into array - FINISHED

	// Check if the campaign owner administrator has a pre-defined return-path email address - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayCampaignAdministrator = mysql_fetch_assoc($ResultSet);
	// Check if the campaign owner administrator has a pre-defined return-path email address - FINISHED

	$ArrayEmailProperties = array();
		$ArrayEmailProperties	= array(
										"CampaignID"		=>	$ArrayCampaign['CampaignID'],
										"CampaignStatisticsID"		=>	$ArrayCampaignStatistics['CampaignStatisticsID'],
										"EmbedObjects"		=>	($ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
										"FromEmail"			=>	$ArrayFromEmail['EmailAddress'],
										"FromName"			=>	$ArrayFromEmail['Name'],
										"ReplyToEmail"		=>	$ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"		=>	$ArrayReplyToEmail['Name'],
										"ReturnPathEmail"	=>	($ArrayCampaignAdministrator['ReturnPathEmail'] == '' ? ($ArrayReturnPathEmail['EmailAddress'] != '' ? $ArrayReturnPathEmail['EmailAddress'] : $ArrayReplyToEmail['EmailAddress']) : $ArrayCampaignAdministrator['ReturnPathEmail']),
										"Priority"			=>	$TMPPriority,
										"CharSet"			=>	$TMPCharSet,
										"Subject"			=>	$TMPSubject,
										"ContentType"		=>	$TMPContentType,
										"ContentMain"		=>	$TMPContentMain,
										"ContentAlternate"	=>	$TMPContentAlternate,
										"Attachments"		=>	$ArrayAttachments,
										"SendEngines"		=>	$ArraySMTP,
										);

	return $ArrayEmailProperties;
	}

//########################################################################################
// Retrieve attachments of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveEmailAddresses($ArrayCampaign, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	if ($ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);



	return array($ArrayFromEmail, $ArrayReplyToEmail, $ArrayReturnPathEmail);
	}

//########################################################################################
// Retrieve content of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveContent($ArrayCampaign, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	if ($ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = RetrieveRemoteContent($ArrayContent['ContentMainURL'], $ArrayConfig);
			$ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = RetrieveRemoteContent($ArrayContent['ContentAlternateURL'], $ArrayConfig);
			$ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			// CMS based newsletter
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayNewsletter = mysql_fetch_assoc($ResultSet);

			// Copy charset, content-type, priority info to content array - STARTED
			$ArrayContent['CharSet']		=	$ArrayNewsletter['CharSet'];
			$ArrayContent['Priority']		=	$ArrayNewsletter['Priority'];
			$ArrayContent['ContentType']	=	$ArrayNewsletter['ContentType'];
			// Copy charset, content-type, priority info to content array - FINISHED
			}
		else
			{
			// Traditional newsletter
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}

	// Add administrator header and footer to the content - STARTED
		include_once($ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$ArrayContent = AddAdministratorHeaderFooter($ArrayContent, $ArrayCampaign['RelAdministratorID'], $ArrayConfig);
	// Add administrator header and footer to the content - FINISHED

	return array($ArrayContent, $ArrayNewsletter);
	}

//########################################################################################
// Retrieve attachments of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveAttachments($ArrayCampaign, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayAttachments[] = array(
									"AttachmentID"		=>	$EachRow['AttachmentID'],
									"FileName"			=>	$EachRow['FileName'],
									"FileSize"			=>	$EachRow['FileSize'],
									);
		}

	return $ArrayAttachments;
	}
	
//########################################################################################
// Retrieve clients
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveClients($ArrayCampaign, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_clients AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."clients AS tblClients ";
	$SQLQuery .= "ON tblAssign.RelClientID = tblClients.ClientID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$ArrayClients = array();

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ArrayClients[] = array(
								"ClientID"		=>		$EachRow['ClientID'],
								"Name"			=>		$EachRow['Name'],
								"Email"			=>		$EachRow['Email'],
								);
		}

	return $ArrayClients;
	}
	
//########################################################################################
// Retrieve SMTP information of the campaign
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveSengEngines($ArrayCampaign, $ArrayAdministrator, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	// Initialize variables - FINISHED

	// Retrieve campaign SMTP settings - STARTED
	$ArraySMTP = array();

	if ($ArrayAdministrator['ForcedSendingMethod'] != 'N/A')
		{
		if ($ArrayAdministrator['ForcedSendingMethod'] == 'Qmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'QMail',
							);
			}
		elseif ($ArrayAdministrator['ForcedSendingMethod'] == 'Sendmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'Sendmail',
							);
			}
		elseif ($ArrayAdministrator['ForcedSendingMethod'] == 'Local MTA')
			{
			$ArraySMTP = array(
							'Type'		=>	'MTA',
							);
			}
		elseif ($ArrayAdministrator['ForcedSendingMethod'] == 'SMTP Server')
			{
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "WHERE tblSMTP.SMTPServerID = '".$ArrayAdministrator['ForcedSendingMethodID']."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TMPArraySMTP = mysql_fetch_assoc($ResultSet);

			$ArraySMTP[] = array(
							'Type'		=>	'SMTP',
							'Host'		=>	$TMPArraySMTP['ServerHost'],
							'Port'		=>	$TMPArraySMTP['ServerPort'],
							'Auth'		=>	$TMPArraySMTP['ServerAuth'],
							'Username'	=>	$TMPArraySMTP['ServerUsername'],
							'Password'	=>	$TMPArraySMTP['ServerPassword'],
							'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
							);
			}
		elseif ($ArrayAdministrator['ForcedSendingMethod'] == 'Raw File')
			{
			$ArraySMTP = array(
							'Type'		=>	'SaveAsFile',
							);
			}
		}
	else
		{
		if ($ArrayCampaign['SendEngine'] == 'SMTP')
			{
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($TMPArraySMTP = mysql_fetch_assoc($ResultSet))
				{
				$ArraySMTP[] = array(
								'Type'		=>	'SMTP',
								'Host'		=>	$TMPArraySMTP['ServerHost'],
								'Port'		=>	$TMPArraySMTP['ServerPort'],
								'Auth'		=>	$TMPArraySMTP['ServerAuth'],
								'Username'	=>	$TMPArraySMTP['ServerUsername'],
								'Password'	=>	$TMPArraySMTP['ServerPassword'],
								'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
								'MaxEmailsPerInterval'		=>	$TMPArraySMTP['MaxEmailsPerInterval'],
								'SecondsDelayPerInterval'	=>	$TMPArraySMTP['SecondsDelayPerInterval'],
								);
				}
			}
		elseif ($ArrayCampaign['SendEngine'] == 'MTA')
			{
			$ArraySMTP = array(
							'Type'		=>	'MTA',
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'QMail')
			{
			$ArraySMTP = array(
							'Type'		=>	'QMail',
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'Sendmail')
			{
			$ArraySMTP = array(
							'Type'		=>	'Sendmail',
							);
			}
		elseif ($ArrayCampaign['SendEngine'] == 'SaveAsFile')
			{
			$ArraySMTP = array(
							'Type'		=>	'SaveAsFile',
							);
			}
		}
	// Retrieve campaign SMTP settings - FINISHED

	return $ArraySMTP;
	}

//########################################################################################
// Retrieve the list of all campaigns that should start
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrievePendingCampaigns($ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists		= false;
	$ArrayCampaignIDs	= array();
	// Initialize variables - FINISHED

	// Retrieve all "send now" campaigns - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
		$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
		$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
		$SQLQuery .= "WHERE tblSchedule.ScheduleType='Send Now' AND IntegrityCheck='Ready'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// Check if this campaign has been sent or not - STARTED
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalFound = $TotalFound['TotalFound'];
			if ($TotalFound > 0)
				{
				continue;
				}
			// Check if this campaign has been sent or not - FINISHED

			$ArrayCampaignIDs[] = $EachRow['CampaignID'];
			}
		}
	// Retrieve all "send now" campaigns - FINISHED

	// Retrieve all "future scheduled" campaigns - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
		$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
		$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
		$SQLQuery .= "WHERE tblSchedule.ScheduleType='One time scheduled' AND (tblSchedule.ScheduleSendDate <= NOW() OR tblSchedule.ScheduleSendDate = '0000-00-00 00:00:00') AND tblCampaigns.IntegrityCheck='Ready'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// Check if this campaign has been sent or not - STARTED
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalFound = $TotalFound['TotalFound'];
			if ($TotalFound > 0)
				{
				continue;
				}
			// Check if this campaign has been sent or not - FINISHED

			$ArrayCampaignIDs[] = $EachRow['CampaignID'];
			}
		}
        
	// Retrieve all "future scheduled" campaigns - FINISHED

	// Retrieve all "recursive scheduled" campaigns - STARTED
	if ($ErrorExists == false)
		{
		$CurrentDayOfMonth	=	date("j");
		$CurrentDayOfWeek	=	date("w");
		$CurrentWeekOfMonth	= ((int)($CurrentDayOfMonth / 7));
			$CurrentWeekOfMonthModulus = $CurrentDayOfMonth % 7;
			if ($CurrentWeekOfMonthModulus > 0)
				{
				$CurrentWeekOfMonth++;
				}
		$CurrentMonth		=	date("n");
		$CurrentHour		=	date("G");
		$CurrentMinute		=	date("i");
			// Strip leading zero from the minute - STARTED
			if (substr($CurrentMinute, 0, 1) == '0')
				{
				$CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
				}
			// Strip leading zero from the minute - FINISHED
			
			// Round the minute with 15 minutes intervals - STARTED
			if (($CurrentMinute >= 0) && ($CurrentMinute <= 14))
				{
				$CurrentMinute = 0;
				}
			elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29))
				{
				$CurrentMinute = 15;
				}
			elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44))
				{
				$CurrentMinute = 30;
				}
			elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59))
				{
				$CurrentMinute = 45;
				}
			// Round the minute with 15 minutes intervals - FINISHED

		$SQLQuery  = "SELECT tblCampaigns.CampaignID, tblSchedule.ScheduleID ";
		$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
		$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
		$SQLQuery .= "WHERE tblSchedule.ScheduleType='Continuous scheduled' ";
		$SQLQuery .= "AND (tblSchedule.ScheduleMonth = '*' OR tblSchedule.ScheduleMonth='".$CurrentMonth."' OR tblSchedule.ScheduleMonth LIKE '".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth."') ";
		$SQLQuery .= "AND (((tblSchedule.ScheduleDayOfMonth = '*' OR tblSchedule.ScheduleDayOfMonth='".$CurrentDayOfMonth."' OR tblSchedule.ScheduleDayOfMonth LIKE '".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth."') ";
		$SQLQuery .= "AND (tblSchedule.ScheduleDayOfWeek = '*' OR tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."' OR tblSchedule.ScheduleDayOfWeek LIKE '".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek."')) ";
		$SQLQuery .= "OR (tblSchedule.ScheduleDayOfMonth='".$CurrentWeekOfMonth."' AND tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."'))";
		$SQLQuery .= "AND (tblSchedule.ScheduleHour = '*' OR tblSchedule.ScheduleHour='".$CurrentHour."' OR tblSchedule.ScheduleHour LIKE '".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour."') ";
		$SQLQuery .= "AND (tblSchedule.ScheduleMinute = '*' OR tblSchedule.ScheduleMinute='".$CurrentMinute."' OR tblSchedule.ScheduleMinute LIKE '".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute."') ";
		$SQLQuery .= "AND (tblSchedule.ScheduleSendInstance < tblSchedule.ScheduleSendMaxInstance OR tblSchedule.ScheduleSendMaxInstance='0') ";
		$SQLQuery .= "AND tblCampaigns.IntegrityCheck='Ready' ";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			$ProcessedOn = date("Y-m-d H:".sprintf('%02d', $CurrentMinute));
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// Check if this sending has been ran before or not - STARTED
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive WHERE RelCampaignID='".$EachRow['CampaignID']."' AND DATE_FORMAT(ProcessedOn, '%Y-%m-%d %H:%i')='".$ProcessedOn."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalFound = $TotalFound['TotalFound'];

			if ($TotalFound > 0)
				{
				continue;
				}
			// Check if this sending has been ran before or not - FINISHED

			// Record the process to "campaigns_schedule_recursive" - STARTED
			$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive (ID, RelCampaignID, ProcessedOn) VALUES ('', '".$EachRow['CampaignID']."', '".$ProcessedOn."')";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			// Record the process to "campaigns_schedule_recursive" - FINISHED

			$ArrayCampaignIDs[] = $EachRow['CampaignID'];
			}
		}
	// Retrieve all "recursive scheduled" campaigns - FINISHED

	// Retrieve all "resumed" campaigns - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics ocs, oemp_campaigns_schedule ocsh WHERE ocs.RelCampaignID = ocsh.RelCampaignID AND ocs.Status='Pending' AND ocsh.ScheduleType != 'Not scheduled'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$ArrayCampaignIDs[] = $EachRow['RelCampaignID'];
			}
		}	
	// Retrieve all "resumed" campaigns - FINISHED

	// Check if owner of these campaigns have enough sending quota - STARTED
	if ($ErrorExists == false)
		{
		$TMPArrayCampaignIDs	= $ArrayCampaignIDs;
		$ArrayCampaignIDs		= array();
		
		foreach ($TMPArrayCampaignIDs as $EachCampaignID)
			{
			$IsAllowedQuota		= false;
			$IsAllowedTimeLimit = false;

			// Check available send amount limit - STARTED
			$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."campaigns AS tblCampaigns ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."administrators as tblAdmins ";
			$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdmins.AdministratorID ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."administrator_limits as tblAdminLimits ";
			$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdminLimits.RelAdministratorID ";
			$SQLQuery .= "WHERE tblCampaigns.CampaignID = '".$EachCampaignID."' ";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayCampaignAdminLimits = mysql_fetch_assoc($ResultSet);

			if ($ArrayCampaignAdminLimits['Limit'] != 'Unlimited')
				{
				include_once('./system/libraries/quota.inc.php');
				$QuotaUsed = CalculateSendLimits($ArrayCampaignAdminLimits['AdministratorID'], $ArrayCampaignAdminLimits, $ArrayConfig);
				$RemainingQuota = $ArrayCampaignAdminLimits['LimitAmount'] - $QuotaUsed;
				
				if ($RemainingQuota > 0)
					{
					$IsAllowedQuota = true;
					}
				}
			else
				{
				$IsAllowedQuota = true;
				}
			// Check available send amount limit - FINISHED

			// Check available send time limit - STARTED
			$TodayDate	= date('D');
			$TodayTime	= strtotime(date('Y-m-d H:i:s'));
			$AllowedDays = explode(',', $ArrayCampaignAdminLimits['TimeLimitDates']);

			if ($AllowedDays[0] != '')
				{
				foreach ($AllowedDays as $EachDay)
					{
					if ($TodayDate == $EachDay)
						{
						if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitTo']))))
							{
							$IsAllowedTimeLimit = true;
							break;
							}
						}
					}
				}
			else
				{
				$IsAllowedTimeLimit = true;
				}
			// Check available send time limit - FINISHED

			if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
				{
				$ArrayCampaignIDs[] = $EachCampaignID;
				}
			}
		}
	// Check if owner of these campaigns have enough sending quota - FINISHED

	// Check if these campaigns have active sending or not - STARTED
	if ($ErrorExists == false)
		{
		$TMPArrayCampaignIDs	= $ArrayCampaignIDs;
		$ArrayCampaignIDs		= array();
		
		foreach ($TMPArrayCampaignIDs as $EachCampaignID)
			{
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachCampaignID."' AND Status='Sending'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet);
				$TotalFound = $TotalFound['TotalFound'];
			if ($TotalFound < 1)
				{
				$ArrayCampaignIDs[] = $EachCampaignID;
				}
			}
		}	
	// Check if these campaigns have active sending or not - FINISHED

	$ArrayCampaignIDs = array_unique($ArrayCampaignIDs);
	return $ArrayCampaignIDs;
	}

//########################################################################################
// Update quota usage
function UpdateQuotaUsage($AddAmount, $CampaignID, $AdministratorID, $ArrayConfig)
	{
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE RelAdministratorID='".$AdministratorID."' AND ActivityHour='".date('h')."' AND ActivityDay='".date('d')."' AND ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayQuota = mysql_fetch_assoc($ResultSet);

	$NewAmount = $ArrayQuota['SendAmount'] + $AddAmount;

	$SQLQuery  = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."administrator_activities ";
	$SQLQuery .= "(`QuotaID`, `RelAdministratorID`, `ActivityHour`, `ActivityDay`, `ActivityWeek`, `ActivityMonth`, `ActivityYear`, `SendAmount`) ";
	$SQLQuery .= "VALUES ('".$ArrayQuota['QuotaID']."', '".$AdministratorID."', '".date('h')."', ".date('d').", ".date('W').", ".date('m').", ".date('Y').", ".$NewAmount.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;	
	}

//########################################################################################
// Checks if there is enough sending quota and time limit for this process
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckQuotaAndTimeLimit($AdministratorID, $ArrayConfig)
	{
	$IsAllowedQuota		= false;
	$IsAllowedTimeLimit = false;

	// Check available send amount limit - STARTED
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_limits AS tblAdminLimits WHERE RelAdministratorID='".$AdministratorID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdminLimits = mysql_fetch_assoc($ResultSet);

	if ($ArrayAdminLimits['Limit'] != 'Unlimited')
		{
		include_once('./system/libraries/quota.inc.php');
		$QuotaUsed = CalculateSendLimits($AdministratorID, $ArrayAdminLimits, $ArrayConfig);
		$RemainingQuota = $ArrayAdminLimits['LimitAmount'] - $QuotaUsed;

		if ($RemainingQuota > 1)
			{
			$IsAllowedQuota = true;
			}
		}
	else
		{
		$IsAllowedQuota = true;
		}
	// Check available send amount limit - FINISHED

	// Check available send time limit - STARTED
	$TodayDate	= date('D');
	$TodayTime	= strtotime(date('Y-m-d H:i:s'));
	$AllowedDays = explode(',', $ArrayAdminLimits['TimeLimitDates']);

	if ($AllowedDays[0] != '')
		{
		foreach ($AllowedDays as $EachDay)
			{
			if ($TodayDate == $EachDay)
				{
				if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitTo']))))
					{
					$IsAllowedTimeLimit = true;
					break;
					}
				}
			}
		}
	else
		{
		$IsAllowedTimeLimit = true;
		}
	// Check available send time limit - FINISHED

	if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
		{
		return true;
		}
	else
		{
		return false;
		}
	}

//########################################################################################
// Integrates CMS parts into content body
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function IntegrateCMSPartsToContent($Content, $ArrayCMSParts)
	{
	$ArrayPattern	= array();
	$ArrayReplace	= array();

	foreach ($ArrayCMSParts as $Key => $ArrayValues)
		{
		$ArrayPattern[]	= "%INSERT:CMS:".$ArrayValues['Key'].":PLAIN%";
		$ArrayPattern[]	= "%INSERT:CMS:".$ArrayValues['Key'].":HTML%";

		$ArrayReplace[]	= $ArrayValues['Content'];
		$ArrayReplace[]	= $ArrayValues['Content'];
		}

	$Content = str_replace($ArrayPattern, $ArrayReplace, $Content);
	
	return $Content;
	}

//########################################################################################
// Retrieves the total amount of members from outsourced list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ReturnOutsourcedListMemberCount($MailListID, $OutsourceType, $ArrayLanguage, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED
	
	// Retrieve mail list information - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$MailListID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayMailList = mysql_fetch_assoc($ResultSet);
		}
	// Retrieve mail list information - FINISHED
	
	// Retrieve from MySQL outsourced list - STARTED
	if (($ErrorExists == false) && ($OutsourceType == 'MySQL'))
		{
		// Connect to outsourced mail list database - STARTED
		mysql_close();
		
		$MySQLLink = mysql_connect($ArrayMailList['OutSourceDBHost'], $ArrayMailList['OutSourceDBUsername'], $ArrayMailList['OutSourceDBPassword']);
			mysql_select_db($ArrayMailList['OutSourceDBName']);
		// Connect to outsourced mail list database - FINISHED

		// Retrieve the total amount of members - STARTED
			$SQLQueryPart = ($ArrayMailList['OutSourceDBWhereQuery'] != '' ? ' WHERE '.$ArrayMailList['OutSourceDBWhereQuery'] : '');
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayMailList['OutSourceDBTable'].$SQLQueryPart;
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalOutsourcedMembers = mysql_fetch_assoc($ResultSet);
			$TotalOutsourcedMembers = $TotalOutsourcedMembers['TotalFound'];
		// Retrieve the total amount of members - FINISHED
		
		// Connect to oemPro database - STARTED
		mysql_close();
	
			// Connect to oemPro MySQL server and database
			$MySQLLink = DatabaseConnect($ArrayConfig["Database"]);
			// Connect to outsourced MySQL list - FINISHED
		// Connect to oemPro database - FINISHED
		}
	// Retrieve from MySQL outsourced list - FINISHED

	// Retrieve from flat file outsourced list - STARTED
	elseif (($ErrorExists == false) && ($OutsourceType == 'FlatFile'))
		{
		}
	// Retrieve from flat file outsourced list - FINISHED

	return $TotalOutsourcedMembers;
	}

?>