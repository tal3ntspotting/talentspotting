<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Sends notification emails to mail list administrators
function SendNotificationEmail($ArrayMailList, $ArrayMember, $ArrayMembership, $Type, $ArrayLanguage, $ArrayConfig)
	{
	// Retrieve the mail list administrators and send them email - STARTED
	$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_administrators AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."administrators AS tblAdmin ";
	$SQLQuery .= "ON tblAssign.RelAdministratorID = tblAdmin.AdministratorID ";
	$SQLQuery .= "WHERE tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	// Define the subject and message -STARTED
	if ($Type == 'Subscription')
		{
		$Subject	=	stripslashes($ArrayLanguage["Texts"]["00367"]);
		$Message	=	$ArrayLanguage["Texts"]["00368"];
		}
	elseif ($Type == 'Unsubscription')
		{
		$Subject	=	stripslashes($ArrayLanguage["Texts"]["00369"]);
		$Message	=	$ArrayLanguage["Texts"]["00370"];
		}
	// Define the subject and message -FINISHED

	while ($EachAdministrator = mysql_fetch_assoc($ResultSet))
		{
		$ObjectMailer 			= new phpmailer();
	
		$ObjectMailer->IsMail();
	
		$ObjectMailer->From		= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->FromName	= $ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->AddAddress(strtolower($EachAdministrator['Email']), $EachAdministrator['Name']);
		$ObjectMailer->AddReplyTo($ArrayConfig['Settings']['SystemMailerEmail'], $ArrayConfig['Settings']['SystemMailerName']);
		$ObjectMailer->ReturnPath= $ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->Sender= $ArrayConfig['Settings']['SystemMailerEmail'];
		
		$ObjectMailer->WordWrap	= 70;
		$ObjectMailer->Priority	= 3;
		$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
		$ObjectMailer->CharSet	= $ArrayLanguage["Config"]["CharSet"];
		$ObjectMailer->Subject	= $Subject;
		
		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $Message;
	
		// Perform personalization - STARTED
			$ArrayPrefixes		=	array(
										"Sender",
										"Member",
										"Membership",
										"MailList",
										);
			$ArrayInformation	=	array(
										$EachAdministrator,
										$ArrayMember,
										$ArrayMembership,
										$ArrayMailList,
										);											
		$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
		$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
		// Perform personalization - FINISHED

		// Insert all member custom field information into the message - STARTED
		$AllMemberInfo = "";
		
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_customfields WHERE RelMailListID='".$ArrayMailList['MailListID']."' ORDER BY ListOrderNo ASC";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		while ($EachRow = mysql_fetch_assoc($ResultSet2))
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."customfields WHERE CustomFieldID='".$EachRow['RelCustomFieldID']."'";
			$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayCustomField = mysql_fetch_assoc($ResultSet3);
			
			$AllMemberInfo .= $ArrayCustomField['Title']." : ".$ArrayMember['CustomField'.$ArrayCustomField['CustomFieldID']]."\n";
			}
		
		$ObjectMailer->Body = str_replace("_InsertMemberInfo_", $AllMemberInfo, $ObjectMailer->Body);
		// Insert all member custom field information into the message - FINISHED
			
		if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
			{
			$ObjectMailer->IsMail();
			}
		else
			{
			include_once($ArrayConfig['Paths']['Software'].'/system/libraries/send_engines.inc.php');
			include_once($ArrayConfig['Paths']['Software']."/system/classes/class_smtp.php");
			
			$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
			
			if ($ArraySendEngine == false)
				{
				$ObjectMailer->IsMail();
				}
			else
				{
					$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
					$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
					$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
					$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
					$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
					$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
					$ObjectMailer->SMTPKeepAlive	=	false;
				$ObjectMailer->IsSMTP();
				}
			}

		$ObjectMailer->Send();
		}
	// Retrieve the mail list administrators and send them email - FINISHED
	}

?>