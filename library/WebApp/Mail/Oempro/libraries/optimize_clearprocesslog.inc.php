<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Clears the process log
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ClearProcessLog($ArrayConfig, $SetupValue1, $SetupValue2, $SetupValue3, $SetupValue4, $SetupValue5, $SetupValue6, $SetupValue7, $SetupValue8, $SetupValue9, $SetupValue10)
	{
	$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."process_log WHERE ProcessFinishTime < DATE_SUB(NOW(), INTERVAL ".$SetupValue1." DAY)";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;
	}

?>