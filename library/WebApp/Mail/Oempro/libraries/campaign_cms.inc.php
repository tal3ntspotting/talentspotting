<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Analyze the template content and return back the list of CMS parts
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function RetrieveCMSParts($Template)
	{
	// Initialize variables - STARTED
	$ArrayCMSParts		=	array();
	// Initialize variables - FINISHED
	
	// %INSERT:CMS:..........% - STARTED
		$Pattern = "/\%INSERT\:CMS\:(.*):(.*)\%/iU";
	if (preg_match_all($Pattern, $Template, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			// Define the field type - STARTED
			if ((strtolower($EachMatch[2]) != 'html') && (strtolower($EachMatch[2]) != 'plain'))
				{
				$FieldType = 'html';
				}
			else
				{
				$FieldType = strtolower($EachMatch[2]);
				}
			// Define the field type - FINISHED

			$ArrayCMSParts[] = array(
									'PartName'		=>	$EachMatch[1],
									'FieldType'		=>	$FieldType,
									);
			}
		}
	// %INSERT:CMS:..........% - FINISHED
		
	return $ArrayCMSParts;
	}
	
?>