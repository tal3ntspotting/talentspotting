<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Database Backup
function BackupDatabase($ArrayLanguage, $ArrayConfig, $BackupStructure = true, $BackupData = true, $GZip = false)
	{	
	$SQLBackupFileName 		= $ArrayConfig["Database"]["Database"]."_".date("Ymd_His").".sql";

	header("Content-type: application/octetstream");
	header("Content-Disposition: attachment; filename=\"".$SQLBackupFileName."\"");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Pragma: public");

	$ResultSet = mysql_list_tables($ArrayConfig["Database"]["Database"]);
		$TotalTables = mysql_num_rows($ResultSet);

	print('#-----------------------------------------'."\n");
	print('#| Database Backup                        |'."\n");
	print('#-----------------------------------------'."\n");
	print('#Backup Date: '.date('Y-m-d H:i:s')."\n");
	print("\n");

	for ($TMPCounter = 0; $TMPCounter < $TotalTables; $TMPCounter++)
		{
		$TableName = mysql_tablename($ResultSet, $TMPCounter);

		print('#-------------------------------------=[ '.$TableName."\n");
		print("\n");
		print("\n");
		
		if ($BackupStructure == true)
			{
			print(GetTableStructure($TableName, $ArrayLanguage, $ArrayConfig));
			}
		
		print("\n");
		print("\n");
		print("\n");

		if ($BackupData == true)
			{
			$SQLQuery = "SELECT * FROM ".$TableName;
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TotalRows = mysql_num_rows($ResultSet2);	
		
			while ($EachRow = mysql_fetch_assoc($ResultSet2))
				{
				$FieldList = '`'.implode('`,`', array_keys($EachRow)).'`';
		
				print("INSERT INTO `".$TableName."` (".$FieldList.") VALUES (");
		
				$Data = array();
				foreach (array_values($EachRow) as $EachValue)
					{
					if (isset($EachValue) == false)
						{
						print(' NULL, ');
						}
					elseif ($EachValue != '')
						{
						print("'".addslashes($EachValue)."',");
						}
					else
						{
						print(" '', ");
						}
					}

				print("); \n");
				}
			}

		print("\n");
		print("\n");
		print("\n");
		}

	exit;
	
	return true;
	}

//########################################################################################
// Retrieve table data
function GetTableRows($TableName, $ArrayLanguage, $ArrayConfig)
	{	
	$ArraySQLQueryLines		= array();

	$SQLQuery = "SELECT * FROM ".$TableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalRows = mysql_num_rows($ResultSet);	
		
	$TMPCounter = 0;
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$FieldList = '`'.implode('`,`', array_keys($EachRow)).'`';
		
		print("INSERT INTO `".$TableName."` (".$FieldList.") VALUES (");
		
		$Data = array();
		foreach (array_values($EachRow) as $EachValue)
			{
			if (isset($EachValue) == false)
				{
				$Data[] = ' NULL ';
				}
			elseif ($EachValue != '')
				{
				$Data[] = "'".addslashes($EachValue)."'";
				}
			else
				{
				$Data[] = " '' ";
				}
	
			}
		
		print(implode(', ', $Data)."); \n");
		}

	return;	
	}
//########################################################################################
// Retrieve table description
function GetTableStructure($TableName, $ArrayLanguage, $ArrayConfig)
	{	
	$ArraySQLQueryLines		= array();
	$ArraySQLQueryLines[]	= "DROP TABLE IF EXISTS `".$TableName."`;"."\n";
	$ArraySQLQueryLines[]	= "CREATE TABLE `".$TableName."` ("."\n";

	$SQLQuery = "SHOW FIELDS FROM ".$TableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFields = mysql_num_rows($ResultSet);	

	$TMPCounter = 0;
	while ($EachField = mysql_fetch_assoc($ResultSet))
		{
		$TMPCounter++;
		
		$ArraySQLQueryLines[]	= "`".$EachField['Field']."` ".$EachField['Type'];

        if(isset($EachField["Default"]) && (!empty($EachField["Default"]) || $EachField["Default"] == "0"))
			{
			$ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " DEFAULT '".$EachField["Default"]."'";
			}

        if($EachField["Null"] != "YES")
			{
            $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " NOT NULL";
			}

        if($EachField["Extra"] != "")
			{
            $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= " ".$EachField["Extra"];
			}

		if ($TotalFields > $TMPCounter)
			{
	        $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= ",\n";
			}
		else
			{
	        $ArraySQLQueryLines[count($ArraySQLQueryLines) - 1] .= "";
			}
		}

	$SQLQuery = "SHOW KEYS FROM ".$TableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFields = mysql_num_rows($ResultSet);	

	$TMPCounter = 0;

	$ArrayIndexList = array();
	
	while ($EachKey = mysql_fetch_assoc($ResultSet))
		{
		$EachIndexName = $EachKey['Key_name'];
       
		if(($EachIndexName != "PRIMARY") && ($EachIndexName['Non_unique'] == 0))
			{
            $EachIndexName="UNIQUE|".$EachIndexName;
			}

         if(isset($ArrayIndexList[$EachIndexName]) == false)
		 	{
			$ArrayIndexList[$EachIndexName] = array();
			}

         $ArrayIndexList[$EachIndexName][] = "`".$EachKey['Column_name']."`";
		}

	foreach ($ArrayIndexList as $Key=>$ArrayColumns)
		{
		$ArraySQLQueryLines[] = ","."\n";
		
		if ($Key == 'PRIMARY')
			{
			$ArraySQLQueryLines[] = "PRIMARY KEY (".implode(',', $ArrayColumns).")";
			}
		elseif (substr($Key, 0, 6) == 'UNIQUE')
			{
			$ArraySQLQueryLines[] = "UNIQUE ".substr($Key, 7)." (".implode(',', $ArrayColumns).")";
			}
		else
			{
			$ArraySQLQueryLines[] = "KEY ".$Key."` (".implode(',', $ArrayColumns).")";
			}
		}
		
	$ArraySQLQueryLines[] = "\n);";

	return implode("", $ArraySQLQueryLines);	
	}



?>