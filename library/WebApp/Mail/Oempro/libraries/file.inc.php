<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Remove the given directory, all files inside and sub directories
function RemoveDirectory($Path, $ArrayConfig)
	{
	// Check if the path is a directory or not - STARTED
	if (is_dir($Path) == false)
		{
		return false;
		}
	// Check if the path is a directory or not - FINISHED

	// Open the directory and start looping each file and directory - STARTED
	$DirHandler = opendir($Path);
	
	while (($EachFile = readdir($DirHandler)) !== false)
		{
		if (($EachFile != '.') && ($EachFile != '..') && (is_dir($Path.'/'.$EachFile) == false))
			{
			unlink($Path.'/'.$EachFile);
			}
		elseif (($EachFile != '.') && ($EachFile != '..') && (is_dir($Path.'/'.$EachFile) == true))
			{
			RemoveDirectory($Path.'/'.$EachFile, $ArrayConfig);
			rmdir($Path.'/'.$EachFile);
			}
		}
	
	closedir($DirHandler);
	// Open the directory and start looping each file and directory - FINISHED

	// Remove the main directory - STARTED
	rmdir($Path);
	// Remove the main directory - FINISHED
	
	
	return true;
	}

?>