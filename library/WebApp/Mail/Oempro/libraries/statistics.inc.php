<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/


//########################################################################################
// Returns the total soft bounced members of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListHardBounced($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	$SQLQuery .= "WHERE tblMembers.BounceType='Hard bounce' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total soft bounced members of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListSoftBounced($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	$SQLQuery .= "WHERE tblMembers.BounceType='Soft bounce' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total bounced members of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListBounced($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	$SQLQuery .= "WHERE tblMembers.BounceType!='Not bounced' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}

//########################################################################################
// Returns the total unsubscribed members of the mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListUnsubscribed($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Unsubscribed' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total opt-out pending members of the mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListOptOut($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Pending for opt-out confirmation' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total opt-in pending members of the mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListOptIn($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Pending for opt-in confirmation' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total active members (including outsourced members) of the list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListActive($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "LEFT OUTER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	$SQLQuery .= "WHERE (tblMembers.BounceType='Not bounced' OR tblMembers.BounceType='Soft bounce') ";
	$SQLQuery .= "AND tblAssign.SubscriptionStatus='Subscribed' AND tblAssign.RelMailListID='".$ArrayMailList['MailListID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	// Calculate the members of outsourced mail lists - STARTED
	$TotalMembers += FindTotalMemberOfOutsourcedList($ArrayMailList, $ArrayConfig, array());
	// Calculate the members of outsourced mail lists - FINISHED

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total members of the list (including outsourced members)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ListTotal($ArrayMailList, $ArrayConfig)
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$ArrayMailList['MailListID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	// Calculate the members of outsourced mail lists - STARTED
	$TotalMembers += FindTotalMemberOfOutsourcedList($ArrayMailList, $ArrayConfig, array());
	// Calculate the members of outsourced mail lists - FINISHED

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total opt-out pending members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalOptOutMembers($ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Pending for opt-out confirmation'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}

//########################################################################################
// Returns the total opt-in pending members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalOptInMembers($ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Pending for opt-in confirmation'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total unsubscribed members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalUnsubscribedMembers($ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblAssign.RelMemberID)) AS TotalFound FROM ";
	$SQLQuery .= $ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "WHERE tblAssign.SubscriptionStatus='Unsubscribed'";

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}

//########################################################################################
// Returns the total active members (including outsourced members)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalActiveMembers($ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "LEFT OUTER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
	$SQLQuery .= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	$SQLQuery .= "WHERE (tblMembers.BounceType='Not bounced' OR tblMembers.BounceType='Soft bounce') ";
	$SQLQuery .= "AND tblAssign.SubscriptionStatus='Subscribed'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	// Calculate the members of outsourced mail lists - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE SourceType='Outsourced - MySQL'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$TotalMembers += FindTotalMemberOfOutsourcedList($EachRow, $ArrayConfig, array());
		}
	// Calculate the members of outsourced mail lists - FINISHED

	return $TotalMembers;
	}

//########################################################################################
// Returns the total bounced members
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalBouncedMembers($ArrayConfig)
	{
	$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
	$SQLQuery .= "WHERE tblMembers.BounceType!='Not bounced'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalMembers = mysql_fetch_assoc($ResultSet);
		$TotalMembers = $TotalMembers['TotalFound'];

	return $TotalMembers;
	}
	
//########################################################################################
// Returns the total members (including outsourced members)
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function TotalMembers($ArrayAdministrator, $ArrayConfig)
	{
	$TotalMembers = 0;
	
	if ($ArrayAdministrator['AccountType'] != 'Super Administrator')
		{
		$SQLQuery  = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
		$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillist_administrators AS tblAssign ";
		$SQLQuery .= "ON tblMailLists.MailListID = tblAssign.RelMailListID ";
		$SQLQuery .= "WHERE tblAssign.RelAdministratorID = '".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachMailList = mysql_fetch_assoc($ResultSet))
			{
			$SQLQuery  = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."members AS tblMembers ";
			$SQLQuery .= "ON tblAssign.RelMemberID = tblMembers.MemberID ";
			$SQLQuery .= "WHERE tblAssign.RelMailListID='".$EachMailList['MailListID']."' AND tblAssign.SubscriptionStatus='Subscribed' AND tblMembers.BounceType!='Hard bounce'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			$TMPTotalMembers = mysql_fetch_assoc($ResultSet2);
				$TotalMembers += $TMPTotalMembers['TotalFound'];
		
			// Calculate the members of outsourced mail lists - STARTED
			if ($EachMailList['SourceType'] == 'Outsourced - MySQL')
				{
				$TotalMembers += FindTotalMemberOfOutsourcedList($EachMailList, $ArrayConfig, array());
				}
			// Calculate the members of outsourced mail lists - FINISHED
			}
		}
	else
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."members";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TMPTotalMembers = mysql_fetch_assoc($ResultSet);
			$TotalMembers += $TMPTotalMembers['TotalFound'];
	
		// Calculate the members of outsourced mail lists - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE SourceType='Outsourced - MySQL'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			$TotalMembers += FindTotalMemberOfOutsourcedList($EachRow, $ArrayConfig, array());
			}
		// Calculate the members of outsourced mail lists - FINISHED
		}

	return $TotalMembers;
	}


?>