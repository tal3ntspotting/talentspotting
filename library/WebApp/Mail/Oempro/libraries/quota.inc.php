<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Calculate the available quota for the user
function CalculateSendLimits($AdministratorID, $ArrayAdministratorLimits, $ArrayConfig)
	{
	if ($ArrayAdministratorLimits['Limit'] == 'Hourly')
		{
		$SQLQuery = "SELECT SendAmount AS UsedAmount FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE ActivityHour='".date('h')."' AND ActivityDay='".date('d')."' AND ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."' AND RelAdministratorID='".$AdministratorID."'";
		}
	elseif ($ArrayAdministratorLimits['Limit'] == 'Daily')
		{
		$SQLQuery = "SELECT SUM(SendAmount) AS UsedAmount FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE ActivityDay='".date('d')."' AND ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."' AND RelAdministratorID='".$AdministratorID."'";
		}
	elseif ($ArrayAdministratorLimits['Limit'] == 'Weekly')
		{
		$SQLQuery = "SELECT SUM(SendAmount) AS UsedAmount FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."' AND RelAdministratorID='".$AdministratorID."'";
		}
	elseif ($ArrayAdministratorLimits['Limit'] == 'Monthly')
		{
		$SQLQuery = "SELECT SUM(SendAmount) AS UsedAmount FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."' AND RelAdministratorID='".$AdministratorID."'";
		}
	elseif ($ArrayAdministratorLimits['Limit'] == 'Yearly')
		{
		$SQLQuery = "SELECT SUM(SendAmount) AS UsedAmount FROM ".$ArrayConfig['Database']['Prefix']."administrator_activities WHERE ActivityYear='".date('Y')."' AND RelAdministratorID='".$AdministratorID."'";
		}

	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$UsedAmount = mysql_fetch_assoc($ResultSet);
		$UsedAmount = $UsedAmount['UsedAmount'];

	return $UsedAmount;
	}
	
?>