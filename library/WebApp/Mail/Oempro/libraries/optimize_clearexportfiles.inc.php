<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Clear followups_tracks table
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ClearExportFiles($ArrayConfig, $SetupValue1, $SetupValue2, $SetupValue3, $SetupValue4, $SetupValue5, $SetupValue6, $SetupValue7, $SetupValue8, $SetupValue9, $SetupValue10)
	{
	// Initialize variables - STARTED
	$FilterDateTime = date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -".$SetupValue1." days ".date("H:i:s")));
	// Initialize variables - FINISHED

	// Check each file in export data directory if they are out-of-date - STARTED
	$DirHandler = opendir($ArrayConfig['Paths']['Data']."/export");
	
	while ($EachFile = readdir($DirHandler))
		{
		if (($EachFile != '.') && ($EachFile != '..'))
			{
			$FileDate = explode("_", $EachFile);
			
			if ($FileDate[count($FileDate) - 1] != '')
				{
				$FileDate = strtotime(date("Y-m-d H:i:s", $FileDate[count($FileDate) - 1]));

				if (($FileDate < strtotime($FilterDateTime)) && ($FileDate != -1) && ($FileDate != 0))
					{
					unlink($ArrayConfig['Paths']['Data']."/export/".$EachFile);
					}
				}
			}
		}
	
	closedir($DirHandler);
	// Check each file in export data directory if they are out-of-date - FINISHED
	
	return;	
	}

?>