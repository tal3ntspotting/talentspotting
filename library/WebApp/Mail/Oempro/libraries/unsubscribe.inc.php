<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Process mail list unsubscription settings
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ProcessMailListUnSubSettings($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayConfig, $ArrayLanguage, $PathOfSystem)
	{
	if ($ArrayMailList['UnSubscriptionSettings'] == 'NA')
		{
		return;
		}
	elseif ($ArrayMailList['UnSubscriptionSettings'] == 'Delete')
		{
			include_once($PathOfSystem."/libraries/delete.inc.php");
		DeleteMember($ArrayMember['MemberID'], $ArrayConfig);
		}
	elseif (($ArrayMailList['UnSubscriptionSettings'] == 'Copy') && ($ArrayMailList['RelUnSubSettingsCopyMailListID'] > 0))
		{
			include_once($PathOfSystem."/libraries/subscribe.inc.php");
			$Email 				= $ArrayMember['Email'];
			$ArrayMailListIDs	= array($ArrayMailList['RelUnSubSettingsCopyMailListID']);
		SubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
		}
	elseif (($ArrayMailList['UnSubscriptionSettings'] == 'UnSubscribeFrom') && ($ArrayMailList['RelUnSubSettingsUnSubscribeMailListID'] > 0))
		{
			include_once($PathOfSystem."/libraries/unsubscribe.inc.php");
			$Email 				= $ArrayMember['Email'];
			$ArrayMailListIDs	= array($ArrayMailList['RelUnSubSettingsUnSubscribeMailListID']);
		UnSubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
		}

	return;
	}
	
//########################################################################################
// Unsubscribe member from the assigned mail lists
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function UnSubscribeMember($Email, $ArrayMailListIDs, $ArrayConfig, $ArrayLanguage, $PathOfSystem)
	{
	// Initialize variables - STARTED
	$ErrorExists		=	false;
	$ErrorMessage		=	"";
	$ArrayInformation	=	array(
								"FormValue_Email"		=>	$Email,
								);
	// Initialize variables - FINISHED

	// Check for required fields - STARTED
	if ($ErrorExists == false)
		{
		$ArrayRequiredFields = array(
									'Email'						=>		$ArrayLanguage["Texts"]["00016"],
									);

		// Load the required module
		include_once($PathOfSystem."/classes/class_octethreqfldchecker.php");
		
		// Initialize the object
		$ObjectReqFieldChecker = new OctethReqFieldChecker($ArrayRequiredFields, $ArrayErrorMessages, $ArrayInformation, 'FormValue_');
			$ArrayErrorMessages = $ObjectReqFieldChecker->CheckForRequiredFields();
		if (count($ArrayErrorMessages) > 0)
			{
			$ErrorExists = true;
			$ErrorMessage = $ArrayLanguage["Texts"]["00314"];
			}
		}
	// Check for required fields - FINISHED

	// Check for unique fields - STARTED
	if ($ErrorExists == false)
		{
		}
	// Check for unique fields - FINISHED

	// Field validations - STARTED
	if ($ErrorExists == false)
		{
		// Include the required module - STARTED
		include_once($PathOfSystem."/libraries/membership.inc.php");
		// Include the required module - FINISHED

		// Check email address - STARTED
		if ($ErrorExists == false)
			{
			if ($Email != '')
				{
				// Load the required module - STARTED
				include_once($PathOfSystem."/classes/class_utilities1.php");
				// Load the required module - FINISHED
		
				$ObjectUtilities = new OctethUtilities();
		
				if ($ObjectUtilities->CheckEmailFormat(strtolower($Email)) == false)
					{
					$ErrorExists = true;
					$ErrorMessage = $ArrayLanguage["Texts"]["00319"];
					}
				}
			}
		// Check email address - FINISHED

		// Check if email address exists or not - STARTED
		if ($ErrorExists == false)
			{
			// Check if email address exists or not - STARTED
			if ($ErrorExists == false)
				{
				$EmailExists = CheckEmailIfExists($Email, $ArrayConfig, '');
							
				if ($EmailExists == false)
					{
					$ErrorExists = true;
					$ErrorMessage = $ArrayLanguage["Texts"]["00320"];
					}
				}
			// Check if email address exists or not - FINISHED
			}
		// Check if email address exists or not - FINISHED
		}
	// Field validations - FINISHED

	// Unsubscribe member from selected mail lists - STARTED
	if ($ErrorExists == false)
		{
		// Retrieve member information - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE Email='".$Email."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayMember = mysql_fetch_assoc($ResultSet);
		// Retrieve member information - FINISHED

		// If mail lists have not been defined in the POST, assume that member wants to unsubscribe from all mail lists he is subscribed - STARTED
		if (count($ArrayMailListIDs) < 1)
			{
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMailListIDs = array();
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$ArrayMailListIDs[] = $EachRow['RelMailListID'];
				}
			}
		// If mail lists have not been defined in the POST, assume that member wants to unsubscribe from all mail lists he is subscribed - FINISHED

		$TotalUnsubscribedMailLists = 0;

		foreach ($ArrayMailListIDs as $EachMailListID)
			{
			// Retrieve mail list and member information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachMailListID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMailList = mysql_fetch_assoc($ResultSet);

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE RelMailListID='".$EachMailListID."' AND RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayMembership = mysql_fetch_assoc($ResultSet);
			// Retrieve mail list and member information - FINISHED

			// Check if already unsubscribed from this mail list - STARTED
			if ($ArrayMembership['SubscriptionStatus'] != 'Subscribed')
				{
				continue;
				}
			// Check if already unsubscribed from this mail list - FINISHED

			$TotalUnsubscribedMailLists++;

			// Unsubscribe from the mail list - STARTED
				$OptOutConfirmed		=	($ArrayMailList['UnsubscriptionType'] == 'Opt-out' ? 'No' : 'Yes');
				$SubscriptionStatus		=	($ArrayMailList['UnsubscriptionType'] == 'Opt-out' ? 'Pending for opt-out confirmation' : 'Unsubscribed');
				$UnsubscriptionDate		=	date('Y-m-d H:i:s');
				$OptOutConfirmDate		=	($ArrayMailList['UnsubscriptionType'] == 'Opt-out' ? '0000-00-00 00:00:00' : date('Y-m-d H:i:s'));	
				$IPOnUnSubscription		=	'0.0.0.0';
			$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."maillist_members SET OptOutConfirmed='".$OptOutConfirmed."', SubscriptionStatus='".$SubscriptionStatus."', UnsubscriptionDate='".$UnsubscriptionDate."', OptOutConfirmDate='".$OptOutConfirmDate."', IPOnUnSubscription='".$IPOnUnSubscription."' WHERE RelMailListID='".$EachMailListID."' AND RelMemberID='".$ArrayMember['MemberID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			// Unsubscribe from the mail list - FINISHED

			// If enabled for this mail list, black list the member - STARTED
			if ($ArrayMailList['BlackListOnUnsubscription'] == 'Yes')
				{
				$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$ArrayMember['Email']."' AND RelMailListID='".$ArrayMailList['MailListID']."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$TotalFound = mysql_fetch_assoc($ResultSet);
					$TotalFound = $TotalFound['TotalFound'];

				if ($TotalFound < 1)
					{					
					$ArrayFieldnValues = array(
												"BlackListID"		=>	'',
												"RelMailListID"		=>	$ArrayMailList['MailListID'],
												"Email"				=>	$ArrayMember['Email'],
												"DateSubmitted"		=>	date('Y-m-d H:i:s'),
												);
			
					$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
						$FieldList = "`".$FieldList."`";
					$ValueList = implode("','", array_values($ArrayFieldnValues));
						$ValueList = "'".$ValueList."'";
					
					$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."maillist_blacklist (".$FieldList.") VALUES (".$ValueList.")";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);				
					}
				}
			// If enabled for this mail list, black list the member - FINISHED

			// If opt-out confirmation is disabled, remove this member from all auto responders that are scheduled to be sent - STARTED
			if ($ArrayMailList['UnsubscriptionType'] != 'Opt-out')
				{
					include_once('./system/libraries/autoresponder.inc.php');
				UnRegisterFromAutoResponders($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayLanguage, $ArrayConfig);
				}
			// If opt-out confirmation is disabled, remove this member from all auto responders that are scheduled to be sent - FINISHED

			// If enabled for this mail list unsubscription, send the opt-out confirmation email to the member - STARTED
			if ($ArrayMailList['UnsubscriptionType'] == 'Opt-out')
				{
				// Load required module - STARTED
				include_once($PathOfSystem."/classes/class_mailer.php");
				include_once($PathOfSystem."/libraries/membership.inc.php");
				include_once($PathOfSystem."/libraries/personalization.inc.php");
				// Load required module - FINISHED

				// Retrieve all opt-out request emails of the mail lists - STARTED
				$SQLQueryPart = array();
					$SQLQueryPart[] = "tblAssign.RelMailListID='".$EachMailListID."'";
		
				$SQLQueryPart = implode(" OR ", $SQLQueryPart);
		
				$SQLQuery  = "SELECT * ";
				$SQLQuery .= "FROM ".$ArrayConfig['Database']['Prefix']."maillist_messages AS tblAssign ";
				$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."emails AS tblEmails ";
				$SQLQuery .= "ON tblAssign.RelEmailID = tblEmails.EmailID ";
				$SQLQuery .= "INNER JOIN ".$ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
				$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
				$SQLQuery .= "WHERE (".$SQLQueryPart.") AND tblAssign.UseFor='OptOutConfirmationRequest'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				// Retrieve all opt-out request emails of the mail lists - FINISHED

				while ($EachRow = mysql_fetch_assoc($ResultSet))
					{
					$ObjectMailer 			= new phpmailer();
			
					$ObjectMailer->IsMail();
		
					$ObjectMailer->From		= $EachRow['FromEmail'];
					$ObjectMailer->FromName	= $EachRow['FromName'];
					$ObjectMailer->AddAddress(strtolower($ArrayMember['Email']), '');
					$ObjectMailer->AddReplyTo($EachRow['ReplyToEmail'], $EachRow['ReplyToName']);
					$ObjectMailer->ReturnPath= $EachRow['ReturnPathEmail'];
					$ObjectMailer->Sender= $EachRow['ReturnPathEmail'];
					
					$ObjectMailer->WordWrap	= 70;
					$ObjectMailer->Priority	= $EachRow['Priority'];
					$ObjectMailer->XMailer	= $ArrayConfig['Settings']['XMailer'];
					$ObjectMailer->CharSet	= substr($EachRow["CharSet"], 0, strlen($EachRow["CharSet"]) - 5);
					$ObjectMailer->Subject	= stripslashes($EachRow['Subject']);
		
					if (($EachRow['ContentType'] == 'Html') || ($EachRow['ContentType'] == 'Both'))
						{
						$ObjectMailer->IsHTML(true);
						$ObjectMailer->Body		= $EachRow['Content'];
						$ObjectMailer->AltBody	= $EachRow['ContentAlternate'];
						}
					elseif ($EachRow['ContentType'] == 'Plain')
						{
						$ObjectMailer->IsHTML(false);
						$ObjectMailer->Body		= $EachRow['Content'];
						}

					// Parse the confirmation link - STARTED
					$EncryptedMailListMembershipID = Encrypt($ArrayMembership['MembershipID'], $ArrayConfig['Settings']['EncryptionKey']);
					
							$ArrayReplaceList = array(
													"%Link%"			=>	$ArrayConfig['URLs']['Software']."/optout_confirm.php?CID=".$EncryptedMailListMembershipID."&MailListID=".$ArrayMailList['MailListID'],
													);
						$ObjectMailer->Body	=	str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
					// Parse the confirmation link - FINISHED

					// Perform personalization - STARTED
						$ArrayPrefixes		=	array(
													"Member",
													"Membership",
													"MailList",
													);
						$ArrayInformation	=	array(
													$ArrayMember,
													$ArrayMembership,
													$ArrayMailList,
													);											
					$ObjectMailer->Subject	=	Personalize($ObjectMailer->Subject, $ArrayPrefixes, $ArrayInformation, '%');
					$ObjectMailer->Body		=	Personalize($ObjectMailer->Body, $ArrayPrefixes, $ArrayInformation, '%');
					$ObjectMailer->AltBody	=	Personalize($ObjectMailer->AltBody, $ArrayPrefixes, $ArrayInformation, '%');
					// Perform personalization - FINISHED
			
					if ($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'] == 0)
						{
						$ObjectMailer->IsMail();
						}
					else
						{
						include_once($PathOfSystem.'/system/libraries/send_engines.inc.php');
						include_once($PathOfSystem."/system/classes/class_smtp.php");
						
						$ArraySendEngine = RetrieveSendEngine($ArrayConfig['Settings']['DefaultSystemEmailSendMethod'], $ArrayConfig);
						
						if ($ArraySendEngine == false)
							{
							$ObjectMailer->IsMail();
							}
						else
							{
								$ObjectMailer->Host				=	$ArraySendEngine['ServerHost'];
								$ObjectMailer->Port				=	$ArraySendEngine['ServerPort'];
								$ObjectMailer->SMTPAuth			=	($ArraySendEngine['ServerAuth'] == 'Authorization' ? true : false);
								$ObjectMailer->Username			=	$ArraySendEngine['ServerUsername'];
								$ObjectMailer->Password			=	$ArraySendEngine['ServerPassword'];
								$ObjectMailer->Timeout			=	$ArraySendEngine['ServerTimeoutSeconds'];
								$ObjectMailer->SMTPKeepAlive	=	false;
							$ObjectMailer->IsSMTP();
							}
						}

					$ObjectMailer->Send();
					}
				}
			// If enabled for this mail list unsubscription, send the opt-out confirmation email to the member - FINISHED

			// If opt-out confirmation is not enabled for this mail list, process unsubscription settings - STARTED
			if ($ArrayMailList['UnsubscriptionType'] == 'Free')
				{
				ProcessMailListUnSubSettings($ArrayMember, $ArrayMembership, $ArrayMailList, $ArrayConfig, $ArrayLanguage, $PathOfSystem);
				}
			// If opt-out confirmation is not enabled for this mail list, process unsubscription settings - FINISHED

			// Update mail list activity statistics - STARTED
			if (($ArrayMailList['UnsubscriptionType'] != 'Opt-out') && ($ErrorExists == false))
				{
				$ArrayFieldnValues = array(
										"ActivityID"		=> '',
										"RelMemberID"		=> $ArrayMember['MemberID'],
										"RelMailListID"		=> $ArrayMailList['MailListID'],
										"Activity"			=> 'Unsubscription',
										"ActivityDateTime"	=> date('Y-m-d H:i:s'),
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList ="'".$ValueList."'";
		
				$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members_activities_detailed (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSetActivityDetails = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				}
			// Update mail list activity statistics - FINISHED
			}
		}
	// Unsubscribe member from selected mail lists - FINISHED

	// If no unsubscriptions have been processed, display error message - STARTED
	if (($ErrorExists == false) && ($TotalUnsubscribedMailLists < 1))
		{
		$ErrorExists = true;
		$ArrayErrorMessages['PageMessage'] = $ArrayLanguage["Texts"]["00321"];
		}
	// If no unsubscriptions have been processed, display error message - FINISHED

	// Update activity statistics - STARTED
	if ($ErrorExists == false)
		{
		$ArrayFieldnValues = array(
								"ActivityID"		=>	'',
								"RelMemberID"		=>	$ArrayMember['MemberID'],
								"Activity"			=>	'Unsubscription',
								"ActivityDateTime"	=>	date("Y-m-d H:i:s"),
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."members_activities (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	// Update activity statistics - FINISHED

	return array($ErrorExists, $ErrorMessage);
	}

?>