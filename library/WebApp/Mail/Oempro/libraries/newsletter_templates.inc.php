<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// 
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DownloadObjects($Content, $ArrayConfig, $ArrayLanguage, $PathToSave, $URLToSave)
	{	
	$ArrayObjects = array();
	
	// src= - STARTED
		$Pattern = "/src\=(\"|\'|)(.*)(\"|\'|\s)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// src= - FINISHED

	// background-image:url( - STARTED
		$Pattern = "/background\-image\:url\((.*)\)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayImages[] = $EachMatch[1];
			}
		}
	// background-image:url( - FINISHED

	// background=" - STARTED
		$Pattern = "/background\=(\"|\'|)(.*)(\"|\'|\s)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - FINISHED

	// <link href=" - STARTED
		$Pattern = "/\<link href\=(\"|\'|)(.*)(\"|\'|\s)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - FINISHED

	$ArrayObjects = array_flip(array_flip($ArrayObjects));

	foreach ($ArrayObjects as $EachObject)
		{
		$ArrayReplacePattern = array();
		$ArrayReplaceReplace = array();

			$ObjectFileName = basename($EachObject);
		$SaveAsPath = $PathToSave."/".$ObjectFileName;
		$SaveAsURL	= $URLToSave."/".$ObjectFileName;

		// Save file to the local folder - STARTED
		copy($EachObject, $SaveAsPath);
		// Save file to the local folder - FINISHED

		$ArrayReplacePattern[]	=	'src="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'background-image:url('.$EachObject.');';
		$ArrayReplacePattern[]	=	'background="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'<link href="'.$EachObject.'"';
	
		$ArrayReplaceReplace[]	=	'src="'.$SaveAsURL.'"';
		$ArrayReplaceReplace[]	=	'background-image:url('.$SaveAsURL.');';
		$ArrayReplaceReplace[]	=	'background="'.$SaveAsURL.'"';
		$ArrayReplaceReplace[]	=	'<link href="'.$SaveAsURL.'"';

		$Content = str_replace($ArrayReplacePattern, $ArrayReplaceReplace, $Content);
		}

	return $Content;
	}
	
?>