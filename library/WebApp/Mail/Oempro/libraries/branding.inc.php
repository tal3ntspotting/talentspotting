<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Apply Branding
function ApplyBranding($ObjectTemplate, $ArrayConfig)
	{
	$ArrayReplaceList = array(
							"_Branding:ProductName_"	=>	$ArrayConfig['Branding']['ProductName'],
							"_Branding:ProductLogoURL_"	=>	$ArrayConfig['Branding']['ProductLogoURL'],
							);
	$ObjectTemplate->replace($ArrayReplaceList);

	if ($ArrayConfig['Branding']['ShowCopyright'] == 'No')
		{
		$ObjectTemplate->remove_block('BRANDING:ShowCopyright');
		}

	if ($ArrayConfig['Branding']['ShowOctethLogo'] == 'No')
		{
		$ObjectTemplate->remove_block('BRANDING:ShowOctethLogo');
		}
	else
		{
		$ObjectTemplate->remove_block('BRANDING:ShowOctethLogo:Off');
		}

	if ($ArrayConfig['Branding']['ShowProductVersion'] == 'No')
		{
		$ObjectTemplate->remove_block('BRANDING:ShowProductVersion');
		}

	if ($ArrayConfig['Branding']['ShowLicenseAndSupportLinks'] == 'No')
		{
		$ObjectTemplate->remove_block('BRANDING:ShowLicenseAndSupportLinks');
		}

	if ($ArrayConfig['Branding']['ProductLogoURL'] == '')
		{
		$ObjectTemplate->remove_block('BRANDING:CustomProductLogo');
		}
	else
		{
		$ObjectTemplate->remove_block('BRANDING:DefaultProductLogo');
		}

	return $ObjectTemplate;
	}

?>