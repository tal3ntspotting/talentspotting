<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Optimizes the system according to defined processes
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function OptimizeSystem($ArrayConfig)
	{
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."periodicprocesses WHERE LastRunDateTime = '0000-00-00 00:00:00' OR NOW()>DATE_ADD(LastRunDateTime, INTERVAL IntervalMinutesToCheck MINUTE)";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$ModuleName			= $EachRow['Type'];
		$ModuleFileName		= strtolower($ModuleName);

		// Update last run date - STARTED
		$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."periodicprocesses SET LastRunDateTime='".date("Y-m-d H:i:s")."' WHERE ProcessID='".$EachRow['ProcessID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Update last run date - FINISHED

		// Include this module - STARTED
		include_once("./system/libraries/optimize_".$ModuleFileName.".inc.php");
		call_user_func_array($ModuleName, array($ArrayConfig, $EachRow['SetupValue1'], $EachRow['SetupValue2'], $EachRow['SetupValue3'], $EachRow['SetupValue4'], $EachRow['SetupValue5'], $EachRow['SetupValue6'], $EachRow['SetupValue7'], $EachRow['SetupValue8'], $EachRow['SetupValue9'], $EachRow['SetupValue10']));		
		// Include this module - FINISHED
		}
	}

?>