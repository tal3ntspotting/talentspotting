<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Check if logged administrator exceeded limits on the action
function CheckIfLimitIsExceeded($AdministratorID, $Section, $BaseSectionID, $ArrayLanguage, $ArrayConfig)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED
	
	// Retrieve administrator information - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$AdministratorID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayAdministrator = mysql_fetch_assoc($ResultSet);
	// Retrieve administrator information - FINISHED
	
	// Retrieve administrator limits - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_limits WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayLimits = mysql_fetch_assoc($ResultSet);
	// Retrieve administrator limits - FINISHED

	// Check if administrator has available limit for this section - STARTED
	if (($Section == 'ResponderPerFollowUp') && ($ArrayLimits['AllowedCampaigns'] > 0))
		{
		// Find the total auto responders assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."followups WHERE RelFollowUpGroupID='".$BaseSectionID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total auto responders assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['ResponderPerFollowUp'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedCampaigns') && ($ArrayLimits['AllowedCampaigns'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."campaigns WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedCampaigns'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedCustomFields') && ($ArrayLimits['AllowedCustomFields'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."customfields WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedCustomFields'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedRSSFeeds') && ($ArrayLimits['AllowedRSSFeeds'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."archive_rss WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedRSSFeeds'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedArchives') && ($ArrayLimits['AllowedArchives'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."archive_list WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedArchives'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedTemplates') && ($ArrayLimits['AllowedTemplates'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."newsletter_templates WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedTemplates'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedClients') && ($ArrayLimits['AllowedClients'] > 0))
		{
		// Find the total campaigns assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."clients WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total campaigns assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedClients'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'FollowUpPerList') && ($ArrayLimits['FollowUpPerList'] > 0))
		{
		// Find the total mail lists assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_followups WHERE RelMailListID='".$BaseSectionID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total mail lists assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['FollowUpPerList'])
			{
			$ErrorExists = true;
			}
		}
	elseif (($Section == 'AllowedMailLists') && ($ArrayLimits['AllowedMailLists'] > 0))
		{
		// Find the total mail lists assigned to this administrator - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_administrators WHERE RelAdministratorID='".$ArrayAdministrator['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		// Find the total mail lists assigned to this administrator - FINISHED

		if ($TotalFound >= $ArrayLimits['AllowedMailLists'])
			{
			$ErrorExists = true;
			}
		}
	// Check if administrator has available limit for this section - FINISHED
	
	return $ErrorExists;	
	}

?>