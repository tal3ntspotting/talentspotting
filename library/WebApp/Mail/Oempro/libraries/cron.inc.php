<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Updates process log
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ChecknUpdateCronStatusSendEngine($CronFileName, $ArrayConfig, $ArrayLanguage)
	{
	$CronModuleName = str_replace(".php", "", $CronFileName);
	
	// Check if this CRON module is enabled - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."cron WHERE CronFileName='".$CronFileName."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TMPArrayCron = mysql_fetch_assoc($ResultSet);
	
	if ($TMPArrayCron['Status'] == 'Disabled')
		{
		return array(false, $ArrayLanguage["Texts"]["00301"]);
		}
	// Check if this CRON module is enabled - FINISHED
	
	$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."cron SET LastRun='".date("Y-m-d H:i:s")."' WHERE CronFileName='".$CronFileName."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return array(true, '');
	}

//########################################################################################
// Updates process log
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ChecknUpdateCronStatus($CronFileName, $ArrayConfig, $ArrayLanguage)
	{
	$CronModuleName = str_replace(".php", "", $CronFileName);
	
	// Check if this CRON module is enabled - STARTED
	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."cron WHERE CronFileName='".$CronFileName."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TMPArrayCron = mysql_fetch_assoc($ResultSet);
	
	if ($TMPArrayCron['Status'] == 'Disabled')
		{
		return array(false, $ArrayLanguage["Texts"]["00301"]);
		}
	// Check if this CRON module is enabled - FINISHED
	
	$SQLQuery = "UPDATE ".$ArrayConfig['Database']['Prefix']."cron SET LastRun='".date("Y-m-d H:i:s")."' WHERE CronFileName='".$CronFileName."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	// Check if another clone of this module is running or not - STARTED
	$MaximumDuration = date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -30 minutes ".date("H:i:s")));

	$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."process_log WHERE Module='".$CronModuleName."' AND ProcessStatus='Started' AND ProcessStartTime>='".$MaximumDuration."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	if (mysql_num_rows($ResultSet) > 0)
		{
		return array(false, $ArrayLanguage["Texts"]["00501"]);
		}
	// Check if another clone of this module is running or not - FINISHED

	return array(true, '');
	}

//########################################################################################
// Updates process log
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function UpdateProcessLog($LogID, $Module, $StartTime, $FinishTime, $ProcessStatus, $Extra1, $Extra2, $Extra3, $ArrayConfig)
	{
	if ($LogID == '')
		{
		$ArrayFieldnValues = array(
								"LogID"				=>	'',
								"Module"			=>	$Module,
								"ProcessStartTime"	=>	$StartTime,
								"ProcessFinishTime"	=>	$FinishTime,
								"ProcessStatus"		=>	$ProcessStatus,
								"Extra1"			=>	$Extra1,
								"Extra2"			=>	$Extra2,
								"Extra3"			=>	$Extra3,
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "INSERT INTO ".$ArrayConfig['Database']['Prefix']."process_log (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$LogID = mysql_insert_id();

		return $LogID;
		}
	else
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."process_log WHERE LogID='".$LogID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayLog = mysql_fetch_assoc($ResultSet);
		
		$ArrayFieldnValues = array(
								"LogID"				=>	$LogID,
								"Module"			=>	($Module == '' ? $ArrayLog['Module'] : $Module),
								"ProcessStartTime"	=>	($StartTime == '' ? $ArrayLog['ProcessStartTime'] : $StartTime),
								"ProcessFinishTime"	=>	($FinishTime == '' ? $ArrayLog['ProcessFinishTime'] : $FinishTime),
								"ProcessStatus"		=>	($ProcessStatus == '' ? $ArrayLog['ProcessStatus'] : $ProcessStatus),
								"Extra1"			=>	($Extra1 == '' ? $ArrayLog['Extra1'] : $Extra1),
								"Extra2"			=>	($Extra2 == '' ? $ArrayLog['Extra2'] : $Extra2),
								"Extra3"			=>	($Extra3 == '' ? $ArrayLog['Extra3'] : $Extra3),
								);

		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";

		$SQLQuery = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."process_log (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		return $LogID;
		}
	}

?>