<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Check opt-in/out confirmation pending members and purge expired records
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function PurgeOptInOutPending($ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED

	// Retrieve the list of all mail lists which are set for auto-purging - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE (SubscriptionType='Opt-in' AND AutoPurgeNotOptedInHours>'0') OR (UnsubscriptionType='Opt-out' AND AutoPurgeNotOptedOutHours>'0')";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// purge it's expired opt-in and outs - STARTED
			$SQLQueryPart = array();
			if (($EachRow['SubscriptionType'] == 'Opt-in') && ($EachRow['AutoPurgeNotOptedInHours'] > 0))
				{
				$OptInTargetDate = date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -".$EachRow['AutoPurgeNotOptedInHours']." days ".date("H:i:s")));
				$SQLQueryPart[] = "(SubscriptionStatus='Pending for opt-in confirmation' AND SubscriptionDate<='".$OptInTargetDate."')";
				}
			if (($EachRow['UnsubscriptionType'] == 'Opt-out') && ($EachRow['AutoPurgeNotOptedOutHours'] > 0))
				{
				$OptOutTargetDate = date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -".$EachRow['AutoPurgeNotOptedOutHours']." days ".date("H:i:s")));
				$SQLQueryPart[] = "(SubscriptionStatus='Pending for opt-out confirmation' AND UnsubscriptionDate<='".$OptOutTargetDate."')";
				}
			
			$SQLQueryPart = implode(" OR ", $SQLQueryPart);

			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE ".$SQLQueryPart;
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TotalPurgedMembers = mysql_num_rows($ResultSet2);		
			
			while ($EachRow = mysql_fetch_assoc($ResultSet2))
				{
				// Remove member subscription record - STARTED
				$SQLQuery = "DELETE FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE MembershipID='".$EachRow['MembershipID']."'";
				$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				// Remove member subscription record - FINISHED
				}			
			// purge it's expired opt-in and outs - FINISHED
			}
		}
	// Retrieve the list of all mail lists which are set for auto-purging - FINISHED

	return $TotalPurgedMembers;
	}


?>