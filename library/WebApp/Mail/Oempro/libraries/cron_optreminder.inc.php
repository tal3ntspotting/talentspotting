<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/

//########################################################################################
// Check opt-in/out confirmation pending members and send them reminder emails
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckOptInOutPending($ArrayConfig, $ArrayLanguage)
	{
	// Initialize variables - STARTED
	$ErrorExists = false;
	// Initialize variables - FINISHED

	// Retrieve all opt-in/out confirmation pending memberships - STARTED
	if ($ErrorExists == false)
		{
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_members WHERE SubscriptionStatus='Pending for opt-in confirmation' OR SubscriptionStatus='Pending for opt-out confirmation' ORDER BY RelMailListID ASC";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$TotalEmailsSent		=	0;

		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{
			// Retrieve member information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$EachRow['RelMemberID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMember = mysql_fetch_assoc($ResultSet2);
			// Retrieve member information - FINISHED
			
			// Retrieve the mail list information - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachRow['RelMailListID']."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$ArrayMailList = mysql_fetch_assoc($ResultSet2);
			// Retrieve the mail list information - FINISHED

			// Retrieve the mail list opt-in/out reminder emails - STARTED
				$UseFor = ($EachRow['SubscriptionStatus'] == 'Pending for opt-in confirmation' ? 'OptInNotConfirmed' : 'OptOutNotConfirmed');
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."maillist_messages WHERE RelMailListID='".$EachRow['RelMailListID']."' AND UseFor='".$UseFor."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			$ArrayOptInEmail	= array();
			$ArrayOptOutEmail	= array();
			$SecondsToSend		= 0;

			while ($EachEmails = mysql_fetch_assoc($ResultSet2))
				{
				if ($EachEmails['UseFor'] == 'OptInNotConfirmed')
					{
					$SecondsToSend = $EachEmails['Extra1'] * 3600;

					$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."emails WHERE EmailID='".$EachEmails['RelEmailID']."'";
					$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$ArrayOptInEmail = mysql_fetch_assoc($ResultSet3);
					}
				elseif ($EachEmails['UseFor'] == 'OptOutNotConfirmed')
					{
					$SecondsToSend = $EachEmails['Extra1'] * 3600;

					$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."emails WHERE EmailID='".$EachEmails['RelEmailID']."'";
					$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$ArrayOptOutEmail = mysql_fetch_assoc($ResultSet3);
					}
				}
			// Retrieve the mail list opt-in/out reminder emails - FINISHED

			// Set variables - STARTED
			$SendOptIn		=	false;
			$SendOptOut		=	false;
			// Set variables - FINISHED
			
			// Calculate the seconds passed since the opt-in/out request - STARTED
				if ($EachRow['SubscriptionStatus'] == 'Pending for opt-in confirmation')
					{
					$TMPRequested	= strtotime($EachRow['SubscriptionDate']);
					$SendOptIn		= true;
					$SendOptOut		= false;
					}
				elseif ($EachRow['SubscriptionStatus'] == 'Pending for opt-out confirmation')
					{
					$TMPRequested	= strtotime($EachRow['UnsubscriptionDate']);
					$SendOptIn		= false;
					$SendOptOut		= true;
					}
			$TMPNow			= strtotime(date("Y-m-d H:i:s"));
			$SecondsPassed		= $TMPNow - $TMPRequested;
			// Calculate the seconds passed since the opt-in/out request - FINISHED

			// Check if reminder has been sent to this member before - STARTED
			$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."cron_optreminder WHERE RelMemberID='".$ArrayMember['MemberID']."' AND RelMailListID='".$ArrayMailList['MailListID']."' AND RelMembershipID='".$EachRow['MembershipID']."' AND Purpose='".($SendOptIn == true ? 'Opt-In' : 'Opt-Out')."'";
			$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			if (mysql_num_rows($ResultSet2) > 0)
				{
// The following lines cause oemPro to send reminder email every XX hours. It is disabled. - STARTED
//				$ArrayTMPReminderLog = mysql_fetch_assoc($ResultSet2);
//				$TMPRequested	= strtotime($ArrayTMPReminderLog['ProcessedOn']);
//				$SecondsPassed	= $TMPNow - $TMPRequested;
// The following lines cause oemPro to send reminder email every XX hours. It is disabled. - FINISHED
				
// The following line will cause oemPro to skip already reminder sent members - STARTED
				continue;
// The following line will cause oemPro to skip already reminder sent members - FINISHED
				}
			// Check if reminder has been sent to this member before - FINISHED

			// Send the reminder email - STARTED
			if ($SecondsPassed > $SecondsToSend)
				{
				// Load required module - STARTED
				include_once("./system/classes/class_mailer.php");
				include_once("./system/libraries/membership.inc.php");
				include_once("./system/libraries/personalization.inc.php");
				// Load required module - FINISHED

					$ArrayMailListIDs		=	array($ArrayMailList['MailListID']);
					$EmailType				=	($SendOptIn == true ? 'OptInNotConfirmed' : 'OptOutNotConfirmed');
				SendMailListEmails($ArrayMailListIDs, $EmailType, $ArrayMember, $EachRow, $ArrayMailList, $ArrayLanguage, $ArrayConfig);

				// Update the reminder process log - STARTED
				$ArrayFieldnValues = array(
										"ProcessID"			=>	$ArrayTMPReminderLog['ProcessID'],
										"RelMemberID"		=>	$ArrayMember['MemberID'],
										"RelMailListID"		=>	$ArrayMailList['MailListID'],
										"RelMembershipID"	=>	$EachRow['MembershipID'],
										"Purpose"			=>	($SendOptIn == true ? 'Opt-In' : 'Opt-Out'),
										"ProcessedOn"		=>	date("Y-m-d H:i:s"),
										);
		
				$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
					$FieldList = "`".$FieldList."`";
				$ValueList = implode("','", array_values($ArrayFieldnValues));
					$ValueList ="'".$ValueList."'";
		
				$SQLQuery = "REPLACE INTO ".$ArrayConfig['Database']['Prefix']."cron_optreminder (".$FieldList.") VALUES (".$ValueList.")";
				$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				// Update the reminder process log - FINISHED

				$TotalEmailsSent++;
				}
			// Send the reminder email - FINISHED
			}
		}
	// Retrieve all opt-in/out confirmation pending memberships - FINISHED
	
	return $TotalEmailsSent;
	}


?>