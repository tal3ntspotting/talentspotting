<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/


/*
Function index:
------------------------------------------
JSRedirectWindow(String RedirectURL, String WhichWindow, Boolean FocusTo)
	Focuses to the target window (if set to true) and redirects window
	to the defined location

JSCloseWindow(String Message)
	Closes the window and prints (alert) a message

JSAlertMessage(String Message)
	Alerts a message
*/

//########################################################################################
// Redirect window to another location
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function JSRedirectWindow($RedirectURL, $WhichWindow, $FocusTo)
	{	
	print('<script language="JavaScript" type="text/JavaScript">');

	if ($FocusTo == true)
		{
		print ($WhichWindow.".window.focus();");
		}

	print ($WhichWindow.".window.location.href = '".$RedirectURL."'");
	
		
	print('</script>');
	
	return true;
	}
	
//########################################################################################
// Close window with JavaScript
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function JSCloseWindow($Message)
	{	
	print('<script language="JavaScript" type="text/JavaScript">');
		print ("window.close();");
	print('</script>');
	
	return true;
	}
//########################################################################################
// Alerts a message with JavaScript
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function JSAlertMessage($Message)
	{	
	print('<script language="JavaScript" type="text/JavaScript">');
		print ("alert('".$Message."');");
	print('</script>');
	
	return true;
	}


?>