<?php
/*
+---------------------------------------------+
| (c)Copyright Octeth Technologies.           |
| All rights reserved.                        |
|                                             |
| For more details, please visit:             |
| http://www.octeth.com/                      |
| info@octeth.com                             |
+---------------------------------------------+
*/
	
//########################################################################################
// Displays "Please wait" processing page
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DisplayProcessMessagePage($DisplayProgressBar, $ProgressBarDataSourceURL, $Title, $Message, $ArrayLoggedClient, $ArrayConfig, $ArrayLanguage, $ArrayFormFields, $ArrayErrorMessages)
	{	
	// Avoid time-out - STARTED
	header ("Content-type: text/html");
	flush();
	print ("                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ");
	flush();
	// Avoid time-out - FINISHED

	// Parse message page - STARTED
		$TemplateFileName	=	"section_client/process.html";
		$BodyTemplate 		=	"templates/body_client.html";
		$PageTitle	 		=	$ArrayLanguage["PageTitles"]["00001"].$ArrayLanguage["PageTitles"]["00034"];
		$InsertMetaRedirect	=	false;
		$ShowMenus			=	false;
	$ObjectTemplate = ParseAdminPage($ArrayLoggedClient, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect, $BodyTemplate, $ShowMenus);

	// Parse progress bar section - STARTED
	if ($DisplayProgressBar == true)
		{
		$ArrayReplaceList = array(
								"_DataSourceURL_"		=>	$ProgressBarDataSourceURL,
								);
		$ObjectTemplate->replace($ArrayReplaceList);
		}
	else
		{
		$ObjectTemplate->remove_block('ENABLE:PROGRESSBAR');
		}
	// Parse progress bar section - FINISED

	// Parse the form - STARTED
	$ObjectTemplate = ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages);
	// Parse the form - FINISHED
	
	$ArrayReplaceList = array(
							"_MessageTitle_"			=>	$Title,
							"_MessageDetails_"		=>	$Message,
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	
	// Parse page action message part - STARTED
		$ErrorMessage = ($ErrorMessage == '' ? '' : $ErrorMessage);
	$ObjectTemplate = ParsePageActionMsg($ErrorMessage, $ObjectTemplate);
	// Parse page action message part - FINISHED

	print $ObjectTemplate->parseandoutput();
	flush();
	// Parse message page - FINISHED
	
	return true;
	}

//########################################################################################
// Check for client ownership for the mail list
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckOwnershipForMailList($ClientID, $MailListID, $ArrayConfig)
	{	
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$ArrayConfig['Database']['Prefix']."maillist_clients WHERE RelClientID='".$ClientID."' AND RelMailListID='".$MailListID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound < 1)
		{
		return false;
		}
	else
		{
		return true;
		}
	}
	
//########################################################################################
// Displays Message Page
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function DisplayMessagePage($Title, $Message, $ArrayLoggedClient, $ArrayConfig, $ArrayLanguage, $ArrayFormFields, $ArrayErrorMessages)
	{	
	// Parse message page - STARTED
		$TemplateFileName	=	"section_client/message.html";
		$BodyTemplate 		=	"templates/body_client.html";
		$PageTitle	 		=	$ArrayLanguage["PageTitles"]["00001"].$Title;
		$InsertMetaRedirect	=	false;
		$ShowMenus			=	true;
	$ObjectTemplate = ParseAdminPage($ArrayLoggedClient, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect, $BodyTemplate, $ShowMenus);
	$ObjectTemplate = ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages);
	
	$ArrayReplaceList = array(
							"_MessageTitle_"		=>	$Title,
							"_MessageDetails_"		=>	$Message,
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	
	// Parse page action message part - STARTED
		$ErrorMessage = ($ErrorMessage == '' ? '' : $ErrorMessage);
	$ObjectTemplate = ParsePageActionMsg($ErrorMessage, $ObjectTemplate);
	// Parse page action message part - FINISHED

	print $ObjectTemplate->parseandoutput();
	// Parse message page - FINISHED
	
	return true;
	}

//########################################################################################
// Make login
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function CheckLogin($ArrayConfig)
	{	
	// Initialize variables
	$ErrorExists	=	false;

	// Check for admin - STARTED
	if ($ErrorExists == false)
		{
		// The following line is removed from the SQL query because it is causing problems if register_globals is turned on
		// AdministratorID='".$_SESSION['oemPro']['AdministratorID']."' AND
		$SQLQuery = "SELECT * FROM ".$ArrayConfig['Database']['Prefix']."clients WHERE Username='".$_SESSION['oemPro']['Client']['Username']."' AND Password='".$_SESSION['oemPro']['Client']['Password']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_num_rows($ResultSet);

		if ($TotalFound < 1)
			{
			// No admin account found. Incorrect admin information
			$ErrorExists = true;
			return false;
			}
		else
			{
			// Admin login information is correct
			$ArrayClient = mysql_fetch_assoc($ResultSet);

			// Reload the correct language pack for this administrator - STARTED
			if ($ArrayClient['Language'] != '')
				{
				include($ArrayConfig['Paths']['Data']."/languages/lang_".strtolower($ArrayClient['Language']).".inc.php");
				unset($GLOBALS['ArrayLanguage']);
				$GLOBALS['ArrayLanguage'] = $ArrayLanguage;
				unset($ArrayLanguage);
				}
			// Reload the correct language pack for this administrator - FINISHED

			return $ArrayClient;
			}
		}
	// Check for admin - FINISHED
	
	return true;
	}
	
//########################################################################################
// Updates login info stored in the session
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function UpdateLoginUser($ArraySessionData, $ArrayConfig, $SessionID)
	{
	// Register session variables
	foreach ($ArraySessionData as $Key=>$Val)
		{
		$_SESSION['oemPro']['Client'][$Key] = $Val;
		}

	return true;
	}
	
//########################################################################################
// Make login
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function LoginUser($ArraySessionData, $ArrayConfig, $SessionID)
	{
	// Register session variables
	foreach ($ArraySessionData as $Key=>$Val)
		{
		$_SESSION['oemPro']['Client'][$Key] = $Val;
		}

	// Set cookie
	setcookie($ArrayConfig['Cookie']['Login']['Client'], $SessionID);

	return true;
	}
	
//########################################################################################
// Parse page action message
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParsePageActionMsg($ErrorMessage, $ObjectTemplate)
	{
	$ArrayReplaceList	=	array(
								"_PageActionMessage_"			=>			addslashes($ErrorMessage),
								);
	$ObjectTemplate->replace($ArrayReplaceList);
		
	return $ObjectTemplate;
	}
	
//########################################################################################
// Parse the template file
// ---
// Void		IN		N/A				- N/A
// Void		OUT		N/A				- N/A
// ---
function ParsePageForm($ObjectTemplate, $ArrayFormFields, $ArrayErrorMessages)
	{
	// Parse the form - STARTED
	$ObjectFormParser = new OctethFormParser();

		$ObjectFormParser->ObjectTemplate			=	$ObjectTemplate;
		$ObjectFormParser->EnteredTagName			=	"_entered";
		$ObjectFormParser->FormFieldTagName			=	"FormValue_";
		$ObjectFormParser->ErrorTagName				=	"_error";
		$ObjectFormParser->ErrorBlockName			=	"ERROR:";
		$ObjectFormParser->SelectedRadio			=	"_selectedradio";
		$ObjectFormParser->SelectedCheckbox			=	"_selectedcheckbox";
		$ObjectFormParser->SelectedList				=	"_selectedlist";
		$ObjectFormParser->SelectedMultiList		=	"_selectedmultlist";
		$ObjectFormParser->ArrayPostVars			=	$_POST;
		$ObjectFormParser->ArrayFormFields			=	$ArrayFormFields;
		$ObjectFormParser->ErrorMessages			=	$ArrayErrorMessages;
	
		$ObjectFormParser->ParseForm();
	$ObjectTemplate = $ObjectFormParser->ReturnTemplate();
	// Parse the form - FINISHED

	// Mark the fields which have errors - STARTED
	$CSSClassName = "FieldValueColumnFieldError";
	
	$ArrayReplaceList = array();
	foreach ($ArrayErrorMessages as $Field=>$Message)
		{
		$ArrayReplaceList['_errorclass'.$Field.'_'] = $CSSClassName;
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Mark the fields which have errors - FINISHED

	return $ObjectTemplate;
	}

//########################################################################################
// Parse the template file
// ---
// Array	IN		ArrayConfig		- Configuration array
// Object	OUT		N/A				- Template object
// ---
function ParseClientPage($ArrayLoggedClient, $ArrayConfig, $TemplateFileName, $ArrayLanguage, $PageTitle, $InsertMetaRedirect = false, $BodyTemplate = "templates/body_client.html", $ShowMenus = true)
	{
	// Load template library - STARTED
	include_once($ArrayConfig['Paths']['Software']."/system/libraries/template.inc.php");
	// Load template library - FINISHED

	// Define which skin file to use - STARTED
	if ($ArrayLoggedClient['Skin'] != '')
		{
		$SkinName = strtolower($ArrayLoggedClient['Skin']);
		}
	else
		{
		$SkinName = strtolower($ArrayConfig['Settings']['Skin']);
		}
	// Define which skin file to use - FINISHED

	// Include template parts - STARTED
		// Backward compability with old plug-ins and modules - STARTED
		$ArrayMainTemplates = array(
									"section_administrator/inserts/body.html"	=>	"templates/body_administrator.html",
									"section_member/inserts/body.html"			=>	"templates/body_member.html",
									"section_client/inserts/body.html"			=>	"templates/body_client.html",
									"section_public/inserts/body.html"			=>	"templates/body_public.html",
									);
		if ($ArrayMainTemplates[$BodyTemplate] != '')
			{
			$BodyTemplate = $ArrayMainTemplates[$BodyTemplate];
			}
		// Backward compability with old plug-ins and modules - FINISHED

		$IncludeTemplateFileName = $BodyTemplate;
	$ObjectTemplate = new OctethTemplate($ArrayConfig['Paths']['Data'].'/templates/'.$SkinName.'/'.$IncludeTemplateFileName, 'file');
	// Include template parts - FINISHED

	// Remove menus if enabled - STARTED
	if ($ShowMenus == false)
		{
		$ObjectTemplate->remove_block("ENABLE:MENUS");
		}
	// Remove menus if enabled - FINISHED

	// Remove META Redirect header if disabled - STARTED
	if ($InsertMetaRedirect == false)
		{
		$ObjectTemplate->remove_block("ENABLE:META:REDIRECT");
		}
	// Remove META Redirect header if disabled - FINISHED

	// Load template - STARTED
	$ObjectTemplateBody = new OctethTemplate($ArrayConfig['Paths']['Data'].'/templates/'.$SkinName.'/'.$TemplateFileName, 'file');
	// Load template - FINISHED

	// Check if template file loaded - STARTED
	if ($ObjectTemplateBody->Error != '')
		{
		$ErrorMessage = $ArrayLanguage["Texts"]["00001"];
		PrintRawError($ErrorMessage);
		exit;;
		}
	// Check if template file loaded - FINISHED

	// Decide the page image - STARTED
	$PageFileName = $_SERVER["SCRIPT_NAME"];
		$PageFileName = pathinfo($PageFileName);
		$PageFileName = $PageFileName['basename'];
		$PageFileName = substr($PageFileName, 0, strlen($PageFileName) - 4);
	if (file_exists($ArrayConfig['Paths']['Data']."/templates/".$SkinName."/section_client/images/page_icons/".$PageFileName.".gif") == true)
		{
		$PageImage = $PageFileName.".gif";
		}
	else
		{
		$PageImage = "default.gif";
		}
	// Decide the page image - FINISHED

	// Decide the page name - STARTED
	$PageName = $PageTitle;
	$PageName = substr($PageTitle, strlen($ArrayLanguage["PageTitles"]["00001"]), strlen($PageTitle));
	// Decide the page name - FINISHED

	// Insert content into template - STARTED
	$ArrayReplaceList = array(
							"_Insert:PageContent_"			=>	$ObjectTemplateBody->parseandoutput(),
							"_Insert:PageName_"				=>	$PageName,
							"_Insert:PageImage_"			=>	$PageImage,
							);
	$ObjectTemplate->replace($ArrayReplaceList);
	
		$PageContent = $ObjectTemplate->parseandoutput();
	$ObjectTemplate = new OctethTemplate($PageContent, 'string');
	unset($PageContent, $ObjectTemplateBody);
	// Insert content into template - FINISHED

	// Parse screen language - STARTED
	$ObjectTemplate = ParseScreenLanguage($ObjectTemplate, $ArrayLanguage, $ArrayConfig);
	// Parse screen language - FINISHED

	// Make replacements - STARTED
		$ArrayReplaceList = array(
								"_SoftwareUrl_"		=>		$ArrayConfig['URLs']['Software'],
								"_ClientUrl_"		=>		$ArrayConfig['URLs']['Software']."/client",
								"_DataUrl_"			=>		$ArrayConfig['URLs']['Data'],
								"_TemplateUrl_"		=>		$ArrayConfig['URLs']['Data']."/templates/".$SkinName."/section_client",
								"_OverAllTemplateUrl_"	=>		$ArrayConfig['URLs']['Data']."/templates/".$SkinName,
								"_PageTitle_"		=>		$PageTitle,
								"_PageCharSet_"		=>		($ArrayLoggedClient['CharSet'] != '' ? substr($ArrayLoggedClient['CharSet'], 0, strlen($ArrayLoggedClient['CharSet']) - 5) : $ArrayLanguage["Config"]["CharSet"]),
								"_oemPro:Version_"	=>		ParseOEMPVersion($ArrayConfig['Settings']['SoftwareVersion'], $ArrayConfig['Settings']['SoftwareVersionType']),
								);
	$ObjectTemplate->replace($ArrayReplaceList);
	// Make replacements - FINISHED

	// Parse logged admin information - STARTED
	$ArrayReplaceList = array();
	foreach ($ArrayLoggedClient as $Key=>$Val)
		{
		$ArrayReplaceList['_LoggedClient:'.$Key.'_'] = $Val;
		}
	$ObjectTemplate->replace($ArrayReplaceList);
	// Parse logged admin information - FINISHED

	// Show/Hide logged in user information if user is logged in - STARTED
	if ($ArrayLoggedClient['ClientID'] == '')
		{
		$ObjectTemplate->remove_block('SHOWHIDE:LOGGEDINUSER');
		}
	// Show/Hide logged in user information if user is logged in - FINISHED

	// Apply branding - STARTED
	include_once('../system/libraries/branding.inc.php');
	$ObjectTemplate = ApplyBranding($ObjectTemplate, $ArrayConfig);
	// Apply branding - FINISHED

	return $ObjectTemplate;
	}

?>