<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-07-05 10:46        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ SUBMITTED FORM PARSING CLASS
----[ SYNOPSIS


$ObjectFormParser = new OctethFormParser();

	$ObjectFormParser->ObjectTemplate			=	$ObjectTemplate;
	$ObjectFormParser->EnteredTagName			=	"_entered";
	$ObjectFormParser->FormFieldTagName			=	"FormValue_";
	$ObjectFormParser->ErrorTagName				=	"_error";
	$ObjectFormParser->ErrorBlockName			=	"ERROR:";
	$ObjectFormParser->SelectedRadio			=	"_selectedradio";
	$ObjectFormParser->SelectedCheckbox			=	"_selectedcheckbox";
	$ObjectFormParser->SelectedList				=	"_selectedlist";
	$ObjectFormParser->SelectedMultiList		=	"_selectedmultlist";
	$ObjectFormParser->ArrayPostVars			=	(CheckVersions(phpversion(), "4.2.0", "PHP") == 1 ? $_REQUEST : $_REQUEST);
	$ObjectFormParser->ArrayFormFields			=	array(
														"Username"	=>	"TextField",
														"Address"	=>	"TextArea",
														"Password"	=>	"Password",
														"Country"	=>	"DropList",
														"MailLists"	=>	"List",
														"Interests"	=>	"Checkbox",
														"Gender"	=>	"RadioButton",
														"MyID"		=>	"Hidden",
														"WorkFile"	=>	"File",
														"
														);
	$ObjectFormParser->ErrorMessages			=	array(
														"Username"	=>	"Error1",
														"Password"	=>	"Error2",
														);

	$ObjectFormParser->ParseForm();
$ObjectTemplate = $ObjectFormParser->ReturnTemplate();




----[ DESCRIPTION
With this module, you can parse the
submitted forms. The submitted forms
will be displayed with the entered
values and also errors will be specific
to fields.
NOTE: This object is compatible with only 
Octeth Template class!

----[ CLASS NAME AND VERSION
OctethFormParser v1.7

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


********************************************************************
********************************************************************
********************************************************************
************** DO NOT FORGET TO UPDATE THE *************************
********* LAST MODIFY DATE AND TIME AFTER MAKING *******************
********************** MODIFICATIONS *******************************
********************************************************************
********************************************************************
********************************************************************

*/

//***********************************************************
// OctethFiller Class
//***********************************************************
class OctethFormParser
{
//***********************************************************
// Public Properties
//***********************************************************
// Octeth Template object
var $ObjectTemplate				=	"";

// The name of the form fields
var $ArrayFormFields			=	array();

// Tag of the entered values
// Ex: _entered
var $EnteredTagName				=	"_entered";

// Tag of the form field
// Ex: FormValue_
var $FormFieldTagName			=	"FormValue_";

// Tag of the error message
// Ex: _error
var $ErrorTagName				=	"_error";

// Block name of the error message
// Ex: ERROR:
var $ErrorBlockName				=	"ERROR:";

// Fields with error
// This is assosciative array. Key is the field name.
// Value is the error message
var $ErrorMessages				=	array();

// HTTP Post variables
var $ArrayPostVars				=	array();

// If selected, what is the tag of the radio button
var $SelectedRadio				=	"_selectedradio";

// If selected, what is the tag of the checkbox
var $SelectedCheckbox			=	"_selectedcheckbox";

// If selected, what is the tag of the list
var $SelectedList				=	"_selectedlist";

// If selected, what is the tag of the list
var $SelectedMultiList			=	"_selectedmultlist";

//***********************************************************
// Private Properties
//***********************************************************
var $_ErrorExists				=	false;

//***********************************************************
// Public Methods
//***********************************************************

//-----------------------------------------------------------
// Constructor function
// ---
// void		IN		N/A		- No inputs
// void		OUT		N/A		- Returns nothing
// ---
//-----------------------------------------------------------
function OctethFormParser()
	{
	}

//-----------------------------------------------------------
// Parse the form
// ---
// void		IN		N/A		- No inputs
// void		OUT		N/A		- Returns true always
// ---
//-----------------------------------------------------------
function ParseForm()
	{
	foreach ($this->ArrayFormFields as $EachField=>$FieldType)
		{
		// Parse the entered form fields value - STARTED
		if ($FieldType == "Hidden")
			{
			// Hidden field
			$ArrayReplaceList[$this->EnteredTagName.$EachField."_"] = htmlentities(stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField]), ENT_COMPAT, 'UTF-8');
			}
		elseif ($FieldType == "File")
			{
			// File field
			$ArrayReplaceList[$this->EnteredTagName.$EachField."_"] = $this->ArrayPostVars[$this->FormFieldTagName.$EachField];
			}
		elseif ($FieldType == "TextField")
			{
			// Text field
			$ArrayReplaceList[$this->EnteredTagName.$EachField."_"] = htmlentities(stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField]), ENT_COMPAT, 'UTF-8');
			}
		elseif ($FieldType == "TextArea")
			{
			// Text area field
			$ArrayReplaceList[$this->EnteredTagName.$EachField."_"] = htmlentities(stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField]), ENT_COMPAT, 'UTF-8');
			}
		elseif ($FieldType == "Password")
			{
			// Password field
			$ArrayReplaceList[$this->EnteredTagName.$EachField."_"] = "";
			}
		elseif ($FieldType == "DropList")
			{
			// Droplist field
			$ArrayReplaceList[$this->SelectedList.$EachField.stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField]."_")] = "selected";
			}
		elseif ($FieldType == "List")
			{
			// List field
			if ((gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "array") || (gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "Array") || (gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "ARRAY"))
				{
				foreach ($this->ArrayPostVars[$this->FormFieldTagName.$EachField] as $EachSelection)
					{
					$ArrayReplaceList[$this->SelectedMultiList.$EachField.stripslashes($EachSelection)."_"] = "selected";
					}
				}
			else
				{
				$ArrayReplaceList[$this->SelectedMultiList.$EachField.stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField])."_"] = "selected";
				}
			}
		elseif ($FieldType == "Checkbox")
			{
			// Checkbox field
			if ((gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "array") || (gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "Array") || (gettype($this->ArrayPostVars[$this->FormFieldTagName.$EachField]) == "ARRAY"))
				{
				foreach ($this->ArrayPostVars[$this->FormFieldTagName.$EachField] as $EachSelection)
					{
					$ArrayReplaceList[$this->SelectedCheckbox.$EachField.stripslashes($EachSelection)."_"] = "checked";
					}
				}
			else
				{
				$ArrayReplaceList[$this->SelectedCheckbox.$EachField.stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField])."_"] = "checked";
				}
			}
		elseif ($FieldType == "RadioButton")
			{
			// Radiobutton field
			$ArrayReplaceList[$this->SelectedRadio.$EachField.stripslashes($this->ArrayPostVars[$this->FormFieldTagName.$EachField])."_"] = "checked";
			}			
		// Parse the entered form fields value - FINISHED

		// Parse the field error blocks - STARTED
			$ErrorBlockName = $this->ErrorBlockName.$EachField;
			$ErrorTagName	= $this->ErrorTagName.$EachField."_";

		if ($this->ErrorMessages[$EachField] != "")
			{
			// Error exists on this field
			$this->ObjectTemplate->define_block($ErrorBlockName);
				$ArrayReplaceList[$ErrorTagName] = $this->ErrorMessages[$EachField];
			$this->ObjectTemplate->duplicate_block($ErrorBlockName, $ErrorBlockName, $ArrayReplaceList);
			}
		else
			{
			$this->ObjectTemplate->remove_block($ErrorBlockName);
			}
		// Parse the field error blocks - FINISHED
		}
	
	$this->ObjectTemplate->replace($ArrayReplaceList);
	
	return true;
	}
	

//-----------------------------------------------------------
// Returns the template object
// ---
// void		IN		N/A		- No inputs
// object	OUT		N/A		- Returns the template object
// ---
//-----------------------------------------------------------
function ReturnTemplate()
	{
	return $this->ObjectTemplate;
	}
	
//***********************************************************
// Private Methods
//***********************************************************
//-----------------------------------------------------------
// Check if error exists in the object processes
// ---
// bool		OUT		N/A		- Returns false if any error exists in the object processes
// ---
//-----------------------------------------------------------
function _ErrorExists()
	{
	return false;
	}


//-----------------------------------------------------------
// Compares the versions
// ---
// String   IN      Version1        - The first version
// String   IN      Version2        - The second version
// String   IN      VersionType     - MySQL, PHP, Zend or OEM
// Integer  OUT     -               - Returns 1 or 2. 1 is the first, 2 is the second version greater
// ---
//-----------------------------------------------------------
function CheckVersions($Version1, $Version2, $VersionType)
    {
    // Initialize variables
    $GreaterVersion = 0;
    
    
    if ($VersionType == "PHP")
        {
        // Check & Compare PHP versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "MySQL")
        {
        // Check & Compare MySQL versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "Zend")
        {
        // Check & Compare Zend Engine versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "OEM")
        {
        // Check & Compare OEM versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    
    return $GreaterVersion;
    }







};
//***********************************************************
// End of the class
//***********************************************************

?>