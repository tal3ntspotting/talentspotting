<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2006-12-19 12:46        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ MIME EMAIL PARSER
----[ SYNOPSIS




----[ DESCRIPTION
This class will let you to parse
MIME encoded emails
Important:
This module requires mimeDecode and
RFC822 classes.

----[ CLASS NAME AND VERSION
OctethMimeParser v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


********************************************************************
********************************************************************
********************************************************************
************** DO NOT FORGET TO UPDATE THE *************************
********* LAST MODIFY DATE AND TIME AFTER MAKING *******************
********************** MODIFICATIONS *******************************
********************************************************************
********************************************************************
********************************************************************

*/

//***********************************************************
// OctethMimeParser Class
//***********************************************************
class OctethMimeParser
{
//***********************************************************
// Public Properties
//***********************************************************


//***********************************************************
// Private Properties
//***********************************************************

//***********************************************************
// Public Methods
//***********************************************************

//-----------------------------------------------------------
// Constructor function
function OctethMimeParser()
	{
	}

//-----------------------------------------------------------
// Constructor function
function DecodeRawEmail($RawEmailContent)
	{
	// Declare objects - STARTED
	$ObjectMIMEDecode = new Mail_mimeDecode($RawEmailContent, "\n");
	$ObjectRFC822 = new Mail_RFC822;
	// Declare objects - FINISHED
	
	$ArrayDecodeParameters['input']				= $input;
	$ArrayDecodeParameters['include_bodies']	= true;
	$ArrayDecodeParameters['decode_bodies'] 	= true;
	$ArrayDecodeParameters['decode_headers']	= true;
	
	$ObjectStructure = $ObjectMIMEDecode->decode($ArrayDecodeParameters);

	return $ObjectStructure;	
	}


	
//***********************************************************
// Private Methods
//***********************************************************


};
//***********************************************************
// End of the class
//***********************************************************

?>