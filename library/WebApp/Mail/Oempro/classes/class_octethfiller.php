<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-06-12 14:29        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ RECORD FILLING CLASS
----[ SYNOPSIS
	$ObjectFiller 		= new OctethFiller();

	$ObjectFiller->ObjectTemplate		=	$ObjectTemplate;
	$ObjectFiller->Prefix				=	"_filladmininfo";
	$ObjectFiller->RecordContent		=	$ArrayCurrentAdmin;

	$ObjectFiller->ParseTemplate();
$ObjectTemplate = $ObjectFiller->ReturnTemplate();




----[ DESCRIPTION
With this module, you can fill your page
with your records details
NOTE: This object is compatible with only 
Octeth Template class!

----[ CLASS NAME AND VERSION
OctethFiller v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


---------------------------------------------------------------------------------------
*/

//-------------------------------------------[ OctethFiller Class
class OctethFiller
{
//-------------------------------------------------------------[ Public Properties
// Octeth Template object
var $ObjectTemplate				=	"";

// Prefix. The prefix which is included
// before the field name
var $Prefix						=	"_form";

// Array (associative) which includes the
// information
var $RecordContent				=	array();

//-------------------------------------------------------------[ Private Properties
var $_ErrorExists				=	false;

//-------------------------------------------------------------[ Public Methods
//###############################################################################
// Constructor function
// ---
// void		IN		N/A		- No inputs
// void		OUT		N/A		- Returns nothing
// ---
function OctethFiller()
	{
	}

//###############################################################################
// Parses the template
// ---
// void		IN		N/A		- No inputs
// object	OUT		N/A		- Returns the template object
// ---
function ParseTemplate()
	{
	foreach ($this->RecordContent as $Key=>$Value)
		{
		$ArrayReplaceList[$this->Prefix.$Key."_"] = htmlentities($Value,ENT_QUOTES,'UTF-8');
		}
	$this->ObjectTemplate->replace($ArrayReplaceList);

	return true;
	}
	
//###############################################################################
// Returns the template object
// ---
// void		IN		N/A		- No inputs
// object	OUT		N/A		- Returns the template object
// ---
function ReturnTemplate()
	{
	return $this->ObjectTemplate;
	}
	
//-------------------------------------------------------------[ Private Methods
//###############################################################################
// Check if error exists in the object processes
// ---
// bool		OUT		N/A		- Returns false if any error exists in the object processes
// ---
function _ErrorExists()
	{
	return false;
	}



}; // end of the class

?>