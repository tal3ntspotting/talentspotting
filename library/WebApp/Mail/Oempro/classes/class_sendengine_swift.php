<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2006-12-20 14:51        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

//***********************************************************
// Send Engine Class
//***********************************************************
class SendEngine
{
//***********************************************************
// Public Properties
//***********************************************************
var $TotalConcurrent		= 0;		// Number of campaigns which are active at the moment
var $TotalAvailableLimit	= 0;		// Available concurrent campaign sending limit
var $TotalPendingCampaigns	= 0;		// Number of pending campaigns to be sent
var $ErrorExists			= false;	// Error status
var $ErrorMessage			= '';		// Error message
var $ArrayPendingCampaignIDs= array();	// Array of pending campaigns to be sent
var $ArrayCampaign			= array();	// Array of the current sending campaign
var $ArrayAdministrator		= array();	// Array of the campaign owner administrator
var $IsResumed				= false;	// The send type of the campaign (Resume of fresh send)
var $ArrayCampaignStatistics= array();	// Current send log array
var $CampaignStatisticsID	= 0;		// Current send log ID
var $ArrayClients			= array();	// List of campaign clients
var $ArraySMTP				= array();	// List of campaign send methods
var $ArrayAttachments		= array();	// List of campaign attachments
var $ArrayContent			= array();	// List of campaign content
var $ArrayNewsletter		= array();	// List of campaign newsletter template
var $ArrayFromEmail			= array();	// List of "from:" header
var $ArrayReplyToEmail		= array();	// List of "reply-to" header
var $ArrayReturnPathEmail	= array();	// List of "return-path" header
var $ArrayEmailProperties	= array();	// List of parameters to generate and send the newsletter
var $RecipientSQLQuery		= '';		// SQL query for retrieving campaign recipients
var $ArrayMailLists			= array();	// List of recipient mail lists
var $TotalRecipients		= 0;		// Total number of recipients (local + outsourced)
var $TotalOverAllRecipients	= 0;		// Total number of recipients (local + outsourced) (without resume)
var $TotalIntervals			= 0;		// Number of intervals to loop all recipients
var $TotalOutsourcedRecipients= 0;		// Number of outsourced recipients
var $ArrayOutsourcedMailLists= array();	// List of recipients outsourced mail lists
var $TotalLocalRecipients	= 0;		// Number of local recipients
var $BenchmarkStartTime		= 0;		// Timestamp of process start time
var $BenchmarkFinishTime	= 0;		// Timestamp of process finish time
var $BechmarkIntervalStartTime	= 0;	// Timestamp of interval start time
var $BechmarkIntervalFinishTime	= 0;	// Timestamp of interval finish time
var $BenchmarkAvgEmailsSec	= 0;		// Average emails per second
var $BenchmarkInsEmailsSec	= 0;		// Instant emails per second (per interval)
var $TotalSentMembers		= 0;		// Amount of sent recipients
var $TotalFailedMembers		= 0;		// Amount of failed recipients
var $TotalLoopedMembers		= 0;		// Amount of looped recipients (from 0)
var $TotalIntervalLoopedMembers	= 0;	// Amount of looped recipients (from interval beginning)
var $LoadBalanceLoopedMembers	= 0;	// Amount of looped recipients for load balancing
var $ArraySMTPProcessLog	= array();	// List of SMTP process log during sending
var $RecipientsPerInterval	=	100;	// Amount of recipients to retrieve from MySQL database per each interval
var $DelayMicroSecondsPerInterval= 0;	// Number of microseconds to wait every interval
var $ProcessStopped			= false;	// Status of the sending process
var $TotalRecipientsSQLQuery= '';		// SQL Query for calculating the total recipients


//***********************************************************
// Private Properties
//***********************************************************
var $ArrayConfig			= array();	// Configuration array of oemPro
var $ArrayLanguage			= array();	// Language array of oemPro
var $ObjectBenchmark		= object;	// Benchmark object of oemPro
var $ObjectMailer			= object;	// Mailer object of oemPro
var $LogID					= 0;		// oemPro internal process log ID
var $TemporaryMySQLTableName= '';		// Temporary campaign recipients table name




//***********************************************************
// Public Methods
//***********************************************************

//-----------------------------------------------------------
// Constructor function
function SendEngine($ArrayConfig, $ArrayLanguage, $ObjectBenchmark, $LogID)
	{
	$this->ArrayConfig		= $ArrayConfig;
	$this->ObjectBenchmark	= $ObjectBenchmark;
	$this->ArrayLanguage	= $ArrayLanguage;
	$this->LogID			= $LogID;

	return;
	}

function StartSending()
	{
	$this->_CalculateAvailableSendingLimit();
	$this->_SelectPendingCampaignToSend();

	$this->_ProcessEachCampaign();

	return;
	}
	
//***********************************************************
// Private Methods
//***********************************************************

//-----------------------------------------------------------
// Process each pending local campaign
function _ProcessEachCampaign()
	{
	foreach ($this->ArrayPendingCampaignIDs as $EachCampaignID)
		{
		if (($EachCampaignID == '') || ($EachCampaignID == 0))
			{
			continue;
			}

		$this->ObjectBenchmark->Calculate("Each campaign loop started. Campaign ID #".$EachCampaignID);

		// Update process log - STARTED
			$LogID			=	$this->LogID;
			$Module			=	'';
			$StartTime		=	'';
			$FinishTime		=	'';
			$ProcessStatus	=	'Started';
			$Extra1			=	'Started to process campaign ID #'.$EachCampaignID;
			$Extra2			=	'';
			$Extra3			=	'';
		$this->LogID = UpdateProcessLog($LogID, $Module, $StartTime, $FinishTime, $ProcessStatus, $Extra1, $Extra2, $Extra3, $this->ArrayConfig);

		$this->ObjectBenchmark->Calculate("Process log updated: Campaign is being processed");
		// Update process log - FINISHED

		// Retrieve campaign information - STARTED
		$this->_GetCampaignRecord($EachCampaignID);
		// Retrieve campaign information - FINISHED

		// Retrieve owner administrator information - STARTED
		$this->_GetAdministratorRecord($EachCampaignID);
		// Retrieve owner administrator information - FINISHED

		// Decide if this is a resume or restart process - STARTED
		$this->_DecideIfThisSendingIsAResume($EachCampaignID);
		// Decide if this is a resume or restart process - FINISHED

		// Update campaign statistics - STARTED
		if (($this->IsResumed == false) && ($this->CampaignStatisticsID == ''))
			{
			$ArrayFieldnValues = array(
									"CampaignStatisticsID"					=>	'',
									"RelCampaignID"							=>	$EachCampaignID,
									"Status"								=>	'Preparing',
									"StatusDescription"						=>	'Preparing campaign',
									"TotalRecipients"						=>	0,
									"SentRecipients"						=>	0,
									"FailedRecipients"						=>	0,
									"SendStartDateTime"						=>	'0000-00-00 00:00:00',
									"SendFinishDateTime"					=>	'0000-00-00 00:00:00',
									"SendLastActionDateTime"				=>	'0000-00-00 00:00:00',
									"InstantEmailsPerSecond"				=>	0,
									"AverageEmailsPerSecond"				=>	0,
									"TotalSentEmailSizeBytes"				=>	0,
									"QueuePointer"							=>	0,
									"QueuePointerOutsource"					=>	0,
									"QueuePointerRelOutsourcedMailListID"	=>	0,
									"Sleeping"					=>	0,	
									);
			$this->_UpdateCampaignStatistics($ArrayFieldnValues, $EachCampaignID, '', false);
			}
		// Update campaign statistics - FINISHED

		// Retrieve campaign clients - STARTED
		$this->_GetCampaignClients($EachCampaignID);
		// Retrieve campaign clients - FINISHED

		// Retrieve campaign SMTP settings - STARTED
		$this->_GetCampaignSendMethods($EachCampaignID);
		// Retrieve campaign SMTP settings - FINISHED

		// Retrieve attachments - STARTED
		$this->_GetCampaignAttachments($EachCampaignID);
		// Retrieve attachments - FINISHED

		// Retrieve content - STARTED
		$this->_GetCampaignContent($EachCampaignID);
		// Retrieve content - FINISHED

		// Retrieve from, reply to and return path emails - STARTED
		$this->_GetCampaignSenderInfo($EachCampaignID);
		// Retrieve from, reply to and return path emails - FINISHED

		// Prepare email properties - STARTED
		$this->_PrepareEmailProperties($EachCampaignID);
		// Prepare email properties - FINISHED

		// Update campaign statistics - STARTED
			$ArrayFieldnValues = array(
									"CampaignStatisticsID"					=>	$this->ArrayCampaignStatistics['CampaignStatisticsID'],
									"RelCampaignID"							=>	$EachCampaignID,
									"Status"								=>	'Preparing',
									"StatusDescription"						=>	'Preparing recipients',
									"TotalRecipients"						=>	$this->ArrayCampaignStatistics['TotalRecipients'],
									"SentRecipients"						=>	$this->ArrayCampaignStatistics['SentRecipients'],
									"FailedRecipients"						=>	$this->ArrayCampaignStatistics['FailedRecipients'],
									"SendStartDateTime"						=>	date("Y-m-d H:i:s"),
									"SendFinishDateTime"					=>	'0000-00-00 00:00:00',
									"SendLastActionDateTime"				=>	date("Y-m-d H:i:s"),
									"InstantEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['InstantEmailsPerSecond'],
									"AverageEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['AverageEmailsPerSecond'],
									"TotalSentEmailSizeBytes"				=>	$this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'],
									"QueuePointer"							=>	$this->ArrayCampaignStatistics['QueuePointer'],
									"QueuePointerOutsource"					=>	$this->ArrayCampaignStatistics['QueuePointerOutsource'],
									"QueuePointerRelOutsourcedMailListID"	=>	$this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID'],
									"Sleeping"					=>	0,	
									);
			$this->_UpdateCampaignStatistics($ArrayFieldnValues, $EachCampaignID, $this->ArrayCampaignStatistics['CampaignStatisticsID'], false);
		// Update campaign statistics - FINISHED

		// Create temporary MySQL table for campaign recipients - STARTED
		$this->_CreateTemporaryMySQLTable($EachCampaignID);
		// Create temporary MySQL table for campaign recipients - FINISHED

		// Generate recipient SQL Query - STARTED
		$this->_GenerateRecipientSQLQuery();
		// Generate recipient SQL Query - FINISHED

		// Update campaign statistics - STARTED
			$ArrayFieldnValues = array(
									"CampaignStatisticsID"					=>	$this->ArrayCampaignStatistics['CampaignStatisticsID'],
									"RelCampaignID"							=>	$EachCampaignID,
									"Status"								=>	'Sending',
									"StatusDescription"						=>	'Sending campaign',
									"TotalRecipients"						=>	($this->IsResumed == true ? $this->ArrayCampaignStatistics['TotalRecipients'] : $this->TotalRecipients),
									"SentRecipients"						=>	$this->ArrayCampaignStatistics['SentRecipients'],
									"FailedRecipients"						=>	$this->ArrayCampaignStatistics['FailedRecipients'],
									"SendStartDateTime"						=>	($this->IsResumed == true ? $this->ArrayCampaignStatistics['SendStartDateTime'] : date('Y-m-d H:i:s')),
									"SendFinishDateTime"					=>	'0000-00-00 00:00:00',
									"SendLastActionDateTime"				=>	date("Y-m-d H:i:s"),
									"InstantEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['InstantEmailsPerSecond'],
									"AverageEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['AverageEmailsPerSecond'],
									"TotalSentEmailSizeBytes"				=>	$this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'],
									"QueuePointer"							=>	$this->ArrayCampaignStatistics['QueuePointer'],
									"QueuePointerOutsource"					=>	$this->ArrayCampaignStatistics['QueuePointerOutsource'],
									"QueuePointerRelOutsourcedMailListID"	=>	$this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID'],
									"Sleeping"					=>	0,	
									);
		$this->_UpdateCampaignStatistics($ArrayFieldnValues, $EachCampaignID, $this->ArrayCampaignStatistics['CampaignStatisticsID'], false);
		// Update campaign statistics - FINISHED

		// Perform global personalization on email content - STARTED
		$this->ArrayEmailProperties['Subject']			= PerformGlobalCampaignPersonalization($this->ArrayEmailProperties['Subject'], $this->ArrayContent['ContentType'], 'Subject', $this->ArrayConfig, $this->ArrayLanguage, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, 0, '%');
		$this->ArrayEmailProperties['ContentMain']		= PerformGlobalCampaignPersonalization($this->ArrayEmailProperties['ContentMain'], $this->ArrayContent['ContentType'], 'Main', $this->ArrayConfig, $this->ArrayLanguage, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, 0, '%');
		$this->ArrayEmailProperties['ContentAlternate']	= PerformGlobalCampaignPersonalization($this->ArrayEmailProperties['ContentAlternate'], $this->ArrayContent['ContentType'], 'Alternate', $this->ArrayConfig, $this->ArrayLanguage, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, 0, '%');

		$this->ObjectBenchmark->Calculate("Global personalization completed");
		// Perform global personalization on email content - FINISHED

		// Initialize variables - STARTED
		$this->BenchmarkStartTime		= time();
		$this->TotalSentMembers			= ($this->IsResumed == true ? $this->ArrayCampaignStatistics['SentRecipients'] : 0);
		$this->TotalFailedMembers		= ($this->IsResumed == true ? $this->ArrayCampaignStatistics['FailedRecipients'] : 0);
		// Initialize variables - FINISHED

		// Prepare email object - STARTED
		$this->_PrepareEmailObject();
		// Prepare email object - FINISHED

		// Loop each interval (local mail lists) - STARTED
		$this->_LoopEachIntervalLocal();
		// Loop each interval (local mail lists) - FINISHED

		// Loop each interval (outsourced mail lists) - STARTED
		if ($this->TotalOutsourcedRecipients > 0)
			{
			$this->_LoopEachIntervalOutsourced();
			}
		// Loop each interval (outsourced mail lists) - FINISHED

		// Record the send process finish time - STARTED
		$this->BenchmarkFinishTime		=	time();
		// Record the send process finish time - FINISHED

		// Update campaign statistics - STARTED
			$ArrayFieldnValues = array(
									"CampaignStatisticsID"					=>	$this->ArrayCampaignStatistics['CampaignStatisticsID'],
									"RelCampaignID"							=>	$this->ArrayCampaignStatistics['RelCampaignID'],
									"Status"								=>	($this->ProcessStopped == false ? 'Completed' : $this->ArrayCampaignStatistics['Status']),
									"StatusDescription"						=>	($this->ProcessStopped == false ? 'Completed' : $this->ArrayCampaignStatistics['StatusDescription']),
									"TotalRecipients"						=>	($this->IsResumed == true ? $this->ArrayCampaignStatistics['TotalRecipients'] : $this->TotalRecipients),
									"SentRecipients"						=>	$this->ArrayCampaignStatistics['SentRecipients'],
									"FailedRecipients"						=>	$this->ArrayCampaignStatistics['FailedRecipients'],
									"SendStartDateTime"						=>	$this->ArrayCampaignStatistics['SendStartDateTime'],
									"SendFinishDateTime"					=>	date('Y-m-d H:i:s', $this->BenchmarkFinishTime),
									"SendLastActionDateTime"				=>	date('Y-m-d H:i:s', $this->BenchmarkFinishTime),
									"InstantEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['InstantEmailsPerSecond'],
									"AverageEmailsPerSecond"				=>	$this->ArrayCampaignStatistics['AverageEmailsPerSecond'],
									"TotalSentEmailSizeBytes"				=>	$this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'],
									"QueuePointer"							=>	$this->ArrayCampaignStatistics['QueuePointer'],
									"QueuePointerOutsource"					=>	$this->ArrayCampaignStatistics['QueuePointerOutsource'],
									"QueuePointerRelOutsourcedMailListID"	=>	$this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID'],
									"Sleeping"					=>	0,	
									);
		$this->_UpdateCampaignStatistics($ArrayFieldnValues, $EachCampaignID, $this->ArrayCampaignStatistics['CampaignStatisticsID'], false);
		// Update campaign statistics - FINISHED

		// Send report to administrator and clients - STARTED
		$this->_SendFinalReport();
		$this->ObjectBenchmark->Calculate("Campaign report is sent to administrators and clients");
		// Send report to administrator and clients - FINISHED

		// Update the Instance amount of the campaign (if recursive campaign) - STARTED
		$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule SET ScheduleSendInstance = ScheduleSendInstance + 1 WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$this->ObjectBenchmark->Calculate("Instance amount of the campaign is updated (if it is a recursive campaign)");
		// Update the Instance amount of the campaign (if recursive campaign) - FINISHED

		$this->ObjectBenchmark->Calculate("Each campaign loop finished. Campaign ID #".$EachCampaignID);
		}
	 // end of foreach

print "3";

	}



//-----------------------------------------------------------
// Prepare email object
function _PrepareEmailObject()
	{
	// Define send engine - STARTED
	if ($this->ArrayEmailProperties['SendEngines']['Type'] == 'Sendmail')
		{
		include_once($this->ArrayConfig['Paths']['Software']."/system/classes/Swift/Connection/Sendmail.php");
		$this->ObjectMailer 			= new Swift(new Swift_Connection_Sendmail($this->ArrayConfig['Settings']['SendmailPath']));
		}
	elseif ($this->ArrayEmailProperties['SendEngines']['Type'] == 'QMail')
		{
		include_once($this->ArrayConfig['Paths']['Software']."/system/classes/Swift/Connection/Sendmail.php");
		$this->ObjectMailer 			= new Swift(new Swift_Connection_Sendmail($this->ArrayConfig['Settings']['SendmailPath']));
		}
	elseif ($this->ArrayEmailProperties['SendEngines']['Type'] == 'MTA')
		{
		include_once($this->ArrayConfig['Paths']['Software']."/system/classes/Swift/Connection/NativeMail.php");
		$this->ObjectMailer 			= new Swift(new Swift_Connection_NativeMail());
		}
	elseif ($this->ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
		{
		include_once($this->ArrayConfig['Paths']['Software']."/system/classes/Swift/Connection/RawEmail.php");
		$this->ObjectMailer 			= new Swift(new Swift_Connection_RawEmailFile());
		}
	elseif ($this->ArrayEmailProperties['SendEngines'][0]['Type'] == 'SMTP')
		{
		// Learn which SMTP server to process - STARTED
		$SMTPIndex = $this->ArraySMTPProcessLog['WhichSMTP'];
		if ($SMTPIndex == '')
			{
			$SMTPIndex = 0;
			$this->ArraySMTPProcessLog['WhichSMTP'] = 0;
			}
		// Learn which SMTP server to process - FINISHED

		// SMTP server load balancing - STARTED
		if ($this->TotalLoopedMembers % $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['MaxEmailsPerInterval'] == 0)
			{
			// Wait for the defined seconds - STARTED
			if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval'] != 0)
				{
				sleep($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval']);
				}
			// Wait for the defined seconds - FINISHED

			// Proceed to next SMTP server (if exists) - STARTED
			if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex + 1]['Type'] == 'SMTP')
				{
				$SMTPIndex = $SMTPIndex + 1;
				$this->ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			else
				{
				$SMTPIndex = 0;
				$this->ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			// Proceed to next SMTP server (if exists) - FINISHED
			}
		// SMTP server load balancing - FINISHED

		include_once($this->ArrayConfig['Paths']['Software']."/system/classes/Swift/Connection/SMTP.php");
		$this->ObjectMailer 			= new Swift(new Swift_Connection_SMTP($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Host'], $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Port']));
		if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Auth'] == 'Authorization')
			{
			$this->ObjectMailer->authenticate($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Username'], $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Password']);
			}
		}

	$this->ObjectBenchmark->Calculate("              Mailer object created");
	// Define send engine - FINISHED

	// Set email addresses - STARTED
	$this->ObjectMailer->setReturnPath($this->ArrayEmailProperties['ReturnPathEmail']);
	$this->ObjectMailer->setReplyTo('"'.$this->ArrayEmailProperties['ReplyToName'].'" <'.$this->ArrayEmailProperties['ReplyToEmail'].'>');
	// Set email addresses - FINISHED

	// Set email properties - STARTED
	$this->ObjectMailer->autoFlush(false);
	$this->ObjectMailer->setPriority($this->ArrayEmailProperties['Priority']);
	$this->ObjectMailer->setCharset(substr($this->ArrayEmailProperties["CharSet"], 0, strlen($this->ArrayEmailProperties["CharSet"]) - 5));
	// Set email properties - FINISHED

	// If enabled, embed images to the email - STARTED
	if (($this->ArrayEmailProperties["ContentType"] == 'Html') || ($this->ArrayEmailProperties["ContentType"] == 'Both'))
		{
		if ($this->ArrayEmailProperties['EmbedObjects'] == true)
			{
			$this->_EmbedObjects();
			$this->ObjectBenchmark->Calculate("              Images are embedded");
			}
		}
	// If enabled, embed images to the email - FINISHED

	// Insert attachments - STARTED
	foreach ($this->ArrayEmailProperties['Attachments'] as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$this->ArrayConfig['Paths']['Data']."/campaign_attachments/".$this->ArrayEmailProperties['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$this->ObjectMailer->addAttachment(implode('', file($AttachmentPath)), $AttachmentName);

		$this->ObjectBenchmark->Calculate("              Attachments are inserted");
		}
	// Insert attachments - FINISHED

	return;	
	}
	
//-----------------------------------------------------------





// Embeds objects found in the email body to the mailer object (phpmailer)
function _EmbedObjects()
	{
	$ArrayObjects = array();
	
	// src= - STARTED
		$Pattern = "/src\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $this->ArrayEmailProperties['ContentMain'], $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - STARTED
			if (substr($EachMatch[2], 0 , strlen($this->ArrayConfig['URLs']['Software']) + 10) == $this->ArrayConfig['URLs']['Software'].'/read.php?')
				{
				continue;
				}
			else
				{
				$ArrayObjects[] = $EachMatch[2];
				}
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - FINISHED
			}
		}
	// src= - FINISHED

	// background-image:url( - STARTED
		$Pattern = "/background\-image\:url\((.*)\)/iU";
	if (preg_match_all($Pattern, $this->ArrayEmailProperties['ContentMain'], $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayImages[] = $EachMatch[1];
			}
		}
	// background-image:url( - FINISHED

	// background=" - STARTED
		$Pattern = "/background\=(\"|\'|)(.*)(\"|\'|\>)/iU";
	if (preg_match_all($Pattern, $this->ArrayEmailProperties['ContentMain'], $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - FINISHED

	// <link href=" - STARTED
		$Pattern = "/\<link href\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $this->ArrayEmailProperties['ContentMain'], $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// <link href=" - FINISHED

	$ArrayObjects = array_flip(array_flip($ArrayObjects));

	$ArrayParsedURL = parse_url($this->ArrayConfig['URLs']['Software']);
	
	$DocumentRoot	= ($_SERVER['DOCUMENT_ROOT'] == '' ? str_replace("\\", "/", substr($_SERVER['SCRIPT_FILENAME'], 0, strlen($_SERVER['SCRIPT_NAME'])*-1)) : $_SERVER['DOCUMENT_ROOT']);
	$oemProHost		= $_SERVER['HTTP_HOST'];

	foreach ($ArrayObjects as $EachObject)
		{
		$ArrayURLInfo	= parse_url($EachObject);
			$FileHost = $ArrayURLInfo['host'];

		// If the object path is defined starting with '/', add website URL to the beginning - STARTED
		if ($oemProHost == $FileHost)
			{
			if ((substr($ArrayURLInfo['path'], 0, 1) == '/') && (substr($DocumentRoot, strlen($DocumentRoot) - 1, strlen($DocumentRoot)) == '/'))
				{
				$EachObjectRenamed = $DocumentRoot.substr($ArrayURLInfo['path'], 1, strlen($ArrayURLInfo['path']));
				}
			else
				{
				$EachObjectRenamed = $DocumentRoot.$ArrayURLInfo['path'];
				}
			}
		elseif (substr($EachObject, 0, 1) == '/')
			{
			$EachObjectRenamed = $DocumentRoot.$EachObject;
			}
		elseif (substr($EachObject, 0, 2) == './')
			{
			$EachObjectRenamed = $DocumentRoot.substr($EachObject, 1, strlen($EachObject));
			}
		elseif (substr($EachObject, 0, 3) == '../')
			{
			$EachObjectRenamed = $DocumentRoot.substr($EachObject, 2, strlen($EachObject));
			}
		else
			{
			$EachObjectRenamed = $EachObject;
			}
		// If the object path is defined starting with '/', add website URL to the beginning - FINISHED

		$ArrayReplacePattern = array();
		$ArrayReplaceReplace = array();

		$CID = $this->ObjectMailer->addImage($EachObjectRenamed);

		$ArrayReplacePattern[]	=	'src="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'background-image:url('.$EachObject.');';
		$ArrayReplacePattern[]	=	'background="'.$EachObject.'"';
		$ArrayReplacePattern[]	=	'<link href="'.$EachObject.'"';

		$ArrayReplaceReplace[]	=	'src="'.$CID.'"';
		$ArrayReplaceReplace[]	=	'background-image:url('.$CID.');';
		$ArrayReplaceReplace[]	=	'background="'.$CID.'"';
		$ArrayReplaceReplace[]	=	'<link href="'.$CID.'"';

		$this->ArrayEmailProperties['ContentMain'] = str_replace($ArrayReplacePattern, $ArrayReplaceReplace, $this->ArrayEmailProperties['ContentMain']);
		}

	return;
	}
	
//-----------------------------------------------------------
// Calculates total pending members in the temporary MySQL table and re-registers variables 
function _CalculateTMPTablePendingRecipients()
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->TemporaryMySQLTableName." WHERE SendStatus='Pending'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$this->TotalLocalRecipients = $TotalFound['TotalFound'];
	
	return;
	}

//-----------------------------------------------------------
// Send final status to the administrator and assigned clients
function _SendFinalReport()
	{
	// Send report to administrator - STARTED
	if ($this->ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		$ObjectMailer 			= new phpmailer();

		$ObjectMailer->IsMail();

		$ObjectMailer->From		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->FromName	= $this->ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->AddAddress(strtolower($this->ArrayAdministrator['Email']), '');
		$ObjectMailer->AddReplyTo($this->ArrayConfig['Settings']['SystemMailerEmail'], $this->ArrayConfig['Settings']['SystemMailerName']);
		$ObjectMailer->ReturnPath= $this->ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->Sender= $this->ArrayConfig['Settings']['SystemMailerEmail'];

		$ObjectMailer->WordWrap	= 70;
		$ObjectMailer->Priority	= 5;
		$ObjectMailer->XMailer	= $this->ArrayConfig['Settings']['XMailer'];
		$ObjectMailer->CharSet	= $this->ArrayLanguage["Config"]["CharSet"];
		$ObjectMailer->Subject	= $this->ArrayLanguage["Texts"]["00247"];

		$ObjectMailer->IsHTML(false);
		$ObjectMailer->Body		= $this->ArrayLanguage["Texts"]["00248"];

			$ArrayReplaceList = array(
									"%Name%"					=>		$this->ArrayAdministrator['Name'],
									"%Campaign:CampaignName%"	=>		$this->ArrayCampaign['CampaignName'],
									"%TotalRecipients%"			=>		number_format($this->ArrayCampaignStatistics['TotalRecipients']),
									"%SentRecipients%"			=>		number_format($this->ArrayCampaignStatistics['SentRecipients']),
									"%FailedRecipients%"		=>		number_format($this->ArrayCampaignStatistics['FailedRecipients']),
									);
			$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);

		$ObjectMailer->Send();
		}
	// Send report to administrator - FINISHED
	
	// Send report to assigned clients - STARTED
	if ($this->ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		foreach ($this->ArrayClients as $Key => $ArrayEachClient)
			{
			$ObjectMailer 			= new phpmailer();
	
			$ObjectMailer->IsMail();
	
			$ObjectMailer->From		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->FromName	= $this->ArrayConfig['Settings']['SystemMailerName'];
			$ObjectMailer->AddAddress(strtolower($this->ArrayEachClient['Email']), '');
			$ObjectMailer->AddReplyTo($this->ArrayConfig['Settings']['SystemMailerEmail'], $this->ArrayConfig['Settings']['SystemMailerName']);
			$ObjectMailer->ReturnPath= $this->ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->Sender= $this->ArrayConfig['Settings']['SystemMailerEmail'];
	
			$ObjectMailer->WordWrap	= 70;
			$ObjectMailer->Priority	= 5;
			$ObjectMailer->XMailer	= $this->ArrayConfig['Settings']['XMailer'];
			$ObjectMailer->CharSet	= $this->ArrayLanguage["Config"]["CharSet"];
			$ObjectMailer->Subject	= $this->ArrayLanguage["Texts"]["00247"];
	
			$ObjectMailer->IsHTML(false);
			$ObjectMailer->Body		= $this->ArrayLanguage["Texts"]["00248"];
	
				$ArrayReplaceList = array(
										"%Name%"					=>		$ArrayEachClient['Name'],
										"%Campaign:CampaignName%"	=>		$this->ArrayCampaign['CampaignName'],
										"%TotalRecipients%"			=>		number_format($this->ArrayCampaignStatistics['TotalRecipients']),
										"%SentRecipients%"			=>		number_format($this->ArrayCampaignStatistics['SentRecipients']),
										"%FailedRecipients%"		=>		number_format($this->ArrayCampaignStatistics['FailedRecipients']),
										);
				$ObjectMailer->Body = str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->Body);
	
			$ObjectMailer->Send();
			}
		}
	// Send report to assigned clients - FINISHED
	}

//-----------------------------------------------------------
// Loop each interval in temporary recipient MySQL table (Outsourced Mail Lists)
function _LoopEachIntervalOutsourced()
	{
	$OriginalSubject		=	$this->ArrayEmailProperties['Subject'];
	$OriginalBody			=	$this->ArrayEmailProperties['ContentMain'];
	$OriginalAltBody		=	$this->ArrayEmailProperties['ContentAlternate'];

	// Loop each outsourced mail lists and send campaign to their recipients - STARTED
	foreach ($this->ArrayOutsourcedMailLists as $EachOutsourcedMailListID)
		{
		if (($EachOutsourcedMailListID < $this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID']) && ($this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID'] != 0))
			{
			continue;
			}
		
		// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
		if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
			{
			$ProcessStopped = true;
			break;
			}
		else
			{
			$ProcessStopped = false;
			}
		// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED

		// Retrieve mail list information - STARTED
		$SQLQueryMailList = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$EachOutsourcedMailListID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQueryMailList, __LINE__, __FILE__);
			$ArrayOutsourcedMailList = mysql_fetch_assoc($ResultSet);
		// Retrieve mail list information - FINISHED

		// Retrieve send rules - STARTED
		$TMPArraySQLQueryPartSendRules = GenerateSendRulesArray($this->ArrayConfig, $this->ArrayCampaign);
		// Retrieve send rules - FINISHED

		// Find the total number of recipients of this outsourced mail list - STARTED
		$ThisMailListTotalOutsourcedRecipients = FindTotalMemberOfOutsourcedList($ArrayOutsourcedMailList, $this->ArrayConfig, $TMPArraySQLQueryPartSendRules);
			$OriginalThisMailListTotalOutsourcedRecipients = $ThisMailListTotalOutsourcedRecipients;

		if (($this->IsResumed == true) && ($this->ArrayCampaignStatistics['QueuePointerOutsource'] > 0))
			{
			$ThisMailListTotalOutsourcedRecipients -= $ArrayCampaignStatistics['QueuePointerOutsource'];
			}
		// Find the total number of recipients of this outsourced mail list - FINISHED

		// Mail List recipient send counter initialize - STARTED
		$TMPThisMailListRecipientSendCounter = ($this->IsResumed == false ? 0 : $this->ArrayCampaignStatistics['QueuePointerOutsource']);
		// Mail List recipient send counter initialize - FINISHED

		// Calculate the total interval - STARTED
		$TotalIntervals		= ceil($ThisMailListTotalOutsourcedRecipients / $this->RecipientsPerInterval);
			$TotalIntervals = ($TotalIntervals == 0 ? 1 : $TotalIntervals);
		// Calculate the total interval - FINISHED

		// Each interval - STARTED
		for ($TMPIntervalCounter = 1; $TMPIntervalCounter <= $TotalIntervals; $TMPIntervalCounter++)
			{
			// Initialize interval variables - STARTED
			$this->TotalIntervalLoopedMembers		= 0;
			$this->BechmarkIntervalStartTime		= time();		
			// Initialize interval variables - FINISHED

			// If this is the last loop, simply check total recipients again and adjust total intervals - STARTED
			if ($TMPIntervalCounter == $this->TotalIntervals)
				{
				$ResultSetOverAllTotal = ExecuteMySQLQuery($this->TotalRecipientsSQLQuery, __LINE__, __FILE__);
				$OverAllTotalRecords = mysql_fetch_assoc($ResultSetOverAllTotal);
					$OverAllTotalRecords = $OverAllTotalRecords['TotalFound'];
	
				if ($OverAllTotalRecords > $this->TotalOverAllRecipients)
					{
					$this->_GenerateRecipientSQLQuery();
					
					$NewRecipients = $OverAllTotalRecords - $this->TotalOverAllRecipients;
					$this->TotalIntervals		+= ceil($NewRecipients / $this->RecipientsPerInterval);
	
					$this->TotalOverAllRecipients = $OverAllTotalRecords;
					
					$ArrayFieldnValues = array(
											"TotalRecipients= '".$this->TotalOverAllRecipients."'",
											);
			
					$TMPSQLQueryPart = implode(", ", $ArrayFieldnValues);
	
					$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET ".$TMPSQLQueryPart." WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
					$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
					}
				}
			// If this is the last loop, simply check total recipients again and adjust total intervals - FINISHED
	
			// If total number of outsourced recipients is 0, then skip sending - STARTED
			if ($this->TotalOutsourcedRecipients == 0)
				{
				$this->ObjectBenchmark->Calculate("Number of local recipients is 0, skipping sending");
	
				break;
				}
			// If total number of outsourced recipients is 0, then skip sending - FINISHED

			// If total number of outsourced recipients is 0, then skip sending - STARTED
			if (($this->TotalOutsourcedRecipients == 0) || ($ThisMailListTotalOutsourcedRecipients == 0))
				{
				break;
				}
			// If total number of outsourced recipients is 0, then skip sending - FINISHED

			// Connect to outsource database and generate the SQL query for retrieving recipients - STARTED
				$StartFrom = $TMPThisMailListRecipientSendCounter;

			if ($ArrayOutsourcedMailList['OutSourceDBQueryType'] == 'Basic')
				{
				$TMPSQLQuery = $this->_GenerateOutsourceSQLQuery($ArrayOutsourcedMailList, $TMPArraySQLQueryPartSendRules)." LIMIT ".$StartFrom.",".$this->RecipientsPerInterval;
				}
			else
				{
				$TMPSQLQuery = $this->_GenerateOutsourceSQLQuery($ArrayOutsourcedMailList, $TMPArraySQLQueryPartSendRules);
				}

			mysql_close();

			$MySQLLink = mysql_connect($ArrayOutsourcedMailList['OutSourceDBHost'], $ArrayOutsourcedMailList['OutSourceDBUsername'], $ArrayOutsourcedMailList['OutSourceDBPassword']);
				mysql_select_db($ArrayOutsourcedMailList['OutSourceDBName']);

			$TMPResultSet = ExecuteMySQLQuery($TMPSQLQuery, __LINE__, __FILE__);

			mysql_close();
		
			$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);
			// Connect to outsource database and generate the SQL query for retrieving recipients - FINISHED

			// Avoid time-out - STARTED
			print(" ");
	
			$this->ObjectBenchmark->Calculate("       Timeout avoided");
			// Avoid time-out - FINISHED

			// If debugging is disabled, reset debug array to avoid memory overload - STARTED
			if ($this->ArrayConfig['Settings']['SystemBenchmarkMode'] == 'Disabled')
				{
				unset($this->ObjectBenchmark->RPTTimes);
				}
			// If debugging is disabled, reset debug array to avoid memory overload - FINISHED

			// Send to each recipient in the interval - STARTED
			while ($EachMember = mysql_fetch_assoc($TMPResultSet))
				{
				$TMPThisMailListRecipientSendCounter++;

				// Load balancing - STARTED
				if ($this->ArrayConfig['Settings']['LoadBalanceEnabled'] == 'Enabled')
					{
					if ($this->LoadBalanceLoopedMembers % $this->ArrayConfig['Settings']['LoadBalanceIntervalAmount'] == 0)
						{
						sleep($this->ArrayConfig['Settings']['LoadBalanceDelaySecondsPerInterval']);
	
						$this->ObjectBenchmark->Calculate("       Load balancing applied for ".$this->ArrayConfig['Settings']['LoadBalanceDelaySecondsPerInterval']." seconds");
						}
					}
				// Load balancing - FINISHED

				// For outsourced members, the email address field may be different. Make a new field with "Email" name - STARTED
				if ($ArrayOutsourcedMailList['OutSourceDBQueryType'] == 'Basic')
					{
					$EachMember['Email']	=	$EachMember[$ArrayOutsourcedMailList['OutSourceDBEmailFieldName']];
					}
				else
					{
					$EachMember['Email']	=	$EachMember[$ArrayOutsourcedMailList['OutSourceDBFullSQLQueryEmailField']];
					}
				// For outsourced members, the email address field may be different. Make a new field with "Email" name - FINISHED
	
				// Process on email content (personalization, tracking links, tracking reads) - STARTED
				$TMPSubject				=	$OriginalSubject;
				
					// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - STARTED
					if (strtolower(gettype($TMPSubject)) == 'array')
						{
						$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
						}
					// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - FINISHED
					
				$TMPContentMain			=	$OriginalBody;
				$TMPContentAlternate	=	$OriginalAltBody;
	
				if ($this->ArrayContent['Personalize'] == 'Yes')
					{
						$ArrayPrefixes		=	array(
													"Sender",
													"Member",
													"Membership",
													"Campaign"
													);
						$ArrayInformation	=	array(
													$ArrayAdministrator,
													$EachMember,
													$EachMember,
													$this->ArrayCampaign,
													);											
					$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
					$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
					$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
					}
	
				$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, true, '%');
				$TMPContentMain			=	PersonalizeCampaignTags($TMPContentMain, $this->ArrayContent['ContentType'], 'Main', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');
				$TMPContentAlternate	=	PersonalizeCampaignTags($TMPContentAlternate, $this->ArrayContent['ContentType'], 'Alternate', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');
	
				$this->ObjectBenchmark->Calculate("       Email content personalized");
				// Process on email content (personalization, tracking links, tracking reads) - FINISHED
	
				// Remove all unwanted HTML codes from email body - STARTED
				if (($this->ArrayContent['ContentType'] == 'Html') || ($this->ArrayContent['ContentType'] == 'Both'))
					{
					$TMPContentMain = RemoveUnwantedHTMLCodes($TMPContentMain, $this->ArrayConfig);
					}
	
				$this->ObjectBenchmark->Calculate("       Unwanted HTML codes are removed from email body");
				// Remove all unwanted HTML codes from email body - FINISHED

				// If debug mode is enabled, display the output - STARTED
				if ($this->ArrayConfig['Settings']['SystemDebugMode'] == 'Enabled')
					{
					print ("Sent to - ".$EachMember['MemberID']." - ".$EachMember['Email']."<br>\n");
	
					$this->ObjectBenchmark->Calculate("       Debug mode is enabled: Output is displayed");
					}
				// If debug mode is enabled, display the output - FINISHED
	
				// Check if member is banned or black listed - STARTED
				$IsSent		= false;
				$IsFailed	= false;
				
				if ($this->_IsMemberBannedOrBlackListed($EachMember) == false)
					{
					$this->ObjectBenchmark->Calculate("       Member is not banned or black listed");
	
					// Send email - STARTED
					$ArrayError = $this->_SendEmail($TMPSubject, $TMPContentMain, $TMPContentAlternate, $EachMember);
					// Send email - FINISHED
	
					$this->ObjectBenchmark->Calculate("       Email sent to member email address");
	
					// Check any error occured while sending - STARTED
					if ($ArrayError[0] == true)
						{
						// Error exists, save error to the campaign log file - STARTED
						$SQLQueryErrorLog = "INSERT INTO ".$this->TemporaryMySQLTableName." (ID, SendStatus, SendStatusDescription, MemberID, Email, Source) VALUES ('', 'Failed', '".$ArrayError[1]."', '".$EachMember['MemberID']."', '".$EachMember['Email']."', 'Outsourced')"; 
						$ResultSetErrorLog = ExecuteMySQLQuery($SQLQueryErrorLog, __LINE__, __FILE__);
						// Error exists, save error to the campaign log file - FINISHED
	
						$this->ObjectBenchmark->Calculate("       Error occured while trying to send. Error log file is being updated");
						
						$this->TotalFailedMembers++;
						$IsFailed = true;
						}
					else
						{
						$this->ObjectBenchmark->Calculate("       Email is sent to the member without any problem");
	
						$this->TotalSentMembers++;
						$IsSent = true;
						}
					// Check any error occured while sending - FINISHED
					}
				else
					{
					$this->ObjectBenchmark->Calculate("       Member is banned or black listed");
	
					// Error exists, save error to the campaign log file - STARTED
						$ErrorLog = "Member is banned or black listed!";
					$SQLQueryErrorLog = "INSERT INTO ".$this->TemporaryMySQLTableName." (ID, SendStatus, SendStatusDescription, MemberID, Email, Source) VALUES ('', 'Failed', '".$ErrorLog."', '".$EachMember['MemberID']."', '".$EachMember['Email']."', 'Outsourced')"; 
					$ResultSetErrorLog = ExecuteMySQLQuery($SQLQueryErrorLog, __LINE__, __FILE__);
					// Error exists, save error to the campaign log file - FINISHED
					
					$this->ObjectBenchmark->Calculate("       Banned or black listed member is recorded into error file");
	
					$this->TotalFailedMembers++;
					$IsFailed = true;
					}
				// Check if member is banned or black listed - FINISHED
	
				// Run external plug-in processes - STARTED
					$SendCompleted = ($ArrayError[0] == true ? false : true);
				$ArrayPlugInProcessResults = RunPlugInProcesses($SendCompleted, $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics);
				// Run external plug-in processes - FINISHED
	
				$this->ObjectBenchmark->Calculate("       External plug-in processes are executed (if any)");
	
				// Check external plug-in process return message. If it is a command, process it - STARTED
				foreach ($ArrayPlugInProcessResults as $PlugInKey=>$ArrayPlugInResults)
					{
					if (strtolower($ArrayPlugInResults['Command']) == 'stop')
						{
						$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Cancelled' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
						$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
						}
					}
				// Check external plug-in process return message. If it is a command, process it - FINISHED
	
				$this->ObjectBenchmark->Calculate("       External plug-in commands are processed (if any)");
	
				// Check quota and stop sending process is there is no available quota or sending time limit - STARTED
				if ($this->_CheckQuotaAndTimeLimit() == false)
					{
					$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Paused' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					}
				else
					{
					// Update quota - STARTED
					$this->_UpdateQuotaUsage(1);
					// Update quota - FINISHED
					}
				// Check quota and stop sending process is there is no available quota or sending time limit - FINISHED
	
				$this->ObjectBenchmark->Calculate("       Administrator quota is checked and updated");
	
				// Update campaign statistics - STARTED
				$LastActionDateTime = date("Y-m-d H:i:s");
				$ArrayFieldnValues = array(
										"SendLastActionDateTime = '".$LastActionDateTime."'",
										"InstantEmailsPerSecond = '".$this->BenchmarkInsEmailsSec."'",
										"AverageEmailsPerSecond = '".$this->BenchmarkAvgEmailsSec."'",
										"TotalSentEmailSizeBytes = '0'",
										"QueuePointerOutsource = '".($OriginalThisMailListTotalOutsourcedRecipients == $TMPThisMailListRecipientSendCounter ? 0 : $TMPThisMailListRecipientSendCounter)."'",
										"QueuePointerRelOutsourcedMailListID = '".($OriginalThisMailListTotalOutsourcedRecipients == $TMPThisMailListRecipientSendCounter ? 0 : $EachOutsourcedMailListID)."'",
										);
				$ArrayFieldnValues[]	= ($IsSent == true ? 'SentRecipients = SentRecipients + 1' : 'SentRecipients = SentRecipients');
				$ArrayFieldnValues[]	= ($IsFailed == true ? 'FailedRecipients = FailedRecipients + 1' : 'FailedRecipients = FailedRecipients');
	
				$this->_UpdateCampaignStatistics($ArrayFieldnValues, $this->ArrayCampaign['CampaignID'], $this->ArrayCampaignStatistics['CampaignStatisticsID'], true);
				// Update campaign statistics - FINISHED
	
				$this->ObjectBenchmark->Calculate("       Campaign statistics are updated");
	
				// Check process command. If sending is stopped, paused or cancelled, do not continue - STARTED
				if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
					{
					$this->ProcessStopped = true;
					break;
					}
				else
					{
					$this->ProcessStopped = false;
					}
				// Check process command. If sending is stopped, paused or cancelled, do not continue - FINISHED
	
				// Calculate overall bechmark (average) - STARTED
				$this->BenchmarkAvgEmailsSec = round(($this->TotalLoopedMembers / (time() - $this->BenchmarkStartTime)));
				// Calculate overall bechmark (average) - FINISHED
	
				$this->ObjectBenchmark->Calculate("       Send performance benchmark values are updated");
	
				$this->TotalLoopedMembers++;
				$this->TotalIntervalLoopedMembers++;
				$this->LoadBalanceLoopedMembers++;
	
				$this->ObjectBenchmark->Calculate("End of sending to Member ID #".$EachMember['MemberID']." - ".$EachMember['Email']);			
				}
			// Send to each recipient in the interval - FINISHED

			usleep($this->DelayMicroSecondsPerInterval);
	
			$this->ObjectBenchmark->Calculate("Sleep for ".$this->DelayMicroSecondsPerInterval." micro seconds");
	
			// Calculate per interval (instant) email sending performance - STARTED
			$this->BenchmarkInsEmailsSec = round(($this->TotalIntervalLoopedMembers / (time() - $this->BechmarkIntervalStartTime)));
			// Calculate per interval (instant) email sending performance - FINISHED
	
			// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
			if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
				{
				$this->ProcessStopped = true;
				break;
				}
			else
				{
				$this->ProcessStopped = false;
				}
			// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED
	
			$this->ObjectBenchmark->Calculate("End of interval loop #".$TMPIntervalCounter);
			}
		// Each interval - FINISHED
		}
	// Loop each outsourced mail lists and send campaign to their recipients - FINISHED

	return;	
	}
		
//-----------------------------------------------------------
// Loop each interval in temporary recipient MySQL table (Local Mail Lists)
function _LoopEachIntervalLocal()
	{
	$OriginalSubject		=	$this->ArrayEmailProperties['Subject'];
	$OriginalBody			=	$this->ArrayEmailProperties['ContentMain'];
	$OriginalAltBody		=	$this->ArrayEmailProperties['ContentAlternate'];

	for ($TMPIntervalCounter = 1; $TMPIntervalCounter <= $this->TotalIntervals; $TMPIntervalCounter++)
		{
		// Initialize interval variables - STARTED
		$this->TotalIntervalLoopedMembers		= 0;
		$this->BechmarkIntervalStartTime		= time();		
		// Initialize interval variables - FINISHED

		// If this is the last loop, simply check total recipients again and adjust total intervals - STARTED
		if ($TMPIntervalCounter == $this->TotalIntervals)
			{
			$ResultSetOverAllTotal = ExecuteMySQLQuery($this->TotalRecipientsSQLQuery, __LINE__, __FILE__);
			$OverAllTotalRecords = mysql_fetch_assoc($ResultSetOverAllTotal);
				$OverAllTotalRecords = $OverAllTotalRecords['TotalFound'];

			if ($OverAllTotalRecords > $this->TotalOverAllRecipients)
				{
				$this->_GenerateRecipientSQLQuery();
				
				$NewRecipients = $OverAllTotalRecords - $this->TotalOverAllRecipients;
				$this->TotalIntervals		+= ceil($NewRecipients / $this->RecipientsPerInterval);

				$this->TotalOverAllRecipients = $OverAllTotalRecords;
				
				$ArrayFieldnValues = array(
										"TotalRecipients= '".$this->TotalOverAllRecipients."'",
										);
		
				$TMPSQLQueryPart = implode(", ", $ArrayFieldnValues);

				$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET ".$TMPSQLQueryPart." WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

				$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
				}
			}
		// If this is the last loop, simply check total recipients again and adjust total intervals - FINISHED

		// If total number of local recipients is 0, then skip sending - STARTED
		if ($this->TotalLocalRecipients == 0)
			{
			$this->ObjectBenchmark->Calculate("Number of local recipients is 0, skipping sending");

			break;
			}
		// If total number of local recipients is 0, then skip sending - FINISHED

			$StartFrom = ($TMPIntervalCounter - 1) * $this->RecipientsPerInterval;
		$TMPSQLQuery = $this->RecipientSQLQuery." LIMIT ".$StartFrom.",".$this->RecipientsPerInterval;
		$TMPResultSet = ExecuteMySQLQuery($TMPSQLQuery, __LINE__, __FILE__);

		$this->ObjectBenchmark->Calculate("SQL query of members for this interval is prepared");

		// Avoid time-out - STARTED
		print(" ");

		$this->ObjectBenchmark->Calculate("       Timeout avoided");
		// Avoid time-out - FINISHED

		// If debugging is disabled, reset debug array to avoid memory overload - STARTED
		if ($this->ArrayConfig['Settings']['SystemBenchmarkMode'] == 'Disabled')
			{
			unset($this->ObjectBenchmark->RPTTimes);
			}
		// If debugging is disabled, reset debug array to avoid memory overload - FINISHED

		// Send to each recipient in the interval - STARTED
		while ($EachMember = mysql_fetch_assoc($TMPResultSet))
			{
			$this->ObjectBenchmark->Calculate("Sending to Member ID #".$EachMember['MemberID']." - ".$EachMember['Email']);

			// Load balancing - STARTED
			if ($this->ArrayConfig['Settings']['LoadBalanceEnabled'] == 'Enabled')
				{
				if ($this->LoadBalanceLoopedMembers % $this->ArrayConfig['Settings']['LoadBalanceIntervalAmount'] == 0)
					{
					sleep($this->ArrayConfig['Settings']['LoadBalanceDelaySecondsPerInterval']);

					$this->ObjectBenchmark->Calculate("       Load balancing applied for ".$this->ArrayConfig['Settings']['LoadBalanceDelaySecondsPerInterval']." seconds");
					}
				}
			// Load balancing - FINISHED

			// Process on email content (personalization, tracking links, tracking reads) - STARTED
			$TMPSubject				=	$OriginalSubject;
			
				// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - STARTED
				if (strtolower(gettype($TMPSubject)) == 'array')
					{
					$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
					}
				// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - FINISHED
				
			$TMPContentMain			=	$OriginalBody;
			$TMPContentAlternate	=	$OriginalAltBody;

			if ($this->ArrayContent['Personalize'] == 'Yes')
				{
					$ArrayPrefixes		=	array(
												"Sender",
												"Member",
												"Membership",
												"Campaign"
												);
					$ArrayInformation	=	array(
												$ArrayAdministrator,
												$EachMember,
												$EachMember,
												$this->ArrayCampaign,
												);											
				$TMPSubject				=	Personalize($TMPSubject, $ArrayPrefixes, $ArrayInformation, '%');
				$TMPContentMain			=	Personalize($TMPContentMain, $ArrayPrefixes, $ArrayInformation, '%');
				$TMPContentAlternate	=	Personalize($TMPContentAlternate, $ArrayPrefixes, $ArrayInformation, '%');
				}

			$TMPSubject				=	PersonalizeCampaignTags($TMPSubject, $TMPContentType, 'Subject', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, true, '%');
			$TMPContentMain			=	PersonalizeCampaignTags($TMPContentMain, $this->ArrayContent['ContentType'], 'Main', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');
			$TMPContentAlternate	=	PersonalizeCampaignTags($TMPContentAlternate, $this->ArrayContent['ContentType'], 'Alternate', $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');

			$this->ObjectBenchmark->Calculate("       Email content personalized");
			// Process on email content (personalization, tracking links, tracking reads) - FINISHED

			// Remove all unwanted HTML codes from email body - STARTED
			if (($this->ArrayContent['ContentType'] == 'Html') || ($this->ArrayContent['ContentType'] == 'Both'))
				{
				$TMPContentMain = RemoveUnwantedHTMLCodes($TMPContentMain, $this->ArrayConfig);
				}

			$this->ObjectBenchmark->Calculate("       Unwanted HTML codes are removed from email body");
			// Remove all unwanted HTML codes from email body - FINISHED

			// If debug mode is enabled, display the output - STARTED
			if ($this->ArrayConfig['Settings']['SystemDebugMode'] == 'Enabled')
				{
				print ("Sent to - ".$EachMember['MemberID']." - ".$EachMember['Email']."<br>\n");

				$this->ObjectBenchmark->Calculate("       Debug mode is enabled: Output is displayed");
				}
			// If debug mode is enabled, display the output - FINISHED

			// Check if member is banned or black listed - STARTED
			$IsSent		= false;
			$IsFailed	= false;
			
			if ($this->_IsMemberBannedOrBlackListed($EachMember) == false)
				{
				$this->ObjectBenchmark->Calculate("       Member is not banned or black listed");

				// Send email - STARTED
				$ArrayError = $this->_SendEmail($TMPSubject, $TMPContentMain, $TMPContentAlternate, $EachMember);
				// Send email - FINISHED

				$this->ObjectBenchmark->Calculate("       Email sent to member email address");

				// Check any error occured while sending - STARTED
				if ($ArrayError[0] == true)
					{
					// Error exists, save error to the campaign log file - STARTED
					$SQLQueryErrorLog = "INSERT INTO ".$this->TemporaryMySQLTableName." (ID, SendStatus, SendStatusDescription, MemberID, Email, Source) VALUES ('', 'Failed', '".$ArrayError[1]."', '".$EachMember['MemberID']."', '".$EachMember['Email']."', 'Local')"; 
					$ResultSetErrorLog = ExecuteMySQLQuery($SQLQueryErrorLog, __LINE__, __FILE__);
					// Error exists, save error to the campaign log file - FINISHED

					$this->ObjectBenchmark->Calculate("       Error occured while trying to send. Error log file is being updated");
					
					$this->TotalFailedMembers++;
					$IsFailed = true;
					}
				else
					{
					$this->ObjectBenchmark->Calculate("       Email is sent to the member without any problem");

					$this->TotalSentMembers++;
					$IsSent = true;
					}
				// Check any error occured while sending - FINISHED
				}
			else
				{
				$this->ObjectBenchmark->Calculate("       Member is banned or black listed");

				// Error exists, save error to the campaign log file - STARTED
					$ErrorLog = "Member is banned or black listed!";
				$SQLQueryErrorLog = "INSERT INTO ".$this->TemporaryMySQLTableName." (ID, SendStatus, SendStatusDescription, MemberID, Email, Source) VALUES ('', 'Failed', '".$ErrorLog."', '".$EachMember['MemberID']."', '".$EachMember['Email']."', 'Local')"; 
				$ResultSetErrorLog = ExecuteMySQLQuery($SQLQueryErrorLog, __LINE__, __FILE__);
				// Error exists, save error to the campaign log file - FINISHED
				
				$this->ObjectBenchmark->Calculate("       Banned or black listed member is recorded into error file");

				$this->TotalFailedMembers++;
				$IsFailed = true;
				}
			// Check if member is banned or black listed - FINISHED

			// Run external plug-in processes - STARTED
				$SendCompleted = ($ArrayError[0] == true ? false : true);
			$ArrayPlugInProcessResults = RunPlugInProcesses($SendCompleted, $this->ArrayConfig, $this->ArrayLanguage, $EachMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics);
			// Run external plug-in processes - FINISHED

			$this->ObjectBenchmark->Calculate("       External plug-in processes are executed (if any)");

			// Check external plug-in process return message. If it is a command, process it - STARTED
			foreach ($ArrayPlugInProcessResults as $PlugInKey=>$ArrayPlugInResults)
				{
				if (strtolower($ArrayPlugInResults['Command']) == 'stop')
					{
					$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Cancelled' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					}
				}
			// Check external plug-in process return message. If it is a command, process it - FINISHED

			$this->ObjectBenchmark->Calculate("       External plug-in commands are processed (if any)");

			// Check quota and stop sending process is there is no available quota or sending time limit - STARTED
			if ($this->_CheckQuotaAndTimeLimit() == false)
				{
				$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Paused' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaignStatistics['RelCampaignID']."'";
				$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				}
			else
				{
				// Update quota - STARTED
				$this->_UpdateQuotaUsage(1);
				// Update quota - FINISHED
				}
			// Check quota and stop sending process is there is no available quota or sending time limit - FINISHED

			$this->ObjectBenchmark->Calculate("       Administrator quota is checked and updated");

			// Update campaign statistics - STARTED
			$LastActionDateTime = date("Y-m-d H:i:s");
			$ArrayFieldnValues = array(
									"SendLastActionDateTime = '".$LastActionDateTime."'",
									"InstantEmailsPerSecond = '".$this->BenchmarkInsEmailsSec."'",
									"AverageEmailsPerSecond = '".$this->BenchmarkAvgEmailsSec."'",
									"TotalSentEmailSizeBytes = '0'",
									"QueuePointer = '".$EachMember['MemberID']."'",
									);
			$ArrayFieldnValues[]	= ($IsSent == true ? 'SentRecipients = SentRecipients + 1' : 'SentRecipients = SentRecipients');
			$ArrayFieldnValues[]	= ($IsFailed == true ? 'FailedRecipients = FailedRecipients + 1' : 'FailedRecipients = FailedRecipients');

			$this->_UpdateCampaignStatistics($ArrayFieldnValues, $this->ArrayCampaign['CampaignID'], $this->ArrayCampaignStatistics['CampaignStatisticsID'], true);
			// Update campaign statistics - FINISHED

			$this->ObjectBenchmark->Calculate("       Campaign statistics are updated");

			// Check process command. If sending is stopped, paused or cancelled, do not continue - STARTED
			if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
				{
				$this->ProcessStopped = true;
				break;
				}
			else
				{
				$this->ProcessStopped = false;
				}
			// Check process command. If sending is stopped, paused or cancelled, do not continue - FINISHED

			// Calculate overall bechmark (average) - STARTED
			$this->BenchmarkAvgEmailsSec = round(($this->TotalLoopedMembers / (time() - $this->BenchmarkStartTime)));
			// Calculate overall bechmark (average) - FINISHED

			$this->ObjectBenchmark->Calculate("       Send performance benchmark values are updated");

			$this->TotalLoopedMembers++;
			$this->TotalIntervalLoopedMembers++;
			$this->LoadBalanceLoopedMembers++;

			$this->ObjectBenchmark->Calculate("End of sending to Member ID #".$EachMember['MemberID']." - ".$EachMember['Email']);			
			} // end while
		// Send to each recipient in the interval - FINISHED

		usleep($this->DelayMicroSecondsPerInterval);

		$this->ObjectBenchmark->Calculate("Sleep for ".$this->DelayMicroSecondsPerInterval." micro seconds");

		// Calculate per interval (instant) email sending performance - STARTED
		$this->BenchmarkInsEmailsSec = round(($this->TotalIntervalLoopedMembers / (time() - $this->BechmarkIntervalStartTime)));
		// Calculate per interval (instant) email sending performance - FINISHED

		// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
		if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
			{
			$this->ProcessStopped = true;
			break;
			}
		else
			{
			$this->ProcessStopped = false;
			}
		// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED

		$this->ObjectBenchmark->Calculate("End of interval loop #".$TMPIntervalCounter);
		} // end for

	return;
	}
	
//-----------------------------------------------------------
// Generate required SQL query for retrieving outsourced members
function _GenerateOutsourceSQLQuery($ArrayMailList, $TMPArraySQLQueryPartSendRules = array())
	{
	if ($ArrayMailList['OutSourceDBQueryType'] == 'Basic')
		{
		$SQLQuery			 = "SELECT * ";
			$SQLQuery		.= "FROM ".$ArrayMailList['OutSourceDBTable']." AS tblMembers ";
	
		// Apply filtering options - STARTED
		$SQLQueryPart = array();
		if ($ArrayMailList['OutSourceDBWhereQuery'] != '')
			{
			$SQLQueryPart[] = $ArrayMailList['OutSourceDBWhereQuery'];
			}
		// Apply filtering options - FINISHED

		// Apply send rules - STARTED
		if (count($SQLQueryPart) > 0)
			{
			$SQLQuery = $SQLQuery." WHERE ".implode(" AND ", $SQLQueryPart)." ";
			$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " AND ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
			}
		else
			{
			$SQLQuery = $SQLQuery." ";
			$SQLQuery .= (count($TMPArraySQLQueryPartSendRules) > 0 ? " WHERE ".implode(" AND ", $TMPArraySQLQueryPartSendRules) : '')." ";
			}
		// Apply send rules - FINISHED
			
		$SQLQuery .= "GROUP BY tblMembers.".$ArrayMailList['OutSourceDBEmailFieldName']." ";
		}
	else
		{
		$SQLQuery		= $ArrayMailList['OutSourceDBFullSQLQuery'];
		}
			
	return $SQLQuery;
	}

//-----------------------------------------------------------
// Update quota usage
function _UpdateQuotaUsage($AddAmount)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_activities WHERE RelAdministratorID='".$this->ArrayAdministrator['AdministratorID']."' AND ActivityHour='".date('h')."' AND ActivityDay='".date('d')."' AND ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayQuota = mysql_fetch_assoc($ResultSet);

	$NewAmount = $ArrayQuota['SendAmount'] + $AddAmount;

	$SQLQuery  = "REPLACE INTO ".$this->ArrayConfig['Database']['Prefix']."administrator_activities ";
	$SQLQuery .= "(`QuotaID`, `RelAdministratorID`, `ActivityHour`, `ActivityDay`, `ActivityWeek`, `ActivityMonth`, `ActivityYear`, `SendAmount`) ";
	$SQLQuery .= "VALUES ('".$ArrayQuota['QuotaID']."', '".$this->ArrayAdministrator['AdministratorID']."', '".date('h')."', ".date('d').", ".date('W').", ".date('m').", ".date('Y').", ".$NewAmount.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;	
	}
//-----------------------------------------------------------
// Checks if there is enough sending quota and time limit for this process
function _CheckQuotaAndTimeLimit()
	{
	$IsAllowedQuota		= false;
	$IsAllowedTimeLimit = false;

	// Check available send amount limit - STARTED
	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_limits AS tblAdminLimits WHERE RelAdministratorID='".$this->ArrayAdministrator['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdminLimits = mysql_fetch_assoc($ResultSet);

	if ($ArrayAdminLimits['Limit'] != 'Unlimited')
		{
		$QuotaUsed = CalculateSendLimits($this->ArrayAdministrator['AdministratorID'], $ArrayAdminLimits, $this->ArrayConfig);
		$RemainingQuota = $ArrayAdminLimits['LimitAmount'] - $QuotaUsed;
	
		if ($RemainingQuota > 0)
			{
			$IsAllowedQuota = true;
			}
		}
	else
		{
		$IsAllowedQuota = true;
		}
	// Check available send amount limit - FINISHED
		
	// Check available send time limit - STARTED
	$TodayDate	= date('D');
	$TodayTime	= strtotime(date('Y-m-d H:i:s'));
	$AllowedDays = explode(',', $ArrayAdminLimits['TimeLimitDates']);
	
	if ($AllowedDays[0] != '')
		{
		foreach ($AllowedDays as $EachDay)
			{
			if ($TodayDate == $EachDay)
				{
				if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitTo']))))
					{
					$IsAllowedTimeLimit = true;
					break;
					}
				}
			}
		}
	else
		{
		$IsAllowedTimeLimit = true;
		}
	// Check available send time limit - FINISHED

	if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
		{
		return true;
		}
	else
		{
		return false;
		}
	}

//-----------------------------------------------------------
// Sends the email to recipient email address
function _SendEmail($TMPSubject, $TMPContentMain, $TMPContentAlternate, $ArrayMember)
	{
	$this->ObjectBenchmark->Calculate("              SendEmail function started");

	// Initialize variables - STARTED
	$ErrorExists 	= false;
	$ArrayError		= array();
	// Initialize variables - FINISHED

	// If SMTP send engine is selected, cycle selected SMTP servers - STARTED
	if ($this->ArrayEmailProperties['SendEngines'][0]['Type'] == 'SMTP')
		{
		// Learn which SMTP server to process - STARTED
		$SMTPIndex = $this->ArraySMTPProcessLog['WhichSMTP'];
		if ($SMTPIndex == '')
			{
			$SMTPIndex = 0;
			$this->ArraySMTPProcessLog['WhichSMTP'] = 0;
			}
		// Learn which SMTP server to process - FINISHED

		// SMTP server load balancing - STARTED
		if ($this->TotalLoopedMembers % $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['MaxEmailsPerInterval'] == 0)
			{
			// Wait for the defined seconds - STARTED
			if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval'] != 0)
				{
				sleep($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['SecondsDelayPerInterval']);
				}
			// Wait for the defined seconds - FINISHED

			// Proceed to next SMTP server (if exists) - STARTED
			if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex + 1]['Type'] == 'SMTP')
				{
				$SMTPIndex = $SMTPIndex + 1;
				$this->ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			else
				{
				$SMTPIndex = 0;
				$this->ArraySMTPProcessLog['WhichSMTP'] = $SMTPIndex;
				}
			// Proceed to next SMTP server (if exists) - FINISHED
			}
		// SMTP server load balancing - FINISHED

		$this->ObjectMailer->connection = new Swift_Connection_SMTP($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Host'], $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Port']);
		$this->ObjectMailer->connect();
		if ($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Auth'] == 'Authorization')
			{
			$this->ObjectMailer->authenticate($this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Username'], $this->ArrayEmailProperties['SendEngines'][$SMTPIndex]['Password']);
			}

		$this->ObjectBenchmark->Calculate("              SMTP Send engine cycle completed");
		}
	// If SMTP send engine is selected, cycle selected SMTP servers - FINISHED

	// Set email addresses - STARTED
	$this->ObjectMailer->flushTo();
	// Set email addresses - FINISHED

	$this->ObjectBenchmark->Calculate("              Email addresses are set");
	
	// Set email properties - STARTED
	$this->ObjectMailer->flushHeaders();
		$Headers  = '';
		$Headers .= 'X-Mailer: '.$this->ArrayConfig['Settings']['XMailer']."\r\n";
		$Headers .= 'X-Mailer-MsgId: '.base64_encode(strtolower($ArrayMember['Email']))."\r\n";
		$Headers .= 'X-Mailer-CSID: '.base64_encode($this->ArrayEmailProperties['CampaignID'].'_'.$this->ArrayEmailProperties['CampaignStatisticsID']);
	$this->ObjectMailer->addHeaders($Headers);
	// Set email properties - FINISHED

	$this->ObjectBenchmark->Calculate("              Email properties are set");

	// Clear MIME Parts - STARTED
	$this->ObjectMailer->flushParts();
	// Clear MIME Parts - FINISHED

	// Set email body - STARTED
	if ($this->ArrayEmailProperties["ContentType"] == 'Plain')
		{
		$this->ObjectMailer->addPart($TMPContentMain, 'text/plain');
		}
	elseif ($this->ArrayEmailProperties["ContentType"] == 'Html')
		{
		$this->ObjectMailer->addPart($TMPContentMain, 'text/html');
		}
	elseif ($this->ArrayEmailProperties["ContentType"] == 'Both')
		{
		$this->ObjectMailer->addPart($TMPContentMain, 'text/html');
		$this->ObjectMailer->addPart($TMPContentAlternate, 'text/plain');
		}
	// Set email body - FINISHED

	$this->ObjectBenchmark->Calculate("              Email body is set");

	// Send the email - STARTED
	if ($this->ArrayConfig['Settings']['SystemDebugMode'] != 'Enabled')
		{
		$TMPReturn = $this->ObjectMailer->send(strtolower($ArrayMember['Email']), '"'.$this->ArrayEmailProperties['FromName'].'" <'.$this->ArrayEmailProperties['FromEmail'].'>', $TMPSubject);


		// Save the returned raw email to the defined directory - STARTED
		if ($this->ArrayEmailProperties['SendEngines']['Type'] == 'SaveAsFile')
			{
				$TMPFileName = strtotime(date('Y-m-d H:i:s')).rand(1000000, 9999999);
			if (($TMPFileHandler = fopen($this->ArrayConfig['Settings']['SaveRawEmailPath'].$TMPFileName, 'w')) == true)
				{
					fwrite($TMPFileHandler, $this->ObjectMailer->currentMail[3]);
				fclose($TMPFileHandler);
				}
			else
				{
				$ArrayError = array(true, $this->ArrayLanguage["Texts"]["00432"]);
				}
			}
		// Save the returned raw email to the defined directory - FINISHED
		}
	// Send the email - FINISHED

	$this->ObjectBenchmark->Calculate("              Email is sent");

	// Detect if any error occured or not - STARTED
	if ($this->ObjectMailer->hasFailed == true)
		{
		$ArrayError = array(true, implode("\n", $this->ObjectMailer->getErrors()));
		}
	// Detect if any error occured or not - FINISHED

	$this->ObjectBenchmark->Calculate("              End of send email function");

	return $ArrayError;
	}

//-----------------------------------------------------------
// Checks if member is banned or black listed
function _IsMemberBannedOrBlackListed($ArrayMember)
	{
	$EmailAddress	=	$ArrayMember['Email'];
	$EmailDomain	=	explode('@', $EmailAddress);
		$EmailDomain=	"*@".$EmailDomain[1];

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_blacklist WHERE Email='".$EmailAddress."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return true;
		}

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_banned WHERE (Banned='".$EmailAddress."' AND Type='Email address') OR (Banned='".$EmailDomain."' AND Type='Domain')";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];

	if ($TotalFound > 0)
		{
		return true;
		}

	return false;
	}
	
//-----------------------------------------------------------
// Retrieve member data from member table and merge it
function _MergeMemberInformation($MemberID, $ArrayMember)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."members WHERE MemberID='".$MemberID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayAdditionalMember = mysql_fetch_assoc($ResultSet);

	$ArrayMember = array_merge($ArrayMember, $ArrayAdditionalMember);
	
	return $ArrayMember;
	}


//-----------------------------------------------------------
// Copy recipients to temporary MySQL table
function _SaveIntoTemporaryMySQLTable($CampaignID)
	{
	// Initialize variables - STARTED
	$RecipientsPerInterval			=	50;
	$DelayMicroSecondsPerInterval	=	0;
	$TotalIntervals		= ceil($this->TotalLocalRecipients / $RecipientsPerInterval);
		$TotalIntervals = ($TotalIntervals == 0 ? 1 : $TotalIntervals);
	// Initialize variables - FINISHED

	// If there are already members in this temporary table, then skip copying members - STARTED
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->TemporaryMySQLTableName;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
		$TotalFound = $TotalFound['TotalFound'];
	
	if ($TotalFound > 0)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->TemporaryMySQLTableName." WHERE Source='Local'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TotalFound = mysql_fetch_assoc($ResultSet);
			$this->TotalLocalRecipients = $TotalFound['TotalFound'];
			
		return;
		}
	// If there are already members in this temporary table, then skip copying members - FINISHED

	// If total number of local recipients is 0, then skip copying members - STARTED
	if ($this->TotalLocalRecipients == 0)
		{
		return;
		}
	// If total number of local recipients is 0, then skip copying members - FINISHED

	// Copy local member, loop each interval - STARTED
	$TotalLocalRecipients = 0;

	for ($TMPIntervalCounter = 1; $TMPIntervalCounter <= $TotalIntervals; $TMPIntervalCounter++)
		{
			$StartFrom = ($TMPIntervalCounter - 1) * $RecipientsPerInterval;
		$TMPSQLQuery = $this->RecipientSQLQuery." LIMIT ".$StartFrom.",".$RecipientsPerInterval;
		$TMPResultSet = ExecuteMySQLQuery($TMPSQLQuery, __LINE__, __FILE__);

		// Copy each member - STARTED
		while ($EachMember = mysql_fetch_assoc($TMPResultSet))
			{
			$ArrayFieldnValues = array(
									'ID'						=> '',
									'SendStatus'				=> 'Pending',
									'SendStatusDescription'		=> '',
									'MemberID'					=> $EachMember['MemberID'],
									'Email'						=> $EachMember['Email'],
									'Source'					=> 'Local',
									);

			$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
				$FieldList = "`".$FieldList."`";
			$ValueList = implode("','", array_values($ArrayFieldnValues));
				$ValueList ="'".$ValueList."'";
			
			$SQLQuery = "INSERT INTO ".$this->TemporaryMySQLTableName." (".$FieldList.") VALUES (".$ValueList.")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			$TotalLocalRecipients++;
			}
		// Copy each member - FINISHED
		}
	// Copy local member, loop each interval - FINISHED

	$this->TotalLocalRecipients = $TotalLocalRecipients;

	return;
	}
	
//-----------------------------------------------------------
// Generate Recipient SQL Query
function _GenerateRecipientSQLQuery()
	{
	$this->RecipientSQLQuery = '';
	
	// Retrieve mail lists - STARTED
	$this->ArrayMailLists = array();
	
	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayMailLists[]	=	array(
										"MailListID"		=>	$EachRow['MailListID'],
										"SourceType"		=>	$EachRow['SourceType'],
										);
		}
	// Retrieve mail lists - FINISHED

	// Retrieve and prepare recipients (local) - STARTED
	$TMPSQLQuery = array();
	foreach ($this->ArrayMailLists as $ArrayVals)
		{
		$TMPSQLQuery[] = "tblAssign.RelMailListID = '".$ArrayVals['MailListID']."'";
		}
	$TMPSQLQuery = implode(" OR ", $TMPSQLQuery);
	$TMPSQLQuery = "(".$TMPSQLQuery.")";

	// Query for retrieving members - STARTED
	$SQLQuery 			 = "SELECT tblMembers.*, tblAssign.* ";
		$SQLQuery		.= "FROM ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery		.= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQuery		.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";

	// Query for calculating the amount of recipiens - STARTED
	$SQLQueryTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryTotal	.= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of recipiens - FINISHED

	// Query for calculating the amount of ALL recipiens (ignoring resume) - STARTED
	$SQLQueryOverAllTotal 		 = "SELECT COUNT(DISTINCT(tblMembers.MemberID)) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryOverAllTotal	.= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryOverAllTotal	.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Query for calculating the amount of ALL recipiens (ignoring resume) - FINISHED
	
	$SQLQueryPart = array();
		$SQLQueryPart[] = $TMPSQLQuery;
		$SQLQueryPart[] = "tblMembers.BounceType!='Hard bounce'";
		$SQLQueryPart[] = "(tblAssign.SubscriptionStatus='Subscribed' OR tblAssign.SubscriptionStatus='Pending for opt-out confirmation')";
	
		// Resume if paused - STARTED
		if ($this->IsResumed == true)
			{
			$SQLQueryPart[] = "tblMembers.MemberID > '".$this->ArrayCampaignStatistics['QueuePointer']."'";
			}
		// Resume if paused - FINISHED
	
		// Apply sending rules - STARTED
		$TMPArraySQLQueryPartSendRules = array();
		
		$TMPSQLQuerySendRules = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
		$TMPResultSetSendRules = ExecuteMySQLQuery($TMPSQLQuerySendRules, __LINE__, __FILE__);
			$TotalSendRules = mysql_num_rows($TMPResultSetSendRules);
			
		$TMPArraySendRule = array();
		$TMPCounter = 1;
		while ($EachRule = mysql_fetch_assoc($TMPResultSetSendRules))
			{
			$TMPSendRule = "";

			if ($EachRule['Operator1'] == 'equals to')
				{
				$TMPOperator1 = '=';
				}
			elseif ($EachRule['Operator1'] == 'greater than')
				{
				$TMPOperator1 = '>';
				}
			elseif ($EachRule['Operator1'] == 'smaller than')
				{
				$TMPOperator1 = '<';
				}
			elseif ($EachRule['Operator1'] == 'not equals to')
				{
				$TMPOperator1 = '!=';
				}
			elseif ($EachRule['Operator1'] == 'includes')
				{
				$TMPOperator1 = 'LIKE';
				}
			elseif ($EachRule['Operator1'] == 'not includes')
				{
				$TMPOperator1 = 'NOT LIKE';
				}
			else
				{
				$TMPOperator1 = '=';
				}

			if (($TMPOperator1 == 'LIKE') || ($TMPOperator1 == 'NOT LIKE'))
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '%".$EachRule['Value1']."%'";
				}
			else
				{
				$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '".$EachRule['Value1']."'";
				}

			if ($EachRule['Boolean1'] != '')
				{
				if ($EachRule['Operator2'] == 'equals to')
					{
					$TMPOperator2 = '=';
					}
				elseif ($EachRule['Operator2'] == 'greater than')
					{
					$TMPOperator2 = '>';
					}
				elseif ($EachRule['Operator2'] == 'smaller than')
					{
					$TMPOperator2 = '<';
					}
				elseif ($EachRule['Operator2'] == 'not equals to')
					{
					$TMPOperator2 = '!=';
					}
				elseif ($EachRule['Operator2'] == 'includes')
					{
					$TMPOperator2 = 'LIKE';
					}
				elseif ($EachRule['Operator2'] == 'not includes')
					{
					$TMPOperator2 = 'NOT LIKE';
					}
				else
					{
					$TMPOperator2 = '=';
					}

				if (($TMPOperator2 == 'LIKE') || ($TMPOperator2 == 'NOT LIKE'))
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '%".$EachRule['Value2']."%'";
					}
				else
					{
					$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '".$EachRule['Value2']."'";
					}
				}
			
			if ($TotalSendRules > $TMPCounter)
				{
				$TMPSendRule = " (".$TMPSendRule.") ".$EachRule['Boolean2']." ";
				}
			else
				{
				$TMPSendRule = " (".$TMPSendRule.") ";
				}
			
			$TMPCounter++;

			$TMPArraySendRule[] = $TMPSendRule;
			}

		if ($TotalSendRules > 0)
			{
			$TMPSendRule = implode("", $TMPArraySendRule);
				$TMPSendRule = "(".$TMPSendRule.")";

			$SQLQueryPart[] = $TMPSendRule;
			$TMPArraySQLQueryPartSendRules[] = $TMPSendRule;
			}
		// Apply sending rules - FINISHED

	$SQLQuery = $SQLQuery.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
	$SQLQueryTotal = $SQLQueryTotal.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";

	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - STARTED
	if ($IsResumed == true)
		{
			$TMPOverAllSQLQueryPart = $SQLQueryPart;
			$TMPOverAllSQLQueryPart[3] = "tblMembers.MemberID > '0'";
		$SQLQueryOverAllTotal = $SQLQueryOverAllTotal.(count($TMPOverAllSQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $TMPOverAllSQLQueryPart) : '')." ";
		}
	else
		{
		$SQLQueryOverAllTotal = $SQLQueryTotal;
		}
	// Remove the resume feature from the SQL query, in this way we can find the overall total recipient amount - FINISHED

	$SQLQuery .= "GROUP BY tblMembers.MemberID ";
	$SQLQuery .= "ORDER BY tblMembers.MemberID ";
	// Query for retrieving members - FINISHED

	$ResultSetTotal = ExecuteMySQLQuery($SQLQueryTotal, __LINE__, __FILE__);
	$ResultSetOverAllTotal = ExecuteMySQLQuery($SQLQueryOverAllTotal, __LINE__, __FILE__);
	
	$TotalRecords	= mysql_fetch_assoc($ResultSetTotal);
		$TotalRecords = $TotalRecords['TotalFound'];
	$TotalIntervals		= ceil($TotalRecords / $this->RecipientsPerInterval);
		$TotalIntervals = ($TotalIntervals == 0 ? 1 : $TotalIntervals);

	$OverAllTotalRecords = mysql_fetch_assoc($ResultSetOverAllTotal);
		$OverAllTotalRecords = $OverAllTotalRecords['TotalFound'];
	
	mysql_free_result($ResultSetTotal);
	// Retrieve and prepare recipients (local) - FINISHED

	// Retrieve the total amount of outsourced recipients - STARTED
	$TotalOutsourcedRecipients = 0;

	$this->ArrayOutsourcedMailLists = array();

	foreach ($this->ArrayMailLists as $Index => $ArrayValues)
		{
		if ($ArrayValues['SourceType'] == 'Outsourced - MySQL')
			{
			$this->ArrayOutsourcedMailLists[] = $ArrayValues['MailListID'];
			
			$SQLQueryMailList = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillists WHERE MailListID='".$ArrayValues['MailListID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQueryMailList, __LINE__, __FILE__);
				$ArrayOutsourcedMailList = mysql_fetch_assoc($ResultSet);

			$TotalOutsourcedRecipients += FindTotalMemberOfOutsourcedList($ArrayOutsourcedMailList, $this->ArrayConfig, $TMPArraySQLQueryPartSendRules);
			}
		}
	// Retrieve the total amount of outsourced recipients - FINISHED

	// Re-define the total amounts - STARTED
	$this->TotalOutsourcedRecipients= $TotalOutsourcedRecipients;
	$this->RecipientSQLQuery		= $SQLQuery;
		unset($SQLQuery);
	$this->TotalRecipientsSQLQuery	= $SQLQueryOverAllTotal;
	$this->TotalRecipients			= $TotalRecords + $TotalOutsourcedRecipients;
	$this->TotalIntervals			= $TotalIntervals;
	$this->TotalLocalRecipients		= $this->TotalRecipients - $this->TotalOutsourcedRecipients;
	$this->TotalOverAllRecipients	= $OverAllTotalRecords + $TotalOutsourcedRecipients;
	// Re-define the total amounts - FINISHED

	return;
	}

//-----------------------------------------------------------
// Create temporary MySQL table for campaign recipients
function _CreateTemporaryMySQLTable($CampaignID)
	{
	$this->TemporaryMySQLTableName = $this->ArrayConfig['Database']['Prefix'].'tmp_'.$this->ArrayCampaign['CampaignID'].'_'.$this->ArrayCampaignStatistics['CampaignStatisticsID'];

	if ($this->IsResumed == false)
		{
		$SQLQuery = "DESCRIBE ".$this->ArrayConfig['Database']['Prefix']."members";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
			$SQLQuery		 = "CREATE TABLE `".$this->TemporaryMySQLTableName."` (";
			$SQLQuery		.= "`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,";
			$SQLQuery		.= "`SendStatus` enum('Pending','Sent','Failed') NOT NULL default 'Pending',";
			$SQLQuery		.= "`SendStatusDescription` VARCHAR( 250 ) NOT NULL,";
			$SQLQuery		.= "`MemberID` INT NOT NULL ,";
			$SQLQuery		.= "`Email` VARCHAR( 250 ) NOT NULL ,";
			$SQLQuery		.= "`Source` ENUM( 'Local', 'Outsourced' ) NOT NULL ,";
			$SQLQuery		.= "INDEX ( `MemberID` , `Email` ) ";
			$SQLQuery		.= ") ENGINE = HEAP;";
		$ResultSet 		 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
		
	return;
	}

//-----------------------------------------------------------
// Prepare email properties
function _PrepareEmailProperties($CampaignID)
	{
	// Define subject - STARTED
	$TMPSubject = $this->ArrayContent['Subject'];
	// Define subject - FINISHED
		
	// Define content - STARTED
	if (count($ArrayNewsletter) > 0)
		{
		// CMS based content
		$TMPPriority 			= $this->ArrayNewsletter['Priority'];
		$TMPCharSet				= $this->ArrayNewsletter['CharSet'];
		$TMPContentType			= $this->ArrayNewsletter['ContentType'];
		if ($TMPContentType == 'Html')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentHTML'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}
		elseif ($TMPContentType == 'Both')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentHTML'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentPlain'], $this->ArrayContent['CMSParts']);
			}
		elseif ($TMPContentType == 'Plain')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentPlain'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}

		// Add administrator header and footer to the content - STARTED
			$TMPArrayContent = array(
									"ContentMain"		=> $TMPContentMain,
									"ContentAlternate"	=> $TMPContentAlternate,
									"ContentType"		=> $TMPContentType,
									);
		$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $this->ArrayCampaign['RelAdministratorID'], $this->ArrayConfig);
			$TMPContentMain = $TMPArrayContent['ContentMain'];
			$TMPContentAlternate = $TMPArrayContent['ContentAlternate'];
		// Add administrator header and footer to the content - FINISHED
		}
	else
		{
		// Traditional template or quick mode
		$TMPPriority 			= $this->ArrayContent['Priority'];
		$TMPCharSet				= $this->ArrayContent['CharSet'];
		$TMPContentType			= $this->ArrayContent['ContentType'];
		$TMPContentMain			= $this->ArrayContent['ContentMain'];
		$TMPContentAlternate	= $this->ArrayContent['ContentAlternate'];
		}
	// Define content - FINISHED

	// Analyze subject. If it is in "multi-mode", explode into array - STARTED
	if (count(explode("\n", $TMPSubject)) > 1)
		{
		$TMPSubject = explode("\n", $TMPSubject);
		}
	// Analyze subject. If it is in "multi-mode", explode into array - FINISHED

	$this->ArrayEmailProperties = array();
		$this->ArrayEmailProperties	= array(
										"CampaignID"			=>	$this->ArrayCampaign['CampaignID'],
										"CampaignStatisticsID"	=>	$this->ArrayCampaignStatistics['CampaignStatisticsID'],
										"EmbedObjects"			=>	($this->ArrayContent['EmbedObjects'] == 'Yes' ? true : false),
										"FromEmail"				=>	$this->ArrayFromEmail['EmailAddress'],
										"FromName"				=>	$this->ArrayFromEmail['Name'],
										"ReplyToEmail"			=>	$this->ArrayReplyToEmail['EmailAddress'],
										"ReplyToName"			=>	$this->ArrayReplyToEmail['Name'],
										"ReturnPathEmail"		=>	($this->ArrayAdministrator['ReturnPathEmail'] == '' ? ($this->ArrayReturnPathEmail['EmailAddress'] != '' ? $this->ArrayReturnPathEmail['EmailAddress'] : $this->ArrayReplyToEmail['EmailAddress']) : $this->ArrayCampaignAdministrator['ReturnPathEmail']),
										"Priority"				=>	$TMPPriority,
										"CharSet"				=>	$TMPCharSet,
										"Subject"				=>	$TMPSubject,
										"ContentType"			=>	$TMPContentType,
										"ContentMain"			=>	$TMPContentMain,
										"ContentAlternate"		=>	$TMPContentAlternate,
										"Attachments"			=>	$this->ArrayAttachments,
										"SendEngines"			=>	$this->ArraySMTP,
										);

	$this->ObjectBenchmark->Calculate("Campaign properties are prepared");

	return;
	}

//-----------------------------------------------------------
// Retrieve from, reply to and return path emails
function _GetCampaignSenderInfo($CampaignID)
	{
	if ($this->ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayFromEmail = mysql_fetch_assoc($ResultSet);

	if ($this->ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);

	if ($this->ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelReturnPathEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelReturnPathEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);

	$this->ObjectBenchmark->Calculate("Campaign sender information retrieved");

	return;
	}

//-----------------------------------------------------------
// Retrieve campaign content
function _GetCampaignContent($CampaignID)
	{
	if ($this->ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - STARTED
		if ($this->ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($this->ArrayContent['ContentMainURL']));
			$this->ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($this->ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = implode("", file($this->ArrayContent['ContentAlternateURL']));
			$this->ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - FINISHED
		}
	elseif ($this->ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			// CMS based newsletter
			$ArrayCMS = mysql_fetch_assoc($ResultSet);

			$this->ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$this->ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayNewsletter = mysql_fetch_assoc($ResultSet);

			// Copy charset, content-type, priority info to content array - STARTED
			$this->ArrayContent['CharSet']		=	$this->ArrayNewsletter['CharSet'];
			$this->ArrayContent['Priority']		=	$this->ArrayNewsletter['Priority'];
			$this->ArrayContent['ContentType']	=	$this->ArrayNewsletter['ContentType'];
			// Copy charset, content-type, priority info to content array - FINISHED
			}
		else
			{
			// Traditional newsletter
			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$CampaignID."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}

	// Add administrator header and footer to the content - STARTED
	$this->ArrayContent = AddAdministratorHeaderFooter($this->ArrayContent, $this->ArrayCampaign['RelAdministratorID'], $this->ArrayConfig);
	// Add administrator header and footer to the content - FINISHED

	$this->ObjectBenchmark->Calculate("Campaign content is retrieved");

	return;
	}

//-----------------------------------------------------------
// Retrieve campaign attachments
function _GetCampaignAttachments($CampaignID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$this->ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayAttachments[] = array(
										"AttachmentID"		=>	$EachRow['AttachmentID'],
										"FileName"			=>	$EachRow['FileName'],
										"FileSize"			=>	$EachRow['FileSize'],
										);
		}

	$this->ObjectBenchmark->Calculate("Campaign attachments are retrieved");

	return;
	}
	
//-----------------------------------------------------------
// Retrieve campaign send methods (MTA, Sendmail, Qmail, SMTP, Raw File, etc.)
function _GetCampaignSendMethods($CampaignID)
	{
	// Retrieve campaign SMTP settings - STARTED
	$this->ArraySMTP = array();

	if ($this->ArrayAdministrator['ForcedSendingMethod'] != 'N/A')
		{
		if ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Qmail')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'QMail',
									);
			}
		elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Sendmail')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'Sendmail',
									);
			}
		elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Local MTA')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'MTA',
									);
			}
		elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'SMTP Server')
			{
			$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "WHERE tblSMTP.SMTPServerID = '".$this->ArrayAdministrator['ForcedSendingMethodID']."' LIMIT 0,1";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
				$TMPArraySMTP = mysql_fetch_assoc($ResultSet);
	
			$this->ArraySMTP = array(
									'Type'		=>	'SMTP',
									'Host'		=>	$TMPArraySMTP['ServerHost'],
									'Port'		=>	$TMPArraySMTP['ServerPort'],
									'Auth'		=>	$TMPArraySMTP['ServerAuth'],
									'Username'	=>	$TMPArraySMTP['ServerUsername'],
									'Password'	=>	$TMPArraySMTP['ServerPassword'],
									'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
									);
			}
		elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Raw File')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'SaveAsFile',
									);
			}
		}
	else
		{
		if ($this->ArrayCampaign['SendEngine'] == 'SMTP')
			{
			$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$this->ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
			while ($TMPArraySMTP = mysql_fetch_assoc($ResultSet))
				{
				$this->ArraySMTP[] = array(
										'Type'		=>	'SMTP',
										'Host'		=>	$TMPArraySMTP['ServerHost'],
										'Port'		=>	$TMPArraySMTP['ServerPort'],
										'Auth'		=>	$TMPArraySMTP['ServerAuth'],
										'Username'	=>	$TMPArraySMTP['ServerUsername'],
										'Password'	=>	$TMPArraySMTP['ServerPassword'],
										'TimeOutSec'=>	$TMPArraySMTP['ServerTimeoutSeconds'],
										'MaxEmailsPerInterval'		=>	$TMPArraySMTP['MaxEmailsPerInterval'],
										'SecondsDelayPerInterval'	=>	$TMPArraySMTP['SecondsDelayPerInterval'],
										);
				}
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'MTA')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'MTA',
									);
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'QMail')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'QMail',
									);
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'Sendmail')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'Sendmail',
									);
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'SaveAsFile')
			{
			$this->ArraySMTP = array(
									'Type'		=>	'SaveAsFile',
									);
			}
		}
	// Retrieve campaign SMTP settings - FINISHED

	$this->ObjectBenchmark->Calculate("Campaign SMTP settings are retrieved");

	return;
	}

//-----------------------------------------------------------
// Retrieve the list of campaign clients
function _GetCampaignClients($CampaignID)
	{
	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_clients AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."clients AS tblClients ";
	$SQLQuery .= "ON tblAssign.RelClientID = tblClients.ClientID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	$this->ArrayClients = array();

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayClients[] = array(
									"ClientID"		=>		$EachRow['ClientID'],
									"Name"			=>		$EachRow['Name'],
									"Email"			=>		$EachRow['Email'],
									);
		}

	$this->ObjectBenchmark->Calculate("Campaign clients are retrieved");

	return;
	}

//-----------------------------------------------------------
// Update campaign statistics (send log) entry
function _UpdateCampaignStatistics($ArrayFieldnValues, $CampaignID, $CampaignStatisticsID, $IsUpdate = false)
	{
	if ($IsUpdate == true)
		{
		$TMPSQLQueryPart = implode(", ", $ArrayFieldnValues);

		$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET ".$TMPSQLQueryPart." WHERE CampaignStatisticsID='".$CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	else
		{
		$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
			$FieldList = "`".$FieldList."`";
		$ValueList = implode("','", array_values($ArrayFieldnValues));
			$ValueList ="'".$ValueList."'";
	
		$SQLQuery = "REPLACE INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		if ($CampaignStatisticsID == '')
			{
			$this->CampaignStatisticsID = mysql_insert_id();
			}
		else
			{
			$this->CampaignStatisticsID = $CampaignStatisticsID;
			}
		}

	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".$this->CampaignStatisticsID."' AND RelCampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
		
	$this->ObjectBenchmark->Calculate("Campaign statistics are updated");

	return;
	}

//-----------------------------------------------------------
// Decide if this campaign was paused or a fresh start
function _DecideIfThisSendingIsAResume($CampaignID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE Status='Pending' AND RelCampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	if (mysql_num_rows($ResultSet) == 0)
		{
		$this->IsResumed = false;
		}
	else
		{
		$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);

		if (($this->ArrayCampaignStatistics['QueuePointer'] > 0) || ($this->ArrayCampaignStatistics['QueuePointerOutsource'] > 0))
			{
			$this->IsResumed = true;
			}
		else
			{
			$this->IsResumed = false;
			}

		$this->CampaignStatisticsID = $this->ArrayCampaignStatistics['CampaignStatisticsID'];
		}

	$this->ObjectBenchmark->Calculate("Decided if this is a resume or restart process");
	}
	
//-----------------------------------------------------------
// Retrieve campaign owner record
function _GetAdministratorRecord($CampaignID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$this->ArrayCampaign['RelAdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayAdministrator = mysql_fetch_assoc($ResultSet);

	return;
	}

//-----------------------------------------------------------
// Retrieve campaign record
function _GetCampaignRecord($CampaignID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns WHERE CampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	if (mysql_num_rows($ResultSet) < 1)
		{
		return;
		}
	else
		{
		$this->ArrayCampaign = mysql_fetch_assoc($ResultSet);
		}

	$this->ObjectBenchmark->Calculate("Campaign information retrieved");
	
	return;
	}

//-----------------------------------------------------------
// Select a pending campaign
function _SelectPendingCampaignToSend()
	{
	// Retrieve all "send now" campaigns - STARTED
	$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='Send Now' AND IntegrityCheck='Ready'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this campaign has been sent or not - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalFound = $TotalFound['TotalFound'];
		if ($TotalFound > 0)
			{
			continue;
			}
		// Check if this campaign has been sent or not - FINISHED

		$this->ArrayPendingCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "send now" campaigns - FINISHED

	// Retrieve all "future scheduled" campaigns - STARTED
	$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='One time scheduled' AND (tblSchedule.ScheduleSendDate <= NOW() OR tblSchedule.ScheduleSendDate = '0000-00-00 00:00:00') AND tblCampaigns.IntegrityCheck='Ready'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this campaign has been sent or not - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalFound = $TotalFound['TotalFound'];
		if ($TotalFound > 0)
			{
			continue;
			}
		// Check if this campaign has been sent or not - FINISHED

		$this->ArrayPendingCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "future scheduled" campaigns - FINISHED

	// Retrieve all "recursive scheduled" campaigns - STARTED
	$CurrentDayOfMonth	=	date("j");
	$CurrentDayOfWeek	=	date("w");
	$CurrentWeekOfMonth	= ((int)($CurrentDayOfMonth / 7));
		$CurrentWeekOfMonthModulus = $CurrentDayOfMonth % 7;
		if ($CurrentWeekOfMonthModulus > 0)
			{
			$CurrentWeekOfMonth++;
			}
	$CurrentMonth		=	date("n");
	$CurrentHour		=	date("G");
	$CurrentMinute		=	date("i");
		// Strip leading zero from the minute - STARTED
		if (substr($CurrentMinute, 0, 1) == '0')
			{
			$CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
			}
		// Strip leading zero from the minute - FINISHED
		
		// Round the minute with 15 minutes intervals - STARTED
		if (($CurrentMinute >= 0) && ($CurrentMinute <= 14))
			{
			$CurrentMinute = 0;
			}
		elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29))
			{
			$CurrentMinute = 15;
			}
		elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44))
			{
			$CurrentMinute = 30;
			}
		elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59))
			{
			$CurrentMinute = 45;
			}
		// Round the minute with 15 minutes intervals - FINISHED

	$SQLQuery  = "SELECT tblCampaigns.CampaignID, tblSchedule.ScheduleID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='Continuous scheduled' ";
	$SQLQuery .= "AND (tblSchedule.ScheduleMonth = '*' OR tblSchedule.ScheduleMonth='".$CurrentMonth."' OR tblSchedule.ScheduleMonth LIKE '".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth."') ";
	$SQLQuery .= "AND (((tblSchedule.ScheduleDayOfMonth = '*' OR tblSchedule.ScheduleDayOfMonth='".$CurrentDayOfMonth."' OR tblSchedule.ScheduleDayOfMonth LIKE '".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleDayOfWeek = '*' OR tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."' OR tblSchedule.ScheduleDayOfWeek LIKE '".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek."')) ";
	$SQLQuery .= "OR (tblSchedule.ScheduleDayOfMonth='".$CurrentWeekOfMonth."' AND tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."'))";
	$SQLQuery .= "AND (tblSchedule.ScheduleHour = '*' OR tblSchedule.ScheduleHour='".$CurrentHour."' OR tblSchedule.ScheduleHour LIKE '".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleMinute = '*' OR tblSchedule.ScheduleMinute='".$CurrentMinute."' OR tblSchedule.ScheduleMinute LIKE '".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleSendInstance < tblSchedule.ScheduleSendMaxInstance OR tblSchedule.ScheduleSendMaxInstance='0') ";
	$SQLQuery .= "AND tblCampaigns.IntegrityCheck='Ready' ";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		$ProcessedOn = date("Y-m-d H:".sprintf('%02d', $CurrentMinute));
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this sending has been ran before or not - STARTED
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive WHERE RelCampaignID='".$EachRow['CampaignID']."' AND DATE_FORMAT(ProcessedOn, '%Y-%m-%d %H:%i')='".$ProcessedOn."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalFound = $TotalFound['TotalFound'];
		if ($TotalFound > 0)
			{
			continue;
			}
		// Check if this sending has been ran before or not - FINISHED

		$this->ArrayPendingCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "recursive scheduled" campaigns - FINISHED

	// Retrieve all "resumed" campaigns - STARTED
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE Status='Pending'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayPendingCampaignIDs[] = $EachRow['RelCampaignID'];
		}
	// Retrieve all "resumed" campaigns - FINISHED

	// Check if owner of these campaigns have enough sending quota - STARTED
	$TMPArrayCampaignIDs	= $this->ArrayPendingCampaignIDs;
	$this->ArrayPendingCampaignIDs		= array();
	
	foreach ($TMPArrayCampaignIDs as $EachCampaignID)
		{
		$IsAllowedQuota		= false;
		$IsAllowedTimeLimit = false;

		// Check available send amount limit - STARTED
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns AS tblCampaigns ";
		$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."administrators as tblAdmins ";
		$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdmins.AdministratorID ";
		$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."administrator_limits as tblAdminLimits ";
		$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdminLimits.RelAdministratorID ";
		$SQLQuery .= "WHERE tblCampaigns.CampaignID = '".$EachCampaignID."' ";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayCampaignAdminLimits = mysql_fetch_assoc($ResultSet);

		if ($ArrayCampaignAdminLimits['Limit'] != 'Unlimited')
			{
			$QuotaUsed = CalculateSendLimits($ArrayCampaignAdminLimits['AdministratorID'], $ArrayCampaignAdminLimits, $this->ArrayConfig);
			$RemainingQuota = $ArrayCampaignAdminLimits['LimitAmount'] - $QuotaUsed;
			
			if ($RemainingQuota > 0)
				{
				$IsAllowedQuota = true;
				}
			}
		else
			{
			$IsAllowedQuota = true;
			}
		// Check available send amount limit - FINISHED

		// Check available send time limit - STARTED
		$TodayDate	= date('D');
		$TodayTime	= strtotime(date('Y-m-d H:i:s'));
		$AllowedDays = explode(',', $ArrayCampaignAdminLimits['TimeLimitDates']);

		if ($AllowedDays[0] != '')
			{
			foreach ($AllowedDays as $EachDay)
				{
				if ($TodayDate == $EachDay)
					{
					if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitTo']))))
						{
						$IsAllowedTimeLimit = true;
						break;
						}
					}
				}
			}
		else
			{
			$IsAllowedTimeLimit = true;
			}
		// Check available send time limit - FINISHED

		if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
			{
			$this->ArrayPendingCampaignIDs[] = $EachCampaignID;
			}
		}
	// Check if owner of these campaigns have enough sending quota - FINISHED

	// Check if these campaigns have active sending or not - STARTED
	$TMPArrayCampaignIDs	= $this->ArrayPendingCampaignIDs;
	$this->ArrayPendingCampaignIDs		= array();
	
	foreach ($TMPArrayCampaignIDs as $EachCampaignID)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachCampaignID."' AND Status='Sending'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		if ($TotalFound < 1)
			{
			$this->ArrayPendingCampaignIDs[] = $EachCampaignID;
			
			// Record the process to "campaigns_schedule_recursive" - STARTED
			$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive (ID, RelCampaignID, ProcessedOn) VALUES ('', '".$EachCampaignID."', '".$ProcessedOn."')";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			// Record the process to "campaigns_schedule_recursive" - FINISHED
			}
		}
	// Check if these campaigns have active sending or not - FINISHED

	$this->ArrayPendingCampaignIDs = array_unique($this->ArrayPendingCampaignIDs);

	// Because oemPro Send Engine can be executed seperately at the same time, only retrieve one campaign (oemPro v>3.6.2) - STARTED
	$RandomNumber = rand(0, count($this->ArrayPendingCampaignIDs) - 1);
		$this->ArrayPendingCampaignIDs = array($this->ArrayPendingCampaignIDs[$RandomNumber]);
	// Because oemPro Send Engine can be executed seperately at the same time, only retrieve one campaign (oemPro v>3.6.2) - FINISHED

	if ($this->ArrayPendingCampaignIDs[0] == '')
		{
		$this->TotalPendingCampaigns = 0;
		unset($this->ArrayPendingCampaignIDs);
		}
	else
		{
		$this->TotalPendingCampaigns = count($this->ArrayPendingCampaignIDs);
		}

	$this->ObjectBenchmark->Calculate("Retrieving pending campaign IDs");

	// Check if max. available concurrent sending, if there is no for all campaigns, remove some of them - STARTED
	if ($this->TotalPendingCampaigns > $this->TotalAvailableLimit)
		{
		$this->ArrayPendingCampaignIDs = array_slice($this->ArrayPendingCampaignIDs, 0, $this->TotalAvailableLimit);
			$this->TotalPendingCampaigns = count($this->ArrayPendingCampaignIDs);
		}
	$this->ObjectBenchmark->Calculate("Exclude some campaigns if max. allowed campaigns are exceeded");
	// Check if max. available concurrent sending, if there is no for all campaigns, remove some of them - FINISHED
	}

//-----------------------------------------------------------
// Calculate available sending limit
function _CalculateAvailableSendingLimit()
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE Status='Sending'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$TotalConcurrent = mysql_fetch_assoc($ResultSet);
		$this->TotalConcurrent = $TotalConcurrent['TotalFound'];

	$this->ObjectBenchmark->Calculate("Sending campaigns are retreived");

	if ($this->ArrayConfig['Settings']['MaxConcurrentSending'] <= $this->TotalConcurrent)
		{
		$this->ErrorExists 	= true;
		$this->ErrorMessage	= 'Maximum concurrent sending limits ('.number_format($this->ArrayConfig['Settings']['MaxConcurrentSending']).' sendings max, '.number_format($this->TotalConcurrent).' active sendings) exceeded. oemPro will try again at the next run';
		}
	else
		{
		$this->TotalAvailableLimit = $this->ArrayConfig['Settings']['MaxConcurrentSending'] - $this->TotalConcurrent;
		}

	$this->ObjectBenchmark->Calculate("Check if current sending campaigns exceed max. concurrent limits or not");
	
	return;
	}


};
//***********************************************************
// End of the class
//***********************************************************

?>