<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-12-24 13:33        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ MIME STRING DECODER AND ENCODER
----[ SYNOPSIS


$text	= "�� �� asa";
$text2	= "=?iso-8859-1?B?/Gf8ZyD8ZyD8Z/wgZ/xnIA==?=";
$text3  = "=?iso-8859-1?Q?=FC=F0_=E7=F6_asa?=";


$ObjectMIME = new OctethMIMEUtilities();


print $ObjectMIME->DecodeMIMEString($text3);
print "<br>";
print ObjectMIME->EncodeMIMEString($text, "iso-8859-1");

exit;



----[ DESCRIPTION
With this module, you can decode and encode
strings into MIME format.

----[ CLASS NAME AND VERSION
OctethMIMEUtilities v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


---------------------------------------------------------------------------------------
*/

//-------------------------------------------[ OctethTemplate Class
class OctethMIMEUtilities
{
//*************************************************************************
// PUBLIC PROPERTIES
//*************************************************************************

//*************************************************************************
// PRIVATE PROPERTIES
//*************************************************************************


//*************************************************************************
// PUBLIC METHODS
//*************************************************************************

//*************************************************************************
// Constructor
// ---
// IN		Void		N/A
// OUT		Void		N/A
function OctethMIMEUtilities()
    {
	}


//*************************************************************************
// Encode strings to MIME format
// ---
// IN		String		string
// OUT		String		MIME encoded string
function EncodeMIMEString($String, $Charset)
	{
	if ($String == "")
		{
		return;
		}
		
	if (!eregi("^([[:print:]]*)$", $String))
		{
		$String = "=?".$Charset."?Q?".str_replace("+","_", str_replace("%","=", urlencode($String)))."?=";
		}
	return $String;
	}

//*************************************************************************
// Decode MIME encoded strings
// ---
// IN		String		MIME Encoded string
// OUT		String		Decoded string
function DecodeMIMEString($String)
	{
	if (($Position = strpos($String,"=?")) === false)
		{
		return $String;
		}

	$NewResult = "";
	
	while (!($Position === false))
		{
		$NewResult		.=		substr($String, 0, $Position);
		$String			=		substr($String, $Position + 2, strlen($String));
		$IntPosition	=		strpos($String, "?");
		$CharSet		=		substr($String, 0, $IntPosition);
		$EncryptionType = 		strtolower(substr($String, $IntPosition + 1, 1));
		$String			=		substr($String, $IntPosition + 3, strlen($String));
		$EndPosition	=		strpos($String, "?=");
		$MyString		=		substr($String, 0, $EndPosition);
		$String			=		substr($String, $EndPosition + 2, strlen($String));

		if($EncryptionType == "q")
			{
			$MyString = quoted_printable_decode(preg_replace("_", " ", $MyString)); 
			}
		else if ($EncryptionType == "b")
			{
			$MyString = base64_decode($MyString);
			}

		$NewResult		.=	$MyString;
		$Position		=	strpos($String,"=?");
		}

	$Result = $NewResult.$String; 

	if(preg_match("koi8", $String))
		{
		$Result = convert_cyr_string($Result, "k", "w");
		}
		
	return $Result;
	}


//*************************************************************************
// PRIVATE METHODS
//*************************************************************************

}; // end of class

?>