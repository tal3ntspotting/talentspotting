<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2003-03-21 15:17        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ UTILITIES #1 CLASS
----[ SYNOPSIS

bool CheckRequiredFields($PostVars, $Prefix) -> Checks required fields
bool CheckEmailFormat($email) -> Checks email format
string GeneratePassword($character_size = "4") -> Generates random password
string GetRandomLine($array) -> Gets random line from the entered array
string GetCurrentDate($type = "datetime") -> Gets current date
void PrintError($error) -> Prints fatal error to the screen and exits
array ConvertToTime($TimeStamp) -> Returns the formatted array of the entered timestamp
void ConvertSpecialHTML(&$Text) -> Convert special HTML characters (like &copy;) into their ASCII equivalents.

----[ DESCRIPTION
This class includes many utilities. For more details,
examine SYNOPSIS section.

----[ CLASS NAME AND VERSION
OctethUtilities v2.1

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>

*/

class OctethUtilities
{
// PROPERTIES
                
// METHODS

//--------[ Constructor
// IN		-			-				No input
// OUT		-			-				Void
function OctethUtilities()
    {
    }

//--------[ Convert Turkish Characters to English Characters
// IN		String			Text		The text to convert
// OUT		String			-			Converted text
function ConvertTR2EN($Text)
	{
	$ArrayChars = array(
						"�"		=>		"G",
						"�"		=>		"U",
						"�"		=>		"S",
						"�"		=>		"I",
						"�"		=>		"O",
						"�"		=>		"C",
						"�"		=>		"g",
						"�"		=>		"u",
						"�"		=>		"s",
						"�"		=>		"i",
						"�"		=>		"o",
						"�"		=>		"c",
						);

	foreach ($ArrayChars as $Pattern=>$Replace)
		{
		$Text = str_replace($Pattern, $Replace, $Text);
		}

	return $Text;
	}

//--------[ Convert special HTML characters (like &copy;) into their ASCII equivalents.
// (PEAR)
// ----
// IN		String			Text		(Reference). The text to convert
// OUT		Void			-			N/A
function ConvertSpecialHTML(&$Text)
	{
	$_HTML_Processor_translation_table = get_html_translation_table();
	
	$Text = strtr($Text, array_keys($_HTML_Processor_translation_table), array_flip(array_values($_HTML_Processor_translation_table)));
	}

//--------[ Checks required fields if they are blank or not
// a function to convert number of seconds (eg; 96172) to a readable format
// such as: 1 day, 2 hours, 42 mins, and 52 secs
// takes unix timestamp as input
// Array[0] => Days
// Array[1] => Hours
// Array[2] => Minutes
// Array[3] => Seconds
// ----
// IN		String			Timestamp		The timestamp which will be transformed
// OUT		Array			-				Array of the formatted way
function ConvertToTime($TimeStamp = 0)
	{
	// Initialize variables
	$ErrorExists = false;

	if (!$TimeStamp)
		{		
		return false;
		}

	if ($TimeStamp < 0)
		{
		$neg 		= 1;
		$TimeStamp	= 0 - $TimeStamp;
		}

	$days = $TimeStamp / 86400;
	$days = floor($days);
	$hrs = ($TimeStamp / 3600) % 24;
	$mins = ($TimeStamp / 60) % 60;
	$secs = $TimeStamp % 60;

	$timestring = "";

	if ($neg) $timestring .= "negative ";

	$ArrayTime = array();

	if ($days)
		{
		$ArrayTime[0] = $days;
//		$timestring .= "$days day" . ($days==1?"":"s");
//		if ($hrs || $mins || $secs) $timestring .= ", ";
		}

	if ($hrs)
		{
		$ArrayTime[1] = $hrs;

//		$timestring .= "$hrs hour" . ($hrs==1?"":"s");
//		if ($mins && $secs) $timestring .= ", ";
//		if (($mins && !$secs) || (!$mins && $secs)) $timestring .= " and ";
		}
	if ($mins)
		{
		$ArrayTime[2] = $mins;

//		$timestring .= "$mins min" . ($mins==1?"":"s");
//		if ($mins && $secs) $timestring .= ", ";
//		if ($secs) $timestring .= " and ";
		}

	if ($secs)
		{
		$ArrayTime[3] = $secs;

//		$timestring .= "$secs sec" . ($secs==1?"":"s");	
		}

	return $ArrayTime;
	}












//--------[ Checks required fields if they are blank or not
// OUT		Bool			-				Returns false if any blank field exists
// IN		Array			PostVars		The HTTP Post variables
// IN		Array			RequiredFields	The list of required fields
// IN		String			Prefix			The prefix which is used to make field required (ex: "FormValueR_")
function CheckRequiredFields($PostVars, $RequiredFields,$Prefix)
    {
	// Initialize variables
	$ErrorExists = false;
	
	// Scan each post item and check if they are blank or not
	foreach ($RequiredFields as $Field)
		{
		if ($PostVars[$Prefix.$Field] == "")
			{
			return false;
			}
		}

	return true;
	}

//--------[ Checks email format
function CheckEmailFormat($email)
    {
    if (!(preg_match("/^[\+\._a-z0-9-]+([\&|-|\.\+][\+\._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/i",$email)))
        {
        return false;
        }
    else
        {
        return true;
        }
    }
    
//--------[ Generates random password
function GeneratePassword($character_size = "4")
    {
    $characters = "qazwsxedcrfvtgbyhnujmikolp" . "QAZWSXEDCRFVTGBYHNUJMIKOLP" . "1234567890";
    $total_characters = strlen($characters);
    srand((double)microtime()*1000000);
    for($counter = 0; $counter < $character_size; ++$counter)
        {
        $random .= $characters[rand(0, $total_characters)];
        }
    return $random;
    }
    
//--------[ Gets random line from the array
function GetRandomLine($array)
    {
    $total_elements = count($array);
    
    srand((double)microtime()*1000000);
    while ($random_line == "")
        {
        $random_line = ereg_replace("\n","",$array[rand(0,$total_elements)]);
        }
    return $random_line;  
    }

//--------[ Gets current date and time
function GetCurrentDate($type = "datetime")
    {
    // get now date
    $nowdate_array = getdate();

    if ($type == "datetime")
        {
        $now_year  = sprintf("%02d", $nowdate_array[year]);
        $now_month = sprintf("%02d", $nowdate_array[mon]);
        $now_day   = sprintf("%02d", $nowdate_array[mday]);
        $now_hour  = sprintf("%02d", $nowdate_array[hours]);
        $now_min   = sprintf("%02d", $nowdate_array[minutes]);
        $now_sec   = sprintf("%02d", $nowdate_array[seconds]);

        $date = "$now_year-$now_month-$now_day $now_hour:$now_min:$now_sec";
        }
    elseif ($type == "date")
        {
        $now_year  = sprintf("%02d", $nowdate_array[year]);
        $now_month = sprintf("%02d", $nowdate_array[mon]);
        $now_day   = sprintf("%02d", $nowdate_array[mday]);

        $date = "$now_year-$now_month-$now_day";
        }
    elseif ($type == "time")
        {
        $now_hour  = sprintf("%02d", $nowdate_array[hours]);
        $now_min   = sprintf("%02d", $nowdate_array[minutes]);
        $now_sec   = sprintf("%02d", $nowdate_array[seconds]);

        $date = "$now_hour:$now_min:$now_sec";
        }
    return ($date);
    }

//--------[ Prints error page
function PrintError($error)
    {
    print("<html><body>");
    print("<hr>\n");
    print("<b>Error occured: </b><br>\n");
    print("$error<br>\n");
    print("<hr><br>\n");
    print("</body></html>");
    exit;
    }


    
}; // end of class OctethUtilities

?>