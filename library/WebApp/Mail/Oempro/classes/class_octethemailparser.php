<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-06-12 14:29        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ EMAIL CONTENT PARSER CLASS
----[ SYNOPSIS

include("./class_octethemailparser.php");
$ObjectPOP3 		= new pop3_class;

$ParsedHeaders = $ObjectEmailParser->ParseHeaders($EmailHeaders);
$ParsedBody    = $ObjectEmailParser->ParseBody($EmailBody);



----[ DESCRIPTION
With this module, you can parse
the content of the emails easily.

----[ CLASS NAME AND VERSION
OctethEmailParser v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


---------------------------------------------------------------------------------------
*/

//-------------------------------------------[ OctethLister Class
class OctethEmailParser
{
//-------------------------------------------------------------[ Public Properties
// The email headers. The datatype is array
// Each element should be the next header
// item.
var $EmailHeaders				=	array();

//-------------------------------------------------------------[ Private Properties
var $_ErrorExists				=	false;
var $_ParsedEmailHeaders		=	array();
var $_ParsedEmailBody			=	array();


//-------------------------------------------------------------[ Public Methods
//###############################################################################
// Parse email body
// ---
// array	IN		EmailBody		- The email body
// array	OUT		N/A				- Returns the parsed email body
// ---
function ParseBody($EmailBody)
	{
	$this->_ParsedEmailBody = $EmailBody;
	
	
	
	return $this->_ParsedEmailBody;
	}
	
//###############################################################################
// Parse email headers
// ---
// array	IN		EmailHeaders	- The email headers
// array	OUT		N/A				- Returns the parsed email headers
// ---
function ParseHeaders($EmailHeaders)
	{
	$this->EmailHeaders = $EmailHeaders;
	
	foreach ($this->EmailHeaders as $Key=>$Header)
		{
		$Header = trim($Header);
		$HeaderParts = explode(": ", $Header);
		if ($HeaderParts[1] == "")
			{
			$this->_ParsedEmailHeaders[$HeaderParts[0]] = $HeaderParts[0];
			}
		else
			{
			$this->_ParsedEmailHeaders[$HeaderParts[0]] = $HeaderParts[1];
			}
		}
	return $this->_ParsedEmailHeaders;	
	}
	
//###############################################################################
// Constructor function
// ---
// void		IN		N/A		- No inputs
// void		OUT		N/A		- Returns nothing
// ---
function OctethEmailParser()
	{
	}
//-------------------------------------------------------------[ Private Methods


}; // end of the class

?>