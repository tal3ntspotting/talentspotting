<?php

##
# OcXML has many XML and SQL funtions
# Options :
#   None
#
class ocxml {
  
    ##
    # Constructor for ocxml class
    # Options :
    #   None
    #
    function ocxml() {
        $this->xmlParser=new xml2Array();
    }
    
    ##
    # Parse XML from given array
    # Options :
    #   $showAttr -> adds attributes for fields
    #
    function arrayToXml($content,$showAttr=false) {
        header("Content-type: text/xml; charset=utf-8");
        echo "<?xml version='1.0' standalone='yes'?>\n<ocxml>\n";
        foreach(array_keys($content) as $tables){
            echo "\t<table name=\"$tables\">\n";
            foreach(array_keys($content[$tables]) as $fields) {
                echo "\t\t<field name=\"$fields\"";
                if ($showAttr)
                    echo "{$content[$tables][$fields]["Attributes"]}";
                echo ">{$content[$tables][$fields]["Default"]}</field>\n";
            }
            echo "\t</table>\n";
        }
        echo "</ocxml>";
    }
    
    ##
    # Returns an array which created from sql file
    # Options :
    #   None
    #
    function sqlToArray($content){
        $ret = Array();
        $tables = split("CREATE TABLE",$content);
        $tables = array_slice($tables,1);
        foreach ($tables as $table) {
            $lines = split("\n",$table);
            $tableName = trim(rtrim($lines[0],"(")," `");
            $ret[$tableName] = Array();
            $lines = array_slice($lines,1);
            foreach ($lines as $line){
                $field = split(" ",trim($line," "));
                if ($field[0][0]=="`") {
                    $fieldName = trim($field[0],"`");
                    $ret[$tableName][$fieldName]=Array();
                    $ret[$tableName][$fieldName]=array_merge($ret[$tableName][$fieldName],getAttributes($line));
                }
            }
        }
        return($ret);
    }

    ##
    # Gets attributes of given line for creating arrays from sql (F:sqlToArray)
    # Options :
    #   None
    #
    function getAttributes($line) {
        $ret = Array();
        $ret["Attributes"] = "";
        $ret["Auto Increment"] = "false";
        $ret["Not Null"] = "false";
        $ret["Default"] = null;
        $line = rtrim(trim($line),",");
        $nodes = split(" ",$line);
        $nodes = array_slice($nodes,1);
        foreach ($nodes as $node) {
            switch (cleanAttr($node)) {
                case "varchar":case "VARCHAR":case "int":case "INT":
                    $ret["Attributes"].=" type=\"".substr($node,0,strlen(cleanAttr($node)))."\"";
                    $size = rtrim(substr($node,strlen(cleanAttr($node))+1,strlen($node)),")");
                    $ret["Attributes"].=" size=\"".$size."\" ";
                break;
                case "enum":case "ENUM":case "set":case "SET":
                    $ret["Attributes"].=" type=\"".substr($node,0,strlen(cleanAttr($node)))."\"";
                    $biStart = strpos($line,"(") + 1;
                    $biStop  = strpos($line,")");
                    $values = substr($line,$biStart,$biStop-$biStart);
                    $ret["Attributes"].=" values=\"".$values."\" ";
                break;
                case "text":case "TEXT":case "longtext":case "LONGTEXT":
                case "datetime":case "DATETIME":case "double":case "DOUBLE":
                    $ret["Attributes"].=" type=\"".substr($node,0,strlen(cleanAttr($node)))."\"";
                break;
                case "auto_increment":case "AUTO_INCREMENT":
                    if ($ret["Auto Increment"]==false) {
                        $ret["Auto Increment"] = "true";
                        $ret["Attributes"].= " auto_increment=\"true\" ";
                    }
                break;
                case "NOT":case "not":
                    if ((substr($line,strpos($line,"NOT"),8)=="NOT NULL") or (substr($line,strpos($line,"not"),8)=="not null")) {
                        if ($ret["Not Null"]==false) {
                            $ret["Not Null"]="true";
                            $ret["Attributes"].= " not_null=\"true\" ";
                        }
                    }
                break;
                case "default":case "DEFAULT":
                    $defPos = strpos($line,"default") + 8;
                    $value = substr($line,$defPos,strlen($line)-$defPos);
                    $ret["Default"] = trim(substr($value,0,strpos($value,"'",1)+1),"'");
                break;
            }
        }
        return $ret;
    }

    ##
    # Checks and returns xml file content 
    # Details :
    #   Changed to use local XML Class for PHP4 support
    #
    function getXml($file){
            $trans_xml = fopen( $file,"r");
            while( !feof( $trans_xml ) )
                $xml_content .= fread( $trans_xml, 1024 );
            return $xml_content;
    }

    ##
    # Returns an array which created from xml file
    # Details :
    #   It uses local class named XML for PHP4 support.
    #    
    function xmlToArray($ixml,$file=true){
        $file == true ? $xml = $xml = $this->xmlParser->parse($this->getXml($ixml)) : $xml = $this->xmlParser->parse((stripslashes($ixml)));
        if (!$xml==0) {
            $ret = Array();
            $i=0;
            foreach ($xml[0][children] as $tables){
                switch($tables[name]) {
                    case "TABLE":                    
                        $ret[$i][(string)$tables[attrs]["NAME"]]=Array();
                        foreach ($tables[children] as $fields) {
                            if ($fields[children]) {$j=0;
                                foreach ($fields[children] as $nodes) {
                                    foreach ($nodes[children] as $childsOfField) {
                                        $ret[$i][(string)$tables[attrs]["NAME"]][(string)$fields[attrs]["NAME"]][$j][(string)$childsOfField[attrs]["NAME"]] = (string) $childsOfField[tagData];
                                    }$j++;
                                }
                            }
                            else {
                                $ret[$i][(string)$tables[attrs]["NAME"]][(string)$fields[attrs]["NAME"]] = (string) $fields[tagData];
                            }
                        }
                        $i++;
                    break;
                    case "USERNAME":case "PASSWORD":case "COMMAND":
                        $ret[(string)$tables[name]] = (string) $tables[tagData];
                    break;
                }
            }
        }
        return $ret;
    }

    ##
    # Returns an array which created from rss file
    # Details :
    #   It uses local class named XML for PHP4 support.
    #    
    function rssToArray($ixml,$file=true){
        $xml=array();
        $file == true ? $xml = $this->xmlParser->parse($this->getXml($ixml)) : $xml = $this->xmlParser->parse((stripslashes($ixml)));
        //print_r ($xml);return;
        if (!$xml==0) {
            $ret = array();
            $i=0;
            foreach ($xml[0][children][0][children] as $rss){
                switch($rss[name]) {
                    case "ITEM":                    
                        $ret[$i]=Array();
                        foreach ($rss[children] as $rssContent)
                            $ret[$i][(string)$rssContent[name]] = $rssContent[tagData];
                        $i++;
                    break;
                }
            }
        }
        return $ret;
    }

    ##
    # Meta function for sqlToArray
    # Options :
    #   None
    #    
    function cleanAttr($line) {
        $pos = strpos($line,"(");
        if (!$pos === false)
            return substr($line,0,$pos);
        return $line;
    }
    
    ##
    # Creates XHTML compatible forms from given Xml
    # Options :
    #   $show -> just shows given table's form
    #   $method -> form send method's GET or POST
    #
    function xmlToXhtml($xml,$show,$method="GET") {
        foreach ($xml->table as $tables){
            $show=="all"?$que="all":$que=$tables['name'];
            if ($show==$que){
                $ret.= "<h3>".$xml->name." » ".$tables['title']."</h3>\n";
                $ret.= "<form action='?' method='$method' id='".$tables['name']."' class='cssform'>\n";
                foreach ($tables->field as $fields){
                    $fields['title']<>""?$title=$fields['title']:$title=$fields['name'];
                    $ret.="<p><label for='{$fields['name']}'>{$title}</label>\n";
                    switch ($fields["type"]) {
                        case "text":case "longtext":case "datetime":
                            $fields["type"]=="text"?$rows=2:$rows=4;
                            $fields["type"]=="datetime"?$rows=1:$rows;
                            $ret.="<textarea id='{$fields['name']}' rows='$rows' cols='35'>{$fields}</textarea>\n";
                        break;
                        case "char":case "varchar":case "int":
                            $prop_=split(':',$fields['prop']);
                            switch($prop_[0]){
                                case 'password':
                                    $itype='password';
                                break;
                                default:
                                    $itype='text';
                            }
                            $ret.="<input type='{$itype}' id='{$fields['name']}' value='{$fields}' maxlength='{$fields['size']}' />\n";
                        break;
                        case "set":case "enum":
                            $values=split(',',$fields['values']);
                            foreach ($values as $val) {
                                $temp=split(',',trim($val,"'"));
                                $value=$temp[0];$temp[1]==""?$caption=$value:$caption=$temp[1];
                                $fields==$value ? $cc="CHECKED":$cc="";
                                $ret.="{$caption} <input type='radio' name='{$fields['name']}' value='{$value}' $cc />";
                            }
                            $ret.="<br />\n";
                        break;
                    }
                    $ret.="</p>\n";
                }
                $ret.="<p><label>Submit</label>
                        <input type='submit' value='Submit' />\n
                        <input type='reset' value='Reset' />\n
                       </p></form><hr />";
            }
        }
        return $ret;
    }
}

##
# XML Class for php4
# Details :
#   Copied Under GPLv2
#
class xml2Array {
   
   var $arrOutput = array();
   var $resParser;
   var $strXmlData;
   
   ##
   # Parses XML
   # Options :
   #   None
   #
   function parse($strInputXML) {
           $this->arrOutput = array();
           $this->resParser = xml_parser_create();
           xml_set_object($this->resParser,$this);
           xml_set_element_handler($this->resParser, "tagOpen", "tagClosed");
           
           xml_set_character_data_handler($this->resParser, "tagData");
       
           $this->strXmlData = xml_parse($this->resParser,$strInputXML );
           if(!$this->strXmlData) {
               die(sprintf("XML error: %s at line %d",
           xml_error_string(xml_get_error_code($this->resParser)),
           xml_get_current_line_number($this->resParser)));
           }
                           
           xml_parser_free($this->resParser);
           
           return $this->arrOutput;
   }
   
   ##
   # Open tag definitions
   # Options :
   #   None
   #
   function tagOpen($parser, $name, $attrs) {
       $tag=array("name"=>$name,"attrs"=>$attrs); 
       array_push($this->arrOutput,$tag);
   }
   
   ##
   # Gets tag data
   # Options :
   #   None
   #
   function tagData($parser, $tagData) {
       if(trim($tagData)) {
           if(isset($this->arrOutput[count($this->arrOutput)-1]['tagData'])) {
               $this->arrOutput[count($this->arrOutput)-1]['tagData'] .= $tagData;
           } 
           else {
               $this->arrOutput[count($this->arrOutput)-1]['tagData'] = $tagData;
           }
       }
   }
   
   ##
   # Close tag definitions
   # Options :
   #   None
   #
   function tagClosed($parser, $name) {
       $this->arrOutput[count($this->arrOutput)-2]['children'][] = $this->arrOutput[count($this->arrOutput)-1];
       array_pop($this->arrOutput);
   }
}

?>