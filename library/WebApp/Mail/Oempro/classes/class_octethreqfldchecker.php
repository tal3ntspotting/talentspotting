<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2003-04-04 10:46        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ REQUIRED FIELD CHECKING
----[ SYNOPSIS






----[ DESCRIPTION
This module will check the required
fields if they filled in or not. This
object is optimized to be used with
Octeth framework standards.

----[ CLASS NAME AND VERSION
OctethReqFieldChecker v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


********************************************************************
********************************************************************
********************************************************************
************** DO NOT FORGET TO UPDATE THE *************************
********* LAST MODIFY DATE AND TIME AFTER MAKING *******************
********************** MODIFICATIONS *******************************
********************************************************************
********************************************************************
********************************************************************

*/

//***********************************************************
// OctethReqFieldChecker Class
//***********************************************************
class OctethReqFieldChecker
{
//***********************************************************
// Public Properties
//***********************************************************
var $ArrayRequiredFields	=	array();
var $ArrayErrorMessages		=	array();
var $ArrayPostVars			=	array();
var $FormFieldTag			=	"";

//***********************************************************
// Private Properties
//***********************************************************
var $_ErrorExists				=	false;

//***********************************************************
// Public Methods
//***********************************************************

//-----------------------------------------------------------
// Constructor function
// ---
// Array	IN		ArrayRequiredFields		- Array of the required fields. Key is the field, value is the error message
// Array	IN		ArrayErrorMessages		- Array of the error messages. To be used with OctethFormParser class
// Array	IN		ArrayPostVars			- Array of the post variables (form elements)
// String	IN		FormFieldTag			- Tag of the form field elements in the form. (Ex: FormValue_)
// Void		OUT		N/A						- N/A
// ---
//-----------------------------------------------------------
function OctethReqFieldChecker($ArrayRequiredFields, $ArrayErrorMessages, $ArrayPostVars, $FormFieldTag)
	{
	$this->ArrayRequiredFields		=		$ArrayRequiredFields;
	$this->ArrayErrorMessages		=		$ArrayErrorMessages;
	$this->ArrayPostVars			=		$ArrayPostVars;
	$this->FormFieldTag				=		$FormFieldTag;

	return true;
	}
	
//-----------------------------------------------------------
// Constructor function
// ---
// Void		IN		N/A						- N/A
// Array	OUT		ArrayErrorMessages		- Array of the error messages. To be used with OctethFormParser class
// ---
//-----------------------------------------------------------
function CheckForRequiredFields()
	{
	// Initialize variables
	$ErrorExists = false;

	// Check for required fields - STARTED
	foreach ($this->ArrayRequiredFields as $Field=>$ErrorMessage)
		{
		if ($this->ArrayPostVars[$this->FormFieldTag.$Field] == '')
			{
			$this->ArrayErrorMessages[$Field]	=	$ErrorMessage;
			}
		}
	// Check for required fields - STARTED

	return $this->ArrayErrorMessages;
	}

	
//***********************************************************
// Private Methods
//***********************************************************
//-----------------------------------------------------------
// Check if error exists in the object processes
// ---
// bool		OUT		N/A		- Returns false if any error exists in the object processes
// ---
//-----------------------------------------------------------
function _ErrorExists()
	{
	return false;
	}


//-----------------------------------------------------------
// Compares the versions
// ---
// String   IN      Version1        - The first version
// String   IN      Version2        - The second version
// String   IN      VersionType     - MySQL, PHP, Zend or OEM
// Integer  OUT     -               - Returns 1 or 2. 1 is the first, 2 is the second version greater
// ---
//-----------------------------------------------------------
function CheckVersions($Version1, $Version2, $VersionType)
    {
    // Initialize variables
    $GreaterVersion = 0;
    
    
    if ($VersionType == "PHP")
        {
        // Check & Compare PHP versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "Zend")
        {
        // Check & Compare Zend Engine versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    
    return $GreaterVersion;
    }







};
//***********************************************************
// End of the class
//***********************************************************

?>