<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-06-01 22:44        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/


/*

Class Explanation :
    With this class, you can benchmark your algorithms easily.
    
Class Usage :
    $ObjectTimer  =  new  Benchmark();
        ... continue to your other processes ... 

    $ObjectTimer->Calculate("label1");
        ... continue to your other processes ... 
    $ObjectTimer->Calculate("label2");
        ... continue to your other processes ... 
    $ObjectTimer->Results();

*/

class  Benchmark
{
// PROPERTIES
    var $BaseTime		= 0;
    var $TotalTime		= 0;
    var $RPTTimes       = array();
	var $ArrayGroups	= array();
        
// METHODS

//--------[ Constructor
function Benchmark()
    {
    $this->BaseTime        = microtime();

    // Make initialization
    ereg("^([^ ]+) (.+)", $this->BaseTime, $ArrayMatches);
    $this->BaseTime = doubleval ($ArrayMatches[2]) + doubleval($ArrayMatches[1]);

    } // end of function benchmark()

//--------[ Add new label and calculate the time
function Calculate($Label)
    {
	// Enable this function to start benchmarking (this may cause memory overload!!!)
	return;
	
    // Record current elapsed time, accumulate
    $NewTime = microtime();
    ereg("^([^ ]+) (.+)", $NewTime, $ArrayMatches);
    $NewTime = doubleval ($ArrayMatches[2]) + doubleval($ArrayMatches[1]);
	$NewTimeFloat = $NewTime;


    $Difference = $NewTime - $this->BaseTime;
	
	if (function_exists('memory_get_usage') == true)
		{
		$Label .= ' ('.number_format(round(memory_get_usage()) / 1024).')';
		}
	
    $this->RPTTimes[] = array($Label, $Difference, $NewTimeFloat);
    $this->BaseTime = $NewTime;
    $this->TotalTime += $Difference;

    return;
    } // end of function calculate()
    
//--------[ Print results
function Results($ReturnType = 'Screen')
    {
	if ($ReturnType == 'Screen')
		{
		print ("<br><br><br><br><br><br><br><br><br><br><br><br>");
		print ("<br><hr><pre>");
		print ("<b><u>Benchmark Results (in seconds)</u></b>\n");
	
		$ArrayRankColors = array('#FF0000', '#FF9933', '#006633', '#666666');
	
		foreach ($this->RPTTimes as $EachIndex=>$EachArrayBenchmark)
			{
			$Key	= $EachArrayBenchmark[0];
			$Val	= $EachArrayBenchmark[1];
			$Time	= $EachArrayBenchmark[2];
	
			if ($Val >= '0.01')
				{
				$RankColor = $ArrayRankColors[0];
				}
			elseif (($Val <= '0.009') && ($Val >= '0.001'))
				{
				$RankColor = $ArrayRankColors[1];
				}
			elseif (($Val <= '0.0009') && ($Val >= '0.0001'))
				{
				$RankColor = $ArrayRankColors[2];
				}
			else
				{
				$RankColor = $ArrayRankColors[3];
				}
			
			print ('<font color="'.$RankColor.'">');
			printf ("%'.-80.80s: %.5f %'.-5.6s %010.5f %s", $Key, $Val, "", $Time, "\n");
			print ("</font>");
			}
	
	
		print ("<b>");
			printf ("%'.-80.80s: %.5f %s","TOTAL PROCESS TIME ELAPSED",$this->TotalTime,"\n");
		print ("</b>");
	
		print ("</pre><hr>");
		}
	elseif ($ReturnType == 'CSV-Download')
		{
		$ArrayCSV = array();
		
		foreach ($this->RPTTimes as $EachIndex=>$EachArrayBenchmark)
			{
			$Key	= $EachArrayBenchmark[0];
			$Val	= $EachArrayBenchmark[1];
			$Time	= $EachArrayBenchmark[2];
	
			$ArrayCSV[] = "'".implode("','", $EachArrayBenchmark)."'";
			}
	
		$ArrayCSV = implode("\n", $ArrayCSV);

		header("Content-type: application/octetstream");
		header("Content-Length: ".strlen($ArrayCSV));
		header("Content-Disposition: attachment; filename=\"BenchmarkResults.csv\"");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: public");

		print $ArrayCSV;
		
		exit;
		}
	    
    return;
    }    // end of function function Results()
    
}; // end of class benchmark

?>