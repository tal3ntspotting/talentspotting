<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth,  5 September, 2007
 * @package default
 **/

/**
 * Engine class for SwiftMailer
 **/

class OctethSwiftEngine
{
/**
 * Send Engine Type ('SMTP', 'NativeMail', 'SendMail')
 *
 * @var string
 **/
var $SendEngine			= 'NativeMail';

/**
 * The path of the sendmail, qmail, etc. (default is empty)
 *
 * @var string
 **/
var $SendMailPath			= '';

/**
 * SMTP Host
 *
 * @var string
 **/
var $SMTPHost			= 'localhost';

/**
 * Authorization mode of SMTP server
 *
 * @var boolean
 **/
var $SMTPAuth			= false;

/**
 * Username of SMTP server
 *
 * @var string
 **/
var $SMTPUsername		= '';

/**
 * Password of SMTP server
 *
 * @var string
 **/
var $SMTPPassword		= '';

/**
 * SMTP Port number (default 25)
 *
 * @var integer
 **/
var $SMTPPort			= 25;

/**
 * SSL mode of SMTP server
 *
 * @var boolean
 **/
var $SMTPSSL			= false;

/**
 * Time out seconds for the connection
 *
 * @var integer
 **/
var $TimeOut			= 10;

/**
 * Content type of the email ('HTML', 'Plain', 'MultiPart')
 *
 * @var string
 **/
var $ContentType		= 'Plain';

/**
 * Main content of the email
 *
 * @var string
 **/
var $MainContent		= '';

/**
 * Alternate content of the email (always plain)
 *
 * @var string
 **/
var $AlternateContent	= '';

/**
 * Subject of the email
 *
 * @var string
 **/
var $Subject			= '';

/**
 * Attachment list (filename => path)
 *
 * @var array
 **/
var $AttachmentList		= array();

/**
 * Image embedding mode
 *
 * @var boolean
 **/
var $EmbedImages		= false;

/**
 * List of all image objects in the email message for embedding
 *
 * @var array
 **/
var $ArrayEmbedObjects	= array();

/**
 * From name
 *
 * @var string
 **/
var $FromName			= '';

/**
 * From email address
 *
 * @var string
 **/
var $FromEmail			= '';

/**
 * To name
 *
 * @var string
 **/
var $ToName				= '';

/**
 * To email address
 *
 * @var string
 **/
var $ToEmail			= '';

/**
 * Reply-to name
 *
 * @var string
 **/
var $ReplyToName		= '';

/**
 * Reply-to email address
 *
 * @var string
 **/
var $ReplyToEmail		= '';

/**
 * Return path email address
 *
 * @var string
 **/
var $ReturnPath			= '';

/**
 * Character Set (default utf-8)
 *
 * @var string
 **/
var $CharSet			= 'utf-8';

/**
 * Priority of the email ('1', '3', '5')
 *
 * @var integer
 **/
var $Priority			= 3;

/**
 * Array which keeps custom header list (name => value)
 *
 * @var array
 **/
var $EmailHeaderList	= array();

/**
 * Absolute path of the SwiftMailer
 *
 * @var string
 **/
var $SwiftMailerPath	= '';

/**
 * Error status
 *
 * @var boolean
 **/
var $ErrorStatus		= false;

/**
 * Error messages
 *
 * @var array
 **/
var $ErrorInfo			= array();

/**
 * SwiftMailer object
 *
 * @var object
 **/
var $ObjectSwift		= '';

/**
 * Connection object for SwiftMailer
 *
 * @var object
 **/
var $ObjectConnection	= '';

/**
 * Message object for SwiftMailer
 *
 * @var object
 **/
var $ObjectMessage		= '';

/**
 * Image embedding object for SwiftMailer
 *
 * @var object
 **/
var $ObjectEmbed		= '';

/**
 * Excluded images in array to avoid embedding
 *
 * @var array
 **/
var $ArrayEmbedExcludedObjects = array();

/**
 * Configuration Array
 *
 * @var array
 **/
var $ArrayConfig = array();

/**
 * Cache directory for storing images until they are embedded
 *
 * @var string
 **/
var $EmbedCacheDir		= '';

/**
 * If you enter a path to this variable, Swift will use disk caching instead of memory
 *
 * @var string
 **/
var $CacheToDiskPath	= '';

/**
 * Enable or disable log
 *
 * @var boolean
 **/
var $EnableLog			= false;

/**
 * Array Campaign Statistics
 *
 * @var array
 **/	
var $ArrayCampaignStatistics;	

/**
 * Constructor
 *
 * @param string $SwiftMailerPath Absolute SwiftMailer path (Ex: /var/www/html/classes)
 * @return void
 * @author Cem Hurturk
 **/
function OctethSwiftEngine($SwiftMailerPath)
	{
	if (file_exists($SwiftMailerPath.'/Swift.php') == true)
		{
		// Include the SwiftMailer object - Start
		$this->SwiftMailerPath = $SwiftMailerPath;
		// Include the SwiftMailer object - End
		}
	else
		{
		$this->ErrorStatus	= true;
		$this->ErrorInfo[]	= 'SwiftMailer not found. Please check SwiftMailer path.';
		return;
		}
	}

/**
 * Sets the connection
 *
 * @return void
 * @author Cem Hurturk
 **/

function SetConnection($SentRecipients, $InLoop)
	{
	define(SWIFT_ABS_PATH, $this->SwiftMailerPath);

	// Include the correct send engine class - Start
	if ($this->SendEngine == 'SMTP')
		{
		include_once($this->SwiftMailerPath.'/Swift/Connection/SMTP.php');

		if (count($this->SMTPHost) > 1)
			{
			// Select choosen SMTP ID via Sent Recipients - Start
			$tmpCounter = count($this->SMTPHost) - 1;
			$middlesum	= 0;
			$i			= 0;

			while (true) 
				{ 			
				if($i == ($tmpCounter+1))
					{
					$i = 0;
					}
			
				$middlesum += $this->MaxEmailsPerInterval[$i];
				
				if($middlesum >= $SentRecipients)
					{
 					$choosenSMTP = $i;
					break;
					}

				$i++;
				}
			// Select choosen SMTP ID via SentRecipients - End
			
			if ($choosenSMTP != $_SESSION['choosenSMTP'])
				{
				sleep($this->SecondsDelayPerInterval[$choosenSMTP]);
				}

			if(($InLoop == '') || ($InLoop == false))
				{		
				$_SESSION['choosenSMTP'] = $choosenSMTP;
	
				$this->ObjectConnection =& new Swift_Connection_SMTP($this->SMTPHost[$choosenSMTP], $this->SMTPPort[$choosenSMTP], ($this->SMTPSSL[$choosenSMTP] == true ? '2' : ''));

				$this->ObjectConnection->setUsername($this->SMTPUsername[$choosenSMTP]);
				$this->ObjectConnection->setPassword($this->SMTPPassword[$choosenSMTP]);
				$this->ObjectConnection->setTimeout($this->TimeOut[$choosenSMTP]);				
				}
			elseif ($choosenSMTP != $_SESSION['choosenSMTP'])
				{
				$_SESSION['choosenSMTP'] = $choosenSMTP;			

				$this->ObjectConnection =& new Swift_Connection_SMTP($this->SMTPHost[$choosenSMTP], $this->SMTPPort[$choosenSMTP], ($this->SMTPSSL[$choosenSMTP] == true ? '2' : ''));

				$this->ObjectConnection->setUsername($this->SMTPUsername[$choosenSMTP]);
				$this->ObjectConnection->setPassword($this->SMTPPassword[$choosenSMTP]);
				$this->ObjectConnection->setTimeout($this->TimeOut[$choosenSMTP]);
				}
			}
		else
			{
			if($InLoop=='' OR $InLoop==false)
				{
				$this->ObjectConnection =& new Swift_Connection_SMTP($this->SMTPHost[0], $this->SMTPPort[0], ($this->SMTPSSL[0] == true ? '2' : ''));

				$this->ObjectConnection->setUsername($this->SMTPUsername[0]);
				$this->ObjectConnection->setPassword($this->SMTPPassword[0]);
				$this->ObjectConnection->setTimeout($this->TimeOut[0]);
				}
			else
				{
				if($SentRecipients % $this->MaxEmailsPerInterval[0] == 0)
					{
					sleep($this->SecondsDelayPerInterval[0]);	
					}
				}				
			}
		}
	elseif ($this->SendEngine == 'NativeMail')
		{
		include_once($this->SwiftMailerPath.'/Swift/Connection/NativeMail.php');
		$this->ObjectConnection =& new Swift_Connection_NativeMail();
		}
	elseif ($this->SendEngine == 'SendMail')
		{
		include_once($this->SwiftMailerPath.'/Swift/Connection/Sendmail.php');
		
		// Check if any sendmail parameter provided - Start
		$getParts=explode(" ",$this->SendMailPath);
		// Check if any sendmail parameter provided - End
		
		// If any parameter not provided, instantiate the sendmail connector with "auto-detect" feature - Start
		if(count($getParts)<=1)
			{
			$this->ObjectConnection =& new Swift_Connection_Sendmail();
			}
		else
			{
			$this->ObjectConnection =& new Swift_Connection_Sendmail($this->SendMailPath);
			}
		// If any parameter not provided, instantiate the sendmail connector with "auto-detect" feature - End
					
		// Set the timout - Start
		$this->ObjectConnection->setTimeout = $this->TimeOut;
		// Set the timout - End
		
		}
	// Include the correct send engine class - Start
	}

/**
 * Disconnect the connection
 *
 * @return void
 * @author Cem Hurturk
 **/
function Disconnect()
	{
	$this->ObjectSwift->disconnect();
	}

/**
* Builds the message
*
* @return string
* @author Cem Hurturk
**/
function SConnector()    
	{
	// Generate the Swift object - Start
	$this->ObjectSwift =& new Swift($this->ObjectConnection);
	// Generate the Swift object - End		
	}

/**
 * Builds the message
 *
 * @return string
 * @author Cem Hurturk
 **/
function BuildMessage()
	{
	// Generate the Swift object - Start
	//$this->ObjectSwift =& new Swift($this->ObjectConnection);
	// Generate the Swift object - End

	// Set caching method - Start
	if ($this->CacheToDiskPath != '')
		{
		Swift_CacheFactory::setClassName("Swift_Cache_Disk");
		Swift_Cache_Disk::setSavePath($this->CacheToDiskPath);
		}
	// Set caching method - End
	
	// Set logging - Start
	if ($this->EnableLog == true)
		{
		$this->ObjectSwift->log->enable();
		}
	// Set logging - End

	// Create the message class - Start
	if ($this->ContentType == 'Plain')
		{
		if (count($this->AttachmentList) == 0)
			{
			$this->ObjectMessage =& new Swift_Message($this->Subject, $this->MainContent, 'text/plain', '', $this->CharSet);
			}
		else
			{
			$this->ObjectMessage =& new Swift_Message($this->Subject, '', 'text/plain', '', $this->CharSet);

			// Add message parts - Start
			$this->ObjectMessage->attach(new Swift_Message_Part($this->MainContent));
			// Add message parts - End
			}
		}
	elseif ($this->ContentType == 'HTML')
		{
			
		if ((count($this->AttachmentList) == 0) && ($this->EmbedImages == false))
			{

			$this->ObjectMessage =& new Swift_Message($this->Subject, $this->MainContent, 'text/html', '', $this->CharSet);
			}
		else
			{
			$this->ObjectMessage =& new Swift_Message($this->Subject, '', 'text/html', '', $this->CharSet);

			// Embed images - Start
			if ($this->EmbedImages == true)
				{
				$this->EmbedImages();
				}
			// Embed images - End
			// Add message parts - Start
			$this->ObjectMessage->attach(new Swift_Message_Part($this->MainContent, 'text/html'));
			// Add message parts - End
			}
		}
	elseif ($this->ContentType == 'MultiPart')
		{
		$this->ObjectMessage =& new Swift_Message($this->Subject, '', 'text/plain', '', $this->CharSet);

		// Embed images - Start
		$this->EmbedImages();
		// Embed images - End

		// Add message parts - Start
		$this->ObjectMessage->attach(new Swift_Message_Part($this->MainContent, 'text/html'));
		$this->ObjectMessage->attach(new Swift_Message_Part($this->AlternateContent));
		// Add message parts - End
		}

	$this->ObjectMessage->setPriority($this->Priority);
	$this->ObjectMessage->setReturnPath($this->ReturnPath);
	// Create the message class - End

	// Attach files - Start
	$this->AttachFiles();
	// Attach files - End
	
	// Add custom headers - Start
	$this->AddCustomHeaders();
	// Add custom headers - End
	}

/**
 * Sends or returns the email
 *
 * @param boolean $SendEmail Sends the email to MTA if set to true
 * @param boolean $ReturnEmail Returns the raw email content if set to true
 * @return string
 * @author Cem Hurturk
 **/
function SendMessage($SendEmail, $ReturnEmail)
	{
	$this->ObjectMessage->setFrom(new Swift_Address($this->FromEmail, $this->FromName));
	$this->ObjectMessage->setReplyTo(new Swift_Address($this->ReplyToEmail, $this->ReplyToName));
	$this->ObjectMessage->setTo(new Swift_Address($this->ToEmail, $this->ToName));

	$ObjectStream =& $this->ObjectMessage->build();

	// Send the email - Start
	if ($SendEmail == true)
		{
		// $log =& Swift_LogContainer::getLog();
		// $log->setLogLevel(4);

		$Result = $this->ObjectSwift->send($this->ObjectMessage, new Swift_Address($this->ToEmail, $this->ToName), new Swift_Address($this->FromEmail, $this->FromName));
		}
	// Send the email - End
	
	// Build the email and return it - Start
	if ($ReturnEmail == true)
		{
		return $ObjectStream->readFull();
		}
	// Build the email and return it - End
	
	// Output debug message - Start
	if ($this->ArrayConfig['Settings']['SystemDebugMode'] == 'Enabled')
		{
		print "Sent to ".$this->ToEmail." -- ".number_format(round(memory_get_usage() / 1024))."KBytes<br />\n";
		}
	// Output debug message - End
	}


/**
 * Embed images into email
 *
 * @return void
 * @author Cem Hurturk
 **/
function EmbedImages()
	{
	// Proceed if image embedding is ON and content type is HTML or BOTH - Start
	if (($this->EmbedImages == true) && (($this->ContentType == 'HTML') || ($this->ContentType == 'MultiPart')))
		{
		// Find all image objects in the message - Start
		if (count($this->ArrayEmbedObjects) == 0)
			{
			$this->ArrayEmbedObjects = $this->DetectImagesInMessage($this->MainContent);
			}
		// Find all image objects in the message - End

		// Loop each object, fetch and insert them - Start
		foreach ($this->ArrayEmbedObjects as $EachObject)
			{
			$isImage			 = true;
			$ArrayReplacePattern = array();
			$ArrayReplaceReplace = array();
			
			// Store images in cache directory temporarily - Start
			if ($this->EmbedCacheDir != '')
				{
				$FileName = basename($EachObject);

				// Fetch image if it does not already exist in the cache folder (not previously fetched) - Start
				if (file_exists($this->EmbedCacheDir.'/'.$FileName) == false)
					{
					if (extension_loaded('curl') == true)
						{
						$CurlHandler = curl_init();
							curl_setopt($CurlHandler, CURLOPT_URL, $EachObject);
							curl_setopt($CurlHandler, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($CurlHandler, CURLOPT_USERAGENT, $this->ArrayConfig['Branding']['ProductName']);

						$FileBuffer = curl_exec($CurlHandler);
						curl_close($CurlHandler);
						}
					else
						{
				        $FileHandler = fopen($EachObject, "rb");
				        $FileBuffer = fread($FileHandler, filesize($EachObject));
						if ($FileBuffer == '')
							{
							$FileBuffer = implode("", file($EachObject));
							}
	        			fclose($FileHandler);
						}
				
			        $FileHandler = fopen($this->EmbedCacheDir.'/'.$FileName, "w");
						fwrite($FileHandler, $FileBuffer);
	       			fclose($FileHandler);
					}
				else
					{
					$isImage = true;
					}
				// Fetch image if it does not already exist in the cache folder (not previously fetched) - End
				}
			// Store images in cache directory temporarily - End
			
			// Check if its image - Start
			if ($isImage == false)
				{
				if(!getimagesize($this->EmbedCacheDir.'/'.$FileName))
					{
					$isImage = false;
					}
				}
			// Check if its image - End
			
			// Embed image into the message body - Start
			if($isImage == true)
				{
				$ObjectImage =& new Swift_Message_Image(new Swift_File($this->EmbedCacheDir.'/'.$FileName));
					
				$CID = $this->ObjectMessage->attach($ObjectImage);
	
				$ArrayReplacePattern[]	=	'src="'.$EachObject.'"';
				$ArrayReplacePattern[]	=	'background-image:url('.$EachObject.');';
				$ArrayReplacePattern[]	=	'background="'.$EachObject.'"';
				$ArrayReplacePattern[]	=	'<link href="'.$EachObject.'"';

				$ArrayReplaceReplace[]	=	'src="'.$CID.'"';
				$ArrayReplaceReplace[]	=	'background-image:url('.$CID.');';
				$ArrayReplaceReplace[]	=	'background="'.$CID.'"';
				$ArrayReplaceReplace[]	=	'<link href="'.$CID.'"';
				
				$this->MainContent = str_replace($ArrayReplacePattern, $ArrayReplaceReplace, $this->MainContent);
				}
			// Embed image into the message body - End
			}
		// Loop each object, fetch and insert them - End
		}
	// Proceed if image embedding is ON and content type is HTML or BOTH - End
	}

/**
 * Attach files into email
 *
 * @return void
 * @author Cem Hurturk
 **/
function AttachFiles()
	{
	foreach ($this->AttachmentList as $EachFilename=>$EachPath)
		{
		$this->ObjectMessage->attach(new Swift_Message_Attachment(new Swift_File($EachPath), $EachFilename));
		}
	}

/**
 * Adds custom headers into the message header
 *
 * @return void
 * @author Cem Hurturk
 **/
function AddCustomHeaders()
	{
	foreach ($this->EmailHeaderList as $EachName=>$EachValue)
		{
		$this->ObjectMessage->headers->set($EachName, $EachValue);
		}
	}

/**
 * Returns failired recipients in an array
 *
 * @return array
 * @author Cem Hurturk
 **/
function GetFailedRecipients()
	{
	return ($this->ObjectSwift->log->getFailedRecipients());
	}

/**
 * Detects images in the message returns the list
 *
 * @param string $Content Detects images and other objects which can be embedded into the email
 * @return array
 * @author Cem Hurturk
 **/
function DetectImagesInMessage($Content)
	{
	$ArrayObjects = array();

	// src= - STARTED
		$Pattern = "/src\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - Start
			$SkipEmbed = false;
			foreach ($this->ArrayEmbedExcludedObjects as $EachExcluded)
				{
				if (substr($EachMatch[2], 0, strlen($EachExcluded)) == $EachExcluded)
					{
					$SkipEmbed = true;
					continue;
					}
				}

			if ($SkipEmbed == false)
				{
				$ArrayObjects[] = $EachMatch[2];
				}
			// Check if this image is the image of read tracking module. If yes, skip it, else embed it - End
			}
		}
	// src= - FINISHED

	// background-image:url( - STARTED
		$Pattern = "/background\-image\:[\s|]url\((.*)\)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayImages[] = $EachMatch[1];
			}
		}
	// background-image:url( - FINISHED

	// background=" - STARTED
		$Pattern = "/background\=(\"|\'|)(.*)(\"|\'|\>)/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// background=" - FINISHED

	// <link href=" - STARTED
		$Pattern = "/\<link href\=(\"|\'|)(.*)(\"|\')/iU";
	if (preg_match_all($Pattern, $Content, $ArrayMatches, PREG_SET_ORDER))
		{
		foreach ($ArrayMatches as $EachMatch) 
			{
			$ArrayObjects[] = $EachMatch[2];
			}
		}
	// <link href=" - FINISHED

	$ArrayObjects = array_flip(array_flip($ArrayObjects));

	$ArrayParsedURL = parse_url($this->ArrayConfig['URLs']['Software']);


	$NewArrayObjects=array();

	foreach ($ArrayObjects as $EachObject)
		{
		// If the object path is defined starting with '/', add website URL to the beginning - STARTED
		if (substr($EachObject, 0, 1) == '/')
			{
		
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].$EachObject;
		
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		elseif (substr($EachObject, 0, 2) == './')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 1, strlen($EachObject));
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		elseif (substr($EachObject, 0, 3) == '../')
			{
			$EachObjectRenamed = $ArrayParsedURL['scheme']."://".$ArrayParsedURL['host'].substr($EachObject, 2, strlen($EachObject));
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		else
			{
			$EachObjectRenamed = $EachObject;
			$NewArrayObjects[]=$EachObjectRenamed;
			}
		// If the object path is defined starting with '/', add website URL to the beginning - FINISHED

		}

	$ArrayObjectsNew=array();
	$ArrayObjectsNew=$NewArrayObjects;

	$this->MainContent = str_replace($ArrayObjects, $ArrayObjectsNew, $Content);

	return $ArrayObjects;
	}

} // end of class OctethSwiftEngine
?>