<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2003-02-22 21:46        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ HTTP HANDLING CLASS
----[ SYNOPSIS

include_once("./system/classes/class_octethhttp.php");

	$ConnectionMethod		= 'POST';
	$Host					= 'www.octeth.com';
	$Port					= 80;
	$Path					= '/newsletter/subscribe.php';

	$ArrayParameters		= array(
									"FormValue_MailListIDs[]"			=>	"2",
									"FormValue_Email"					=>	$_POST['FormValue_Email'],
									"FormValue_SuccessURL"				=>	"http://www.octeth.com/newsletter/subscription_success.html",
									"FormValue_FailureURL"				=>	"http://www.octeth.com/newsletter/subscription_failure.html",
									"Subscribe"							=>	"Submit",
									);
	$UserAgent				= 'Octeth HTTP v1.0';
	$ArrayOtherHeaders		= array();
$ObjectHTTP = new OctethHTTP($ConnectionMethod, $Host, $Port, $Path, $ArrayParameters, $UserAgent, $ArrayOtherHeaders);




----[ DESCRIPTION
With this module, you can post/get
and perform any HTTP connections

----[ CLASS NAME AND VERSION
OctethHTTP v1.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


********************************************************************
********************************************************************
********************************************************************
************** DO NOT FORGET TO UPDATE THE *************************
********* LAST MODIFY DATE AND TIME AFTER MAKING *******************
********************** MODIFICATIONS *******************************
********************************************************************
********************************************************************
********************************************************************

*/

//***********************************************************
// OctethHTTP Class
//***********************************************************
class OctethHTTP
{
//***********************************************************
// Public Properties
//***********************************************************
// Connection method (POST,GET,HEAD,PUT)
var $ConnectionMethod		=		'POST';

// Host to connect (http://www.google.com)
var $Host					=		'';

// Port of the host to connect (80)
var $Port					=		'';

// Path to reach (/submit/submit.php)
var $Path					=		'';

// Parameters
var $ArrayParameters		=		array();

// User agent (OctethHTTP v1.x)
var $UserAgent				=		'OctethHTTP v1.x';

// Other headers
var $ArrayOtherHeaders		=		array();


//***********************************************************
// Private Properties
//***********************************************************
var $_ErrorExists				=	false;
var $_RequestLength				=	0;

//***********************************************************
// Public Methods
//***********************************************************

//-----------------------------------------------------------
// Constructor function
// ---
// String	IN		ConnectionMethod		- Method of connection
// String	IN		Host					- Host to connect
// String	IN		Port					- Port to connect
// String	IN		Path					- Path to reach
// Array	IN		ArrayParameters			- Parameters to submit (optional)
// String	IN		UserAgent				- User agent header
// Array	IN		ArrayOtherHeaders		- Other headers to send
// void		OUT		N/A		- Returns nothing
// ---
//-----------------------------------------------------------
function OctethHTTP($ConnectionMethod, $Host, $Port, $Path, $ArrayParameters, $UserAgent, $ArrayOtherHeaders)
	{
	$this->ConnectionMethod		=		$ConnectionMethod;
	$this->Host					=		$Host;
	$this->Port					=		$Port;
	$this->Path					=		$Path;
	$this->ArrayParameters		=		$ArrayParameters;
	$this->UserAgent			=		$UserAgent;
	$this->ArrayOtherHeaders	=		$ArrayOtherHeaders;
	
	return true;
	}

//-----------------------------------------------------------
// Connect to target
// ---
// void		IN		N/A		- N/A
// void		OUT		N/A		- Returns nothing
// ---
//-----------------------------------------------------------
function Connect()
	{
	// This is a must-have for PHP >=v4.3 versions, otherwise process will hang while retrieving the result content - STARTED
	ini_set('auto_detect_line_endings', '1');
	// This is a must-have for PHP >=v4.3 versions, otherwise process will hang while retrieving the result content - FINISHED
	
	$RequestString = $this->_BuildRequestString();

	// Build header - STARTED
	$ArrayHeader	=	array();
	
	if ($this->ConnectionMethod == 'POST')
		{
		$ArrayHeader[]	=	"POST ".$this->Path." HTTP/1.0";
		$ArrayHeader[]	=	"Host: ".$this->Host;
		$ArrayHeader[]	=	"User-Agent: ".$this->UserAgent;
		$ArrayHeader[]	=	"Content-type: application/x-www-form-urlencoded";
		$ArrayHeader[]	=	"Content-length: ".$this->_RequestLength;
		$ArrayHeader[]	=	"\n";
		$Headers = implode("\n", $ArrayHeader);
		}
	elseif ($this->ConnectionMethod == 'GET')
		{
		$ArrayHeader[]	=	"GET ".$this->Path."?".$RequestString." HTTP/1.0";
		$ArrayHeader[]	=	"Host: ".$this->Host;
		$ArrayHeader[]	=	"User-Agent: ".$this->UserAgent;
		$ArrayHeader[]	=	"Content-type: application/x-www-form-urlencoded";
		$ArrayHeader[]	=	"Content-length: ".$this->_RequestLength;
		$ArrayHeader[]	=	"\n";
		$Headers = implode("\n", $ArrayHeader);
		}
	else
		{
		$ArrayHeader[]	=	$this->ConnectionMethod." ".$this->Path."?".$RequestString." HTTP/1.0";
		$ArrayHeader[]	=	"Host: ".$this->Host;
		$ArrayHeader[]	=	"User-Agent: ".$this->UserAgent;
		$ArrayHeader[]	=	"\n";
		$Headers = implode("\n", $ArrayHeader);
		}		
	// Build header - FINISHED

	// Open connection - STARTED
    $SocketHandler = fsockopen($this->Host, $this->Port, $ErrorNumber, $ErrorMessage, 30);
	
	if (version_compare('4.3.0', phpversion(), '<') == true)
		{
		stream_set_blocking($SocketHandler, 0);
		stream_set_timeout($SocketHandler, 2);
		}
	// Open connection - FINISHED

	// Couldn't connect - STARTED
    if($SocketHandler == false)
		{ 
		return false;
		}
	// Couldn't connect - FINISHED

	// Send data - STARTED
    fputs($SocketHandler, $Headers.$RequestString);
	// Send data - FINISHED

	// Remove HTTP headers - STARTED
    while(trim(fgets($SocketHandler, 4096)) != '');
	// Remove HTTP headers - FINISHED

	// Retrieve the response - STARTED
	$Response = '';
    while (!feof($SocketHandler))
		{
        $Response .= fgets($SocketHandler, 4096);
	    }
	// Retrieve the response - FINISHED

	// Close connection - STARTED
	fclose($SocketHandler);
	// Close connection - FINISHED

	return $Response;
	}


	
//***********************************************************
// Private Methods
//***********************************************************
//-----------------------------------------------------------
// Builds request string
// ---
// String	OUT		N/A		- Returns generated request string
// ---
//-----------------------------------------------------------
function _BuildRequestString()
	{
	$RequestString = array();
	
	foreach ($this->ArrayParameters as $Key=>$Val)
		{
		$RequestString[]	=	$Key."=".urlencode($Val);
		}
	$RequestString = implode("&", $RequestString);
	$this->_RequestLength = strlen($RequestString);
	
	return $RequestString;
	}

//-----------------------------------------------------------
// Check if error exists in the object processes
// ---
// bool		OUT		N/A		- Returns false if any error exists in the object processes
// ---
//-----------------------------------------------------------
function _ErrorExists()
	{
	return false;
	}


//-----------------------------------------------------------
// Compares the versions
// ---
// String   IN      Version1        - The first version
// String   IN      Version2        - The second version
// String   IN      VersionType     - MySQL, PHP, Zend or OEM
// Integer  OUT     -               - Returns 1 or 2. 1 is the first, 2 is the second version greater
// ---
//-----------------------------------------------------------
function CheckVersions($Version1, $Version2, $VersionType)
    {
    // Initialize variables
    $GreaterVersion = 0;
    
    
    if ($VersionType == "PHP")
        {
        // Check & Compare PHP versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "MySQL")
        {
        // Check & Compare MySQL versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "Zend")
        {
        // Check & Compare Zend Engine versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    elseif ($VersionType == "OEM")
        {
        // Check & Compare OEM versions
        $TMPVersionPart1 = explode(".", $Version1);
        $TMPVersionPart2 = explode(".", $Version2);
        
        for ($TMPCounter = 0; $TMPCounter < count($TMPVersionPart1); $TMPCounter++)
            {
            $TMPVersionPart1[$TMPCounter] = sprintf("%03d", $TMPVersionPart1[$TMPCounter]);
            $TMPVersionPart2[$TMPCounter] = sprintf("%03d", $TMPVersionPart2[$TMPCounter]);
            }

        $TMPVersion1 = implode("", $TMPVersionPart1);
        $TMPVersion2 = implode("", $TMPVersionPart2);
            settype($TMPVersion1, "integer");
            settype($TMPVersion2, "integer");
         
        $HigherVersion = max($TMPVersion1, $TMPVersion2);
        
        if ($HigherVersion == $TMPVersion1)
            {
            $GreaterVersion = 1;
            }
        else
            {
            $GreaterVersion = 2;
            }
        }
    
    return $GreaterVersion;
    }

};
//***********************************************************
// End of the class
//***********************************************************

?>