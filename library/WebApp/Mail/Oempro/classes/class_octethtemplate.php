<?php
/*
+-------------------------------------------+
| (c)Copyright Octeth Technologies.         |
| All rights reserved.                      |
+-------------------------------------------+
| www.octeth.com                            |
| octeth-support@octeth.com                 |
+-------------------------------------------+
| Last Update Date: 2002-10-03 15:41        |
+-------------------------------------------+
|                                           |
+-------------------------------------------+
*/

/*
----[ TEMPLATE PARSING CLASS
----[ SYNOPSIS

///////////////////////// HTML Code
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<p>_PageName_</p>
<table width="750"  border="0" cellspacing="1" cellpadding="1">
<LIST:ONE>
  <tr bgcolor="#CCCCCC"> 
    <td>_ListName_</td>
    <td>_ListValue_
	<br>
		<table width="100%"  border="0" cellspacing="1" cellpadding="1">
			<LIST:TWO>
			  <tr bgcolor="#FF0000"> 
				<td>_ListSubName_</td>
				<td>_ListSubValue_
				<br>
					<table width="100%"  border="0" cellspacing="1" cellpadding="1">
						<LIST:THREE>
						  <tr bgcolor="#CC0000"> 
							<td>_ListSub2Name_</td>
							<td>_ListSub2Value_
							<br>
							
							</td>
						  </tr>
						</LIST:THREE>
					</table>
				</td>
			  </tr>
			</LIST:TWO>
		</table>
	</td>
  </tr>
</LIST:ONE>
</table>
<p>&nbsp;</p>
</body>
</html>



///////////////////////// PHP Code
include_once("class_octethtemplate.php");

$ObjTemplate = new OctethTemplate("./test.html", "file");

	$ArrayReplaceList	=	array(
								"_PageName_"		=>		"Cem",
								);
$ObjTemplate->replace($ArrayReplaceList);

$ObjTemplate->define_block("LIST:ONE");
$ObjTemplate->define_block_in_block("LIST:TWO", "LIST:ONE");

$ObjTemplate->remove_block_in_block("LIST:THREE", "LIST:TWO");

$ObjTemplate->define_block_in_block("LIST:THREE", "LIST:TWO");

for ($TMPCounter = 1; $TMPCounter <= 3; $TMPCounter++)
	{
		$ArrayReplaceList = array(
								"_ListSub2Name_"		=>		"--- --- ".$TMPCounter,
								"_ListSub2Value_"	=>		"--- --- "."Turkline",
								);
	$ObjTemplate->duplicate_block_in_block("LIST:THREE", "LIST:TWO", $ArrayReplaceList);
	$ObjTemplate->duplicate_block_in_block("LIST:THREE", "LIST:TWO", $ArrayReplaceList);
	$ObjTemplate->duplicate_block_in_block("LIST:THREE", "LIST:TWO", $ArrayReplaceList);

		$ArrayReplaceList = array(
								"_ListSubName_"		=>		"--- ".$TMPCounter,
								"_ListSubValue_"	=>		"--- "."Turkline",
								);
	$ObjTemplate->duplicate_block_in_block("LIST:TWO", "LIST:ONE", $ArrayReplaceList);

		$ArrayReplaceList = array(
								"_ListSub2Name_"		=>		"--- --- ".$TMPCounter,
								"_ListSub2Value_"	=>		"--- --- "."Turkline",
								);
	$ObjTemplate->duplicate_block_in_block("LIST:THREE", "LIST:TWO", $ArrayReplaceList);
	$ObjTemplate->duplicate_block_in_block("LIST:THREE", "LIST:TWO", $ArrayReplaceList);

		$ArrayReplaceList = array(
								"_ListSubName_"		=>		"--- ".$TMPCounter,
								"_ListSubValue_"	=>		"--- "."Turkline",
								);
	$ObjTemplate->duplicate_block_in_block("LIST:TWO", "LIST:ONE", $ArrayReplaceList);


		$ArrayReplaceList = array(
								"_ListName_"		=>		$TMPCounter,
								"_ListValue_"		=>		"Turkline",
								);
	$ObjTemplate->duplicate_block("LIST:ONE", "LIST:ONE", $ArrayReplaceList);
	}



print $ObjTemplate->parseandoutput();

exit;







----[ DESCRIPTION
With this module, you will be able to parse templates
easily. Also, this template module supports multi-level
sections like trees. Also supports nested blocks (infinite
level after v4.0)

----[ CLASS NAME AND VERSION
OctethTemplate v4.0

----[ AUTHOR
Octeth Technologies www.octeth.com <help@octeth.com>
Cem Hurturk <cem@octeth.com>


---------------------------------------------------------------------------------------
*/

//-------------------------------------------[ OctethTemplate Class
class OctethTemplate
{
//*************************************************************************
// PUBLIC PROPERTIES
//*************************************************************************
var $Template            		=	'';            // this will keep the template
var $TemplateType	      	  	=	'file';        // this will keep the type of template (file or string)
var $TemplateContent	    	=	'';            // this will keep the generated template
var $ArrayReplaceList 	        =	array();	    // list and values of replaces
var $ArrayBlocks           		=	array();   	 // list and contents of blocks
var $ArrayOrgBlocks        		=	array();   	 // list and contents of blocks
var $Error 		               	=	'';            // keeps error string
var $TMPCounter					=	0;	

//*************************************************************************
// PRIVATE PROPERTIES
//*************************************************************************


//*************************************************************************
// PUBLIC METHODS
//*************************************************************************

//*************************************************************************
// Constructor
// ---
// IN		String		The location of the template file or the string of the template content
// IN		String		The type of the first parameter ("file", "string")
// OUT		Boolean		Returns false if an error occurs
function OctethTemplate($Template, $TemplateType = 'file')
    {
	// Assign the variables
	$this->Template			=		$Template;
	$this->TemplateType		=		$TemplateType;
	
	// If the template type is "file", then check if file exists or not - STARTED
	if ($this->TemplateType == 'file')
		{
		if (file_exists($this->Template))
			{
			$TMPTemplateContent = file($this->Template);
				$TMPTemplateContent = implode('', $TMPTemplateContent);
			$this->TemplateContent = $TMPTemplateContent;
			}
		else
			{
			$this->return_error('Couldn\'t find the template');
			return false;
			}
		}
	else
		{
		$this->TemplateContent = $this->Template;
		}
	// If the template type is "file", then check if file exists or not - FINISHED

	return true;
	}

//*************************************************************************
// Insert each replace list element to the replace queue
// ---
// IN		Array		The replace list
// OUT		Void		N/A
function replace($ArrayReplaceList)
    {
	foreach ($ArrayReplaceList as $Key=>$Value)
		{
        $this->ArrayReplaceList[$Key] = $Value;
        }

	return;
    }

//*************************************************************************
// Define the block and insert the block content into block pool
// ---
// IN		String		The name of the block
// OUT		Void		N/A
function define_block($BlockName)
    {
	// Get the block content and insert it with it's name to associative array
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
	preg_match_all($Pattern, $this->TemplateContent, $Matches, PREG_SET_ORDER);
	$BlockContent = $Matches[0][1];
	
	$this->ArrayBlocks[$BlockName] 		= $BlockContent;
	$this->ArrayOrgBlocks[$BlockName]	= $BlockContent;
	
		$Replace = "<$BlockName::TEMP>";
	$this->TemplateContent = preg_replace($Pattern, $Replace, $this->TemplateContent);

	return;
	}

//*************************************************************************
// Define the block in an existing block and insert the block content into block pool
// ---
// IN		String		The name of the block
// IN		String		The name of the owner block
// OUT		Void		N/A
function define_block_in_block($BlockName, $OwnerBlockName)
    {
	// Get the block content and insert it with it's name to associative array
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
	preg_match_all($Pattern, $this->ArrayBlocks[$OwnerBlockName], $Matches, PREG_SET_ORDER);
	$BlockContent = $Matches[0][1];
	
	$this->ArrayBlocks[$BlockName] 		= $BlockContent;
	$this->ArrayOrgBlocks[$BlockName]	= $BlockContent;
	
		$Replace = "<$BlockName::TEMP>";
	$this->ArrayBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayBlocks[$OwnerBlockName]);

	$this->ArrayOrgBlocks[$OwnerBlockName]	= $this->ArrayBlocks[$OwnerBlockName];

	return;
	}

//*************************************************************************
// Duplicate the block
// ---
// IN		String		The name of the target block
// IN		String		The name of the source block
// IN		Array		The list of the replacements
// OUT		Void		N/A
function duplicate_block($TargetBlock, $WhichBlock, $ArrayReplaceList)
    {
    $WhichBlockContent = $this->ArrayBlocks[$WhichBlock];
    $ToTag = "<$TargetBlock::TEMP>";

    // Process the given ArrayReplaceList on the block content
	foreach ($ArrayReplaceList as $Key=>$Value)
        {
        $WhichBlockContent = str_replace($Key, $Value, $WhichBlockContent);
        }
            
        $Replace = $WhichBlockContent."\n".$ToTag;
    $this->TemplateContent = str_replace($ToTag, $Replace, $this->TemplateContent);

	// Reset the block content
	$this->ArrayBlocks[$WhichBlock] = $this->ArrayOrgBlocks[$WhichBlock];

	return;
	}

//*************************************************************************
// Duplicate the block in an existing block
// ---
// IN		String		The name of the source block
// IN		String		The name of the owner block
// IN		Array		The list of the replacements
// OUT		Void		N/A
function duplicate_block_in_block($WhichBlock, $OwnerBlock, $ArrayReplaceList)
    {
    $WhichBlockContent = $this->ArrayBlocks[$WhichBlock];
    $ToTag = "<$WhichBlock::TEMP>";

    // Process the given ArrayReplaceList on the block content
	foreach ($ArrayReplaceList as $Key=>$Value)
        {
        $WhichBlockContent = str_replace($Key, $Value, $WhichBlockContent);
        }
            
        $Replace = $WhichBlockContent."\n".$ToTag;
    $this->ArrayBlocks[$OwnerBlock] = str_replace($ToTag, $Replace, $this->ArrayBlocks[$OwnerBlock]);

	// Reset the block content
	$this->ArrayBlocks[$WhichBlock] = $this->ArrayOrgBlocks[$WhichBlock];

	return;
	}

//*************************************************************************
// Removes the block from the template
// ---
// IN		String		The name of the block
// OUT		Void		N/A
function remove_block($BlockName)
    {
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
		$Replace = '';
    $this->TemplateContent = preg_replace($Pattern, $Replace, $this->TemplateContent);

	return;
    }

//*************************************************************************
// Removes the block from owner block
// ---
// IN		String		The name of the block
// IN		String		The name of the owner block
// OUT		Void		N/A
function remove_block_in_block($BlockName, $OwnerBlockName)
    {
		$Pattern = "/<$BlockName>([\w\W]*)<\/$BlockName>/U";
		$Replace = '';
    $this->ArrayBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayBlocks[$OwnerBlockName]);
    $this->ArrayOrgBlocks[$OwnerBlockName] = preg_replace($Pattern, $Replace, $this->ArrayOrgBlocks[$OwnerBlockName]);

	return;
    }

//*************************************************************************
// Removes the block from the template
// ---
// IN		Void		N/A
// OUT		String		Parsed template content
function parseandoutput()
    {
    // Remove all trash temporary templates and tags from template_content
    // Clear all block tags (ex: <START> ... </START>) from template_content

	foreach ($this->ArrayBlocks as $Key=>$Value)
        {
            $Pattern	= "<$Key::TEMP>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
            $Pattern    =    "<$Key>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
            $Pattern    =    "</$Key>";
            $Replace	= '';
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
        }
        
    // Parse replaces
	foreach ($this->ArrayReplaceList as $Key=>$Value)
        {
            $Pattern    	= $Key;
            $Replace       	= $Value;
        $this->TemplateContent = str_replace($Pattern, $Replace, $this->TemplateContent);
        }
    
    // Clear all variables except TemplateContent
    $this->clear(true);
    
    return $this->TemplateContent;
    }

//*************************************************************************
// Clears all contents
// ---
// IN		Boolean		N/A
// OUT		Void		N/A
function clear($SkipTemplateContent = false)
    {
    $this->Template 		= "";
    $this->TemplateType 	= "";
    $this->ArrayReplaceList	= array();
    $this->ArrayBlocks		= array();
    $this->Error 			= "";

    if ($SkipTemplateContent == false)
        {
        $this->TemplateContent = "";
        }

    return;
    }



//*************************************************************************
// PRIVATE METHODS
//*************************************************************************

//*************************************************************************
// Returns the error message
// ---
// IN		String		The built-in error message
// OUT		Boolean		Returns false always
function return_error($Message = "")
    {
    $this->Error = "Error: $Message";
    return false;
    }



}; // end of class

?>