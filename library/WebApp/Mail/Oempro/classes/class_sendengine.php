<?php
/**
 * 
 *
 * @author Cem Hurturk
 * @version $Id$
 * @copyright Octeth,  8 September, 2007
 * @package default
 **/

/**
 * Oempro Send Engine Class
 *
 * @package default
 * @author Cem Hurturk
 **/
class SendEngine
{
/**
 * Process Log ID (oemp_process_log table)
 *
 * @var integer
 **/
var $ProcessLogID				= '';

/**
 * Process Log Module Name
 *
 * @var string
 **/
var $ProcessLogModule			= 'cron_sendengine';

/**
 * Config array of Oempro
 *
 * @var array
 **/
var $ArrayConfig				= array();

/**
 * Language array of Oempro
 *
 * @var array
 **/
var $ArrayLanguage				= array();

/**
 * Pending campaign IDs
 *
 * @var array
 **/
var $ArrayPendingCampaignIDs	= array();

/**
 * Campaign information
 *
 * @var array
 **/
var $ArrayCampaign				= array();

/**
 * Campaign send statistics
 *
 * @var array
 **/
var $ArrayCampaignStatistics	= array();

/**
 * The resume flag of the campaign
 *
 * @var boolean
 **/
var $IsCampaignResumed			= false;

/**
 * Campaign owner administrator information
 *
 * @var array
 **/
var $ArrayAdministrator			= array();

/**
 * Campaign clients array
 *
 * @var array
 **/
var $ArrayClients				= array();

/**
 * SMTP servers of the campaign
 *
 * @var array
 **/
var $ArraySMTP					= array();

/**
 * Attachments of the campaign
 *
 * @var array
 **/
var $ArrayAttachments			= array();

/**
 * Content of the campaign
 *
 * @var array
 **/
var $ArrayContent				= array();

/**
 * From email address and name
 *
 * @var array
 **/
var $ArrayFromEmail				= array();

/**
 * Reply-to email address and name
 *
 * @var array
 **/
var $ArrayReplyToEmail			= array();

/**
 * Return-path email address
 *
 * @var array
 **/
var $ArrayReturnPathEmail		= array();

/**
 * Newsletter template of the campaign
 *
 * @var array
 **/
var $ArrayNewsletter			= array();

/**
 * Local mail list total recipients of the campaign
 *
 * @var integer
 **/
var $TotalLocalRecipients		= 0;

/**
 * Outsourced mail list total recipients of the campaign
 *
 * @var integer
 **/
var $TotalOutsourcedRecipients	= 0;

/**
 * Recipient mail list array
 *
 * @var array
 **/
var $ArrayCampaignMailLists		= array();

/**
 * Start time of the sending in seconds
 *
 * @var integer
 **/
var $BenchmarkStartTime			= 0;

/**
 * Finish time of the sending in seconds
 *
 * @var integer
 **/
var $BenchmarkFinishTime		= 0;

/**
 * Start time of the interval in seconds
 *
 * @var integer
 **/
var $BenchmarkIntervalStartTime	= 0;

/**
 * Finish time of the interval in seconds
 *
 * @var integer
 **/
var $BenchmarkIntervalFinishTime= 0;

/**
 * Average emails per second
 *
 * @var double
 **/
var $BenchmarkAvgEmailPerSec	= 0;

/**
 * Instant emails per second
 *
 * @var double
 **/
var $BenchmarkInstEmailPerSec	= 0;

/**
 * Total looped members
 *
 * @var integer
 **/
var $TotalLoopedMembers			= 0;

/**
 * Looped members for load balancing
 *
 * @var integer
 **/
var $LoadBalanceLoopedMembers	= 0;

/**
 * Total sent members
 *
 * @var integer
 **/
var $TotalSentMembers			= 0;

/**
 * Total failed members
 *
 * @var integer
 **/
var $TotalFailedMembers			= 0;

/**
 * Multi-SMTP process log
 *
 * @var array
 **/
var $ArraySMTPProcessLog		= array();

/**
 * Mailer clas ('SwiftMailer', 'PHPMailer')
 *
 * @var string
 **/
var $MailerClass				= 'SwiftMailer';

/**
 * Mailer object
 *
 * @var object
 **/
var $ObjectMailer				= '';

/**
 * File handler for failed recipients
 *
 * @var handler
 **/
var $ErrorFileHandler			= '';

/**
 * Pending recipients amount
 *
 * @var integer
 **/
var $TotalPendingRecipients		= 0;

/**
 * Loop amount for sending to all pending recipients
 *
 * @var integer
 **/
var $LoopAmount					= 0;

/**
 * Number of email sending per loop
 *
 * @var integer
 **/
var $SendPerLoop				= 500;

/**
 * Total sent bytes
 *
 * @var integer
 **/
var $TotalSentBytes				= 0;

/**
 * Flag for campaign sending process
 *
 * @var boolean
 **/
var $IsSendingPaused			= false;

/**
 * Number of emails to wait until calculating benchmark
 *
 * @var integer
 **/
var $CalculateBenchmarkPeriod	= 50;

/**
 * Error message array
 *
 * @var array
 **/
var $ArrayErrorMessages			= array(
										'0'			=> array('10001', 'Campaign not found'),
										'1'			=> array('10002', 'Mailer class not defined'),
										'2'			=> array('10003', 'Unable to save email raw file'),
										'3'			=> array('10004', 'PHPMailer is not supported in the new send engine module'),
										);
//---------------------------------------------------------------------------------------

/**
 * Constructor
 *
 * @param array $ArrayConfig Oempro configuration array
 * @return void
 * @author Cem Hurturk
 **/
function SendEngine($ArrayConfig)
	{
	$this->ArrayConfig = $ArrayConfig;
	}

/**
 * Sends status report email to campaign administrator and clients
 *
 * @return void
 * @author Cem Hurturk
 **/
function SendReportEmail()
	{
	// Send to administrator - Start
	if ($this->ArrayCampaign['SendReportToAdmin'] == 'Yes')
		{
		$ObjectMailer = new OctethSwiftEngine($this->ArrayConfig['Paths']['Software'].'/system/classes');

			$ObjectMailer->ArrayConfig	= $this->ArrayConfig;
			$ObjectMailer->SendEngine	= 'NativeMail';
			$ObjectMailer->TimeOut		= 3;

		$ObjectMailer->SetConnection();

		$ObjectMailer->EnableLog		= false;
		$ObjectMailer->CacheToDiskPath	= '';

		$ObjectMailer->EmailHeaderList	= array(
											'X-Mailer'			=> $this->ArrayConfig['Settings']['XMailer'],
											);

		$ObjectMailer->Subject			= $this->ArrayLanguage["Texts"]["00247"];
		$ObjectMailer->FromName			= $this->ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->FromEmail		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->ReplyToName		= $this->ArrayConfig['Settings']['SystemMailerName'];
		$ObjectMailer->ReplyToEmail		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->ReturnPath		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
		$ObjectMailer->CharSet			= $this->ArrayLanguage["Config"]["CharSet"];
		$ObjectMailer->Priority			= 3;
		$ObjectMailer->ToName			= '';
		$ObjectMailer->ToEmail			= $this->ArrayAdministrator['Email'];
		$ObjectMailer->ContentType		= 'MultiPart';
		$ObjectMailer->MainContent		= nl2br($this->ArrayLanguage["Texts"]["00248"]);
		$ObjectMailer->AlternateContent	= $this->ArrayLanguage["Texts"]["00248"];
		$ObjectMailer->EmbedImages		= false;
		$ObjectMailer->AttachmentList	= array();

		$ArrayReplaceList = array(
								"%Name%"					=>		$this->ArrayAdministrator['Name'],
								"%Campaign:CampaignName%"	=>		$this->ArrayCampaign['CampaignName'],
								"%TotalRecipients%"			=>		number_format($this->ArrayCampaignStatistics['TotalRecipients']),
								"%SentRecipients%"			=>		number_format($this->ArrayCampaignStatistics['SentRecipients']),
								"%FailedRecipients%"		=>		number_format($this->ArrayCampaignStatistics['FailedRecipients']),
								);
		$ObjectMailer->MainContent		= str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->MainContent);
		$ObjectMailer->AlternateContent	= str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->AlternateContent);

		$ObjectMailer->SConnector();
		
		$ObjectMailer->BuildMessage();
		
		$ObjectMailer->SendMessage(true, false);
		}
	// Send to administrator - End

	// Send to assigned clients - Start
	if ($this->ArrayCampaign['SendReportToClients'] == 'Yes')
		{
		foreach ($this->ArrayClients as $Key => $ArrayEachClient)
			{
			$ObjectMailer = new OctethSwiftEngine($this->ArrayConfig['Paths']['Software'].'/system/classes');
				$ObjectMailer->ArrayConfig	= $this->ArrayConfig;
				$ObjectMailer->SendEngine	= 'NativeMail';
				$ObjectMailer->TimeOut		= 3;
	
			$ObjectMailer->SetConnection();
	
			$ObjectMailer->EnableLog		= false;
			$ObjectMailer->CacheToDiskPath	= '';
	
			$ObjectMailer->EmailHeaderList	= array(
												'X-Mailer'			=> $this->ArrayConfig['Settings']['XMailer'],
												);
	
			$ObjectMailer->Subject			= $this->ArrayLanguage["Texts"]["00247"];
			$ObjectMailer->FromName			= $this->ArrayConfig['Settings']['SystemMailerName'];
			$ObjectMailer->FromEmail		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->ReplyToName		= $this->ArrayConfig['Settings']['SystemMailerName'];
			$ObjectMailer->ReplyToEmail		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->ReturnPath		= $this->ArrayConfig['Settings']['SystemMailerEmail'];
			$ObjectMailer->CharSet			= $this->ArrayLanguage["Config"]["CharSet"];
			$ObjectMailer->Priority			= 3;
			$ObjectMailer->ToName			= '';
			$ObjectMailer->ToEmail			= $ArrayEachClient['Email'];
			$ObjectMailer->ContentType		= 'MultiPart';
			$ObjectMailer->MainContent		= nl2br($this->ArrayLanguage["Texts"]["00248"]);
			$ObjectMailer->AlternateContent	= $this->ArrayLanguage["Texts"]["00248"];
			$ObjectMailer->EmbedImages		= false;
			$ObjectMailer->AttachmentList	= array();
	
			$ArrayReplaceList = array(
									"%Name%"					=>		$ArrayEachClient['Name'],
									"%Campaign:CampaignName%"	=>		$this->ArrayCampaign['CampaignName'],
									"%TotalRecipients%"			=>		number_format($this->ArrayCampaignStatistics['TotalRecipients']),
									"%SentRecipients%"			=>		number_format($this->ArrayCampaignStatistics['SentRecipients']),
									"%FailedRecipients%"		=>		number_format($this->ArrayCampaignStatistics['FailedRecipients']),
									);
			$ObjectMailer->MainContent		= str_replace(array_keys($ArrayReplaceList), array_values($ArrayReplaceList), $ObjectMailer->MainContent);
	
			$ObjectMailer->SConnector();

			$ObjectMailer->BuildMessage();
	
			$ObjectMailer->SendMessage(true, false);
			}
		}
	// Send to assigned clients - End
	
	return;
	}

/**
 * Clears the outbox database table
 *
 * @return void
 * @author Cem Hurturk
 **/
function ClearOutbox()
	{
	$SQLQuery = "DELETE FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	}

/**
 * Updates administrator quota usage
 *
 * @param integer $AddAmount total usage amount
 * @return void
 * @author Cem Hurturk
 **/
function UpdateQuotaUsage($AddAmount)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_activities WHERE RelAdministratorID='".$this->ArrayAdministrator['AdministratorID']."' AND ActivityHour='".date('h')."' AND ActivityDay='".date('d')."' AND ActivityWeek='".date('W')."' AND ActivityMonth='".date('m')."' AND ActivityYear='".date('Y')."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$ArrayQuota = mysql_fetch_assoc($ResultSet);

	$NewAmount = $ArrayQuota['SendAmount'] + $AddAmount;

	$SQLQuery  = "REPLACE INTO ".$this->ArrayConfig['Database']['Prefix']."administrator_activities ";
	$SQLQuery .= "(`QuotaID`, `RelAdministratorID`, `ActivityHour`, `ActivityDay`, `ActivityWeek`, `ActivityMonth`, `ActivityYear`, `SendAmount`) ";
	$SQLQuery .= "VALUES ('".$ArrayQuota['QuotaID']."', '".$this->ArrayAdministrator['AdministratorID']."', '".date('h')."', ".date('d').", ".date('W').", ".date('m').", ".date('Y').", ".$NewAmount.")";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	return;	
	}

/**
 * Checks the limits of the administrator
 *
 * @return boolean
 * @author Cem Hurturk
 **/
function CheckQuotaAndTimeLimit()
	{
	$IsAllowedQuota		= false;
	$IsAllowedTimeLimit = false;

	// Check available send amount limit - Start
	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_limits AS tblAdminLimits WHERE RelAdministratorID='".$this->ArrayAdministrator['AdministratorID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayAdminLimits = mysql_fetch_assoc($ResultSet);

	if ($ArrayAdminLimits['Limit'] != 'Unlimited')
		{
		include_once('./system/libraries/quota.inc.php');
		$QuotaUsed = CalculateSendLimits($this->ArrayAdministrator['AdministratorID'], $ArrayAdminLimits, $this->ArrayConfig);
		$RemainingQuota = $ArrayAdminLimits['LimitAmount'] - $QuotaUsed;

		if ($RemainingQuota > 1)
			{
			$IsAllowedQuota = true;
			}
		}
	else
		{
		$IsAllowedQuota = true;
		}
	// Check available send amount limit - End

	// Check available send time limit - Start
	$TodayDate	= date('D');
	$TodayTime	= strtotime(date('Y-m-d H:i:s'));
	$AllowedDays = explode(',', $ArrayAdminLimits['TimeLimitDates']);

	if ($AllowedDays[0] != '')
		{
		foreach ($AllowedDays as $EachDay)
			{
			if ($TodayDate == $EachDay)
				{
				if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayAdminLimits['TimeLimitTo']))))
					{
					$IsAllowedTimeLimit = true;
					break;
					}
				}
			}
		}
	else
		{
		$IsAllowedTimeLimit = true;
		}
	// Check available send time limit - End

	if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
		{
		return true;
		}
	else
		{
		return false;
		}
	}

/**
 * Updates member row in campaigns_outbox table
 *
 * @param integer $MemberID Member ID
 * @param string $Type Send status ('Sent', 'Failed')
 * @param string $Reason Status reason
 * @return void
 * @author Cem Hurturk
 **/
function UpdateOutboxRow($MemberID, $Type, $Reason)
	{
	$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox SET Status='".$Type."', StatusReason='".$Reason."', ProcessTime='".date('Y-m-d H:i:s')."' WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelMemberID='".$MemberID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	return;
	}

/**
 * Updates member row in campaigns_outbox table Specialized for outsourced members
 *
 * @param integer $OutboxID Outbox ID
 * @param string $Type Send status ('Sent', 'Failed')
 * @param string $Reason Status reason
 * @return void
 * @author Cem Ozturk
 **/	
function UpdateOutboxRowOutsourced($OutboxID, $Type, $Reason)
	{
	$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox SET Status='".$Type."', StatusReason='".$Reason."', ProcessTime='".date('Y-m-d H:i:s')."' WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND OutboxID='".$OutboxID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	return;
	}	
	
/**
 * Calculates instant and average emails per hour
 *
 * @return void
 * @author Cem Hurturk
 **/
function CalculateBenchmark()
	{
	// Instant emails per second (last 10 sending considered) - Start
	$this->BenchmarkInstEmailPerSec = round($this->CalculateBenchmarkPeriod / (time() - $this->BenchmarkIntervalStartTime));
		$this->BenchmarkInstEmailPerSec = ($this->BenchmarkInstEmailPerSec < 0 ? 0 : $this->BenchmarkInstEmailPerSec);
	// Instant emails per second (last 10 sending considered) - End
	
	// Average emails per second (from the beginning) - Start
	$this->BenchmarkAvgEmailPerSec = round($this->TotalLoopedMembers / (time() - $this->BenchmarkStartTime));
		$this->BenchmarkAvgEmailPerSec = ($this->BenchmarkAvgEmailPerSec < 0 ? 0 : $this->BenchmarkAvgEmailPerSec);
	// Average emails per second (from the beginning) - End

	return;
	}
	
/**
 * Updates sleeping bools for campaigns_statistics table
 *
 * @return void
 * @author Cem Ozturk
 **/
function UpdateSleepingMode($Bool)
	{
	$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Sleeping='".$Bool."' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	return;
	}	

/**
 * Loop each pending recipient and send the newsletter. SHOW TIME! GO GO GO!
 *
 * @return void
 * @author Cem Hurturk
 **/
function LoopPendingRecipients()
	{
	// Set the counter for retrieving members (LIMIT xxx, ...) - Start
	$StartFrom = 0;
	// Set the counter for retrieving members (LIMIT xxx, ...) - End
	
	// Set the correct SQL query to retrieve outbox items based on recipient list types - Start
	if ($this->TotalOutsourcedRecipients > 0)
		{
		// Set the SQL query required for retrieving members from outbox queue - Start
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox ";
		$SQLQuery .= "WHERE tblOutbox.RelMemberID='0' AND tblOutbox.RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND tblOutbox.RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND tblOutbox.Status='Pending'";	
		// Set the SQL query required for retrieving members from outbox queue - End		
		}
	else
		{
		// Set the SQL query required for retrieving members from outbox queue - Start
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox ";
		$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery .= "ON tblOutbox.RelMemberID=tblMembers.MemberID ";
		$SQLQuery .= "WHERE tblOutbox.RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND tblOutbox.RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND tblOutbox.Status='Pending'";		
		// Set the SQL query required for retrieving members from outbox queue - End	
		}	
	// Set the correct SQL query to retrieve outbox items based on recipient list types - End
	
	// Set benchmark information - Start
	$this->RecordBenchmark('Start');
	$this->BenchmarkInstEmailPerSec		= 0;
	$this->BenchmarkAvgEmailPerSec		= 0;
	// Set benchmark information - End

	// Set interval benchmark information - Start
	$this->RecordBenchmark('IntervalStart');
	// Set interval benchmark information - End
	
	// Reset the loop member counters - Start
	$this->TotalLoopedMembers		= 0;
	$this->TotalSentMembers			= 0;
	$this->TotalFailedMembers		= 0;
	$this->TotalSentBytes			= 0;
	// Reset the loop member counters - End

	// Calculate the loop amount again - Start
	$this->CalculateLoopAmount();
	// Calculate the loop amount again - End
	
	// Initiate the Swift object - Start
	$this->ObjectMailer->SConnector();
	// Initiate the Swift object - End
	
	// Perform loop of every interval - Start
	for ($i=0; $i < $this->LoopAmount; $i++) 
		{ 
		// Set the SQL query for retrieving pending recipients in this loop - Start
		$SQLQueryLoop = $SQLQuery ." LIMIT 0, ".$this->SendPerLoop;
		$ResultSetOutbox = ExecuteMySQLQuery($SQLQueryLoop, __LINE__, __FILE__);
		// Set the SQL query for retrieving pending recipients in this loop - End
		
		// Avoid browser time-out - Start
		print(" ");
		// Avoid browser time-out - End
		
		// Loop each pending recipients in this loop - Start
		while ($EachOutbox = mysql_fetch_assoc($ResultSetOutbox))
			{ 
			// Add outsourced email address - Start
			$EachOutbox['Email']	=	$EachOutbox['EmailAddress'];
			// Add outsourced email address - End
		
			// Load balancing (set in settings > preferences > load balancing tab) - Start
			if ($this->ArrayConfig['Settings']['LoadBalanceEnabled'] == 'Enabled')
				{
				if ($this->TotalLoopedMembers % $this->ArrayConfig['Settings']['LoadBalanceIntervalAmount'] == 0 and $this->TotalLoopedMembers!=0)
					{	
					$this->UpdateSleepingMode(1);						
					sleep($this->ArrayConfig['Settings']['LoadBalanceDelaySecondsPerInterval']);
					$this->UpdateSleepingMode(0);
					
					// Reset the MTA connection to avoid any send errors because of MTA connection time-out during the 'sleep' state - Start
					$this->ObjectMailer->SetConnection($this->ArrayCampaignStatistics['SentRecipients'], true);
					$this->ObjectMailer->SConnector();
					// Reset the MTA connection to avoid any send errors because of MTA connection time-out during the 'sleep' state - End
					}
				}
			// Load balancing (set in settings > preferences > load balancing tab) - End

			// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
			if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
				{
				$this->IsSendingPaused = true;
				break;
				}
			// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED

			// Calculate instant emails per second - Start	
			if ($this->TotalLoopedMembers % $this->CalculateBenchmarkPeriod == 0)
				{
				$this->CalculateBenchmark();
			
				$this->RecordBenchmark('IntervalStart');
				}
			// Calculate instant emails per second - End
		
			// Update the loop member counter - Start
			$this->TotalLoopedMembers++;
			// Update the loop member counter - End
			
			// Avoid browser time-out - Start
			print(" ");
			// Avoid browser time-out - End
		
			// Define the email subject for the current recipient - Start
			$TMPSubject		= $this->DefineEmailSubject($EachOutbox);
			// Define the email subject for the current recipient - End

			// Define the email contents for the current recipient - Start
			$TMPContents		= $this->DefineEmailContents($EachOutbox);
				$TMPContentMain			= $TMPContents[0];
				$TMPContentAlternate	= $TMPContents[1];
			// Define the email contents for the current recipient - End

			// Perform personalization for member, sender and campaign - Start
			if ($this->ArrayContent['Personalize'] == 'Yes')
				{
				$TMPContents = $this->PersonalizeEmailContents($TMPSubject, $TMPContentMain, $TMPContentAlternate, $EachOutbox);
	
				$TMPSubject				= $TMPContents[0];
				$TMPContentMain			= $TMPContents[1];
				$TMPContentAlternate	= $TMPContents[2];
				}
			// Perform personalization for member, sender and campaign - End

			// If debug mode is enabled, display the output. Else send the email to the recipient email address - Start
			if ($this->ArrayConfig['Settings']['SystemDebugMode'] == 'Enabled')
				{
				print ("Local Mail List - ".$EachOutbox['RelMemberID']." - ".$EachOutbox['Email']."<br>\n");

				$this->TotalSentMembers++;

				// Mark the queue row of this member as 'failed' - Start
				$this->UpdateOutboxRow($EachOutbox['MemberID'], 'Sent', '');
				// Mark the queue row of this member as 'failed' - En	
				}
			elseif ($this->ArrayConfig['Settings']['SystemDebugMode'] != 'Enabled')
				{
				// Set email headers and body - Start
				$this->SetEmailProperties($TMPSubject, $EachOutbox);
				$this->SetEmailBody($TMPContentMain, $TMPContentAlternate);
				// Set email headers and body - End

				// Decide to send or save to the disk - Start
				if (($this->ArraySMTP['SendEngines']['Type'] == 'SaveAsFile') || ($this->ArraySMTP['Type'] == 'SaveAsFile'))
					{
					$TMPSendEmail = false;
					$TMPSaveEmail = true;
					}
				else
					{
					$TMPSendEmail = true;
					$TMPSaveEmail = false;
					}
				// Decide to send or save to the disk - Start

				// Build and send the email - Start
				$this->ObjectMailer->BuildMessage();
				if ($this->ObjectMailer->ErrorStatus == true)
					{
					echo $this->ObjectMailer->ErrorInfo[0];
					exit;
					}
	
				$CheckStatus=true;
			
				if ($this->check_email_address($EachOutbox["EmailAddress"]))
					{
					// Check if connection to the MTA is still alive or not. If it's not, reconnect - Start
					if ($this->ObjectMailer->ObjectConnection->isAlive() == false)
						{
						$this->ObjectMailer->SetConnection($this->ArrayCampaignStatistics['SentRecipients'], true);
						$this->ObjectMailer->SConnector();
						}
					// Check if connection to the MTA is still alive or not. If it's not, reconnect - End

					$TMPEmailContent = $this->ObjectMailer->SendMessage($TMPSendEmail, $TMPSaveEmail);
					}
				else
					{
					$CheckStatus=false;
					}
				// Build and send the email - End
				
				// Save the returned raw email to the defined directory - Start
				if (($this->ArraySMTP['SendEngines']['Type'] == 'SaveAsFile') || ($this->ArraySMTP['Type'] == 'SaveAsFile'))
					{
						$TMPFileName = strtotime(date('Y-m-d H:i:s')).rand(1000000, 9999999);
					if (($TMPFileHandler = fopen($this->ArrayConfig['Settings']['SaveRawEmailPath'].$TMPFileName, 'w')) == true)
						{
						fwrite($TMPFileHandler, $TMPEmailContent);
						fclose($TMPFileHandler);
						}
					else
						{
						echo $this->ArrayErrorMessages[2][0]." - ".$this->ArrayErrorMessages[2][1];
						exit;					
						}
					}
				// Save the returned raw email to the defined directory - End

				// Error exists, save error to the campaign log file - Start
				if (($this->ObjectMailer->ErrorStatus == true) || ($CheckStatus==false))
					{
					// Send failed
						$DataLine = $EachOutbox['MemberID']." - ".$EachOutbox['Email']." - ".$this->ObjectMailer->ErrorInfo[0]."\n";
					fwrite($this->ErrorFileHandler, $DataLine);
				
					$this->TotalFailedMembers++;

					// Mark the queue row of this member as 'failed' - Start
					if($this->TotalOutsourcedRecipients>0)
						{
						$this->UpdateOutboxRowOutsourced($EachOutbox['OutboxID'], 'Failed', $this->ObjectMailer->ErrorInfo);
						}
						else
						{
						$this->UpdateOutboxRow($EachOutbox['MemberID'], 'Failed', $this->ObjectMailer->ErrorInfo);
						}
					// Mark the queue row of this member as 'failed' - End
					}
				else
					{
					// Send succeeded
					$this->TotalSentMembers++;
					$this->TotalSentBytes += strlen($TMPEmailContent);
					// Mark the queue row of this member as 'sent' - Start
					if($this->TotalOutsourcedRecipients>0)
						{
						$this->UpdateOutboxRowOutsourced($EachOutbox['OutboxID'], 'Sent');
						}
						else
						{
						$this->UpdateOutboxRow($EachOutbox['MemberID'], 'Sent');
						}
					// Mark the queue row of this member as 'sent' - End
					}
				// Error exists, save error to the campaign log file - End

				// Run external plug-in processes - Start
					$SendCompleted = ($this->ObjectMailer->ErrorStatus == true ? false : true);
				$ArrayPlugInProcessResults = RunPlugInProcesses($SendCompleted, $this->ArrayConfig, $this->ArrayLanguage, $EachOutbox, $this->ArrayCampaign, $this->ArrayCampaignStatistics);
				// Run external plug-in processes - End

				// Check external plug-in process return message. If it is a command, process it - Start
				foreach ($ArrayPlugInProcessResults as $PlugInKey=>$ArrayPlugInResults)
					{
					if (strtolower($ArrayPlugInResults['Command']) == 'stop')
						{
						$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Cancelled' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
						$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
						}
					}
				// Check external plug-in process return message. If it is a command, process it - End

				// Check quota and stop sending process is there is no available quota or sending time limit - STARTED
				if ($this->CheckQuotaAndTimeLimit() == false)
					{
					$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics SET Status='Paused' WHERE CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
					$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
					}
				else
					{
					// Update quota - STARTED
					$this->UpdateQuotaUsage(1);
					// Update quota - FINISHED
					}
				// Check quota and stop sending process is there is no available quota or sending time limit - FINISHED
				}
			// If debug mode is enabled, display the output. Else send the email to the recipient email address - End

			// Update campaign statistics - Start
			if ($this->ObjectMailer->ErrorStatus == false)
				{
				$this->UpdateCampaignStatistics('', '', '', $this->ArrayCampaignStatistics['SentRecipients'] + 1, '', '', '', '', $this->BenchmarkInstEmailPerSec, $this->BenchmarkAvgEmailPerSec, $this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'] + $this->TotalSentBytes, '', '', '');
				}
			else
				{
				$this->UpdateCampaignStatistics('', '', '', '', $this->ArrayCampaignStatistics['FailedRecipients'] + 1, '', '', '', $this->BenchmarkInstEmailPerSec, $this->BenchmarkAvgEmailPerSec, $this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'] + $this->TotalSentBytes, '', '', '');
				}
			// Update campaign statistics - End

			// Check process command. If sending is stopped, paused or canceled, do not continue - STARTED
			if ($this->ArrayCampaignStatistics['Status'] != 'Sending')
				{
				$this->IsSendingPaused = true;
				break;
				}
			// Check process command. If sending is stopped, paused or canceled, do not continue - FINISHED

			// Make LoadBalance for SMTP - Start
			if($this->ObjectMailer->SendEngine=="SMTP")
				{
				$this->ObjectMailer->SetConnection($this->ArrayCampaignStatistics['SentRecipients'], true);
				}
			// Make LoadBalance for SMTP - End			
			}
		// Loop each pending recipients in this loop - End
		}
	// Perform loop of every interval - End

	// Set interval benchmark information - Start
	$this->RecordBenchmark('IntervalFinish');
	// Set interval benchmark information - End

	// Set benchmark information - Start
	$this->RecordBenchmark('Finish');
	// Set benchmark information - End

	return;
	}


function check_email_address($email) 
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) 
		{
		// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
		return false;
		}
	
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
	
		for ($i = 0; $i < sizeof($local_array); $i++) 
		{
			if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) 
			{
				return false;
			}
		}
	
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) 
		{ 
			// Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
	
			if (sizeof($domain_array) < 2) 
			{
				return false; // Not enough parts to domain
			}
	
			for ($i = 0; $i < sizeof($domain_array); $i++) 
			{
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
				{
					return false;
				}
			}
		}
		
		return true;
	}


/**
 * Builds the message for the recipient
 *
 * @return void
 * @author Cem Hurturk
 **/
function BuildMessage()
	{
	$this->ObjectMailer->BuildMessage();

	if ($this->ObjectMailer->ErrorStatus == true)
		{
		echo $this->ObjectMailer->ErrorInfo[0];
		exit;		
		}
	}

/**
 * Sets the email body (contents, attachments, image embedding, etc.)
 *
 * @param string $MainContent Main content of the email body
 * @param string $AlternateContent Alternate content of the email body
 * @return void
 * @author Cem Hurturk
 **/
function SetEmailBody($MainContent, $AlternateContent)
	{
	// Set email body - Start
	if ($this->ArrayContent["ContentType"] == 'Plain')
		{
		$this->ObjectMailer->ContentType		= 'Plain';
		$this->ObjectMailer->MainContent		= $MainContent;
		$this->ObjectMailer->AlternateContent	= '';
		}
	elseif ($this->ArrayContent["ContentType"] == 'Html')
		{
		$this->ObjectMailer->ContentType		= 'HTML';
		$this->ObjectMailer->MainContent		= $MainContent;
		$this->ObjectMailer->AlternateContent	= '';
		}
	elseif ($this->ArrayContent["ContentType"] == 'Both')
		{
		$this->ObjectMailer->ContentType		= 'MultiPart';
		$this->ObjectMailer->MainContent		= $MainContent;
		$this->ObjectMailer->AlternateContent	= $AlternateContent;
		}
	// Set email body - End

	// If enabled, embed images to the email - Start
	if ((($this->ArrayContent["ContentType"] == 'Html') || ($this->ArrayContent["ContentType"] == 'Both')) && ($this->ArrayContent['EmbedObjects'] == 'Yes'))
		{
		$this->ObjectMailer->ArrayEmbedExcludedObjects	= array(
																$this->ArrayConfig['URLs']['Software'].'/read.php?'
																);
		$this->ObjectMailer->EmbedCacheDir				= $this->ArrayConfig['Paths']['Data']."/campaign_processing/".$this->ArrayCampaignStatistics['CampaignStatisticsID'];
		$this->ObjectMailer->EmbedImages				= true;
		}
	// If enabled, embed images to the email - End

	// Insert attachments - Start
	$this->ObjectMailer->AttachmentList = array();
	
	foreach ($this->ArrayAttachments as $Key => $ArrayValues)
		{
		$AttachmentPath		=	$this->ArrayConfig['Paths']['Data']."/campaign_attachments/".$this->ArrayCampaign['CampaignID']."/".$ArrayValues['AttachmentID'].".att";
		$AttachmentName		=	$ArrayValues['FileName'];

		$this->ObjectMailer->AttachmentList[$AttachmentName] = $AttachmentPath;
		}
	// Insert attachments - End

	return;
	}

/**
 * Sets email properties such as from, reply-to, etc.
 *
 * @param string $Subject Subject of the email
 * @param array $ArrayMember Member information of the recipient
 * @return void
 * @author Cem Hurturk
 **/
function SetEmailProperties($Subject, $ArrayMember)
	{
	$this->ObjectMailer->EmailHeaderList	= array(
													'X-Mailer'			=> $this->ArrayConfig['Settings']['XMailer'],
													'X-Mailer-MsgId'	=> base64_encode(strtolower($ArrayMember['Email'])),
													'X-Mailer-CSID'		=> base64_encode($this->ArrayCampaign['CampaignID'].'_'.$this->ArrayCampaignStatistics['CampaignStatisticsID']),
													);
	$this->ObjectMailer->Subject			= $Subject;
	$this->ObjectMailer->FromName			= $this->ArrayFromEmail['Name'];
	$this->ObjectMailer->FromEmail			= $this->ArrayFromEmail['EmailAddress'];
	$this->ObjectMailer->ReplyToName		= $this->ArrayReplyToEmail['Name'];
	$this->ObjectMailer->ReplyToEmail		= $this->ArrayReplyToEmail['EmailAddress'];
	$this->ObjectMailer->ReturnPath			= $this->ArrayReturnPathEmail['EmailAddress'];
	$this->ObjectMailer->CharSet			= substr($this->ArrayContent["CharSet"], 0, strlen($this->ArrayContent["CharSet"]) - 5);
	$this->ObjectMailer->Priority			= $this->ArrayContent['Priority'];
	$this->ObjectMailer->ToName				= '';
	$this->ObjectMailer->ToEmail			= $ArrayMember['Email'];

	return;
	}

/**
 * Personalizes the email contents based on member, sender and campaign information
 *
 * @param string $Subject Subject of the email
 * @param string $MainContent Main content of the email body
 * @param string $AlternateContent Alternate content of the email body
 * @param array $ArrayMember Member information of the recipient
 * @return array
 * @author Cem Hurturk
 **/


function PersonalizeEmailContents($Subject, $MainContent, $AlternateContent, $ArrayMember)
	{
	$ArrayPrefixes		=	array("Sender",	"Member", "Membership",	"Campaign");
	$ArrayInformation	=	array($this->ArrayAdministrator, $ArrayMember, $ArrayMember, $this->ArrayCampaign);											

	$Subject			=	Personalize($Subject, $ArrayPrefixes, $ArrayInformation, '%');
	$MainContent		=	Personalize($MainContent, $ArrayPrefixes, $ArrayInformation, '%');
	$AlternateContent	=	Personalize($AlternateContent, $ArrayPrefixes, $ArrayInformation, '%');

	// FIXME: Outsourced list personalization doesn't seem to be working!!!
	// $Subject			=	PersonalizeOutsourcedMember($Subject, $ArrayPrefixes, $ArrayInformation, $this->ArrayConfig);
	// $MainContent		=	PersonalizeOutsourcedMember($MainContent, $ArrayPrefixes, $ArrayInformation, $this->ArrayConfig);
	// $AlternateContent	=	PersonalizeOutsourcedMember($AlternateContent, $ArrayPrefixes, $ArrayInformation, $this->ArrayConfig);

	$Subject			=	PersonalizeCampaignTags($Subject, $this->ArrayContent['ContentType'], 'Subject', $this->ArrayConfig, $this->ArrayLanguage, $ArrayMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, true, '%');
	$MainContent		=	PersonalizeCampaignTags($MainContent, $this->ArrayContent['ContentType'], 'Main', $this->ArrayConfig, $this->ArrayLanguage, $ArrayMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');
	$AlternateContent	=	PersonalizeCampaignTags($AlternateContent, $this->ArrayContent['ContentType'], 'Alternate', $this->ArrayConfig, $this->ArrayLanguage, $ArrayMember, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayContent, false, '%');

	// Remove all unwanted HTML codes from email body - Start
	if (($this->ArrayContent['ContentType'] == 'Html') || ($this->ArrayContent['ContentType'] == 'Both'))
		{
		$MainContent = RemoveUnwantedHTMLCodes($MainContent, $this->ArrayConfig);
		}
	// Remove all unwanted HTML codes from email body - End

	return array($Subject, $MainContent, $AlternateContent);
	}

/**
 * Defines email contents (main and alternate)
 *
 * @param array $ArrayMember Member information of the member
 * @return array
 * @author Cem Hurturk
 **/
function DefineEmailContents($ArrayMember)
	{
	$TMPContentMain			=	$this->ArrayContent['ContentMain'];
	$TMPContentAlternate	=	$this->ArrayContent['ContentAlternate'];

	// Run external plug-ins which are content specific - Start
	$ArrayPlugInReturn = RunPlugInContentProcesses($TMPContentMain, $TMPContentAlternate, $this->ArrayAdministrator, $this->ArrayCampaign, $this->ArrayCampaignStatistics, $this->ArrayLanguage, $ArrayMember, $this->ArrayConfig);
		$TMPContentMain			=	$ArrayPlugInReturn[0];
		$TMPContentAlternate	=	$ArrayPlugInReturn[1];
	// Run external plug-ins which are content specific - End

	return array($TMPContentMain, $TMPContentAlternate);
	}
	
/**
 * Defines the email subject for the recipient
 *
 * @param array $ArrayMember Member information of the member
 * @return string
 * @author Cem Hurturk
 **/
function DefineEmailSubject($ArrayMember)
	{
	$TMPSubject = $this->ArrayContent['Subject'];

	// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - Start
	if (strtolower(gettype($TMPSubject)) == 'array')
		{
		$TMPSubject			=	$TMPSubject[rand(0, count($TMPSubject) - 1)];
		}
	// If subject is an array, then it means that multi-subject is enabled. Select one of them randomly - End

	return $TMPSubject;
	}

/**
 * Records the benchmark value ($type can be 'Start','Finish','IntervalStart','IntervalFinish')
 *
 * @param string $Type Benchmark type ('Start', 'Finish', 'IntervalStart', 'IntervalFinish')
 * @return void
 * @author Cem Hurturk
 **/
function RecordBenchmark($Type)
	{
	if ($Type == 'Start')
		{
		$this->BenchmarkStartTime			= time();
		}
	elseif ($Type == 'Finish')
		{
		$this->BenchmarkFinishTime			= time();
		}
	elseif ($Type == 'IntervalStart')
		{
		$this->BenchmarkIntervalStartTime	= time();
		}
	elseif ($Type == 'IntervalFinish')
		{
		$this->BenchmarkIntervalFinishTime	= time();
		}
	}

/**
 * Calculates the loop amount required to send all pending emails
 *
 * @return void
 * @author Cem Hurturk
 **/
function CalculateLoopAmount()
	{
	$this->LoopAmount = ceil($this->TotalPendingRecipients / $this->SendPerLoop);
	}

/**
 * Calculates pending recipents in the outbox queue table
 *
 * @return void
 * @author Cem Hurturk
 **/
function CalculatePendingRecipients()
	{
	$SQLQuery = "SELECT COUNT(*) AS TotalPendingRecipients FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND Status='Pending'";
	$ResultSet = mysql_query($SQLQuery);
	$this->TotalPendingRecipients = mysql_fetch_assoc($ResultSet);
	$this->TotalPendingRecipients = $this->TotalPendingRecipients['TotalPendingRecipients'];
	}

/**
 * While sending to recipients, this function checks if SMTP server looping is needed (if multi SMTP servers are set)
 *
 * @return void
 * @author Cem Hurturk
 **/
function SMTPServerLoadBalance()
	{
	// Set the new SMTP Server settings in mail engine - Start
	$this->ObjectMailer->SendEngine			= 'SMTP';

	$this->ObjectMailer->SMTPHost			= array();
	$this->ObjectMailer->SMTPAuth			= array();
	$this->ObjectMailer->SMTPUsername		= array();
	$this->ObjectMailer->SMTPPassword		= array();
	$this->ObjectMailer->SMTPPort			= array();
	$this->ObjectMailer->SMTPSSL			= array();
	$this->ObjectMailer->TimeOut			= array();

	foreach ($this->ArraySMTP as $EachIndex=>$ArrayEachValues)
		{
		$this->ObjectMailer->SMTPHost[]					= $ArrayEachValues['Host'];
		$this->ObjectMailer->SMTPAuth[]					= ($ArrayEachValues['Auth'] == 'Authorization' ? true : false);
		$this->ObjectMailer->SMTPUsername[]				= $ArrayEachValues['Username'];
		$this->ObjectMailer->SMTPPassword[]				= $ArrayEachValues['Password'];
		$this->ObjectMailer->SMTPPort[]					= $ArrayEachValues['Port'];
		$this->ObjectMailer->SMTPSSL[]					= false;
		$this->ObjectMailer->TimeOut[]					= $ArrayEachValues['TimeOutSec'];		
		$this->ObjectMailer->MaxEmailsPerInterval[]		= $ArrayEachValues['MaxEmailsPerInterval'];
		$this->ObjectMailer->SecondsDelayPerInterval[]	= $ArrayEachValues['SecondsDelayPerInterval'];
		}
	// Set the new SMTP Server settings in mail engine - End

	return;
	}

/**
 * Disconnects the mail engine
 *
 * @return void
 * @author Cem Hurturk
 **/
function Disconnect()
	{
	$this->ObjectMailer->Disconnect();
	return;
	}

/**
 * Sets the mailer class
 *
 * @param string $MailerType Mail engine type ('SwiftMailer', 'PHPMailer')
 * @return void
 * @author Cem Hurturk
 **/
function InitMailer($MailerType)
	{
	if ($MailerType == '')
		{
		echo $this->ArrayErrorMessages[1][0]." - ".$this->ArrayErrorMessages[1][1];
		exit;
		}
	elseif ($MailerType == 'SwiftMailer')
		{
		$this->MailerClass = 'SwiftMailer';

		// Include required SwiftMailer and other classes - Start
		include_once($this->ArrayConfig['Paths']['Software'].'/system/classes/Swift.php');
		include_once($this->ArrayConfig['Paths']['Software'].'/system/classes/class_swiftengine.php');
		// Include required SwiftMailer and other classes - End

		// Construct the send engine object - Start
		$this->ObjectMailer = new OctethSwiftEngine($this->ArrayConfig['Paths']['Software'].'/system/classes');
		
		$this->ObjectMailer->ArrayConfig				= $this->ArrayConfig;
		$this->ObjectMailer->ArrayCampaignStatistics	= $this->ArrayCampaignStatistics;

		if ($this->ObjectMailer->ErrorStatus == true)
			{
			echo $this->ObjectMailer->ErrorInfo[0];
			exit;			
			}
		// Construct the send engine object - End

		// Set the sending method - Start
		if ($this->ArraySMTP['Type'] == 'Sendmail')
			{
			$this->ObjectMailer->SendMailPath		= $this->ArrayConfig['Settings']['SendmailPath'];	
			$this->ObjectMailer->SendEngine			= 'SendMail';
			$this->ObjectMailer->TimeOut			= 3;
			}
		elseif ($this->ArraySMTP['Type'] == 'QMail')
			{
			$this->ObjectMailer->SendMailPath		= $this->ArrayConfig['Settings']['QMailPath'];
			$this->ObjectMailer->SendEngine			= 'SendMail';
			$this->ObjectMailer->TimeOut			= 3;
			}
		elseif ($this->ArraySMTP['Type'] == 'MTA')
			{
			$this->ObjectMailer->SendEngine			= 'NativeMail';
			$this->ObjectMailer->TimeOut			= 3;
			}
		elseif ($this->ArraySMTP['Type'] == 'SaveAsFile')
			{
			$this->ObjectMailer->SendEngine			= 'NativeMail';
			$this->ObjectMailer->TimeOut			= 3;
			}
		elseif (($this->ArraySMTP['Type'] == 'SMTP') || ($this->ArraySMTP[0]['Type'] == 'SMTP'))
			{
			$this->SMTPServerLoadBalance();
			}
		// Set the sending method - End

		// Setup the connection to MTA - Start
		$this->ObjectMailer->SetConnection();
		// Setup the connection to MTA - End

		// Set SwiftMailer properties - Start
		$this->ObjectMailer->EnableLog				= false;
		$this->ObjectMailer->CacheToDiskPath		= '';
		// Set SwiftMailer properties - End
		}
	elseif ($MailerType == 'PHPMailer')
		{
		$this->MailerClass = 'PHPMailer';
		echo $this->ArrayErrorMessages[3][0]." - ".$this->ArrayErrorMessages[3][1];
		exit;
		}
	}

/**
 * Updates instant amount of recursively scheduled campaign
 *
 * @return void
 * @author Cem Hurturk
 **/
function UpdateCampaignInstanceAmount()
	{
	$SQLQuery = "UPDATE ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule SET ScheduleSendInstance = ScheduleSendInstance + 1 WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	}

/**
 * Copies outsourced recipient information to campaign queue outbox table
 *
 * @param array $OutsourcedRecipientsList Out-MySQL query's result set's recipients list
 * @return void
 * @author Cem Ozturk
 **/
 

function MoveOutSourcedRecipientsToQueue($OutsourcedRecipientsList)
	{

	// Check if campaing is paused - Start	
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND CampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."'";
		$ResultSet3 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
		$Situation = mysql_fetch_assoc($ResultSet3);
		
		if($Situation['SentRecipients']==0 and $Situation['FailedRecipients']==0 )
			{
				$SituationProgress=1;
			}
			
	
	// Check if campaign is paused - End				
	
		if ($SituationProgress == 1)
			{

			foreach ($OutsourcedRecipientsList as $OutSourcedMember)
				{
		
				$ArrayFieldnValues = array(
										'OutboxID'					=> '',
										'RelCampaignID'				=> $this->ArrayCampaign['CampaignID'],
										'RelCampaignStatisticsID'	=> $this->ArrayCampaignStatistics['CampaignStatisticsID'],
										'RelMemberID'				=> '0',
										'EmailAddress'				=> $OutSourcedMember,
										'Source'					=> 'Outsourced',
										'Status'					=> 'Pending',
										'StatusReason'				=> '',
										'ProcessTime'				=> '',
										);
				
			
				$FieldList = '`'.implode('`, `', array_keys($ArrayFieldnValues)).'`';
				$ValueList = "'".implode("', '", array_values($ArrayFieldnValues))."'";
		
				$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox (".$FieldList.") VALUES (".$ValueList.")";
						
				$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);						
				}
			}
		
	return;
	}	

/**
 * Removes provided member ID from outbox table
 *
 * @param integer $MemberID Member ID to be removed from outbox table
 * @return void
 * @author Cem Hurturk
 **/
function RemoveFromOutbox($MemberID, $OutboxID = 0)
	{
	if (($OutboxID > 0) || (count($OutboxID) > 0))
		{
		if (is_array($OutboxID) == true)
			{
			$SQLQuery = "DELETE FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND OutboxID IN (".implode(',', $OutboxID).")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		else
			{
			$SQLQuery = "DELETE FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE OutboxID='".$OutboxID."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		}
	else
		{
		if (is_array($MemberID) == true)
			{
			$SQLQuery = "DELETE FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND RelMemberID IN (".implode(',', $MemberID).")";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		else
			{
			$SQLQuery = "DELETE FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelMemberID='".$MemberID."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			}
		}
	}

/**
 * Generates the SQL query for retrieving members from oemp_members table
 *
 * @return string SQL query for retrieving members is returned
 * @author Cem Hurturk
 **/
function GenerateRecipientSQLQuery()
	{
	// Mail list SQL query filtering - Start
	$TMPSQLQuery = array();

	foreach ($this->ArrayCampaignMailLists['Local'] as $EachIndex=>$EachValue)
		{	
		$TMPSQLQuery[] = "tblAssign.RelMailListID = '".$EachValue."'";
		}
	$TMPSQLQuery = implode(" OR ", $TMPSQLQuery);
	$TMPSQLQuery = "(".$TMPSQLQuery.")";
	// Mail list SQL query filtering - End

	// Only active members SQL query filtering - Start
	$SQLQueryPart = array();
		$SQLQueryPart[] = $TMPSQLQuery;
		$SQLQueryPart[] = "tblMembers.BounceType!='Hard bounce'";
		$SQLQueryPart[] = "(tblAssign.SubscriptionStatus='Subscribed' OR tblAssign.SubscriptionStatus='Pending for opt-out confirmation')";
	// Only active members SQL query filtering - Start

	// Send rule SQL query filtering - Start
	$TMPReturn		= $this->CreateSendRuleSQLQueryPart();
	if ($TMPReturn != '')
		{
		$SQLQueryPart[]	= $TMPReturn;
		}
	// Send rule SQL query filtering - End

	// Main SQL query - Start
	$SQLQuery 			 = "SELECT '', ".$this->ArrayCampaign['CampaignID'].", ".$this->ArrayCampaignStatistics['CampaignStatisticsID'].", tblMembers.MemberID, tblMembers.Email, 'Local', 'Pending', '', '0000-00-00 00:00:00' ";
		$SQLQuery		.= "FROM ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQuery		.= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQuery		.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Main SQL query - End
	
	// Main SQL Total found query - Start
	$SQLQueryTotalFound = "SELECT COUNT(*) AS TotalFound ";
		$SQLQueryTotalFound		.= "FROM ".$this->ArrayConfig['Database']['Prefix']."members AS tblMembers ";
		$SQLQueryTotalFound		.= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_members AS tblAssign ";
		$SQLQueryTotalFound		.= "ON tblMembers.MemberID = tblAssign.RelMemberID ";
	// Main SQL Total Found query - End	

	// If this is a resumed campaign, retrieve new subscribed members only - Start
	if ($this->IsCampaignResumed == true)
		{
		$SQLQuery		.= "LEFT OUTER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox ";
		$SQLQuery		.= "ON tblOutbox.RelMemberID = tblMembers.MemberID ";

		$SQLQueryTotalFound .= "LEFT OUTER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox ";
		$SQLQueryTotalFound .= "ON tblOutbox.RelMemberID = tblMembers.MemberID ";

		$SQLQueryPart[] = "tblOutbox.RelMemberID IS NULL";
		}
	// If this is a resumed campaign, retrieve new subscribed members only - End

	// Generate the SQLQuery - Start
	$SQLQuery = $SQLQuery.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
	$SQLQueryTotalFound  = $SQLQueryTotalFound.(count($SQLQueryPart) > 0 ? " WHERE ".implode(" AND ", $SQLQueryPart) : '')." ";
	// Generate the SQLQuery - End

	$SQLQuery .= "ORDER BY tblMembers.MemberID ";
	$SQLQueryTotalFound .="ORDER BY tblMembers.MemberID ";;

	// Add recipients to outbox table - Start
	$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox (`OutboxID`, `RelCampaignID`, `RelCampaignStatisticsID`, `RelMemberID`, `EmailAddress`, `Source`, `Status`, `StatusReason`, `ProcessTime`) ".$SQLQuery;
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Add recipients to outbox table - End

	// Optimize the outbox table to speed-up the process - Start
	$SQLQuery = "OPTIMIZE TABLE ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Optimize the outbox table to speed-up the process - End

	// Remove any duplicate emails in the outbox table - Start
	$this->RemoveDuplicatesFromOutbox();
	// Remove any duplicate emails in the outbox table - End

	// Remove banned or black listed emails in the outbox table - Start
	$this->RemoveBanndAndBlackListedFromOutbox();
	// Remove banned or black listed emails in the outbox table - End

	// Optimize the outbox table to speed-up the process - Start
	$SQLQuery = "OPTIMIZE TABLE ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// Optimize the outbox table to speed-up the process - End
	
	return;
	}

/**
 * Removes banned and black listed subscribers from the outbox table for the campaign/campaign statistics ID
 *
 * @return void
 * @author Cem Hurturk
 **/
function RemoveBanndAndBlackListedFromOutbox()
	{
	// Check against black list - Start
	while (true)
		{
		$SQLQuery = "SELECT tblOutbox.OutboxID AS BlackListedID FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_blacklist AS tblBlackList ON tblOutbox.EmailAddress = tblBlackList.Email WHERE tblOutbox.RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND tblOutbox.RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND tblBlackList.RelMailListID IN (0".(count($this->ArrayCampaignMailLists['Local']) > 0 ? ','.implode(',', $this->ArrayCampaignMailLists['Local']) : '').")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayRemoveList = array();

			while ($EachBlackListedOutboxItem = mysql_fetch_assoc($ResultSet))
				{
				$ArrayRemoveList[] = $EachBlackListedOutboxItem['BlackListedID'];
				}

			$this->RemoveFromOutbox('', $ArrayRemoveList);
			}
		else
			{
			break;
			}
		}
	// Check against black list - End

	// Check against banned list - Start
	while (true)
		{
		$SQLQuery = "SELECT tblOutbox.OutboxID AS BannedID FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox AS tblOutbox INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillist_banned AS tblBanList ON tblOutbox.EmailAddress = tblBanList.Banned WHERE tblOutbox.RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND tblOutbox.RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND tblBanList.RelMailListID IN (0".(count($this->ArrayCampaignMailLists['Local']) > 0 ? ','.implode(',', $this->ArrayCampaignMailLists['Local']) : '').")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayRemoveList = array();

			while ($EachBannedOutboxItem = mysql_fetch_assoc($ResultSet))
				{
				$ArrayRemoveList[] = $EachBannedOutboxItem['BannedID'];
				}

			$this->RemoveFromOutbox('', $ArrayRemoveList);
			}
		else
			{
			return true;
			}
		}
	// Check against banned list - End
	
	return true;
	}

/**
 * Removes duplicate email addresses from the outbox table for the campaign/campaign statistics ID.
 *
 * @return void
 * @author Cem Hurturk
 **/
function RemoveDuplicatesFromOutbox()
	{
	while (true)
		{
		$SQLQuery = "SELECT MAX(OutboxID) AS DuplicateID, COUNT(OutboxID) AS DuplicateCount, EmailAddress FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' GROUP BY EmailAddress HAVING DuplicateCount > 1";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			$ArrayRemoveList = array();

			while ($EachDuplicateOutboxItem = mysql_fetch_assoc($ResultSet))
				{
				$ArrayRemoveList[] = $EachDuplicateOutboxItem['DuplicateID'];
				}

			$this->RemoveFromOutbox('', $ArrayRemoveList);
			}
		else
			{
			return true;
			}
		}

	return true;
	}


/**
 * Generates the Out sourced Recipients List array for retrieving members from other Mysql tables
 *
 * @return array for retrieving members is returned
 * @author Cem Ozturk
 **/
function GenerateOutsourceRecipientsList()
	{
	$OutsourcedRecipientsList = array();	

	foreach ($this->ArrayCampaignMailLists['Outsourced - MySQL'] as $EachIndex=>$EachValue)
		{
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillists WHERE MailListID=".$EachValue;
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);	
		while ($EachRow = mysql_fetch_assoc($ResultSet))
			{		
			if ($EachRow['OutSourceDBQueryType'] == 'Basic')	
				{
				// Connect to outsourced mail list database - STARTED
				mysql_close();
				
				$MySQLLink = mysql_connect($EachRow['OutSourceDBHost'], $EachRow['OutSourceDBUsername'], $EachRow['OutSourceDBPassword']);
					mysql_select_db($EachRow['OutSourceDBName']);
				// Connect to outsourced mail list database - FINISHED
		
				$SQLQuery2	 = "SELECT * ";
				$SQLQueryTotalFound = "SELECT COUNT(*) AS TotalFound ";
				
				$SQLQuery2	.= "FROM ".$EachRow['OutSourceDBTable']." AS tblMembers ";
				$SQLQueryTotalFound .= "FROM ".$EachRow['OutSourceDBTable']." AS tblMembers ";
		
				// Apply filtering options - STARTED
				$SQLQueryPart2 = array();
				if ($EachRow['OutSourceDBWhereQuery'] != '')
					{
					$SQLQueryPart2[] = $EachRow['OutSourceDBWhereQuery'];
					}
				// Apply filtering options - FINISHED

				mysql_close();
				
				$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);	

				// Send rule SQL query filtering - Start
				$TMPReturn		= $this->CreateSendRuleSQLQueryPart();
				mysql_close();
				
				$MySQLLink = mysql_connect($EachRow['OutSourceDBHost'], $EachRow['OutSourceDBUsername'], $EachRow['OutSourceDBPassword']);
					mysql_select_db($EachRow['OutSourceDBName']);	
										
				if ($TMPReturn != '')
					{
					$SQLQueryPart2[]	= $TMPReturn;
					}
				// Send rule SQL query filtering - End						

				// Apply send rules - STARTED
				if (count($SQLQueryPart2) > 0)
					{
					$SQLQuery2 = $SQLQuery2." WHERE ".implode(" AND ", $SQLQueryPart2)." ";
					$SQLQueryTotalFound  = $SQLQueryTotalFound." WHERE ".implode(" AND ", $SQLQueryPart2)." ";
					}
				else
					{
					$SQLQuery2 = $SQLQuery2." ";
					$SQLQueryTotalFound = $SQLQueryTotalFound." ";
					}
				// Apply send rules - FINISHED

				$SQLQuery2 .= "GROUP BY tblMembers.".$EachRow['OutSourceDBEmailFieldName']." ";	
				
				// Get Totalfound for find loop amount - STARTED
				$ResultSet = ExecuteMySQLQuery($SQLQueryTotalFound, __LINE__, __FILE__);
					$TMPTotalLocalRecipients = mysql_fetch_assoc($ResultSet);
					$TMPTotalLocalRecipients = $TMPTotalLocalRecipients['TotalFound'];	
					
				$TMPLoopAmount = ceil($TMPTotalLocalRecipients / 500);						
				// Get Totalfound for find loop amount - FINISHED
				
				$PerSet=0;
				$StartFrom=0;
				
				// Getting members per step by 500 to get safe memory  - STARTED
				while($PerSet<=$TMPLoopAmount)
					{
					$StartFrom=$PerSet*500;
				
					$SQLQueryX = $SQLQuery2."limit ".$StartFrom.",500";
					$ResultSet2 = ExecuteMySQLQuery($SQLQueryX, __LINE__, __FILE__);
				
					$OutsourcedRecipientsList = array();
				
					while ($EachRow2 = mysql_fetch_assoc($ResultSet2))
						{
						$OutsourcedRecipientsList[] = $EachRow2[$EachRow['OutSourceDBEmailFieldName']];
						}
					
					$OutsourcedRecipientsList = array_keys(array_flip($OutsourcedRecipientsList));
					
					mysql_close();	

					$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);

					$this->MoveOutSourcedRecipientsToQueue($OutsourcedRecipientsList);
					
					mysql_close();

					$MySQLLink = mysql_connect($EachRow['OutSourceDBHost'], $EachRow['OutSourceDBUsername'], $EachRow['OutSourceDBPassword']);
						mysql_select_db($EachRow['OutSourceDBName']);							

					$PerSet++;
					}
				// Getting members per step by 500 to get safe memory  - FINISHED
			
				// Connect to oemPro database - STARTED
					mysql_close();	
					$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);
					}
				else
					{
				// Connect to outsourced mail list database - STARTED
				mysql_close();
				
				$MySQLLink = mysql_connect($EachRow['OutSourceDBHost'], $EachRow['OutSourceDBUsername'], $EachRow['OutSourceDBPassword']);
					mysql_select_db($EachRow['OutSourceDBName']);
				// Connect to outsourced mail list database - FINISHED						
				$SQLQuery2		= $EachRow['OutSourceDBFullSQLQuery'];
				
				
				$ResultSet2 = ExecuteMySQLQuery($SQLQuery2, __LINE__, __FILE__);
			
			
					while ($EachRow2 = mysql_fetch_assoc($ResultSet2))
					{
						$OutsourcedRecipientsList[]=$EachRow2[$EachRow['OutSourceDBEmailFieldName']];
					}
				
				
				// Connect to oemPro database - STARTED
				mysql_close();	
				$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);
				
				$OutsourcedRecipientsList = array_keys(array_flip($OutsourcedRecipientsList));

				$this->MoveOutSourcedRecipientsToQueue($OutsourcedRecipientsList);													
				}
			}			
		}
		
	$MySQLLink = DatabaseConnect($this->ArrayConfig["Database"]);		

	// Remove any duplicate emails in the outbox table - Start
	$this->RemoveDuplicatesFromOutbox();
	// Remove any duplicate emails in the outbox table - End

	// Remove banned or black listed emails in the outbox table - Start
	$this->RemoveBanndAndBlackListedFromOutbox();
	// Remove banned or black listed emails in the outbox table - End
	
	return;
	}
	
/**
 * Calculates the total amount of local and outsourced recipients
 *
 * @return void
 * @author Cem Hurturk
 **/
function CalculateRecipients()
	{
	// Retrieve and store recipient mail lists - Start
	$this->ArrayCampaignMailLists	= array();
	$this->ArrayOutGoing			= array();

	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_maillists AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."maillists AS tblMailLists ";
	$SQLQuery .= "ON tblAssign.RelMailListID = tblMailLists.MailListID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayCampaignMailLists[$EachRow['SourceType']][]	= $EachRow['MailListID'];
		}
	// Retrieve and store recipient mail lists - End
	
	// If there is any outsourced mail list, calculate outsourced subscribers - Start
	if ($this->ArrayCampaignMailLists['Outsourced - MySQL']!='')	
		{
		$this->GenerateOutsourceRecipientsList();	

		// Calculate total recipients of the campaign - Start
		$this->CalculateTotalOutsourcedRecipientsAmount();
		// Calculate total recipients of the campaign - End	
		}
	else
		{
		// Move local recipients to outbox queue table and calculate total local recipients - Start
		$this->GenerateRecipientSQLQuery();	
		// Move local recipients to outbox queue table and calculate total local recipients - End	
		// Calculate total recipients of the campaign - Start
		$this->CalculateTotalLocalRecipientsAmount();
		// Calculate total recipients of the campaign - End
		}
	// If there is any outsourced mail list, calculate outsourced subscribers - End
	}

/**
 * Calculates the total amount of recipients
 *
 * @return void
 * @author Cem Hurturk
 **/
function CalculateTotalLocalRecipientsAmount()
	{
		
	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND Source='Local'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->TotalLocalRecipients = mysql_fetch_assoc($ResultSet);
		$this->TotalLocalRecipients = $this->TotalLocalRecipients['TotalFound'];
	}
	
function CalculateTotalOutsourcedRecipientsAmount()
	{
		

	$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_outbox WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."' AND RelCampaignStatisticsID='".$this->ArrayCampaignStatistics['CampaignStatisticsID']."' AND Source='Outsourced'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->TotalOutsourcedRecipients = mysql_fetch_assoc($ResultSet);
		$this->TotalOutsourcedRecipients = $this->TotalOutsourcedRecipients['TotalFound'];

	}	

/**
 * Generates the required SQL query part for send rules of the campaign
 *
 * @return mixed
 * @author Cem Hurturk
 **/
function CreateSendRuleSQLQueryPart()
	{
	$TMPArraySQLQueryPartSendRules = array();
	
	$TMPSQLQuerySendRules = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_rules WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$TMPResultSetSendRules = ExecuteMySQLQuery($TMPSQLQuerySendRules, __LINE__, __FILE__);
		$TotalSendRules = mysql_num_rows($TMPResultSetSendRules);

	if ($TotalSendRules == 0)
		{
			
		return false;
		}

	$TMPArraySendRule = array();
	$TMPCounter = 1;

	while ($EachRule = mysql_fetch_assoc($TMPResultSetSendRules))
		{
		$TMPSendRule = "";

		// Set the operator #1 - Start
		if ($EachRule['Operator1'] == 'equals to')
			{
			$TMPOperator1 = '=';
			}
		elseif ($EachRule['Operator1'] == 'greater than')
			{
			$TMPOperator1 = '>';
			}
		elseif ($EachRule['Operator1'] == 'smaller than')
			{
			$TMPOperator1 = '<';
			}
		elseif ($EachRule['Operator1'] == 'not equals to')
			{
			$TMPOperator1 = '!=';
			}
		elseif ($EachRule['Operator1'] == 'includes')
			{
			$TMPOperator1 = 'LIKE';
			}
		elseif ($EachRule['Operator1'] == 'not includes')
			{
			$TMPOperator1 = 'NOT LIKE';
			}
		else
			{
			$TMPOperator1 = '=';
			}
		// Set the operator #1 - End

		// Set the expressions - Start
		if (($TMPOperator1 == 'LIKE') || ($TMPOperator1 == 'NOT LIKE'))
			{
			$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '%".$EachRule['Value1']."%'";
			}
		else
			{
			$TMPSendRule = $EachRule['Key']." ".$TMPOperator1." '".$EachRule['Value1']."'";
			}
		// Set the expressions - End
		
		// Set the operator #2 - Start
		if ($EachRule['Boolean1'] != '')
			{
			if ($EachRule['Operator2'] == 'equals to')
				{
				$TMPOperator2 = '=';
				}
			elseif ($EachRule['Operator2'] == 'greater than')
				{
				$TMPOperator2 = '>';
				}
			elseif ($EachRule['Operator2'] == 'smaller than')
				{
				$TMPOperator2 = '<';
				}
			elseif ($EachRule['Operator2'] == 'not equals to')
				{
				$TMPOperator2 = '!=';
				}
			elseif ($EachRule['Operator2'] == 'includes')
				{
				$TMPOperator2 = 'LIKE';
				}
			elseif ($EachRule['Operator2'] == 'not includes')
				{
				$TMPOperator2 = 'NOT LIKE';
				}
			else
				{
				$TMPOperator2 = '=';
				}

			// Set the expressions - Start
			if (($TMPOperator2 == 'LIKE') || ($TMPOperator2 == 'NOT LIKE'))
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '%".$EachRule['Value2']."%'";
				}
			else
				{
				$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean1']." ".$EachRule['Key']." ".$TMPOperator2." '".$EachRule['Value2']."'";
				}
			// Set the expressions - End
			}
		// Set the operator #2 - End
		
		if ($TotalSendRules > $TMPCounter)
			{
			$TMPSendRule = $TMPSendRule." ".$EachRule['Boolean2']." ";
			}
		
		$TMPCounter++;

		$TMPArraySendRule[] = $TMPSendRule;
		}

	$TMPSendRule = implode("", $TMPArraySendRule);
		$TMPSendRule = "(".$TMPSendRule.")";
	return $TMPSendRule;
	}

/**
 * Gets the campaign information
 *
 * @param integer $CampaignID Campaign ID information
 * @return boolean
 * @author Cem Hurturk
 **/
function GetCampaignInfo($CampaignID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns WHERE CampaignID='".$CampaignID."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	if (mysql_num_rows($ResultSet) < 1)
		{
		echo $this->ArrayErrorMessages[0][0]." - ".$this->ArrayErrorMessages[0][1];
		exit;
		}
	else
		{
			
		$this->ArrayCampaign = mysql_fetch_assoc($ResultSet);

		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrators WHERE AdministratorID='".$this->ArrayCampaign['RelAdministratorID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayAdministrator = mysql_fetch_assoc($ResultSet);

		// Decide if this is a resume or restart process - Start
		$TotalStatistics = $this->GetPendingCampaignStatisticsInfo();
			if (($TotalStatistics == 0) || ($this->ArrayCampaignStatistics['SentRecipients'] + $this->ArrayCampaignStatistics['FailedRecipients'] == 0))
				{
				$this->IsCampaignResumed = false;
				}
			else
				{
				
				$this->IsCampaignResumed = true;
				}
		// Decide if this is a resume or restart process - End

		return true;
		}
	}

/**
 * Retrieves clients, SMTP settings, attachments, contents,... of the campaign
 *
 * @return void
 * @author Cem Hurturk
 **/
function GetCampaignParameters()
	{
	// Campaign clients - Start
	$this->ArrayClients = array();

	$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_clients AS tblAssign ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."clients AS tblClients ";
	$SQLQuery .= "ON tblAssign.RelClientID = tblClients.ClientID ";
	$SQLQuery .= "WHERE tblAssign.RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayClients[] = array(
								"ClientID"		=>		$EachRow['ClientID'],
								"Name"			=>		$EachRow['Name'],
								"Email"			=>		$EachRow['Email'],
								);
		}
	// Campaign clients - End
	
	// Retrieve campaign SMTP settings - Start
	$this->ArraySMTP = array();

	if ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Qmail')
		{
		$this->ArraySMTP = array('Type' => 'QMail');
		}
	elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Sendmail')
		{
		$this->ArraySMTP = array('Type' => 'Sendmail');
		}
	elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Local MTA')
		{
		$this->ArraySMTP = array('Type'	=> 'MTA');
		}
	elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'SMTP Server')
		{
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
		$SQLQuery .= "WHERE tblSMTP.SMTPServerID = '".$this->ArrayAdministrator['ForcedSendingMethodID']."' LIMIT 0,1";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$TMPArraySMTP = mysql_fetch_assoc($ResultSet);

		$this->ArraySMTP[] = array(
								'Type'						=>	'SMTP',
								'Host'						=>	$TMPArraySMTP['ServerHost'],
								'Port'						=>	$TMPArraySMTP['ServerPort'],
								'Auth'						=>	$TMPArraySMTP['ServerAuth'],
								'Username'					=>	$TMPArraySMTP['ServerUsername'],
								'Password'					=>	$TMPArraySMTP['ServerPassword'],
								'TimeOutSec'				=>	$TMPArraySMTP['ServerTimeoutSeconds'],
								'MaxEmailsPerInterval'		=>	$TMPArraySMTP['MaxEmailsPerInterval'],
								'SecondsDelayPerInterval'	=>	$TMPArraySMTP['SecondsDelayPerInterval'],
								);
		}
	elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'Raw File')
		{
		$this->ArraySMTP = array('Type' => 'SaveAsFile');
		}
	elseif ($this->ArrayAdministrator['ForcedSendingMethod'] == 'N/A')
		{
		if ($this->ArrayCampaign['SendEngine'] == 'SMTP')
			{
			$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_smtp AS tblAssign ";
			$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."smtp_servers AS tblSMTP ";
			$SQLQuery .= "ON tblAssign.RelSMTPServerID = tblSMTP.SMTPServerID ";
			$SQLQuery .= "WHERE tblAssign.RelCampaignID = '".$this->ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

			while ($TMPArraySMTP = mysql_fetch_assoc($ResultSet))
				{
				$this->ArraySMTP[] = array(
										'Type'						=>	'SMTP',
										'Host'						=>	$TMPArraySMTP['ServerHost'],
										'Port'						=>	$TMPArraySMTP['ServerPort'],
										'Auth'						=>	$TMPArraySMTP['ServerAuth'],
										'Username'					=>	$TMPArraySMTP['ServerUsername'],
										'Password'					=>	$TMPArraySMTP['ServerPassword'],
										'TimeOutSec'				=>	$TMPArraySMTP['ServerTimeoutSeconds'],
										'MaxEmailsPerInterval'		=>	$TMPArraySMTP['MaxEmailsPerInterval'],
										'SecondsDelayPerInterval'	=>	$TMPArraySMTP['SecondsDelayPerInterval'],
										);
				}
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'MTA')
			{
			$this->ArraySMTP = array('Type'	=> 'MTA');
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'QMail')
			{
			$this->ArraySMTP = array('Type'	=> 'QMail');
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'Sendmail')
			{
			$this->ArraySMTP = array('Type'	=> 'Sendmail');
			}
		elseif ($this->ArrayCampaign['SendEngine'] == 'SaveAsFile')
			{
			$this->ArraySMTP = array('Type'	=> 'SaveAsFile');
			}
		}
	// Retrieve campaign SMTP settings - End

	// Retrieve campaign attachments - Start
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_attachments WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$this->ArrayAttachments = array();
	
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		$this->ArrayAttachments[] = array(
										"AttachmentID"		=>	$EachRow['AttachmentID'],
										"FileName"			=>	$EachRow['FileName'],
										"FileSize"			=>	$EachRow['FileSize'],
										);
		}
	// Retrieve campaign attachments - End

	// Retrieve campaign content - Start
	if ($this->ArrayCampaign['CampaignType'] == 'Quick Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayContent = mysql_fetch_assoc($ResultSet);

		// Check if content or link is available - Start
		if ($this->ArrayContent['ContentMainURL'] != '')
			{
			$TMPRetrievedContent = RetrieveRemoteContent($this->ArrayContent['ContentMainURL'], $this->ArrayConfig);
			$this->ArrayContent['ContentMain'] = $TMPRetrievedContent;
			}

		if ($this->ArrayContent['ContentAlternateURL'] != '')
			{
			$TMPRetrievedContent = RetrieveRemoteContent($this->ArrayContent['ContentAlternateURL'], $this->ArrayConfig);
			$this->ArrayContent['ContentAlternate'] = $TMPRetrievedContent;
			}
		// Check if content or link is available - End
		}
	elseif ($this->ArrayCampaign['CampaignType'] == 'Newsletter Template Based Campaign')
		{
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_cms WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

		if (mysql_num_rows($ResultSet) > 0)
			{
			// CMS based newsletter
			$ArrayCMS = mysql_fetch_assoc($ResultSet);
			
			$this->ArrayContent = $ArrayCMS;

			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_cms_refs WHERE RelContentCMSID='".$ArrayCMS['ContentCMSID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		
			while ($EachRow = mysql_fetch_assoc($ResultSet))
				{
				$this->ArrayContent['CMSParts'][$EachRow['Key']] = $EachRow;
				}
			
			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."newsletter_templates WHERE NewsletterTemplateID='".$this->ArrayContent['RelNewsletterTemplateID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayNewsletter = mysql_fetch_assoc($ResultSet);

			// Copy charset, content-type, priority info to content array - Start
			$this->ArrayContent['CharSet']		=	$this->ArrayNewsletter['CharSet'];
			$this->ArrayContent['Priority']		=	$this->ArrayNewsletter['Priority'];
			$this->ArrayContent['ContentType']	=	$this->ArrayNewsletter['ContentType'];
			// Copy charset, content-type, priority info to content array - End
			}
		else
			{
			// Traditional newsletter
			$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_content WHERE RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
			$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayContent = mysql_fetch_assoc($ResultSet);
			}
		}

	// Add administrator header and footer to the content - Start
		include_once($this->ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
	$this->ArrayContent = AddAdministratorHeaderFooter($this->ArrayContent, $this->ArrayCampaign['RelAdministratorID'], $this->ArrayConfig);
	// Add administrator header and footer to the content - End

	$this->MakeContentReady();
	// Retrieve campaign content - End

	// Retrieve from/reply-to and return-path email addresses - Start
	// From email address - Start
	if ($this->ArrayCampaign['FromEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelFromEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelFromEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayFromEmail = mysql_fetch_assoc($ResultSet);
	// From email address - End

	// Reply-to email address - Start
	if ($this->ArrayCampaign['ReplyToEmailIDType'] == 'Administrator')
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelReplyToEmailID']."'";
		}
	else
		{
		$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelReplyToEmailID']."'";
		}
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$this->ArrayReplyToEmail = mysql_fetch_assoc($ResultSet);
	// Reply-to email address - End

	// Return-path email address- Start
	if ($this->ArrayAdministrator['ReturnPathEmail'] != '')
		{
		$this->ArrayReturnPathEmail = array();
			$this->ArrayReturnPathEmail['EmailAddress'] = $this->ArrayAdministrator['ReturnPathEmail'];
			$this->ArrayReturnPathEmail['Name']			= '';
		}
	else
		{
		if ($this->ArrayCampaign['ReturnPathEmailIDType'] == 'Administrator')
			{
			$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."administrator_emails WHERE EmailID='".$this->ArrayCampaign['RelReturnPathEmailID']."'";
			}
		else
			{
			$SQLQuery ="SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."maillist_emails WHERE EmailAddressID='".$this->ArrayCampaign['RelReturnPathEmailID']."'";
			}
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ArrayReturnPathEmail = mysql_fetch_assoc($ResultSet);
		}
	// Return-path email address- End
	// Retrieve from/reply-to and return-path email addresses - End
	}

/**
 * Parses the content and makes it ready for delivery
 *
 * @return void
 * @author Cem Hurturk
 **/
function MakeContentReady()
	{
	include_once('./system/libraries/cron_sendengine.inc.php');	

	// Define subject - Start
	$TMPSubject = $this->ArrayContent['Subject'];
	// Define subject - End

	// Analyze subject. If it is in "multi-mode", explode into array - Start
	if (count(explode("\n", $TMPSubject)) > 1)
		{
		$TMPSubject = explode("\n", $TMPSubject);
		}
	// Analyze subject. If it is in "multi-mode", explode into array - End

	// Define content - Start
	if (count($this->ArrayNewsletter) > 0)
		{
		// CMS based content
		$TMPPriority 			= $this->ArrayNewsletter['Priority'];
		$TMPCharSet				= $this->ArrayNewsletter['CharSet'];
		$TMPContentType			= $this->ArrayNewsletter['ContentType'];

		if ($TMPContentType == 'Html')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentHTML'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}
		elseif ($TMPContentType == 'Both')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentHTML'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentPlain'], $this->ArrayContent['CMSParts']);
			}
		elseif ($TMPContentType == 'Plain')
			{
			$TMPContentMain			= IntegrateCMSPartsToContent($this->ArrayNewsletter['ContentPlain'], $this->ArrayContent['CMSParts']);
			$TMPContentAlternate	= '';
			}

		// Add administrator header and footer to the content - STARTED
			include_once($this->ArrayConfig['Paths']['Software']."/system/libraries/campaigns.inc.php");
			$TMPArrayContent = array(
									"ContentMain"		=> $TMPContentMain,
									"ContentAlternate"	=> $TMPContentAlternate,
									"ContentType"		=> $TMPContentType,
									);
		$TMPArrayContent = AddAdministratorHeaderFooter($TMPArrayContent, $this->ArrayCampaign['RelAdministratorID'], $this->ArrayConfig);
			$TMPContentMain			= $TMPArrayContent['ContentMain'];
		
			$TMPContentAlternate	= $TMPArrayContent['ContentAlternate'];
		// Add administrator header and footer to the content - FINISHED
		}
	else
		{
		// Traditional template or quick mode
		$TMPPriority 			= $this->ArrayContent['Priority'];
		$TMPCharSet				= $this->ArrayContent['CharSet'];
		$TMPContentType			= $this->ArrayContent['ContentType'];
		$TMPContentMain			= $this->ArrayContent['ContentMain'];
		$TMPContentAlternate	= $this->ArrayContent['ContentAlternate'];
		}
	// Define content - End

	$this->ArrayContent['Subject']			= $TMPSubject;
	$this->ArrayContent['ContentType']		= $TMPContentType;
	$this->ArrayContent['ContentMain']		= $TMPContentMain;
	$this->ArrayContent['ContentAlternate']	= $TMPContentAlternate;
	}

/**
 * Updates the campaign statistics record on database
 *
 * @param string $Status
 * @param string $StatusDescription
 * @param integer $TotalRecipients
 * @param integer $SentRecipients
 * @param integer $FailedRecipients
 * @param string $SendStartDateTime
 * @param string $SendFinishDateTime
 * @param string $SendLastActionDateTime
 * @param integer $InstantEmailsPerSecond
 * @param integer $AverageEmailsPerSecond
 * @param integer $TotalSentEmailSizeBytes
 * @param integer $QueuePointer
 * @param integer $QueuePointerOutsource
 * @param integer $QueuePointerRelOutsourcedMailListID
 * @return void
 * @author Cem Hurturk
 **/



function UpdateCampaignStatistics($Status, $StatusDescription, $TotalRecipients, $SentRecipients, $FailedRecipients, $SendStartDateTime, $SendFinishDateTime, $SendLastActionDateTime, $InstantEmailsPerSecond, $AverageEmailsPerSecond, $TotalSentEmailSizeBytes, $QueuePointer, $QueuePointerOutsource, $QueuePointerRelOutsourcedMailListID)
	{
		
	
	
	$this->GetCampaignStatisticsInfo($CampaignStatisticsID);
//FIXME: Even values are set to 0, it's getting the default value from table because 0 is equal to ''. It's causing problems on ban / black filtering
	$ArrayFieldnValues = array(
							"CampaignStatisticsID"		=>	($this->ArrayCampaignStatistics['CampaignStatisticsID'] == '' ? '' : $this->ArrayCampaignStatistics['CampaignStatisticsID']),
							"RelCampaignID"				=>	$this->ArrayCampaign['CampaignID'],
							"Status"					=>	($Status == '' ? $this->ArrayCampaignStatistics['Status'] : $Status),
							"Sleeping"					=>	0,	
							"StatusDescription"			=>	($StatusDescription == '' ? $this->ArrayCampaignStatistics['StatusDescription'] : $StatusDescription),
							"TotalRecipients"			=>	($TotalRecipients == '' ? $this->ArrayCampaignStatistics['TotalRecipients'] : $TotalRecipients),
							"SentRecipients"			=>	($SentRecipients == '' ? $this->ArrayCampaignStatistics['SentRecipients'] : $SentRecipients),
							"FailedRecipients"			=>	($FailedRecipients == '' ? $this->ArrayCampaignStatistics['FailedRecipients'] : $FailedRecipients),
							"SendStartDateTime"			=>	($SendStartDateTime == '' ? $this->ArrayCampaignStatistics['SendStartDateTime'] : $SendStartDateTime),
							"SendFinishDateTime"		=>	($SendFinishDateTime == '' ? $this->ArrayCampaignStatistics['SendFinishDateTime'] : $SendFinishDateTime),
							"SendLastActionDateTime"	=>	($SendLastActionDateTime == '' ? $this->ArrayCampaignStatistics['SendLastActionDateTime'] : $SendLastActionDateTime),
							"InstantEmailsPerSecond"	=>	($InstantEmailsPerSecond == '' ? $this->ArrayCampaignStatistics['InstantEmailsPerSecond'] : $InstantEmailsPerSecond),
							"AverageEmailsPerSecond"	=>	($AverageEmailsPerSecond == '' ? $this->ArrayCampaignStatistics['AverageEmailsPerSecond'] : $AverageEmailsPerSecond),
							"TotalSentEmailSizeBytes"	=>	($TotalSentEmailSizeBytes == '' ? $this->ArrayCampaignStatistics['TotalSentEmailSizeBytes'] : $TotalSentEmailSizeBytes),
							"QueuePointer"				=>	($QueuePointer == '' ? $this->ArrayCampaignStatistics['QueuePointer'] : $QueuePointer),
							"QueuePointerOutsource"		=>	($QueuePointerOutsource == '' ? $this->ArrayCampaignStatistics['QueuePointerOutsource'] : $QueuePointerOutsource),
							"QueuePointerRelOutsourcedMailListID"	=>	($QueuePointerRelOutsourcedMailListID == '' ? $this->ArrayCampaignStatistics['QueuePointerRelOutsourcedMailListID'] : $QueuePointerRelOutsourcedMailListID),
							);

	$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
		$FieldList = "`".$FieldList."`";
	$ValueList = implode("','", array_values($ArrayFieldnValues));
		$ValueList ="'".$ValueList."'";

	if ($this->ArrayCampaignStatistics['CampaignStatisticsID'] == '')
		{
		$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$CampaignStatisticsID = mysql_insert_id();
		}
	else
		{
		$SQLQuery = "REPLACE INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}

	$this->GetCampaignStatisticsInfo($CampaignStatisticsID);
	}

/**
 * Retrieves the campaign statistic information
 *
 * @param integer $CampaignStatisticsID Campaign statistics ID
 * @return void
 * @author Cem Hurturk
 **/
function GetCampaignStatisticsInfo($CampaignStatisticsID)
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE CampaignStatisticsID='".($CampaignStatisticsID == '' ? $this->ArrayCampaignStatistics['CampaignStatisticsID']: $CampaignStatisticsID)."' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
	}

/**
 * Retrieves the pending campaign statistics information
 *
 * @return integer
 * @author Cem Hurturk
 **/
function GetPendingCampaignStatisticsInfo()
	{
	$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE Status='Pending' AND RelCampaignID='".$this->ArrayCampaign['CampaignID']."'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	
	$TotalStatistics = mysql_num_rows($ResultSet);

	if ($TotalStatistics > 0)
		{
		$this->ArrayCampaignStatistics = mysql_fetch_assoc($ResultSet);
		}	
	
	return ($TotalStatistics);
	}

/**
 * Retrieves one of the pending campaigns and returns the information of the campaign
 *
 * @return array
 * @author Cem Hurturk
 **/
function RetrievePendingCampaigns()
	{
	// Initialize variables - Start
	$ArrayCampaignIDs	= array();
	// Initialize variables - End

	// Retrieve all "send now" campaigns - Start
	$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='Send Now' AND IntegrityCheck='Ready'";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
    
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this campaign has been sent or not - Start
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."' AND Status='Pending'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalPendingFound = mysql_fetch_assoc($ResultSet2);
			$TotalPendingFound = $TotalPendingFound['TotalFound'];

		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."' AND Status!='Pending'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalNotPendingFound = mysql_fetch_assoc($ResultSet2);
			$TotalNotPendingFound = $TotalNotPendingFound['TotalFound'];

		if (($TotalPendingFound == 0) && ($TotalNotPendingFound > 0))
			{
			continue;
			}
		// Check if this campaign has been sent or not - End

		$ArrayCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "send now" campaigns - End

	// Retrieve all "future scheduled" campaigns - Start
	$SQLQuery  = "SELECT tblCampaigns.CampaignID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='One time scheduled' AND (tblSchedule.ScheduleSendDate <= NOW() OR tblSchedule.ScheduleSendDate = '0000-00-00 00:00:00') AND tblCampaigns.IntegrityCheck='Ready'";
		
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);

	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this campaign has been sent or not - Start
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."' AND Status='Pending'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalPendingFound = mysql_fetch_assoc($ResultSet2);
			$TotalPendingFound = $TotalPendingFound['TotalFound'];

		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachRow['CampaignID']."' AND Status!='Pending'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalNotPendingFound = mysql_fetch_assoc($ResultSet2);
			$TotalNotPendingFound = $TotalNotPendingFound['TotalFound'];

		if (($TotalPendingFound == 0) && ($TotalNotPendingFound > 0))
			{
			continue;
			}
		// Check if this campaign has been sent or not - End

		$ArrayCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "future scheduled" campaigns - End

	// Retrieve all "recursive scheduled" campaigns - Start
	$CurrentDayOfMonth	=	date("j");
	$CurrentDayOfWeek	=	date("w");
	$CurrentWeekOfMonth	= ((int)($CurrentDayOfMonth / 7));
		$CurrentWeekOfMonthModulus = $CurrentDayOfMonth % 7;
		if ($CurrentWeekOfMonthModulus > 0)
			{
			$CurrentWeekOfMonth++;
			}
	$CurrentMonth		=	date("n");
	$CurrentHour		=	date("G");
	$CurrentMinute		=	date("i");
		// Strip leading zero from the minute - Start
		if (substr($CurrentMinute, 0, 1) == '0')
			{
			$CurrentMinute = substr($CurrentMinute, 1, strlen($CurrentMinute));
			}
		// Strip leading zero from the minute - End
		
		// Round the minute with 15 minutes intervals - Start
		if (($CurrentMinute >= 0) && ($CurrentMinute <= 14))
			{
			$CurrentMinute = 0;
			}
		elseif (($CurrentMinute >= 15) && ($CurrentMinute <= 29))
			{
			$CurrentMinute = 15;
			}
		elseif (($CurrentMinute >= 30) && ($CurrentMinute <= 44))
			{
			$CurrentMinute = 30;
			}
		elseif (($CurrentMinute >= 45) && ($CurrentMinute <= 59))
			{
			$CurrentMinute = 45;
			}
		// Round the minute with 15 minutes intervals - End

	$SQLQuery  = "SELECT tblCampaigns.CampaignID, tblSchedule.ScheduleID ";
	$SQLQuery .= "FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule as tblSchedule ";
	$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns as tblCampaigns ";
	$SQLQuery .= "ON tblSchedule.RelCampaignID = tblCampaigns.CampaignID ";
	$SQLQuery .= "WHERE tblSchedule.ScheduleType='Continuous scheduled' ";
	$SQLQuery .= "AND (tblSchedule.ScheduleMonth = '*' OR tblSchedule.ScheduleMonth='".$CurrentMonth."' OR tblSchedule.ScheduleMonth LIKE '".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth.",%' OR tblSchedule.ScheduleMonth LIKE '%,".$CurrentMonth."') ";
	$SQLQuery .= "AND (((tblSchedule.ScheduleDayOfMonth = '*' OR tblSchedule.ScheduleDayOfMonth='".$CurrentDayOfMonth."' OR tblSchedule.ScheduleDayOfMonth LIKE '".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth.",%' OR tblSchedule.ScheduleDayOfMonth LIKE '%,".$CurrentDayOfMonth."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleDayOfWeek = '*' OR tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."' OR tblSchedule.ScheduleDayOfWeek LIKE '".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek.",%' OR tblSchedule.ScheduleDayOfWeek LIKE '%,".$CurrentDayOfWeek."')) ";
	$SQLQuery .= "OR (tblSchedule.ScheduleDayOfMonth='".$CurrentWeekOfMonth."' AND tblSchedule.ScheduleDayOfWeek='".$CurrentDayOfWeek."'))";
	$SQLQuery .= "AND (tblSchedule.ScheduleHour = '*' OR tblSchedule.ScheduleHour='".$CurrentHour."' OR tblSchedule.ScheduleHour LIKE '".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour.",%' OR tblSchedule.ScheduleHour LIKE '%,".$CurrentHour."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleMinute = '*' OR tblSchedule.ScheduleMinute='".$CurrentMinute."' OR tblSchedule.ScheduleMinute LIKE '".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute.",%' OR tblSchedule.ScheduleMinute LIKE '%,".$CurrentMinute."') ";
	$SQLQuery .= "AND (tblSchedule.ScheduleSendInstance < tblSchedule.ScheduleSendMaxInstance OR tblSchedule.ScheduleSendMaxInstance='0') ";
	$SQLQuery .= "AND tblCampaigns.IntegrityCheck='Ready' ";
	$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);


		$ProcessedOn = date("Y-m-d H:".sprintf('%02d', $CurrentMinute));
	while ($EachRow = mysql_fetch_assoc($ResultSet))
		{
		// Check if this sending has been ran before or not - Start
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive WHERE RelCampaignID='".$EachRow['CampaignID']."' AND DATE_FORMAT(ProcessedOn, '%Y-%m-%d %H:%i')='".$ProcessedOn."'";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet2);
			$TotalFound = $TotalFound['TotalFound'];

		if ($TotalFound > 0)
			{
			continue;
			}
		// Check if this sending has been ran before or not - End

		// Record the process to "campaigns_schedule_recursive" - Start
		$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule_recursive (ID, RelCampaignID, ProcessedOn) VALUES ('', '".$EachRow['CampaignID']."', '".$ProcessedOn."')";
		$ResultSet2 = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		// Record the process to "campaigns_schedule_recursive" - End

		$ArrayCampaignIDs[] = $EachRow['CampaignID'];
		}
	// Retrieve all "recursive scheduled" campaigns - End

	// The following lines are disabled because it was causing problems with re-send future scheduled campaigns. It's been fixed above.
	// Retrieve all "resumed" campaigns - Start
	// 
	// $SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics AS tblStatistics INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."campaigns_schedule AS tblSchedules ON tblStatistics.RelCampaignID =tblSchedules.RelCampaignID WHERE tblStatistics.Status='Pending' AND tblStatistics.Sleeping='0' AND (tblSchedules.ScheduleType = 'Send Now' OR tblStatistics.StatusDescription='Sending campaign')";
	// $ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
	// 
	// while ($EachRow = mysql_fetch_assoc($ResultSet))
	// 	{
	// 	$ArrayCampaignIDs[] = $EachRow['RelCampaignID'];
	// 	}
	// Retrieve all "resumed" campaigns - End

	// Check if owner of these campaigns have enough sending quota - Start
	$TMPArrayCampaignIDs	= $ArrayCampaignIDs;
	$ArrayCampaignIDs		= array();
	
	foreach ($TMPArrayCampaignIDs as $EachCampaignID)
		{
		$IsAllowedQuota		= false;
		$IsAllowedTimeLimit = false;

		// Check available send amount limit - Start
		$SQLQuery  = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns AS tblCampaigns ";
		$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."administrators as tblAdmins ";
		$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdmins.AdministratorID ";
		$SQLQuery .= "INNER JOIN ".$this->ArrayConfig['Database']['Prefix']."administrator_limits as tblAdminLimits ";
		$SQLQuery .= "ON tblCampaigns.RelAdministratorID = tblAdminLimits.RelAdministratorID ";
		$SQLQuery .= "WHERE tblCampaigns.CampaignID = '".$EachCampaignID."' ";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$ArrayCampaignAdminLimits = mysql_fetch_assoc($ResultSet);

		if ($ArrayCampaignAdminLimits['Limit'] != 'Unlimited')
			{
			include_once('./system/libraries/quota.inc.php');
			$QuotaUsed = CalculateSendLimits($ArrayCampaignAdminLimits['AdministratorID'], $ArrayCampaignAdminLimits, $this->ArrayConfig);
			$RemainingQuota = $ArrayCampaignAdminLimits['LimitAmount'] - $QuotaUsed;
			
			if ($RemainingQuota > 0)
				{
				$IsAllowedQuota = true;
				}
			}
		else
			{
			$IsAllowedQuota = true;
			}
		// Check available send amount limit - End

		// Check available send time limit - Start
		$TodayDate	= date('D');
		$TodayTime	= strtotime(date('Y-m-d H:i:s'));
		$AllowedDays = explode(',', $ArrayCampaignAdminLimits['TimeLimitDates']);
		
		if ($AllowedDays[0] != '')
			{
			foreach ($AllowedDays as $EachDay)
				{
				if ($TodayDate == $EachDay)
					{
					if (($TodayTime >= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitFrom']))) && ($TodayTime <= strtotime(date('Y-m-d '.$ArrayCampaignAdminLimits['TimeLimitTo']))))
						{
						$IsAllowedTimeLimit = true;
						break;
						}
					}
				}
			}
		else
			{
			$IsAllowedTimeLimit = true;
			}
		// Check available send time limit - End

		if (($IsAllowedQuota == true) && ($IsAllowedTimeLimit == true))
			{
			$ArrayCampaignIDs[] = $EachCampaignID;
			}
		}
	// Check if owner of these campaigns have enough sending quota - End

	// Check if these campaigns have active sending or not - Start
	$TMPArrayCampaignIDs	= $ArrayCampaignIDs;
	$ArrayCampaignIDs		= array();
	
	foreach ($TMPArrayCampaignIDs as $EachCampaignID)
		{
		$SQLQuery = "SELECT COUNT(*) AS TotalFound FROM ".$this->ArrayConfig['Database']['Prefix']."campaigns_statistics WHERE RelCampaignID='".$EachCampaignID."' AND Status='Sending'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$TotalFound = mysql_fetch_assoc($ResultSet);
			$TotalFound = $TotalFound['TotalFound'];
		if ($TotalFound < 1)
			{
			$ArrayCampaignIDs[] = $EachCampaignID;
			}
		}
	// Check if these campaigns have active sending or not - End

	$this->ArrayPendingCampaignIDs = array_unique($ArrayCampaignIDs);
	}

/**
 * Updates Oempro's process log
 *
 * @param string $StartTime
 * @param string $FinishTime
 * @param string $ProcessStatus
 * @param string $Extra1 Campaign
 * @param string $Extra2 Campaign
 * @param string $Extra3 Campaign
 * @return void
 * @author Cem Hurturk
 **/
function UpdateProcessLog($StartTime, $FinishTime, $ProcessStatus, $Extra1, $Extra2, $Extra3)
	{
	if ($this->ProcessLogID == '')
		{
		// Create the new process log entry
		$ArrayFieldnValues = array(
								"LogID"				=>	'',
								"Module"			=>	$this->ProcessLogModule,
								"ProcessStartTime"	=>	$StartTime,
								"ProcessFinishTime"	=>	$FinishTime,
								"ProcessStatus"		=>	$ProcessStatus,
								"Extra1"			=>	$Extra1,
								"Extra2"			=>	$Extra2,
								"Extra3"			=>	$Extra3,
								);
		}
	else
		{
		// Update existing process log entry
		$SQLQuery = "SELECT * FROM ".$this->ArrayConfig['Database']['Prefix']."process_log WHERE LogID='".$this->ProcessLogID."'";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		$ArrayLog = mysql_fetch_assoc($ResultSet);

		$ArrayFieldnValues = array(
								"LogID"				=>	$this->ProcessLogID,
								"Module"			=>	($this->ProcessLogModule == '' ? $ArrayLog['Module'] : $this->ProcessLogModule),
								"ProcessStartTime"	=>	($StartTime == '' ? $ArrayLog['ProcessStartTime'] : $StartTime),
								"ProcessFinishTime"	=>	($FinishTime == '' ? $ArrayLog['ProcessFinishTime'] : $FinishTime),
								"ProcessStatus"		=>	($ProcessStatus == '' ? $ArrayLog['ProcessStatus'] : $ProcessStatus),
								"Extra1"			=>	($Extra1 == '' ? $ArrayLog['Extra1'] : $Extra1),
								"Extra2"			=>	($Extra2 == '' ? $ArrayLog['Extra2'] : $Extra2),
								"Extra3"			=>	($Extra3 == '' ? $ArrayLog['Extra3'] : $Extra3),
								);
		}

	$FieldList = implode("`,`", array_keys($ArrayFieldnValues));
		$FieldList = "`".$FieldList."`";
	$ValueList = implode("','", array_values($ArrayFieldnValues));
		$ValueList ="'".$ValueList."'";

	if ($this->ProcessLogID == '')
		{
		$SQLQuery = "INSERT INTO ".$this->ArrayConfig['Database']['Prefix']."process_log (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
			$this->ProcessLogID = mysql_insert_id();
		}
	else
		{
		$SQLQuery = "REPLACE INTO ".$this->ArrayConfig['Database']['Prefix']."process_log (".$FieldList.") VALUES (".$ValueList.")";
		$ResultSet = ExecuteMySQLQuery($SQLQuery, __LINE__, __FILE__);
		}
	}



} // END class SendEngine



?>