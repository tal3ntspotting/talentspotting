<?php

/**
 * This is the Raw Email File Generator for Swift Mailer, a PHP Mailer class.
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Cem Hurturk - Octeth - www.octeth.com
 * @date	26th December 2006
 * @license	http://www.gnu.org/licenses/lgpl.txt Lesser GNU Public License
 *
 * @copyright Copyright &copy; 2006 Octeth - www.octeth.com - All Rights Reserved.
 * @filesource
 * 
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to
 *
 *   The Free Software Foundation, Inc.,
 *   51 Franklin Street,
 *   Fifth Floor,
 *   Boston,
 *   MA  02110-1301  USA
 *
 *    "Cem Hurturk" <cem@octeth.com>
 *
 */

/**
 * SMTP Connection Class.
 * Connects to a remote MTA and stores the connections internally
 * @package Swift
 */
class Swift_Connection_RawEmailFile
{
	function Swift_Connection_RawEmailFile()
	{
	}
	/**
	 * Establishes a connection with the MTA
	 * The SwiftInstance Object calls this
	 *
	 * @return	bool	connected
	 */
	function start()
	{
	}
	/**
	 * Establishes a connection with the MTA
	 *
	 * @return	bool	connected
	 * @private
	 */
	function connect()
	{
	}
	/**
	 * Closes the connection with the MTA
	 * Called by the SwiftInstance object
	 *
	 * @return	void
	 */
	function stop()
	{
	}
	/**
	 * Closes the connection with the MTA
	 * @return	void
	 */
	function disconnect()
	{
	}
	/**
	 * Returns TRUE if the socket is connected
	 * @return bool connected
	 */
	function isConnected()
	{
	}
	/**
	 * Change the default timeout from 30 seconds
	 * @param int timeout secs
	 * @return void
	 */
	function setConnectTimeout($seconds)
	{
	}
}

?>