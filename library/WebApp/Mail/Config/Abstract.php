<?php
abstract class WebApp_Mail_Config_Abstract
{
    /**
     * Zend_Mail
     * @var Netbridge_Mail
     */
    protected $_mail;

    /**
     * Timeout for the host ( secounds )
     * @var int
     */
    protected $_hostTimeOut = 1;

    /**
     * Construct
     * @param Netbridge_Mail $mail
     */
    public function __construct(Zend_Mail $mail)
    {
        $this->_mail = $mail;
    }

    /**
     * Config Zend_Mail object
     * @param Zend_Mail $mail
     */
    abstract public function init();

    /**
     * 
     * Check if an ip is online
     * @param string $host
     * @param int $port
     */
    public function checkOnline($host, $port)
    {
        $fp = @fsockopen($host, $port, $errno, $errstr, $this->_hostTimeOut);

        if (!$fp) {
            return false;
        } else {
            stream_set_timeout($fp, 2);
            fwrite($fp, "EHLO");
            stream_set_timeout($fp,$this->_hostTimeOut);
            $info = fread($fp, 200);
            fclose($fp);

            if ($info) {
                return true;    
            } else {
                return false;
            }
        }
    }

    /**
     * Set timeout for the host
     * @param int $seconds
     */
    public function setHostTimeOut($seconds)
    {
        $this->_hostTimeOut = $seconds;
    }

    /**
     * Get timeout for the host
     */
    public function getHostTimeOut()
    {
        return $this->_hostTimeOut;
    }
}