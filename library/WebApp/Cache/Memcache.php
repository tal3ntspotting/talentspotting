<?php

class WebApp_Cache_Memcache
{

    private static $instance = null;
    public $_cache ;
    public $_slug;
    public $_settings;

    // The constructor is private
    // to prevent initiation with outer code.
    private function __construct()
    {
        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $subdomain = explode('.', $this->_settings['sitename']);
        if (in_array("www", $subdomain)) {
            array_shift($subdomain);
        }
        $subdomain = $subdomain[0];
        $slug = preg_replace('#^https?://#', '', $subdomain);
        $this->_slug = $slug;
        $host = "127.0.0.1";
        $port = 11211;
        $this->_cache = new Memcache();
        $this->_cache->addServer($host, $port);
        $this->_cache->connect($host, $port) or die ("Could not connect") ;

    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null)
        {

            self::$instance = new self();
        }

        return self::$instance;
    }

    public function set($key,$value)
    {
        $key = $this->_slug.":".$key;
        $this->_cache->set($key,$value);
    }

    public function get($key)
    {
        $key = $this->_slug.":".$key;
        return $this->_cache->get($key);
    }

    public function delete($key)
    {
        $key = $this->_slug.":".$key ;
        $this->_cache->delete($key);
    }
}