<?php
/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/Abstract.php';

class WebApp_Validate_AgainstDb extends Zend_Validate_Abstract
{
    const VALUE_EXISTS = 'ValueExistsInDatabase';
    const VALUE_NOT_EXISTS = 'ValueNotExistsInDatabase';
    const DB_ADAPTER_ERROR = "DbAdapterError";
    const DB_QUERY_ERROR = "DbQueryError";
    const VALUE_CUSTOM = 'Custom';
    
    protected $_messageTemplates = array(
        self::VALUE_EXISTS   		=> "'%value%' already exists in the database.",
        self::VALUE_NOT_EXISTS 		=> "'%value%' does not exist in the database.",
        self::DB_ADAPTER_ERROR 		=> "Eroare la conectarea la baza de date.",
        self::DB_QUERY_ERROR		=> "Database query error",
        self::VALUE_CUSTOM			=> "a"
    );

    protected $table = '';
    protected $field = '';
    protected $validation_type = '';
	protected $exclude = null;
	protected $excludeValue = null;
    protected $excludeType = null;
	
    public function __construct($table, $field, $validation_type = 'NOT_EXISTS', $exclude = null, $excludeValue = null)
    {
        $this->table = $table;
        $this->field = $field;
        $this->validation_type = $validation_type;
		$this->exclude = $exclude;
		$this->excludeValue = $excludeValue;
    }

    public function setExcludeType($value) 
    {
        $this->excludeType = $value;
        return $this;
    }

	public function setExclude($value) 
    {
		$this->exclude = $value;
		return $this;
	}

	public function getExclude() 
    {
		return $this->exclude;
	}

	public function setExcludeValue($value) 
    {
		$this->excludeValue = $value;
		return $this;
	}

	public function getExcludeValue() 
    {
		return $this->excludeValue;
	}
	
    public function setEroare($eroarea = '') 
    {
		$this->setMessage($eroarea, self::VALUE_EXISTS);
	}

    public function isValid($value) 
    {
		$valueString = (string) $value;
		$this->_setValue($valueString);
		
        if ($dbAdapter = Zend_Db_Table::getDefaultAdapter()) 
        {
            if (!empty($value) && !empty($this->table) && !empty($this->field)) 
            {
				try {
					$sql = $dbAdapter->select()
	       	   	   					->from($this->table, array($this->field))
	       	   	   					->where('`'.$this->field.'` = ?', $value)
									->limit(1,0);

					if(!empty($this->exclude) && !empty($this->excludeValue)) 
                    {
						if (is_array($this->exclude) && is_array($this->excludeValue) && count($this->exclude) > 0 && count($this->exclude) == count($this->excludeValue)) 
                        {
							for ($a=0; $a < count($this->exclude); $a++) 
                            {
                                if (isset($this->excludeType[$a])) 
                                {
                                    $semn = $this->excludeType[$a];

                                } 
                                else 
                                {
                                    $semn = ' <> ';
                                }
								$sql->where('`'.$this->exclude[$a].'` '.$semn.'  ?', $this->excludeValue[$a]);
							}
						} 
                        else 
                        {
							$sql->where('`'.$this->exclude.'` <>  ?', $this->excludeValue);
						}
					}

	       	   	   	$res = $dbAdapter->fetchAll($sql);
	       	   	   	if (($this->validation_type == 'EXISTS' && count($res) == 1) || ($this->validation_type == 'NOT_EXISTS' && count($res) <= 0)) 
                    {
	       	   	   		return true;

	       	   	   	} 
                    else 
                    {
	       	   	   		if (count($res) == 1) 
                        {
	       	   	   			$this->_error(self::VALUE_EXISTS);
       						return false;
	       	   	   		} 
                        else 
                        {
	       	   	   			$this->_error(self::VALUE_NOT_EXISTS);
       						return false;
	       	   	   		}
	       	   	   	}
       	   		} 
                catch (Zend_Exception $e) 
                {
					$this->_error(self::DB_QUERY_ERROR);
       				return false;
       	   		}
       	   }
       }
	   $this->_error(self::DB_ADAPTER_ERROR);
       return false;
    }
}