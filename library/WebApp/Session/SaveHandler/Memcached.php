<?php
class WebApp_Session_SaveHandler_Memcached implements Zend_Session_SaveHandler_Interface
{
    private static $_lifetime = null;
    private static $_label = null;
    private $_memcache = null;

    private $_defaultValues = array(
        'persistent'	=> true,
        'weight'        => 1,
        'timeout'       => 1,
        'retryInterval'	=> 15,
        'status'        => true,
    );

    public function getMemcache()
    {
    	if (null == $this->_memcache) {
    	    $this->_memcache = new Memcache();
    	}

	   return $this->_memcache;
    }

    public function __construct(array $options = array(), $lifetime = null)
    {
        // load default lifetime
        if (null !== $lifetime) {
            self::$_lifetime = $lifetime;
        }

    	// load default options
    	foreach ($this->_defaultValues as $key => $value) {
    	    if (!array_key_exists($key, $options)) {
    		$options[$key] = $value;
    	    }
    	}
    
    	// load label name
    	self::$_label = __CLASS__;
    	if (array_key_exists('label', $options)) {
    	    self::$_label = $options['label'];
    	}

    	$this->_memcache = new Memcache();

        if (is_array($options['servers']) && count($options['servers']) > 0)
        {
            foreach ($options['servers'] as $value)
            {
                $this->_memcache->addServer($value['host'], $value['port'], $options['persistent'],
    				    $options['weight'], $options['timeout'], $options['retryInterval'], $options['status'], array($this, 'handleServerError'));
            }
        }
    
	
    }

    public function handleServerError($host, $port)
    {
        if (Zend_Registry::isRegistered('Zend_Log')) {
            $log = Zend_Registry::get('Zend_Log');
            $log->log("Memcached server $host:$port failed on connection", Zend_Log::ERR);
            $log->log("URL = " . 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], Zend_Log::DEBUG);
        }
    }

    public function __destruct()
    {
        Zend_Session::writeClose();
    }

    /**
     * Open Session - retrieve resources
     *
     * @param string $save_path
     * @param string $name
     */
    public function open($save_path, $name)
    {
        return true;
    }

    /**
     * Close Session - free resources
     *
     */
    public function close()
    {
        return true;
    }

    /**
     * Read session data
     *
     * @param string $id
     */
    public function read($id)
    {
        return $this->_memcache->get(self::$_label. "/sessions/{$id}", MEMCACHE_COMPRESSED);
    }

    /**
     * Write Session - commit data to resource
     *
     * @param string $id
     * @param mixed $data
     */
    public function write($id, $data)
    {
        return $this->_memcache->set(self::$_label. "/sessions/{$id}", $data, MEMCACHE_COMPRESSED, self::$_lifetime);
    }

    /**
     * Destroy Session - remove data from resource for
     * given session id
     *
     * @param string $id
     */
    public function destroy($id)
    {
        return $this->_memcache->delete(self::$_label. "/sessions/{$id}", 0);
    }

    /**
     * Garbage Collection - remove old session data older
     * than $maxlifetime (in seconds)
     *
     * @param int $maxlifetime
     */
    public function gc($maxlifetime)
    {
        return true;
    }

}
