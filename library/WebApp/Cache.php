<?php
class WebApp_Cache
{
    protected $_cache;
    protected $_cachedEntity;
    protected $_cacheKey;
    protected $_cacheTag;
    protected $_queryCacheEnabled;

    public function __construct($cachedEntity, $cacheKey = null, $cacheTag = null)
    {
        if ( Zend_Controller_Front::getInstance()->getParam('bootstrap') )
        {
            $resources = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_queryCacheEnabled = $resources['resources']['Cache']['queryCacheEnabled'];
        }
        $this->_cachedEntity = $cachedEntity;
        $this->_cache = (Zend_Registry::isRegistered('Zend_Cache_Core')) ? Zend_Registry::get('Zend_Cache_Core') : null ;

        if ($this->_queryCacheEnabled == 1)
        {
            $this->_cacheKey = $cacheKey;
            $this->_cacheTag = $cacheTag;
        }
        return $cachedEntity;
    }
    /**
     * The key of this class
     * @param string $method
     * @param array $args
     * @throws Exception
     */
    public function __call($method, $args)
    {
        $class = is_object($this->_cachedEntity) ? get_class($this->_cachedEntity) : $this->_cachedEntity;
        $class_methods = get_class_methods($class);

        if(in_array($method, $class_methods))
        {
            $caller = array($this->_cachedEntity, $method);

            if (isset($this->_cacheKey) && null !== $this->_cacheKey)
            {
                if ($data = $this->_cache->load($this->_cacheKey))
                {
                    return $data;
                }
                else
                {
                    $data = call_user_func_array($caller, $args);
                    if (is_object($data))
                        $data = $data->toArray();

                    $this->_cache->save($data, $this->_cacheKey);
                    if (isset($this->_cacheTag) && null !== $this->_cacheTag)
                    {
                        $this->_setTag();
                    }
                    return $data;
                }
            }
            else
            {
                $data = call_user_func_array($caller, $args);
                if (is_object($data))
                    $data = $data->toArray();

                return $data;
            }
        }
        throw new Exception( " Method " . $method . " does not exist in this class " . get_class($class ) . "." );
    }

    public function removeKey($key)
    {
        return $this->_cache->remove($key);
    }

    public function removeTag($tag)
    {
        if ($data = $this->_cache->load($tag))
        {
            foreach ($data as $key)
            {
                $this->_cache->remove($key);
            }
            $this->_cache->remove($tag);
        }
    }

    private function _setTag()
    {
        if ($tag = $this->_cache->load($this->_cacheTag))
        {
            if (!in_array($this->_cacheKey, $tag))
            {
                array_push($tag, $this->_cacheKey);
                $this->_cache->save($tag, $this->_cacheTag);
            }
        }
        else
        {
            $tag = array($this->_cacheKey);
            $this->_cache->save($tag, $this->_cacheTag);
        }
    }
}
