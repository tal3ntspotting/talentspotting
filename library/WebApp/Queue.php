<?php

class WebApp_Queue
{
	protected $_database;
	protected $_queryQueueEnabled;
	protected $_queue;
	protected $_options;

	public function __construct($database)
	{
		if ( Zend_Controller_Front::getInstance()->getParam('bootstrap') )
		{
			$this->_options = $resources['resources']['Queue'];
		}

		$this->_database = $database;
		$this->_queue = Zend_Registry::get('Zend_Queue');

		return $this;
	}

	public function insert($data)
	{
		if ($this->_queue instanceof Zend_Queue_Adapter_Memcacheq)
		{
			$this->_addToQueue(serialize($data));
		}
		else
		{
			return $this->_insertDb($data);
		}
	}

	/**
	 * @param (array) - ('fields' => <array of fields to be updated>, 'where' => <where condition as string>, 'table' => <table name as string>)
	 */
	public function update($data)
	{
		if ($this->_queue instanceof Zend_Queue_Adapter_Memcacheq)
		{
			$this->_addToQueue(serialize($data));
		}
		else
		{
			return $this->_updateDb($data);
		}
	}

	public function delete($data)
	{
		if ($this->_queue instanceof  Zend_Queue_Adapter_Memcacheq)
		{
			$this->_addToQueue(serialize($data));
		}
		else
		{
			return $this->_deleteDb($data);
		}
	}

	private function _insertDb($data)
	{
		if (empty($data['tables']))
		{
			return false;
		}

		$insert_result = true;

		foreach ($data['tables'] as $table => $fields)
		{
			if (empty($data['relations']) || !array_key_exists($table, $data['relations'])) {
				$insert_result = $insert_result && $this->_database->insert($fields, $table);
				$data['tables'][$table]['id'] = $this->_database->lastInsertId();
			}
		}

		if (!empty($data['relations']))
		{
			$result = true;

			foreach ($data['relations'] as $destination_table => $fk)
			{
				$fk_fields = array();
				foreach ($fk as $fk_field => $source) {
					$source_breakdown = explode('.', $source);
					$source_table = $source_breakdown[0];
					$source_field = $source_breakdown[1];

					$fk_fields[$fk_field] = $data['tables'][$source_table][$source_field];
				}

				$result = $result && $this->_database->insert(array_merge($data['tables'][$destination_table], $fk_fields), $destination_table);
				$data['tables'][$destination_table]['id'] = $this->_database->lastInsertId();

				$source_breakdown = array();
				$update = array();
			}
		}

		// if everything went well, return $data['tables'] so we have the insert id for every table,
		// and use it further if needed
		if (empty($data['relations'])) {
			if ($insert_result) {
				return $data;
			} else {
				return false;
			}
		}

		if ($insert_result && $result) {
			return $data;
		} else {
			return false;
		}

	}

	private function _updateDb ($data = array())
	{
		if (!count($data)) {
			return false;
		}

		return $this->_database->update($data['fields'], $data['where'], $data['table']);
	}

	private function _deleteDb ($data = array())
	{
		if (!count($data)) {
			return false;
		}

		return $this->_database->delete($data['where'], $data['table']);
	}

	private function _addToQueue($data)
	{
		$this->_queue->send($data);
	}
}