<?php
set_time_limit(0);
class WebApp_UploadImage
{
	public static function cropImage($nw, $nh, $source, $dest, $watermark = false) 
    {
        $size = getimagesize($source);
	    $w = $size[0];
	    $h = $size[1];
	    $stype = $size['mime']; 
	    
	    switch($stype) 
        {
	        case 'image/gif': $simg = imagecreatefromgif($source); break;
	        case 'image/jpeg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/jpg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/png': $simg = imagecreatefrompng($source);  break;
	    }
        
		//$dest .= $ext; 
		
	    $dimg = imagecreatetruecolor($nw, $nh); 
        
        
        if($w < $h) 
        {
            $wm = $w/$nw;
            $hm = $h/$nh;
            
            $h_height = $nh/2;
            $w_height = $nw/2;
        
            $adjusted_height = $h / $wm;
	        $half_height = $adjusted_height / 2;
	        $int_height = $half_height - $h_height;
	        imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
	    } 
        else
        {
            imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
        }        
        
        /*
		$wm = $w/$nw;
		$hm = $h/$nh;
	
	    $h_height = $nh/2;
	    $w_height = $nw/2;
	
	    if($w > $h) 
        {
	        $adjusted_width = $w / $hm;	
	        $half_width = $adjusted_width / 2;	
	        $int_width = $half_width - $w_height;
	        imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
	
	    } 
        elseif(($w < $h) || ($w == $h)) 
        {
	        $adjusted_height = $h / $wm;
	        $half_height = $adjusted_height / 2;
	        $int_height = $half_height - $h_height;
	        imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
	
	    } 
        else 
        {
	        imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
	    }
        */

	    imagejpeg($dimg,$dest,100);
	    
        if ($watermark === true)
        {   
            $image_p = imagecreatefromstring(file_get_contents($dest));
        
            $watermark = imagecreatefrompng("resources/images/admin/logo.png");
            $watermark_width = imagesx($watermark);
            $watermark_height = imagesy($watermark);
            
            imagecopy($image_p, $watermark, 0, 0, 0, 0, $watermark_width, $watermark_height);            
            imagejpeg($image_p, $dest, 100);            
        }
        
	    $list = explode('/',  $dest);
	    return (array_pop($list));	    
	}
    
    public static function remove($path, $image)
    {
        $fullpath = ABSOLUTE_PATH."/{$path}";
        
        unlink($fullpath."/big/{$image}");
        unlink($fullpath."/thumb/{$image}");
    }	
	
	public static function constrainImage($nw, $nh, $source, $dest, $watermark = false) 
    {
        $size = getimagesize($source);
	    $w = $size[0];
	    $h = $size[1];
	    $stype = $size['mime']; 
	    
	    switch($stype) 
        {
	        case 'image/gif': $simg = imagecreatefromgif($source); break;
	        case 'image/jpeg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/jpg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/png': $simg = imagecreatefrompng($source);  break;
	    }

        $percent_width=$nw*100/$w;
        $percent_height=$nh*100/$h;
        $percent_width<$percent_height ? $percent=$percent_width : $percent=$percent_height;
        
        $new_height = $h*$percent/100;
        $new_width = $w*$percent/100;        
        
        if ($nw >= $w && $nh >= $h)
        {
            $new_width = $w;
            $new_height = $h;
        }    
        
        if($w < $h)
        {
            $new_width = $nw;
        }    
        
        //echo $new_width."|".$new_height; die();
        
        $pos = intval(($nw - $new_width)/2);
        				
	    $dimg = imagecreatetruecolor($new_width,$new_height); 
        $backgroundColor = imagecolorallocate($dimg, 255, 255, 255);
        imagefill($dimg, 0, 0, $backgroundColor);
        
        $start_x = floor(($nw / 2) - ($new_width / 2));
        $start_y = floor(($nh / 2) - ($new_height / 2));
        
        if($w < $h) 
        {
            $start_x = floor(($nw / 2) - ($w*$percent/100 / 2));
	        imagecopyresampled($dimg,$simg,$start_x,0,0,0,$w*$percent/100,$new_height,$w,$h);
	    }
        else
        {
            imagecopyresampled($dimg,$simg,0,0,0,0,$new_width,$new_height,$w,$h);    
        }

	    imagejpeg($dimg,$dest,100);
        
        if ($watermark === true)
        {   
            $image_p = imagecreatefromstring(file_get_contents($dest));
                        
            $watermark = imagecreatefrompng("resources/images/admin/logo.png");
            $watermark_width = imagesx($watermark);
            $watermark_height = imagesy($watermark);
            
            if($w < $h)
            {
                imagecopy($image_p, $watermark, $start_x, 0, 0, 0, $watermark_width, $watermark_height);
            }
            else
            {
                imagecopy($image_p, $watermark, 0, 0, 0, 0, $watermark_width, $watermark_height);
            }            
            imagejpeg($image_p, $dest, 100);            
        }        
        // Set the content type header - in this case image/jpeg        
        
	    $list = explode('/',  $dest);        
	    return (array_pop($list));	    
	}
    
    public static function constrainImageGrey($nw, $nh, $source, $dest) 
    {
        $size = getimagesize($source);
	    $w = $size[0];
	    $h = $size[1];
	    $stype = $size['mime']; 
	    
	    switch($stype) 
        {
	        case 'image/gif': $simg = imagecreatefromgif($source); break;
	        case 'image/jpeg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/jpg': $simg = imagecreatefromjpeg($source);  break;
	        case 'image/png': $simg = imagecreatefrompng($source);  break;
	    }
        
		//$dest .= $ext; 
		
        $percent_width=$nw*100/$w;
        $percent_height=$nh*100/$h;
        $percent_width<$percent_height ? $percent=$percent_width : $percent=$percent_height;
        
        $new_height=$h*$percent/100;
        $new_width=$w*$percent/100;
        
				
	    $dimg = imagecreatetruecolor($nw,$new_height); 
        $backgroundColor = imagecolorallocate($dimg, 255, 255, 255);
        imagefill($dimg, 0, 0, $backgroundColor);        
	    imagecopyresampled($dimg,$simg,0,0,0,0,$new_width,$new_height,$w,$h);
        
        if (imagefilter($dimg, IMG_FILTER_GRAYSCALE))
        {
            imagepng($dimg,$dest,9);
   
        }
        else
        {
            imagejpeg($dimg,$dest,90);
        }
	            
	    
	    $list = explode('/',  $dest);
	    return (array_pop($list));	    
	}
    
    public static function saveConstrainedFixedImage($nw, $nh, $source, $pos = 'right', $dest = null)
    {
        $image = new Imagick();
        $image->newImage($nw, $nh, "#FFFFFF");
        $image->setImageFormat("jpg");
        $image->setGravity(Imagick::GRAVITY_CENTER);
        
        if (file_exists($source) && is_file($source))
        {
            $box = new Imagick($source);    
        }
        else
        {
            $box = new Imagick(ABSOLUTE_PATH."/public/resources/files/companies/list/no_image.png");
        }
        $box->scaleImage($nw, $nh, true);
        $box->setImageFormat("jpg");        
        $geometry = self::getImageGeometry($box, $nw, $nh, $pos);        
        
        $image->compositeImage($box, Imagick::COMPOSITE_OVER, $geometry['left'], $geometry['top']);
        
        if (null !== $dest)
        {
            $image->writeImage($dest);    
        }
        else
        {
            header("Content-Type: image/jpeg");
            $progress = $image->getImageBlob();
            echo $progress;
            exit();                       
        }
    }
    
    public static function saveCroppedFixedImage($nw, $nh, $source)
    {
        $image = new Imagick();
        $image->newImage($nw, $nh, "#FFFFFF");
        $image->setImageFormat("jpg");
        $image->setGravity(Imagick::GRAVITY_CENTER);
        
        if (file_exists($source) && is_file($source))
        {
            $box = new Imagick($source);    
        }
        else
        {
            $box = new Imagick(ABSOLUTE_PATH."/public/resources/files/companies/list/no_image.png");
        }
        //$box->scaleImage($nw, $nh, true);
        $box->cropImage($nw, $nh, $nw/2, 50);
        
        $box->setImageFormat("jpg");      
        
        $width = $box->getImageWidth();  
        $height = $box->getImageHeight();
        $box->roundCorners($width,$height);
        
        $geometry = self::getImageGeometry($box, $nw, $nh, 'center');
                
        
        $image->compositeImage($box, Imagick::COMPOSITE_OVER, $geometry['left'], $geometry['top']);
        
        if (null !== $dest)
        {
            $image->writeImage($dest);    
        }
        else
        {
            header("Content-Type: image/jpeg");
            $progress = $image->getImageBlob();
            echo $progress;
            exit();                       
        }
    }
    
    public static function getImageGeometry($image, $width, $height, $pos)
    {
        $geometry = $image->getImageGeometry();
        
        
        if ($geometry['width'] == $width)
        {
            $position['left'] = 0;
        }
        else
        {
            $position['left'] = intval(($width - $geometry['width']) / 2);
        }
                
        
        if ($geometry['height'] == $height)
        {
            $position['top'] = 0;
        }
        else
        {
            $position['top'] = intval(($height - $geometry['height']) / 2);
        }
        
        if ($pos == 'left')
        {
            $position['left'] = 0;
        }
        
        if ($pos == 'right')
        {
            //$position['left'] = $width;
        }
        
        return $position;
    }

}