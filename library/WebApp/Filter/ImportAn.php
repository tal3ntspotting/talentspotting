<?php

class WebApp_Filter_ImportAn implements Zend_Filter_Interface
{
    protected $_db;
    
    public function __construct()
    {        
        $this->_db = Zend_Registry::get('dbAdapter');
    }

    public function filter($value)
    {
        if (isset($value))
        {
            $an = $value;
            
            if (is_object($value))
                $an = $value->__toString();
            
            if (is_numeric($an))
            {
                return $an;
            }
            else
            {
               return date("Y", strtotime($an));    
            }
        }
        else
        {
            return FILTER_NUMERIC_ERROR_VALUE;
        }
    }
}