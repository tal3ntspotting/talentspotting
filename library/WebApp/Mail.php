<?php
class WebApp_Mail extends Zend_Mail
{
    protected $email_type ;
    protected $email_condition ;
    protected $email_logger ;
    protected $_viewScriptsPath = '';
    protected $_legend = array('[user.firstname]','[user.company_title]', '[user.lastname]', '[user.id]', '[user.email]', '[user.password]', '[req_user.firstname]', '[req_user.lastname]', '[data.date]', '[company.firstname]', '[company.lastname]', '[event.slug]', '[event.title]', '[current_user.firstname]', '[current_user.lastname]', '[current_user.id]', '[message.content]', '[session.title]', '[session.start]', '[session.end]', '[session.id]', '[user.rest_auth_token]', '[event.tickets]', '[payment.amount]', '[user.code]', '[package.title]', '[package.type]', '[user.confirmation]');

    public function __construct($charset = 'utf-8')
    {
        $this->_viewScriptsPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails';
        parent::__construct($charset);
    }
    
    public function setBodyFromViewScript($name, $params = null, $type = 'html', $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        if ($params == null || !is_array($params))
            $params = array();

        $view = new Zend_View();
        $view->setScriptPath($this->_viewScriptsPath);
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $bodyData = $partialHelper->partial( $name, $params );

        if ($type == 'html') 
        {
            $this->setBodyHtml($bodyData, $charset, $encoding);
        } 
        else 
        {
            $this->setBodyText($bodyData, $charset, $encoding);
        }
    }

    public function send($transport = null)
    {       
        return parent::send($transport);
    }
    
    public function setViewScriptPath($path)
    {
        $this->_viewScriptsPath = $path;
    }

    public function getViewScriptPath()
    {
        return $this->_viewScriptsPath;
    }
    
    public function setBodyFromHTML($data, $params = null, $type = 'html', $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        $bodyData = $this->personalizeEmail($data, $params);
        $this->setBodyHtml($bodyData, $charset, $encoding);        
    }
    
    public function personalizeEmail($data, $params)
    {
        foreach ($this->_legend as $variable)
        {
            if (strpos($data, $variable) !== false)
            {
                switch ($variable)
                {
                    case "[user.firstname]":
                        $data = str_replace($variable, $params['data']['firstname'], $data);
                    break;
                    case "[user.company_title]":
                        $data = str_replace($variable, $params['data']['company_title'], $data);
                    break;
                    
                    case "[user.lastname]":
                        $data = str_replace($variable, $params['data']['lastname'], $data);
                    break;
                    
                    case "[user.id]":
                        $data = str_replace($variable, $params['data']['id'], $data);
                    break;
                    
                    case "[user.email]":
                        $data = str_replace($variable, $params['data']['email'], $data);
                    break;
                    
                    case "[user.confirmation]":
                        $url = APPLICATION_URL."/confirm-account/?hash=".$params['hash'];
                        $data = str_replace($variable, $url, $data);
                    break;
                    
                    case "[user.rest_auth_token]":
                        $data = str_replace($variable, $params['data']['rest_auth_token'], $data);
                    break;
                    
                    case "[user.password]":
                        $data = str_replace($variable, $params['password'], $data);
                    break;
                    
                    case "[user.code]":
                        $data = str_replace($variable, $params['code'], $data);
                    break;
                    
                    case "[data.date]":
                        $data = str_replace($variable, $params['date'], $data);
                    break;
                                                            
                    case "[event.title]":
                        $data = str_replace($variable, $params['event']['title'], $data);
                    break;
                    
                    case "[event.slug]":
                        $data = str_replace($variable, $params['event']['slug'], $data);
                    break;
                    
                    case "[event.tickets]":
                        $data = str_replace($variable, $params['tickets'], $data);
                    break;
                    
                    case "[session.title]":
                        $data = str_replace($variable, $params['session']['title'], $data);
                    break;
                    
                    case "[session.slug]":
                        $data = str_replace($variable, $params['session']['slug'], $data);
                    break;
                    
                    case "[session.start]":
                        $data = str_replace($variable, date("d F Y, H:i", strtotime($params['session']['start'])), $data);
                    break;
                    
                    case "[session.end]":
                        $data = str_replace($variable, date("d F Y, H:i", strtotime($params['session']['end'])), $data);
                    break;
                    
                    case "[session.id]":
                        $data = str_replace($variable, date("d F Y, H:i", strtotime($params['session']['id'])), $data);
                    break;
                    
                    case "[company.firstname]":
                        $data = str_replace($variable, $params['company']['firstname'], $data);
                    break;
                    
                    case "[company.lastname]":
                        $data = str_replace($variable, $params['company']['lastname'], $data);
                    break;
                    
                    case "[req_user.firstname]":
                        $data = str_replace($variable, $params['requester']['firstname'], $data);
                    break;
                    
                    case "[req_user.lastname]":
                        $data = str_replace($variable, $params['requester']['lastname'], $data);
                    break;
                    
                    case "[current_user.firstname]":
                        $data = str_replace($variable, $params['current_user']['firstname'], $data);
                    break;
                    
                    case "[current_user.lastname]":
                        $data = str_replace($variable, $params['current_user']['lastname'], $data);
                    break;
                    
                    case "[current_user.id]":
                        $data = str_replace($variable, $params['current_user']['id'], $data);
                    break;
                    
                    case "[message.content]":
                        $data = str_replace($variable, $params['message']['content'], $data);
                    break;
                    
                    case "[payment.amount]":
                        $data = str_replace($variable, $params['amount_ron'], $data);
                    break;
                    
                    case "[package.title]":
                        $data = str_replace($variable, $params['package']['title'], $data);
                    break;
                }
            }
        }        
        
        return $data;
    }
    
    public function setSubject($data, $params = array())
    {        
        $subject = (!empty($params)) ? $this->personalizeEmail($data, $params) : $data;        
        parent::setSubject($subject);
    }

    public function addTo($email, $name='')
    {
        parent::addTo($email, $name='');
    }

    public function addEmailType($type)
    {
        $this->email_type = $type;
    }

    public function getEmailType()
    {
        return $this->email_type ;
    }

    public function setLog($result)
    {
        $this->email_logger = $result;
    }

    public function getLog()
    {
        return $this->email_logger;
    }

    public function setCondition($condition)
    {
        $this->email_condition = $condition ;
    }

    public function getCondition()
    {
        return $this->email_condition;
    }


}