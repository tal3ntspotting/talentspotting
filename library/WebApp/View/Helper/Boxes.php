<?php
/** Zend_Registry */
require_once 'Zend/Registry.php';

/** Zend_View_Helper_Abstract.php */
require_once 'Zend/View/Helper/Abstract.php';

class WebApp_View_Helper_Boxes extends Zend_View_Helper_Abstract {

    static protected  $_starCounter = 0;

	public function __construct() {
	}

	function boxes() {
		return $this;
	}

    public function getFacebookBox()
    {
        return "";
        $modelMmp = new models_Mmp();

        if ($modelMmp->userIsLoggedIntoFacebook())
        {
            $logout_url = '/mmp/index/logout';
            return $this->view->partial('partials/logged/facebook-box.phtml', null, array('login' => true, 'logout_url' => $logout_url));
        }
        else
        {
            $login_url = $modelMmp->getFacebookLoginUrl();
            return $this->view->partial('partials/logged/facebook-box.phtml', null, array('login' => false, 'login_url' => $login_url));
        }
    }
}