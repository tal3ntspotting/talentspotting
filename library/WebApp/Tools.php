<?php

class WebApp_Tools
{
    protected static $_varName = null;

    public static function initRuntime($varName = 'TIME_START')
    {
        self::$_varName = $varName;
        define($varName, microtime(true));
    }

    public static function getRuntime($unit = false)
    {
        $diff = microtime(true) - constant(self::$_varName);
        if (false === $unit) {
            if ($diff >= 1) {
                $unit = 's';
            } else {
                $unit = 'ms';
            }
        }
        switch ($unit) {
            case 's':
                $time = number_format($diff, 3, '.', ','). 's';
                break;
            case 'ms':
            default:
                $time = number_format($diff*1000, 2, '.', ','). 'ms';
        }

        return $time;
    }


    public static function CreateAlias($name, $dash = '-') {
        //$alias = str_replace(">", $dash, $name);
		$alias = htmlspecialchars_decode(trim($name), ENT_QUOTES);
		$alias = str_replace(
				array('&#259;','&#355;','&#351;','&#0226;','&#0171;','&#0187;','&#0238;','&#0206;', '&#350;', '&#0259;', '&#0355;', '&#259;', '&#0350;', '&_0350;', '&#0351;'),
				array(  'a',     't',     's',      'a',     '"',      '"',      'i',      'I',        'S',   'a',          't', 		'a',     'S',       'S',       's'),
				$alias);
		$alias = str_replace(
			array('$', '\\','/','.',  ' ', '(', ')',  '%', '`', ',', '\n', '\r', '\t', '#', '"', "'", ":", "!","?", "__", ">"),
			$dash,
			$alias
			);
		$alias = preg_replace('/[^a-zA-Z_\-0-9\.]/', '', $alias);
		$alias = str_replace('__', $dash, $alias);
        $alias = str_replace('--', $dash, $alias);
        $alias = str_replace('---', $dash, $alias);
        $alias = str_replace('-', $dash, $alias);
        $alias = str_replace('_', $dash, $alias);
		$alias1 =  str_replace('__', '_', $alias);
		$alias1 = strtolower(trim($alias1, ' -'));

		return $alias1;
    }
    
    public static function ClearString($name, $dash = '') {
        //$alias = str_replace(">", $dash, $name);
        $alias = htmlspecialchars_decode(trim($name), ENT_QUOTES);
        $alias = str_replace(
                array('&#259;', '&#355;', '&#351;', '&#0226;', '&#0171;', '&#0187;', '&#0238;', '&#0206;', '&#350;', '&#0259;', '&#0355;', '&#259;', '&#0350;', '&_0350;', '&#0351;'), array('a', 't', 's', 'a', '"', '"', 'i', 'I', 'S', 'a', 't', 'a', 'S', 'S', 's'), $alias);
        $alias = str_replace(
                array('$', '\\', '/', '.', '(', ')', '%', '`', ',', '\n', '\r', '\t', '#', '"', "'", ":", "!", "?", "__", ">"), $dash, $alias
        );
        $alias = preg_replace('/[^a-zA-Z_\-0-9\.]/', ' ', $alias);
        $alias = str_replace('__', $dash, $alias);
        $alias = str_replace('--', $dash, $alias);
        $alias = str_replace('---', $dash, $alias);
        $alias = str_replace('-', $dash, $alias);
        $alias1 = $alias = str_replace('__', '_', $alias);
        $alias1 = strtolower(trim($alias1, ' -'));

        return $alias1;
    }

    public static function getAssocResults($rowset, $id_key = 'id', $id_val = null) {
		$retur = array ();
		if (is_object ( $rowset )) {
			$rowval = $rowset->toArray ();
			echo 'a';
		} else if (is_array($rowset)) {
			$rowval = $rowset;
		}
		if (is_array($rowval)) {

			if (count ( $rowval ) > 0) {
				foreach ( $rowval as $k2 ) {
					if (is_object($k2)) {
						$k = $k2->toArray();
					} else {
						$k = $k2;
					}
					if ($id_key && strlen ( $id_val ) > 0) {
						$retur [$k [$id_key]] = $k [$id_val];
					} else {
						$retur [$k [$id_key]] = $k;
					}
				}
			}
		}

		return $retur;
	}

	public function cleanPhone($phone){
		 $phone = preg_replace('/[^0-9+]/','',$phone);
		 return $phone;
	}


	// Luna in format ro
	public function roLuna($data){
		$luni = array('Jan' => 'Ian',
					  'Feb' => 'Feb',
					  'Mar' => 'Mar',
					  'Apr' => 'Apr',
					  'May' => 'Mai',
					  'Jun'	=> 'Iun',
					  'Jul' => 'Iul',
					  'Aug' => 'Aug',
					  'Sep' => 'Sep',
					  'Oct' => 'Oct',
					  'Nov' => 'Nov',
					  'Dec'	=> 'Dec'
		);

		$rodata = $luni[$data];
		return $rodata;
	}

    public static function strTrim(&$str, $maxLength = 100, $finishChr = NULL, $maxOverflow = 15, $overspace = false)
    {
        $str = strip_tags($str);
    	if (strlen($str) > $maxLength)
    	{
    		$output = NULL;
    		$body = explode(" ", $str);
    		$body_count = count($body);

            $i=0;
    		do
    		{
    			$output .= $body[$i]." ";
    			$thisLen = strlen($output);
    			$cycle = ($thisLen < $maxLength && $i < $body_count-1 && ($thisLen+strlen($body[$i+1])) < $maxLength + $maxOverflow ? true : false);

                $i++;
    		}
    		while ($cycle);
    		return $output.$finishChr;
    	}
    	else
        {
            if ($overspace == true)
            {
                $dist = $maxLength - strlen($str);
                for ($i = 1; $i <= $dist; $i++)
                {
                    $overspace_txt .= "&nbsp;";
                }
                
                return $str.(string)$overspace_txt;
            }
            else
            {
                return $str;
            }  
        } 
    }

    public static function dateDiff($time1, $time2, $precision = 6)
    {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1))
        {
            $time1 = strtotime($time1);
        }

        if (!is_int($time2))
        {
            $time2 = strtotime($time2);
        }

        if ($time1 > $time2)
        {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year','month','day','hour','minute','second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval)
        {
            // Set default diff to 0
            $diffs[$interval] = 0;
            // Create temp time from time1 and interval
            $ttime = strtotime("+1 " . $interval, $time1);
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime)
            {
                $time1 = $ttime;
                $diffs[$interval]++;
                // Create new temp time from time1 and interval
                $ttime = strtotime("+1 " . $interval, $time1);
            }
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value)
        {
            // Break if we have needed precission
            if ($count >= $precision)
            {
                break;
            }

            // Add value and interval
            // if value is bigger than 0
            if ($value > 0)
            {

                // Add s if value is not 1
                if ($value != 1)
                {
                    $interval .= "s";
                }

                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }

        // Return string with times
        return implode(", ", $times);
    }

    public static function showNiceConcat($string)
    {
        $str = str_replace(array(',,', ',,,', ',,,,', ',,,,'), ', ', $string);

        if (substr($str, strlen($str) - 1, 1) == ",")
        {
            $str = substr($str, 0, -1);
        }

        $returnString = $str;
        $original = $str;
        if (strpos($str, ',') !== false)
        {
            $returnString = substr($str, 0, strpos($str, ',')).' <a href="#" class="moreGroups" title="'.$original.'" original-title="'.$original.'"> ...more</a>';
        }

        return $returnString;
    }

    public function campaignPersonalize($string, $contact, $Tag = '%')
    {
    	// Personalize the string according to defined information
    	foreach ($contact as $key => $value)
        {
            $string = str_replace($Tag."Contact:".$key.$Tag, $value, $string);
  		}
        //personalize string with custom data(read, click, unsubscribe, htmlversion)    	     
        $string = $this->conditionalPersonalization($string, $contact);    	

    	return $string;
    }

    public function conditionalPersonalization($string, $contact)
    {        
        $string = $this->replaceStaticUrlsInEmail($string, $contact);
        $string = $this->replaceLinksInEmail($string, $contact);
        $string = $this->addTrackingImage($string, $contact);        

        return $string;
    }
    
    public function replaceLinksInEmail($string, $contact)
    {
        $string = stripslashes($string);
        $ArrayLinks = array();
        
        $NewString = str_replace("</a>", "</a>\n", $string);
        $TMPPattern = "/<a(.*)>.*<\/a>/isU";
        preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);
        
        $TMPCounter = 0;
        foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
        {
            $SearchOn = $ArrayEachMatch[1];
            if (strpos($SearchOn, 'post_social.php') === false)
            {
                $TMPPattern = "/name=\"([^\r\n]*)\"/iU";
                preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
                               
                $TMPPattern = "/href=\"([^\r\n]*)\"/isU";
                preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
                
                $search = $ArraySubMatches[0][0];
                
                $url = $ArraySubMatches[0][1];                                
                $replace = 'href="'.APPLICATION_URL.'/link-click/?id_campaign='.$contact['id_campaign'].'&link='.base64_encode($url).'&email='.base64_encode($contact['email']).'"';
                
                if (stripos($search, "http") !== false)
                {
                    $string = str_replace($search, $replace, $string);
                }
            }
        }
        
        $TMPPattern = "/<area(.*)>/isU";
        preg_match_all($TMPPattern, $NewString, $ArrayMatches, PREG_SET_ORDER);
        
        foreach ($ArrayMatches as $EachIndex=>$ArrayEachMatch)
        {
            $SearchOn = $ArrayEachMatch[1];
            $TMPPattern = "/name=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
            
            $TMPPattern = "/href=\"([^\r\n]*)\"/iU";
            preg_match_all($TMPPattern, $SearchOn, $ArraySubMatches, PREG_SET_ORDER);
            
            $search = $ArraySubMatches[0][0];
            $link = $ArraySubMatches[0][1];                
            $params = parse_url($link);
            
            $url = $ArraySubMatches[0][1];                                
            $replace = 'href="'.APPLICATION_URL.'/link-click/?id_campaign='.$contact['id_campaign'].'&link='.base64_encode($url).'&email='.base64_encode($contact['email']).'"';
            
            if (stripos($search, "http") !== false)
            {
                $string = str_replace($search, $replace, $string);
            }
		}
                
        return $string;
    }
    
    public function replaceStaticUrlsInEmail($string, $contact)
    {
        $pattern = "%UnSubscribe%";
        $replace = APPLICATION_URL."/unsubscribe/?id_campaign=".$contact['id_campaign']."&email=".base64_encode($contact['email']);
        $string = str_replace($pattern, $replace, $string);
        
        $pattern = "%link:unsubscribe%";
        $replace = APPLICATION_URL."/unsubscribe/?id_campaign=".$contact['id_campaign']."&email=".base64_encode($contact['email']);
        $string = str_replace($pattern, $replace, $string);
        
        $pattern = "%HTMLVersion%";
        $replace = APPLICATION_URL."/html-version/?id_campaign=".$contact['id_campaign']."&email=".base64_encode($contact['email']);
        $string = str_replace($pattern, $replace, $string);

        return $string;
    }
    
    public function addTrackingImage($string, $contact)
    {
        $url = '<img src="'.APPLICATION_URL.'/email-read/?id_campaign='.$contact['id_campaign'].'&email='.base64_encode($contact['email']).'" width="10" height="10" alt="---">';
        $newstring = str_replace('</body>', "\n".$url."\n\n</body>", $string);
        if ($newstring == $string)
        {
            $string .= "\n\n".$url;
        }
        else        
        {
            $string = $newstring;
        }
        
        return $string;
    }
    
    public static function setHtmlFunctionsOnEmail($content)
    {
        $head = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                    <html><head><title>Subiect aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</title></head>
                    <body style="padding: 0pt; margin: 0pt; background:#e5e5e5; font-size:13px; line-height:19px;"><span id="preheader" style="display: none !important; font-size:0px; visibility: hidden; opacity: 0; color: #e5e5e5; height: 0px; width: 0px;">Portal contabilitate - portalul tau de contabilitate aplicata</span><center>"';
        $foot = '</center></body></html>';
        
        return $head.$content.$foot;
    }
    
    public static function getReadEmailHeaders()
    {
        include_once(ABSOLUTE_PATH."/library/WebApp/Mail/Oempro/classes/MobileDetect.php");
        
        $detect = new Mobile_Detect;
        
        $headers['os'] = $detect->getOS();
        if ($detect->isMobile())
        {
            $headers['device'] = 'Smartphone';
        }
        
        if ($detect->isTablet())
        {
            $headers['device'] = 'Tablet';
        }
            
        if (!$detect->isMobile() && !$detect->isTablet())
        {
            if (stripos($os, "mac") !== false)
            {
                $headers['device'] = "Mac";
            }
            else
            {
                $headers['device'] = "Pc";
            }
        }
        $headers['browser'] = $detect->getBrowser();
        $headers['header'] = $_SERVER['HTTP_USER_AGENT'];
        $headers['ip'] = getenv("REMOTE_ADDR");
        
        return $headers; 
    }
    
    public static function calculateAge($date)
    {
        $now = time();
        if ($date != "0000-00-00")
        {
            $time = strtotime($date);
            return intval(($now - $time)/86400/31/12);
        }
        
        return "-";
    }
    
    public static function ordinalSuffix($num) 
    {
        $suffixes = array("st", "nd", "rd");
        $lastDigit = $num % 10;
        if(($num < 20 && $num > 9) || $lastDigit == 0 || $lastDigit > 3) return "th";
        
        return $suffixes[$lastDigit - 1];
    }
    
    public static function createJobSlug($job)
    {
        $id = ($job['id'] != $job['id_job'] && $job['id_job'] > 0) ? $job['id_job'] : $job['id'];
        return APPLICATION_URL."/jobs/".$id."/".$job['slug']."/".$job['company_slug'];
    }
    
    public static function createCampaignSlug($id_campaign,$campaignTitle) {
            $model = new models_Campaign ;
            $companySlug = $model->getCompanySlugByCampaignId($id_campaign);
        return "campaign/". (int) $id_campaign."/".  str_replace(" ", "-", $campaignTitle)."/".$companySlug['slug'];
    }
    
    public static function showNiceTime($time)
    {
        if ($time >= 0)
        {
            $tokens = array (31536000 => 'years', 2592000 => 'months',604800 => 'weeks', 86400 => 'days', 3600 => 'hours', 60 => 'minutes', 1 => 'second');
            foreach ($tokens as $unit => $text) 
            {
                if ($time < $unit) continue;
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
            }    
        }
        else
        {
            return "0 minutes";
        }
    }
    
    public static function calculateTimeAgo($time)
    {
        $time = strtotime($time);
        $time = time() - $time;
        
        if ($time >= 0)
        {
            $tokens = array (31536000 => 'year', 2592000 => 'month',604800 => 'week', 86400 => 'day', 3600 => 'hour', 60 => 'minute', 1 => 'second');
            foreach ($tokens as $unit => $text) 
            {
                if ($time < $unit) continue;
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
            }    
        }
        else
        {
            return "1 minute";
        }
    }
    
    public static function getSeoTitles($page_type, $object_type, $personalizeOptions = array())
    {
        $modelContent = new models_Content;
        $content = $modelContent->getSeoTitlesFront($page_type, $object_type);
        if ($object_type == 2)
        {
            $content = WebApp_Tools::strTrim($content, 150);
        }
        
        return $modelContent->_personalize($content, $personalizeOptions);
    }
    
    public static function getImageAlts($object_type, $personalizeOptions = array())
    {
        $modelContent = new models_Content;
        $content = $modelContent->getImageAltsFront($object_type);
        
        return $modelContent->_personalize($content, $personalizeOptions);
    }
    
    public static function getImageAlt($key, $params = array())
    {
        $modelContent = new models_Content;
        $content = $modelContent->getImageAltFront($key);
        
        switch ($key)
        {
            case "company_photo":
                return $modelContent->_personalize($content, $params);            
            break;
            
            case "news_photo":
                return $modelContent->_personalize($content, $params);            
            break;
            
            default:
                return $modelContent->getImageAltFront($key);
            break;
        }
    }
    
    public static function resizeImage($location, $width, $height)
    {
        $file = ABSOLUTE_PATH."/public/resources/files".$location;
        WebApp_UploadImage::saveConstrainedFixedImage($width, $height, $file);        
    }
    
    
    
    public static function createShareFacbook($url, $type, $params = array()) {
        
        $modelSocial = new models_Social;
        $model_job = new models_Jobs;
        $url = $modelSocial->createShortUrl($url)."&ts=".uniqid(time());
         switch ($type)
        {
              case "facebook":
                  $url = $url."&source=".$model_job->getSocilaMediaIdByType($type) ;
                return $url;
            break;
        }
    }
    
    
    public static function createSharingUrl($url, $type, $params = array())
    {
        $modelSocial = new models_Social;
        $model_job = new models_Jobs;
        $url = $modelSocial->createShortUrl($url)."&ts=".uniqid(time());
        
        switch ($type)
        {
            case "linkedin":
                $url = $url."&source=".$model_job->getSocilaMediaIdByType($type) ;
                return "https://www.linkedin.com/shareArticle?mini=true&url=".urlencode($url)."&title=".$params['title'];
            break;
            
            case "facebook":
//                  $url = $url."&source=".md5("facebook") ;
                return "https://www.facebook.com/sharer/sharer.php?&u={$url}&display=popup&ref=plugin";
            break;
            
            case "twitter":
                $url = $url."&source=".$model_job->getSocilaMediaIdByType($type) ;
//                if ($params['type'] == "job")
//                {
//                 return "http://www.twitter.com/share?url=".urlencode($url)."&text=Check out this great Job posted on Talentspotting: {$params['title']}";
//                }
//                else
//                {
                    return "http://www.twitter.com/share?url=".urlencode($url);   
//                }
            break;
            
            case "gplus":
                return "https://plus.google.com/share?url=".urlencode($url);
            break;
            
            default:
                return $url;
            break;
        }
    }
    
    public static function RandomImg(){
        $images = array('starts');
        shuffle($images);
        $path = "/resources/frontend/css/theme/images/".$images[0].".png";
        return $path;
    }
    
    public static function getSocialName($param,$sourceData) {
        array_walk_recursive($sourceData, function(&$value,&$key) {
        
            return $value =  md5($value);
        });
        if (false !== $key = array_search($param, $sourceData)) {
           return $key ;
        } else {
            return "website";
        }
    }
    
    
    public static function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

    public static function display($x, $length)
        {
          if(strlen($x)<=$length)
          {
            echo $x;
          }
          else
          {
            $y=substr($x,0,$length) . ' ...';
            echo $y;
          }
        }
        
        
        public static function Debug($log) {
            file_put_contents('php://stderr', print_r($log, TRUE)).PHP_EOL;
    }

    public static function get_time_difference_php($created_time)
    {
        date_default_timezone_set('Asia/Calcutta'); //Change as per your default time
        $str = strtotime($created_time);
        $today = strtotime(date('Y-m-d H:i:s'));

        // It returns the time difference in Seconds...
        $time_differnce = $today-$str;

        // To Calculate the time difference in Years...
        $years = 60*60*24*365;

        // To Calculate the time difference in Months...
        $months = 60*60*24*30;

        // To Calculate the time difference in Days...
        $days = 60*60*24;

        // To Calculate the time difference in Hours...
        $hours = 60*60;

        // To Calculate the time difference in Minutes...
        $minutes = 60;

        if(intval($time_differnce/$years) > 1)
        {
            return intval($time_differnce/$years)." years ago";
        }else if(intval($time_differnce/$years) > 0)
        {
            return intval($time_differnce/$years)." year ago";
        }else if(intval($time_differnce/$months) > 1)
        {
            return intval($time_differnce/$months)." months ago";
        }else if(intval(($time_differnce/$months)) > 0)
        {
            return intval(($time_differnce/$months))." month ago";
        }else if(intval(($time_differnce/$days)) > 1)
        {
            return intval(($time_differnce/$days))." days ago";
        }else if (intval(($time_differnce/$days)) > 0)
        {
            return intval(($time_differnce/$days))." day ago";
        }else if (intval(($time_differnce/$hours)) > 1)
        {
            return intval(($time_differnce/$hours))." hours ago";
        }else if (intval(($time_differnce/$hours)) > 0)
        {
            return intval(($time_differnce/$hours))." hour ago";
        }else if (intval(($time_differnce/$minutes)) > 1)
        {
            return intval(($time_differnce/$minutes))." minutes ago";
        }else if (intval(($time_differnce/$minutes)) > 0)
        {
            return intval(($time_differnce/$minutes))." minute ago";
        }else if (intval(($time_differnce)) > 1)
        {
            return intval(($time_differnce))." seconds ago";
        }else
        {
            return "few seconds ago";
        }
    }

    public static function getDaysBetweenDate($date_1,$date_2)
    {
        $date_1 = date_create(date('Y-m-d', strtotime($date_1)));
        $date_2 = date_create(date('Y-m-d', strtotime($date_2)));
        $diff = date_diff($date_1, $date_2);
        $days = (int) $diff->format("%a");
        return $days ;
    }

    public static function mapInputArray($array = array(),$flag = false)
    {
        if(empty($array)) return [];
        $constructed = [] ;
        foreach($array as $key => &$value)
        {
            if($flag)
            {
                foreach($value as $val)
                {
                    if(isset($val['name']) && $val['name'] === 'id') continue;
                    @$constructed[$key][$val['name']] = $val['value'];
                }
            } else
            {
                if($value['name'] == 'id') continue;
                $constructed[$value['name']] = $value['value'];
            }
        }
        return $constructed;
    }

    public static function collectValuesByKey($key,$array)
    {
        if(empty($array)) return;
        $data = [];
        foreach($array as $index => $value)
        {
            if(isset($value[$key]))
            {
                $data[] = $value[$key];
            }
        }
        return $data;
    }

    public static function mapMuliToAssoc($array)
    {
        $assoc = [];
        if(empty($array)) return;
        foreach($array as $arr)
        {
            foreach($arr as $val)
            {
                $assoc[] = $val;
            }
        }
        return $assoc;
    }

    public static function mapError(Zend_Form $form)
    {
        $form_inputs = $form->getErrors();
        $formMsg = $form->getMessages();
        $return = [] ;
        foreach($form_inputs as $index => $input)
        {
            if(!empty($input))
            {
                foreach ($input as $key => $value)
                {
                    $return[$index][] =$formMsg[$index][$value];
                }
            }
        }
        $errors = "";
        foreach($return as $key => $array)
        {
            $key = ucfirst($key);
            $str = "";
            $str .= " $key : ".implode($array,', '). " \n";
            $errors .= $str;
        }
        return $errors;
    }

    public static function getDefaultLanguage($domain = null)
    {
        $site = ($domain == null) ? $_SERVER["SERVER_NAME"] : $domain;

        $subdomain = explode('.', $site);
        if (in_array("www", $subdomain)) {
            array_shift($subdomain);
        }
        $subdomain = $subdomain[0];
        $db = Zend_Db_Table::getDefaultAdapter() ;
        $sql = $db->select()->from(['c' => 'companies'],[''])
            ->join(['l' => 'languages'],'l.id=c.default_language',['title'])
        ->where('c.slug=?' ,$subdomain);
        return $db->fetchRow($sql)['title'];
    }
    public function check_in_range($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        if (($user_ts >= $start_ts) && ($user_ts <= $end_ts)) return true;

        return false;
    }
}