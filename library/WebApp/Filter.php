<?php
/**
 * Class to help creating filter columns.
 */
class WebApp_Filter 
{

	//public $_actionController  = '';
	private $_filters = array();
	private $_data = array();
	private $_content = '';
	private $_url = '';
	private $viu;
	private $_filterStorage;
	
	/**
	 * Manage and store filters and filters data into it's own namespace
	 *
	 */
	public function __construct($filters = array(), $defaultData = array(), $url = null) {
		$this->_url = $url;
		if(empty($this->_url)) {
			$this->_url = '';
		}
		// setting elements
		$this->_filters = $filters;
		
		// setting data
		if(!empty($defaultData)) {
			# WARNING!!! The order of operands in the next row is important. Overwrite is possible if inverted
			$data = (array)Zend_Controller_Front::getInstance()->getRequest()->getParam('filter') + $defaultData;
		}
		else {
			$data = Zend_Controller_Front::getInstance()->getRequest()->getParam('filter');
		}
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		
		$this->setSort();
		
		$filterStorage = new Zend_Session_Namespace('filter_'.$module.'_'.$controller.'_'.$action);
		$this->_filterStorage = $filterStorage;
		if(count($data) > 0) {
			if(isset($data['all']) && $data['all'] === "") {
				$filterStorage->_data = array();
				$data = $defaultData;
			}
			
			foreach($data as $key => $value) {
				if($value === "" || "NULL" == strtoupper($value) || !array_key_exists($key, $this->_filters)) {
					unset($data[$key]);
					unset($filterStorage->_data[$key]);
				}
			}
		}
		
		if(!is_array($filterStorage->_data)) $filterStorage->_data = array();
		if(!is_array($data)) $data = array();
		$this->_data = array_merge($filterStorage->_data, $data);
		$filterStorage->_data = $this->_data;
	}
	
	/**
	 * Render filter form and active filters
	 *
	 * @return (string) $this->_content
	 */
	public function render() {
		$this->viu = new Zend_View();
		$this->viu->setScriptPath(APPLICATION_PATH . '/layouts/scripts/partials/admin/');
		
		$this->viu->filters = $this->_filters;
		
		if(!is_array($this->_filters) || count($this->_filters) <= 0) return '';
			$this->renderFilterForm();
		if(Zend_Controller_Front::getInstance()->getRequest()->getParam('noFilterList') != 1) {
			$this->renderFilterList();
		}
		
		return $this->viu->render('filter.phtml');
	}
	
	/**
	 * Render active filters
	 *
	 * @return (string) $content
	 */
	public function renderFilterList() {
		$this->viu->data = $this->_data;

		return true;
	}
	
	/**
	 * Render filter form
	 *
	 * @return (string) $content
	 */
	public function renderFilterForm() {
		$form = new Zend_Form();

		$form->removeDecorator('HtmlTag');
		$form->setMethod('get');

		$form->setAction($this->_url);
		$this->viu->url = $this->_url;
		$filtersubForm = new Zend_Form_SubForm();
		$filtersubForm->removeDecorator('HtmlTag');
		$filtersubForm->removeDecorator('Label');
		$filtersubForm->removeDecorator('DtDdWrapper');
		$filtersubForm->removeDecorator('fieldset');
		
		$filtersubForm->setElements($this->_filters);

		$form->addSubForm($filtersubForm, 'filter');
				
		$filtersubForm->setElementDecorators(array(
			array('ViewHelper', array()),
			array('Errors'),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'nbFilter_Input')),
			//array('Label', array('tag' => 'div', 'class' => 'nbFilter_Label')),
			//array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'nbFilter_Row')),
		));
		
		$submitImage = new Zend_Form_Element_Image('submit');
		$submitImage->class = "filtreaza";
		$submitImage->setImage('/resources/images/admin/filter.png');
		$form->addElement($submitImage);

		$form->setElementDecorators(array(
			array('ViewHelper'),
			array('Errors'),
			array('HtmlTag', array('tag' => 'div', 'class' => 'nbFilter_Submit')),
		));
		$form->populate(array('filter' => $this->_data));
		$form->removeDecorator('Fieldset');
		
		$formDisplay = $form->render();
		$width = 770;
		
		$this->viu->width = $width;
		$this->viu->filterform = $form;

       	return true;
	}
	
	/**
	 * Generate WHERE conditions to be passed to sql statement
	 *
	 * @return (string) $where
	 */
	public function getWhere() {
		$dbAdapter = Zend_Registry::get('dbAdapter');
		$where = array();
		if(!is_array($this->_filters)) return null;
		
		foreach($this->_filters as $key => $value) {
			# get both ways (associative array AND numeric array) up and running
			if($this->_filters[$key]['type'] && !$this->_filters[$key][0]) {
				$this->_filters[$key][0] = $this->_filters[$key]['type'];
			}
			if($this->_filters[$key]['options'] && !$this->_filters[$key][1]) {
				$this->_filters[$key][1] = $this->_filters[$key]['options'];
			} // END get both ways (associative array AND numeric array) up and running

			
			if (!isset($this->_filters[$key][1]['isHelper'])) {
				$this->_filters[$key][1]['isHelper'] = false;
			}
			if(isset($this->_data[$key]) && $this->_data[$key] !== "" && !$this->_filters[$key][1]['isHelper']) {
				$table = empty($this->_filters[$key][1]['table']) ? '' : '`'.$this->_filters[$key][1]['table'].'`.';
				$field = empty($this->_filters[$key][1]['field']) ? '`'.$key.'`' : '`'.$this->_filters[$key][1]['field'].'`';
				
				$filterData = $this->_data[$key];
				
				
				if (isset($this->_filters[$key]['js']) && $this->_filters[$key]['js'] == 'timestamp') {
					$where[] = $dbAdapter->quoteInto('(DATE_FORMAT('.$table.'`'.$this->_filters[$key]['real_field'].'`, \'%Y-%m-%d\')'.' '.$this->_filters[$key]['semn'].' ?)', ''.$this->_data[$key].'');
				} else if (isset($this->_filters[$key]['semn'])) {
   
                    if ($this->_filters[$key]['js'] == 'date')
                    {
                        if ($this->_filters[$key]['semn'] == '<=')
                        {
                            $where[] = $dbAdapter->quoteInto('('.$table.'`'.$this->_filters[$key]['real_field'].'`'.' '.$this->_filters[$key]['semn'].' ?)', ''.$this->_data[$key]." 23:59:59".'');        
                        }
                        else                        
                        {
                            $where[] = $dbAdapter->quoteInto('('.$table.'`'.$this->_filters[$key]['real_field'].'`'.' '.$this->_filters[$key]['semn'].' ?)', ''.$this->_data[$key].'');
                        }
                    }
					else
                    {
                        $where[] = $dbAdapter->quoteInto('('.$table.'`'.$this->_filters[$key]['real_field'].'`'.' '.$this->_filters[$key]['semn'].' ?)', ''.$this->_data[$key].'');
                    }
                                        
				} else {
					
//					if(!empty($this->_filters[$key][1]['operator'])) {
//						if(strpos($this->_filters[$key][1]['operator'], '?')) {
//							$operator = $this->_filters[$key][1]['operator'];
//							$filterData = addcslashes($filterData, "\000\n\r\\'\"\032");
//							$where[] = str_replace('?', $filterData, '('.$table.$field.$operator.')');
//							continue;
//						}
//						$operator = ' '.$this->_filters[$key][1]['operator'].' ? ';
//					} elseif($this->_filters[$key][0] == 'text') {
//						$operator = ' LIKE ? ';
//						$filterData = '%'.$this->_data[$key].'%';
//					} else {
//						if ($key == 'categorie' && $filterData == '999')
//                        {
//                            $operator = ' != ? ';
//                            $filterData = '6';
//                        }
//                        else
//                        {
//                            $operator = ' = ? ';
//                        }
//
//					}
					
					if(!empty($this->_filters[$key][1]['operator'])) {
						
						if(
//							strpos($this->_filters[$key][1]['operator'], '?') &&
							(
								strpos($this->_filters[$key][1]['operator'], '%field%') ||
								strpos($this->_filters[$key][1]['operator'], '%table%')
							)
						) {
							$operator = $this->_filters[$key][1]['operator'];
							$operator = str_replace('%field%', '`'.$this->_filters[$key][1]['field'].'`', $operator);
							if(false !== strpos($this->_filters[$key][1]['operator'], '%table%')) {
								$operator = str_replace('%table%', '`'.$this->_filters[$key][1]['table'].'`', $operator);
							}
							
							$filterData = addcslashes($filterData, "\000\n\r\\'\"\032");
							$where[] = str_replace('?', $filterData, '('.$operator.')');
							continue;
						}
						
						if(strpos($this->_filters[$key][1]['operator'], '?')) {
							$operator = $this->_filters[$key][1]['operator'];
							$filterData = addcslashes($filterData, "\000\n\r\\'\"\032");
							$where[] = str_replace('?', $filterData, '('.$table.$field.$operator.')');
							continue;
						}
						
						$operator = ' '.$this->_filters[$key][1]['operator'].' ? ';
					} elseif($value[0] == 'text') {
						$operator = ' LIKE ? ';
						$filterData = '%'.$this->_data[$key].'%';
					} else {
						if($key == 'categorie' && $filterData == '999') {
							$operator = ' != ? ';
							$filterData = '6';
						}
						else {
							$operator = ' = ? ';
						}
					}
					
				
					$where[] = $dbAdapter->quoteInto('('.$table.$field.$operator.')', $filterData);
				}
			}
		}

		return join(' AND ', $where);
	}

	/**
	 * Set WHERE conditions to a given SQL statement
	 *
	 * @return (Zend_Db_Select) $select
	 */
	public function applyTo(Zend_Db_Select $select) {
		$safeSql = clone($select);

		$where = $this->getWhere();
		if(!empty($where)) $select->where($where);
		
		try {
			Zend_Registry::get('dbAdapter')->query($select->limit(1));
			return $select->reset('limitcount');
		} catch(Zend_Exception $e) {
			Zend_Controller_Action_HelperBroker::getHelper('FlashMessenger')->addMessage('We are sorry but for the moment the filter is broken. You can still use this listing but without the use of the filter from. <a href="http://proiecte.dev/framework/newticket">(report this bug)</a>');
			return $safeSql;
		}
	}
	
	
	public function setSort() {
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		$filterStorage = new Zend_Session_Namespace('filter_'.$module.'_'.$controller.'_'.$action);
		
		$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		
		$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		if (isset($params['orderBy']) && strlen($params['orderBy'])>1 && isset($params['order']) && strlen($params['orderBy'])>1) {
			$orderBy = Zend_Controller_Front::getInstance()->getRequest()->getParam('orderBy');
			$order = Zend_Controller_Front::getInstance()->getRequest()->getParam('order');
		} else if (isset($filterStorage->_order) && is_array($filterStorage->_order) && count($filterStorage->_order)==2) {
			$orderBy = $filterStorage->_order['orderBy'];
			$order = $filterStorage->_order['order'];
		}
		if (isset($orderBy) && isset($order)) {
			$filterStorage->_order = array('orderBy'=>$orderBy, 'order'=>$order);
		}
	}
	
	/**
	 * Render sort
	 */
	public function renderSort($text, $fieldName, $tableName = null) {
		$content = '';
		
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		$filterStorage = new Zend_Session_Namespace('filter_'.$module.'_'.$controller.'_'.$action);
		
		$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		if (isset($params['orderBy']) && strlen($params['orderBy'])>1 && isset($params['order']) && strlen($params['orderBy'])>1) {
			$orderBy = Zend_Controller_Front::getInstance()->getRequest()->getParam('orderBy');
			$order = Zend_Controller_Front::getInstance()->getRequest()->getParam('order');
		} else if (isset($filterStorage->_order) && is_array($filterStorage->_order) && count($filterStorage->_order)==2) {
			$orderBy = $filterStorage->_order['orderBy'];
			$order = $filterStorage->_order['order'];
		} else {
			$order = '';
			$orderBy = '';
		}
		

		
		if(!empty($tableName)) {
			$fieldName = $tableName.'.'.$fieldName;
		}
		
		if($orderBy == $fieldName) {
			if($order == 'ASC') {
				$arraow = '&darr;';
				$orderNew = 'DESC';
			} else if($order == 'DESC') {
				$arraow = '&uarr;';
				$orderNew = 'ASC';
			} else if($order == '') {
				$arraow = '';
				$orderNew = 'ASC';
			}
		} else {
			$order = '';
			$arraow = '';
			$orderNew = 'ASC';
		}
		
		$url = $this->_url .'/orderBy/'.$fieldName.'/order/'.$orderNew."/perpage/".(isset($params['perpage']) ? $params['perpage'] : 25);
		
		$content .= '<a href="'.$url.'">'.$text.'</a>';
		$content .= '&nbsp;'.$arraow;
		return $content;
	}
	
	public static function getSort() {
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		$filterStorage = new Zend_Session_Namespace('filter_'.$module.'_'.$controller.'_'.$action);
		
		$order = array();
		$filterStorage = new Zend_Session_Namespace('filter_'.$module.'_'.$controller.'_'.$action);
		if (is_array($filterStorage->_order) && count($filterStorage->_order)>0) {
			$order = $filterStorage->_order;
			if (!is_null($order['orderBy']) && !is_null($order['order'])) {
				return $order;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
		
	}
	
	
    /**
     * Serialize as string
     *
     * Proxies to {@link render()}.
     *
     * @return string
     */
    public function __toString() {
        try {
            $return = $this->render();
            return $return;
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
        
        return '';
    }
	
	public function __get($field)
	{
		return $this->_data[$field];
	}
	
	public function __set($field, $value)
	{
		if (!isset($value)){
			unset($this->_data[$field]);
		}
		else{
			$this->_data[$field] = $value;
		}

		$this->_filterStorage->_data = $this->_data;
		return $this;
	}
	
}