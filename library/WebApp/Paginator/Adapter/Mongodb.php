<?php

/**
 * @author LethaK Maas (modified)
 * @link http://pastebin.com/CZxxATTW
 * @see Zend_Paginator_Adapter_Interface
 */
class WebApp_Paginator_Adapter_Mongodb implements Zend_Paginator_Adapter_Interface {

    protected $_cursor = null;

    public function __construct (MongoCursor $cursor) {
        $this->_cursor = $cursor;
    }

    /**
     * Returns the total number of rows in the cursor
     *
     * @return (integer)
     */
    public function count () {
        return $this->_cursor->count();
    }

    /**
     * Returns a cursor limited to items for the page
     *
     * @param (integer) $offset - page offset
     * @param (integer) $itemCountPerPage - number of items per page
     * @return (MongoCursor)
     */
    public function getItems ($offset, $itemCountPerPage) {
        $cursor = $this->_cursor->skip($offset)->limit($itemCountPerPage);
        return $cursor;
    }

}