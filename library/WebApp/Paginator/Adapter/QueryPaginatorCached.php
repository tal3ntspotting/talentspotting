<?php

class WebApp_Paginator_Adapter_QueryPaginatorCached implements Zend_Paginator_Adapter_Interface
{
    private $_query;
    private $_adapter;

    private $_queryExecuted = null;
    private $_count = null;


    public function __construct($query, $adapter = null)
    {
        $this->_query = $query;
        
        if ( !is_null($adapter) )
        {
        	$this->_adapter = $adapter;
        	$this->_bbb = true;
        } else {
        	$this->_adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        }
    }

    protected function getQuery($offset, $itemCountPerPage)
    {
        $initialQuery = $this->_query;

        // replace existing LIMIT in query
        $hasLimit = preg_match("/LIMIT\s+[0-9\s]+(,[0-9\s]+)?;?$/i", $initialQuery, $matches);
        if (!$hasLimit) {
            // clean up end of query
            $query = preg_replace("/\s*;$/", '', $initialQuery);
        } else {
            // remove existing LIMIT
            $query = str_replace($matches[0], '', $initialQuery);
        }

        // add limit to query
        $query = $query. " LIMIT $offset, $itemCountPerPage;";
        return $query;
    }

    protected function getCountQuery()
    {
        $initialQuery = $this->_query;

        // remove existing LIMIT in query
        $hasLimit = preg_match("/LIMIT\s+[0-9\s]+(,[0-9\s]+)?;?$/i", $initialQuery, $matches);
        if (!$hasLimit) {
            // clean up end of query
            $query = preg_replace("/\s*;$/", '', $initialQuery);
        } else {
            // remove existing LIMIT
            $query = str_replace($matches[0], '', $initialQuery);
        }

        // remove ORDER BY fields
        $hasOrderBy = preg_match("/ORDER BY[\w\.,`'_-\s]+$/i", $query, $matches);
        if ($hasOrderBy) {
            $query = str_replace($matches[0], '', $query);
        }

        // remove FORCE INDEX fields
        $hasForceIndex = preg_match("/FORCE INDEX\([\w_\s,]+\)/i", $query, $matches);
        if ($hasForceIndex) {
            $query = str_replace($matches[0], '', $query);
        }

        // replace existing SELECT fields with COUNT(*)
        $hasSelect = preg_match("/SELECT([\w\.,`'\"\%\/_-\s\*)(><=\:]+)FROM/i", $query, $matches);

        if ($hasSelect) {
            $query = str_replace($matches[1], ' COUNT(*) as cnt ', $query);
        } else {
            // if no fields are selected there is an error in the query
            return "SELECT '0' as cnt;";
        }

        return $query;
    }

    public function getItems($offset, $itemCountPerPage)
    {
        $query = $this->getQuery($offset, $itemCountPerPage);

		// Get current instantiation data
		$request = Zend_Controller_Front::getInstance()->getRequest();
		if ($request) {
			$action = $request->getActionName();
			$controller = $request->getControllerName();
			$module = $request->getModuleName();
			$uri = APPLICATION_URL. $request->getRequestUri();
		} else {
			$action = '-';
			$controller = '-';
			$module = '-';
			$uri = 'http:///';
		}

		$q = $this->cleanQuery($query);
//        Zend_Registry::get('Zend_Log')->log("Data query($module/$controller/$action):\nURL: $uri\n-----------------------------------------\n$q\n-----------------------------------------\n", Zend_Log::DEBUG);

        // run query and get results
        $cache = Zend_Registry::get("Zend_Cache_Core");
        $resources = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $params = $request->getParams();
        
        $pagina = (!empty($params) && array_key_exists("pagina", $params)) ? $params['pagina'] : 1;
        
        if ( $resources['resources']['Cache']['queryCacheEnabled'] == 1 && $pagina <= 20)
        {
        	if (!$result = $cache->load("listing_sh_".md5($this->_query)."_".$offset."_".$itemCountPerPage."_".$this->_count))
            {
                $result = $this->_adapter->query($query)->fetchAll();
                $cache->save($result, "listing_sh_".md5($this->_query)."_".$offset."_".$itemCountPerPage."_".$this->_count);
            }
        }
        else
        {
            $result = $this->_adapter->query($query)->fetchAll();
        }
        
        return $result;
    }
    
	private function cleanQuery($q)
	{
		return preg_replace("/[\t\s]+/", " ", $q);
	}
	
    public function count()
    {
        if (null === $this->_count) {
        	
            $query = $this->getCountQuery();


			// Get current instantiation data
//			$request = Zend_Controller_Front::getInstance()->getRequest();
//			if ($request) {
//				$action = $request->getActionName();
//				$controller = $request->getControllerName();
//				$module = $request->getModuleName();
//				$uri = APPLICATION_URL. $request->getRequestUri();
//			} else {
//				$action = '-';
//				$controller = '-';
//				$module = '-';
//				$uri = 'http:///';
//			}
			
			$q = $this->cleanQuery($query);
//			Zend_Registry::get('Zend_Log')->log("Paginator query($module/$controller/$action):\nURL: $uri\n-----------------------------------------\n{$q}\n-----------------------------------------\n", Zend_Log::DEBUG);
			//BEFORE
            //$sql = $this->_adapter->query($query)->fetch();
            //AFTER : daca exista o clauza 'GROUP BY' in query, rezultatul intors este gresit
            $sql = $this->_adapter->fetchAll($query);
//			Zend_Debug::dump($sql);
//			Zend_Debug::dump($query);
			//END

            $this->_count = count($sql) > 1 ? count($sql) : (is_array($sql[0]) ? $sql[0]['cnt'] : $sql[0]->cnt);
        }

        return $this->_count;
    }
    
    public function setStaticCount($cnt) {
    	$this->_count = $cnt;
    }

}
