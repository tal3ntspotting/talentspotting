<?php

class WebApp_signin {


    public $_data;
    public $_session;
    static $_instance;
    static protected $events = array();
    const CHECK = "check_inputs";
    const KENNEXA = "call";

    static public function register($callable, $event) {
        if (!isset(self::$events[$event])) {
            self::$events[$event] = array();
        }
        self::$events[$event][] = $callable;
    }

    static public function notify($event, $params = array()) {
        if (isset(self::$events[$event])) {
            foreach (self::$events[$event] AS $callable) {
                @call_user_func_array(array(self,$callable), $params);
            }
        }
    }

    public function __construct($data = []) {
        if (!$this->_session instanceof Zend_Session_Namespace) {
            $this->_session = new Zend_Session_Namespace('flow');
        }
        
        if(!empty($data)) $this->_data = $data;
        self::register('count', self::CHECK);
        self::register('kennexa', self::KENNEXA);
    }
    
    public static function getInstance() {
        if (!self::$_instance instanceof WebApp_signin) {
            self::$_instance = new self ;
        }
        return self::$_instance;
    }

    public function registerStep($step) {
//        echo '<pre>';
//        print_r($this->_session->_POST);
        $this->_session->_STEPS[] = $step;
        $this->_session->_POST[$step] = $this->_data;
        WebApp_signin::notify(self::CHECK,array(WebApp_signin::getInstance()));
    }

    public function Debug() {
//        echo '<pre>';
//        print_r('==== DEBUG ====');
//        print_r($this->_session->_POST);
//        print_r($this);
//        die;
    }
    
    public function count($session) {
        $modelUser = new models_User;
        $input = $session->_session->_POST;
        $steps = $session->_session->STEPS;
        if(count($input) === 3 ) {
            $model_user = new models_User;
            $id = $model_user->saveNewFlowSignIn($input);
            $model_user->updateFlowSign($id,$session->_session->_LINKEDIN);
            $model_user->uploadCvById($id);
            
            
            $user = $modelUser->getUserById($id);
            
            $recommender_session = new Zend_Session_Namespace('recommender');
               if($recommender_session->recommender) {
                   $modelUser->update(['type' => 1], 'id='.$user['id'],'users');
                }
            $modelUser->checkIfUserFromComapnyDomain($user);
             
            
            $kennex = new kennexa_api($user,  kennexa_api::create);
            $kennex->createUser();
            
           
            Zend_Session::namespaceUnset('flow');
            Zend_Session::destroy('flow');
            return $id ;
        } 
    }
    
    public function setLinkedin($value) {
        $this->_session->_LINKEDIN = $value;
    }
    
    public function getLinkedin() {
        return $this->_session->_LINKEDIN ;
    }
    
    public function kennexa($id_user)
    {
        $modelUser = new models_User;
        
//        if($id_user > 0) 
//        {
            $user = $modelUser->getUserById($id_user);
            $kennexa = new kennexa_api();
            echo '<pre>';
            print_r($id_user);
            print_r($user);
            die;
            
//        }
    }


    
    
    
    

}

?>
