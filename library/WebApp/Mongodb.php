<?php
class WebApp_Mongodb extends Zend_Db_Adapter_Mongodb
{
    protected $_db;

    public function __construct()
    {
        $this->_db = Zend_Registry::get('dbMongoAdapter');
    }

    /**
     * @param (array) $sort - http://www.php.net/manual/en/mongocursor.sort.php
     */
    public function fetchAll($collection, $query = array(), $fields = array(), $sort = array(), $limit = 0, $return_cursor = false)
    {
        $result = $this->_db->collection($collection)->find($query, $fields)->limit($limit)->sort($sort);

        if ($return_cursor) {
            return $result;
        }

        return iterator_to_array($result);
    }

    public function fetchRow($collection, $query = array(), $fields = array())
    {
        return $this->_db->collection($collection)->findOne($query, $fields);
    }

    public function insert($collection, array $data)
    {
        $this->_db->collection($collection)->insert($data);

        return (string) $data['_id'];
    }

    public function batchInsert($collection, array $data)
    {
        $this->_db->collection($collection)->batchInsert($data);

        return $data;
    }

    public function update($collection, array $data, $where = array())
    {
        $current_data = $this->fetchRow($collection, $where);
        if (!empty($current_data))
        {
            foreach ($current_data as $field => $value)
            {
                if (!array_key_exists($field, $data) && $field != "_id")
                {
                    $data[$field] = $value;
                }
            }
        }
        $data = $this->_db->collection($collection)->update($where, $data);
    }
    /*
    public function lastInsertId()
    {

    }
    */

    public function count($collection, $query)
    {
        return $this->_db->collection($collection)->count($query);
    }

    public function delete($collection, $query = array())
    {
        return $this->_db->collection($collection)->remove($query);
    }
}