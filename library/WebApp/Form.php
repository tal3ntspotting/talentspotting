<?php
class WebApp_Form extends Zend_Form
{
    public $translate ;
    public function __construct($options = array(), $defaults = array())
    {
        $resources = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        if (!empty($resources['resources']['View']['form']))
        {
            foreach ((array) $resources['resources']['View']['form'] as $key => $value)
            {
                if (!array_key_exists($key, (array) $options))
                {
                    $options[$key] = $value;
                }
            }
        }

        $this->setOptions($options);

        $this->translate = Zend_Registry::get("Zend_Translate");
		parent::__construct($options);
   }

    /**
     * @param null|Zend_Form|Zend_Form_Element $translate
     */
    public function setTranslate($translate)
    {
        $this->translate = $translate;
    }
}