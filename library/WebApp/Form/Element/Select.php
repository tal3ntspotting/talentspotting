<?php

class WebApp_Form_Element_Select extends Zend_Form_Element_Select
{
    public function __construct($field)
    {
        parent::__construct($field);
        $this->removeDecorator('HtmlTag')->removeDecorator('Label')->removeDecorator('Errors');
        $this->addFilter('StringTrim');
    }
}
