<?php

class WebApp_Form_Element_File extends Zend_Form_Element_File
{
    public function __construct($field)
    {
        parent::__construct($field);
        $this->removeDecorator('HtmlTag')->removeDecorator('Label')->removeDecorator('Errors');
        $this->addFilter('StringTrim');
    }
}
