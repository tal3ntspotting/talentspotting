<?php

/** Zend_Form_Element_Xhtml */
require_once 'Zend/Form/Element/Xhtml.php';

/**
 * HTML 5 specific form elements
 */
class WebApp_Form_Element_Html5 extends WebApp_Form_Element_Text {

	public $helper = 'formElementHtml5';

}