<?php

class WebApp_Form_Element_Radio extends Zend_Form_Element_Radio
{
    public function __construct($field)
    {
        parent::__construct($field);
        $this->removeDecorator('HtmlTag')->removeDecorator('Label')->removeDecorator('Errors');
        $this->addFilter('StringTrim');
    }
}
