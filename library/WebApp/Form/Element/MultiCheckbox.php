<?php
/** Zend_Form_Element_Multi */
// require_once 'Zend/Form/Element/Multi.php';

/**
 * MultiCheckbox form element
 */
class WebApp_Form_Element_MultiCheckbox extends Zend_Form_Element_MultiCheckbox
{
	/**
	 * Use formMultiCheckbox view helper by default
	 * @var string
	 */
	// public $helper = 'formMultiCheckbox';

	/**
	 * MultiCheckbox is an array of values by default
	 * @var bool
	 */
	// protected $_isArray = true;

	public function __construct($field)
	{
		parent::__construct($field);
		$this->removeDecorator('HtmlTag')->removeDecorator('Label')->removeDecorator('Errors');
		$this->addFilter('StringTrim');
	}

	/**
	 * @param int $elementKey
	 */
	// public function option($elementKey)
	// {
	// 	if ( !isset($this->options[$elementKey]) )
	// 		throw new Zend_Form_Element_Exception('Bad option for a MultiCheckbox');

	// 	$this->helper = 'formCheckbox';

	// 	$auxOptions = $this->options;
	// 	$auxValues = $this->_value;

	// 	if ( is_array($this->_value) && in_array($elementKey, $this->_value) ) {
	// 		$this->_value = $elementKey;
	// 	}

	// 	$this->options = array($elementKey);

	// 	$htmlString = (string) $this;

	// 	$this->options = $auxOptions;
	// 	$this->_value = $auxValues;

	// 	return $htmlString;
	// }
}
