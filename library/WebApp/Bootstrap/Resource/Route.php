<?php

class WebApp_Bootstrap_Resource_Route extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $options = $this->getOptions();
        
        $host = substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], "."));
        
        if (array_key_exists('configFile', $options)) 
        {
            $config = new Zend_Config_Ini($options['configFile'], 'routes');
            $router = new Zend_Controller_Router_Rewrite();
            $router->addConfig($config, 'routes');
        }
        if (array_key_exists('removeDefaultRoute', $options) && true == $options['removeDefaultRoute']) {
            $router->removeDefaultRoutes();
        }
        
        $this->getBootstrap()->getResource('frontController')->setRouter($router);        
    }

}