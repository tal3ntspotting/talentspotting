<?php
class WebApp_Bootstrap_Resource_Cache extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $options = $this->getOptions();

        $cache = Zend_Cache::factory(
            $options['frontend'],
            $options['backend'],
            $options['frontendOptions'],
            $options['backendOptions']
        );

        Zend_Registry::set('Zend_Cache_Core', $cache);
        Zend_Registry::set('queryCacheEnabled', $options['queryCacheEnabled']);

        return $cache;
    }
}