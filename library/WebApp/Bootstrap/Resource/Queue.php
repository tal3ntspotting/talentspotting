<?php

class WebApp_Bootstrap_Resource_Queue extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $options = $this->getOptions();
        
        $queue = null;
        
        if ($options['queryQueueEnabled'] == 1)
        {
            $queue = new Zend_Queue('MemcacheQ', $options['options']);                        
        }
        
        Zend_Registry::set('Zend_Queue', $queue);
    }

}