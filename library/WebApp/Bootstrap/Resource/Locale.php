<?php

class Webapp_Bootstrap_Resource_Locale extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $options = $this->getOptions();
        $namespace = new Zend_Session_Namespace('Language');
        $options['default'] = WebApp_Tools::getDefaultLanguage() ? WebApp_Tools::getDefaultLanguage() : $options['default'];
        $lang = (isset($namespace->lang)) ? $namespace->lang : $options['default'];
        $host = $_SERVER['HTTP_HOST'];

        $db = Zend_registry::get("dbMysqlAdapter"); 
//        $sql = $db->select()->from(array("s" => "subdomains"));
//        $subdomains = $db->fetchAll($sql);
//        
//        foreach ((array) $subdomains as $subdomain)
//        {
//            if ($host == $subdomain['title'])
//            {
//                $lang = $options['languages'][$subdomain['id_lang']];
//                $change = new Zend_Session_Namespace('language_change');
//                if (isset($change->lang) && $change->lang != "")
//                {
//                    if ($change->lang == $lang)
//                    {
//                        $namespace->lang = $lang;   
//                    }
//                    else
//                    {
//                        $namespace->lang = $change->lang;
//                    }
//                }                
//                else
//                {
//                    $namespace->lang = $lang;
//                }
//                
//                Zend_Registry::set('subdomain', $subdomain);
//            }
//        }
        $options['default'] = $lang;
        defined('APPLICATION_LOCALE') || define('APPLICATION_LOCALE', $options['default']);
        $locale = new Zend_Locale($options['default']);
        Zend_Registry::set('Zend_Locale', $locale);
    }
}