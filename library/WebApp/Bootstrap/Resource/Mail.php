<?php

class WebApp_Bootstrap_Resource_Mail extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $options = $this->getOptions();

        $default = false;
        if (array_key_exists('default', $options)) 
        {
            $default = $options['default'];
        }

        if (array_key_exists('sendmail', $options)) 
        {
            $mailTransport = new Zend_Mail_Transport_Sendmail();

            if (!strcmp('sendmail', $options['default'])) 
            {
                Zend_Mail::setDefaultTransport($mailTransport);
            }

            Zend_Registry::set('Zend_Mail_Transport_Sendmail', $mailTransport);
        }
    }

}