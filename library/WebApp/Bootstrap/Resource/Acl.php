<?php
/**
 * Resource for initializing the Acl
 */
class WebApp_Bootstrap_Resource_Acl extends Zend_Application_Resource_ResourceAbstract
{

    const DEFAULT_REGISTRY_KEY = 'Acl_Roles';
    const DEFAULT_ROLE_ID = 2;
    
    protected $_acl;
    protected $_roles = array();
    protected $_rights = array();
    protected $_enabled = false;
  
    public function init()
    {
        $options = $this->getOptions();
        
        $config = new Zend_Config_Ini($options['configFile']);
        $data = $config->toArray();
        
        foreach ($data as $key => $value)
        {
            if (strpos($key, "blocked_pages") !== false)
            {
                $permissions["blocked_pages"][substr($key, strrpos($key, ".")+1)] = $value;
            }
            else
            {
                $permissions[$key] = $value;
            }
        }
        
        Zend_Registry::set('APP_PERMISIONS', $permissions);
    }
}