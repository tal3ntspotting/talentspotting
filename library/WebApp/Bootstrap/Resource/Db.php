<?php
class WebApp_Bootstrap_Resource_Db extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $options = $this->getOptions();

        /////////////////////////////////////////////////////////////////
        //$REFERRER = $_SERVER['HTTP_REFERER'];
        //$domain = substr($REFERRER, strpos($REFERRER, '://')+3);
        //$domain = substr($domain, 0, strpos($domain, '/'));
        //$subdomain = substr($domain, 0, strpos($domain, '.'));
        //$options['params']['dbname'] = $options['clients'][$subdomain];
        //////////////////////////////////////////////////////////////////


        $dbAdapter = Zend_Db::factory($options['adapter'], $options['params']);
        Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
        Zend_Db_Table_Abstract::setDefaultMetadataCache(Zend_Registry::get('Zend_Cache_Core'));
        
        Zend_Registry::set('dbMysqlAdapter', $dbAdapter);        
    }
}