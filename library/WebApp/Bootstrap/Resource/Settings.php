<?php

class WebApp_Bootstrap_Resource_Settings extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
    	$options = $this->getOptions();
        Zend_Registry::set('_LANG_DETAILS', array_flip($options['languages']));
    }
}