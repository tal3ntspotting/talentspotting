<?php

class Webapp_Bootstrap_Resource_Translate extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $localeKey = Zend_Registry::get('Zend_Locale')->toString();

        $options = $this->getOptions();

        if (array_key_exists('log', $options))
        {
            if (Zend_Registry::isRegistered($options['log']))
            {
                $options['log'] = Zend_Registry::get($options['log']);
            }
            else
            {
                unset($options['log']);
            }
        }

        $class_options = array('scan' => Zend_Translate::LOCALE_FILENAME, 'locale' => $localeKey);
        $array = $class_options + $options;

        $translate = new Zend_Translate($array);

        $translate->setLocale(Zend_Registry::get('Zend_Locale'));
        Zend_Registry::set('Zend_Translate', $translate);
   }

}