<?php

class WebApp_Bootstrap_Resource_Form extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $options = $this->getOptions();

        $form = new Zend_Form();
        $form->setOptions($options);
    }

}