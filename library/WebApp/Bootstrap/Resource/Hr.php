<?php 

class WebApp_Bootstrap_Resource_Hr extends Zend_Application_Resource_ResourceAbstract {

    public function init() {
        $options = $this->getOptions();
        $config = new Zend_Config_Ini($options['configFile']);
        $Access = $config->toArray();

        Zend_Registry::set('Hr', $Access);
    }

}

?>
