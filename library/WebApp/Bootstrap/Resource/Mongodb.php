<?php
class WebApp_Bootstrap_Resource_Mongodb extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        return true;
        $options = $this->getOptions();
        $dbAdapter = Zend_Db::factory($options['adapter'], $options['params']);

        Zend_Registry::set('dbMongoAdapter', $dbAdapter);
    }
}