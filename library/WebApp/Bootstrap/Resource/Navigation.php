<?php

class WebApp_Bootstrap_Resource_Navigation extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
        $options = $this->getOptions();

        if (array_key_exists('configFile', $options)) {
            $config = new Zend_Config_Ini($options['configFile'], 'navigation');
            $navi = new Zend_Navigation($config);
            
            Zend_Registry::set('Zend_Navigation', new Zend_Navigation($config));
        }
    }

}