<?php

class WebApp_Bootstrap_Resource_Mobile extends Zend_Application_Resource_ResourceAbstract {

    public function init() {
        $options = $this->getOptions();
        $config = new Zend_Config_Ini($options['configFile']);
        $mobile_page = $config->toArray();

        Zend_Registry::set('Mobile', $mobile_page);
    }

}
