<?php

class WebApp_Bootstrap_Resource_View extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $options = $this->getOptions();

        $view = new Zend_View();

        $view->translate = Zend_Registry::get('Zend_Translate');

        if (array_key_exists('doctype', $options)) {
            $view->doctype($options['doctype']);
        }

        $view->headTitle()->setSeparator(' - ')
                          ->setIndent(4);


        if (array_key_exists('httpEquiv', $options)) {
            $view->headMeta()->setIndent(4);
            foreach ($options['httpEquiv'] as $type => $value) {
                $view->headMeta()->appendHttpEquiv($type, $value);
            }
        }

	if (!array_key_exists('minifyVersion', $options)) {
	    $options['minifyVersion'] = 0;
	}

        $view->headScript()->setIndent(4);
        if (array_key_exists('scripts', $options)) {
    	    if (false == $options['minify']) {
        		foreach ($options['scripts'] as $script) {
        		    $view->headScript()->appendFile($script);
        		}
    	    } else {
    		  $view->headScript()->appendFile(APPLICATION_URL. "/resources/m.php/scripts_{$options['minifyVersion']}.js");
    	    }
        }

        $view->headLink()->setIndent(4);
        $stylesKey = 'styles';

        // process correct styles request depending on $_SERVER['SERVER_NAME']
        if (array_key_exists($stylesKey, $options)) {
            if (false == $options['minify']) {
                foreach ($options[$stylesKey] as $style) {
                    $view->headLink()->appendStylesheet($style);
                }
            } else {
                $view->headLink()->appendStylesheet(APPLICATION_URL. "/resources/m.php/{$stylesKey}_{$options['minifyVersion']}.css");
            }
        }


        if (array_key_exists('helperPaths', $options)) {
            foreach ($options['helperPaths'] as $prefix => $path) {
                $view->addHelperPath($path, $prefix);
            }
        }

        if (array_key_exists('scriptPaths', $options)) {
            foreach ($options['scriptPaths'] as $path) {
                $view->addScriptPath($path);
            }
        }

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setView($view);

        return $view;
    }

}