<?php
class WebApp_Bootstrap_Resource_Log extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        // Load options from configuration file
        $options = $this->getOptions();

        $logger = new Zend_Log();
        if (array_key_exists('writers', $options)) {

            // log pe zile
            if (isset($options['writers']['Stream'])) {
                $options['writers']['Stream']  = str_replace('.txt', '_'.date('Y_m_d').'.txt', $options['writers']['Stream']);
            }

            foreach ($options['writers'] as $writerString => $start) {
                $className = "Zend_Log_Writer_$writerString";

                if (is_subclass_of($className, 'Zend_Log_Writer_Abstract') && false != $start) {

                    $writer = new $className($start);

                    if ( $writerString == 'Stream' ) {
                        // Formatter
                        $format = '%timestamp%::%priorityName% : %message%' . PHP_EOL;
                        $formatter = new Zend_Log_Formatter_Simple($format);

                        $writer->setFormatter($formatter);
                    }
                    $logger->addWriter($writer);
                }
            }
        } else {
            $logger->addWriter(new Zend_Log_Writer_Null());
        }

        Zend_Registry::set('Zend_Log', $logger);
    }
}