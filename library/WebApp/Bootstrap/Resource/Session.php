<?php

class WebApp_Bootstrap_Resource_Session extends Zend_Application_Resource_ResourceAbstract {

    private $_handler = null;
    protected static $_instance;
    protected static $_lifetime = 3600;

    public function init() {
        $options = $this->getOptions();

        $lifetime = self::$_lifetime;
        if (array_key_exists('remember_me_seconds', $options)) {
            $lifetime = $options['remember_me_seconds'];
        }

        // future saveHandler support
        if (array_key_exists('saveHandler', $options)) {
            $saveHandler = $options['saveHandler'];
            $saveHandlerClass = $saveHandler['class'];

            $saveHandlerOptions = array();

            if (array_key_exists('options', $saveHandler)) {
                $saveHandlerOptions = $saveHandler['options'];
            }

            $saveHandler = new $saveHandlerClass($saveHandlerOptions, $lifetime);
            $this->_handler = $saveHandler;
            Zend_Session::setSaveHandler($saveHandler);

            unset($options['saveHandler']);
        }

        if (count($options) > 0) {
            Zend_Session::setOptions($options);
        }

        $defaultNamespace = new Zend_Session_Namespace();

        if (!isset($defaultNamespace->initialized)) {
            Zend_Session::regenerateId();
            $defaultNamespace->initialized = true;
        }

        Zend_Registry::set('Zend_Session', $defaultNamespace);
        $_SESSION['isRegistered'] = (string) Zend_Auth::getInstance()->hasIdentity();
    }

    public function getHandler() {
        return $this->_handler;
    }

}
