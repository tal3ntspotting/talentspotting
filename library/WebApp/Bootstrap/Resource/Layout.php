<?php
class WebApp_Bootstrap_Resource_Layout extends Zend_Application_Resource_ResourceAbstract
{

    public function init()
    {
    	$layoutSuffix = '';
		Zend_Registry::set('LAYOUT_SUFFIX', $layoutSuffix);
    	
		return Zend_Layout::startMvc($this->getOptions());
    }

}