<?php
class WebApp_Db_Table extends Zend_Db_Table_Abstract
{
    protected $_db;
    public function __construct($adapter = 'mysql')
    {
        parent::__construct(array('adapter' => $adapter));
        $this->setAdapter($adapter);
    }

    public function setAdapter($adapter = 'mysql')
    {
        switch ($adapter)
        {
            case 'mysql':
                $dbAdapter = Zend_Registry::get('dbMysqlAdapter');
            break;

            case 'warehouse':
                $dbAdapter = Zend_Registry::get('dbWarehouseAdapter');
            break;
        }

        $this->_db = $dbAdapter;
        $this->setDefaultAdapter($dbAdapter);
    }

    public function cache($key = null, $tag = null)
    {
        return new WebApp_Cache($this, $key, $tag);
    }

    public function queue()
    {
        return new WebApp_Queue($this);
    }

    public function select ($withFromPart = Zend_Db_Table_Abstract::SELECT_WITHOUT_FROM_PART)
    {
        return parent::select($withFromPart)->setIntegrityCheck(false);
    }

    public function query($sql)
    {
        return $this->_db->query($sql);
    }

    public function quote($value, $type = null)
    {
        return $this->_db->quote($value, $type);
    }

    public function quoteInto($value, $type = null)
    {
        return $this->_db->quoteInto($value, $type);
    }

    // @param $sql_object - OPTIONAL An SQL WHERE clause or Zend_Db_Table_Select object.
    public function fetchAll($sql_object = null, $order = null, $count = null, $offset = null)
    {
        if (null !== $sql_object)
        {
            return $this->_db->fetchAll($sql_object);
        }
    }
    
    public function fetchAssoc($sql_object = null, $order = null, $count = null, $offset = null)
    {
        if (null !== $sql_object)
        {
            return $this->_db->fetchAssoc($sql_object);
        }
    }
    
    public function fetchRow($sql_object = null, $order = null, $offset = null, $object = null)
    {
        if (null !== $sql_object)
        {
            $data = $this->_db->fetchRow($sql_object);            
            if (!empty($data))
            {
                if (null !== $object)
                {
                    return (object) $data;
                }
                else
                {                    
                    return $data;
                }
            }
            else
            {
                return array();       
            }
        }
        else
        {            
            return array();
        }
    }

    public function fetchPairs($sql)
    {
        return $this->_db->fetchPairs($sql);
    }

    public function lastInsertId()
    {
        return $this->_db->lastInsertId();
    }

    public function update(array $data, $where, $table = null)
    {
        if (null === $table)
        {
            $table = $this->_name;
        }
        return $this->_db->update($table, $data, $where);
    }

    public function insert(array $data, $table = null)
    {
        if (null === $table)
        {
            $table = $this->_name;
        }
        return $this->_db->insert($table, (array) $data);
    }

    public function delete($where, $table = null)
    {
        if (null === $table)
        {
            $table = $this->_name;
        }
        return $this->_db->delete($table, $where);
    }

    public function upAppHisTable()
    {
        $sql = "ALTER TABLE `job_application_status_history` ADD `notes` longtext AFTER `date`;";
        $this->query($sql);
    }
}