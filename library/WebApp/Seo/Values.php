<?php
class WebApp_Seo_Values
{
    protected $_filters;
    protected $_controller;
    protected $_title = array("1" => "Synthesis Management Consulting", "2" => "Synthesis Management Consulting");
    protected $_keywords = array("1" => "proiecte europene, consultanta, Finantare UE, Training, Synthesis Software", "2" => "european projects, consultancy, EU financing, training, Synthesis Software");
    protected $_description = array("1" => "Compania noastra este autorizata de catre Consiliul National de Formare Profesionala a Adultilor (CNFPA) pe urmatoarele domenii: Competente Antreprenoriale, Manager Proiect, Manager imbunatatire procese, Lucrator social, Asistent social (nivel mediu), Mediator social, Lucrator pensiune turistica, Operator introducere si validare date, Formator, Manager de inovare.", "2" => "Compania noastra este autorizata de catre Consiliul National de Formare Profesionala a Adultilor (CNFPA) pe urmatoarele domenii: Competente Antreprenoriale, Manager Proiect, Manager imbunatatire procese, Lucrator social, Asistent social (nivel mediu), Mediator social, Lucrator pensiune turistica, Operator introducere si validare date, Formator, Manager de inovare.");
    protected $_id_lang;

    public function __construct($controller, $id_lang)
    {
        $this->_filters = $filters;
        $this->_controller = $controller;
        $this->_id_lang = $id_lang;
    }
    
    private function _set()
    {
        $this->_controller->view->headTitle()->set($this->_title[$this->_id_lang]);        
        $this->_controller->view->headMeta()->setName("keywords", $this->_keywords[$this->_id_lang]);
        $this->_controller->view->headMeta()->setName("description", $this->_description[$this->_id_lang]);
    }
    
    public function index()
    {
        $this->_set();
    }
    
    public function category($category)
    {        
        $this->_title[$this->_id_lang] = "{$category['title']}";
        $this->_keywords[$this->_id_lang] = implode(",", explode(" ", $category['description']));
        $this->_description[$this->_id_lang] = $category['description'];
        $this->_set();
    }
    
    public function project($project)
    {        
        $this->_title[$this->_id_lang] = "{$project['title']}";
        $this->_keywords[$this->_id_lang] = implode(",", explode(" ", $project['intro']));
        $this->_description[$this->_id_lang] = $project['intro'];
        $this->_set();
    }
}
