<?php

class Helper
{
    public $helper;
    public function __construct()
    {
        $this->helper = new models_Jobs;
    }

    public function createEmailByType($type = null)
    {
        $this->helper->insert([
            'id_company' => 1,
            'type' => $type,
            'status' => 1,
        ],'emails');
        $id = $this->helper->lastInsertId();

        $this->helper->insert([
            'content' => 'mail content',
            'subject' => 'mail subject',
            'id_email' => $id
        ],'emails_lang');

    }

    public function addCompany()
    {
        $this->helper->insert(
            ['email'=> 'jeddey.seifeddine@gmail.com',
            'contact_person' => 'jeddey seifeddine',
            'notification' => 0,
            'slug' => 'tsp_company',
            'title' => 'Tsp Company',
            'status' => 1
            ],
            'companies');
        return $this->helper->lastInsertId();
    }
}
class Smarty {
    public function assign($tpl_var, $value = null, $nocache = false)
    {
        return $this;
    }
    public function fetch($template = null, $cache_id = null, $compile_id = null, $parent = null)
    {

    }
}

?>