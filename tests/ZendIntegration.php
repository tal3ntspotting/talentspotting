<?php

abstract class ZendIntegration extends Zend_Test_PHPUnit_DatabaseTestCase {
    private $_connectionMock;

    public $app;

    public function __construct()
    {
        defined('APPLICATION_ENV') || define('APPLICATION_ENV','testing');
        $this->app = new Zend_Application(APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini');
        $this->app->bootstrap();
    }

    /**
     * Returns the test database connection.
     *
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected function getConnection()
    {
        $params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "testing", false);
        if($this->_connectionMock == null) {
            $connection = Zend_Db::factory($params_productions->resources->Db->adapter, array(
                'host'             => $params_productions->resources->Db->params->host,
                'username'         => $params_productions->resources->Db->params->username,
                'password'         => $params_productions->resources->Db->params->password,
                'dbname'           => $params_productions->resources->Db->params->dbname
            ));
            $connection->query("SET GLOBAL sql_mode = '';");
            $this->_connectionMock = $this->createZendDbConnection(
                $connection, 'zfunittests'
            );

            Zend_Db_Table_Abstract::setDefaultAdapter($connection);
            Zend_Registry::set('dbMysqlAdapter', $this->getAdapter());

        }
        return $this->_connectionMock;
    }
    /**
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createMySQLXMLDataSet(
            dirname(__FILE__) . '/records/data.xml'
        );

    }

    public function tearDown()
    {
    }

}
?>