<?php

abstract class Zend extends Zend_Test_PHPUnit_ControllerTestCase
{
    /**
     * @var Zend_Application
     */
    protected $application;

    public function setUp() {
        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();
    }

    public function appBootstrap() {
        defined('APPLICATION_ENV') || define('APPLICATION_ENV','testing');
        $this->application = new Zend_Application(APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini');
        $this->application->bootstrap();
    }

    public function dispatch($url = null)
    {
        $this->getFrontController()->setParam('bootstrap', $this->application->getBootstrap());
        parent::dispatch($url);
    }
    public function tearDown()
    {
        /* Tear Down Routine */
    }

}

?>