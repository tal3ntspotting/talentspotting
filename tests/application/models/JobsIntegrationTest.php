<?php
use Mockery as m;
use Codeception\Stub as Stub;
class JobsIntegrationTest extends ZendIntegration{


    public function testApplyJobWithoutTrack()
    {
        Zend_Controller_Front::getInstance()->setParam('bootstrap',$this->app->getBootstrap());
        $model = $this->getMockBuilder(models_Jobs::class)
            ->setMethods(['getRecommenderFromSession','checkRecommnederContract'])
            ->getMock();

        $model->method('getRecommenderFromSession')
            ->will($this->returnValue(0));
        $model->method('checkRecommnederContract')
            ->will($this->returnValue(null));

        $model->_user =  $this->createUserStorage();
        $job = $this->createJobStub();
        $user = $this->createUserStub();
        $helper = new Helper;
        $helper->addCompany();
        $helper->createEmailByType(25);
        $helper->createEmailByType(26);
        $id_application = $model->apply($user,$job,false);
        $this->assertTrue(is_int($id_application),true);
    }

    public function createJobStub()
    {
        return [
          'id' => 1,
          'id_company' => 1,
            'company_email' => 'jeddey.seifeddine@gmail.com',
            'company_title' => 'test'
        ];
    }

    public function createUserStorage()
    {
        $obj = new stdClass();
        $obj->last_login = "2018-10-10 10:10L10";
        $obj->id_company = 1;
        $obj->type = 9;
        $obj->id = 1;
        $obj->id_parent = 2;
        $obj->image = "image.jog";
        $obj->firstname = "candidate";
        $obj->lastname = "candidate";
        $obj->email = "jeddey.seifeddine@gmail.com";
        Zend_Auth::getInstance()->getStorage()->write($obj);
        return $obj;
    }
    public function createUserStub()
    {
        return [
          'id' => 1,
          'id_company' => 1,
          'firstname' => 'edin',
            'email' => 'jeddey.seifeddine@gmail.com',
        ];
    }
}
?>