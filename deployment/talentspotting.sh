#!/bin/bash

cd ../

git checkout master
git add .
git commit -am "Bot update Run"
git push origin master
git commit -am "Update Database if dumped"
git push origin master

git checkout adpharma
git pull origin adpharma
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin adpharma

git checkout conectys
git pull origin conectys
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin conectys

git checkout demo
git pull origin demo
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin demo

git checkout eyromania
git pull origin eyromania
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin eyromania

git checkout macromex
git pull origin macromex
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin macromex

git checkout michelin
git pull origin michelin
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin michelin

git checkout microchip
git pull origin microchip
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin microchip

git checkout radissonblujobs
git push origin radissonblujobs
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin radissonblujobs

git checkout stefanini
git pull origin stefanini
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin stefanini

git checkout test
git pull origin test
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin test

git checkout unicredit
git pull origin unicredit
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin unicredit

git checkout garmin
git pull origin garmin
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin garmin

git checkout ats
git pull origin ats
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin ats


git checkout accesa
git pull origin accesa
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin accesa

git checkout tpp
git pull origin tpp
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin tpp

git checkout gmv
git pull origin gmv
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin gmv

git checkout euromaster
git pull origin euromaster
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin euromaster

git checkout provident
git pull origin provident
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin provident

git checkout usr
git pull origin usr
git merge --no-commit --no-ff master --strategy-option=theirs --no-edit
git reset -- application/configs/application.ini
git reset -- scripts/cli_config.php
git reset -- public/.htaccess
git checkout public/.htaccess
git checkout application/configs/application.ini
git checkout scripts/cli_config.php
git commit -am "Update Database if dumped"
git push origin usr

git checkout master




