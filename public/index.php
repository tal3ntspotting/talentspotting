<?php

error_reporting(E_ALL); 
ini_set('display_errors', 1);
ini_set("auto_detect_line_endings", true);
set_time_limit(0);
ini_set('memory_limit', '-1');
defined('ABSOLUTE_PATH') || define('ABSOLUTE_PATH', realpath(dirname(__FILE__) . '/..'));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', ABSOLUTE_PATH . '/application');
if($_SERVER['HTTP_HOST'] == "lh.talentspotting.com" || $_SERVER['HTTP_HOST'] == "lh.talentspotting:8080" || $_SERVER['HTTP_HOST'] == "lh.talentspotting")
    {
    defined('APPLICATION_URL') || define('APPLICATION_URL', 'http://' . $_SERVER['HTTP_HOST']);
    } else 
        {
        defined('APPLICATION_URL') || define('APPLICATION_URL', 'https://' . $_SERVER['HTTP_HOST']);
        }


switch (@$_SERVER['APPLICATION_ENV']) {
    case "production":
        error_reporting(0);
        ini_set('display_errors', 0);
        $env = 'production';
        break;

    case "staging":
        error_reporting(0);
        ini_set('display_errors', 0);
        $env = 'staging';
        break;

    case "development":
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'development';
        break;
    case "testing":
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'testing';
        break;

    default:
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', 1);
        $env = 'development';
}

defined('APPLICATION_ENV') || define('APPLICATION_ENV', ($env ? $env : 'production'));
# Ensure library is on include_path

set_include_path(implode(PATH_SEPARATOR, array(
    ABSOLUTE_PATH . '/library',
    APPLICATION_PATH,
)));

include_once 'WebApp/Tools.php';
include_once 'WebApp/Unoconv.php';
include_once 'smarty/Smarty.class.php';
include_once 'smarty/SmartyBC.class.php';
WebApp_Tools::initRuntime();

/** Zend_Application */
include_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
);
$bootstrap = $application->bootstrap()->run();


// here we begin