jQuery(document).ready(function(){
	
	var skills = [];
	
	jQuery('.client-star-rating').raty({half: true});
	jQuery('.job-rating-star').each(function(){
		var starValue = jQuery(this).attr('rel');
		jQuery(this).raty({ readOnly: true, score: starValue }); 
	});
	
	jQuery('.personal-rating').each(function(){
		var starValue = jQuery(this).attr('rel');
		jQuery(this).raty({ readOnly: true, score: starValue }); 
	});

	/* jQuery('#sign_in form input[type="text"]').attr('style', '-webkit-box-shadow: inset 0 0 0 1000px #fafafa !important');
	jQuery('#sign_in form input[type="password"]').attr('style', '-webkit-box-shadow: inset 0 0 0 1000px #fafafa !important'); */
	
	jQuery('.sign-in-mail form').submit(function(e){
		jQuery('.personal-skills .alert').fadeOut('fast');
		jQuery('.personal-skills').fadeIn('slow');
		
		// get text input
	    var $inputs = jQuery('.sign-in-mail form :input[type="text"]');
	
		// build skill array 
	    // replace white spaces with __ for easy manipulation
	    // append new skill to skill tree
	    $inputs.each(function() {
	    	if (jQuery(this).val().match(/[a-z]/i)) {
		    	skills.push(jQuery(this).val().replace(/ /g,'__'));
		    	var rel = jQuery(this).val().replace(/ /g,'__');
		    	jQuery('.sign-in-mail .skill-tree').append('<li rel="'+rel+'">'+jQuery(this).val()+'<span class="delete-skill">-</span></li>');		    	
	    	}else{
	    		jQuery('.personal-skills .alert').show();
	    	}
	    });
	    
	    // restore blank value for text field
	    jQuery('.sign-in-mail form input[type="text"]').val('');

	    return false;
	});
	
	jQuery('.custom-dropdown-container').on('click',function(){
		jQuery(this).find('.custom-drop-down').toggle();
	});
	
	jQuery(document).on('click','.delete-skill', function(){
		var rel = jQuery(this).parent().attr('rel');
		removeSkill(skills,rel);
		jQuery(this).parent().remove();
	});
	
	
});

jQuery(window).load(function(){
	setTimeout(function () {
        jQuery('#gif-loader').fadeOut('slow');
    }, 2000);
	
	jQuery('.location-blue-search').select2();
	jQuery('.desktop-select select').select2();	
	
	jQuery(".country_selector").countrySelect({
		onlyCountries: ['us', 'fr']
	});
	notification_effect();
	
	// Homepage filter select tags assocition
	var filters = [];
	var $eventLog = $(".js-event-log");
	var $eventSelect = $(".desktop-select select");
 	$eventSelect.on("select2:select", function (e) { log("select2:select", e); });
 	function event_append(loc,elem){
 		jQuery('.table-tags').show();
 		jQuery('.table-tags tr td#' + loc).append(elem);
 	}
 
	function log (name, evt) {
	  if (!evt) {
	    var args = "{}";
	  } else {
	    var args = JSON.stringify(evt.params, function (key, value) {
	      if (value && value.nodeName) return "[DOM node]";
	      if (value instanceof $.Event) return "[$.Event]";
	      return value;
	    });
	  }
	 var json_string = JSON.parse(args);
	 var table_value = json_string.data.text; 
	 var table_place = json_string.data.title;
	 var $e = $("<span class='btn btn-info'><span class='info'>" + table_value + "</span><span class='closeMe'>x</span></span>");
	 filters.push({table_place: table_place}); 
	 console.log(filters);
	 event_append(table_place,$e);
	  $e.animate({ opacity: 1 }, 10000, 'linear', function () { });
	 }
	
	//see more / less profile
	jQuery('.work-entry a.more-info').on('click',function(e){
		e.preventDefault();
		var status = jQuery(this).attr('rel');
		if(status == 'more'){
			status = jQuery(this).attr('rel','less');
			jQuery(this).parent().find('.job-attr').slideToggle( "slow" );
			jQuery(this).html('See More');
			
		}else{
			status = jQuery(this).attr('rel','more');
			jQuery(this).parent().find('.job-attr').slideToggle( "slow" );
			jQuery(this).html('See Less');
		}
	});
	
});

function removeSkill(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

 
function notification_effect(){
	var notifications = jQuery('.notifications');
	var container = jQuery(".notifications ul");
	var notification_number = jQuery('.notifications-number');

	jQuery(document).on('click', function (e){
		var target = e.target;
		if(jQuery(target).hasClass('notifications')){
			container.toggle();
			jQuery(notification_number).addClass('hiden').html("");
			
		}else{
			if(!jQuery(target).is('a')){
				container.hide();		
			}
		}
	}); 
}


