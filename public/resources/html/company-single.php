<?php include('header.php'); ?>
<div class="container">
		<div class="row client-single">
			<div class="col-md-9">
				<div class="job-client-info col-md-12">
					<div class="col-md-12 job-cover-photo hidden-xs">
						<img src="images/company-1.jpg" alt="client-1-cover-photo" />
					</div>
					<div class="col-md-12">
						<div class="col-md-3 job-client-profile-photo col-xs-6">
							<img src="images/client-thumb-normal.jpg" alt="client-profile-photo" class="img-thumbnail">
							<div class="client-star-rating"></div>
							<span>Rate Wema Bank</span>
						</div>
						<div class="col-md-2 job-client-info-name col-xs-6">
							<div class="job-client">Wema Bank</div>
							<div class="job-industry">Industry: <span>Banking</span></div>
							<div class="job-country">Country: <span>Nigeria</span></div>
							<div class="city">City: <span>Langos</span></div>
						</div>
						<div class="col-md-3 col-xs-6 job-applications">
							<ul class="list-group">
							  <li class="list-group-item orange">
							    <span class="badge">28</span>
							    Recommendations
							  </li>
							  <li class="list-group-item turcoaz">
							    <span class="badge">14</span>
							    Applications
							  </li>
							  <li class="list-group-item gray">
							    Posted: 09 March 2015
							  </li>
							</ul>
						</div>
						<div class="col-md-4 job-short-description col-xs-12">
							<p>
								At Wema Bank, we cherish the value of relationships 
								and you have been a critical part of our resilience & 
								successes for over 60 years. Every interaction with 
								you helps us serve you better. 
								<br /><br />
								For more information, kindly log on to 
								www.wemabank.com
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 job-full-description">					
					<h2>Company Info</h2>
					<article>
						Wema Bank is one of the largest employers in the financial services industry. We strive to offer our employees leading-edge opportunities that provide an exciting vision of the future. Whether you are an individual contributor or part of a larger team, Wema Bank shares accountability with employees in shaping their careers and in providing an excellent platform to learn and grow.
						<br /><br />
						Wema Bank offers a wide range of career opportunities. There are exciting job positions in business development, branch operations and central operations, corporate and investment banking, and legal and corporate services etc.
						<br /><br />
						At Wema Bank, people are the key to realising our vision. Building trust with colleagues and clients, strong teamwork, professionalism and being accountable for our actions are the foundation for our success. We are an organisation that appreciates and values your unique talents, contributions and accomplishments.
						<br /><br />
						Everyone at Wema Bank either directly serves our customers, or works in partnership to support those that do. Our employees are accountable for fulfilling the responsibilities of their job positions and striving for the highest level of performance. This is especially important in respect of decisions that affect our customers.
					</article>
				</div>
				<div class="job-personal-reviews col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2>Wema Bank <span class="color-turcoaz">Reviews</span></h2>
					<?php
						for ($i=0; $i < 2 ; $i++) { 
							?>
								<div class="job-testimonial">
									<div class="col-lg-1 col-md-1 col-sm-1 col-xs-3 job-testimonial-image">
										<img src="images/default-account-image.png" alt="Account Image" class="img-circle">
									</div>		
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-9 job-testimonial-details">
										<h3>Awesome experience; great people</h3>
										<div class="personal-rating" rel="3.5"></div><!-- use rel attribute to set star rating value, values 1-5 -->
										<div class="details">
											Worked at <a href="#">Wema Bank</a> as an <a href="#">Marketing Specialist</a> for <a href="#">2 years</a> and <a href="#">11 months </a>
										</div>
									</div>	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 job-testimonial-content">
										Focus on people, encouraging initiative, passion. product and customers are key for all teams, focused on being 
										agile and flexible. love the people, fast growing, 
										continuous learning.
										As with any fast growing environment, 
										wanting to do too much, too fast can be frustrating but 
										also a challenge.
									</div>
								</div>	
							<?php
						}
					?>		
			     </div>
			</div>
			<div class="col-md-3 job-sidebar">
				<div class="job-company-criteria-box job-sidebar-box">
					<span class="job-company-rating">Company Info</span>
					<span class="job-company-criteria">Website</span>
					<a href="#" class="job-company-criteria-answer">www.webmabank.co.za</a>
					<span class="job-company-criteria">Size</span>
					<span class="job-company-criteria-answer">100-500 Employees</span>
					<span class="job-company-criteria">Industry</span>
					<span class="job-company-criteria-answer">Banking</span>
					<span class="job-company-criteria">Work Enviroment</span>
					<div class="job-star-rating-enviroment job-rating-star" rel="3"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Career Opportunities</span>
					<div class="job-star-rating-career job-rating-star" rel="1"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Comp &#38; Benefits</span>
					<div class="job-star-rating-benefits job-rating-star" rel="4"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Work-Life Balance</span>
					<div class="job-star-rating-balance job-rating-star" rel="5"></div><!-- use rel attribute to set star rating value, values 1-5 -->
				</div>
				<div class="job-similar-box job-sidebar-box col-md-12">
					<span class="job-title-box">Company Jobs</span>
					<?php  
						for ($i=0; $i < 10 ; $i++) { ?>
							<div class="job-single-similar col-md-12 col-sm-6 col-xs-12">
								<a href="#">
									<span class="job-thumbnail-small"><img src="images/job-icon-small.png" alt="Firm <?php echo $i; ?>" /></span>
									<span class="job-position-similar">Investors Relations Officer</span>
									<span class="job-client-similar">Wema Bank - Nigeria, Lagos</span>
								</a>
							</div>
						<?php } ?>					
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php'); ?>
