<?php include('header.php'); ?>
<div class="container">

	<div class="row single-news-article">
		<div class="news-list col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
			<div class="single-news-image">
				<img src="images/news-1.jpg" alt="post title" />
				<div class="wrapper-title-news">
					<h1>5 Interview questions you must prepare for</h1>
				</div>
			</div>
			<div class="single-about">
				<small>Posted by <a href="#">Admin</a> on <span class="date">27.07.2015</span></small>
				<small>in category: <a href="#">Trends</a></small>
			</div>
			<article>
				<h2>Heading test H2</h2>
				<h3>Heading test H3</h3>
				<h4>Heading test H4</h4>
				<h5>Heading test H5</h5>
				<h6>Heading test H6</h6>
				By now I believe that everyone knows the value of <a href="#">talented individuals within an organization.</a> But in order to hire the best ones you need to find them first. This can be a long and bumpy road ahead. But, if you identify your target, study it, communicate smart and focus the recruitment efforts in the right direction, you will be able to attract the right people for the right jobs. 
				<br /><br />
				<hr />
				Here’s an example of how to do it, based on our wonderful experience in Mozambique, Cameroon, Ghana and Senegal, with Bollore, one of the largest employers in Africa. 
				<br /><br />
				<strong>1. Advertise Smart</strong> <br />
				No need to say that recruiting is not a process of wishing the right candidates will knock at your door out of the blue. You need to show yourself out there, but not anywhere. Deciding how and where to advertise is directly related to the candidate profile. So what we did first was to identify the exact profile (competencies, skills, knowledge and motivation that will suit the job perfectly) and understand the behavior (where could wey factor of success, because otherwise we would be barking at the wrong tree. 
				<br /><br />
				Next, we wanted to make sure that applying for the job was an easy process, at a click away. So we created an online application system for collecting resumes. You really don’t want to find your talent and loose it as soon as they want to apply because of an unbearable journey of never ending steps.
				 <br /><br />
				At the end of this strategic approach we came up with a well-structured advertising plan and a list of channels, each with specific objectives and timings for promotion. Also, we developed an eye-catching site with comprehensive job ads, a friendly application system and of course, the Tell-a-Friend button that helped us to go viral and collect thousands of applications. 
				<br /><br />
				Surely, this sounds nice and kind of easier said than done. We got to the implementation phase where we tracked, measured and optimized constantly our channels. Long story short, this is where we advertised and found talent: 
				<br /><br />
				<ul>
					<li> Major local newspapers </li>
					<li> Local Career & News websites </li>
					<li> NGO network partnerships </li>
					<li> University alumni networks </li>
					<li>Direct contacting  find them, what do they like, how to communicate with them). </li>
					<li>Based on this research we identified channels and prioritized them according to their relevance and reach power. Identifying the right channels is a ke</li>
					<li> Online Ads (with very well targeted audiences on Facebook, Google and LinkedIn)</li>
					<li> Personal & Professional network recommendations </li>
				</ul>
				<ol>
					<li> Major local newspapers </li>
					<li> Local Career & News websites </li>
					<li> NGO network partnerships </li>
					<li> University alumni networks </li>
					<li>Direct contacting  find them, what do they like, how to communicate with them). </li>
					<li>Based on this research we identified channels and prioritized them according to their relevance and reach power. Identifying the right channels is a ke</li>
					<li> Online Ads (with very well targeted audiences on Facebook, Google and LinkedIn)</li>
					<li> Personal & Professional network recommendations </li>
				</ol>
				<br /><br />
				Great! I suppose you are already wandering about the results we had. Well, the entire campaign generated more than 5000 resumes in one month. Pretty impressive, right? But what are the chances to have 5000 matches? That’s why we continued with an assessment process. Here’s how: 
				<br /><br />
				<strong>2. Select for a perfect match</strong> <br />
				Having 5000 applicants was very encouraging, but this demanded a thorough design of assessment to narrow it down to a short list (a very short list) of 24 recommendations to be hired. 
				Long story short, we followed the following steps:
				<br /><br />
				- CV Screening 
				5000 CV’s were read and assessed according to a set of criteria demanded by the role. This procedure resulted in finding an estimate of 1000 CV’s with a matching potential. 
				<br /><br />
				- Phone screening 
				<br /><br />
				Next, to further sift out, we started contacting every one of them for a phone interview. This helped us to identify whatever information we couldn’t find mentioned in the CV. Valuable information like motivation, availability, interests and such were found during the interview. 
				<br /><br />
				- Ability testing 
				500 candidates qualified further and entered into the third phase. We invited all of them, in clusters, to take a written logical test. This activity happened in 2 locations: Abidjan and Maputo. 
				<br /><br />
				- Assessment Centers <br />
				We conducted Assessment Centers in 3 countries: Senegal, Cameroon and Ghana with a total al 48 people assessed – the first 48 professionals according to the logical reasoning test results. This was the final and most elaborated step, as each assessment center measured a set of 14 competencies within 3 exercises:
				<br /><br />
					- Individual presentation – this was held in front of the assessors committee and measured leadership, training abilities, self-control and stress resilience. 
					- Group exercise – based on a case study, the participants were asked to work as a group to make several business decisions. We looked for competencies like teamwork, conflict management, communication skills, listening skills, open minded approach, adaptability and others. 
					- Group interview – this was the final assessment tool, in which we assessed communications skills, motivation, willingness to travel and language skills. 
				<br /><br />
				After 6 assessment days, we concluded a short list of 24 people. These were the final candidates that were recommended and got the desired job within Bollore. 
				<br /><br />
				  <div class="col-xs-6 col-md-3">
				    <a href="#" class="thumbnail">
				      <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTRlNmZkZDdmODAgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNGU2ZmRkN2Y4MCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI2MSIgeT0iOTQuNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" alt="...">
				    </a>
				  </div>
				Now, this is only one example of how to attract and select the best talent on the market. We have experience in many more, and I can tell you that these projects rarely look alike. If you’re wondering what is the most important step in the process, well, the truth is every step has a key role in reaching the final objective, which is hiring the best match for your company and role. 
				<br /><br />
				Starting with advertising and finding your way to them and reach their attention, to having a well-planned assessment strategy, they are all completing each other and focusing you on the right people. 
				<br /><br />
				I know, you are probably thinking that this process took maybe a couple of months to come to an end and you are right. It took 3 month and a few days to reach the short list (though I must restate we found the most suited candidates). Surely, most of you out there don’t have a similar schedule to work with. So here comes the good news! 
				<br /><br />
				WorkinAfrica does it all for you in a matter of minutes - is faster, easier and equally skilled to find the best match for you and your company. How does it do it? WorkinAfrica matches every candidate’s skills with those required by the job and provides you instantly will a list of suited candidates ordered by the matching score
				. <br /><br />
				Let’s say you’re looking for a banking magician, an inspirational teacher or a passionate programmer. As soon as you post your job ad online, WorkinAfrica will search the data base for the best professionals to perform at that job. After screening more than 500.000 professionals (which takes no more than a couple of minutes!) you will be presented with the top professionals that match your job, just like that. 
				<br /><br />
				
				  <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
				      poster="http://video-js.zencoder.com/oceans-clip.png"
				      data-setup="{}">
				    <source src="http://video-js.zencoder.com/oceans-clip.mp4" type='video/mp4' />
				    <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
				    <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
				    <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
				    <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
				    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
				  </video>
				
				In other words, WorkinAfrica makes 3 month become 3 minutes. I let you imagine the money and the resources you save. Basically, you have all the above steps integrated in one technology – from the advertising phase that reaches more than 500.000 professionals, to the selection phase based on the candidate – job matching. 
				To keep it short, WorkinAfrica is the way we attract talent. Care to give it a try?
				<div class="social-sharing">
					<hr />
					<ul>
						<li><a href="#" title="Share this on Linkedin"><img src="images/icon-in.png" alt="Linkedin Share News" /></a></li>
						<li><a href="#" title="Share this on Facebook"><img src="images/icon-fb.png" alt="Facebook Share News" /></a></li>
						<li><a href="#" title="Share this on Twitter"><img src="images/icon-tw.png" alt="Twitter Share News" /></a></li>
						<li><a href="#" title="Share this on Google Plus"><img src="images/icon-g.png" alt="Google Plus Share News" /></a></li>
					</ul>
				</div>
			</article>
			<div id="comments" class="col-md-12 comments">
				<div class="bs-example" data-example-id="default-media">
				    <div class="media">
				      <div class="media-left">
				        <a href="#">
				          <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNGU3MGJhN2QyMCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE0ZTcwYmE3ZDIwIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
				        </a>
				      </div>
				      <div class="media-body">
				        <h4 class="media-heading">Username</h4>
				        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
				      </div>
				    </div>
				    <div class="media">
				      <div class="media-left">
				        <a href="#">
				          <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNGU3MGJhYzgzNCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE0ZTcwYmFjODM0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
				        </a>
				      </div>
				      <div class="media-body">
				        <h4 class="media-heading">Username</h4>
				        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
				        <div class="media">
				          <div class="media-left">
				            <a href="#">
				              <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNGU3MGJhOTBhNCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE0ZTcwYmE5MGE0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true">
				            </a>
				          </div>
				          <div class="media-body">
				            <h4 class="media-heading">Username</h4>
				            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
				          </div>
				        </div>
				      </div>
				    </div>
				  	<hr />
				  	<form>
				  		<h4>Leave a comment</h4>
				  		<textarea class="form-control" rows="5" id="comment"></textarea>
				  		<button type="submit" value="login" class="btn btn btn-warning right">Submit</button>
				  	</form>			    				   
				  </div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 news-sidebar">
			<div class="categories">
				<div class="hp-sidebar-title">Categories</div>
				<ul>
					<li><a href="#">Bussines News</a></li>
					<li><a href="#">Voluntary News</a></li>
				</ul>
			</div>
			<div class="tab-content">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Popular</a></li>
				  </ul>
				  <div role="tabpanel" class="tab-pane fade in active" id="home">
				  	<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>			 
				  </div>
				  <div role="tabpanel" class="tab-pane fade" id="profile">
				  		<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>			  		
				  </div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-test col-xs-12">
					<div class="hp-sidebar-title">Latest Tests</div>
					<div class="hp-sidebar-latest-test">
						<div class="hp-sidebar-latest-test-title">
							<img class="left" src="images/sidebar-test-icon.png" alt="icon-test-siderbar" />
							<h3 class="left">16Personalities</h3>
						</div>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
						</p>
						<a href="#" class="btn">Take the Test</a>
					</div>
				</div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-gamification col-xs-12">
					<div class="hp-sidebar-title">Earn Gamification Points</div>
					<div class="hp-sidebar-gamification-inner">
						<img src="images/gamification-icon.png" alt="gamification-icon" />
						<p>Get a concrete, accurate description of who you are, what you can do and what are the best jobs for you.</p>
						<a href="#" class="btn">Complete your profile</a>
					</div>
				</div>
			</div> 
	</div>
</div>
</div>
<?php include('footer.php'); ?>
