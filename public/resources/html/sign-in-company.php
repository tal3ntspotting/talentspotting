<?php include('header.php')?>
<div class="company-signin">
	<div class="company-first">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="outer">
						<div class="middle">
							<div class="inner">
								<h1>I already have an account</h1>
								<form id="sign-up-company">
									 <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
									<input type="password" class="form-control"  name="password" placeholder="Password" />
									<div class="company-signin-form"><input type="checkbox" /><span>Remember me</span> <a id="forgotPass" href="#">I forgot my password</a></div>									
									<button type="submit" value="login" class="btn btn btn-warning">Submit</button>
								</form>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sign-in-company">
					<h2>I don't have an account yet</h2>
					<form id="sign-in-company">
						<input type="text" class="form-control" name="company-fname" placeholder="Firstname" />
						<input type="text" class="form-control" name="company-lname" placeholder="Lastname" />
						<input type="email" class="form-control" name="company-email" placeholder="Email" />
						<input type="text" class="form-control" name="company-name" placeholder="Company name"/>					
						<input type="password" class="form-control" name="company-pass" placeholder="Password" />											
						<!-- <input type="text" class="form-control" name="company-country" placeholder="Country" /> -->
						<input type="text" class="form-control" name="company-city" placeholder="City" />
						<button type="submit" value="login" class="btn btn btn-warning">Register new account</button>						
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="company-second">
		<div class="container">
		<h3>We want people to make the most of their professional time.</h3>
		<p>
			Time limit make us creatively and transparently connect professionals with their dream jobs.
			<br />
			<br />
			<br />
			We are a recruiting company, specialized in finding, growing and connecting the right 
			people with their dream jobs, considering all relevant factors from company’s organizational 
			culture fit, to the expected skills match, right timing and motivation.
		</p>
		</div>
	</div>
	<div class="company-third">
		
	</div>
</div>
<?php include('footer.php')?>