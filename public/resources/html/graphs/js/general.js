/*
 * Company : Waio
 * JS Template : Graph
 * Author : Robert Voicu
 * Date : 21.08.2015
 * Version : 1.0.0
 * 
 * Copyright 2015 Robert Voicu - Waio
 * Released under the MIT license
 * 
 */
 
$(window).load(function(){

	 	console.log('!');
	 	$( "#box" ).animate({
      width: "90%"
    	}, 
    	{
     	 queue: false,
      duration: 2000
    });
	
});
var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
        }
    ]
};

var data1 = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My Second dataset",
            fillColor: "rgba(136,27,101,0.9)",
            strokeColor: "#881b65",
            pointColor: "#881b65",
            pointStrokeColor: "#ba2d8d",
            pointHighlightFill: "#ba2d8d",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 90, 56, 77, 12, 81]
        },
        {
            label: "My Third dataset",
            fillColor: "rgba(69,201,249,0.9)",
            strokeColor: "#20c0fa",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#20c0fa",
            pointHighlightFill: "#20c0fa",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [100, 10, 20, 100, 44, 49, 40, 66, 98, 45]
        },
        {
            label: "My Third dataset",
            fillColor: "rgba(251,194,41,0.9)",
            strokeColor: "#fbc028",
            pointColor: "#c0921c)",
            pointStrokeColor: "#fbc028",
            pointHighlightFill: "#fbc028",
            pointHighlightStroke: "#c0921c",
            data: [12, 2, 5, 22, 14, 11, 20, 8, 10, 17]
        }
    ]
};

// Get context with jQuery - using jQuery's .get() method.
var ctx1 = $("#chart1").get(0).getContext("2d");


// This will get the first returned node in the jQuery collection.
var myLineChart1 = new Chart(ctx1).Line(data1,{
            responsive: true
        });




var trace1 = {
  x: ['giraffes', 'orangutans', 'monkeys'],
  y: [20, 14, 23],
  name: 'SF Zoo',
  type: 'bar'
};

var trace2 = {
  x: ['giraffes', 'orangutans', 'monkeys'],
  y: [12, 18, 29],
  name: 'LA Zoo',
  type: 'bar'
};

var data = [trace1, trace2];

var layout = {barmode: 'stack'};

Plotly.newPlot('myDiv', data, layout);


 window.onload = function () {
                        var chart = new CanvasJS.Chart("chartContainer",
                        {
                          title:{
                            text: "Division of products Sold in Quarter."

                          },
                          data:[
                          {
                            type: "stackedBar100",
                            showInLegend: "true",
                            dataPoints: [
                            {y: 600, label: "Water Filter" },
                            {y: 400, label: "Modern Chair" },
                            {y: 120, label: "VOIP Phone" },
                            {y: 100, label: "Microwave" },
                            {y: 120, label: "Water Filter" },
                            {y: 374, label: "Expresso Machine" },
                            {y: 350, label: "Lobby Chair" }

                            ]
                          },
                           {
                            type: "stackedBar100",
                            dataPoints: [
                            {y: 400, label: "Water Filter" },
                            {y: 500, label: "Modern Chair" },
                            {y: 220, label: "VOIP Phone" },
                            {y: 350, label: "Microwave" },
                            {y: 220, label: "Water Filter" },
                            {y: 474, label: "Expresso Machine" },
                            {y: 450, label: "Lobby Chair" }

                            ]
                          },
                          {
                            type: "stackedBar100",
                            dataPoints: [
                            {y: 300, label: "Water Filter" },
                            {y: 610, label: "Modern Chair" },
                            {y: 215, label: "VOIP Phone" },
                            {y: 100, label: "Microwave" },
                            {y: 75, label: "Water Filter" },
                            {y: 310, label: "Expresso Machine" },
                            {y: 340, label: "Lobby Chair" }

                            ]
                          },
                          {
                            type: "stackedBar100",
                            dataPoints: [
                            {y: 0, label: "Water Filter" },
                            {y: 200, label: "Modern Chair" },
                            {y: 0, label: "VOIP Phone" },
                            {y: 50, label: "Microwave" },
                            {y: 300, label: "Water Filter" },
                            {y: 0, label: "Expresso Machine" },
                            {y: 0, label: "Lobby Chair" }

                            ]
                          }

                          ]

                        });

                    chart.render();
                    }