<?php include('header.php'); ?>
	<main>
		<div class="container">
		        <h1>Graphs</h1>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<canvas id="chart1" class="chart line-chart" data-attr="chart1" ></canvas>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Division of products Sold in Quarter."

      },
      data:[
      {
        type: "stackedBar100",
        showInLegend: "true",
        dataPoints: [
        {y: 600, label: "Water Filter" },
        {y: 400, label: "Modern Chair" },
        {y: 120, label: "VOIP Phone" },
        {y: 100, label: "Microwave" },
        {y: 120, label: "Water Filter" },
        {y: 374, label: "Expresso Machine" },
        {y: 350, label: "Lobby Chair" }

        ]
      },
       {
        type: "stackedBar100",
        dataPoints: [
        {y: 400, label: "Water Filter" },
        {y: 500, label: "Modern Chair" },
        {y: 220, label: "VOIP Phone" },
        {y: 350, label: "Microwave" },
        {y: 220, label: "Water Filter" },
        {y: 474, label: "Expresso Machine" },
        {y: 450, label: "Lobby Chair" }

        ]
      },
      {
        type: "stackedBar100",
        dataPoints: [
        {y: 300, label: "Water Filter" },
        {y: 610, label: "Modern Chair" },
        {y: 215, label: "VOIP Phone" },
        {y: 100, label: "Microwave" },
        {y: 75, label: "Water Filter" },
        {y: 310, label: "Expresso Machine" },
        {y: 340, label: "Lobby Chair" }

        ]
      },
      {
        type: "stackedBar100",
        dataPoints: [
        {y: 0, label: "Water Filter" },
        {y: 200, label: "Modern Chair" },
        {y: 0, label: "VOIP Phone" },
        {y: 50, label: "Microwave" },
        {y: 300, label: "Water Filter" },
        {y: 0, label: "Expresso Machine" },
        {y: 0, label: "Lobby Chair" }

        ]
      }

      ]

    });

chart.render();
}
</script>
					<div id="chartContainer" style="height: 300px; width: 100%;">
				</div>
			</div>
		</div>
		
	</main>
<?php include('footer.php'); ?>