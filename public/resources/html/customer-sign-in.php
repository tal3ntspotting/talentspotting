<?php include('header.php'); ?>
	<div class="customer-sign-in">
		<section class="intro">  
		  <div class="container">
			  <div class='content'>
			  	<div class="row">
			  		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sign-in-form">
			  			<h1>Sign In</h1>
			  			<form>
			  				<input type="email" class="form-control" placeholder="Email" />
			  				<input type="password" class="form-control" placeholder="Password" />
			  				<button class="right btn btn-warning">Sign In</button>
			  			</form>
			  		</div>
			  		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			  			<h2>Or you can sign in with one of your social platforms</h2>
			  			<ul>
			  				<li><a href="#">Linkedin</a></li>
			  				<li><a href="#">Facebook</a></li>
			  				<li><a href="#">Google +</a></li>
			  				<li><a href="#">Twitter</a></li>
			  			</ul>
			  		</div>
			  	</div>
			  </div>
		 </div>
		</section>
	</div>
<?php include('footer.php'); ?>