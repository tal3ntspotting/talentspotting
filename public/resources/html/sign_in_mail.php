<?php include('header.php')	?>
<div class="sign-in_2-lvl1 sign-in-mail">
	<div class="container">
		<div class="row">
			<h2>GREAT JOB!</h2>
			<h3>Ok <span class="candidate">Alex</span>, now let’s see what you can do.</h3>
			<div class="example-skills">
				<span>Exemple:</span>
				<span class="example-skill">Java</span>
				<span class="example-skill">Training</span>
				<span class="example-skill">Time Management</span>
				<span class="example-skill">Teamwork</span>
			</div>
			<div class="personal-skills">
				<div class="alert alert-warning alert-dismissible fade in" style="display:none;" role="alert">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			      Every skill need atleast <strong>one letter!</strong>
			    </div>
				<span>Your Skills:</span>
				<ul class="skill-tree">
				</ul>
			</div>
			<form>
				<input type="text" name="addSkill" value="" placeholder="Add Skill" />
				<input type="submit" value=" " />
			</form>			
			<a href="#">Done</a>
		</div>
	</div>
</div>
<div class="sign-in_2-lvl2">
	<div class="container">
		<div class="row">
			<p>
				We believe that professional life affects people’s personal life too much to be taken lightly.
 				<br /><br />
				Our mission is to build the frame in which people are encouraged to set career goals, <br />
				develop their core and soft skills and get their dream jobs faster. <br />
				<br />
				Time is too short to wait for a call ...
			</p>
			<ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<li class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
					<img src="images/opp-1.png" alt="Suitable Jobs" />
					<span>Suitable jobs just for you</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-2.png" alt="Suitable Jobs" />
					<span>Rewards based on your professional activity</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-3.png" alt="Suitable Jobs" />
					<span>News, company reviews and more other informations about your interests</span>
				</li>
			</ul>
			<p>Join our professionals league and grow faster!</p>
		</div>	
	</div>
</div>
<div class="sign-in_2-lvl3">
	<div class="container">
		<div class="center-lvl3">
			<h1>We help you get a better job for a better life in Africa</h1>
			<a href="#">Let's start!</a>
		</div>
	</div>
</div>

<?php include('footer.php')	?>