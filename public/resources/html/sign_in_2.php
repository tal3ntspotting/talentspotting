<?php include('header.php')	?>
 <!--	<div class="sign-in_2-lvl1">
<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding" id="sign_in">
				<div class="sign-in-opt-left">I already have an account</div>
				<div class="sign-in-label">Sign In</div>
				<form autocomplete="off">
					<input type="email" name="sign_in_mail" value="" placeholder="Email" autocomplete="off"/>
					<input type="password" name="sign_in_pass" value="" placeholder="Password" />
					<a href="#" id="sign_in_linkedin">
						Linkedin
					</a>
					<ul>
						<li><a href="#"><img src="images/fb-icon-transparent.png" alt="Sign in with Facebook" /></a></li>
						<li><a href="#"><img src="images/gplus-icon-transparent.png" alt="Sign in with Google+" /></a></li>
						<li><a href="#"><img src="images/twitter-icon-transparent.png" alt="Sign in with Twitter" /></a></li>
					</ul>
					<input type="submit" value="Log In" />
				</form>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding" id="sign_up">
				<div class="sign-up">
					<div class="sign-in-opt-right">I don't have an account yet</div>
					<div class="sign-up-label">Sign Up</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<form>
							<input type="email" name="sign_up_mail" value="" placeholder="Email" />
							<input type="password" name="sign_up_pass" value="" placeholder="Password" />
							<input type="password" name="sign_up_confirm_password" value="" placeholder="Confirm Password" />
							<input type="text" name="sign_up_first_name" value="" placeholder="First Name" />
							<input type="text" name="sign_up_last_name" value="" placeholder="Last Name" />
							<input type="submit" value="Register new account" />
						</form>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sign-up-social">
						<span>or you can use</span>
						<a href="#" class="sign-up-2-linkedin">
							Linkedin
						</a>
						<ul>
							<li><a href="#"><img src="images/fb-icon-transparent.png" alt="Sign in with Facebook" /></a></li>
							<li><a href="#"><img src="images/gplus-icon-transparent.png" alt="Sign in with Google+" /></a></li>
							<li><a href="#"><img src="images/twitter-icon-transparent.png" alt="Sign in with Twitter" /></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
<div class="sign-in_2-lvl2">
	<div class="container">
		<div class="row">
			<p>
				We believe that professional life affects people’s personal life too much to be taken lightly.
 				<br /><br />
				Our mission is to build the frame in which people are encouraged to set career goals, <br />
				develop their core and soft skills and get their dream jobs faster. <br />
				<br />
				Time is too short to wait for a call ...
			</p>
			<ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<li class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
					<img src="images/opp-1.png" alt="Suitable Jobs" />
					<span>Suitable jobs just for you</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-2.png" alt="Suitable Jobs" />
					<span>Rewards based on your professional activity</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-3.png" alt="Suitable Jobs" />
					<span>News, company reviews and more other informations about your interests</span>
				</li>
			</ul>
			<p>Join our professionals league and grow faster!</p>
		</div>	
	</div>
</div>
<div class="sign-in_2-lvl3">
	<div class="container">
		<div class="center-lvl3">
			<h1>We help you get a better job for a better life in Africa</h1>
			<a href="#">Let's start!</a>
		</div>
	</div>
</div>

<?php include('footer.php')	?>