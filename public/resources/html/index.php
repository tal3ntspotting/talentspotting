<?php include('header.php')	?>
<div class="hp-top-search">
	<div class="container-fluid">
		<div class="row">
			<div class="container  blue-center-search">
				<h1>We help you get a better job for a better life in Africa</h1>
				<div id="hp-tabs-search" class="col-md-12 col-lg-offset-1 col-lg-10 col-lg-offset-1" role="tabpanel">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#Jobs" aria-controls="home" role="tab" data-toggle="tab">Jobs</a></li>
				    <li role="presentation"><a href="#Skills" aria-controls="profile" role="tab" data-toggle="tab">Skills</a></li>
				    <li role="presentation"><a href="#Companies" aria-controls="messages" role="tab" data-toggle="tab">Companies</a></li>
				  </ul>
				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="Jobs">
				    	<form>
				    		
				    		<input type="search" class="col-md-4 col-lg-6" name="job-search" placeholder="Find jobs in programming">
				    		<select class="location-blue-search">
				    			<option disabled="disabled" selected="selected">Location</option>
				    			<option> option 1</option>
				    			<option> option 2</option>
				    			<option> option 3</option>
				    		</select> 
				    		<input type="submit" value=" " />
				    		<div class="clear"></div>
				    	</form>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="Skills">
				    	<form>
				    		<input type="submit" value=" " />
				    		<input type="search" class="col-md-4 col-lg-6" name="job-search" placeholder="Find jobs in programming">
				    		<select class="location-blue-search">
				    			<option disabled="disabled" selected="selected">Location</option>
				    			<option> option 1</option>
				    			<option> option 2</option>
				    			<option> option 3</option>
				    		</select> 
				    		<div class="clear"></div>
				    	</form>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="Companies">
				    	<form>
				    		<input type="submit" value=" " />
				    		<input type="search" class="col-md-4 col-lg-6" name="job-search" placeholder="Find jobs in programming">
				    		<select class="location-blue-search">
				    			<option disabled="disabled" selected="selected">Location</option>
				    			<option> option 1</option>
				    			<option> option 2</option>
				    			<option> option 3</option>
				    		</select> 
				    		<div class="clear"></div>
				    	</form>
				    </div> 
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="hp-body">
<div class="white-band">
	<div class="container">
		<div class="row info-bullets">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-1"><p>854</p></div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-2"><p>New Jobs this month</p></div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-1"><p>245</p></div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-2"><p>Hired professionals this month</p></div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-1"><p>+1000</p></div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 level-2"><p>Companies</p></div>
			</div>
		</div>
	</div>
</div>
	<div class="container">
		<div class="row">
			<div class="col-md-9 hp-body-job-offers col-xs-12">
				<div class="hp-offer-content">
				<?php
					for ($i=0; $i < 12 ; $i++) { 
						?>
							<div class="col-md-4 hp-body-single-offer col-sm-6">
								<div class="hp-offer-inner <?php if($i == 0){ echo 'highlight'; } ?>">
									<h2>Investors Relations Officer</h2>
									<h3>Lagos, Nigeria</h3>
									<img src="images/client-1.png" alt="Wema Bank" />
									<h4>Wema Bank</h4>
									<p>To pro-actively develop and manage Wema	Bank’s Investor relations strategy (locally and internationally), by building and maintaining</p>
									<hr />
									<ul>
										<li><a href="#">Recommend</a></li>
										<li class="middle-btn"><a href="#">Share</a></li>
										<li><a href="#">Apply</a></li>
									</ul>
								</div>					
							</div>		
						<?php
					}
				?>	
				</div>
				<div class="">
					<button class="btn hp-load-more"><span class="glyphicon glyphicon-menu-hamburger"></span> Load More</button>
				</div>
			</div>
			<div class="col-md-3 hp-body-sidebar col-xs-12">
				<div class="col-md-12 hp-sidebar-item hp-sidebar-news">
					<div class="hp-sidebar-title">News</div>
					<div class="hp-news-item-picture hp-sidebar-item-inner">
						<a href="#"><img src="images/news-photo-1.png" alt="africa-got-talent" /></a>
						<h3><a href="#">Africa got Talent:</a></h3>						
						<p><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also</a></p>
					</div>
					<div class="hp-news-item-no-picture hp-sidebar-item-inner">
						<h3><a href="#">Africa got Talent:</a></h3>
						<p><a href="#">Trall alalal allalal ala llala lalla lalla llfd sa lf gfsgfsd gfsdg erre ttttee.</a></p>
					</div>
				</div>
				<div class="col-md-12 hp-sidebar-item hp-siderbar-salary">
					<img src="images/salsay-survey-sidebar.jpg" alt="Salary Survery" class="img-responsive"/>
					<div class="hp-siderbar-salary-inner">
						<h3>Find how well you are paid!</h3>
						<h3>Compelte the WorkinAfrica <br /> Salary Survery</h3>
						<p><a class="btn" href="#">Complete the Survery</a></p>
					</div>
				</div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-test col-xs-12">
					<div class="hp-sidebar-title">Latest Tests</div>
					<div class="hp-sidebar-latest-test">
						<div class="hp-sidebar-latest-test-title">
							<img class="left" src="images/sidebar-test-icon.png" alt="icon-test-siderbar" />
							<h3 class="left">16Personalities</h3>
						</div>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
						</p>
						<a href="#" class="btn">Take the Test</a>
					</div>
				</div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-gamification col-xs-12">
					<div class="hp-sidebar-title">Earn Gamification Points</div>
					<div class="hp-sidebar-gamification-inner">
						<img src="images/gamification-icon.png" alt="gamification-icon" />
						<p>Get a concrete, accurate description of who you are, what you can do and what are the best jobs for you.</p>
						<a href="#" class="btn">Complete your profile</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>	
<?php include('footer.php')	?>