<?php include('header.php'); ?>
<div class="container">
	<div class="row">
		<h1 class="news-title">Trending news</h1>
	</div>
	<div class="row">
		<div class="news-list col-lg-9 col-md-9 col-sm-9 col-xs-12 ">
			<div class="news-listing">
			<a href="#" class="news-box">
				<div class="news-img">
					<img src="images/news-1.jpg" alt="title 1" />
				</div>
				<h2>5 Interview questions you must prepare for</h2>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
			</a>
			<a href="#"  class="news-box">
				<div class="news-img">
					<img src="images/news-2.jpg" alt="title 1" />
				</div>
				<h2>5 Interview questions you must prepare for</h2>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
			</a>
			<a href="#"  class="news-box">
				<div class="news-img">
					<img src="images/news-1.jpg" alt="title 1" />
				</div>
				<h2>5 Interview questions you must prepare for</h2>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
			</a>
			<a href="#"  class="news-box">
				<div class="news-img">
					<img src="images/news-2.jpg" alt="title 1" />
				</div>
				<h2>5 Interview questions you must prepare for</h2>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
			</a>
		</div>
			<div class="">
					<button class="btn hp-load-more"><span class="glyphicon glyphicon-menu-hamburger"></span> Load More</button>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 news-sidebar">
			<div class="categories">
				<div class="hp-sidebar-title">Categories</div>
				<ul>
					<li><a href="#">Bussines News</a></li>
					<li><a href="#">Voluntary News</a></li>
				</ul>
			</div>
			<div class="tab-content">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Popular</a></li>
				  </ul>
				  <div role="tabpanel" class="tab-pane fade in active" id="home">
				  	<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">5 Interview questions you must prepare for</h3>
						  </div>
						</div>			 
				  </div>
				  <div role="tabpanel" class="tab-pane fade" id="profile">
				  		<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>	
						<div class="media">
						  <div class="media-left media-middle">
						    <a href="#">
						      <img class="media-object" src="images/news-2.jpg" alt="post title">
						    </a>
						  </div>
						  <div class="media-body">
						    <h3 class="media-heading">Middle aligned media</h3>
						  </div>
						</div>			  		
				  </div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-test col-xs-12">
					<div class="hp-sidebar-title">Latest Tests</div>
					<div class="hp-sidebar-latest-test">
						<div class="hp-sidebar-latest-test-title">
							<img class="left" src="images/sidebar-test-icon.png" alt="icon-test-siderbar" />
							<h3 class="left">16Personalities</h3>
						</div>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
						</p>
						<a href="#" class="btn">Take the Test</a>
					</div>
				</div>
				<div class="col-md-12 hp-sidebar-item hp-sidebar-gamification col-xs-12">
					<div class="hp-sidebar-title">Earn Gamification Points</div>
					<div class="hp-sidebar-gamification-inner">
						<img src="images/gamification-icon.png" alt="gamification-icon" />
						<p>Get a concrete, accurate description of who you are, what you can do and what are the best jobs for you.</p>
						<a href="#" class="btn">Complete your profile</a>
					</div>
				</div>
			</div> 
	</div>
</div>
</div>
<?php include('footer.php'); ?>