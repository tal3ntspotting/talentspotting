<?php include('header.php')	?>
<div class="prices">
	<div class="container">
		<div class="row">
			<h1>Grow your business now!</h1>
			<div class="col-lg-12 col-md-10 col-sm-10 col-xs-12 free-month">
				<div class="col-lg-3 col-md-2 col-sm-2 col-xs-12 turcoaz">
					<h2><small>Get 30 days</small> <br/> DEMO</h2>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ligh-grey">
					<ul>
						<li>1 job posting</li>
						<li>See how many candidates applied</li>
						<li>See the general profile of the candidates</li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ligh-grey">
					<a href="#">Start your free mounth</a>
				</div>
			</div>
		</div>
		<div class="row price-plan-columns">
			<div id="pricing_plan" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 price-column">
				<div class="column-block-1">
					 <div class="wrap-outer">
					     <div class="wrap-inner">
					       <div class="wrap-center">
					See platform pricing plans >
						   </div>
					 </div>
				</div>
				</div>
				<div class="column-block-2">
					<ul>
						<li>Job Posting</li>
						<li>Database Search</li>
						<li>Included in newsletters to Professionals</li>
						<li>Highlighted Jobs</li>
						<li>Personalized and dedicated newsletters </li>
					</ul>
				</div>
			</div>
			<div id="starter_pack" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 price-column">
				<div class="column-block-1">
					<div class="pack-name">Starter Pack</div>
					<div class="pack-short">Great for short time projects </div>
					<div class="pack-icon">
						<img src="images/plan1.png" alt="Starter Pack Icon" />
					</div>
					<div class="pack-month">80 &#8364; / month </div>
					<div class="pack-year">870 &#8364; / year</div>
				</div>
				<div class="column-block-2">
					<ul>
						<li>3</li>
					    <li>15 CVs/month</li>
					    <li><img src="images/plan-x.png" alt="option not available" /></li>
					    <li><img src="images/plan-x.png" alt="option not available" /></li>
					    <li><img src="images/plan-x.png" alt="option not available" /></li>
					</ul>
				</div>
				<div class="column-block-3 turcoaz">
					<a href="#">Purchase this plan</a>
				</div>
			</div>
			<div id="nurturing_pack" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 price-column">
				<div class="promo-block">
					Most desired pack
				</div>
				<div class="column-block-1">
					<div class="pack-name">Nurturing Pack:</div>
					<div class="pack-short">Excelent for building teams</div>
					<div class="pack-icon">
						<img src="images/plan2.png" alt="Starter Pack Icon" />
					</div>
					<div class="pack-month">160 &#8364; / month </div>
					<div class="pack-year">1560 &#8364; / year</div>
				</div>
				<div class="column-block-2">
					<ul>
						<li>10</li>
					    <li>50 CVs/month</li>
					    <li><img src="images/plan-check.png" alt="check option" /></li>
					    <li><img src="images/plan-x.png" alt="option not available" /></li>
					    <li><img src="images/plan-x.png" alt="option not available" /></li>
					</ul>
				</div>
				<div class="column-block-3 turcoaz">
					<a href="#">Purchase this plan</a>
				</div>
			</div>
			<div id="growing_pack" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 price-column">
				<div class="column-block-1">
					<div class="pack-name">Growing Pack:</div>
					<div class="pack-short">Amazing for business growth</div>
					<div class="pack-icon">
						<img src="images/plan3.png" alt="Starter Pack Icon" />
					</div>
					<div class="pack-month">320 &#8364; / month </div>
					<div class="pack-year">2280 &#8364; / year</div>
				</div>
				<div class="column-block-2">
					<ul>
						<li>unlimited</li>
					    <li>150 CVs/month</li>
					    <li><img src="images/plan-check.png" alt="check option" /></li>
					    <li>5 / month</li>
					    <li>3 / month</li>
					</ul>
				</div>
				<div class="column-block-3 turcoaz">
					<a href="#">Purchase this plan</a>
				</div>
			</div>
		</div>
		<div class="row add_extra">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 dark-grey wrap-extra">
				<div class="extra">Add Extra</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 light-grey-1">
				<span class="small-title">Database Search</span>
				<div class="form-group">
				  <input type="number" class="form-control" id="quantity-cv"  placeholder="Quantity">
				</div>
				<div class="quantity-cv-result"><span>4</span>$/CV</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 light-grey-2">
				<span class="small-title">Highlighted Jobs</span>
				<div class="form-group">
				  <input type="number" class="form-control" id="quantity-job" placeholder="Quantity">
				</div>
				<div class="quantity-job-result"><span>6</span>$/job</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ligh-grey">
				<span class="small-title">Exclusive newsletters</span>
				<small>Starting from <br /> 320$ / newsletter</small>
				<a href="#" class="orange">Configure</a>
			</div>
		</div>
		<div class="row pricing-boxes">
			<h2>Personalized Services:</h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-contact">
				<div class="block-contact-1">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<h3>
							Because hiring is so complex and multi-faceted, let <br /> us personally help you in finding the right people.
						</h3>
						<div class="what-we-can-do">Here's what we can do:</div>
						<ul>
							<li>Promoting your Jobs to over 3.000.000 professionals</li>
							<li>CV Screening</li>
							<li>Candidate Ability Testing</li>
							<li>Assessment Centers</li>
							<li>Shortlisting the Top Professionals</li>
							<li>Recommendations</li> 
						</ul>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<img src="images/mail-box-pricing.png" alt="pricing mail" />
						<a class="orange" href="mailto:someone@example.com?Subject=Hello%20again" target="_top">Contact Us</a>
					</div>
				</div>
				<div class="block-contact-2">					
					We are happy to mention that we have made more than 50 successfull projects in<br />
					sub-Saharan Africa.<br />					
					<a href="#">CLICK HERE</a> to see an exemple of one of our successful projects<br />
					<strong>Let's meet!</strong>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 orange-contact">
				<div class="block-contact-1">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">  
						We believe in the power of people working together <br />
						for a great goal so we decided to support you in your <br />
						quest by offering free accces to the platform for <br />
						recruiting volunteers.<br />
						<br />
						Contact us to prepare the setup.
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<img src="images/mail-box-pricing.png" alt="pricing mail" />
						<a class="turcoaz" href="mailto:someone@example.com?Subject=Hello%20again" target="_top">Contact Us</a>
					</div>
				</div>
				<div class="block-contact-2">
					<strong>FREE for NGOs</strong>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php')	?>