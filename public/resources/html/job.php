<?php include('header.php')	?>
	<div class="container">
		<div class="row client-single">
			<div class="col-md-9">
				<div class="job-client-info col-md-12">
					<div class="col-md-12 job-cover-photo hidden-xs">
						<img src="images/cover-photo.jpg" alt="client-1-cover-photo" />
					</div>
					<div class="col-md-12">
						<div class="col-md-3 job-client-profile-photo col-xs-6">
							<img src="images/client-thumb-normal.jpg" alt="client-profile-photo" class="img-thumbnail">
							<div class="client-star-rating"></div>
							<span>Rate Wema Bank</span>
						</div>
						<div class="col-md-2 job-client-info-name col-xs-6">
							<div class="job-client">Wema Bank</div>
							<div class="job-industry">Industry: <span>Banking</span></div>
							<div class="job-country">Country: <span>Nigeria</span></div>
							<div class="city">City: <span>Langos</span></div>
						</div>
						<div class="col-md-3 col-xs-6 job-applications">
							<ul class="list-group">
							  <li class="list-group-item orange">
							    <span class="badge">28</span>
							    Recommendations
							  </li>
							  <li class="list-group-item turcoaz">
							    <span class="badge">14</span>
							    Applications
							  </li>
							  <li class="list-group-item gray">
							    Posted: 09 March 2015
							  </li>
							</ul>
						</div>
						<div class="col-md-4 job-short-description col-xs-12">
							<p>
								At Wema Bank, we cherish the value of relationships 
								and you have been a critical part of our resilience & 
								successes for over 60 years. Every interaction with 
								you helps us serve you better. 
								<br /><br />
								For more information, kindly log on to 
								www.wemabank.com
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 job-full-description">					
					<h1>Investors Relations Officer</h1>
					<ul class="social-client-list">
						<li><a class="facebook-icon" href="#"></a></li>
						<li><a class="linkedin-icon" href="#"></a></li>
						<li><a class="twitter-icon" href="#"></a></li>
						<li><a class="gplus-icon" href="#"></a></li>
					</ul>
					<h2>Job description</h2>
					<article>
						- Audit of all applications in the Bank.<br />
						- Reviewing established strategies that will provide assurance that the Bank’s IS resources are efficiently and effectively utilized.<br />
						- Reviewing established strategies that will provide assurance that the Bank’s IS resources are adequately protected.<br />
						- Develop and maintain audit programs to reflect changes in operations, technologies, risks, and audit standards or techniques.<br />
						- Assist in developing IS Audit year planner as well as planning individual audit assignment.<br />
						- Provide technical assistance to other Audit team members.<br />
						- Assist in the training of branch control officers on general IT controls.<br />
						- Work directly with IT staff to review/create/enhance procedures and controls.<br />
						- Perform tests of information system processes and controls, and maintain appropriate work papers.<br />
						- Assist with monitoring application development process for information systems projects.<br />
						- Act as Subject Matter Expert for assigned areas by developing a level of expertise, establishing and maintaining a program of Continuous Audit Monitoring of the area, directing the creation and maintenance of a Business Profile for the area, conducting follow-up of open audit findings until resolved.<br />
						- Perform audit assignments in Operating System.<br />
						- Review of Electronic Payment Operations<br />
						- Review of IT organizations and operations, design and Implementation of Electronic Payment Control and Database.<br />
						- Assist in investigation of IT related Irregularities, Abuses, and Frauds.<br />
						- Maximizing the potentials of ACL CAAT software for revenue assurance and financial/compliance auditing innovations.<br />
						- Assemble of work papers, providing first-level, formal quality appraisals of audit documentation, and reporting results and recommendations<br />
						- Reviewing the communications and network infrastructure.<br />
						- Review of Business Continuity Plan (BCP) and Disaster Recovery Plan (DRP)<br />
						- Review of computer hardware acquisition, maintenance and retirement.<br />
						- Audit of Database management system (DBMS)<br />
						- Review of software development, acquisition process and User Acceptance testing of new Systems.<br />
						- Review of contractual agreement with service providers/vendors.<br />
						- Review of physical and logical access security.<br />
						- Review of Information systems security policy.<br />
						- Other responsibility as may be assigned by the Head of IS Audit<br />
					</article>
					<a href="#" class="apply-to-job btn">Apply</a>
				</div>
				<div class="col-md-12 job-requirements">
					<h2>Requirements</h2>
					<div class="requirement-field">
						<h3>Competence Requirements:</h3>
						<p>
							- Good financial and accounting knowledge, including international accounting and financial reporting standards<br />
							- Appreciable understanding of the Nigerian banking system and good exposure to other international banking systems<br />
							- Oral and Written Communication Skill<br />
							- Good Customer Service / Relationship Management<br />
						</p>
					</div>
					<div class="requirement-field">
						<h3>Minimum Education Qualification:</h3>
						<p>
						- First Degree from a reputable University<br />
						- Masters and Professional Qualification will be of added advantage <br />
						</p>
					</div>
					<div class="requirement-field">
						<h3>Work Experience:</h3>
						<p>Minimum of 8 years’ experience in relevant field </p>
					</div>
				</div>
				<style>
					.job-locations  {background:#fff; border-radius:5px; margin-bottom:20px;}
					.job-locations ul {
						list-style-type:none;
						padding:0px;
					}
					.job-locations  ul li {
						background:#53bdb1;
						display:inline-block;
						margin-right:10px;
						color:#fff;
						border-radius:5px;
						overflow:hidden;
					}
					.job-locations  ul li .pin {
						float:left;
					}
					.job-locations  ul li img {
						display:block;
						max-width:30px;
						padding:7px;
						max-height:30px;
					}
					.job-locations ul li .pin-loc {
						  display: block;
						  float: left;
						  padding: 5px 8px;
					}
					
					
				</style>
				<div class="col-md-12 job-locations">
					<h3>Job Locations Availability</h3>
					<ul>
						<li><div class="pin"><img src="images/pin-location.png" alt="pin" /></div><span class="pin-loc">Nigeria</span></li>
						<li><div class="pin"><img src="images/pin-location.png" alt="pin" /></div><span class="pin-loc">Nigeria</span></li>
						<li><div class="pin"><img src="images/pin-location.png" alt="pin" /></div><span class="pin-loc">Nigeria</span></li>
						<li><div class="pin"><img src="images/pin-location.png" alt="pin" /></div><span class="pin-loc">Nigeria</span></li>
						<li><div class="pin"><img src="images/pin-location.png" alt="pin" /></div><span class="pin-loc">Nigeria</span></li>
					</ul>
				</div>
				<div class="col-md-12 job-skills">
					<h2>Skills</h2>
					<div class="skills">
						<ul>
								<li><span class="btn btn-info">java</span></li>
								<li><span class="btn btn-info">css</span></li>
								<li><span class="btn btn-info">html</span></li>								
								<li><span class="btn btn-info">project management</span></li>
								<li><span class="btn btn-info">time consumption</span></li>								
								<li><span class="btn btn-info">client service</span></li>
								<li><span class="btn btn-info">developer</span></li>
								<li><span class="btn btn-info">killing bugs</span></li>
								<li><span class="btn btn-info">slaying dragons</span></li>
								<li><span class="btn btn-info">ping ponies</span></li>
						</ul>
						<!--<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Java</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>HTML</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>CSS</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Javascript</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Training</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Team work</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Wordpress</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Magento</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Time Management</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Drupal</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Zend</span></div>
						<div class="col-md-3 col-md-offset-1 col-sm-5"><span>Code Igniter</span></div> -->
					</div>
				</div>
				<div class="job-personal-reviews col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2>Wema Bank <span class="color-turcoaz">Reviews</span></h2>
					<?php
						for ($i=0; $i < 2 ; $i++) { 
							?>
								<div class="job-testimonial">
									<div class="col-lg-1 col-md-1 col-sm-1 col-xs-3 job-testimonial-image">
										<img src="images/default-account-image.png" alt="Account Image" class="img-circle">
									</div>		
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-9 job-testimonial-details">
										<h3>Awesome experience; great people</h3>
										<div class="personal-rating" rel="3.5"></div><!-- use rel attribute to set star rating value, values 1-5 -->
										<div class="details">
											Worked at <a href="#">Wema Bank</a> as an <a href="#">Marketing Specialist</a> for <a href="#">2 years</a> and <a href="#">11 months </a>
										</div>
									</div>	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 job-testimonial-content">
										Focus on people, encouraging initiative, passion. product and customers are key for all teams, focused on being 
										agile and flexible. love the people, fast growing, 
										continuous learning.
										As with any fast growing environment, 
										wanting to do too much, too fast can be frustrating but 
										also a challenge.
									</div>
								</div>	
							<?php
						}
					?>		
			     </div>
			</div>
			<div class="col-md-3 job-sidebar">
				<div class="job-alert-box job-sidebar-box">
					<span class="job-cta">Get updates for jobs like this</span>
					<button class="btn">Create Alert</button>
				</div>
				<div class="job-recommend-box job-sidebar-box">
					<span class="job-cta">Recommend your friends and earn prizes</span>
					<button class="btn">Recommend</button>
				</div>
				<div class="job-company-criteria-box job-sidebar-box">
					<span class="job-company-rating">Company Info</span>
					<span class="job-company-criteria">Website</span>
					<a href="#" class="job-company-criteria-answer">www.webmabank.co.za</a>
					<span class="job-company-criteria">Size</span>
					<span class="job-company-criteria-answer">100-500 Employees</span>
					<span class="job-company-criteria">Industry</span>
					<span class="job-company-criteria-answer">Banking</span>
					<span class="job-company-criteria">Work Enviroment</span>
					<div class="job-star-rating-enviroment job-rating-star" rel="3"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Career Opportunities</span>
					<div class="job-star-rating-career job-rating-star" rel="1"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Comp &#38; Benefits</span>
					<div class="job-star-rating-benefits job-rating-star" rel="4"></div><!-- use rel attribute to set star rating value, values 1-5 -->
					<span class="job-company-criteria">Work-Life Balance</span>
					<div class="job-star-rating-balance job-rating-star" rel="5"></div><!-- use rel attribute to set star rating value, values 1-5 -->
				</div>
				<div class="job-similar-box job-sidebar-box col-md-12">
					<span class="job-title-box">Similar Jobs</span>
					<?php  
						for ($i=0; $i < 10 ; $i++) { ?>
							<div class="job-single-similar col-md-12 col-sm-6 col-xs-12">
								<a href="#">
									<span class="job-thumbnail-small"><img src="images/job-icon-small.png" alt="Firm <?php echo $i; ?>" /></span>
									<span class="job-position-similar">Investors Relations Officer</span>
									<span class="job-client-similar">Wema Bank - Nigeria, Lagos</span>
								</a>
							</div>
						<?php } ?>					
				</div>
			</div>
		</div>
	</div>
<?php include('footer.php')	?>