<?php include('header.php'); ?>
<div class="hp-body">
	<style>

	</style>
		<div class="jobs-main-keyword">
			<div class="container">
				<div class="row">
					<div class="cian-box-keyword blue-center-search">
						<h1>You searched for the keyword : <span>Programming</span></h1>
						<input type="text" placeholder="Keyword" name="keyword-search" />
						<a href="#">
							<svg enable-background="new 0 0 512 512" height="30px" id="Layer_1" version="1.1" viewBox="0 0 512 512" width="30px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M226.057,105.557v61.918v19.701l19.701,0.194c136.853,1.321,213.945,62.191,234.025,185.542  c-75.179-86.962-161.868-87-233.821-87.02h-19.905v19.895v61.025L41.634,236.175L226.057,105.557 M245.952,67.07L7.216,236.175  l238.736,169.105v-99.493c95.549,0.039,184.133,4.857,259.048,138.447c0-84.844-14.018-274.387-259.048-276.758V67.07L245.952,67.07  z" fill="#37404D"/></svg>
						</a>
					</div>
				</div>
			</div>
		</div>
        <div class="white-band">
            <div class="container">
                <div class="row hp-filter-area">
                    <form class="col-md-12 hidden-xs desktop-select" method="POST" action="/search">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 right">
                                    <input name="search" type="text" value="" placeholder="Search" class="form-control">
                                </div>
                                <br><br>
                            </div>
                        </div>
                        <select rel="id_city" class="col-md-2 col-xs-12 col-sm-2 select2-hidden-accessible" name="id_city" tabindex="-1" aria-hidden="true">
                            <option value="">Cities</option>
                                                        <option title="id_city" value="22">Abia</option>
                                                        <option title="id_city" value="3">Abidjan</option>
                                                        <option title="id_city" value="16">Abuja</option>
                                                        <option title="id_city" value="9">Accra</option>
                                                        <option title="id_city" value="23">Adamawa</option>
                                                        <option title="id_city" value="24">Akwa Ibom</option>
                                                        <option title="id_city" value="25">Anambra</option>
                                                        <option title="id_city" value="13">Bamako</option>
                                                        <option title="id_city" value="26">Bauchi</option>
                                                        <option title="id_city" value="27">Bayelsa</option>
                                                        <option title="id_city" value="28">Benue</option>
                                                        <option title="id_city" value="15">Bertoua</option>
                                                        <option title="id_city" value="18">Borno</option>
                                                        <option title="id_city" value="56">Chipata</option>
                                                        <option title="id_city" value="59">Chisamba</option>
                                                        <option title="id_city" value="60">Copperbelt</option>
                                                        <option title="id_city" value="29">Cross River</option>
                                                        <option title="id_city" value="5">Dakar</option>
                                                        <option title="id_city" value="30">Delta</option>
                                                        <option title="id_city" value="7">Dodoma</option>
                                                        <option title="id_city" value="4">Douala</option>
                                                        <option title="id_city" value="20">Ebonyi</option>
                                                        <option title="id_city" value="31">Edo</option>
                                                        <option title="id_city" value="32">Ekiti</option>
                                                        <option title="id_city" value="21">Enugu</option>
                                                        <option title="id_city" value="33">Gombe</option>
                                                        <option title="id_city" value="34">Imo</option>
                                                        <option title="id_city" value="35">Jigawa</option>
                                                        <option title="id_city" value="54">Kabwe</option>
                                                        <option title="id_city" value="36">Kaduna</option>
                                                        <option title="id_city" value="11">Kampala</option>
                                                        <option title="id_city" value="37">Kano</option>
                                                        <option title="id_city" value="58">Katete &amp; Lundazi District</option>
                                                        <option title="id_city" value="38">Katsina</option>
                                                        <option title="id_city" value="39">Kebbi</option>
                                                        <option title="id_city" value="53">Kitwe</option>
                                                        <option title="id_city" value="40">Kogi</option>
                                                        <option title="id_city" value="41">Kwara</option>
                                                        <option title="id_city" value="6">Lagos</option>
                                                        <option title="id_city" value="8">Lusaka</option>
                                                        <option title="id_city" value="57">Lusaka/Ndola</option>
                                                        <option title="id_city" value="12">Maputo</option>
                                                        <option title="id_city" value="52">Maroua</option>
                                                        <option title="id_city" value="10">Nairobi</option>
                                                        <option title="id_city" value="42">Nassarawa</option>
                                                        <option title="id_city" value="55">Ndola</option>
                                                        <option title="id_city" value="43">Niger</option>
                                                        <option title="id_city" value="44">Ogun</option>
                                                        <option title="id_city" value="45">Ondo</option>
                                                        <option title="id_city" value="46">Osun</option>
                                                        <option title="id_city" value="19">Oyo</option>
                                                        <option title="id_city" value="47">Plateau</option>
                                                        <option title="id_city" value="17">Rivers</option>
                                                        <option title="id_city" value="48">Sokoto</option>
                                                        <option title="id_city" value="61">Solwezi</option>
                                                        <option title="id_city" value="51">Taraba</option>
                                                        <option title="id_city" value="14">Yaounde</option>
                                                        <option title="id_city" value="49">Yobe</option>
                                                        <option title="id_city" value="50">Zamfara</option>
                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 190px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_city-9e-container"><span class="select2-selection__rendered" id="select2-id_city-9e-container" title="Cities">Cities</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <select rel="id_department" class="col-md-2 col-xs-12 col-sm-2 select2-hidden-accessible" name="id_department[]" tabindex="-1" aria-hidden="true">
                            <option value="">Departments</option>
                                                        <option title="id_department" value="12">Accounting - Finance</option>
                                                        <option title="id_department" value="5">Acquisition - Logistics - Procurement</option>
                                                        <option title="id_department" value="35">Administratif</option>
                                                        <option title="id_department" value="6">Administrative</option>
                                                        <option title="id_department" value="30">Advertising</option>
                                                        <option title="id_department" value="7">Agriculture</option>
                                                        <option title="id_department" value="41">Analyst / Quality Control</option>
                                                        <option title="id_department" value="8">Architecture - Design</option>
                                                        <option title="id_department" value="40">Audit</option>
                                                        <option title="id_department" value="9">Banking</option>
                                                        <option title="id_department" value="10">Chemistry</option>
                                                        <option title="id_department" value="38">Civil Engineering</option>
                                                        <option title="id_department" value="46">Commerce</option>
                                                        <option title="id_department" value="42">Communication</option>
                                                        <option title="id_department" value="11">Construction</option>
                                                        <option title="id_department" value="25">Consulting</option>
                                                        <option title="id_department" value="39">Customer Service</option>
                                                        <option title="id_department" value="13">Customer support</option>
                                                        <option title="id_department" value="43">Editorial</option>
                                                        <option title="id_department" value="14">Education</option>
                                                        <option title="id_department" value="19">Engineering</option>
                                                        <option title="id_department" value="50">Environment</option>
                                                        <option title="id_department" value="53">food &amp; beverages</option>
                                                        <option title="id_department" value="47">Hospitality</option>
                                                        <option title="id_department" value="4">Human Resources</option>
                                                        <option title="id_department" value="18">Import - Export</option>
                                                        <option title="id_department" value="45">Insurance</option>
                                                        <option title="id_department" value="20">Internet - eCommerce </option>
                                                        <option title="id_department" value="21">Internship</option>
                                                        <option title="id_department" value="22">IT Hardware</option>
                                                        <option title="id_department" value="23">IT Software</option>
                                                        <option title="id_department" value="44">Legal</option>
                                                        <option title="id_department" value="24">Legal</option>
                                                        <option title="id_department" value="1">Management</option>
                                                        <option title="id_department" value="26">Management Trainee</option>
                                                        <option title="id_department" value="27">Marketing</option>
                                                        <option title="id_department" value="28">Media</option>
                                                        <option title="id_department" value="36">Medical</option>
                                                        <option title="id_department" value="16">NGOs</option>
                                                        <option title="id_department" value="15">Pharmacy</option>
                                                        <option title="id_department" value="29">Production</option>
                                                        <option title="id_department" value="31">Public Relations</option>
                                                        <option title="id_department" value="17">Real estate</option>
                                                        <option title="id_department" value="49">Research</option>
                                                        <option title="id_department" value="3">Sales</option>
                                                        <option title="id_department" value="37">Secretariat</option>
                                                        <option title="id_department" value="51">Supervising</option>
                                                        <option title="id_department" value="52">Technical</option>
                                                        <option title="id_department" value="32">Telecom</option>
                                                        <option title="id_department" value="34">Tourism</option>
                                                        <option title="id_department" value="33">Transportation</option>
                                                        <option title="id_department" value="48">Vocational Trade and Services</option>
                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 190px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_department[]-j4-container"><span class="select2-selection__rendered" id="select2-id_department[]-j4-container" title="Departments">Departments</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <select rel="id_industry" class="col-md-2 col-xs-12 col-sm-2 select2-hidden-accessible" name="id_industry" tabindex="-1" aria-hidden="true">
                            <option value="">Industries</option>
                                                        <option title="id_industry" value="38">Advertising Agency</option>
                                                        <option title="id_industry" value="3">Agences de recrutement</option>
                                                        <option title="id_industry" value="4">Agriculture</option>
                                                        <option title="id_industry" value="25">Audit</option>
                                                        <option title="id_industry" value="5">Auto</option>
                                                        <option title="id_industry" value="6">Autres</option>
                                                        <option title="id_industry" value="7">Bancaire - services financiers</option>
                                                        <option title="id_industry" value="43">Beverage</option>
                                                        <option title="id_industry" value="44">Chemical</option>
                                                        <option title="id_industry" value="39">Commerce</option>
                                                        <option title="id_industry" value="8">Commerce au detail</option>
                                                        <option title="id_industry" value="9">Conseil</option>
                                                        <option title="id_industry" value="10">Construction</option>
                                                        <option title="id_industry" value="33">Consulting</option>
                                                        <option title="id_industry" value="26">diplomatic</option>
                                                        <option title="id_industry" value="36">e-Commerce</option>
                                                        <option title="id_industry" value="11">Education</option>
                                                        <option title="id_industry" value="30">Energétique</option>
                                                        <option title="id_industry" value="12">Energie &amp; Huile</option>
                                                        <option title="id_industry" value="34">Events Planning</option>
                                                        <option title="id_industry" value="28">FMCG</option>
                                                        <option title="id_industry" value="42">Food Service</option>
                                                        <option title="id_industry" value="37">Home Care</option>
                                                        <option title="id_industry" value="40">Hospitality</option>
                                                        <option title="id_industry" value="45">Human Resources</option>
                                                        <option title="id_industry" value="31">ICT / Telecommunications</option>
                                                        <option title="id_industry" value="13">Immobilier</option>
                                                        <option title="id_industry" value="47">Information Technology and Services</option>
                                                        <option title="id_industry" value="14">Ingenierie</option>
                                                        <option title="id_industry" value="35">Insurance</option>
                                                        <option title="id_industry" value="1">IT&amp;C</option>
                                                        <option title="id_industry" value="16">Logistique</option>
                                                        <option title="id_industry" value="46">Manufacturing</option>
                                                        <option title="id_industry" value="24">Medical</option>
                                                        <option title="id_industry" value="41">Military</option>
                                                        <option title="id_industry" value="48">Mining</option>
                                                        <option title="id_industry" value="50">Oil Industry</option>
                                                        <option title="id_industry" value="17">ONG</option>
                                                        <option title="id_industry" value="49">Pastry</option>
                                                        <option title="id_industry" value="18">Produits Pharmaceutiques</option>
                                                        <option title="id_industry" value="19">Produits/biens de grande consommation</option>
                                                        <option title="id_industry" value="20">Publicite &amp; Medias</option>
                                                        <option title="id_industry" value="27">Retail / Wholesales</option>
                                                        <option title="id_industry" value="21">Services &amp; BPO</option>
                                                        <option title="id_industry" value="22">Technique</option>
                                                        <option title="id_industry" value="23">Tourisme</option>
                                                        <option title="id_industry" value="29">Transportation</option>
                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 190px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_industry-34-container"><span class="select2-selection__rendered" id="select2-id_industry-34-container" title="Industries">Industries</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <select rel="career_level" class="col-md-2 col-xs-12 col-sm-2 select2-hidden-accessible" name="career_level" tabindex="-1" aria-hidden="true">
                            <option value="">Career level</option>
                                                        <option title="career_level" value="1">Junior</option>
                                                        <option title="career_level" value="2">Middle</option>
                                                        <option title="career_level" value="3">Senior</option>
                                                        <option title="career_level" value="4">Management</option>
                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 190px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-career_level-dm-container"><span class="select2-selection__rendered" id="select2-career_level-dm-container" title="Career level">Career level</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <select rel="job_type" class="col-md-2 col-xs-12 col-sm-2 select2-hidden-accessible" name="job_type" tabindex="-1" aria-hidden="true">
                            <option value="">Job type</option>
                                                        <option title="job_type" value="1">Part Time</option>
                                                        <option title="job_type" value="2">Full Time</option>
                                                        <option title="job_type" value="3">Seasonal</option>
                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 190px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-job_type-cb-container"><span class="select2-selection__rendered" id="select2-job_type-cb-container" title="Job type">Job type</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <input type="submit" value="Filter" class="col-md-1 btn right hp-filter-btn col-xs-12 col-sm-1">
                    </form>
                    <form class="col-md-12 visible-xs" method="POST" action="/search">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 right">
                                    <input name="search" type="text" value="" placeholder="Search" class="form-control">
                                </div>
                                <br><br>
                            </div>
                        </div>
                        <select rel="id_city" multiple="" class="col-md-2 col-xs-12 col-sm-2" name="id_city[]">
                            <option value="">Cities</option>
                                                        <option value="22" title="id_city">Abia</option>
                                                        <option value="3" title="id_city">Abidjan</option>
                                                        <option value="16" title="id_city">Abuja</option>
                                                        <option value="9" title="id_city">Accra</option>
                                                        <option value="23" title="id_city">Adamawa</option>
                                                        <option value="24" title="id_city">Akwa Ibom</option>
                                                        <option value="25" title="id_city">Anambra</option>
                                                        <option value="13" title="id_city">Bamako</option>
                                                        <option value="26" title="id_city">Bauchi</option>
                                                        <option value="27" title="id_city">Bayelsa</option>
                                                        <option value="28" title="id_city">Benue</option>
                                                        <option value="15" title="id_city">Bertoua</option>
                                                        <option value="18" title="id_city">Borno</option>
                                                        <option value="56" title="id_city">Chipata</option>
                                                        <option value="59" title="id_city">Chisamba</option>
                                                        <option value="60" title="id_city">Copperbelt</option>
                                                        <option value="29" title="id_city">Cross River</option>
                                                        <option value="5" title="id_city">Dakar</option>
                                                        <option value="30" title="id_city">Delta</option>
                                                        <option value="7" title="id_city">Dodoma</option>
                                                        <option value="4" title="id_city">Douala</option>
                                                        <option value="20" title="id_city">Ebonyi</option>
                                                        <option value="31" title="id_city">Edo</option>
                                                        <option value="32" title="id_city">Ekiti</option>
                                                        <option value="21" title="id_city">Enugu</option>
                                                        <option value="33" title="id_city">Gombe</option>
                                                        <option value="34" title="id_city">Imo</option>
                                                        <option value="35" title="id_city">Jigawa</option>
                                                        <option value="54" title="id_city">Kabwe</option>
                                                        <option value="36" title="id_city">Kaduna</option>
                                                        <option value="11" title="id_city">Kampala</option>
                                                        <option value="37" title="id_city">Kano</option>
                                                        <option value="58" title="id_city">Katete &amp; Lundazi District</option>
                                                        <option value="38" title="id_city">Katsina</option>
                                                        <option value="39" title="id_city">Kebbi</option>
                                                        <option value="53" title="id_city">Kitwe</option>
                                                        <option value="40" title="id_city">Kogi</option>
                                                        <option value="41" title="id_city">Kwara</option>
                                                        <option value="6" title="id_city">Lagos</option>
                                                        <option value="8" title="id_city">Lusaka</option>
                                                        <option value="57" title="id_city">Lusaka/Ndola</option>
                                                        <option value="12" title="id_city">Maputo</option>
                                                        <option value="52" title="id_city">Maroua</option>
                                                        <option value="10" title="id_city">Nairobi</option>
                                                        <option value="42" title="id_city">Nassarawa</option>
                                                        <option value="55" title="id_city">Ndola</option>
                                                        <option value="43" title="id_city">Niger</option>
                                                        <option value="44" title="id_city">Ogun</option>
                                                        <option value="45" title="id_city">Ondo</option>
                                                        <option value="46" title="id_city">Osun</option>
                                                        <option value="19" title="id_city">Oyo</option>
                                                        <option value="47" title="id_city">Plateau</option>
                                                        <option value="17" title="id_city">Rivers</option>
                                                        <option value="48" title="id_city">Sokoto</option>
                                                        <option value="61" title="id_city">Solwezi</option>
                                                        <option value="51" title="id_city">Taraba</option>
                                                        <option value="14" title="id_city">Yaounde</option>
                                                        <option value="49" title="id_city">Yobe</option>
                                                        <option value="50" title="id_city">Zamfara</option>
                                                    </select>
                        <select rel="id_department" multiple="" class="col-md-2 col-xs-12 col-sm-2" name="id_department[]">
                            <option value="">Departments</option>
                                                        <option title="id_department" value="12">Accounting - Finance</option>
                                                        <option title="id_department" value="5">Acquisition - Logistics - Procurement</option>
                                                        <option title="id_department" value="35">Administratif</option>
                                                        <option title="id_department" value="6">Administrative</option>
                                                        <option title="id_department" value="30">Advertising</option>
                                                        <option title="id_department" value="7">Agriculture</option>
                                                        <option title="id_department" value="41">Analyst / Quality Control</option>
                                                        <option title="id_department" value="8">Architecture - Design</option>
                                                        <option title="id_department" value="40">Audit</option>
                                                        <option title="id_department" value="9">Banking</option>
                                                        <option title="id_department" value="10">Chemistry</option>
                                                        <option title="id_department" value="38">Civil Engineering</option>
                                                        <option title="id_department" value="46">Commerce</option>
                                                        <option title="id_department" value="42">Communication</option>
                                                        <option title="id_department" value="11">Construction</option>
                                                        <option title="id_department" value="25">Consulting</option>
                                                        <option title="id_department" value="39">Customer Service</option>
                                                        <option title="id_department" value="13">Customer support</option>
                                                        <option title="id_department" value="43">Editorial</option>
                                                        <option title="id_department" value="14">Education</option>
                                                        <option title="id_department" value="19">Engineering</option>
                                                        <option title="id_department" value="50">Environment</option>
                                                        <option title="id_department" value="53">food &amp; beverages</option>
                                                        <option title="id_department" value="47">Hospitality</option>
                                                        <option title="id_department" value="4">Human Resources</option>
                                                        <option title="id_department" value="18">Import - Export</option>
                                                        <option title="id_department" value="45">Insurance</option>
                                                        <option title="id_department" value="20">Internet - eCommerce </option>
                                                        <option title="id_department" value="21">Internship</option>
                                                        <option title="id_department" value="22">IT Hardware</option>
                                                        <option title="id_department" value="23">IT Software</option>
                                                        <option title="id_department" value="44">Legal</option>
                                                        <option title="id_department" value="24">Legal</option>
                                                        <option title="id_department" value="1">Management</option>
                                                        <option title="id_department" value="26">Management Trainee</option>
                                                        <option title="id_department" value="27">Marketing</option>
                                                        <option title="id_department" value="28">Media</option>
                                                        <option title="id_department" value="36">Medical</option>
                                                        <option title="id_department" value="16">NGOs</option>
                                                        <option title="id_department" value="15">Pharmacy</option>
                                                        <option title="id_department" value="29">Production</option>
                                                        <option title="id_department" value="31">Public Relations</option>
                                                        <option title="id_department" value="17">Real estate</option>
                                                        <option title="id_department" value="49">Research</option>
                                                        <option title="id_department" value="3">Sales</option>
                                                        <option title="id_department" value="37">Secretariat</option>
                                                        <option title="id_department" value="51">Supervising</option>
                                                        <option title="id_department" value="52">Technical</option>
                                                        <option title="id_department" value="32">Telecom</option>
                                                        <option title="id_department" value="34">Tourism</option>
                                                        <option title="id_department" value="33">Transportation</option>
                                                        <option title="id_department" value="48">Vocational Trade and Services</option>
                                                    </select>
                        <select rel="id_industry" multiple="" class="col-md-2 col-xs-12 col-sm-2" name="id_industry[]">
                            <option value="">Industries</option>
                                                        <option title="id_industry" value="38">Advertising Agency</option>
                                                        <option title="id_industry" value="3">Agences de recrutement</option>
                                                        <option title="id_industry" value="4">Agriculture</option>
                                                        <option title="id_industry" value="25">Audit</option>
                                                        <option title="id_industry" value="5">Auto</option>
                                                        <option title="id_industry" value="6">Autres</option>
                                                        <option title="id_industry" value="7">Bancaire - services financiers</option>
                                                        <option title="id_industry" value="43">Beverage</option>
                                                        <option title="id_industry" value="44">Chemical</option>
                                                        <option title="id_industry" value="39">Commerce</option>
                                                        <option title="id_industry" value="8">Commerce au detail</option>
                                                        <option title="id_industry" value="9">Conseil</option>
                                                        <option title="id_industry" value="10">Construction</option>
                                                        <option title="id_industry" value="33">Consulting</option>
                                                        <option title="id_industry" value="26">diplomatic</option>
                                                        <option title="id_industry" value="36">e-Commerce</option>
                                                        <option title="id_industry" value="11">Education</option>
                                                        <option title="id_industry" value="30">Energétique</option>
                                                        <option title="id_industry" value="12">Energie &amp; Huile</option>
                                                        <option title="id_industry" value="34">Events Planning</option>
                                                        <option title="id_industry" value="28">FMCG</option>
                                                        <option title="id_industry" value="42">Food Service</option>
                                                        <option title="id_industry" value="37">Home Care</option>
                                                        <option title="id_industry" value="40">Hospitality</option>
                                                        <option title="id_industry" value="45">Human Resources</option>
                                                        <option title="id_industry" value="31">ICT / Telecommunications</option>
                                                        <option title="id_industry" value="13">Immobilier</option>
                                                        <option title="id_industry" value="47">Information Technology and Services</option>
                                                        <option title="id_industry" value="14">Ingenierie</option>
                                                        <option title="id_industry" value="35">Insurance</option>
                                                        <option title="id_industry" value="1">IT&amp;C</option>
                                                        <option title="id_industry" value="16">Logistique</option>
                                                        <option title="id_industry" value="46">Manufacturing</option>
                                                        <option title="id_industry" value="24">Medical</option>
                                                        <option title="id_industry" value="41">Military</option>
                                                        <option title="id_industry" value="48">Mining</option>
                                                        <option title="id_industry" value="50">Oil Industry</option>
                                                        <option title="id_industry" value="17">ONG</option>
                                                        <option title="id_industry" value="49">Pastry</option>
                                                        <option title="id_industry" value="18">Produits Pharmaceutiques</option>
                                                        <option title="id_industry" value="19">Produits/biens de grande consommation</option>
                                                        <option title="id_industry" value="20">Publicite &amp; Medias</option>
                                                        <option title="id_industry" value="27">Retail / Wholesales</option>
                                                        <option title="id_industry" value="21">Services &amp; BPO</option>
                                                        <option title="id_industry" value="22">Technique</option>
                                                        <option title="id_industry" value="23">Tourisme</option>
                                                        <option title="id_industry" value="29">Transportation</option>
                                                    </select>
                        <select rel="career_level" multiple="" class="col-md-2 col-xs-12 col-sm-2" name="career_level[]">
                            <option value="">Career level</option>
                                                        <option title="career_level" value="1">Junior</option>
                                                        <option title="career_level" value="2">Middle</option>
                                                        <option title="career_level" value="3">Senior</option>
                                                        <option title="career_level" value="4">Management</option>
                                                    </select>
                        <select rel="job_type" multiple="" class="col-md-2 col-xs-12 col-sm-2" name="job_type[]">
                            <option value="">Job type</option>
                                                        <option title="job_type" value="1">Part Time</option>
                                                        <option title="job_type" value="2">Full Time</option>
                                                        <option title="job_type" value="3">Seasonal</option>
                                                    </select>
                        <input type="submit" value="Filter" class="col-md-1 btn right hp-filter-btn col-xs-12 col-sm-1">
                    </form>
                    <div class="clearfix"><br></div>
                    <div class="container">
                        <table class="table table-bordered table-tags" style="display: table;">
                            <thead>
                                <tr><th>City</th>
                                <th>Department</th>
                                <th>Industry</th>
                                <th>Career level</th>
                                <th>Type of job</th>
                            </tr></thead>
                            <tbody><tr class="active">
                                <td id="id_city">
                                                                    </td>                
                                <td id="id_department">
                                                                    </td>
                                <td id="id_industry">
                                                                    </td>
                                <td id="career_level">
                                                                    </td>
                                <td id="job_type">
                                                                    </td>
                            </tr>
                        </tbody></table>
                        <ul class="js-event-log"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9 hp-body-job-offers col-xs-12">
                    <div class="hp-offer-content">
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner highlight">
                                <h2><a href="/jobs/765/senior-engineer/microsoft-romania">Senior Engineer</a></h2>
                                <h3>Abia, Nigeria</h3>
                                <a href="/company/microsoft-romania"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8xXzQwNC5wbmc=&amp;width=180&amp;height=53" alt="workinafrica Senior Engineer Microsoft Romania"></a>
                                <h4><a href="/company/microsoft-romania">Microsoft Romania</a></h4>
                                <p>The most talented engineer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/senior-engineer?id=765&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/senior-engineer" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/senior-engineer?id=765&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner highlight">
                                <h2><a href="/jobs/16/head-purple-connect/wema-bank">Head, Purple Connect</a></h2>
                                <h3>Lagos, Nigeria</h3>
                                <a href="/company/wema-bank"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8yNF8yNF9XZW1hIEJhbmsucG5n&amp;width=180&amp;height=53" alt="workinafrica Head, Purple Connect Wema Bank"></a>
                                <h4><a href="/company/wema-bank">Wema Bank</a></h4>
                                <p>Provision of leadership, strategic direction, administration in the drive to living the bank’s vision on </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/head-purple-connect?id=16&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/head-purple-connect" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/head-purple-connect?id=16&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner highlight">
                                <h2><a href="/jobs/15/investors-relations-officer/wema-bank">Investors Relations Officer</a></h2>
                                <h3>Lagos, Nigeria</h3>
                                <a href="/company/wema-bank"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8yNF8yNF9XZW1hIEJhbmsucG5n&amp;width=180&amp;height=53" alt="workinafrica Investors Relations Officer Wema Bank"></a>
                                <h4><a href="/company/wema-bank">Wema Bank</a></h4>
                                <p>To pro-actively develop and manage Wema Bank’s Investor relations strategy (locally and internationally), </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/investors-relations-officer?id=15&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/investors-relations-officer" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/investors-relations-officer?id=15&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner highlight">
                                <h2><a href="/jobs/17/is-audit-officer/wema-bank">IS Audit Officer</a></h2>
                                <h3>Lagos, Nigeria</h3>
                                <a href="/company/wema-bank"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8yNF8yNF9XZW1hIEJhbmsucG5n&amp;width=180&amp;height=53" alt="workinafrica IS Audit Officer Wema Bank"></a>
                                <h4><a href="/company/wema-bank">Wema Bank</a></h4>
                                <p>Provision of leadership, strategic direction, administration in the drive to living the bank’s vision on </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/is-audit-officer?id=17&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/is-audit-officer" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/is-audit-officer?id=17&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner highlight">
                                <h2><a href="/jobs/18/head-project-finance-officer/wema-bank">Head, Project Finance Officer</a></h2>
                                <h3>Lagos, Nigeria</h3>
                                <a href="/company/wema-bank"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8yNF8yNF9XZW1hIEJhbmsucG5n&amp;width=180&amp;height=53" alt="workinafrica Head, Project Finance Officer Wema Bank"></a>
                                <h4><a href="/company/wema-bank">Wema Bank</a></h4>
                                <p>To drive the Bank’s profitability through creating viable risk assets from large ticket transactions financing. </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/head-project-finance-officer?id=18&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/head-project-finance-officer" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/head-project-finance-officer?id=18&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/755/manager-group-life-business/focus-life-assurance">Manager Group Life Business </a></h2>
                                <h3>Lusaka, Zambia</h3>
                                <a href="/company/focus-life-assurance"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC8zODdfRm9jdXMtRmluYW5jaWFsLVNlcnZpY2VzLmpwZw==&amp;width=180&amp;height=53" alt="workinafrica Manager Group Life Business  Focus Life Assurance"></a>
                                <h4><a href="/company/focus-life-assurance">Focus Life Assurance</a></h4>
                                <p>Focus Ironclad Group was incorporated to provide world class financial solutions to Small and Medium Scale </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/manager-group-life-business?id=755&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/manager-group-life-business" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/manager-group-life-business?id=755&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/756/assistant-accountants/national-milling-corporation">Assistant Accountants</a></h2>
                                <h3>Lusaka, Zambia</h3>
                                <a href="/company/national-milling-corporation"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MDlfZG93bmxvYWQuanBn&amp;width=180&amp;height=53" alt="workinafrica Assistant Accountants National Milling Corporation"></a>
                                <h4><a href="/company/national-milling-corporation">National Milling Corporation</a></h4>
                                <p>National Milling Corporation Limited has been in existence since the pre-independence era in 1964, operating </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/assistant-accountants?id=756&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/assistant-accountants" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/assistant-accountants?id=756&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/757/chief-accountant/national-milling-corporation">Chief Accountant</a></h2>
                                <h3>Kitwe, Zambia</h3>
                                <a href="/company/national-milling-corporation"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MDlfZG93bmxvYWQuanBn&amp;width=180&amp;height=53" alt="workinafrica Chief Accountant National Milling Corporation"></a>
                                <h4><a href="/company/national-milling-corporation">National Milling Corporation</a></h4>
                                <p>National Milling Corporation Limited has been in existence since the pre-independence era in 1964, operating </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/chief-accountant?id=757&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/chief-accountant" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/chief-accountant?id=757&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/758/management-accountant/national-milling-corporation">Management Accountant</a></h2>
                                <h3>Lusaka, Zambia</h3>
                                <a href="/company/national-milling-corporation"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MDlfZG93bmxvYWQuanBn&amp;width=180&amp;height=53" alt="workinafrica Management Accountant National Milling Corporation"></a>
                                <h4><a href="/company/national-milling-corporation">National Milling Corporation</a></h4>
                                <p>National Milling Corporation Limited has been in existence since the pre-independence era in 1964, operating </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/management-accountant?id=758&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/management-accountant" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/management-accountant?id=758&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/759/chief-of-party/population-council">Chief of Party</a></h2>
                                <h3>Lusaka, Zambia</h3>
                                <a href="/company/population-council"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTBfYS5wbmc=&amp;width=180&amp;height=53" alt="workinafrica Chief of Party Population Council"></a>
                                <h4><a href="/company/population-council">Population Council</a></h4>
                                <p>The Population Council conducts research to address critical health and development issues. Our work allows </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/chief-of-party?id=759&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/chief-of-party" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/chief-of-party?id=759&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/760/deputy-chief-of-party/population-council">Deputy Chief of Party</a></h2>
                                <h3>Lusaka, Zambia</h3>
                                <a href="/company/population-council"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTBfYS5wbmc=&amp;width=180&amp;height=53" alt="workinafrica Deputy Chief of Party Population Council"></a>
                                <h4><a href="/company/population-council">Population Council</a></h4>
                                <p>The Population Council conducts research to address critical health and development issues. Our work allows </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/deputy-chief-of-party?id=760&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/deputy-chief-of-party" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/deputy-chief-of-party?id=760&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/761/doctors/marybegg-memorial-clinic">Doctors</a></h2>
                                <h3>Solwezi, Zambia</h3>
                                <a href="/company/marybegg-memorial-clinic"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTFfZG93bmxvYWQgKDEpLmpwZw==&amp;width=180&amp;height=53" alt="workinafrica Doctors MaryBegg Memorial Clinic"></a>
                                <h4><a href="/company/marybegg-memorial-clinic">MaryBegg Memorial Clinic</a></h4>
                                <p>Mary Begg Community Clinic is a non-profit organisation which provides quality health care for the Zambian </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/doctors?id=761&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/doctors" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/doctors?id=761&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/762/nurse-tutor/marybegg-memorial-clinic">Nurse Tutor</a></h2>
                                <h3>Solwezi, Zambia</h3>
                                <a href="/company/marybegg-memorial-clinic"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTFfZG93bmxvYWQgKDEpLmpwZw==&amp;width=180&amp;height=53" alt="workinafrica Nurse Tutor MaryBegg Memorial Clinic"></a>
                                <h4><a href="/company/marybegg-memorial-clinic">MaryBegg Memorial Clinic</a></h4>
                                <p>Mary Begg Community Clinic is a non-profit organisation which provides quality health care for the Zambian </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/nurse-tutor?id=762&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/nurse-tutor" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/nurse-tutor?id=762&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/763/registered-nurses/marybegg-memorial-clinic">Registered Nurses</a></h2>
                                <h3>Ndola, Zambia</h3>
                                <a href="/company/marybegg-memorial-clinic"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTFfZG93bmxvYWQgKDEpLmpwZw==&amp;width=180&amp;height=53" alt="workinafrica Registered Nurses MaryBegg Memorial Clinic"></a>
                                <h4><a href="/company/marybegg-memorial-clinic">MaryBegg Memorial Clinic</a></h4>
                                <p>Mary Begg Community Clinic is a non-profit organisation which provides quality health care for the Zambian </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/registered-nurses?id=763&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/registered-nurses" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/registered-nurses?id=763&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                                <div class="col-md-4 hp-body-single-offer col-sm-6">
                            <div class="hp-offer-inner">
                                <h2><a href="/jobs/764/pastel-evolution-data-capturer/marybegg-memorial-clinic">Pastel Evolution Data Capturer</a></h2>
                                <h3>Ndola, Zambia</h3>
                                <a href="/company/marybegg-memorial-clinic"><img src="/constrain-image/?url=L2NvbXBhbmllcy9vcmlnaW5hbC80MTFfZG93bmxvYWQgKDEpLmpwZw==&amp;width=180&amp;height=53" alt="workinafrica Pastel Evolution Data Capturer MaryBegg Memorial Clinic"></a>
                                <h4><a href="/company/marybegg-memorial-clinic">MaryBegg Memorial Clinic</a></h4>
                                <p>Mary Begg Community Clinic is a non-profit organisation which provides quality health care for the Zambian </p>
                                <hr>
                                <ul>
                                    <li><a href="/recommend/pastel-evolution-data-capturer?id=764&amp;noredir" class="link" data-target="#preview-modal" data-toggle="modal"><i class="fa fa-thumbs-o-up"></i> Recommend</a></li>
                                    <li class="middle-btn"><a href="/share/pastel-evolution-data-capturer" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-share"></i> Share</a></li>
                                    <li><a href="/apply/pastel-evolution-data-capturer?id=764&amp;noredir" data-target="#preview-modal" data-toggle="modal" class="link"><i class="fa fa-check"></i> Apply</a></li>
                                </ul>
                            </div>
                        </div>
                                            </div>
                                        <div class=""><button class="btn hp-load-more"><span class="glyphicon glyphicon-menu-hamburger"></span> Load More</button></div>
                                    </div>
                <div class="col-md-3 hp-body-sidebar col-xs-12">
                    <div class="col-md-12 hp-sidebar-item hp-siderbar-salary">
    					<img src="/resources/frontend/images/salsay-survey-sidebar.jpg" alt="Salary Survery" class="img-responsive">
    					<div class="hp-siderbar-salary-inner">
    						<h3>Find how well you are paid!</h3>
    						<h3>Compelte the WorkinAfrica <br> Salary Survery</h3>
    						<p><a class="btn" href="/salary-survey">Complete the Survery</a></p>
    					</div>
    				</div>
                    <div class="col-md-12 hp-sidebar-item hp-sidebar-news">
                        <div class="hp-sidebar-title">News</div>
                                                <div class="hp-news-item-picture hp-sidebar-item-inner">                            
                            <a href="/news/soft-skills-will-make-the-difference"><img src="/constrain-image/?url=L25ld3MvN18xMDAxMTIuanBn&amp;width=243&amp;height=162" alt="africa-got-talent"></a>
                            <h3><a href="/news/soft-skills-will-make-the-difference">Soft Skills Will Make the Difference:</a></h3>
                            <p><a href="/news/soft-skills-will-make-the-difference.html">No matter how well experienced you are, the importance of the less-honored so called “soft skills” can sometimes overcome more objective qualities.</a></p>
                        </div>
                                                <div class="hp-news-item-picture hp-sidebar-item-inner">                            
                            <a href="/news/dress-to-impress-for-an-interview"><img src="/constrain-image/?url=L25ld3MvM19oZW5yeTIuanBn&amp;width=243&amp;height=162" alt="africa-got-talent"></a>
                            <h3><a href="/news/dress-to-impress-for-an-interview">Dress to Impress for an Interview:</a></h3>
                            <p><a href="/news/dress-to-impress-for-an-interview.html">Choosing what to wear for an interview might not be an easy task. But if you get it right, it will earn you extra points in the eye of the employer.</a></p>
                        </div>
                                                
                    </div>                    
                    <div class="col-md-12 hp-sidebar-item hp-sidebar-test col-xs-12">
                        <div class="hp-sidebar-title">Latest Tests</div>
                        <div class="hp-sidebar-latest-test">
                            <div class="hp-sidebar-latest-test-title">
                                <img class="left" src="/resources/files/quiz/1_sidebar-test-icon.png" alt="icon-test-siderbar">
                                <h3 class="left">Find out your Team Role</h3>
                            </div>
                            <p>This is a questionnaire, so there's no right or wrong answers, so feel free to rate each trait just as you feel according to your personality</p>
                            <a href="/quiz/find-out-your-team-role" class="btn">Take the test</a>
                        </div>
                    </div>
                    <div class="col-md-12 hp-sidebar-item hp-sidebar-gamification col-xs-12">
                        <div class="hp-sidebar-title">Earn Gamification Points</div>
                        <div class="hp-sidebar-gamification-inner">
                            <img src="/resources/frontend/images/gamification-icon.png" alt="gamification-icon">
                            <p>Get a concrete, accurate description of who you are, what you can do and what are the best jobs for you.</p>
                            <a href="/profile" class="btn">Complete your profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

<?php include('footer.php'); ?>

