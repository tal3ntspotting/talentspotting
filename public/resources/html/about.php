<?php include('header.php'); ?>
<style>
.about-us {
	background:#fff;
}
	.about-lvl-1 {
		background:url('images/poza-about-us.png') no-repeat;
		background-position:center center;
		background-size:cover;
	}
	.about-lvl-1 .container {
		margin:0 auto;
		padding:100px 0px;
		margin-bottom:40px;
	}
	.about-lvl-1 h1 {
		color:#fff;
		text-align:center;
		font-family:Helvetica;
		font-size:48px;
		padding-bottom:20px;
		margin:0px;
	}
	
	.about-lvl-2 p,
	.about-lvl-1 p {
		color:#fff;
		text-align:center;
		font-size:24px;
		font-family:Helvetica;
	}
	
	.about-lvl-2 p {
		color:#333;
	}
	
</style>
<div class="about-us">
	<div class="about-lvl-1">
		<div class="container">
			<h1>About Us</h1>
			<p>We are happy to mention that we have made more than 50 successfull projects in <br />sub-Saharan Africa.</p>
		</div>
	</div>
	<div class="about-lvl-2">
		<div class="container">
			<p>
						We are a recruiting company, specialized in finding, growing and connecting the right people<br /> with their dream jobs, considering all relevant factors from company’s organizational culture fit,<br /> to the expected skills match, right timing and motivation.<br /><br />
We want people to make the most of their professional time and to connect companies with<br /> the best candidates faster than anyone else. <br /><br />
We take a close look at every applicant’s skills, interest and motivation and match them with<br /> the organizational culture and requirements so that both company’s goals and the candidate’s are met. <br /><br />
Time limit make us creatively and transparently connect professionals<br /> with their dream jobs and companies.<br /><br />
			</p>
		</div>

	</div>
</div>
<?php include('footer.php'); ?>
