<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3>Latest News</h3>
				<ul>
					<li><a href="#">A short infographic about the most wanted jobs in Nigeria</a></li>
					<li><a href="#">What are the top companies in Sub-Saharan Africa </a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h3>Events</h3>
				<ul>
					<li><a href="#">HR Summit 2015</a></li>
					<li><a href="#">Nigeria got Talent may - june 2015</a></li>
					<li><a href="#">Management Training Nigeria</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h3>Employers</h3>
				<ul>
					<li><a href="#">Sign In</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">News</a></li>
					<li><a href="#">Pricing</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h3>Language</h3>
				<ul>
					<li><a href="#">English</a></li>
					<li><a href="#">Francais</a></li>
				</ul>
			</div>
		</div>
	</div>
    <!-- Analytics CODE -->
    <!-- End of Analytics CODE -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="js/jquery.raty.js"></script>
    <script type="text/javascript" src="js/countrySelect.min.js"></script>
    <script type="text/javascript" src="js/video.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</footer>