<?php include('header.php')	?>
<div class="sign-in_2-lvl1 sign-in-linkedin">
	<div class="container">
		<div class="row">
			<h2>Hello <span class="candidate">Jacob</span> and thank your for singing in with LinkedIn.</h2>
			<h3>Now let’s look at your skills</h3>
			<ul class="skill-tree">
				<li rel="Java">Java</li>
				<li rel="Social Media">Social Media</li>
				<li rel="Project Management">Project Management</li>
				<li rel="Teamwork">Teamwork</li>
				<li rel="Training">Training</li>
				<li rel="Wordpress">Wordpress</li>
				<li rel="TimeManagement">TimeManagement</li>
			</ul>
			<div class="linkedin-btns">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<a href="#" class="continue-skills">
						This is it! <br /> We can continue.
					</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div  class="add-skills">I can do more <br />ADD SKILLS</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sign-in_2-lvl2">
	<div class="container">
		<div class="row">
			<p>
				We believe that professional life affects people’s personal life too much to be taken lightly.
 				<br /><br />
				Our mission is to build the frame in which people are encouraged to set career goals, <br />
				develop their core and soft skills and get their dream jobs faster. <br />
				<br />
				Time is too short to wait for a call ...
			</p>
			<ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<li class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
					<img src="images/opp-1.png" alt="Suitable Jobs" />
					<span>Suitable jobs just for you</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-2.png" alt="Suitable Jobs" />
					<span>Rewards based on your professional activity</span>
				</li>
				<li class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 col-xs-12">
					<img src="images/opp-3.png" alt="Suitable Jobs" />
					<span>News, company reviews and more other informations about your interests</span>
				</li>
			</ul>
			<p>Join our professionals league and grow faster!</p>
		</div>	
	</div>
</div>
<div class="sign-in_2-lvl3">
	<div class="container">
		<div class="center-lvl3">
			<h1>We help you get a better job for a better life in Africa</h1>
			<a href="#">Let's start!</a>
		</div>
	</div>
</div>

<?php include('footer.php')	?>