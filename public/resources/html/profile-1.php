<?php include('header.php')
?>
<div class="profile">
	<div class="">
		<div class="container profile-details-1 white">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 profile-avatar">
				<img src="https://scontent.xx.fbcdn.net/hphotos-xtp1/v/t1.0-9/11025143_1010919002271260_4203272829691760359_n.jpg?oh=18fc33266b21e2f37efa2032dca65837&oe=562B9E08" alt="User Avatar" class="img-circle">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 user-personal-details">
				<h1>Adrian Vasilescu</h1>
				<h2>Online Marketing Specialist at Brainspotting Romania</h2>
				<span class="user-type">Regular User</span>
				<button type="button" class="btn btn-primary">
					Sync with Linkedin
				</button>
				<button type="button" class="btn btn-primary">
					Import your CV
				</button>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="profile-points col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<span class="number">90</span>
					<span class="indicator">Points</span>
				</div>
				<div class="profile-graph col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<img src="images/graph.jpg" alt="procent" class="img-responsive" />
					<span id="procent-complete">Completed 20%</span>
				</div>
				<div class="profile-applications col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<span class="number">3</span>
					<span class="indicator">Applications</span>
				</div>
				<div class="profile-recommandations col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<span class="number">2</span>
					<span class="indicator">Recommandations</span>
				</div>
			</div>
		</div>
	</div>
	<div class="profile-main ">
		<div class="container">
		<section class="panel">
			<div class="panel-heading tab-bg-dark-navy-blue">
				<ul class="nav nav-tabs nav-justified ">
					<li class="active">
						<a data-toggle="tab" href="#overview"> CV </a>
					</li>
					<li class="">
						<div class="tooltip_closable pos_top hidden">
							<span class="content">See your applications here</span>
							<a class="close_tooltip"><i class="fa fa-times"></i></a>
							<div class="tooltip-arrow-down"></div>
						</div>
						<a data-toggle="tab" href="#job-history" id="job-history-button"> Job Application History </a>
					</li>
					<li>
						<a data-toggle="tab" href="#contacts" class="contact-map" id="job-recommendations-button"> Recommendations </a>
					</li>
					<li>
						<div class="tooltip_closable pos_top hidden">
							<span class="content">Create job alerts to receive new jobs</span>
							<a class="close_tooltip"><i class="fa fa-times"></i></a>
							<div class="tooltip-arrow-down"></div>
						</div>
						<a data-toggle="tab" href="#alerts" id="job-alerts-button"> My Alerts </a>
					</li>
					<li>
						<a data-toggle="tab" href="#rewards" class="contact-map" id="job-rewards-button"> Rewards </a>
					</li>
					<li>
						<a data-toggle="tab" href="#settings" id="job-settings-button"> Settings </a>
					</li>
				</ul>
			</div>
			<div class="panel-body"> 
				<div class="tab-content tasi-tab">
					<div id="overview" class="tab-pane active">
						<div class="need-padding col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white history">
							<div class="top-line">
								<span class="white-space-title">Job history</span>
								<button type="button" class="btn btn-primary right edit">Edit</button>
							</div>
							<div class="jobs-timeline">
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry edit-job"> 
									<a class="single-edit"><img src="images/btn-edit.png" alt="edit btn" /></a>
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>	
									<a href="#" class="more-info" rel="less">See More</a>									
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>		
									<a href="#" rel="more">See More</a>							
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>	
									<a href="#" rel="less">See More</a>									
								</div>
								<hr />
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>		
									<a href="#" rel="less">See More</a>								
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>		
									<a href="#" rel="less">See More</a>								
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>					
									<a href="#" rel="more">See More</a>					
								</div>
								<hr />
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>		
									<a href="#" rel="less">See More</a>								
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12 work-entry">
									<span class="job-period"><span class="small-circle"></span> Mar 2015 - Present</span>
									<span class="job-position">Online Marketing Specialist at Brainspotting Romania</span>
									<div class="job-attr">
										<ul>
											<li>
												<strong>Job Description:</strong>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
											</li>
											<li>
												<strong>Job Description:</strong>
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
											</li>
										</ul>
									</div>	
									<a href="#" rel="less">See More</a>									
								</div>
								
							</div>
						</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white edu">
								<div class="top-line">
									<span class="white-space-title">Education</span>
									<button type="button" class="btn btn-primary right edit">Edit</button>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<span class="edu-place"><span class="small-circle"></span>University of Bucharest</span>
									<span class="edu-profile">Psyhology</span>
									<span class="edu-level">Bachelor's degree</span>
									<span class="edu-time"><span class="edu-start">2014</span>-<span class="edu-end">2015</span></span>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<span class="edu-place"><span class="small-circle"></span>University of Bucharest</span>
									<span class="edu-profile">Psyhology</span>
									<span class="edu-level">Bachelor's degree</span>
									<span class="edu-time"><span class="edu-start">2014</span>-<span class="edu-end">2015</span></span> 
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<span class="edu-place"><span class="small-circle"></span>University of Bucharest</span>
									<span class="edu-profile">Psyhology</span>
									<span class="edu-level">Bachelor's degree</span>
									<span class="edu-time"><span class="edu-start">2014</span>-<span class="edu-end">2015</span></span>
								</div>
						
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white cert">
								<div class="top-line">
									<span class="white-space-title">Certifications</span>
									<button type="button" class="btn btn-primary right edit">Edit</button>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<span class="certify"><span class="small-circle"></span>IELTS certification</span>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 white">
							<span class="white-space-title">All skils</span>
							<!-- For later Use - stage 2 implementation -->
							<!-- <ul class="skill-chart">
								<li class="even"><span>Customer relations service</span>
									<ul>
										<li></li>
										<li></li> 
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
								<li class="odd"><span>Java</span>
									<ul>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
								<li class="even"><span>Java</span>
									<ul>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
								<li class="odd"><span>Java</span>
									<ul>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
							</ul> -->
							<ul class="all-my-skills">
								<li><span class="btn btn-info">java</span></li>
								<li><span class="btn btn-info">css</span></li>
								<li><span class="btn btn-info">html</span></li>								
								<li><span class="btn btn-info">project management</span></li>
								<li><span class="btn btn-info">time consumption</span></li>								
								<li><span class="btn btn-info">client service</span></li>
								<li><span class="btn btn-info">developer</span></li>
								<li><span class="btn btn-info">killing bugs</span></li>
								<li><span class="btn btn-info">slaying dragons</span></li>
								<li><span class="btn btn-info">ping ponies</span></li>
							</ul>
						</div>
					</div>		
					<div id="job-history" class="tab-pane">
						<div class="top-line">
							<span>Job history</span>
							<button type="button" class="btn btn-primary right edit">Edit</button>
						</div>
					</div>		
					<div id="alerts"  class="tab-pane">
						<div class="top-line">
							<span>Job history</span>
							<button type="button" class="btn btn-primary right edit">Edit</button>
						</div>
					</div>	
					<div id="contacts"  class="tab-pane">
						<div class="top-line">
							<span>Job history</span>
							<button type="button" class="btn btn-primary right edit">Edit</button>
						</div>
					</div>
					<div id="rewards"  class="tab-pane">
						<div class="top-line">
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="gamification-box game-box-1">
										<div class="gamification-lvl-1">Data <strong>FREE</strong> on our platform</div>
										<div class="gamification-lvl-2">200p</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="gamification-box game-box-2">
										<div class="gamification-lvl-1">Data <strong>FREE</strong> on our platform</div>
										<div class="gamification-lvl-2">200p</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="gamification-box game-box-3">
										<div class="gamification-lvl-1">Data <strong>FREE</strong> on our platform</div>
										<div class="gamification-lvl-2">200p</div>
									</div>
								</div>
								<table class="table table-bordered">
									<thead>
										<th>Actions</th>
										<th>Points</th>
										<th>Check Box</th>
									</thead>
									<tbody>
										<tr>
											<td><strong>Profile</strong></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><span title="This is some information for our tooltip." class="tooltip1"><span title=" ">Create profile</span></span></td>
											<td>50</td>
											<td><img src="images/plan-check.png" alt="check option"></td>
										</tr>
										<tr>
											<td><span title="This is some information for our tooltip." class="tooltip1"><span title=" ">Upload profile picture</span></span></td>
											<td>50</td>
											<td><img src="images/plan-check.png" alt="check option"></td>
										</tr>
										<tr>
											<td>Add experience</td>
											<td>50</td>
											<td><img src="images/plan-check.png" alt="check option"></td>
										</tr>
										<tr>
											<td>Add education</td>
											<td>50</td>
											<td><img src="images/plan-x.png" alt="option not available"></td>
										</tr>
										<tr>
											<td>Add your skills</td>
											<td>50</td>
											<td><img src="images/plan-x.png" alt="option not available"></td>
										</tr>
										<tr>
											<td>Create profile</td>
											<td>50</td>
											<td><img src="images/plan-x.png" alt="option not available"></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="settings"  class="tab-pane">
						<form role="form" class="form-horizontal settings col-lg-7 col-lg-offset-2 " action="/profile" method="POST" enctype="multipart/form-data" >
                                        <div class="alert alert-success fade in hidden">
                                            <button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button>
                                            <strong>Well Done!</strong> Profile updated successfully                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Email</label>
                                            <div class="col-lg-8">
                                                <input type="email" value="voicu.robert90@gmail.com" id="email" required="" name="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> Profile image</label>
                                            <div class="col-lg-8">
                                                <input type="file" id="exampleInputFile" name="image" class="file-pos"><br>
                                                                                                <img src="" id="profile_image" class="hidden" alt="">
                                                                                                <p>Upload image info</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Headline</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="title" name="title" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Location</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="location" required="" name="location" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Description</label>
                                            <div class="col-lg-8">
                                                <textarea rows="3" cols="30" class="form-control" id="description" name="description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Availability</label>
                                            <div class="col-lg-8">
                                                <select class="form-control" name="availability">
                                                                                                        <option value="immediately">Immediately</option>
                                                    <option value="1month">Up to 1 month</option>
                                                    <option value="3months">Up to 3 months</option>
                                                    <option value="moremonths">Over 3 months</option>
                                                    <option value="not_available">Currently not available</option>
                                                </select>
                                                <!-- <input class="form-control" id="availability" required="" name="availability" value=""> -->
                                            </div>
                                        </div>
                                        <div class="prf-contacts sttng">
                                            <h2>Contact info</h2>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Firstname</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="robert" required="" id="firstname" name="firstname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Lastname</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="voicu" required="" id="lastname" name="lastname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Phone</label>
                                            <div class="col-lg-8">
                                                <input type="phone" value="" pattern="(\+?\d[- .]*){7,13}" required="" id="phone" name="phone" class="form-control">
                                            </div>
                                        </div>     
                                        <div class="prf-contacts sttng">
                                            <h2>Billing Info</h2>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Address</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_address" name="billing_address" class="form-control">
                                            </div>
                                        </div>   
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Company</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_company" name="billing_company" class="form-control">
                                            </div>
                                        </div>     
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Bank</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_bank" name="billing_bank" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Account</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_iban" name="billing_iban" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">VAT No. / Personal Number</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_cui" name="billing_cui" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Registration number</label>
                                            <div class="col-lg-8">
                                                <input type="text" value="" id="billing_j" name="billing_j" class="form-control">
                                            </div>
                                        </div>                                        
                                        <div class="prf-contacts sttng">
                                            <h2>Change password</h2>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Original password</label>
                                            <div class="col-lg-8">
                                                <input type="password" value="" id="original_password" name="original_password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">New Password</label>
                                            <div class="col-lg-8">
                                                <input type="password" value="" id="new_password" name="new_password" class="form-control" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">New password confirm</label>
                                            <div class="col-lg-8">
                                                <input type="password" value="" id="new_password_confirm" name="new_password_confirm" class="form-control" autocomplete="off">
                                            </div>
                                        </div>                                     
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <button class="btn btn-warning" type="submit">Save</button>
                                                <button class="btn btn-default" type="button">Cancel</button>
                                            </div>
                                        </div>                                        
                                    </form>
					</div>
				</div>
	 		</div>
		</section>
		<div class="row">
			<div class="profile-schoolar col-lg-8 col-md-8 col-sm-12 col-xs-12">
				
			</div>
		</div>
		</div>
		</div>

	</div>
</div>
<?php include('footer.php')	?>