<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="apple-touch-icon" sizes="57x57" href="favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favico/favicon-16x16.png">
    <link rel="manifest" href="favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Work in Africa - jobs</title> <!-- title from Owner -->
    <!-- META FOR SEO -- Dynamically generated -->
    <meta name='keywords' content='your, tags'>
    <meta name='description' content='150 words'>
    <meta name='subject' content='your websites subject'>
    <meta name='copyright' content='Brainspotting'>
    <meta name='language' content='EN'>
    <meta name='robots' content='index,follow'>
    <meta name='revised' content='Sunday, May 24th, 2015, 5:15 pm'>
    <meta name='abstract' content=''>
    <meta name='topic' content=''>
    <meta name='summary' content=''>
    <meta name='Classification' content='Business'>
    <meta name='author' content='name, email@hotmail.com'>
    <meta name='designer' content=''>
    <meta name='reply-to' content='email@hotmail.com'>
    <meta name='owner' content=''>
    <meta name='url' content='http://www.websiteaddrress.com'>
    <meta name='identifier-URL' content='http://www.websiteaddress.com'>
    <meta name='directory' content='submission'>
    <meta name='pagename' content='Work in Africa -- Homepage'>
    <meta name='category' content=''>
    <meta name='coverage' content='Worldwide'>
    <meta name='distribution' content='Global'>
    <!-- End of Meta for SEO -->
    <!-- Open Graph -- Needs SEO Revision -- Dynamically generated -->
    <meta property="og:title" content="Work in africa" />
    <meta property="og:type" content="Job Platform" />
    <meta property="og:url" content="favico/favicon-16x16.png" /> <!-- needs Image -->
    <meta property="og:image" content="favico/favicon-16x16.png" /> <!-- needs Image -->
    <!-- End of Open Graph -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <link href="css/jquery.raty.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/countrySelect.min.css">
    <link rel="stylesheet" href="css/video-js.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700,300,600,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/querys.css"/>
</head>
<div id="gif-loader">
	
</div>
<header class="container-fluid">
    <div class="row"> 
        <div class="container header-cont">
           <!-- Mobile Header -->	
           <div class="navbar-header visible-xs hidden-md visible-sm">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button> 
             <a class="navbar-brand" href="/">
               <img src="images/logo-wia-new.png" alt="Work in Africa Logo" />
            </a>
            <div class="col-md-3 container-quick-acces visible-xs visible-sm">
              <ul id="header-quick-acces">
                 <li class="notifications has-notifications ">
                   <span class="notifications-number">20</span>
                     <ul>
                       <li class="notification-small-nr">Notifications (<span>2</span>):</li>
                       <li>
                         <a href="http://www.google.ro">We have 2 events for you close to your locations. <br />
                         Check them out!</a>
                       </li>
                       <li>
                         <a href="http://www.youtube.com">Adobe just posted a new job entry!</a>
                       </li>
                     </ul>
                 </li>              
               </ul>
            </div>         
          </div>
            <!-- Desktop Header -->
            <div id="navbar" class="navbar-collapse collapse hidden-sm">
                <div class="col-md-3 logo hidden-xs col-sm-2 hidden-sm">
                    <a href="/index.php">
                        <img src="images/logo-wia-new.png" alt="Work in Africa Logo" />
                    </a>
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-4 container-quick-links">
                    <ul id="header-quick-links">
		                <li class="dropdown hidden-lg hidden-md custom-dropdown-container">
			                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
			                  <img class="avatar-small-20" alt="" src="images/default_avatar.png">
		                      <span class="user-name">robert voicu</span>
		                      <b class="caret"></b>
			                </a>
			                <ul class="custom-drop-down" role="menu">
			                  <li><a href="#">Action</a></li>
			                  <li><a href="#">Another action</a></li>
			                  <li><a href="#">Something else here</a></li>
			                  <li><a href="#">Separated link</a></li>
			                  <li><a href="#">One more separated link</a></li>
			                </ul>
			            </li>		            
			            <li class="container-language-selector dropdown language hidden-lg hidden-md custom-dropdown-container" >
                            <!--<div class="form-item header-language-selector">
                                <input class="country_selector" type="text">
                                <label for="country_selector" style="display:none;">Select a country here...</label>
                            </div> -->

		                        <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
		                            <img src="images/en.png" alt="here">
		                            <span class="username">EN</span>
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="custom-drop-down" role="menu">
		                            <li><a href="/en"><img src="images/en.png" alt="here"> English</a></li>
		                            <li><a href="/fr"><img src="images/fr.png" alt="here"> Francais</a></li>
		                        </ul>
                        </li>
                        <li><a href="#">Employers</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">Jobs</a></li>
                    </ul>              
                </div>
                <div class="col-md-4 col-sm-4 container-quick-acces hidden-xs hidden-sm">
                    <ul id="header-quick-acces" class="hidden-sm hidden-xs">
                        <li class="notifications has-notifications ">
                            <span class="notifications-number">20</span>
                            <ul>
                            	<li class="notification-small-nr">Notifications (<span>2</span>):</li>
                                <li>
                                    <a href="#">We have 2 events for you close to your locations. <br />
                                    Check them out!</a>
                                </li>
                                <li>
                                    <a href="#">Adobe just posted a new job entry!</a>
                                </li>
                            </ul>
                        </li>
                       <!-- <li><a href="#">Sing In</a></li> -->
                       <li>
                       	<li class="dropdown account-signed-in">
		                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
		                      <img class="avatar-small-20" alt="" src="images/default_avatar.png">
		                      <span class="user-name">robert voicu</span>
		                      <b class="caret"></b>
		                    </a>
		                    <ul class="dropdown-menu extended logout">
		                        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
		                        <li><a href="#"><i class="fa fa-key"></i> Log Out</a></li>
		                    </ul>
		                </li>
                       </li>
                        <li class="container-language-selector dropdown language" >
                            <!--<div class="form-item header-language-selector">
                                <input class="country_selector" type="text">
                                <label for="country_selector" style="display:none;">Select a country here...</label>
                            </div> -->
		                        <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
		                            <img src="images/en.png" alt="here">
		                            <span class="username">EN</span>
		                            <b class="caret"></b>
		                        </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="/en"><img src="images/en.png" alt="here"> English</a></li>
		                            <li><a href="/fr"><img src="images/fr.png" alt="here"> Francais</a></li>
		                        </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>