$(document).ready(function () {

    $('#status-area').click(function () {
    });

    $.fn.flash_message = function (options) {
        options = $.extend({
            title_class: '',
            title: '',
            color: '',
            text: 'Done',
            time: 1000,
            how: 'before',
            class_name: ''
        }, options);
        return $(this).each(function () {
            if ($(this).parent().find('.flash_message').get(0))
                return;
            var message = '<span class="flash_message" style="color:' + options.color + '"><h2 class="' + options.title_class + '">' + options.title + '</h2>' + options.text + '</span>';

            $(this)[options.how](message);
        });
    };
    $('span.icn').on('click', function () {
        window.open('https://docsend.com/view/rqgm4dr');
    });
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (isMobile.any()) {
        $('.lap-content > h2').css({'text-align': 'center'});
        $('.img-slider').css({'padding-top': '30px'});
        $('input[id="email"]').css({'width': '100% !important'});
        $('#email').removeClass('col-sm-offset-3');
        $('.btn-block').css({'width': '100%'});
        $('#arrows').hide();
//        $('#talentspotting > .bx-controls-direction').hide();
    } else {
//        $.scrollify({
//            section: "section",
//            scrollSpeed: 800,
//            before: function (data) {
//
//                if (data == 5) {
//                    $('#footer').fadeIn();
//                } else {
//                    $('#footer').hide();
//                }
//            },
//        });
    }
    
       
    var form = $('.form-slider').bxSlider({
        adaptiveHeight: true,
        infiniteLoop: false,
        prevText: 'Previous Question',
        nextText: 'Next Question',
        onSlideBefore: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            if(isMobile.any()) {
                var slide = totalSlideQty ;
                if(slide == 3) {
                    $('.talent-data').one('click', function () {
                        $('.form-slider').parent().parent().hide();
                        $('#arrows').hide();
                        $.get("/contact", function (response) {
                            content = response;
                            $('#next').after(content);
                            $('form#ContactForm').submit(function () {
                                console.log(134);
                                var formData = $("form#ContactForm").serialize();
                                $.ajax({
                                    url: "ajax2.php",
                                    data: formData,
                                    Type: "POST",
                                    success: function () {
                                        calculResult(data);
                                        $('#contact').remove();
                                        delete $.fn.circleGraphic;
                                    }
                                });
                                return false;
                            });
                            $('.not-intersted-click').on('click', function () {
                                calculResult(data);
                                $('#contact').remove();
                            });
                        });
                    })
                }
            }
            
        }
    });

    var data = [];
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    };
     var sts = 0 ;
     var sts_2 = 0 ;
    // Get the size of an object
    $('.talent-data').one('click',function () {
        var $this_slide = form.getCurrentSlide();
        if ($(this).parent().parent().find('.talent-data').hasClass('added')
                && $this_slide !== 2
                && $this_slide !== 3) {
            return false;
        } 
        if ($this_slide == 3) {
            if ($(this).hasClass('none')) {
                if(sts_2 == 1) {
                    return false ;
                }
                console.log('just none clicked');
                $(this).attr('data-none', 1);
                sts = 1 ;
            } else {
                sts_2 = 1 ;
                if(sts == 1) {
                    sts_2 = 1 ;
                    return false ;
                } 
            }
        }
        var slide = $(this).parent().parent().data('slide');
        var points = $(this).data('points');
        var obj = {'slide': slide, 'points': points};
        data.push(obj);

        $(this).addClass('added');
        var size = Object.size(data);
        console.log(size);
        $(".talentspotting-content > .margtp > .bx-wrapper > .bx-has-controls-direction > .bx-controls-direction > .bx-next").on('click',function() {
            console.log($this_slide);
            if($this_slide == 4) {
                $('#arrows').hide();
                $('.form-slider').parent().parent().hide();
                var content;
                $.get("/contact", function (response) {
                    content = response;
                    console.log(content);
                    $('#next').html(response);
                    $('form#ContactForm').submit(function () {
                        console.log(12222);
                        var formData = $("form#ContactForm").serialize();
                        $.ajax({
                            url: "/subscribe",
                            data: formData,
                            Type: "POST",
                            success: function () {
                                calculResult(data);
                                $('#contact').remove();
                                delete $.fn.circleGraphic;
                            }
                        });
                        return false;
                    });
                    $('.not-intersted-click').on('click', function () {
                        calculResult(data);
                        $('#contact').remove();
                    });
                });
            }
        })
        return false;
    });
    var distance = $('#top').offset().top(),
            $window = $(window);
    $window.scroll(function () {
        if ($window.scrollTop() >= distance) {
            // Your div has reached the top
            $('#nav-bar').addClass('fixed').fadeIn();
            $('#nav-bar').removeClass('hidden');
        } else {
            $('#nav-bar').removeClass('fixed');
            $('#nav-bar').addClass('hidden');
        }
    });
    var mySlider = $('.slider-lap').bxSlider({
        onSlideBefore: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            switch ($(currentSlideNumber).attr('id')) {
                case("0") :
                    $('#img-slider').attr('src', 'resources/frontend/_landing/images/sliders/social.png');
                    break;
                case("1") :
                    $('#img-slider').attr('src', 'resources/frontend/_landing/images/sliders/recruitment.png');

                    break;
                case("2") :
                    $('#img-slider').attr('src', 'resources/frontend/_landing/images/sliders/gamification.png');
                    break;
                case("3") :
                    $('#img-slider').attr('src', 'resources/frontend/_landing/images/sliders/analytics-stats.png');
                    break;
                case("4") :
                    $('#img-slider').attr('src', 'resources/frontend/_landing/images/sliders/mobile.png');
                    break;
            }
        }
    });
    var currentSlide = mySlider.getCurrentSlide();
});
function calculResult(data) {
    var result = 0;
    for (var i in data) {
        result += data[i].points;
    }


    if (result >= 0 && result <= 26) {
        $('.circleGraphic1').circleGraphic({'percent': randomIntFromInterval(0, 26)});
        $('#status-area').flash_message({
            title_class: 'title_class_red',
            title: 'Get 86% more quality candidates in your process',
            color: 'rgb(29, 29, 29)',
            text: 'Talentspotting can improve your recruitment performance through the quality of short-listed people that your are going to spend valuable time on during interviews. Contact us to boost your recruitment performance: <a  class="mail-br" href="mailto:hello@talenstpotting.com">hello@talenstpotting.com</a>',
            how: 'append'
        });
    }
    if (result >= 27 && result <= 51) {
        $('.circleGraphic1').circleGraphic({'percent': randomIntFromInterval(27, 51)});
        $('#status-area').flash_message({
            title_class: 'title_class_orange',
            title: 'Get 86% more quality candidates in your process fast and easy',
            color: 'rgb(29, 29, 29)',
            text: "Talentspotting can improve your recruitment performance through the quality of short-listed candidates and the rapidity in reaching them. Contact us to boost your recruitment performance:<br /> <a class='mail-br' href='mailto:hello@talenstpotting.com'>hello@talenstpotting.com</a>",
            how: 'append'
        });
    }
    if (result >= 52 && result <= 130) {
        $('.circleGraphic1').circleGraphic({'percent': randomIntFromInterval(52, 95)});
        $('#status-area').flash_message({
            title_class: 'title_class_green',
            title: 'Get 86% more quality candidates in your process fast and easy',
            color: 'rgb(29, 29, 29)',
            text: "Talentspotting can improve your recruitment performance by reaching a high number of quality and relevant people already connected to and appreciated by your company’s employees. Contact us to boost your recruitment performance: <br /> <a class='mail-br' href='mailto:hello@talenstpotting.com'>hello@talenstpotting.com</a>",
            how: 'append'
        });
    }
    $('.talent-data').off('click');
}
function randomIntFromInterval(min, max)
{
    return Math.floor(Math.random() * (max - min + 1) + min);
}




  var speed = 5000;

    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();


    //if user clicked on prev button

    $('#buttons a').click(function (e) {
        //slide the item

        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }

        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }

        //cancel the link behavior
        return false;

    });

    //if mouse hover, pause the auto rotation, otherwise rotate it
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });


    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }


//a simple function to click next link
//a timer will call this function, and the rotation will begin

function rotate() {
    $('#next').click();
}
