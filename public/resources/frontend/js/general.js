jQuery(document).ready(function () {

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if (isMobile.any()) {
        $(".slimScrollDiv").addClass("front-side");
        $("#mobile-nav-profile").show();

        $("nav#menu-nav > ul > li").click(function() {
            $(".slimScrollDiv").addClass("front-side");
            $(".header").addClass('mg_iuu');
        })
        window.onload = function () {
            $(".header").addClass('mg_iuu');
            $(".slimScrollDiv").addClass("front-side");

        }
    }





    $("#profile-sidebar").slimscroll({
        height: $(window).height() + 'px',
        wheelStep: 20,
        position: "left",
    });

    $(window).resize(function () {
        var width = $(window).width();
        if (width <= 765) {
            $(".slimScrollDiv").addClass("front-side");
            console.log('123');

        } else {
            $(".slimScrollDiv").removeClass("front-side");
            console.log('456');
        }
    });

    $('#fbar-frontend-click').click(function () {
        $(".slimScrollDiv").toggleClass("front-side").fadeIn('slow');
        if ($(".slimScrollDiv").hasClass('front-side')) {
            $(".header").addClass('mg_iuu');

        } else {
            $(".header").removeClass('mg_iuu');
        }
    });





    $(".correct-home ul li").on('click', function (e) {

        e.preventDefault();
        e.stopPropagation();
        var url = window.location;
        $(window).attr("location", window.location.origin + "/" + $(this).find('a').attr('href'));
        // console.log($(this).find('a').attr('href'));
        // var url = window.location;
        // window.location.replace(url['hostname']+"/"+url['hash']);
    })

    // $("img#profile-image").hover(function () {
    //     $('.img-hover').toggleClass('hidden');
    // })

    $('.sidebar-toggle-box .fa-bars').click(function (e) {
        $(this).addClass('abc');
        $('#sidebar').toggleClass('hide-left-bar');
        $('.brand').toggleClass('blue');
        $('header').toggleClass('merge-left');
        $('#ora-logo').toggleClass('white');
        if ($('#sidebar').hasClass('hide-left-bar')) {
            $('head').append('<style>.fa-bars:before{color:#FFF !important;}</style>');
        } else {
            $('head').append('<style>.fa-bars:before{color:#34C3D8 !important;}</style>');
        }
        $('#main-content').toggleClass('merge-left');
        $('section#job-detail').toggleClass('merge-left');
        $('section#jobs-detail').toggleClass('merge-left');
        $('footer#footer').toggleClass('merge-left');
        $('section#search-bar').toggleClass('merge-left');
        e.stopPropagation();
        if ($('#container').hasClass('open-right-panel')) {
            $('#container').removeClass('open-right-panel')
        }
        if ($('.right-sidebar').hasClass('open-right-bar')) {
            $('.right-sidebar').removeClass('open-right-bar')
        }

        if ($('.header').hasClass('merge-header')) {
            $('.header').removeClass('merge-header')
        }


    });
    if ($.fn.dcAccordion) {
        $('#nav-accordion').dcAccordion({
            eventType: 'click',
            autoClose: true,
            saveState: true,
            disableLink: true,
            speed: 'slow',
            showCount: false,
            autoExpand: true,
            classExpand: 'dcjq-current-parent'
        });
    }

    jQuery(".dataTables_length").append("<span  class='abc'></span>");

    jQuery(".glyphicon-plus").on('click', function () {
        $(this).addClass('glyphicon-minus');
    });
    jQuery('#preview-modal').on('hidden.bs.modal', function () {
        // do something…
        $('.glyphicon-plus').removeClass('glyphicon-minus');
    })

//    jQuery("#date").daterangepicker();

    var skills = [];
    // jQuery('.client-star-rating').raty({half: true});
    jQuery('.job-rating-star').each(function ()
    {
        var starValue = jQuery(this).attr('rel');
        jQuery(this).raty({readOnly: true, score: starValue});
    });

    jQuery('.personal-rating').each(function ()
    {
        var starValue = jQuery(this).attr('rel');
        jQuery(this).raty({readOnly: true, score: starValue});
    });

    jQuery('.sign-in-mail form').submit(function (e)
    {
        if (jQuery("#addSkill").val() == jQuery("#idSkill").val())
        {
            jQuery('.personal-skills .alert').fadeOut('fast');
            jQuery('.personal-skills').fadeIn('slow');

            // get text input

            var $inputs = jQuery('.sign-in-mail form :input[type="text"]');

            // build skill array
            // replace white spaces with __ for easy manipulation
            // append new skill to skill tree

            $inputs.each(function ()
            {

                if (jQuery(this).val().match(/[a-z]/i))
                {
                    skills.push(jQuery(this).val().replace(/ /g, '__'));
                    var rel = jQuery(this).val().replace(/ /g, '__');
                    jQuery('.sign-in-mail .skill-tree').append('<li rel="' + rel + '">' + jQuery(this).val() + '<span class="delete-skill">-</span></li>');
                } else
                {
                    jQuery('.personal-skills .alert').show();
                }
            });
            // restore blank value for text field
            jQuery('.sign-in-mail form input[type="text"]').val('');
        } else
        {
            alert("Please select a skill from the search list");
        }

        return false;
    });

    jQuery('.custom-dropdown-container').on('click', function ()
    {
        jQuery(this).find('.custom-drop-down').toggle();

    });

    jQuery(document).on('click', '.delete-skill', function ()
    {
        var rel = jQuery(this).parent().attr('rel');
        removeSkill(skills, rel);
        jQuery(this).parent().remove();
    });
});



jQuery(window).load(function () {
    setTimeout(function () {
        jQuery('#gif-loader').fadeOut();
    }, 400);
    jQuery('.location-blue-search').select2();

    jQuery('.desktop-select select').select2();



    jQuery(".country_selector").countrySelect({
        onlyCountries: ['us', 'fr']

    });

    notification_effect();



    // Homepage filter select tags assocition

    var filters = [];

    var $eventLog = $(".js-event-log");

    var $eventSelect = $(".desktop-select select");

    $eventSelect.on("select2:select", function (e) {
        log("select2:select", e);
    });

    function event_append(loc, elem, input)
    {
        jQuery('.table-tags').show();
        jQuery('.table-tags tr td#' + loc).append(elem);
        jQuery('.desktop-select').append(input);
    }



    function log(name, evt)
    {
        if (!evt)
        {
            var args = "{}";
        } else
        {
            var args = JSON.stringify(evt.params, function (key, value)
            {
                if (value && value.nodeName)
                    return "[DOM node]";
                if (value instanceof $.Event)
                    return "[$.Event]";
                return value;
            });
        }
        var json_string = JSON.parse(args);
        var table_value = json_string.data.text;
        var table_place = json_string.data.title;

        var $e = $("<span class='btn btn-info'><span class='info'>" + table_value + "</span><span class='closeMe deleteFilter' rel='filters_" + json_string.data.title + "_" + json_string.data.id + "'>x</span></span>");
        var $ef = $("<input name='filters_" + json_string.data.title + "[]' value='" + json_string.data.id + "' class='filters_" + json_string.data.title + "_" + json_string.data.id + "' type='hidden'>");
        filters.push({table_place: table_place});

        //$(".desktop-select > select[name="+json_string.data.title+"] option[value="+json_string.data.id+"]").attr("selected", "selected");
        //console.log($(".desktop-select  select[name='"+table_place+"']").val());
        //console.log(json_string.data.id);

        event_append(table_place, $e, $ef);
        $e.animate({opacity: 1}, 10000, 'linear', function () { });
    }



    //see more / less profile
    jQuery('.work-entry a.more-info').on('click', function (e) {
        e.preventDefault();
        var status = jQuery(this).attr('rel');
        if (status == 'more') {
            status = jQuery(this).attr('rel', 'less');
            jQuery(this).parent().find('.job-attr').slideToggle("slow");
            jQuery(this).html('See More');

        } else {
            status = jQuery(this).attr('rel', 'more');
            jQuery(this).parent().find('.job-attr').slideToggle("slow");
            jQuery(this).html('See Less');
        }
    });




});



function removeSkill(arr) {

    var what, a = arguments, L = a.length, ax;

    while (L > 1 && arr.length) {

        what = a[--L];

        while ((ax = arr.indexOf(what)) !== -1) {

            arr.splice(ax, 1);

        }

    }

    return arr;

}





function notification_effect() {

    var notifications = jQuery('.notifications');

    var container = jQuery(".notifications ul");

    var notification_number = jQuery('.notifications-number');



    jQuery(document).on('click', function (e) {

        var target = e.target;

        if (jQuery(target).hasClass('notifications')) {

            container.toggle();

            jQuery(notification_number).addClass('hidden').html("");
        } else
        {
            if (!jQuery(target).is('a'))
            {
                container.hide();
            }
        }
    });
}

$("body").on("click", ".hp-load-more", function ()
{
    $.post("/load-more", {"limit": 15, "filters": jobs_params, "existing": $('.hp-offer-content > div').length}, function (data)
    {
        $(".hp-offer-content").append(data);
        if (data == '')
        {
            $(document).scroll(function (e) {});
        }
    });
});

$("body").on("click", ".news-load-more", function ()
{
    $.post("/load-more-news", {"limit": 4, "category": category_slug, "existing": $('.news-listing > a').length}, function (data)
    {
        $(".news-listing").append(data);
        if (data == '')
        {
            $(document).scroll(function (e) {});
        }
    });
});

$('body').on('hide.bs.modal', function ()
{
    if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') == false)
    {
        $('html').addClass('modal-open');
    } else if ($('.modal:visible').size() <= 1)
    {
        $('html').removeClass('modal-open');
        $(this).removeData('bs.modal');
    }
});

$('body').on('show.bs.modal', '.modal', function ()
{
    if ($(this).hasClass("modal-scroll"))
    {
        $('body').addClass("modal-open-noscroll");
    }
});

$('body').on('hidden.bs.modal', '.modal', function ()
{
    $('#preview-modal').html("");
    $('body').removeClass("modal-open-noscroll");
});

// $(document).on('click', '[data-toggle="modal"]', function (e)
// {
//     e.preventDefault();
//     var target = $(this).attr("href");
//     $("#preview-modal").load(target, function ()
//     {
// //        console.log(target);
//         $("#preview-modal").modal("show");
//     });
// });
$(document).on('click', '.job-alert-box-recommend', function (e) {
    e.preventDefault();
    var target = $(this).find('a').attr('href');
    $("#preview-modal").load(target, function ()
    {
//        console.log(target);
        $("#preview-modal").modal("show");
    });
    return false;
})
$(document).on('click', '.job-recommend-box-apply-job', function (e) {
    e.preventDefault();
    var target = $(this).find('a').attr('job');
    $("#preview-modal").load(target, function ()
    {
//        console.log(target);
        $("#preview-modal").modal("show");
    });
})
$(document).on('click', '.job-recommend-box-apply-job-notlogged', function (e) {
    e.preventDefault();
    window.location.href = "/login";
})

$("body").on("click", ".client-star-rating img", function (e)
{
    var score = $(".client-star-rating > input").val();
    var url = $("a.rate-company").attr('href') + "&score=" + score;
    $("a.rate-company").attr('href', url);
    $("a.rate-company").click();

});

$("body").on("submit", ".ajaxify", function (e)
{
    e.preventDefault();
    var form = $(this).serializeArray();
    var method = $(this).attr("action");
    var showAlert = $(this).hasClass("show-alert")
    $.post(method, form, function (data)
    {
        if (data.msg == 'ok')
        {
            if (showAlert)
            {
                $("div.alert-success").removeClass("hidden");
            } else
            {
                $("button.close").click();
            }
        } else
        {
            alert(data.error);
        }
    }, 'json');

    return false;
});
//
// $("body").on("click", ".submit-skills", function (e)
// {
//     e.preventDefault();
//     var skills = [];
//     $("ul.skill-tree > li").each(function ()
//     {
//         skills.push($(this).attr("rel"));
//     });
//
//     if (skills.length > 0)
//     {
//         $.post("/add-skills", {"skills": skills}, function (data)
//         {
//             $("div.alert").text("Skills save successfully").toggle();
//             $("ul.skill-tree > li").remove();
//         });
//     }
//
//     return false;
// });

$("body").on("click", ".message_check", function ()
{
    $(".send_message.space10").text("");
});


$(document).on("click", ".google-open", function ()
{
    gapi.client.setApiKey(apiKey);
    window.setTimeout(checkAuth, 3);
});

function checkAuth()
{
    gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
}


function handleAuthResult(authResult)
{
    if (authResult && !authResult.error)
    {
        $(".cont").hide();
        $(".div").show();

        $.get("https://www.google.com/m8/feeds/contacts/default/full?alt=json&access_token=" + authResult.access_token + "&max-results=700&v=3.0", function (response)
        {
            if (response.feed.entry.length > 0)
            {
                $(".email-results").removeClass("hidden");
                $(".send_invite").removeClass("hidden");
                console.log(response);
                var email = [];
                var pic = [];
                var surya = [];
                var licontent = "";
                console.log(response.feed.entry.length);

                for (var i = 0; i < response.feed.entry.length; i++)
                {
                    console.log(i);
                    if (response.feed.entry[i].gd$phoneNumber)
                    {
                        continue;
                    }

                    if (response.feed.entry[i].gd$email)
                    {
                        email[i] = response.feed.entry[i].gd$email[0].address;
                        console.log(email[i]);
                        $(".email-results ul").append("<li><input type='checkbox' class='email-check' name='email_check[]' value='" + email[i] + "'> " + email[i] + "</li>");
                    } else
                    {
                        continue;
                    }
                }
            } else
            {
                alert("something went wrong ! your contact list is undefined");
            }
        });
    }
}
if (typeof (jobs_params) !== "undefined")
{
    if (jobs_params.length > 0)
    {
        $(".table-tags").show();
    }
}

$("body").on('change', '#search_credits, #job_credits', function (e)
{
    form = $(".price_configurator").serializeArray();
    $.post("/ajax/index/get-package-price", form, function (data)
    {
        $(".total-value").html(data.total);
        $("#total_value").val(data.total_amount);
    }, 'json');
    return false;
});

$("body").on("click", ".deleteFilter", function ()
{
    var rel = $(this).attr("rel");
    $("." + rel + "").remove();
    $(this).parent("span").remove();
})

$("body").on('change', '.price_configurator #search_credits', function (e)
{
    e.preventDefault();
    form = $(".price_configurator").serializeArray();
    $.post("/ajax/index/get-package-price", form, function (data)
    {
        $("#total_price").val(data.val);
    }, 'json');
    return false;
});

$("body").on("click", ".delete_item", function ()
{
    return confirm("Are you sure you want to delete this item?");
});
$("body").on("click", ".delete-user-account", function ()
{
    return confirm("Are you sure you want to delete your profile ?");
});

$("body").on("click", ".close-modal", function (e)
{
    e.preventDefault();
    $("#preview-modal").modal("hide");
    $(this).removeData('bs.modal');
    return false;
});

$("body").on("click", ".edit-positions", function ()
{
    $(".edit-position").toggle();
    $(".add-position").toggle();
    if ($(this).text() == "Edit")
    {
        $(this).text("Done");
    } else
    {
        $(this).text("Edit");
    }
});

$("body").on("click", ".edit-certifications", function ()
{
    $(".edit-certification").toggle();
    $(".add-certification").toggle();
    if ($(this).text() == "Edit")
    {
        $(this).text("Done");
    } else
    {
        $(this).text("Edit");
    }
});

$("body").on("click", ".edit-educations", function ()
{
    $(".edit-education").toggle();
    $(".add-education").toggle();
    if ($(this).text() == "Edit")
    {
        $(this).text("Done");
    } else
    {
        $(this).text("Edit");
    }
});

$("body").on("click", ".edit-hobbies", function ()
{
    if ($(this).text() == "Edit")
    {
        $(this).text("Done");
        $(".edit-hobby").toggle();
    } else
    {
        var scope = $("#scope").val();
        $.post("/profile", {"scope": scope}, function ()
        {
            $(".hobby-details").html(nl2br(scope));
            $(".edit-hobby").toggle();
        });

        $(this).text("Edit");
    }
});

$("body").on("click", ".edit-alerts", function ()
{
    $(".edit-alert").toggle();
    $(".add-alert").toggle();
    if ($(this).text() == "Edit")
    {
        $(this).text("Done");
    } else
    {
        $(this).text("Edit");
    }
});

function emailRecommend(obj)
{
    var action = $(obj).attr("action");
    var form = $(obj).serializeArray();

    $.post(action, form, function (data)
    {
        $(".recommend_email").addClass("hidden");
        $(".message_email").removeClass("hidden").text(data.msg);
    }, 'json');
    return false;
}

$("body").on("click", ".delete-ajax", function (e)
{
    e.preventDefault();
    if (confirm("Are you sure you want to delete this item?"))
    {
        $(this).parent("span").remove();
        $.get($(this).attr("href"));
    }
    return false;
});

// $("body").on('submit', '.ajaxformprofile', function (e)
// {
//     e.preventDefault();
//     var form = $(this).serializeArray();
//     var action = $(this).attr("action");
//     var form_type = $(this).attr("type");
//
//     $.post(action, form, function (data)
//     {
//         if (data.msg == 'ok')
//         {
//             $("#preview-modal").modal("hide");
//             switch (form_type)
//             {
//                 case "alert":
//                     window.location.reload();
//                     break;
//
//                 default:
//                     window.location.reload();
//                     break;
//             }
//
//         }
//     }, 'json');
//     return false;
// });

function nl2br(str, is_xhtml)
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

$("body").on("click", ".see-all-reviews", function ()
{
    $(this).remove();
    $(".job-testimonial").removeClass("hidden");
});

$("body").on("change", ".load-cities", function ()
{
    var value = $(this).val();
    $(".loaded-city").html("");
    var options = "";
    $.post("/ajax/index/get-cities", {"id_country": value}, function (data)
    {
        $.each(data.cities, function (key, value)
        {
            if (key > 0)
            {
                $(".loaded-city").append("<option value='" + key + "'>" + value + "</option>");
            }

        });

    }, 'json');
    $(".loaded-city").html(options);

    if ($(".loaded-city").hasClass("select2"))
    {
        $(".loaded-city").select2();
    }
});


$("body").on("change", ".estimate", function ()
{
    var form = $(this).serializeArray();
    $.post("/exclusive?est=1", form, function (data)
    {
        $(".estimated-reach").text(data.count);
    }, 'json')
});


$("body").on("change", ".exclusive-countries", function ()
{
    var value = $(this).val();
    $(".exclusive-cities").html("");
    $.post("/ajax/index/get-cities", {"id_country": value}, function (data)
    {
        $.each(data.cities, function (key, value)
        {
            if (key > 0)
            {
                $(".exclusive-cities").append("<option value='" + key + "'>" + value + "</option>");
            }

        });

    }, 'json');
    $(".exclusive-cities").select2();
});

$(".select2").select2();

$("body").on("change", ".supervise-action", function ()
{
    $(".supervision").toggle();
});

$("body").on("change", ".studies-select", function ()
{
    $(".name-studies").text($(this).find(":selected").attr("title"));
    $(".studies-block").css("display", "block");
});

$("body").on("click", ".configure-package", function ()
{
    $(".price_configurator").submit();
});

$("body").on("keyup", "#addSkill", function ()
{
    $(".skills-search").html("");
    var searchmenu = $(".skills-search");
    $.post("/ajax/index/get-skills", {"search": $(this).val()}, function (data)
    {
        $.each(data.skills, function (key, value)
        {
            var li = $('<li/>').addClass('search-item').text(value).appendTo(searchmenu);
        });

    }, 'json');
    $(".skills-search").removeClass("hidden");
});

$("body").on("click", ".search-item", function ()
{
    var value = $(this).text();
    $("#addSkill").val(value);
    $("#idSkill").val(value);
    $(".skills-search").addClass("hidden");
});

$("body").on("keypress", ".search-key", function (e)
{
    if (e.keyCode == 13) {
        $(".submit-form-search").click();
    }
});

$("body").on("keyup", ".search-key", function (e)
{
    $(".search-field-form").val($(this).val());

    if (e.keyCode == 13) {
        $(".submit-form-search").click();
    }
});
$("body").on("submit", ".recommendForm", function (e)
{
    var form = $(this).serializeArray();
    var action = $(this).attr("action");
    $.post(action, form, function (data)
    {
        if (data.msg == 'ok')
        {
            window.location.href = data.url;
        } else
        {
            alert(data.text);
        }
    }, 'json');
});
var selector = '.br-navigation li';

$(selector).on('click', function () {
    $(selector).removeClass('active');
    $(this).addClass('active');
});

var recommend = $('.recommend');

recommend.on('click', function () {
    $(this).parent().parent().css({'background': '#7dcc93'}).first().css({'color': '#FFFFFF'}).find('strong').css({'color': '#FFFFFF'});
//    $(this).remove();
    $(this).parent().html('<span class="checked"></span>');
});
if(window.location.pathname.split("/")[1] == "profile") {
    $("#menu-nav ul li").click(function (e) {
        var hash = $(this).find("a").attr("href");
        window.location.hash = hash;
        e.preventDefault();
        $('a[href="' + location.hash + '"]').tab('show');
        $(".tab-pane").removeClass("in active");
        $(hash).addClass("in active");


//    $(this).tab('show');
    });

}


if (location.hash !== '') {
    var hash = location.hash ;
    hash = hash.substring(2);
      $('a[href="#'+hash+'"]').tab('show');
}

//if (location.hash !== '')
//    $('a[href="' + location.hash + '"]').tab('show');
//$('#menu-nav > ul > li').click(function (e) {
//    location.reload();
//    e.preventDefault();
//    $(location).attr('href', $(this).find('a').attr('href'));
//
//
//})


Dropzone.autoDiscover = false;
$("div.dropzone").dropzone({
    url: "/upload-profile-photo",
    maxFiles: 1,
    complete: function () {
        window.location.reload();
    },
    dictDefaultMessage: "<div id='EN' class='brain-msg' style='margin-top: 30px;display:none'>\n\
<p>Drag and Drop the file here or use the button below</p><p>Profile picture should be at least 400 x 400px</p></div>\n\
<div id='RO' class='brain-msg' style='margin-top: 30px; display:none'>\n\
<p>Adauga poza apasand butonul sau trage direct imaginea in campul delimitat</p><p>Poza de profil trebuie sa aiba o dimensiune minima de 400 x 400px</p></div><div><a class='drop-z-selectfile' href='javascript:;'>Select file</a></div>\n\
<div style='text-align:center'></div>",
});
var dragdrop = $('._iiutyrt').html();

//window.onload = function(){
 $("#"+dragdrop).show();
//};

$('a#uploadImageBtn').click(function()
{
    $('#profile-dropzone').click();
})
$("body").on("click", ".trigger-cv-upload", function ()
{
    $("input#cv").click();
//console.log('lll');
    $('.remove-cv').toggleClass('hidden');
});
$("body").on("click", ".trigge-cv-upload", function ()
{
//   $("input#cv").click();
//console.log('lll');
    $('.remove-cv').toggleClass('hidden');
});


$("body").on("click", ".edit-profile-button", function ()
{
    var $self = $(this);
    var originalText = $self.text().trim();
    $(".edit-profile-button").text("Close Edit");
    $(".edit-profile-button").css({'width': '100px'});
    $(".edit-profile-button").css({'background': '#f0ad4e'});
    if (originalText == "Close Edit") {
        $self.text("Edit");
        $self.css({'width': ''});
        $(".edit-profile-button").css({'background': '#7dcc93'});
    }

    $('.delete-edit-btn').toggleClass('hidden');
    $('nav#profile-skills ul li').toggleClass('fixing-2');
});

$("body").on("change", "input#cv", function ()
{
    // $("form#upload_cv").submit();
    $("#submit-cv-upload").click();
});

$("body").on("click", ".submit-profile", function (e)
{
    e.preventDefault();
    e.stopPropagation();
    console.log('hahahah')
    $("#profile_info").submit();
    if (typeof(localStorage.getItem("redirect")) !== "undefined") {
        var strg = localStorage.getItem("redirect");
        strg = JSON.parse(strg);
        var d1 = strg.expire;
        var d2 = new Date().getTime();
        if(d2 < d1)
        {
            // console.log(123);
            window.location.href = strg.url
        } else {
            // console.log(456);
            location.reload();
        }


    } else {
        location.reload();
    }
});
$("body").on("click", ".submit-pwd", function (e)
{
    e.preventDefault();

    var new_password = $("#new-password").val();
    var retype_password = $("#retype-password").val();

    if(new_password === retype_password)
    {
        var status = new_password.match(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/);
        if(status) {
             $("#change_password").submit();
        } else {
            alert('Password requires a length of at least eight characters and that the password contains characters of at least three of the following four classes:uppercase characters, lowercase characters, special characters and numbers.');
            return false ;
        }
    } else
    {
        alert("password not match");
        return false ;
    }

//    $("#change_password").submit();
});
$('.remove-cv').click(function () {
    var user_id = $(this).data('user-id');
    var cv_id = $(this).data('cv-id');

    var a = confirm('Are you sure for removing this CV');
    if (a) {
        $.ajax({
            url: "removecv",
            type: "POST",
            data: {id: user_id, cv: cv_id},
            success: function (data) {
                location.reload();
//               console.log(data);
            }
        })
    }
});


function icnClick() {
    $('a[href="#settings-view"]').tab('show');
    window.location.hash = 'settings-view';
    var url = window.location.href;
    if (url.split('#')[1] == 'settings-view') {
        setTimeout(function () {
            $('.dropzone > .dz-default').find('button').click();
        }, 1000)

    }
}

function position(id) {
    if (confirm("Are you sure you want to delete this Job History?"))
    {
        $.ajax({
            type: "POST",
            data: {id: id},
            url: "removepositions",
            success: function () {
                location.reload();
            }
        })
    }
}

function removeSkills(id) {
    if (confirm("Are you sure you want to delete this Skill")) {

        $.ajax({
            type: "POST",
            data: {id: id},
            url: "/removeskill",
            success: function (data) {
                location.reload();
            }
        })

    }
}
function removeCertification(id) {
    if (confirm("Are you sure you want to delete this Skill")) {

        $.ajax({
            type: "POST",
            data: {id: id},
            url: "removecertification",
            success: function () {
                location.reload();
            }
        })

    }
}
function removeEducation(id) {
    if (confirm("Are you sure you want to delete this Skill")) {

        $.ajax({
            type: "POST",
            data: {id: id},
            url: "removeeducation",
            success: function () {
                location.reload();
            }
        })

    }
}
$('body').on('click', '#view-alert-jobs', function () {
    var data = $(this).data('rel');
    var divClone = $("#render-job-alert").clone()
    $.ajax({
        type: "POST",
        data: {data: data},
        url: "/jobalert",
        success: function (data) {
            $('#render-data').append(data);
            $('#myModal').modal('show');
            $('[data-dismiss="modal"]').on('click', function () {
                $("#render-job-alert").replaceWith(divClone.clone());
            })
//            $("#render-job-alert").replaceWith(divClone.clone());
        }
    })
});

$('body').on('click', ".agree-term", function () {
    var link = window.location.href;
    $.ajax({
        type: "POST",
        data: {agree: 1,link:link},
        url: "/term",
        success: function (data) {
            console.log(data);
            var result = $.parseJSON(data);
            window.location.href = result.url;
//            $('.job-alert-box-recommend').fadeIn();
//            $('.job-recommend-box-apply-job').fadeIn();
//             window.location.reload();
        },
    })
})

































$(document).ready(function () {
    //rotation speed and timer
    var speed = 5000;

    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();


    //if user clicked on prev button

    $('#buttons a').click(function (e) {
        //slide the item

        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }

        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }

        //cancel the link behavior
        return false;

    });

    //if mouse hover, pause the auto rotation, otherwise rotate it
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });


    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }

});
//a simple function to click next link
//a timer will call this function, and the rotation will begin

function rotate() {
    $('#next').click();
}
