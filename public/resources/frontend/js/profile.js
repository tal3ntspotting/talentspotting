$(document).ready(function() {
    if ($('#signature').length > 0) {
                
            CKEDITOR.replace('signature', {
                height: 170,
        toolbar:
                [
                    {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                    {name: 'basicstyles', items: ['Bold', 'Italic']},
                    {name: 'tools', items: ['Maximize', '-', 'About']}
                ]
    });
}

});