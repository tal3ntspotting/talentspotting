<?php
class Jobs_AdminController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Jobs;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
    
    public function viewAction()
    {
        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "view";
        $filters = ($this->_model->_user->type == 2) ? array("id_company" => array($this->_model->_user->id_company)) : array();
        $this->view->items = $this->_model->getJobs($filters);
        $this->view->views = $this->_model->getJobsViewsCounted();
        $this->view->applications = $this->_model->getJobsApplicationsCounted();        
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "jobs_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Jobs | '.$this->_settings['sitename'].'');
    }
    
    public function editAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        if ($this->_model->_user->id_company > 0 && $this->_model->_user->type == 2)
        {
            $company = $this->_model->getCompanyById($this->_model->_user->id_company);
            if (($id < 1 && $company['job_credits'] < 1) || $company['status'] == 0)
            {
                $this->_flashMessenger->addMessage('no_job_credits');
                $this->_redirect("/view-jobs");
            }
        }
        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "edit";
        
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getJobById($id);
            
            if ($this->_model->_user->id_company > 0
                && $data['id_company'] != $this->_model->_user->id_company
                && $this->_model->_user->type == 2)
            {
                throw new Zend_Controller_Action_Exception('Authorization failed', 403);
                exit();
            }
            
            $data['skills'] = $this->_model->getJobSkills($id);
            $data['cities'] = $this->_model->getJobCities($id);
            $data['departments'] = $this->_model->getJobDepartments($id);
            $data['industries'] = $this->_model->getJobIndustries($id);
            $data['id_lang'] = $this->_model->getJobLanguages($id);
            
            
            $data['skills'] = array_keys($data['skills']);
            $data['cities'] = array_keys($data['cities']);
            $data['departments'] = array_keys($data['departments']);
            $data['industries'] = array_keys($data['industries']);
            $data['id_lang'] = array_keys($data['id_lang']);
        }
        
        
        $form = new modules_jobs_forms_EditJob(array('disableLoadDefaultDecorators' => true, 'company' => (array) $company));		
        $form->populate($data);
        
        $this->view->id = $id;
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
//            echo '<pre>';print_r($_POST); die;
            $this->_model->save($_POST);
            $this->_redirect("/view-jobs");
        }        
        
        $this->view->headTitle()->set('Add/Edit Job | '.$this->_settings['sitename'].'');
    }
    
    public function jobDetailsAction()
    {
        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "view";
        $this->view->headTitle()->set('Job Applications | '.$this->_settings['sitename'].'');
        
        $id_app = $this->_params['id_app'];
        
        if ($id_app > 0)
        {
            $this->_model->update(array("viewed" => 1), "id = {$id_app}", "job_applications");
        }
        
        $id = $this->_params['id'];
        $job = $this->_model->getJobById($id);
        $application = $this->_model->getJobApplicationById($this->_params['id_application']);
        $modelCompany = new models_Company;
        $company = $modelCompany->getCompanyById($job['id_company']);
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->update(array("status" => $this->_params['status'], "status_reason" => $this->_params['status_reason'], "pending" => 0), "id = '{$this->_params['id_application']}'", "job_applications");
            if ($this->_params['status'] == 4 && $this->_params['status_reason'] == 5)
            {
                $this->_model->insert(array("id_user" => $application['id_user'], "action" => 5, "points" => $this->_model->_stats['complete_application'], 'date' => date("Y-m-d H:i:s")), "gamification_points");
                $this->_model->insert(array("id_user" => $this->_model->_user->id, "action" => 5, "points" => $this->_model->_stats['complete_application'], 'date' => date("Y-m-d H:i:s")), "gamification_points");
                $this->_model->_stats->setNotification($application['id_user'], "user", "jobs", "Congratulations for your new career start!<br>Help your friends as well.<br>Recommend a job now!");
            }
            
            if ($this->_params['status'] == 4 && $this->_params['status_reason'] == 0)
            {
                $this->_model->_stats->setNotification($application['id_user'], "user", "profile", "<strong>Great news!</strong><br>Company {$company['title']} invited you to an interview.<br>Best of luck!");
            }
            
            $this->_flashMessenger->addMessage('status_changed');
            $this->_redirect("/job-details/?id={$this->_params['id']}");
        }
        
        $this->view->job = $job;
        if ($this->_params['location'] == 'applicant')
        {
            $this->view->items = $this->_model->getJobApplicationsDetails($id);    
            $filters['location'] = 'applicant';
        }
        else if ($this->_params['location'] == 'database')
        {
            $items = $this->_model->getJobApplicationsDetails($id);
            foreach ((array)$items  as $item)
            {
                $users[] = $item['id'];
            }
            $this->view->database = $this->_model->getSimilarUsersForJob($job, $users);
            $filters['location'] = 'database';    
        }
        else
        {
            $this->view->items = $this->_model->getJobApplicationsDetails($id);
            foreach ((array)$this->view->items as $item)
            {
                $users[] = $item['id'];
            }
            $this->view->database = $this->_model->getSimilarUsersForJob($job, $users);
            $filters['location'] = ''; 
        }
        $this->view->filters = $filters;
    }
    
    public function statsAction()
    {
        $this->view->current_menu = "statistics";
        $this->view->current_sub_menu = "jobs";  
        
        
               
        $id = (isset($this->_params['id']) && $this->_params['id'] > 0) ? $this->_params['id'] : null;        
        $filters['id_department'] = $this->_params['id_department'];
        $filters['status'] = $this->_params['status'];
        $filters['start_date'] = $this->_params['start_date'];
        $filters['end_date'] = $this->_params['end_date'];
        $filters['job_start_date'] = $this->_params['job_start_date'];        
        $filters['id_skill'] = $this->_params['id_skill'];
        $filters['id_industry'] = $this->_params['id_industry'];        
        $filters['status'] = $this->_params['status'];
         
        if ($id > 0 || !empty($filters))
        {
            $job = $this->_model->getJobById($id);
            $this->view->medium = $this->_model->getJobApplicationsMedium($id, $filters);
            $this->view->canceled_positions = $this->_model->getJobCanceledPositions($id, $filters);
            $this->view->candidates_per_status = $this->_model->getJobCandidatesPerStatus($id, $filters);
            $this->view->referrals_per_status = $this->view->candidates_per_status;
            $this->view->job_status = $this->_model->getCurrentJobStatusesGraph($id, $filters);
            $this->view->job_virality = $this->_model->getJobVirality($id, $filters);
            $this->view->skill_bars = $this->_model->getSkillBars($id, $filters);
            $this->view->top3 = $this->_model->getTop3Jobs($id, $filters);
        }
        else
        {
            $this->view->medium = $this->_model->getJobApplicationsMedium();    
            $this->view->canceled_positions = $this->_model->getJobCanceledPositions();
            $this->view->candidates_per_status = $this->_model->getJobCandidatesPerStatus();
            $this->view->referrals_per_status = $this->view->candidates_per_status;
            $this->view->job_status = $this->_model->getCurrentJobStatusesGraph();
            $this->view->job_virality = $this->_model->getJobVirality();
            $this->view->skill_bars = $this->_model->getSkillBars();
            $this->view->top3 = $this->_model->getTop3Jobs(s);
        }
        
        $this->view->id = $id;
        $this->view->id_department = $filters['id_department'];
        $this->view->id_skill = $filters['id_skill'];
        $this->view->id_industry = $filters['id_industry'];
        $this->view->start_date = $filters['start_date'];
        $this->view->end_date = $filters['end_date'];
        $this->view->job_start_date = $filters['job_start_date'];
        $this->view->status = $filters['status'];
        $this->view->jobs = $this->_model->getJobsSelect();
        $this->view->departments = $this->_model->getDepartmentsSelect();
        $this->view->skills = $this->_model->getSkillsSelect();
        $this->view->industries = $this->_model->getIndustriesSelect();
        $this->view->job_departments = $job['departments'];
        $this->view->job_skills = $job['skills'];
        $this->view->job_industries = $job['industries'];
        
        $this->view->headTitle()->set('View Jobs Stats | '.$this->_settings['sitename'].'');
    }
    
    public function bannersAction()
    {
        $this->view->current_sub_menu = "banners";
        $this->view->current_menu = "jobs";
        
        $this->view->items = $this->_model->getBanners();
        
        $this->view->headTitle()->set('View Banners | '.$this->_settings['sitename'].'');
    }
    
    public function editBannerAction()
    {
        $id = $this->_params['id'];        
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getBannerById($id);            
        }
        
        
        $form = new modules_jobs_forms_EditBanner(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->id = $id;
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveBanner($_POST);
            $this->_redirect("/jobs/index/banners");
        }
        
        $this->view->current_sub_menu = "banners";
        $this->view->current_menu = "jobs";        
        
        $this->view->headTitle()->set('Add/Edit Banner | '.$this->_settings['sitename'].'');
    }
    
    public function deleteBannerAction()
    {
        $id = $this->_params['id'];
        $this->_model->update(array("status" => 0), "id = {$id}", "banners");
        $this->_redirect("/banners");
    }
    
    public function activateBannerAction()
    {
        $id = $this->_params['id'];
        $this->_model->update(array("status" => 1), "id = {$id}", "banners");
        $this->_redirect("/banners");
    }
    
    public function embedAction()
    {
        if ($this->getRequest()->isPost())
        {
            $url = APPLICATION_URL."/embed?";
            foreach ((array) $_POST as $key => $values)
            {
                foreach ((array) $values as $value)
                {
                    $url .= "&{$key}[]={$value}";    
                }
                
            }
            
            $code = '<iframe src="'.$url.'" width="800" frameborder="0"></iframe>';
            
            echo json_encode(array("code" => $code));
            exit();
            
        }
        $modelUser = new models_User;
        $modelContent = new models_Content;
        $modelCompany = new models_Company;
        
        $this->view->skills = $modelContent->getSkillsSelect();
        $this->view->countries = $modelContent->getCountriesSelect();
        $this->view->cities = $modelContent->getCitiesSelect();
        $this->view->departments = $modelContent->getDepartmentsSelect();
        $this->view->industries = $modelContent->getIndustriesSelect();
        $this->view->companies = $modelCompany->getCompaniesSelect();
        
        $this->view->current_sub_menu = "embed";
        $this->view->current_menu = "users";
        
        $this->view->headTitle()->set('Create jobs embed | '.$this->_settings['sitename'].'');
    }
    
    public function deleteAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "jobs");
        $this->_redirect("/view-jobs");
    }
}