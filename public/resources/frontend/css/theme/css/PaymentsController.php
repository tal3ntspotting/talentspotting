<?php

class Jobs_PaymentsController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;
    public $translate;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_Payments;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;
        $this->view->headTitle()->set('Brainspotting');

        if ($this->_model->_user->type > 1) {
            $session = new Zend_Session_Namespace("redirector");
            $session->url = $this->getRequest()->getRequestUri();
        }
    }

    public function viewAction() {
        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "view";
        $this->view->items = $this->_model->getPayments($filters);
        $this->view->packages = $this->_model->getPackagesSelect();

        if (isset($this->_params['export'])) {
            $this->_model->export($this->view->items, "payments_list.csv");
        }

        if (isset($this->_params['export-pdf'])) {
            echo "<script>window.print();</script>";
        }

        $this->view->headTitle()->set('View payments | ' . $this->_settings['sitename'] . '');
    }

    public function packagesAction() {
        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "packages";
        $this->view->items = $this->_model->getPackages();

        if (isset($this->_params['export'])) {
            $this->_model->export($this->view->items, "payments_list.csv");
        }

        if (isset($this->_params['export-pdf'])) {
            echo "<script>window.print();</script>";
        }

        $this->view->headTitle()->set('View packages | ' . $this->_settings['sitename'] . '');
    }

    public function rechargeAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('frontend');

        $this->view->current_menu = "recharge";
        $this->view->current_sub_menu = "recharge";
        $this->view->packages = $this->_model->getPackagesDefault();
        $this->view->company = $this->_model->getCompanyById($this->_model->_user->id_company);

        $id_package = $this->_params['id_package'];
        $total_price = $this->_params['total_price'];
        $params = $this->_params;

        if ($id_package > 0 || $total_price != "") {
            if (!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->type != 2) {
                $this->_redirect("/register-company?id_package={$id_package}&total_price={$total_price}");
            } else {
                $this->_forward("billing", "payments", "jobs", array("params" => $params));
            }
        }


        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(12, 3));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(12, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(12, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(4);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(12, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(12, 2));
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(12, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(12, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(12, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function billingAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('frontend');
        $id_billing = $this->_params['id_billing'];
        $this->view->billing_info = $this->_model->getBillingInfo();
        $this->view->billing = $this->_model->getBillingData($id_billing);
        $this->view->id_billing = $this->view->billing['id'];

        if ($this->getRequest()->isPost()) {
            $company = $this->_model->getCompanyById($this->_model->_user->id_company);

            if ($this->_params['id_billing'] < 1) {
                $billingData['title'] = $this->_params['title'];
                $billingData['cui'] = $this->_params['cui'];
                $billingData['j'] = $this->_params['j'];
                $billingData['address'] = $this->_params['address'];
                $billingData['bank'] = $this->_params['bank'];
                $billingData['firstname'] = $this->_params['firstname'];
                $billingData['lastname'] = $this->_params['lastname'];
                $billingData['phone'] = $this->_params['phone'];
                $billingData['email'] = $this->_params['email'];
                $billingData['payment_method'] = $this->_params['payment_method'];
                $billingData['id_company'] = $this->_model->_user->id_company;

                $this->_model->insert($billingData, "payment_billing_info");
                $id_billing = $this->_model->lastInsertId();
            } else {
                $id_billing = $this->_params['id_billing'];
            }

            if ($this->_params['id_package'] < 1) {
                $package['title'] = webapp_tools::CreateAlias($company['title'] . "-" . $this->_params['job_credits'] . "-" . $this->_params['search_credits']);
                $package['job_credits'] = $this->_params['job_credits'];
                $package['search_credits'] = $this->_params['search_credits'];
                $package['amount'] = $this->_getPackageAmount($this->_params['job_credits'], $this->_params['search_credits']);
                $package['currency'] = 1;

                $this->_model->insert($package, "packages");
                $id_package = $this->_model->lastInsertId();
            } else {
                $id_package = $this->_params['id_package'];
                $package = $this->_model->getPackageById($id_package);
            }

            $payment['id_billing_info'] = $id_billing;
            $payment['id_company'] = $this->_model->_user->id_company;
            $payment['id_user'] = $this->_model->_user->id;
            $payment['id_package'] = $id_package;
            $payment['currency'] = $package['currency'];
            $payment['amount'] = $package['amount'];
            $payment['type'] = $this->_params['payment_method'];
            $payment['status'] = 'pending';



            $this->_model->insert($payment, "payments");
            $this->_model->update(array("had_demo" => 1), "id = {$payment['id_company']}", "companies");
            //$this->_model->update(array("job_credits" => $package['job_credits'] + $company['job_credits'], "search_credits" => $package['search_credits'] + $company['search_credits']), "id_company = {$company['id']}", "company_credits");

            switch ($this->_params['payment_method']) {
                case "op":
                    $msg = sprintf($this->translate->_("payment_pending"), round($package['amount']) . " USD", $package['job_credits'] + $company['job_credits'], $package['search_credits'] + $company['search_credits']);
                    break;

                default:
                    $msg = sprintf($this->translate->_("payment_success"), round($package['amount']) . " USD", $package['job_credits'] + $company['job_credits'], $package['search_credits'] + $company['search_credits']);
                    break;
            }


            $this->_model->insert(array("id_company" => $company['id'], "type" => "payment", "message" => "new_package_purchase", "url" => "view-payments"), "notifications");
            $this->_model->insert(array("id_company" => $company['id'], "target" => "admin", "type" => "payment", "message" => "new_package_purchase", "url" => "view-payments"), "notifications");

            $transport = null;
            if (!empty($this->_settings['email_smtp'])) {
                $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }

            $users = $this->_model->getAdmins();

            $mail = new WebApp_Mail();
            $mail->addTo($users[0]['email'], $users[0]['firstname']);
            if (count($users) > 1) {
                for ($i = 1; $i < count($users); $i++) {
                    $mail->addCc($users[$i]['email']);
                }
            }

            $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
            $mail->setSubject("New payment made");
            $mail->setBodyFromViewScript("payment.phtml", array("user" => $users[0], "company" => $company, "package" => $package, "settings" => $this->_settings, "payment" => $payment));
            $mail->send($transport);

            $this->_flashMessenger->addMessage($msg);
            if ($this->_params['acc'] == 'new') {
                $this->_redirect("/company");
            } else {
                $this->_redirect("/view-payments");
            }
        }

        $this->view->current_menu = "payments";

        $this->view->headTitle()->set('Billing | ' . $this->_settings['sitename'] . '');
    }

    private function _getPackageAmount($job_credits, $search_credits) {
        $modelPayments = new models_Payments;
        $package = $modelPayments->getDefaultPackage();

        $amount = 0;
        if ($job_credits > 0) {
            $amount += $this->_settings['costs']['job'] * $job_credits;
        }

        if ($search_credits > 0) {
            $amount += $this->_settings['costs']['search'] * $search_credits / 3;
        }

        return $amount;
    }

    public function editPackageAction() {
        $id = $this->_params['id'];

        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "edit-package";

        $data = array();

        if ($id > 0) {
            $data = $this->_model->getPackageById($id);
        }


        $form = new modules_jobs_forms_EditPackage(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->id = $id;
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $this->_model->savePackage($_POST);
            $this->_redirect("/view-packages");
        }


        $this->view->headTitle()->set('Add/Edit Package | ' . $this->_settings['sitename'] . '');
    }

    public function confirmAction() {
        $id = $this->_params['id'];
        $payment = $this->_model->getPaymentById($id);
        $package = $this->_model->getPackageById($payment['id_package']);
        $company = $this->_model->getCompanyById($payment['id_company']);

        $this->_model->update(array("job_credits" => $package['job_credits'] + $company['job_credits'], "search_credits" => $package['search_credits'] + $company['search_credits'], "highlight_credits" => $package['highlight_credits'] + $company['highlight_credits'], "added" => date("Y-m-d H:i:s")), "id_company = {$company['id']}", "company_credits");
        $this->_model->update(array("status" => "confirmed"), "id = {$id}", "payments");
        $this->_model->insert(array("id_company" => $company['id'], "type" => "notification", "message" => "payment_confirmed"), "notifications");
        $this->_redirect("/view-payments");
    }

    public function bountiesAction() {
        $this->view->items = $this->_model->getBounties();
        
        $modelsUser = new models_User;
        
        $this->view->bounties_to_approve = $modelsUser->getBountiesToApprove($this->_model->_user->id_company);
        $this->view->bounties_paid = $modelsUser->getPaidBounties($this->_model->_user->id_company);
            
            
        $this->view->current_menu = "bounties";
//        $this->view->current_sub_menu = "bounty";

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }

    public function payBountyAction() {
        $id = $this->_params['id'];
        $this->_model->payCompanyBounty($id);

        $this->_redirect("/bounties");
    }

    public function viewBountiesAction() {
        $this->view->items = $this->_model->getBounties();

        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "bounty";

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }

    public function confirmBountyAction() {
        $id = $this->_params['id'];
        $status = $this->_params['status'];

        $this->_model->payBounty($id, $status);

        $this->_redirect("/view-bounties");
    }

    public function bountyFilesAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = $this->_params['id'];
        $this->view->bounty = $this->_model->getBountyDetailed($id);
        $this->view->files = $this->_model->getBountyFiles($id);
    }

    public function deletePaymentAction() {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "payments");
        $this->_redirect("/view-payments");
    }

    public function deletePackageAction() {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "packages");
        $this->_redirect("/view-packages");
    }

    public function demoAction() {
        $company = $this->_model->getCompanyById($this->_model->_user->id_company);

        if ($company['had_demo'] < 1 && $company['id'] > 0) {
            $this->_model->update(array("credits" => 1, "job_credits" => 1, "search_credits" => 1, "highlight_credits" => 0, "added" => date("Y-m-d H:i:s")), "id_company = {$company['id']}", "company_credits");
            $this->_model->update(array("had_demo" => 1), "id = {$company['id']}", "companies");
            $this->_redirect("/dashboard");
        } else if (!isset($company['id'])) {
            $this->_flashMessenger->addMessage("You have to register a company first. <a href='/login-company'>Click here</a> to create a new company.");
            $this->_redirect("/recharge");
        } else {
            $this->_flashMessenger->addMessage("You already have a purchased package. You can try the demo only if you had no previous subscription.");
            $this->_redirect("/recharge");
        }
    }

    public function exclusiveAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelContent = new models_Content;

        $sql = $this->_model->select()->from(array("u" => "users"));
        $users = $this->_model->fetchAll($sql);


        $this->view->countries = $modelContent->getCountriesSelect();
        $this->view->cities = $modelContent->getCitiesSelect();
        /*
          foreach ($users as $user)
          {
          foreach ($this->view->countries as $key => $value)
          {
          if (stripos($user['location'], $value) !== false)
          {
          $data[$user['id']]['id_country'] = $key;
          }
          }
          foreach ($this->view->cities as $key => $value)
          {
          if (stripos($user['location'], $value) !== false)
          {
          $data[$user['id']]['id_city'] = $key;
          }
          }
          }

          foreach ((array) $data as $id_user => $value)
          {
          $this->_model->update($value, "id = {$id_user}", "users");
          }

          $this->_model->query("update users u set id_country = (select id_country from cities where u.id_city = id");update users u set id_country = (select id from countries where u.location = title);");
         */
        $this->view->skills = $modelContent->getSkillsSelect();
        $this->view->countries = $modelContent->getCountriesSelect();
        $this->view->cities = $modelContent->getCitiesSelect();
        $this->view->departments = $modelContent->getDepartmentsSelect();
        $this->view->industries = $modelContent->getIndustriesSelect();

        if ($this->getRequest()->isPost()) {
            if ($this->_params['est'] == 1) {
                $count = $this->_model->checkExclusive($_POST);
                echo json_encode(array("count" => $count));
                exit();
            } else {
                $this->_model->exclusiveMail($_POST, $this->view->skills, $this->view->countries, $this->view->cities, $this->view->departments, $this->view->industries);
                echo json_encode(array("msg" => "ok"));
                exit();
            }
        }
    }

}
