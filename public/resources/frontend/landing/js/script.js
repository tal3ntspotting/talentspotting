var reset;

function full_height() {
    var sectionHeight = $(window).height() - $('#header').height(),
            contentHeight = $('.vertical-center-wrap').height() + $('#header').height(),
            $secFullHeight = $("#menu-details .getheight");

    if ($(window).width() > 767) {
        $('#mobile-nav').slideUp();
        if ($(window).height() > contentHeight) {
            $secFullHeight.height(sectionHeight);
            $('#footer.fixed').css('position', 'fixed');

        } else {
            $secFullHeight.height('auto');
            $('#footer.fixed').css('position', 'static');

        }
    } else {
        $secFullHeight.height('auto');
        $('#footer.fixed').css('position', 'static');

    }
}

function menu_trigger() {
    //menu trigger
    $('.menu-trigger').on('click', function () {
        $('#mobile-nav').slideToggle();
    });
    jQuery('body').on('click', function(evt){    
       if(jQuery(evt.target).attr('id') != "mobile-nav" && jQuery(evt.target).attr('class') != 'menu-trigger' && jQuery(evt.target).attr('class') != 'fa fa-bars'){
            jQuery('#mobile-nav').slideUp();
       }
       console.log(jQuery(evt.target).attr('class'));
    });
}

function slider(element) {
    //slider 
    element.slick({
        slide: '.slider-item',
        dots: true,
        arrows: false,
        infinite: true,
        customPaging: function (a, b) {
            return '<span></span>'
        }

    });
}

function select_placeholder() {
    $(".select-placeholder select").change(function () {
        if ($(this).val() == "0")
            $(this).addClass("select-placeholder-on");
        else
            $(this).removeClass("select-placeholder-on")
    });
    $(".select-placeholder select").change();
}


$(document).ready(function ($) {
        if($(location).attr('pathname') == "/upload")  {
            $("#location").change(function() {
                var select_height = $(".select2-selection__rendered").height();
                console.log(select_height);
            })
        }
      $("div#profile-sidebar").slimscroll({
                height: $(window).height()+'px',
                wheelStep: 20
            });
    
    $(window).resize(function() {
        var width = $(window).width();
       if(width <= 765) {
            $(".slimScrollDiv").addClass("front-side");
          
       } else {
            $(".slimScrollDiv").removeClass("front-side");
       }
    }) ;
    
    $('.fbar-frontend-click').click(function () {
//        $("div#profile-sidebar").toggleClass("front-side");
        $(".slimScrollDiv").toggleClass("front-side");
//        $(".slimScrollBar").toggleClass("front-side");
    });
    
    if (!$('#fbar-frontend-click').hasClass("hidden")) {
        console.log(123);
    }

    
    
    $('#menu-nav > ul > li').click(function (e) {
//    location.reload();
    e.preventDefault();
//    $(location).attr('href', $(this).find('a').attr('href'));
     $(window).attr("location",window.location['origin']+"/"+$(this).find('a').attr('href'));


})
    
     $('.sidebar-toggle-box .fa-bars').click(function (e) {
           $(this).addClass('abc');

//            $(".leftside-navigation").niceScroll({
//                cursorcolor: "#1FB5AD",
//                cursorborder: "0px solid #fff",
//                cursorborderradius: "0px",
//                cursorwidth: "3px"
//            });

            $('#sidebar').toggleClass('hide-left-bar');
            $('.brand').toggleClass('blue');
            $('header').toggleClass('merge-left');
            $('#ora-logo').toggleClass('white');
            if($('#sidebar').hasClass('hide-left-bar')) {
                $('head').append('<style>.fa-bars:before{color:#FFF !important;}</style>');
            } else {
                 $('head').append('<style>.fa-bars:before{color:#34C3D8 !important;}</style>');
            }
//            if ($('#sidebar').hasClass('hide-left-bar')) {
//                $(".leftside-navigation").getNiceScroll().hide();
//            }
//            $(".leftside-navigation").getNiceScroll().show();
            $('#main-content').toggleClass('merge-left');
            $('section#jobs').toggleClass('merge-left');
            $('footer#footer').toggleClass('merge-left');
            $('section#search-bar').toggleClass('merge-left');
            e.stopPropagation();
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });
    if ($.fn.dcAccordion) {
        $('#nav-accordion').dcAccordion({
            eventType: 'click',
            autoClose: true,
            saveState: true,
            disableLink: true,
            speed: 'slow',
            showCount: false,
            autoExpand: true,
            classExpand: 'dcjq-current-parent'
        });
    }
    
    
    if ($(window).width() > 1440) {
        reset = 1;
    } else {
        reset = 0;
    }
    // call functions
    full_height();
    menu_trigger();
    select_placeholder();

    $(window).resize(function () {
        full_height();
    });

    $(".justified-icon-list").slick({
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        autoplaySpeed: 4000,
        autoplay: true,
        speed: 400,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
    });

    $('.justified-icon-list').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var next = $('div[data-slick-index="' + nextSlide + '"');
        var prev = $('div[data-slick-index="' + currentSlide + '"');
        next.removeClass('slick-disabled');
        prev.addClass('slick-disabled');
        next.click();
    });

    $(".justified-icon-list div.icon-lins").click(function ()
    {
        var rel = $(this).attr("data-slick-index");
        var content = $('p[rel="' + rel + '"]').html();
        var obj = $(this);

        $(".justified-icon-list div.icon-lins").each(function ()
        {
            if ($(this).attr("data-slick-index") != rel)
            {
                //alert($(this).attr("data-slick-index")+"-"+$(obj).attr("data-slick-index"));
                $(this).addClass('slick-disabled');
                if ($(this).hasClass('slick-enabled'))
                {
                    $(this).removeClass('slick-enabled');
                }
            } else
            {
                $(this).removeClass('slick-disabled');
                //$(this).addClass('slick-enabled');
            }

        });

        $(".slick-content").html(content);
        $('div[data-slick-index="' + rel + '"').removeClass('slick-disabled');
        //$('div[data-slick-index="'+rel+'"').addClass('slick-enabled');
    });

    $(".links-list").slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        autoplaySpeed: 4000,
        autoplay: true,
        speed: 400,
    });

    $('.links-list').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var next = $('li[data-slick-index="' + nextSlide + '"');
        var prev = $('li[data-slick-index="' + currentSlide + '"');
    });

    $('.links-list li').click(function ()
    {
        var object = $(this);
        var current_id = object.attr("id");
        var image = $('img.' + current_id + '').attr('src');
        var text = $('p[rel="' + current_id + '"]').text();
        var title = $('a[rel="' + current_id + '"]').text();
        $('.links-list li').each(function ()
        {
            $(this).removeClass('selected');
            var id = $(this).attr("id");
            var src = $('img.' + id + '').attr('src');
            if (src.search("-sel") > 0)
            {
                $('img.' + id + '').attr('src', src.replace("-sel", ""));
            }
        });

        object.addClass('selected');
        $('img.' + current_id + '').attr('src', image.replace(".png", "-sel.png"));
        $("h2.links").text(title);
        $("p.links").html(text);
        return false;
    })
    
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
    $("body").on("click", ".register-click", function (e)
    {
        e.preventDefault();

        var new_password = $("#new-password").val();
        var retype_password = $("#retype-password").val();
        var email = $("input[name='email']").val();

        if (new_password === retype_password)
        {
            
            //    ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$    6/char pattern
            //    ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}        8/char pattern + special char
            //    ^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$        8/char pattern + special char 3 claases from 4
            var status = new_password.match(/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/);
            if (status) {
                var terms = $("input[name='terms']").is(":checked");
                if(terms) {
                    if(email) {
                        if(validateEmail(email)) {
                            $("#register-form").submit();    
                        }else{
                            $("div.alert").remove();
                            $(".stepsregister").hide();
                            var template = '<div class="alert alert-block alert-danger fade in _yyrt65546"><button type="button" class="close close-sm" data-dismiss="alert">\n\
                                <i class="fa fa-times"></i></button> Please check your Email format ! </div>';
                            $('.box-login-head').append(template);
                   
                        }
                        
                    } else {
                        $("div.alert").remove();
                        $(".stepsregister").hide();
                        var template = '<div class="alert alert-block alert-danger fade in _yyrt65546"><button type="button" class="close close-sm" data-dismiss="alert">\n\
                    <i class="fa fa-times"></i></button> Email Shouldn\'t be empty </div>';
                        $('.box-login-head').append(template);
                    }
                     
                } else {
                    $("div.alert").remove();
                    $(".stepsregister").hide();
                    var template = '<div class="alert alert-block alert-danger fade in _yyrt65546"><button type="button" class="close close-sm" data-dismiss="alert">\n\
                    <i class="fa fa-times"></i></button> Please Check the terms & Conditions</div>';
                    $('.box-login-head').append(template);
                }
               
            } else {
                alert('Password requires a length of at least eight characters and that the password contains characters of at least three of the following four classes:uppercase characters, lowercase characters, special characters and numbers.');
                return false;
            }
        } else
        {
            $("div.alert").remove();
            $(".stepsregister").hide();
            var template = '<div class="alert alert-block alert-danger fade in _yyrt65546"><button type="button" class="close close-sm" data-dismiss="alert">\n\
                    <i class="fa fa-times"></i></button> Password not match</div>';
            $('.box-login-head').append(template);
            return false;
        }

//    $("#change_password").submit();
    });
});

$('body').on('hide.bs.modal', function ()
{
    if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') == false)
    {
        $('html').addClass('modal-open');
    } else if ($('.modal:visible').size() <= 1)
    {
        $('html').removeClass('modal-open');
        $(this).removeData('bs.modal');
    }
});

$('body').on('show.bs.modal', '.modal', function ()
{
    if ($(this).hasClass("modal-scroll"))
    {
        $('body').addClass("modal-open-noscroll");
    }
});

$('body').on('hidden.bs.modal', '.modal', function ()
{
    //alert("here");
    $('#preview-modal').html("");
    $('body').removeClass("modal-open-noscroll");
});

$("body").on('click', '[data-toggle="modal"]', function (e)
{
    e.preventDefault();
    var target = $(this).attr("href");
    $("#preview-modal").load(target, function ()
    {
        $("#preview-modal").modal("show");
    });
});
$("body").on("submit", ".ajaxify", function (e)
{
    e.preventDefault();

    if ($(this).hasClass("reset"))
    {
        $(".alert").removeClass("hidden");
    }

    if ($(this).hasClass("demo"))
    {
        $(".alert").removeClass("hidden");
        $(this).addClass("hidden");
    }

    var form = $(this).serializeArray();
    var method = $(this).attr("action");
    var showAlert = $(this).hasClass("show-alert")
    $.post(method, form, function (data)
    {
        if (data.msg == 'ok')
        {
            if (showAlert)
            {
                $("div.alert-success").removeClass("hidden");
            } else
            {
                $(".close-modal").click();
            }
        } else
        {
            alert(data.error);
        }
    }, 'json');

    return false;
});

$("body").on("click", ".close-modal", function (e)
{
    e.preventDefault();
    $("#preview-modal").modal("hide");
    $(this).removeData('bs.modal');
    return false;
});

$("body").on("click", ".view-more", function (e)
{
    if ($(this).prev().prev().hasClass('visible'))
    {
        $(this).prev().prev().removeClass('visible');
    } else
    {
        $(this).prev().prev().addClass('visible');
    }

    return false;
});

$("body").on("submit", ".recommendForm", function (e)
{
    var form = $(this).serializeArray();
    var action = $(this).attr("action");
    $.post(action, form, function (data)
    {
        if (data.msg == 'ok')
        {
            window.location.href = data.url;
        } else
        {
            alert(data.text);
        }
    }, 'json');

    return false;
});
var search = $(".input-search");
$('a.search-link').on('click', function () {
    
    search.toggleClass('show');
    search.on('click', function (e) {
        e.stopPropagation();
        search.css({'display': 'block !importnat'});
    });
});
$('a.filter-link').on('click', function () {
    $(this).toggleClass('active');
   $('.search-filters-input').slideToggle();
});

$(document).ready(function () {
    $("select.select2").select2();
//    var link = window.location.href.split()
})

// redirect when click on th box
$('body').on('click' ,'.bounty-link',function() {
   var $self = $(this) ;
   var link = $self.find('.br-link-detail > a').attr('href');
   // console.log(link);
   window.location.href = link ;
});

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if (!isMobile.any()) {
//    $("#job-mobile").parent().hide();
    $("#logout-mobile").parent().hide();
} else {
    window.onload = function () {
        $(".slimScrollDiv").addClass("front-side");
    }
}

//if (isMobile.any()) {
////    $("#job-mobile").parent().hide();
//    $("#logout-mobile").parent().hide();
//    window.onload = function () {
//        alert(123);
//      
//
//    }
//}

$(document).ready(function() {
    if(localStorage.getItem('register') == 1 && window.location.pathname != "/upload") { 
        window.location.href = "/upload";
    }
    if(localStorage.getItem('register') == 2 && window.location.pathname != "/final") { 
        window.location.href = "/final";
    }
    
    console.log(localStorage.getItem('register'));
    $('#register-form').submit(function() {
        if(!$('._yyrt65546').text().length > 0) {
            localStorage.setItem("register", 1);
        }
    });
    $('#upForm').submit(function() {
        localStorage.removeItem('register');
        localStorage.setItem("register", 2);
    });
    $('#final-signin-action').submit(function() {
        localStorage.removeItem('register');
    });
})


