function fnFormatDetails(oTable, nTr)
{
    var aData = oTable.fnGetData(nTr);
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
    sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
    sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
    sOut += '</table>';

    return sOut;
}

$(document).ready(function () {

   var dataTable = $('#dynamic-table, .dynamic-table').dataTable({
        "stateSave": true,
        "aaSorting": [],
//        "aoColumns": [{ "bSortable": false },{ "bSortable": false }],
        responsive: true,
        "oLanguage": {
            "sInfoEmpty": '',
            "sEmptyTable":"<span id='ro' style='display:none'>Nu exista date in tabel</span> <span id='en' style='display:none'>No data available in table</span>",
            "sInfo": "_START_ - _END_ of _TOTAL_",
            "sLengthMenu": "<select style='display:none' id='myDropDown' class='form-control selectpicker ro' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 randuri per pagina</option>" +
                    "<option id='stl2' value='25'>25 randuri per pagina</option>" +
                    "<option id='stl3' value='50'>50 randuri per pagina</option>" +
                    "<option id='stl4' value='100'>100 randuri per pagina</option>" +
                    "<option id='stl5' value='-1'>Toate</option>" +
                    "</select> <select style='display:none' id='myDropDown' class='form-control selectpicker en' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 rows per page</option>" +
                    "<option id='stl2' value='25'>25 rows per page</option>" +
                    "<option id='stl3' value='50'>50 rows per page</option>" +
                    "<option id='stl4' value='100'>100 rows per page</option>" +
                    "<option id='stl5' value='-1'>All</option>" +
                    "</select>",
            "oPaginate": {
                "sNext": "",
                "sPrevious": ""
            },
        },
        "sDom": 'Rfrtlip',
        "sDom" : 'rCt<"footer dt-footer"><"bottom"ilp><"clear">',
    });

var rowCount = $('#dynamic-table >tbody >tr').length;
if(rowCount <= 9 ) {
    $('select#myDropDown').attr('disabled','')
}
    $("input#searchbox").keyup(function () {
        dataTable.fnFilter(this.value);
    });

    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    nCloneTd.innerHTML = '<img src="/resources/frontend/images/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each(function () {
        this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#hidden-table-info tbody tr').each(function () {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [0]}
        ],
        "aaSorting": [[0, 'asc']],
        responsive: true
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $(document).on('click', '#hidden-table-info tbody td img', function () {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr))
        {
            /* This row is already open - close it */
            this.src = "/resources/frontend/images/details_open.png";
            oTable.fnClose(nTr);
        } else
        {
            /* Open this row */
            this.src = "/resources/frontend/images/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });

    $('.dynamic-table').dataTable({
        // "aaSorting": [[0, "desc"]],
        responsive: true
    });

    $(window).load(function () {
        $('table#dynamic-table').css({'margin-bottom': '0px'});
        $('table#dynamic-table + .footer').css({
            'width': $('#dynamic-table').width() + "px !important",
        });
        $(window).resize(function () {
            $('table#dynamic-table + .footer').css({
                'width': $('#dynamic-table').width() + "px !important",
            });
        });

        // fix user style
        var url = window.location.href;
        if (url.split('/')[3] == "view-users") {
            $('#dynamic-table_length').css({'bottom': '73px !important'});
//            $('#dynamic-table_length').addClass('fix');
        }
    })
    
    
    
    var usersTable = $('.users-table').dataTable({
        "processing": true,
        "serverSide": true,
         "ajax": "/user-pagination",
        "columns": [
            {"data": "id"},
            {"data": "image"},
            {"data": "firstname"},
            {"data": "lastname"},
            {"data": "departments"},
//            {"data": "custom_departments"},
            {"data": "adder_firstname"},
            {"data": "adder_lastname"},
            {"data": "email"},
            {"data": "last_login"},
            {"data": "options"},
        ],
        "aaSorting": [[0, "desc"]],
//        "aoColumns": [{ "bSortable": false },{ "bSortable": false }],
        responsive: true,
        "oLanguage": {
            "sInfoEmpty": '',
            "sEmptyTable": "<span id='ro' style='display:none'>Nu exista date in tabel</span> <span id='en' style='display:none'>No data available in table</span>",
            "sInfo": "_START_ - _END_ of _TOTAL_",
            "sLengthMenu": "<select style='display:none' id='myDropDown' class='form-control selectpicker ro' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 randuri per pagina</option>" +
                    "<option id='stl2' value='25'>25 randuri per pagina</option>" +
                    "<option id='stl3' value='50'>50 randuri per pagina</option>" +
                    "<option id='stl4' value='100'>100 randuri per pagina</option>" +
                    "<option id='stl5' value='-1'>Toate</option>" +
                    "</select> <select style='display:none' id='myDropDown' class='form-control selectpicker en' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 rows per page</option>" +
                    "<option id='stl2' value='25'>25 rows per page</option>" +
                    "<option id='stl3' value='50'>50 rows per page</option>" +
                    "<option id='stl4' value='100'>100 rows per page</option>" +
                    "<option id='stl5' value='-1'>All</option>" +
                    "</select>",
            "oPaginate": {
                "sNext": "",
                "sPrevious": ""
            },
        },
        "sDom": 'Rfrtlip',
        "sDom" : 'rCt<"footer dt-footer"><"bottom"ilp><"clear">',
    });
    
    
    
    
    
    
    
    $(".dataTables_paginate > ul > li").removeClass('disabled');
    // $(".dataTables_paginate > ul > li").one('click',function() {
    //     $(this).find('a').click();
    // });
    
    
    
    
    
    
    
    var leaderDoards = $('.leaderboard-ajax-table').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/users/admin/leaderboards-pagination",
        "columns": [
             {"data": "image"},
                    {"data": "name"},
           
            {"data": "total_points"},
//            {"data": "custom_departments"},
            {"data": "total_recommendation"},
            // {"data": "account_created"},
            {"data": "hires"},
            {"data": "real_views"},
            {"data": "total_shares"},
            {"data": "activate_account"},
            {"data": "extend_network"},
            {"data": "completed_profile"},

            // {"data": "successful_hire"},
            {"data": "new_applicants"},
            {"data": "new_candidates"},
//            {"data": "views"},
            
        ],
        "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == null) return '<img src="/resources/frontend/images/no-picture.jpg" class="_urytyy" alt="" />';
                    return '<img src="/resources/files/user/'+data+'" class="_urytyy" alt="" />' ;
                },
                "targets": 0
            },
        ],
//                    {"data": "lastname"},
//            {"data": "image"},
//            {"data": "total_points"},
////            {"data": "custom_departments"},
//            {"data": "total_recommendation"},
//            {"data": "account_created"},
//            {"data": "hires"},
//            {"data": "real_views"},
//            {"data": "total_shares"}
        
        
        "aaSorting": [[0, "desc"]],
//        "aoColumns": [{ "bSortable": false },{ "bSortable": false }],
        responsive: true,
        "oLanguage": {
            "sInfoEmpty": '',
            "sEmptyTable": "<span id='ro' style='display:none'>Nu exista date in tabel</span> <span id='en' style='display:none'>No data available in table</span>",
            "sInfo": "_START_ - _END_ of _TOTAL_",
            "sLengthMenu": "<select style='display:none' id='myDropDown' class='form-control selectpicker ro' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 randuri per pagina</option>" +
                    "<option id='stl2' value='25'>25 randuri per pagina</option>" +
                    "<option id='stl3' value='50'>50 randuri per pagina</option>" +
                    "<option id='stl4' value='100'>100 randuri per pagina</option>" +
                    "<option id='stl5' value='-1'>Toate</option>" +
                    "</select> <select style='display:none' id='myDropDown' class='form-control selectpicker en' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 rows per page</option>" +
                    "<option id='stl2' value='25'>25 rows per page</option>" +
                    "<option id='stl3' value='50'>50 rows per page</option>" +
                    "<option id='stl4' value='100'>100 rows per page</option>" +
                    "<option id='stl5' value='-1'>All</option>" +
                    "</select>",
            "oPaginate": {
                "sNext": "",
                "sPrevious": ""
            },
        },
        "sDom": 'Rfrtlip',
        "sDom" : 'rCt<"footer dt-footer"><"bottom"ilp><"clear">',
    });
    
    
    
    
    
    var campaign = $('#campaign-ajax-table').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/users/admin/campaign-reports",
        "cache": true,
        "columns": [
//             {"data": "id"},
             {"data": "url"},
             {"data": "recommenders"}
//                    {"data": "lastname"},
//           
//            {"data": "total_points"},
////            {"data": "custom_departments"},
//            {"data": "total_recommendation"},
//            {"data": "account_created"},
//            {"data": "hires"},
//            {"data": "real_views"},
//            {"data": "total_shares"},
//            {"data": "activate_account"},
//            {"data": "extend_network"},
//            {"data": "completed_profile"},
//            
//            {"data": "successful_hire"},
//            {"data": "new_applicants"},
//            {"data": "new_candidates"},
//            {"data": "views"},
            
        ],
        "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    return '<a href="'+data[1]+'">'+data+'</a>' ;
                },
                "targets": 0
            },
        ],
//                    {"data": "lastname"},
//            {"data": "image"},
//            {"data": "total_points"},
////            {"data": "custom_departments"},
//            {"data": "total_recommendation"},
//            {"data": "account_created"},
//            {"data": "hires"},
//            {"data": "real_views"},
//            {"data": "total_shares"}
        
        
        "aaSorting": [[0, "desc"]],
//        "aoColumns": [{ "bSortable": false },{ "bSortable": false }],
        responsive: true,
        "oLanguage": {
            "sInfoEmpty": '',
            "sEmptyTable": "<span id='ro' style='display:none'>Nu exista date in tabel</span> <span id='en' style='display:none'>No data available in table</span>",
            "sInfo": "_START_ - _END_ of _TOTAL_",
            "sLengthMenu": "<select style='display:none' id='myDropDown' class='form-control selectpicker ro' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 randuri per pagina</option>" +
                    "<option id='stl2' value='25'>25 randuri per pagina</option>" +
                    "<option id='stl3' value='50'>50 randuri per pagina</option>" +
                    "<option id='stl4' value='100'>100 randuri per pagina</option>" +
                    "<option id='stl5' value='-1'>Toate</option>" +
                    "</select> <select style='display:none' id='myDropDown' class='form-control selectpicker en' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
                    "<option id='stl' value='10'>10 rows per page</option>" +
                    "<option id='stl2' value='25'>25 rows per page</option>" +
                    "<option id='stl3' value='50'>50 rows per page</option>" +
                    "<option id='stl4' value='100'>100 rows per page</option>" +
                    "<option id='stl5' value='-1'>All</option>" +
                    "</select>",
            "oPaginate": {
                "sNext": "",
                "sPrevious": ""
            },
        },
        "sDom": 'Rfrtlip',
        "sDom" : 'rCt<"footer dt-footer"><"bottom"ilp><"clear">',
    });



    var settings_users = $('.settings-users-ajax-table').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/users/admin/users-pagination",
        "columns": [
            {"data": "checkbox"},
            {"data": "image"},
            {"data": "username"},
            {"data": "adder_name"},
            {"data": "email"},
            {"data": "type"},
            {"data": "last_login"},
            {"data": "options"}

        ],
        "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var t = '<div class="checkbox checkbox-primary">\n' +
                        '                                            <input value="'+row.id+'" id="checkbox'+row.id+'" class="styled" type="checkbox">\n' +
                        '                                            <label for="checkbox'+row.id+'">\n' +
                        '                                            </label>\n' +
                        '                                        </div>\n'
                    if(data == null) return t;
                    return t ;
                },
                "targets": 0,
                "orderable": false
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == null) return '<img src="/resources/frontend/images/no-picture.jpg" class="_urytyy" alt="" />';
                    return '<img src="/resources/files/user/'+data+'" class="_urytyy" alt="" />' ;
                },
                "targets": 1
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var dpdown = '' +
'' +
'' +
'' +
'<a href=\'/users/admin/ats-user?id='+row.id+'\' data-target="#myModal" data-toggle="modal" class="btn-edit"><i class="fas fa-edit"></i></a>\n' +
'\n' +
'<a href="/ajax/index/reminder?email='+row.email+'" data-toggle="modal" data-target="#myModal" class="btn-approuve"><i class="fas fa-envelope"></i></a>\n' +
'<a href="/delete-user?id='+row.id+'" class="delete_item btn-del"><i class="fas fa-trash"></i></a>\n' +
'' +
'';
                    if(data == null) return dpdown;
                    return dpdown ;
                },
                "targets": 7
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var type = "";
                    if(data == null) return 'no-type';
                    switch (data){
                        case(1):
                            return 'Recommender';
                            break;
                        case(2):
                            return 'Admin';
                            break;
                        case(9):
                            return 'Candidate';
                            break;
                        case(4):
                            return 'Recruiter';
                            break;
                        case(5):
                            return 'Hiring Manager';
                            break;
                        default:
                            return "-";
                            break;

                    }
                },
                "targets": 5
            },
        ],
        // "aaSorting": [[0, "desc"]],
//        "aoColumns": [{ "bSortable": false },{ "bSortable": false }],
        responsive: true,
        "oLanguage": {
            "sInfoEmpty": '',
            "sEmptyTable": "<span id='ro' style='display:none'>Nu exista date in tabel</span> <span id='en' style='display:none'>No data available in table</span>",
            "sInfo": "_START_ - _END_ of _TOTAL_",
            "sLengthMenu": "<select style='display:none' id='myDropDown' class='form-control selectpicker ro' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
            "<option id='stl' value='10'>10 randuri per pagina</option>" +
            "<option id='stl2' value='25'>25 randuri per pagina</option>" +
            "<option id='stl3' value='50'>50 randuri per pagina</option>" +
            "<option id='stl4' value='100'>100 randuri per pagina</option>" +
            "<option id='stl5' value='-1'>Toate</option>" +
            "</select> <select style='display:none' id='myDropDown' class='form-control selectpicker en' data-hide-disabled='true' data-show-subtext='true' data-live-search='true'>" +
            "<option id='stl' value='10'>10 rows per page</option>" +
            "<option id='stl2' value='25'>25 rows per page</option>" +
            "<option id='stl3' value='50'>50 rows per page</option>" +
            "<option id='stl4' value='100'>100 rows per page</option>" +
            "<option id='stl5' value='-1'>All</option>" +
            "</select>",
            "oPaginate": {
                "sNext": "",
                "sPrevious": ""
            },
        },
        "sDom": 'Rfrtlip',
        "sDom" : 'rCt<"footer dt-footer"><"bottom"ilp><"clear">',
    });
    $("input[id='searchbox']").on( 'keyup', function () {
        settings_users.dataTable().fnFilter(this.value);
    } );
    // settings_users.search($().val()).draw();




});