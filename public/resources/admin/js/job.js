
/*

$(document).on('keyup', '#s2id_autogen89', function (e) {

    if(e.keyCode != 40 && e.keyCode != 38)
                $("#skills").html("");

    if(e.target.value.length>=1 && e.keyCode != 40 && e.keyCode != 38)
        $.ajax({
            type: 'POST',
            url: '/get-skill',
            data: {skill:e.target.value},
        }) .done(function( data ) {

            var a = document.getElementById("skills");
            id = Object.keys(data);
            data = Object.values(data);
      //      $("#skills").html("");

            for(var i=0;i<id.length;i++)
            {
                var option = document.createElement("option");
                var newOption = new Option(data[i], id[i], false, false);
                $(a).append(newOption);
            }

            //$("#skills").trigger('change');


        });



});



$(document).on('keyup', '#s2id_autogen10', function (e) {
\\        if(e.keyCode != 40 && e.keyCode != 38)
                $("#skills").html("");

    if(e.target.value.length>=1 && e.keyCode != 40 && e.keyCode != 38)
        $.ajax({
            type: 'POST',
            url: '/get-skill',
            data: {skill:e.target.value},
        }) .done(function( data ) {

            var a = document.getElementById("skills");
            id = Object.keys(data);
            data = Object.values(data);
           // $("#skills").html("");
            for(var i=0;i<id.length;i++)
            {
                var option = document.createElement("option");
                var newOption = new Option(data[i], id[i], false, false);
                $(a).append(newOption);
            }

            //$("#skills").trigger('change');
        });

});*/




CKEDITOR.replace('description', {
    height: 250,
    toolbar:
        [
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
            {name: 'basicstyles', items: ['Bold', 'Italic']},
            {name: 'tools', items: ['Maximize', '-', 'About']}
        ]
});

CKEDITOR.replace('role', {
    height: 120,
    toolbar:
        [
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
            {name: 'basicstyles', items: ['Bold', 'Italic']},
            {name: 'tools', items: ['Maximize', '-', 'About']}
        ]
});

CKEDITOR.replace('requirements', {
    height: 250,
    toolbar:
        [
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
            {name: 'basicstyles', items: ['Bold', 'Italic']},
            {name: 'tools', items: ['Maximize', '-', 'About']}
        ]
});


var id_countries_selected = [];
var id_counties_selected = [];
var id_departaments_selected = [];
var id_sub_departaments_selected = [];
var id_cur = 0, id_pay = 0;


var currentTab = 0;
showTab(currentTab);

$('select').on('select2:select', function (e) {
    var data = e.params.data;
});

function showTab(n) {
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    var next_btn=$('#nextBtn').text();
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = next_btn;
    }
    fixStepIndicator(n)
}

$('.select2').on('select2:select', function (e) {
    var data = e.params.data;
});;
function setJobImages(id_job)
{
    var data, xhr;
    data = new FormData();
    data.append( 'bounty_file', $( '#job_upload_input_bounty_file' )[0].files[0] );
        data.append( 'job_public_image', $( '#job_upload_input_job_public_image' )[0].files[0] );
    data.append( 'id', id_job );
    xhr = new XMLHttpRequest();
    xhr.open( 'POST', '/ajax/index/upload-image', true );
    xhr.onreadystatechange = function ( response ) {
        if (xhr.readyState == 4 && xhr.status == 200) {



            window.location.replace("/view-jobs");
        }
    };
    xhr.send( data );
}
function nextPrev(n) {
    var x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;
    if(currentTab < x.length-1 || n == -1)
        x[currentTab].style.display = "none";
    currentTab = currentTab + n;
    if (currentTab >= x.length) {
        CKEDITOR.instances.description.updateElement();
        CKEDITOR.instances.requirements.updateElement();
        CKEDITOR.instances.role.updateElement();
        var asd = $("#regForm").serialize() + "&id=" + document.location.href.split("=")[1]+"&is_promoted="+$("#switch").css("background-color").includes("33")
        ;
        $.ajax({
            url: '/job',
            data: asd,
            type: 'POST',
            success: function(data) {
                if(data.error)
                {
                    alert(data.message);
                    currentTab--;
                } else
                {
                    if(typeof data == "string")
                        data = JSON.parse(data)
                    setJobImages(data.id);
                    currentTab--;
                }

            }
        });
    }
    else
        showTab(currentTab);
}

function stepsclick(n) {
    var a = "step"+n;
    if ($("#step"+n).hasClass("finish"))
    {
        var x = document.getElementsByClassName("tab");
        x[currentTab].style.display = "none";
        currentTab = n;
        showTab(n);
    }
    while(currentTab < n)
    {
        if(!validateForm()) return false;
        var x = document.getElementsByClassName("tab");

        x[currentTab].style.display = "none";
        currentTab++;
        showTab(currentTab);
    }

    return 0;

}

function validate_input(a)
{
    if(currentTab == 1)
    {
        if( CKEDITOR.instances['description'].getData().replace(/<[^>]*>/gi, '').length < 10 ) {
            $("#_description").addClass("invalid");
            $("#_description").tooltip({title:"Minimum 10 characters"})
            $("#_description").tooltip('show');
            return 1;
        }
        if( CKEDITOR.instances['role'].getData().replace(/<[^>]*>/gi, '').length < 10) {
            $("#_role").addClass("invalid");
            $("#_role").tooltip({title:"Minimum 10 characters"})
            $("#_role").tooltip('show');
            return 1;
        }
        return 0;
    }

    if(currentTab == 2) {
        if( CKEDITOR.instances['requirements'].getData().replace(/<[^>]*>/gi, '').length < 10 ) {
            $("#_requirements").addClass("invalid");
            $("#_requirements").tooltip({title:"Minimum 10 characters"})
            $("#_requirements").tooltip('show');
            return 1;
        }
        return 0;
    }

    var element = a.childNodes[1];
    if($(element).hasClass("required_select"))
        if(element.childNodes[0].childNodes.length <= 2) {
            $(element).tooltip({title:"Select at least one option"})
            $(element).tooltip('show');
            element.childNodes[0].className += " invalid";
            return 1;
        }
    if(element)
        if (element.tagName == "INPUT")
            if(element.type == "text") {
                if(element.name == "title" ) {
                    if(element.value.length <= 2) {
                        $('#title').tooltip({title:"Minimum 3 characters"})
                        $('#title').tooltip('show');
                        return 1;
                    }
                    return 0;
                }
            }
    return 0;
}

function validateForm() {
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("p");
    for (i = 0; i < y.length; i++) {
        if ($(y[i]).hasClass("required") === true)
            if(validate_input(y[i])) {
                y[i].childNodes[1].className += " invalid";
                valid = false;
            }
    }
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}

function fixStepIndicator(n) {
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
}

function    fetch_types(data)
{
    var a = document.getElementById("types");
     var   keys = Object.keys(data);
    data = Object.values(data);
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = keys[i];
        option.innerHTML = data[i];
        if(option.value != $("#types").val())
            $(a).append(option);
    }
}

// function fetch_bounty_dd()
// {
//     $.get( "/get-bounty-dd", function( data ) {
//     var a = document.getElementById("job_ad_lifetime");
//     data = Object.values(data)
//     for(var i=0;i<data.length;i++)
//     {
//       var option = document.createElement("option");
//       option.value = i+1;
//       option.innerHTML = data[i];
//       $(a).append(option);
//     }
// });
// }
//
function  fetch_careers(data)
{
    var a = document.getElementById("carrers");
    var     keys = Object.keys(data)
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = keys[i];
        option.innerHTML = data[i];
                if(option.value != $("#carrers").val())
        $(a).append(option);
    }
}

function    fetch_periode(data)
{
    var a = document.getElementById("periode");
    var keys = Object.keys(data);

    data = Object.values(data);
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = keys[i];
        option.innerHTML = data[i];
        if(option.value != $("#periode").val())
            $(a).append(option);
    }
}

function    fetch_recruiters(data)
{
    var a = document.getElementById("recruiters");
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = data[i].id;
        option.innerHTML = data[i].recruiter;
        if(!data[i].recruiter)
            continue;
        if(option.value != $("#recruiters").val())
         $(a).append(option);
    }
}

function    fetch_managers(data)
{
    var a = document.getElementById("managers");
     var keys = Object.values(data)
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = keys[i];
        option.innerHTML = data[i];
        $(a).append(option);
    }
}

function    ls(data)
{
    var a = document.getElementById("skills");
    var length = data.length;
    id = Object.keys(data);
    text = Object.values(data)
    for(var i=0;i<length;i++)
    {
        var newOption = new Option(text[i], id[i], true, true);
        $(a).append(newOption).trigger("change");
    }
}


function    fetch_countries(data)
{
    var a = document.getElementById("countries");
    id = Object.keys(data);
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = id[i];
        option.innerHTML = data[i];

        $(a).append(option);
        $("#countries").select2({
            placeholder: "Select countries",
            minimumResultsForSearch: Infinity}
        );
    }
}

// function    fetch_departaments(data)
// {
//     var a = document.getElementById("departaments");
//     data = Object.values(data)
//     for(var i=0;i<data.length;i++)
//     {
//       var option = document.createElement("option");
//       option.value = i+1;
//       option.innerHTML = data[i];
//       $(a).append(option);
//     $("#departaments").select2({
//     placeholder: "Select departaments",
//     minimumResultsForSearch: Infinity}
//     );
//     }
//
// }

function    fetch_departaments(data)
{
    var a = document.getElementById("departaments");
    // data = Object.values(data)

    id = Object.keys(data);
    data = Object.values(data)


    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");

        option.value = id[i];
        option.innerHTML = data[i];
        $(a).append(option);
        $("#departaments").select2({
            placeholder: "Select departaments",
            minimumResultsForSearch: Infinity}
        );
    }
}

function    fetch_projects(data)
{
    var a = document.getElementById("project");
    id = Object.keys(data);
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");

        option.value = id[i];
        option.innerHTML = data[i];
        $(a).append(option);
    }
}

function    fetch_currency(data)
{
    var a = document.getElementById("currency");
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = i+1;
        option.innerHTML = data[i];
        $(a).append(option);
    }
    if(id_cur)
        $('#currency').val( id_cur);

}


function    fetch_payment(data)
{
    var a = document.getElementById("bounty_payment_type");
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = i+1;
        option.innerHTML = data[i];
        $(a).append(option);
    }
    if(id_pay)
        $("#bounty_payment_type").val(id_pay);
}

function fetch_bounty_dd(data)
{
    var a = document.getElementById("job_ad_lifetime");
    data = Object.values(data)
    for(var i=0;i<data.length;i++)
    {
        var option = document.createElement("option");
        option.value = i+1;
        option.innerHTML = data[i];
        $(a).append(option);
    }
    $("#job_ad_lifetime").val(job_ad_lifetime_id);

}




function  fetch_data()
{
    var data;
    $.ajax({
        type: 'GET',
        contentType: "application/x-www-form-urlencoded",
        url: '/get-contents',
    }) .done(function( json ) {

        data = json;
        fetch_types(data.types);
        fetch_careers(data.careers);
        fetch_periode(data.periode);
        fetch_recruiters(data.recruiters);
        fetch_managers(data.hiring_managers);
        fetch_countries(data.countries);
        fetch_departaments(data.departments);
        fetch_projects(data.projects);
        fetch_currency(data.currenices);
        fetch_payment(data.payment_types);
        fetch_bounty_dd(data.bounty_dd);

    });




    $("#cities").select2({
        placeholder: "Select cities",
        minimumResultsForSearch: Infinity}
    );

    $("#counties").select2({
        placeholder: "Select counties",
        minimumResultsForSearch: Infinity}
    );


    $("#unit_region").select2({
        placeholder: "Select sub-department",
        minimumResultsForSearch: Infinity}
    );

    $("#team").select2({
        placeholder: "Select team",
        minimumResultsForSearch: Infinity}
    );

    $("#project").select2({
        placeholder: "Select project",
        minimumResultsForSearch: Infinity}
    );

        document.getElementsByClassName("_asdagarg")[0].style.display = "none";






}



function    fetch_counties(counties, cities, x)
{
    if(id_countries_selected)
        $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded",
            url: '/get-counties-by-list',
            data: {countries:id_countries_selected},
        })   .done(function( data ) {

            var a = document.getElementById("counties");
            // data = Object.values(data)
            id = Object.keys(data);
            data = Object.values(data)
            for(var i=0;i<data.length;i++)
            {
                var option = document.createElement("option");
                option.value = id[i];
                option.innerHTML = data[i];
                $(a).append(option);
            }
            $('#counties').trigger('change');



            if(counties) {
                $('#counties').val(counties);
                $('#counties').trigger('change');
                id_counties_selected = counties;
                if(cities)
                    fetch_cities(cities);
            }
            if(x)
            {
                var sub_id = Object.keys(populate_cities);
                var sub_text = Object.values(populate_cities);

                for(var z=0;z<sub_id.length;z++)
                {
                    var newOption = new Option(sub_text[z], sub_id[z], false, false);
                    $('#cities').append(newOption).trigger('change');
                }
                $("#cities").val(selected_cities).trigger("change");
            }
        });

}

function    fetch_subdepartaments(sdepartaments, ssdepartaments)
{
    if(id_departaments_selected)
        $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded",
            url: '/get-sub-department-by-list',
            data: {departments:id_departaments_selected},
        })   .done(function( data ) {
            var a = document.getElementById("unit_region");
            $('#unit_region').val("");
            $('#unit_region').trigger('change');
                        id = Object.keys(data);

            data = Object.values(data)

            for(var i=0;i<data.length;i++)
            {
                var option = document.createElement("option");
                option.value = id[i];
                option.innerHTML = data[i];
                $(a).append(option);
            }
            if(sdepartaments) {
                $('#unit_region').val(sdepartaments);
                $('#unit_region').trigger('change');
                id_sub_departaments_selected = sdepartaments;
                if(ssdepartaments)
                    fetch_subsubdepartaments(ssdepartaments);
            }
        });
}


function    fetch_subsubdepartaments(ssdepartaments)
{
    if(id_sub_departaments_selected)
        $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded",
            url: '/get-sub-sub-department-by-list',
            data: {sub_departments:id_sub_departaments_selected},
        })   .done(function( data ) {
            var a = document.getElementById("team");
            id = Object.keys(data);
            data = Object.values(data)
            for(var i=0;i<data.length;i++)
            {
                var option = document.createElement("option");

                option.value = id[i];
                option.innerHTML = data[i];
                $(a).append(option);
            }
            if(ssdepartaments)
            {
                $('#team').val(ssdepartaments);
                $('#team').trigger('change');
            }
        });

}




function    fetch_cities(cities)
{
    if(id_counties_selected)
        $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded",
            url: '/get-citie-by-list',
            data: {counties:id_counties_selected},
        }).done(function( data ) {
            var a = document.getElementById("cities");
            // data = Object.values(data)
            id = Object.keys(data);
            data = Object.values(data)

            for(var i=0;i<data.length;i++)
            {
                var option = document.createElement("option");

                option.value = id[i];
                option.innerHTML = data[i];
                $(a).append(option);
            }



            if(cities)
            {

                $('#cities').val(cities);
                $('#cities').trigger('change');
            }
        });
}

$(function(){
    $('#countries').select2()
        .on("change", function(e) {
            id_countries_selected = e.val;
            $("#counties").html("");
            $("#counties").val("").trigger("change");
            $("#cities").html("");
            $("#cities").val("").trigger("change");
            fetch_counties();
        })
    $('#counties').select2()
        .on("change", function(e) {
            id_counties_selected = e.val;
            $("#cities").html("");
            $("#cities").val("").trigger("change");
            fetch_cities();
        })
    $('#departaments').select2()
        .on("change", function(e) {
            $("#unit_region").html("");
            $("#unit_region").val("").trigger("change");
            $("#team").html("");
            $("#team").val("").trigger("change");
            id_departaments_selected = e.val;
            fetch_subdepartaments();
        })
    $('#unit_region').select2()
        .on("change", function(e) {
            $("#team").html("");
            $("#team").val("").trigger("change");
            id_sub_departaments_selected = e.val;
            fetch_subsubdepartaments();
        })
});


$('#countries').on('select2:select', function (e) {
    var data = e.params.data;
});


function  load_data()
{
    $.ajax({
        type: 'GET',
        contentType: "application/x-www-form-urlencoded",
        url: '/get-job-by-id',
        data: {id: document.location.href.split("=")[1] },
    })   .done(function( data ) {
        CKEDITOR.on('instanceReady', function(evt) {
            CKEDITOR.instances.requirements.setData(data[0].requirements);
            CKEDITOR.instances.description.setData(data[0].description);
            CKEDITOR.instances.role.setData(data[0].role);
        });
        if(data[0].title)
            document.getElementById("title").value = data[0].title;
        if(data[0].countries)
        {
            for(var i=0;i<data[0].countries.length;i++)
            {
                var newOption = new Option(data[0].countries[i].title, data[0].countries[i].id, true, true);
                $('#countries').append(newOption).trigger('change');
                if(!id_countries_selected)
                    id_countries_selected = [data[0].countries[i].id.toString()];
                else
                    id_countries_selected.push(data[0].countries[i].id.toString());
            }
            fetch_counties('', '', 1);

            /*  id_countries_selected = data[0].countries;
              fetch_counties(data[0].counties, data[0].cities);
              $('#countries').val(data[0].countries);*/
            $('#countries').trigger('change');
        }

        if(data[0].counties)
        {
            for(var i=0;i<data[0].counties.length;i++)
            {
                var newOption = new Option(data[0].counties[i].title, data[0].counties[i].id, true, true);
                $('#counties').append(newOption).trigger('change');
                $('#counties').trigger('change');
                if(!id_counties_selected)
                    id_counties_selected = [data[0].counties[i].id.toString()]
                else
                    id_counties_selected(data[0].counties[i].id.toString())

            }
            /*  id_countries_selected = data[0].countries;
              fetch_counties(data[0].counties, data[0].cities);
              $('#countries').val(data[0].countries);*/

        }



        if(data[0].recruiters)
        if(data[0].recruiters.length > 0)
        if(data[0].recruiters[0])
        if( ("title" in data[0].recruiters[0]) && ("id" in data[0].recruiters[0]) )
        if((data[0].recruiters[0].title) && (data[0].recruiters[0].id)){
            var newOption = new Option(data[0].recruiters[0].title, data[0].recruiters[0].id, true, true);
            $('#recruiters').append(newOption).trigger('change');
        }

        if(data[0].sub_departments)
        {
            for(var i=0;i<data[0].sub_departments.length;i++)
            {
                var newOption = new Option(data[0].sub_departments[i].title, data[0].sub_departments[i].id, true, true);
                $('#unit_region').append(newOption).trigger('change');
                if(!id_sub_departaments_selected)
                    id_sub_departaments_selected = [data[0].sub_departments[i].id.toString()];
                else
                    id_sub_departaments_selected.push(data[0].sub_departments[i].id.toString());
            }
            fetch_subdepartaments();


            /* $('#departaments').val(data[0].departments);
             $('#departaments').trigger('change');
             id_departaments_selected = data[0].departments;*/
            $('#departments').trigger('change');
            //fetch_subdepartaments(data[0].sub_departments, data[0].sub_sub_departments);
        }

        if(data[0].sub_sub_departments)
        {
            for(var i=0;i<data[0].sub_departments.length;i++)
            {
                var newOption = new Option(data[0].sub_departments[i].title, data[0].sub_departments[i].id, true, true);
                $('#team').append(newOption).trigger('change');
            }


            /* $('#departaments').val(data[0].departments);
             $('#departaments').trigger('change');
             id_departaments_selected = data[0].departments;
//          $('#departments').trigger('change');
             fetch_subdepartaments(data[0].sub_departments, data[0].sub_sub_departments);*/
        }

        if(data[0].departments)
        {
            id_departaments_selected = deps_selected;
            for(var i=0;i<data[0].departments.length;i++)
            {
                var newOption = new Option(data[0].departments[i].title, data[0].departments[i].id, true, true);
                $('#departaments').append(newOption).trigger('change');
            }
            var sub_id = Object.keys(populate_sub_department);
            var sub_text = Object.values(populate_sub_department);

            for(var z=0;z<sub_id.length;z++)
            {
                var newOption = new Option(sub_text[z], sub_id[z], false, false);
                $('#unit_region').append(newOption).trigger('change');
            }
            $("#unit_region").val(sub_deps_selected).trigger("change");

            var sub_sub_id = Object.keys(populate_sub_sub_department);
            var sub_sub_text = Object.values(populate_sub_sub_department);

            for(var z=0;z<sub_sub_id.length;z++)
            {
                var newOption = new Option(sub_sub_text[z], sub_sub_id[z], false, false);

                $('#team').append(newOption).trigger('change');
            }
            $("#team").val(sub_sub_deps_selected).trigger("change");

            //fetch_subdepartaments();


            /* $('#departaments').val(data[0].departments);
             $('#departaments').trigger('change');
             id_departaments_selected = data[0].departments;
//          $('#departments').trigger('change');
             fetch_subdepartaments(data[0].sub_departments, data[0].sub_sub_departments);*/
        }
if("is_promoted" in data[0])
    if(data[0].is_promoted)
        $(".switch")[0].click()

        if(data[0].bounty_file)
        {
            $('#bounty_image_div').css('background-image', 'url(' + data[0].bounty_file + ')');
        }

        if(data[0].job_public_image)
        {
            $('#job_image_div').css('background-image', 'url(' + data[0].job_public_image + ')');
        }

        if(data[0].projects)
        {
            for(var i=0;i<data[0].projects.length;i++)
            {
                var newOption = new Option(data[0].projects[i].title, data[0].projects[i].id, true, true);
                $('#project').append(newOption).trigger('change');

            }
        }

        if(data[0].career_level)
        {
            var option =  new Option(data[0].career_level.title, data[0].career_level.id, 1, 1);
            $('#carrers').append(option);
        }

        if(data[0].skills)
        {
            var _results = [];
            for(var i=0;i<data[0].skills.length;i++)
            {
                for(var i=0;i<data[0].skills.length;i++)
            {
                var employee = {
                  "id": data[0].skills[i].id,
                  "text": data[0].skills[i].title
                }
                _results.push(employee);   
            }
            }
            /* $('#skills').val(data[0].skills);
             $('#skills').trigger('change');*/
             $('#skills').data().select2.updateSelection(_results);

        }
     //   document.getElementsByClassName("_asdagarg")[0].style.display = "none";

        $("#job_ad_lifetime").val(job_ad_lifetime_id);

        if(data[0].type) {
            var option =  new Option(data[0].type.title, data[0].type.id, 1, 1);
            $('#types').append(option);

        }


        if(data[0].job_periode) {
            var option =  new Option(data[0].job_periode.title, data[0].job_periode.id, 1, 1);
            $('#periode').append(option);
        }
        if(data[0].start)
            document.getElementById("start").valueAsDate = new Date(data[0].start.substring(6, 10), parseInt(data[0].start.substring(3, 5))-1, parseInt(data[0].start.substring(0, 2))+1);
        if(data[0].end)
            document.getElementById("end").valueAsDate = new Date(data[0].end.substring(6, 10), parseInt(data[0].end.substring(3, 5))-1, parseInt(data[0].end.substring(0, 2))+1);
        if(data[0].job_headcount)
            document.getElementById("job_headcount").value = data[0].job_headcount;

        if(data[0].bounty_title)
            document.getElementById("bounty_title").value = data[0].bounty_title;

        if(data[0].bounty_requirements)
            document.getElementById("bounty_requirements").value = data[0].bounty_requirements;
        if(data[0].bounty_payment_type)
            id_pay = data[0].bounty_payment_type.id;
        if(data[0].bounty)
            document.getElementById("bounty").value = data[0].bounty;
        if(data[0].id_currency)
        if(data[0].id_currency.id)
            id_cur = data[0].id_currency.id
        if(CKEDITOR.instances.requirements.getData() != data[0].requirements)
        {
            CKEDITOR.instances.requirements.setData(data[0].requirements);
            CKEDITOR.instances.description.setData(data[0].description);
            CKEDITOR.instances.role.setData(data[0].role);
        }
        CKEDITOR.instances.requirements.setData(data[0].requirements);
        CKEDITOR.instances.description.setData(data[0].description);
        CKEDITOR.instances.role.setData(data[0].role);

    });

}


$( document ).ready(function() {
    $("#job_ad_lifetime").val(job_ad_lifetime_id);

    $("#skills").select2({
    data: ['zzzzz'],
      tags: "true",
      allowClear: true,
      placeholder: "Select a skill",
      multiple: true,
      ajax: {
        type: 'POST',
        dataType: 'json',
        url: "/get-skill",
       delay: 250,
        data: function (params) {
          return {
            skill: params, // search term
          };
        },
        results: function (data, page) {
          // parse the results into the format expected by Select2.
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data
            id = Object.keys(data);
            data = Object.values(data);
            var _results = [];
            for(var i=0;i<id.length;i++)
            {
                var employee = {
                  "id": id[i],
                  "text": data[i]
                }
                _results.push(employee);   
            }
          return {
            results: _results
          };
        },
        cache: true
      },
     escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });


function formatRepo (repo) {
    if (repo.loading) return repo.text;

    var markup = 
    '<div class="avatar">' +
    '<img src="' + repo.owner.avatar_url + '" style="max-width: 100%" />' +
    '</div>' +
    '<div class="info">' +
   
    '<div class="col-sm-6">' + repo.full_name + '</div>' +
    '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + repo.forks_count + '</div>' +
    '<div class="col-sm-2"><i class="fa fa-star"></i> ' + repo.stargazers_count 
    ;

    if (repo.description) {
      markup += '<div>' + repo.description + '</div>';
    }

    markup += '</div></div>';

    return markup;
  }

  var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];



  function formatRepoSelection (repo) {
    return repo.full_name || repo.text;
  }


    if(document.location.href.split("=")[1]) {
        load_data();
    }

    fetch_data();



    function openFileInput(id)
    {
        $("input#"+id).click();
    }


    // $('#job_image_div').click(function() {
    //     $("input#job_upload_input_job_public_image").click();
    // })
    //
    // $('#bounty_image_div').click(function() {
    //     $("#job_upload_input_bounty_file").click();
    // })

    document.getElementById("job_upload_input_job_public_image").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            $('#job_image_div').css('background-image', 'url(' + e.target.result + ')');
            if ($('#job_upload_input_job_public_image')[0].files[0])
                if ($('#job_upload_input_job_public_image')[0].files[0].size > 120000) {
                    alert("Job image is bigger than 120, image will not be saved")
                    return 0;
                }
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };

    document.getElementById("job_upload_input_bounty_file").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            $('#bounty_image_div').css('background-image', 'url(' + e.target.result + ')');
            if ($('#job_upload_input_bounty_file')[0].files[0])
                if ($('#job_upload_input_bounty_file')[0].files[0].size > 120000) {
                    alert("Bounty image is bigger than 120, image will not be saved")
                    return 0;
                }
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
});




