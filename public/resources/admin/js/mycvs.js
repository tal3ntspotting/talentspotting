function getApplicationJobs($this, id) {
    $($this).next('.jobapptitle-' + id).toggleClass('hidden');
    $.ajax({
        TYPE: 'GET',
        data: {id: id},
        url: "/appjobs",
        success: function (data) {
            if ($($this).next('.jobapptitle-' + id).is(':empty')) {
                $($this).next('.jobapptitle-' + id).append(data);
            }
        }

    })
}

function HideGetApp($this, id) {
    $($this).next('.jobapptitle-' + id).toggleClass('hidden');
}


function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}

$(document).ready(function () {
    if (location.hash !== '')
        $('a[href="' + location.hash + '"]').tab('show');

    var $checkboxes = $('td input[type="checkbox"]');
    $checkboxes.change(function () {
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        // $('#count-checked-checkboxes').text(countCheckedCheckboxes);
        if (!countCheckedCheckboxes || countCheckedCheckboxes == 0) {
            $('#edit-count-checked-checkboxes').html("");
            $('#edit-count-checked-checkboxes-jobs').html("");
//            console.log('remove');
        } else if (countCheckedCheckboxes == 1) {
            $('#edit-count-checked-checkboxes').html('<strong style="font-size:15px">' + countCheckedCheckboxes + '</strong> candidate selected');
            $('#edit-count-checked-checkboxes-jobs').html('<strong style="font-size:15px">' + countCheckedCheckboxes + '</strong> job selected');
        } else {
            $('#edit-count-checked-checkboxes').html('<strong style="font-size:15px">' + countCheckedCheckboxes + '</strong> candidates selected');
            $('#edit-count-checked-checkboxes-jobs').html('<strong style="font-size:15px">' + countCheckedCheckboxes + '</strong> jobs selected');
        }
    });
//    $("#dynamic-table_length").append("<span  class='abc'></span>");
//    $(".dynamic-table , #dynamic-table_length").append("<span  class='abc'></span>");
    var doClick = function () {
        'use strict';
        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('mousedown', true, true, window);
        return event;
    }
    $('span.abc').on('click', function () {
        'use strict';
        $("#myDropDown").focus().get(0).dispatchEvent(doClick());
    });
    $('[id^=stl]').css({'fontSize': '15px'});


//    $(window).load(function () {
//        $(".demo").customScrollbar();
//        $("#fixed-thumb-size-demo").customScrollbar({fixedThumbHeight: 50, fixedThumbWidth: 60});
//    });

    var checked = []
    var $checkboxes = $('td input[type="checkbox"]');
    $checkboxes.change(function () {
        checked = $.map($('input[type="checkbox"]:checked'), function (a) {
            return a.value;
        })

//        var value = $(this).val();
//        $('.query-' + value).toggleClass('edit-icon').animate();
//        console.log(value);
    });
//    $('#dynamic-table').on('mo')
    $("#dynamic-table > tbody > tr").hover(
            function () {
                $(this).find("#circle-green").addClass("edit-icon");
            }, function () {
        $(this).find("#circle-green").removeClass("edit-icon");
    }
    );

    $("#check").on('click', function (e) {
        if (checked === undefined || checked.length == 0) {

            alert('please choose candidate');
//            $('#myModal').modal('hide');
            e.stopPropagation();
            return false;
        } else {
            $('#myModal').modal('show');
            localStorage.setItem("arrayData", checked);
            console.log(checked);
        }
//        $('#myModal').modal('show');
        localStorage.setItem("arrayData", checked);
        console.log(checked);
    })
    $('#filter , #filter-clicked').on('click', function () {
        $("#filter-area").slideToggle();
        $("#filter-clicked").toggleClass('filter-clicked');
    });
    $('.search-brain').on('click', function () {
        $("#search-cont").slideToggle();
        $("#filter-clicked-2").toggleClass('filter-clicked-2');
        $("#filter-clicked-3").toggleClass('filter-clicked-3');
        $("#filter-clicked-4").toggleClass('filter-clicked-4');
        $("#filter-clicked-5").toggleClass('filter-clicked-5');
        $("#filter-clicked-6").toggleClass('filter-clicked-6');
        $("#filter-clicked-7").toggleClass('filter-clicked-7');

    });


    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    var $thisPage = window.location.href.split("/")
    if (inArray("view-jobs", $thisPage)) {
        if (isMobile.any()) {
           $("span._uurtr33445").removeClass('_uurtr33445');
           $("span._iiuiu").removeClass('_iiuiu');
        }
    }

});

