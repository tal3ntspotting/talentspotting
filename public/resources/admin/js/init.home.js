$(document).ready(function () {

    if ($(location).attr('pathname') === "/home" || $(location).attr('pathname') === "/")
    {

        if (localStorage.getItem("data") !== null || localStorage.getItem("_data") !== null) {
            $(".filter-link").addClass("active");
            $(".search-filters-input").css({"display": "block"});
        }
        if (typeof (Storage) !== "undefined") {
            data = JSON.parse(localStorage.getItem("data"));
            console.log(data);
        }
        $("#cities")
                .select2({
                    initSelection: function (element, callback) {

                        $(element.val()).each(function () {
                            data.push({id: this, text: this});
                        });
                        if (data == null) {
                            callback([]);
                        } else {
                            callback(data);
                        }

                    }

                })
                .change(function () {
                    var data = [];
                    var selected = $("#cities").select2("data");
                    var id = $(selected).attr('id');
                    var text = $(selected).attr('text');
                    data.push({id: id, text: text});
                    localStorage.setItem("data", JSON.stringify(data));
                    $(this).parents('form').submit();
                    console.log(id);
                    console.log(text);
                })
        $("#cities").next().find('span.select2-selection__choice__remove').on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var text = $(this).parent().attr("title");
            localStorage.removeItem("data");
//            localStorage.clear();
            $(this).parents('form').submit();
            e.preventDefault();
            e.stopPropagation();
        });







        if (typeof (Storage) !== "undefined") {
            _data = JSON.parse(localStorage.getItem("_data"));
            console.log("skills" + _data);
        }



        $("#skills").select2({
            initSelection: function (element, callback) {

                $(element.val()).each(function () {
                    _data.push({id: this, text: this});
                });
                if (_data == null) {
                    callback([]);
                } else {
                    callback(_data);
                }

            }
        }).change(function () {
            var _data = [];
            var selected = $("#skills").select2("data");
            var id = $(selected).attr('id');
            var text = $(selected).attr('text');
            _data.push({id: id, text: text});
            localStorage.setItem("_data", JSON.stringify(_data));
            $(this).parents('form').submit();
            console.log(id);
            console.log(text);
        });

        $("#skills").next().find('span.select2-selection__choice__remove').on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var text = $(this).parent().attr("title");
            localStorage.removeItem("_data");
            $(this).parents('form').submit();
            e.preventDefault();
            e.stopPropagation();
        });


    }


})