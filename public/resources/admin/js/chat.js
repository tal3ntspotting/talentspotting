function getChatTemplate(id_user) {
    $.ajax({
        url: '/users/index/get-chat-template',
        type: 'POST',
        data: {id_user: id_user},
        success: function (data) {
            $('.chat-render-template').html(data).fadeIn('fast');
            localStorage.setItem('user', JSON.stringify({id_user: id_user}));
        }

    })
}

var full = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
$(document).ready(function () {
    // $(".chat").niceScroll();
    $(document).on('click','div.user',function(){
        var id_user = $(this).data('id-user');
        getChatTemplate(id_user);
    });

    $(document).on('click','span#send-message',function(){
        if($(this).parent().find('input#message-value').val() == "") return false;
        var data = {};
         data['message'] = $(this).parent().find('input#message-value').val();
         data['from'] = minimize_me;
         data['to'] = $(this).data('to');
         data['host'] = minimize_me.sitename;
         data['container'] = $(this).data('container');
        socket.emit('sendMessage',data);

        //save message to server
        $.ajax({
            url:'/users/index/save-chat',
            type : 'POST',
            data : {data:data},
        });
        $(this).parent().find('input#message-value').val("");

        // render message
        $('div.chat-body div:last').before(renderAnswer(data));

        // scroll bottom
        var height = 0;
        $('.chat-body div').each(function(i, value){
            height += parseInt($(this).height());
        });
        height += '';
        $('.chat').animate({scrollTop: height});


        console.log(user);
        console.log(minimize_me);
    });

    socket.on('newMessage',function (data) {
        var full = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
        if(full != data.message.host) return false;
        console.log(minimize_me.id);
        console.log(data.message.to.id_user);
        if(minimize_me.id != data.message.to.id_user) return false

        console.log('new message ',data);

        $('div.chat-body div:last').before(renderResponse(data.message));
        // scroll bottom
        var height = 0;
        $('.chat-body div').each(function(i, value){
            height += parseInt($(this).height());
        });
        height += '';
        $('.chat').animate({scrollTop: height});

    })



})

function renderAnswer(data)
{
    var myDate = new Date();
    var now = (myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  myDate.getDate() + ' '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2)+ ':'+myDate.getSeconds());
    console.log(data);
    var img = full+'/resources/files/user/'+data.from.image;
    if(data.from.image == null || data.from.image == ""){
        img = full+'/resources/admin/images/no-picture.jpg';
    }
    var template = '<div class="answer left">\n' +
        '        <div class="avatar">\n' +
        '            <img src="'+img+'" alt="User name">\n' +
        '            <div class="status online"></div>\n' +
        '        </div>\n' +
        '        <div class="name">'+capitalizeFirstLetter(data.from.firstname)+' '+capitalizeFirstLetter(data.from.lastname)+'</div>\n' +
        '        <div class="text">\n' +
        '            '+data.message+'\n' +
        '        </div>\n' +
        '        <div class="time">'+now+'</div>\n' +
        '    </div>';
    return template;
}

function renderResponse(data){
    var myDate = new Date();
    var now = (myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  myDate.getDate() + ' '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2)+ ':'+myDate.getSeconds());
    // console.log((data.from)['image']);return false;
    var user_image = data.from['image'];
    var img = full+'/resources/files/user/'+user_image;
    if(user_image == null || user_image == ""){
        img = full+'/resources/admin/images/no-picture.jpg';
    }
    var template = '<div class="answer right">\n' +
        '        <div class="avatar">\n' +
        '            <img src="'+img+'" alt="User name">\n' +
        '            <div class="status on"></div>\n' +
        '        </div>\n' +
        '        <div class="name">'+capitalizeFirstLetter(data.from.firstname)+' '+capitalizeFirstLetter(data.from.lastname)+'</div>\n' +
        '        <div class="text">\n' +
        '            '+data.message+'\n' +
        '        </div>\n' +
        '        <div class="time">'+now+'</div>\n' +
        '    </div>';
    return template;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

