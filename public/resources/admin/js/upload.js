var files;

$('#exampleInputFile').on('change', uploadFiles);

function uploadFiles(event)
{
    files = event.target.files;
    event.stopPropagation();
    event.preventDefault();
    
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });

    $.ajax({
        url: '/upload-profile-photo',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR)
        {
            if(typeof data.error === 'undefined')
            {
                $("#profile_image").attr('src', "/resources/files/user/"+data.success);
                $("#profile_image").removeClass("hidden");
                //submitForm(event, data);
                //console.log(data.success);
            }
            else
            {
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            console.log('ERRORS: ' + textStatus);
        }
    });
}

$('#logoCompany').on('change', uploadFilesCompany);

function uploadFilesCompany(event)
{
    files = event.target.files;
    event.stopPropagation();
    event.preventDefault();
    
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });
    
    if ($("#id").val() > 0)
    {
        $.ajax({
        url: '/upload-company-photo?id='+$("#id").val(),
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    $("#company_logo").attr('src', "/resources/files/companies/large/"+data.success);
                    $("#company_logo").removeClass("hidden");
                    //submitForm(event, data);
                    //console.log(data.success);
                }
                else
                {
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log('ERRORS: ' + textStatus);
            }
        });    
    }
    
}
