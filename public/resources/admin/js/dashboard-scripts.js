

function create_chart111(em) {

    var _dates = [];

    // console.log(em);
    var starting = em.chart[0].numbers * 0.7;

    total_employees_shares = [];
    em_ending = em.chart[em.chart.length - 1].numbers;
    var ending = em_ending * 1.2;
    if (em_ending > 1000) {
        //            em_ending=Math.round(em_ending/1000,1)+"k";
        e_ending = em_ending / 1000;
        e_ending = (e_ending.toFixed(1)) + "k";
        // document.getElementById("total_shares").innerHTML = e_ending;
    }
    else {
        // document.getElementById("total_shares").innerHTML = em_ending;
    }

    for (var i = 0; i < em.chart.length; i++) {
        var date = em.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        //            console.log(dd+"/"+mm);

        _dates.push(dd + "/" + mm);
        total_employees_shares.push(em.chart[i].numbers);
    }

    Highcharts.chart('chart111', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            min: starting,
            max: ending,
            startOnTick: true,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.employee_shares,
            color: '#82be00',
            data: total_employees_shares
        }]
    });
}

function date_picker() {

    if (document.getElementById("date_picker1").value)
        if (document.getElementById("date_picker2").value) {
            $(".div_arrow").remove();
            var a = document.getElementById("date_picker1").value;
            a = a.substring(3, 5) + '/' + a.substring(0, 2) + '/' + a.substring(6, a.length);
            var b = document.getElementById("date_picker1").value;
            b = b.substring(3, 5) + '/' + b.substring(0, 2) + '/' + b.substring(6, b.length);
            fetch_ajax(a, b)
        }

}

var evanghelists_employees_no = 0;
var shares_of_jobs_no = 0;
var engaged_employees_no = 0;
var active_employees_no = 0;
var total_employees_no = 0;

function resetCharts() {
    dates = [];
    total_users_values = [];
    active_users_values = [];
    engaged_employed = [];
    employee_evanghelists = [];
    active_jobs = [];
    jobs_with_applications = [];
    views_of_jobs = [];
    potential_candidates = [];
    applications = [];
    $(".div_arrow_prior").remove();
    $(".div_arrow_ly").remove();
}

function create_arrow(value0, value1) {
    var div = document.createElement("div");
    var div1 = document.createElement("div");
    $(div1).addClass("div_arrow_prior");

    var title = document.createElement("div");
    $(title).addClass("triangle-title");
    title.innerText = "VS PRIOR";
    $(div1).append(title);

    var triangle = document.createElement("div");
    (value0 >= 0) ? $(triangle).addClass("triangle-up") : $(triangle).addClass("triangle-down");
    $(div1).append(triangle);
    $(triangle).addClass("agagagahoagarm");

    var value = document.createElement("div");
    $(value).addClass("triangle-value");
    value.innerText = Math.round(value0) + '%';
    $(div1).append(value);
//        div1.setAttribute("style", "margin-left: 10em;")
    if (value0 != "n/a")
        $(div).append(div1);

    var div2 = document.createElement("div");
    $(div2).addClass("div_arrow_ly");

    var title = document.createElement("div");
    $(title).addClass("triangle-title");
    title.innerText = "vs LY";
    $(div2).append(title);

    var triangle = document.createElement("div");
//        triangle.setAttribute("style", "margin-left:1.3em;margin-right:1.3em");
    (value1 >= 0) ? $(triangle).addClass("triangle-up") : $(triangle).addClass("triangle-down");
    $(div2).append(triangle);

    var value = document.createElement("div");
    $(value).addClass("triangle-value");
    value.innerText = Math.round(value1) + '%';
    $(div2).append(value);
    if (value1 != "n/a")
        $(div).append(div2);

    return div;
}

var dates = [];

function create_arrow(value0, value1) {
    var div = document.createElement("div");
    var div1 = document.createElement("div");
    $(div1).addClass("div_arrow_prior");

    var title = document.createElement("div");
    $(title).addClass("triangle-title");
    title.innerText = "VS PRIOR";
    $(div1).append(title);

    var triangle = document.createElement("div");
    (value0 >= 0) ? $(triangle).addClass("triangle-up") : $(triangle).addClass("triangle-down");
    $(div1).append(triangle);
    $(triangle).addClass("agagagahoagarm");

    var value = document.createElement("div");
    $(value).addClass("triangle-value");
    value.innerText = Math.round(value0) + '%';
    $(div1).append(value);
    //        div1.setAttribute("style", "margin-left: 10em;")
    if (value0 != "n/a")
        $(div).append(div1);

    var div2 = document.createElement("div");
    $(div2).addClass("div_arrow_ly");

    var title = document.createElement("div");
    $(title).addClass("triangle-title");
    title.innerText = "vs LY";
    $(div2).append(title);

    var triangle = document.createElement("div");
    //        triangle.setAttribute("style", "margin-left:1.3em;margin-right:1.3em");
    (value1 >= 0) ? $(triangle).addClass("triangle-up") : $(triangle).addClass("triangle-down");
    $(div2).append(triangle);

    var value = document.createElement("div");
    $(value).addClass("triangle-value");
    value.innerText = Math.round(value1) + '%';
    $(div2).append(value);
    if (value1 != "n/a")

        $(div).append(div2);

    return div;
}

var dates = [];
var total_users_values = [];

function create_chart1(em) {
    var _total_users_values = [];

    var _dates = [];
    var starting = em.chart[0].numbers * 0.7;

    em_ending = em.chart[em.chart.length - 1].numbers;
    var ending = em_ending * 1.2;
    if (em_ending > 1000) {
        //            em_ending=Math.round(em_ending/1000,1)+"k";
        e_ending = em_ending / 1000;
        e_ending = (e_ending.toFixed(1)) + "k";
        document.getElementById("total_users").innerHTML = e_ending + "<span class=\"incremental\">(+" + Math.round(em_ending - em.chart[0].numbers) + ")</span>";
    }
    else {
        document.getElementById("total_users").innerHTML = em_ending + "<span class=\"incremental\">(+" + Math.round(em_ending - em.chart[0].numbers) + ")</span>";
    }

    for (var i = 0; i < em.chart.length; i++) {
        var date = em.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);

        _dates.push(dd + "/" + mm);
        _total_users_values.push(em.chart[i].numbers);
    }

    Highcharts.chart('chart1', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            min: starting,
            max: ending,
            startOnTick: true,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.employee_created,
            color: '#828282',
            data: _total_users_values
        }]
    });
    total_users_values = _total_users_values;
}

var total_employees_shares = [];

var active_users_values = [];

function create_chart2(em) {
    console.log(total_users_values)
    var _total_users_values = total_users_values;
    var _active_users_values = [];
    // total_users_values = [];
    var starting = em.chart[0].numbers * 0.7;
    active_employees_no = users_current_ending = em.chart[em.chart.length - 1].numbers;
    var ending = users_current_ending * 1.2;
    if (users_current_ending > 1000) {
        //            users_created_ending=Math.round(users_created_ending/1000,1)+"k";
        users_current_ending = users_current_ending / 1000;
        users_current_ending = (users_current_ending.toFixed(1)) + "k";
    }
    document.getElementById("active_employees").innerHTML = users_current_ending + "<span class=\"incremental\">(+" + Math.round(em.chart[em.chart.length - 1].numbers - em.chart[0].numbers) + ")</span>";

    var _datess = [];

    for (var i = 0; i < em.chart.length; i++) {
        var date = em.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        //            console.log(dd+"/"+mm);

        _datess.push(dd + "/" + mm);
        //            total_users_values.push(em.chart[i].numbers);
        _active_users_values.push(em.chart[i].numbers);
    }


    //        console.log(starting);
    //        for (var i = 0; i < em.chart.length; i++)
    //        {
    //            active_users_values.push(em.chart[i].numbers);
    //        }

    Highcharts.chart('chart2', {
        exporting: {enabled: false},
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: _datess,
        },
        yAxis: {
            // min: starting,
            max: ending,
            startOnTick: true,
            title: {
                text: ''
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [
            {
                name: trans_key.employee_created,
                color: '#828282',
                data: _total_users_values
            },
            {
                name: trans_key.active_employees,
                color: '#828282',
                data: _active_users_values
            }
        ]
    });
}

var engaged_employed = [];

function create_chart3(em) {

    var _dates = [];
    total_users_values = [];
    for (var i = 0; i < em.chart.length; i++) {
        var date = em.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        //            console.log(dd+"/"+mm);

        _dates.push(dd + "/" + mm);
        total_users_values.push(em.chart[i].numbers);
    }
    var starting = em.chart[0].numbers * 0.7;
    em_ending = em.chart[em.chart.length - 1].numbers;
    var ending = em_ending * 1.2;
    if (em_ending > 1000) {
        //            users_created_ending=Math.round(users_created_ending/1000,1)+"k";
        em_ending = em_ending / 1000;
        em_ending = (em_ending.toFixed(1)) + "k";
    }
    document.getElementById("engaged_employees").innerHTML = em.chart[em.chart.length - 1].numbers + "<span class=\"incremental\">(+" + Math.round(em.chart[em.chart.length - 1].numbers - em.chart[0].numbers) + ")</span>";
    //        console.log(data[0].em.total_users);

    var engaged_employed = [];

    for (var i = 0; i < em.chart.length; i++) {
        engaged_employed.push(em.chart[i].numbers);
    }

    Highcharts.chart('chart3', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            min: starting,
            max: ending,
            startOnTick: true,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [
            {
                name: trans_key.active_employees,
                color: '#828282',
                data: active_users_values
            },
            {
                name: trans_key.eng_employees,
                color: '#9DC8F1',
                data: engaged_employed
            }]
    });
}

var employee_evanghelists = [];

function create_chart4(em) {
    var _dates = [];
    _dates = [];
    employee_evanghelists = [];
    total_users_values = [];
    for (var i = 0; i < em.chart.length; i++) {
        var date = em.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        //            console.log(dd+"/"+mm);

        _dates.push(dd + "/" + mm);
        total_users_values.push(em.chart[i].numbers);
    }
    var starting = em.chart[0].numbers * 0.7;
    em_ending = em.chart[em.chart.length - 1].numbers;
    var ending = em_ending * 1.2;

    //        console.log(em_ending);

    for (var i = 0; i < em.chart.length; i++)//corrected
    {
        employee_evanghelists.push(em.chart[i].numbers);
    }

    Highcharts.chart('chart4', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            min: starting,
            max: ending,
            startOnTick: true,
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [
            {
                name: trans_key.promoters,
                color: '#f3c44d',
                data: employee_evanghelists
            }]
    });
}

var active_jobs = [];

function create_chart5(data) {
    var _dates = [];
    for (var i = 0; i < data[1].active_jobs.chart.length; i++) {
        active_jobs.push(data[1].active_jobs.chart[i].numbers);
        var date = data[1].active_jobs.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);

    }


    Highcharts.chart('chart5', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Active jobs',
            color: '#828282',
            data: active_jobs
        }]
    });
}


function create_chart6(data) {
    var jobs_with_shares = [];

    jobs_with_shares = (Object.keys(data[1].jobs_shared.chart));   //jobs that were shared

    var jobs_with_shares_number = [];
    for (var i = 0; i < jobs_with_shares.length; i++) {
        jobs_with_shares_number.push(data[1].jobs_shared.chart[jobs_with_shares[i]]);
    }


    Highcharts.chart('chart6', {
        exporting: {enabled: false},
        chart: {
            type: 'bar',
            marginLeft: 200
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: jobs_with_shares
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false,
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: trans_key.shared_jobs,
            color: '#9DC8F1',
            data: jobs_with_shares_number
        }]
    });
}

function create_chart7(data) {
    var jobs_with_views = [];
    var jobs_with_views_number = [];
    jobs_with_views = (Object.keys(data[1].jobs_viewed.chart));
    //        console.log(jobs_with_views);

    var jobs_with_views_number = [];
    for (var i = 0; i < jobs_with_views.length; i++) {
        jobs_with_views_number.push(data[1].jobs_viewed.chart[jobs_with_views[i]]);
    }

    for (var i = 0; i < jobs_with_views.length; i++) {
        jobs_with_views[i] = parseInt(jobs_with_views[i]);
    }

    //        console.log(jobs_with_views)

    Highcharts.chart('chart7', {
        exporting: {enabled: false},
        chart: {
            type: 'bar',
            marginLeft: 200
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: jobs_with_views_number.reverse()
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: trans_key.viewed_jobs,
            color: '#f3c44d',
            data: jobs_with_views.reverse()
        }]
    });
}

var jobs_with_applications = [];

function create_chart8(data) {
    jobs_with_applications = (Object.keys(data[1].jobs_with_applications.chart));


    var jobs_with_applications_number = [];
    for (var i = 0; i < jobs_with_applications.length; i++) {
        jobs_with_applications_number.push(data[1].jobs_with_applications.chart[jobs_with_applications[i]]);
        jobs_with_applications[i] = parseInt(jobs_with_applications[i]);
    }


    Highcharts.chart('chart8', {
        exporting: {enabled: false},
        chart: {
            type: 'bar',
            marginLeft: 200
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: jobs_with_applications_number.reverse()
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false,
            reversed: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: trans_key.jobs_with_applications,
            color: '#82be00',
            data: jobs_with_applications.reverse()
        }]
    });
}

var views_of_jobs = [];

function create_chart9(data) {
    var _dates = [];

    for (var i = 0; i < data[2].views_of_jobs.chart.length; i++) {
        views_of_jobs.push(data[2].views_of_jobs.chart[i].numbers);
        var date = data[2].views_of_jobs.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    var ending = data[2].views_of_jobs.chart[data[2].views_of_jobs.chart.length - 1].numbers;
    // document.getElementById('view_of_candidates').innerText = ending;

    Highcharts.chart('chart9', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.job_views,
            color: '#828282',
            data: views_of_jobs
        }]
    });
}

var potential_candidates = [];

function create_chart13(data) {
    var _dates = [];
    for (var i = 0; i < data[2].potential_candidates.chart.length; i++) {
        potential_candidates.push(data[2].potential_candidates.chart[i].numbers);
        var date = data[2].potential_candidates.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    Highcharts.chart('chart13', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.potential_candidates,
            color: '#828282',
            data: potential_candidates
        }]
    });
}


var candidates_base = [];

function create_chart10(data) {
    var _dates = [];
    candidates_base = [];
    for (var i = 0; i < data[2].total_candidates_accounts.chart.length; i++) {
        candidates_base.push(data[2].total_candidates_accounts.chart[i].numbers);
        var date = data[2].total_candidates_accounts.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    Highcharts.chart('chart10', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.candidates_accounts,
            data: candidates_base
        }]
    });
}


var new_candidate_accounts = [];

function create_chart14(data) {
    var _dates = [];
    for (var i = 0; i < data[2].new_candidates_accounts.chart.length; i++) {
        new_candidate_accounts.push(data[2].new_candidates_accounts.chart[i].numbers);
        var date = data[2].new_candidates_accounts.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    Highcharts.chart('chart14', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.candidates_new,
            data: new_candidate_accounts
        }]
    });
}


var unique_applicants_candidates = [];
var unique_applicants_employees = [];

function create_chart15(data) {
    var _dates = [];
    for (var i = 0; i < data[2].unique_applicants.chart.length; i++) {
        unique_applicants_candidates.push(data[2].unique_applicants.chart[i]['candidates']);
        unique_applicants_employees.push(data[2].unique_applicants.chart[i]['employees']);
        var date = data[2].unique_applicants.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    document.getElementById("unique_applicants").innerText = data[2].unique_applicants.total;
    Highcharts.chart('chart15', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            },
            area: {
                stacking: 'normal',
                // lineColor: '#666666',
                // lineWidth: 1,
                // marker: {
                //     lineWidth: 1,
                //     lineColor: '#666666'
                // }
            }
        },
        series: [
            {
                name: trans_key.unique_candidates,
                color: '#f3c44d',
                data: unique_applicants_candidates
            },
            {
                name: trans_key.unique_employees,
                color: '#B7CCE1',
                data: unique_applicants_employees
            }]
    });
}

var applicants_by_status = [];
function create_chart16(data) {
    var _steps = [];
    for (var i = 0; i < data[2].applicants_by_status.chart.length; i++) {
        applicants_by_status.push(data[2].applicants_by_status.chart[i].numbers);
        var step = data[2].applicants_by_status.chart[i].step;
        _steps.push(step);
    }

    Highcharts.chart('chart16', {
        exporting: {enabled: false},
        chart: {
            type: 'bar',
            marginLeft: 100
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: _steps
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false,
            reversed: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: trans_key.unique_applicants_status,
            color: '#82be00',
            data: applicants_by_status
        }]
    });
}


var applications = [];

function create_chart11(data) {
    applications = [];
    var _dates = [];
    for (var i = 0; i < data[2].total_applications.chart.length; i++) {
        applications.push(data[2].total_applications.chart[i].numbers);
        var date = data[2].total_applications.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }
    var ending = data[2].total_applications.chart[data[2].total_applications.chart.length - 1].numbers;
    // document.getElementById('total_applications').innerText = ending;
    Highcharts.chart('chart11', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: trans_key.total_applications,
            color: '#f3c44d',
            data: applications
        }]
    });
}


function create_chart12(data) {
    var _dates = [];
    var total_applications = [];
    for (var i = 0; i < data[2].in_recruiting_process_applications.chart.length; i++) {
        total_applications.push(data[2].in_recruiting_process_applications.chart[i].numbers);
        var date = data[2].in_recruiting_process_applications.chart[i].date;
        var dd = date.slice(-2);
        var mm = date.substring(7, 2).slice(-2);
        _dates.push(dd + "/" + mm);
    }

    //        console.log(total_applications);

    Highcharts.chart('chart12', {
        exporting: {enabled: false},
        legend: {
            enabled: false
        },
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: _dates
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            // name: "<?php echo ucfirst($this->translate->_('Applications in process')); ?>",
            name: trans_key.process_applications,
            data: total_applications,
            color: '#82be00'
        }]
    });
}

/*
                color: '#82be00',

 */
function create_chart_asd(data) {
    var channels = [];
    var views = [];
    var applications = [];

    for (var i = 0; i < data['channels'].length; i++) {
        channels.push(data['channels'][i].title);
        views.push(data['channels'][i].views);
        applications.push(data['channels'][i].applications);

    }

    Highcharts.chart('spesal', {
        exporting: {enabled: false},
        chart: {
            type: 'column'
        },
        title: {
            text: trans_key.t_channel
        },
        xAxis: {
            categories: channels
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    //                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            //                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: '',
                dataLabels: {
                    enabled: true,
                    //                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
            name: trans_key.views,
            color: '#828282',
            data: views
        }, {
            name: trans_key.applications,
            color: '#f3c44d',
            data: applications
        }]
    });
}


function fetch_candidates(data) {
    //data[0].active_employee.chart[0].numbers

    $("#view_of_candidates_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].views_of_jobs.vs_prior_period_percent, data[2].views_of_jobs.vs_ly_percent));

    $("#total_candidates_accounts_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].total_candidates_accounts.vs_prior_period_percent, data[2].total_candidates_accounts.vs_ly_percent));

    $("#total_applications_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].total_candidates_accounts.vs_prior_period_percent, data[2].total_candidates_accounts.vs_ly_percent));

    $("#in_recruiting_process_applications_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].in_recruiting_process_applications.vs_prior_period_percent, data[2].in_recruiting_process_applications.vs_ly_percent));

    $("#new_candidates_accounts_created_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].in_recruiting_process_applications.vs_prior_period_percent, data[2].in_recruiting_process_applications.vs_ly_percent));

    // $("#unique_applicants_").append(
    //     //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
    //     create_arrow(data[2].unique_applicants.vs_prior_period_percent, data[2].unique_applicants.vs_ly_percent));

    $("#applicants_by_status_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].unique_applicants.vs_prior_period_percent, data[2].unique_applicants.vs_ly_percent));

    $("#potential_candidates_to_total_applications_").append(
        //                create_arrow(data[2].potential_candidates.vs_ly_percent, data[2].potential_candidates.vs_ly_percent))
        create_arrow(data[2].applicants_by_status.vs_prior_period_percent, data[2].applicants_by_status.vs_ly_percent));

    var potential_candidates_no = data[2].potential_candidates.chart[data[2].potential_candidates.chart.length - 1].numbers;
    var views_candidates_no = data[2].views_of_jobs.chart[data[2].views_of_jobs.chart.length - 1].numbers;
    var views_no_per_candidate = (views_candidates_no / potential_candidates_no).toFixed(1);

    var unique_applicants_no = data[2].unique_applicants.chart[data[2].unique_applicants.chart.length - 1].numbers;
    var views_applicants_no = data[2].total_applications.chart[data[2].total_applications.chart.length - 1].numbers;
    var applications_no_per_applicant = (views_applicants_no / unique_applicants_no).toFixed(1);
    var unique_applicants_candidates_total=data[2].unique_applicants.chart[data[2].unique_applicants.chart.length - 1].candidates;
    var candidates_base=data[2].total_candidates_accounts.total;
    var candidate_through_applicants = (unique_applicants_candidates_total / candidates_base * 100).toFixed(0);
    //        console.log(candidate_through_applicants);

    //        document.getElementById("total_users").innerText = data[0].users_created.total_users;


    document.getElementById("view_of_candidates").innerText = data[2].views_of_jobs.total;
    document.getElementById("potential_candidates").innerText = data[2].potential_candidates.total;
    document.getElementById("total_applications").innerText = data[2].total_applications.total;
    document.getElementById("unique_applicants").innerText = data[2].unique_applicants.total;
    document.getElementById("total_candidates_accounts").innerText = data[2].total_candidates_accounts.total;
    document.getElementById("in_recruiting_process_applications").innerText = data[2].in_recruiting_process_applications.total;
    document.getElementById("new_candidates_accounts_created").innerText = data[2].new_candidates_accounts.total;
    document.getElementById("applicants_by_status").innerText = data[2].applicants_by_status.total;

    //=======================================================================

    //        document.getElementById("active_employees_to_engaged_employees_rate").innerText = Math.round(data[0].users_created_to_active_employess_rate) + "%";//incorrect rate from backend

    //        document.getElementById("engaged_employees_to_employee_evanghelists_rate").innerText = Math.round(data[0].active_employees_to_engaged_employees_rate) + "%"; //incorrect rate from backend

    // if ((isNaN(views_no_per_candidate)))
    //     document.getElementById("potential_candidates_to_total_applications").innerHTML = 0 + "<span class='rate-explanation' title='Average number of views per potential candidate'>avg views/person</span>";
    // else
    //     document.getElementById("potential_candidates_to_total_applications").innerHTML = views_no_per_candidate + "<span class='rate-explanation' title='Average number of views per potential candidate'>avg views/person</span>";
    // if ((isNaN(applications_no_per_applicant)))
    //     document.getElementById("views_of_jobs_to_potential_candidates").innerHTML = 0 + "<span class='rate-explanation' title='Average number of applications per applicant'>avg applications/applicant</span>";
    // //        document.getElementById("total_applications_to_unique_applicants").innerText = Math.round(data[2].total_applications_to_unique_applicants) + "%";
    // else
    //     document.getElementById("views_of_jobs_to_potential_candidates").innerHTML = applications_no_per_applicant + "<span class='rate-explanation' title='Average number of applications per applicant'>avg applications/applicant</span>";

    if (isNaN(candidate_through_applicants))
        candidate_through_applicants = 0;
    if (candidate_through_applicants < 5) {
        document.getElementById("pamrgargouagoa").innerHTML = trans_key.app_conversion.replace("candidate_through_applicants", candidate_through_applicants)+".</b>";
        $('#pamrgargouagoa').addClass("opportunity");
    } else {
        document.getElementById("pamrgargouagoa").innerHTML = "Applicants conversion rate from candidates base looks great at <b> " + candidate_through_applicants + "% </b>";
        $('#jobs_viewed_to_jobs_with_applications').addClass("awesome-rate");
    }


    //        $("#total_applications_to_unique_applicants_").append(
    //                create_arrow(data[2].total_applications.vs_prior_period_percent, data[2].total_applications.vs_ly_percent))
    $("#total_applications_to_unique_applicants_").append(
        create_arrow(data[2].potential_candidates.vs_prior_period_percent, data[2].potential_candidates.vs_ly_percent))
    // $("#views_of_jobs_to_potential_candidates_").append(
    //     create_arrow(data[2].total_applications.vs_prior_period_percent, data[2].total_applications.vs_ly_percent))

    $("#unique_applicants_").append(
        create_arrow(data[2].unique_applicants.vs_prior_period_percent, data[2].unique_applicants.vs_ly_percent));

    //Employees engagement looks great at ";"Employees engagement can be improved from
    //        console.log(data[0].engaged_employees_to_employee_evanghelists_rate);


    //        if (data[2].total_applications_to_unique_applicants < 20) {
    //            document.getElementById("pamrgargouagoa").innerText = "Candidates conversion rate can be improved from " + data[2].total_applications_to_unique_applicants + "%";
    //        } else {
    //            document.getElementById("pamrgargouagoa").innerText = "Candidates conversion rate looks very good at " + data[2].total_applications_to_unique_applicants + "%";
    //        }

    //        document.getElementById("mhiwenagpgb").innerText = "Jobs applications rate looks great at " + data[1].jobs_viewed_to_jobs_with_applications + "%"
    //        document.getElementById("pamrgargouagoa").innerText = "Candidates conversion rate looks very good: " + data[2].total_applications_to_unique_applicants + '%';

    create_chart9(data);
    create_chart10(data);
    create_chart11(data);
    create_chart12(data);
    create_chart13(data);
    create_chart14(data);
    create_chart15(data);
    create_chart16(data);
    create_chart_asd(data);

}

function fetch_employees(users_created, active_employee, engaged_employees, employee_evanghelists) {
    //        console.log(users_created);
    //        var total_employees_no = users_created.chart[users_created.chart.length - 1].numbers;
    //        var current_employees_no = active_employee[0].chart[active_employee[0].chart.length - 1].numbers;
    //        var engaged_employees_no = engaged_employees[0].chart[engaged_employees[0].chart.length - 1].numbers;
    //        var employee_evanghelists_no = employee_evanghelists[0].chart[employee_evanghelists[0].chart.length - 1].numbers;
    //
    //        document.getElementById("total_users").innerText = users_created[0].total_users;
    //        document.getElementById("active_employees").innerText = active_employee[0].total_users;
    //        document.getElementById("engaged_employees").innerText = engaged_employees[0].total_users;
    //        document.getElementById("employee_evanghelists").innerText = employee_evanghelists[0].total_users;
    //
    //
    //        create_chart1(employees);
    //        create_chart2(employees);
    //        create_chart3(employees);
    //        create_chart4(employees);
    //
    //        $("#active_employees_to_engaged_employees_rate_").append(
    //                create_arrow(users_created.vs_prior_period_percent, users_created.vs_ly_percent))
    //        $("#engaged_employees_to_employee_evanghelists_rate_").append(
    //                create_arrow(active_employee.vs_prior_period_percent, active_employee.vs_ly_percent))
    //        $("#users_created_to_active_employess_rate_").append(
    //                create_arrow(engaged_employees.vs_prior_period_percent, engaged_employees.vs_ly_percent))
    //        $("#employee_evanghelists_").append(
    //                create_arrow(employee_evanghelists.vs_prior_period_percent, employee_evanghelists.vs_ly_percent))
    //
    //        document.getElementById("active_employees_to_engaged_employees_rate").innerHTML = Math.round(current_employees_no / total_employees_no * 100) + "%" + "<span class='rate-explanation' title='Current employees number / Total number of created employees'>current/total</span>";//incorrect rate
    //        if (Math.round(employee_evanghelists_no / engaged_employees_no * 100) < 10) {
    //            document.getElementById("poaagrmpiaga").innerHTML = "Employees promoters score should be improved from <b>" + Math.round(employee_evanghelists_no / engaged_employees_no * 100) + "%</b> out of your <b>" + engaged_employees_no + "</b> currently signed in employees. Your potential could grow even more if we engage (convince to sign in) more than currently <b>" + Math.round(engaged_employees_no / current_employees_no * 100) + "%</b> of employees.";
    //            $('#poaagrmpiaga').addClass("opportunity");
    //            $('#users_created_to_active_employess_rate').addClass("opportunity-rate");
    //        } else {
    //            document.getElementById("poaagrmpiaga").innerText = "Employees engagement looks great at <b>" + Math.round(employee_evanghelists_no / engaged_employees_no * 100) + "</b>%";
    //            $('#users_created_to_active_employess_rate').addClass("awesome-rate");
    //        }
    //        document.getElementById("engaged_employees_to_employee_evanghelists_rate").innerHTML = Math.round(engaged_employees_no / current_employees_no * 100) + "%" + "<span class='rate-explanation' title='Engaged employees number / Current employees number'>engaged/current</span>";
    //
    ////        document.getElementById("users_created_to_active_employess_rate").innerText = Math.round(parseInt(data[0].engaged_employees_to_employee_evanghelists_rate)) + "%";//incorrect rate from backend
    //        document.getElementById("users_created_to_active_employess_rate").innerHTML = Math.round(employee_evanghelists_no / engaged_employees_no * 100) + "%" + "<span class='rate-explanation' title='Employees that promoted at elast one job / Engaged employees number'>promoters/engaged</span>";
    //        console.log(Math.round(employee_evanghelists_no / engaged_employees_no * 100) + "%");

}

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time)
)
    ;
}

function update_rates() {
    //        $("#active_employees_to_engaged_employees_rate_").append(
    //                create_arrow(created_employees_o.vs_prior_period_percent, created_employees_o.vs_ly_percent));
    //        $("#engaged_employees_to_employee_evanghelists_rate_").append(
    //                create_arrow(active_employees_o.vs_prior_period_percent, active_employees_o.vs_ly_percent))
    //        $("#users_created_to_active_employess_rate_").append(
    //                create_arrow(engaged_employees_0.vs_prior_period_percent, engaged_employees_0.vs_ly_percent))
    //        $("#employee_evanghelists_").append(
    //                create_arrow(evanghelists_employees_0.vs_prior_period_percent, evanghelists_employees_0.vs_ly_percent))
     if (!total_employees_no || !active_employees_no || !engaged_employees_no || !evanghelists_employees_no) {
        if (!total_employees_no)
            total_employees_no = 0;
        if (!active_employees_no)
            active_employees_no = 0;
        if (!engaged_employees_no)
            engaged_employees_no = 0
        if (!evanghelists_employees_no)
            evanghelists_employees_no = 0;
    }
    // if (active_employees_no != 0 && total_employees_no != 0)
    //     document.getElementById("active_employees_to_engaged_employees_rate").innerHTML = Math.round(active_employees_no / total_employees_no * 100) + "%" + "<span class='rate-explanation' title='Current employees number / Total number of created employees'>current/total</span>";//incorrect rate
    // else
    //     document.getElementById("active_employees_to_engaged_employees_rate").innerHTML = 0 + "%" + "<span class='rate-explanation' title='Current employees number / Total number of created employees'>current/total</span>";//incorrect rate

    var a = Math.round(evanghelists_employees_no / engaged_employees_no * 100);
    var b = Math.round(engaged_employees_no / active_employees_no * 100);
    if (isNaN(a) || a == Infinity)
        a = 0;
    if (isNaN(b) || b == Infinity)
        b = 0;
    if (Math.round(evanghelists_employees_no / engaged_employees_no * 100) < 10) {
        document.getElementById("poaagrmpiaga").innerHTML = trans_key.engaged_employee.replace('a',a)
            .replace('engaged_employees_no',engaged_employees_no)
            .replace('b_data',b);
        $('#poaagrmpiaga').addClass("opportunity");
        // $('#active_employees').addClass("opportunity-rate");
    } else {

        document.getElementById("poaagrmpiaga").innerHTML = trans_key.engaged_great+" <b>" + a + "</b>%";
        // $('#active_employees').addClass("awesome-rate");
    }

    if (engaged_employees_no != 0 && active_employees_no != 0)
        document.getElementById("engaged_employees_to_employee_evanghelists_rate").innerHTML = Math.round(engaged_employees_no / active_employees_no * 100) + "%" + "<span class='rate-explanation' title='"+trans_key.engaged_employee+" "+trans_key.number+" / "+trans_key.engaged_employee+" "+trans_key.number+"'>"+trans_key.engaged+"/"+trans_key.current+"</span>";
    else
        document.getElementById("engaged_employees_to_employee_evanghelists_rate").innerHTML = 0 + "%" + "<span class='rate-explanation' title='Engaged employees number / Current employees number'>engaged/current</span>";

    //        document.getElementById("users_created_to_active_employess_rate").innerText = Math.round(parseInt(data[0].engaged_employees_to_evanghelists_employees_no_rate)) + "%";//incorrect rate from backend
    document.getElementById("users_created_to_active_employess_rate").innerHTML = Math.round(evanghelists_employees_no / engaged_employees_no * 100) + "%" + "<span class='rate-explanation' title='"+trans_key.e_over_one_job+" "+trans_key.number+" / "+trans_key.engaged_employee+" "+trans_key.number+"'>"+trans_key.promoters+"/"+trans_key.engaged+"</span>";
}


function fetch_created_employees(em) {
    //        console.log(em.users_created);
    total_employees_no = em.users_created.chart[em.users_created.chart.length - 1].numbers;
    // document.getElementById("total_users").innerText = em.users_created.total_users;
    // create_chart1(em.users_created);
    //
    // $("#active_employees_to_engaged_employees_rate_").append(
    //     create_arrow(em.users_created.vs_prior_period_percent, em.users_created.vs_ly_percent));

}


function fetch_active_employees(em) {
    active_employees_no = em.active_employee.chart[em.active_employee.chart.length - 1].numbers;

    document.getElementById("active_employees").innerText = em.active_employee.total_users ? em.active_employee.total_users_values : 0;
    create_chart2(em.active_employee);

    $("#engaged_employees_to_employee_evanghelists_rate_").append(
        create_arrow(em.active_employee.vs_prior_period_percent, em.active_employee.vs_ly_percent));

}


function fetch_engaged_employees(em) {
    //        console.log(em.engaged_employees);
    engaged_employees_no = em.engaged_employees.chart[em.engaged_employees.chart.length - 1].numbers;
    document.getElementById("engaged_employees").innerText = em.engaged_employees.total_users ? em.engaged_employees.total_users : 0;
    create_chart3(em.engaged_employees);

    $("#users_created_to_active_employess_rate_").append(
        create_arrow(em.engaged_employees.vs_prior_period_percent, em.engaged_employees.vs_ly_percent));

}


function fetch_evanghelists_employees(em) {
           // console.log(em);
    evanghelists_employees_no = em.employee_evanghelists.chart[em.employee_evanghelists.chart.length - 1].numbers;
    document.getElementById("employee_evanghelists").innerText = em.employee_evanghelists.total_users;
    create_chart4(em.employee_evanghelists);

    $("#employee_evanghelists_").append(
        create_arrow(em.employee_evanghelists.vs_prior_period_percent, em.employee_evanghelists.vs_ly_percent));

}

function fetch_promoters_shares(em) {
           // console.log(em);
    shares_of_jobs_no = em.shares_of_jobs.chart[em.shares_of_jobs.chart.length - 1].numbers;
    document.getElementById("total_shares").innerText = em.shares_of_jobs.total;
    create_chart111(em.shares_of_jobs);

    $("#total_shares_").append(
        create_arrow(em.shares_of_jobs.vs_prior_period_percent, em.shares_of_jobs.vs_ly_percent));

}

function fetch_jobs(data) {
    document.getElementById("active_jobs").innerText = data[1].active_jobs.total_jobs;
    document.getElementById("jobs_shared").innerText = data[1].jobs_shared.total_jobs;
    document.getElementById("jobs_viewed").innerText = data[1].jobs_viewed.total_jobs;
    document.getElementById("jobs_with_applications").innerText = data[1].jobs_with_applications.total_jobs;

    if (!isNaN(Math.round(data[1].jobs_shared.total_jobs / data[1].active_jobs.total_jobs * 100)))
        document.getElementById("active_jobs_to_jobs_shared").innerHTML = Math.round(data[1].jobs_shared.total_jobs / data[1].active_jobs.total_jobs * 100) + "%" + "<span class='rate-explanation' title='Number of shared jobs / Total number of available jobs in selected period'>shared/vacancies</span>";
    else
        document.getElementById("active_jobs_to_jobs_shared").innerHTML = "0%" + "<span class='rate-explanation' title='Number of shared jobs / Total number of available jobs in selected period'>shared/vacancies</span>";

    if (!isNaN(Math.round(data[1].jobs_viewed.total_jobs / data[1].active_jobs.total_jobs * 100)))
        document.getElementById("jobs_shared_to_jobs_viewed").innerHTML = Math.round(data[1].jobs_viewed.total_jobs / data[1].active_jobs.total_jobs * 100) + "%" + "<span class='rate-explanation' title='Number of viewed jobs / Total number of available jobs in selected period'>viewed/vacancies</span>";
    else
        document.getElementById("jobs_shared_to_jobs_viewed").innerHTML = "0%" + "<span class='rate-explanation' title='Number of viewed jobs / Total number of available jobs in selected period'>viewed/vacancies</span>";

    if (!isNaN(Math.round(data[1].jobs_with_applications.total_jobs / data[1].active_jobs.total_jobs * 100)))
        document.getElementById("jobs_viewed_to_jobs_with_applications").innerHTML = Math.round(data[1].jobs_with_applications.total_jobs / data[1].active_jobs.total_jobs * 100) + "%" + "<span class = 'rate-explanation' title = 'Number of jobs with applicants/ Total number of available jobs in selected period' > applied / vacancies </span>";
    else
        document.getElementById("jobs_viewed_to_jobs_with_applications").innerHTML = "0%" + "<span class = 'rate-explanation' title = 'Number of jobs with applicants/ Total number of available jobs in selected period' > applied / vacancies </span>";


    $("#active_jobs_to_jobs_shared_").append(
        create_arrow(data[1].active_jobs.vs_prior_period_percent, data[1].active_jobs.vs_ly_percent))
    $("#jobs_shared_to_jobs_viewed_").append(
        create_arrow(data[1].jobs_shared.vs_prior_period_percent, data[1].jobs_shared.vs_ly_percent))
    $("#jobs_viewed_to_jobs_with_applications_").append(
        create_arrow(data[1].jobs_viewed.vs_prior_period_percent, data[1].jobs_viewed.vs_ly_percent))
    $("#jobs_with_applications_").append(
        create_arrow(data[1].jobs_with_applications.vs_prior_period_percent, data[1].jobs_with_applications.vs_ly_percent))

    $ja_rate = Math.round(data[1].jobs_with_applications.total_jobs / data[1].active_jobs.total_jobs * 100);
    if (isNaN($ja_rate))
        $ja_rate = 0;

    if ($ja_rate < 10) {
        document.getElementById("mhiwenagpgb").innerHTML = trans_key.jobs_app_rate+" <b>" + $ja_rate + "%</b>";
        $('#mhiwenagpgb').addClass("opportunity");
        $('#jobs_viewed_to_jobs_with_applications').addClass("opportunity-rate");
    } else {
        document.getElementById("mhiwenagpgb").innerHTML = trans_key.app_rate+" <b>" + $ja_rate + "%</b>";
        $('#jobs_viewed_to_jobs_with_applications').addClass("awesome-rate");
    }


    create_chart5(data);
    create_chart6(data);
    create_chart7(data);
    create_chart8(data);
}

var agargarg;
var a = 0, b = 0, c = 0, d = 0;

function balblaalbal() {
    var elem = document.getElementById("poaagrmpiaga");
    var width = 10;
    var id = setInterval(frame, 10);

    function frame() {
        if (width > 96.5) {
            clearInterval(id);
        } else {
            width++;
            if (width == 97)
                width = 97.5;
            elem.style.width = width + '%';
        }
    }
}


function ngoargnorag() {
    if (a > 0 && b > 0 && c > 0 && d > 0) {
        sleep(2000);
        update_rates();
        clearInterval(agargarg);
    }
}

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time)
)
    ;
}


function fetch_ajax(start, end) {
    $.post("/default/admin/get-created-employees", {start, end})
        .done(function (_em) {
            created_employees_o = _em[0];
            total_employees_no=_em[0].users_created.total_users;
            // console.log(total_employees_no);
            a = 1;
            fetch_created_employees(_em[0]);
        }).always(
        $.post("/default/admin/get-engaged-employees", {start, end})
            .done(function (_em) {
                sleep(500);
                engaged_employees_0 = _em[0];
                c = 1;
                fetch_engaged_employees(_em[0]);
            })).always(
        $.post("/default/admin/get-active-employees", {start, end})
            .done(function (_em) {
                sleep(500);
                active_employees_o = _em[0];
                b = 1;
                balblaalbal();
                fetch_active_employees(_em[0]);
            })).always(
        $.post("/default/admin/get-evanghelists-employees", {start, end})
            .done(function (_em) {
                sleep(500);
                evanghelists_employees_0 = _em[0];
                fetch_evanghelists_employees(_em[0]);
                d = 1;
            })).always(
        $.post("/default/admin/get-evanghelists-employees", {start, end})
            .done(function (_em) {
                sleep(500);
                promoters_shares = _em[2];
                fetch_promoters_shares(_em[2]);
                d = 1;
            })).always(
        agargarg = setInterval(ngoargnorag, 5000)
    )

    $.post("/default/admin/get-jobs", {start, end})
        .done(function (_jobs) {
            fetch_jobs(_jobs);
        });
    $.post("/default/admin/get-applicants", {start, end})
        .done(function (_candidates) {
            fetch_candidates(_candidates);
        });

    $('form#export-report-form > input#startExport').val($('#start').val());
    $('form#export-report-form > input#endExport').val($('#end').val());

}

window.onload = function () {
    shares_of_jobs_no = 0;
    evanghelists_employees_no = 0;
    engaged_employees_no = 0;
    active_employees_no = 0;
    total_employees_no = 0;
    var created_employees_o, active_employees_o, engaged_employees_0, evanghelists_employees_0 = [];
    var start = document.getElementById('start').value;
    var end = document.getElementById('end').value;
    fetch_ajax(start, end);
};


