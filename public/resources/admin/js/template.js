CKEDITOR.replace('content', {
    height: 250,
    toolbar:
        [
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
            {name: 'basicstyles', items: ['Bold', 'Italic']},
            {name: 'tools', items: ['Maximize', '-', 'About']}
        ]
});
