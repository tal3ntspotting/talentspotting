var myApp = angular.module('myApp', ['ngAnimate', 'ui.bootstrap', 'flash', 'ngRoute','mp.datePicker']);
var led = angular.module('led', ['ngRoute']);


led.config(function ($routeProvider) {
    $routeProvider

            // route for the leaderboards page
            .when('/leaderboards', {
                templateUrl: '/users/admin/leaderboards-profile',
                controller: 'leaderboards'
            })

            // route for the campaign page
            .when('/campaign', {
                templateUrl: '/users/admin/campaign-profile',
                controller: 'campaign'
            })
            .when('/campaign/:id', {
                templateUrl: '/users/admin/profile-leaderboards-details',
                controller: 'campaignid'
            })
            .when('/jobs/:id', {
                templateUrl: '/users/admin/profile-leaderboards-details',
                controller: 'jobid'
            })

            // route for the contact page
            .when('/jobs', {
                templateUrl: '/users/admin/jobs-profile',
                controller: 'jobs'
            })
            .when('/users/admin/mycvs', {
//                templateUrl: 'users/admin/mycvs',
                controller: 'atsmycvs'
            });
});

/**
 *  Controller shared service
 * @param {array} candidateID
 * @param {array} jobId
 */
myApp.service('myCvService', function () {
    var myCvService = {
        candidateID: "",
        jobId: "",
        status : 0,
        status_reason : 0,
        application_id : 0
    }
    return  {
        setCandidateID: function (id) {
            myCvService.candidateID = id;
        },
        getcandidateID: function () {
            return myCvService.candidateID
        },
        setJobID: function(id) {
            return myCvService.jobId = id ;
        },
        getJobID:function () {
            return myCvService.jobId ;
        },
        setStatus : function(id) {
            myCvService.status = id ;
        },
        getStatus: function() {
            return myCvService.status;
        },
        setStatusReason : function(id) {
            myCvService.status_reason = id ;
        },
        getStatusReason : function() {
            return myCvService.status_reason
        },
        setJobApplicationID : function(id) {
            return myCvService.application_id = id;
        },
        getJobApplicationID : function() {
            return myCvService.application_id ;
        }
    }

});
myApp.controller("tablemycv", ['$scope', '$location', 'myCvService', 'Flash', '$http', '$modal', function ($scope, $location, myCvService, Flash, $http, $modal) {
//        console.log('tablemycv controller');
        $scope.ids = [];
        $scope.check = function (id) {
            $scope.updateIds = function () {
                $scope.ids.filter(function (bool, id) {
                    return id;
                });
            };
        }
//        var message = '<strong> Job Alert !</strong>  Please Choose  candidates to move.';
//                Flash.create('danger', message, 'custom-class');
        $scope.$watch('ids', $scope.updateIds, true);
        $scope.getJobs = function () {
            if ($scope.ids === undefined || $scope.ids.length == 0) {
                // empty
                var message = '<strong> Job Alert !</strong>  Please Choose  candidates to move.';
                Flash.create('danger', message);
                return false;
            }
            myCvService.setCandidateID($scope.ids);

            var modalInstance = $modal.open({
                templateUrl: '/jobspopup',
                controller: "popuptable",
                scope: $scope,
            });

        }

//        $scope.check = function () {
//            if ($scope.ids === undefined || $scope.ids.length == 0) {
//                // empty
//                var message = '<strong> Job Alert !</strong>  Please Choose  candidates to move.';
//                Flash.create('danger', message);
//                return false;
//            }
//
//
//            console.log('send');
//        }
    }]);







myApp.controller("popuptable", ['$rootScope', '$scope', '$location', 'myCvService', 'Flash', '$http', '$modalInstance', '$document', function ($rootScope, $scope, $location, myCvService, Flash, $http, $modalInstance, $document) {

        $scope.ids = [];
        $scope.check = function (id) {
            $scope.updateIds = function () {
                $scope.ids.filter(function (bool, id) {
                    return id;
                });
            };
        }
        $scope.$watch('ids', $scope.updateIds, true);
        $scope.copyProject = function () {
            var candidateId = myCvService.getcandidateID();
            var jobId = $scope.ids;
            if (typeof jobId !== 'undefined' && jobId.length > 0) {
            } else {
                var message = '<strong> Job Alert !</strong>  Please Choose just one job to move candidate.';
                Flash.create('danger', message, 'custom-class');
                return false;
            }
            $http({
                url: "/ajax/index/copy",
                data: {canID: candidateId, jobID: jobId},
                method: "POST"
            }).success(function (data, status) {
                $.each(data, function (key, value) {
                    if (value === true) {
                        var message = '<strong> Project Moved!</strong>  You successfully Move the candidate.';
                        Flash.create('success', message, 'custom-class');
                    } else if (value === 1) {
                        var message = '<strong> Error job!</strong>  Please choose just one job to move candidate.';
                        Flash.create('danger', message, 'custom-class');
                    } else if (value === 2) {
                        var message = '<strong> Error candidate !</strong>  Please choose at least one candidate to move.';
                        Flash.create('danger', message, 'custom-class');
                    }  else if (value === 3) {
                    var message = '<strong> Error candidate !</strong>  Candiate Already applied for this job.';
                    Flash.create('danger', message, 'custom-class');
                    }
                    else {
                        var message = '<strong> Error!</strong>  Please check your smtp configuration .';
                        Flash.create('danger', message, 'custom-class');
                    }
                });
            });
//            console.log($scope.ids);
//            console.log(myCvService.getcandidateID());
        }
        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        }
    }]);







myApp.controller("notify", ['$rootScope', '$scope', '$location', 'myCvService', 'Flash', '$http', '$modalInstance', '$document', function ($rootScope, $scope, $location, myCvService, Flash, $http, $modalInstance, $document) {

//        $(document).ready(function() {
//            $( 'textarea' ).ckeditor();
//        }) ;

//        $scope.close = function () {
//            $modalInstance.dismiss('cancel');
//        }

        $scope.send = function () {
//            console.log('qnhhs');
        }

    }]);
led.controller("leaderboards", ['$scope', '$location', '$http', '$document', function ($scope, $location, $http, $document) {

//        $http({
//            url: "index.php?option=com_affiliates&task=campaigns.campaigns",
//            method: "GET"
//        }).success(function (data, status) {
//            $scope.data = data;
//
//        });

    }]);
led.controller("campaign", ['$scope', '$location', '$http', '$document', function ($scope, $location, $http, $document) {

    }]);
led.controller("campaignid", ['$scope', '$routeParams', '$location', '$http', '$document', function ($scope, $routeParams, $location, $http, $document) {
        var id_campaign = $routeParams.id;
        $http({
            url: "/users/admin/profile-leaderboards-details",
            method: "POST",
            data : {"id_campaign":id_campaign}
        }).success(function (data, status) {
            
            $scope.user = data;

        });

    }]);

led.controller("jobid", ['$scope', '$routeParams', '$location', '$http', '$document', function ($scope, $routeParams, $location, $http, $document) {
        var id_job = $routeParams.id;
        $http({
            url: "/users/admin/profile-leaderboards-details",
            method: "POST",
            data : {"id_job":id_job}
        }).success(function (data, status) {
            
            $scope.user = data;

        });

    }]);
led.controller("jobs", ['$scope', '$routeParams', '$location', '$http', '$document', function ($scope, $routeParams, $location, $http, $document) {
        console.log('jobs controller');

    }]);


myApp.controller("atsmycvs", ['$scope','$sce', '$routeParams', '$location', 'myCvService','$http', '$document','$timeout','$modal','Flash','$attrs', function ($scope,$sce, $routeParams, $location,myCvService, $http, $document,$timeout,$modal,Flash,$attrs) {
        
        var modalInstance;
        var ats_viewed_height = angular.element('.ats-viewed').height() ;
        
        var candidates = [] ;
        
        $scope.init =function (){
            
            $http({
                url: "/ats/admin/get-candidate",
                method: "GET",
            }).success(function (data, status) {

                $scope.candidates = data.candidates;
                var $first = data.candidates.slice(0,1);
                if($first.length > 0) {
                    
                    var $first_id = $first[0]['id'];
                    $scope.status_history = [];
                    $http({
                        url: "/ats/admin/get-candidate-info",
                        method: "POST",
                        data: {"id": $first_id}
                    }).success(function (data, status) {
//                console.log(data.all_status);
                        $scope.candidate = data.candidate;
                        $scope.applications = data.applications;
                        $scope.language = data.language;
                        if($scope.applications.length == 0) $scope.applications_no_data = "No data yet";
                        if (data.status_history.length > 0) $scope.status_history = data.status_history;
                        $scope.positions = data.position;
                        if($scope.positions.length == 0) $scope.positions_no_data = "No data yet";
                        $scope.skills = data.skills ;
                        if($scope.skills.length == 0) $scope.skills_no_data = "No data yet"; 
                        $scope.educations = data.educations;
                        if($scope.educations.length == 0) $scope.educations_no_data = "No data yet";
                        $scope.emails = data.emails;
                        if($scope.emails.length == 0) $scope.emails_no_data = "No data yet";
                        // if($scope.language.length == 0) $scope.language_no_data = "No data yet";

                        $scope.all_status = data.all_status;
                        $scope.note = data.note;
                        
                        $scope.status = data.last_status ;
                        


                        $timeout(function () {

                            var height = angular.element('#header_').height() - ats_viewed_height;
                            $scope.height = height;

                            var ats_candidadate_tbody = angular.element('.ats-candidadate-tbody').height();
                            $scope.tbody_height = height - (ats_candidadate_tbody + ats_viewed_height);
                            $scope.note_add_height = $scope.height - 60;
                            console.log('height : ' + $scope.height);
                            var app_table_height = angular.element('.app-table-height').height();
                            var app_buton_action = angular.element('.app-buton-action').height();
                            var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action);
                            $scope.tbody_height = "height:" + tbody_height + 'px';

                        });

                    });
                    
                    
                    
                }
                
                if(data.me.type == 5) {
//                    $('.div-ats-change-status').remove();
                }
                if($scope.candidates) {
                                $http({
                url: "/ats/admin/get-jobs",
                method: "GET",
            }).success(function (data, status) {
                var jobs = data.jobs   
                $scope.jobs = jobs;
                $scope.filterJobs = $scope.jobs[0];
            });
            $http({
                url: "/ats/admin/get-departments",
                method: "GET",
            }).success(function (data, status) {
                var departments = data.departments   
                $scope.departments = departments;
                $scope.filterDepartments = departments[0];
            });
            $http({
                url: "/ats/admin/get-skills",
                method: "GET",
            }).success(function (data, status) {
                if(data.skills) {
                var skilluri = data.skills
//                skilluri.unshift({'title': 'Skills'});
                $scope.skilluri = skilluri;
                $scope.filterItem = $scope.skilluri[0];
                }
                
            });

                    $http({
                        url: "/ats/admin/get-sources",
                        method: "GET",
                    }).success(function (data, status) {
                        if(data) {
                            $scope.sources = data;
                            $scope.sourceIndex = 0 ;
                        }

                    });

            $http({
                url: "/ats/admin/get-status",
                method: "GET",
            }).success(function (data, status) {
                if(data) {
                    var st = data.status
//                    console.log(st);
//                    var $statusIndex = Object.keys(st).sort(function (a, b) {
//                        return b - a
//                    })[0];
                    $scope.statusIndex = data.default;
                    $scope.st = st;
                    $scope.filterStatus = data.default;
                            setTimeout(function(){
                                $('select[ng-model="filterStatus"] > option')[0].remove();
                            },100)
                    
                }            
            }); 
                }

            });
            

             
        }
        
        window.onload = function() {
            $('.ats-note').css({'height':angular.element('#header_').height() - ats_viewed_height+'px'});
            var ats_mycsv_table = angular.element('.ats-mycsv-table').height();
            if(ats_mycsv_table < angular.element('#header_').height()) {
                var table_height = angular.element('#header_').height() - ats_mycsv_table ;
//                $('.row-with-n-pad').css({'height':table_height+'px'});
            }
            
            var app_table_height = angular.element('.app-table-height').height() ;
            var app_buton_action = angular.element('.app-buton-action').height() ;
            var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action) ;
            $scope.tbody_height = "height:"+tbody_height+'px';
//            console.log('header_ '+angular.element('#header_').height());
//            console.log('app_table_height '+app_table_height);
//            console.log('app_buton_action '+app_buton_action);
            
            $scope.show_add_note = 1 ;
            
        };
   
        
        $scope.getCandidateInfo = function(id) {
            $scope.status_history = [];
            $http({
                url: "/ats/admin/get-candidate-info",
                method: "POST",
                data: {"id": id,"status":true}
            }).success(function (data, status) {
//                console.log(data.all_status);
                $scope.candidate = data.candidate;
                
                $scope.applications = data.applications;
                $scope.language = data.language;
                if(data.status_history.length > 0) $scope.status_history = data.status_history;
                $scope.positions = data.position;
                $scope.skills = data.skills;
                $scope.educations = data.educations;
                $scope.all_status = data.all_status;
                $scope.note = data.note;
                $scope.emails = data.emails;
                $scope.email_history = data.email_history;
                if($scope.applications.length <= 0) $scope.applications_no_data = "";
                if($scope.emails.length <= 0) $scope.emails_no_data = "";
                if($scope.skills.length <= 0) $scope.skills_no_data = "";
                if($scope.educations.length <= 0) $scope.educations_no_data = "";
                if($scope.positions.length <= 0) $scope.positions_no_data = "";
                if($scope.language.length == 0) $scope.language_no_data = "No data yet";
                
                $scope.status = data.last_status ;
                $timeout(function () {
                    $('.ats-note').css({'height':angular.element('#header_').height() - ats_viewed_height+'px'});
                    var height = angular.element('#header_').height() - ats_viewed_height ;
                    $scope.height = height;
                    
                    var ats_candidadate_tbody = angular.element('.ats-candidadate-tbody').height() ;
                    $scope.tbody_height = height - (ats_candidadate_tbody + ats_viewed_height);
                    $scope.note_add_height = $scope.height-60 ;
                    console.log('height : '+$scope.height);
                    var app_table_height = angular.element('.app-table-height').height() ;
            var app_buton_action = angular.element('.app-buton-action').height() ;
            var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action) ;
            $scope.tbody_height = "height:"+tbody_height+'px';
           
                });

            });
        }
        
        $scope.getJobaStatus = function(application_id) {
            
             $http({
                    url: "/ats/admin/get-application-status",
                            method: "POST",
                            data: {"id": application_id}
                    }).success(function (data, status) {
                        $scope.status = data.status ;
//                        console.log(data);
                    });
        };
        
        
        
        $scope.changeApplicationStatus = function() {

            console.log($scope.applications);
            console.log($scope.applications.length);
            if($scope.applications.length > 0)
            {
                myCvService.setJobID($scope.applications[0].id);
                myCvService.setJobApplicationID($scope.applications[0].job_application_id);

            }

            var id = ($('.ats-change-status').attr('data-hash'));
//            console.log(id);
            if(!id)
            {
            var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
            Flash.create('warning', message);
            return false;
            }
             modalInstance = $modal.open({
                templateUrl: '/ats/admin/change-application-status',
                scope: $scope,
            });
            
//            modalInstance.$promise.then(modalInstance.show);
//         
        }
        
           $scope.close = function () {
//               console.log('close function');
//               monModel.hide();
                modalInstance.close();
//                $scope.test.close();
            }
//         $scope.test = $modal.open({
//            templateUrl: '/ats/admin/change-application-status',
//            scope: $scope,
//        });
//        
        $scope.job_name = "Select Job" ;
        $scope.populate_job_name  = function(name,value,application_id) 
        {
//            console.log(value);
            if($scope.job_name == "") $scope.job_name = "Select Job";
            $scope.job_name = name ;
            myCvService.setJobID(value);
            myCvService.setJobApplicationID(application_id);
        };
        
        
        $scope.status_name = "Change Status" ;
        $scope.populate_status  = function(status,value) 
        {
            console.log(status);
            console.log(value);
            if($scope.status_name == "") 
            {
                $scope.status_name = "Change Status";
                $scope.status_value = "";
            }
            var app_status ;
            var app_status_reason ;
//            for (var i in value) {
                myCvService.setStatus(status);
                myCvService.setStatusReason(status);
//            }
            
            $scope.status_name = value;
            $scope.status_value = status;
            
            
 
        };
        
        $scope.saveStatus = function() {
            
            var selected_status = ($('#status-selected').attr('data-status-selected'));
//            console.log('status : '+myCvService.getStatus());
//            console.log('status_reason : '+myCvService.getStatusReason());
//            console.log('jobId : '+myCvService.getJobID());
//            console.log('jobApplicationId : '+myCvService.getJobApplicationID());
            if(myCvService.getJobApplicationID() == 0) {
                // modalInstance.close()
                var message = '<strong> Change Status : !</strong>  Please select a job application .';
                Flash.create('warning', message);
                return false;
            }
            $http({
                url: "/ats/admin/set-candidate-status",
                method: "POST",
                data: {"status": myCvService.getStatus(),
                       'status_reason' : myCvService.getStatusReason(),
                       'id_job' : myCvService.getJobID(),
                       'id_application' : myCvService.getJobApplicationID()}
            }).success(function(data, status) {
                $scope.applications = data.applications;
                $scope.status= data.applications[0].application_status;
                if (data.status_history.length > 0) $scope.status_history = data.status_history;
                var target = data.data_target ;
                var status_class = "" ;
                if(myCvService.getStatus() == '13') status_class = "banned";
                if(myCvService.getStatus() == '5') status_class = "hired";
                $('tr[data-target="'+target+'"]').removeClass('banned hired');
                $('tr[data-target="'+target+'"]').addClass(status_class);
                $('tr[data-target="'+target+'"]').find('a').find('span').addClass(status_class);

                myCvService.setJobID("");
                myCvService.setJobApplicationID(0);
                    
            })
            modalInstance.close()
            var message = '<strong> Change Status : !</strong>  Successfully Status changed.';
            Flash.create('success', message);
            return false;
        }
        

        
        
        //////// Date Picker
        $scope.dp_class = "display:none";
        $scope.formatDate = function (date) {
            function pad(n) {
                return n < 10 ? '0' + n : n;
            }

            return date && date.getFullYear()
                    + '-' + pad(date.getMonth() + 1)
                    + '-' + pad(date.getDate());
        };

        $scope.parseDate = function (s) {
            var tokens = /^(\d{4})-(\d{2})-(\d{2})$/.exec(s);

            return tokens && new Date(tokens[1], tokens[2] - 1, tokens[3]);
        };
        
        $scope.changeClass = function() {
//            console.log('change class proccess');
            if ($scope.dp_class === "display:none")
                $scope.dp_class = "display:inline-block";
            else
                $scope.dp_class = "display:none";
        }
        
        $scope.closeDropdown = function() {
            $scope.dp_class = "display:none";
            var date = $scope.date ;
//            console.log(date);
        }
        
        $scope.customFilter = function (data)
        {
//            console.log($scope.filterItem);
            if (typeof $scope.filterItem === "undefined") {
                return true;
            } else if ($scope.filterItem.title != "" && $scope.filterItem.title != "Skills") {
                var exist = false;
                angular.forEach(data.skills, function (value, key) {
                    if (value.title === $scope.filterItem.title) {
//                        console.log('exist');
                        exist = true;
//                      return true ;  
                    }
                });
                if (exist === true)
                    return true;
            } else if ($scope.filterItem.title == "Skills") {
                return true;
            } else {
                return false;
            }
        }
        
        $scope.filterDate = function(data) {
//            console.log('scope date' +$scope.date);
            var last_login = data.last_login.split(' ')[0];
//            console.log(last_login);
            if(last_login <= $scope.date) {
//                console.log('exist');
                return true;
            }
            else if (typeof $scope.date === "undefined") {
                return true ;
            }
            else {
                return false ;
            }
        }
        
        
        $scope.filterStatusFunction = function(data) {
//            console.log(data);
//            console.log('filter status '+$scope.filterStatus);
//            return false ;
            var userStatus = data.status;
            var selectedStatus = $scope.filterStatus ;
            // console.log(selectedStatus);
            if (typeof selectedStatus === "undefined") {
                return true;
            } 
            else if(selectedStatus == "0") {
                return true ;
            }
            else {
                var internalStatus = false ;
                angular.forEach(userStatus , function(value,key) {
//                    alert($scope.filterStatus.split('.')[0]);
//                    if($scope.filterStatus.split('.')[0] == "-1") return true ;
                    if(value.status == $scope.filterStatus) {
                        internalStatus = true;  
                    }
                })
                if(internalStatus)
                        return true ;
                
            }
        }


    $scope.filterSkillFunction = function(data) {
//            console.log(data);
//            console.log('filter status '+$scope.filterStatus);
//            return false ;
        var userSkills = data.skills;
        var selectedSkill = $scope.filterSkills ;
        // console.log(selectedSkill)
        // console.log(selectedStatus);
        if (typeof selectedSkill === "undefined") {
            return true;
        }
        else if(selectedSkill == "") {
            return true ;
        }
        else {
            var internalStatus = false ;
            angular.forEach(userSkills , function(value,key) {
//                    alert($scope.filterStatus.split('.')[0]);
//                    if($scope.filterStatus.split('.')[0] == "-1") return true ;
                if(value.id == $scope.filterSkills) {
                    internalStatus = true;
                }
            })
            if(internalStatus)
                return true ;

        }
    }
    $scope.filterSourceFunction = function(data) {
//            console.log(data);
//            console.log('filter status '+$scope.filterStatus);
//            return false ;
        var userSource = data.job_application;
        var selectedSource = $scope.filterSources ;
        // console.log("source : "+selectedSource)
        // console.log(selectedStatus);
        if (typeof selectedSource == "undefined") {
            return true;
        }
        else if(selectedSource == 0) {
            return true ;
        }
        else {
            console.log("source : "+selectedSource)
            var internalStatus = false ;
                // if(userSource == selectedSource) {
                //     internalStatus = true;
                // }

            angular.forEach(userSource, function(value , key) {
                if (value.source == selectedSource)
                {
                    internalStatus = true
                };
            });

            if(internalStatus)
                return true ;

        }
    }
        
        $scope.filterJobsFunction = function(data) {

            var job_applications = data.job_application;
            if (typeof $scope.filterJobs === "undefined") {
                return true;
            } else if($scope.filterJobs.id === 0 ) {
                return true ;
            } else {
                var job_status = false ;
                angular.forEach(job_applications, function(value , key) {
                    if (value.id_job == $scope.filterJobs.id) 
                    {
                        job_status = true
                    };
                });
                if(job_status) 
                    return true ;
            }
////            if(typeof $scope.filterJobs === "undefined") {
////                return true ;
////            }  else {
////                return false ;
////            }
        }
        
        $scope.filterDepartmentsFunction = function(data) {
//            console.log(data);
            var userDepartments = data.user_departments ;
//            console.log($scope.filterDepartments.id ==0);
            if (typeof $scope.filterDepartments === "undefined") {
                return true;
            } else if($scope.filterDepartments.id == 0 ) {
                return true ;
            } else {
                var job_status = false ;
                angular.forEach(userDepartments, function(value , key) {
                    if (value.id_department == $scope.filterDepartments.id) 
                    {
                        job_status = true
                    };
                });
                if(job_status) 
                    return true ;
            }
        }
        
        $scope.addNote = function() {
            var id = ($('#add-note').attr('data-hash'));
            if (!id)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            
//            $scope.status_name = "Select Jobs";
//            $scope.selectJob = function(job) {$scope.status_name = job;};
            
            $http({
                url: "/ats/admin/get-application",
                method: "POST",
                data : {email:id}
            }).success(function (data, status) {
                $scope.user_applications = data.user_applications;
                $scope.user_email = data.email;
//                console.log($scope.user_applications);

            });
            modalInstance = $modal.open({
                templateUrl: '/ats/admin/note',
                scope: $scope,
            });
            $scope.general = {
                value: 0
            };
        }
        
        $scope.status_job_name = "Select Job";
        $scope.setSelected = function (item) {
            myCvService.setJobID(item.id_job);
            $scope.status_job_name = item.title;
        }
        
        
        $scope.uploadFile = function (files) {
            var fd = new FormData();
            //Take the first selected file
            fd.append("file", files[0]);

            $http.post("/ats/admin/note-upload-file", fd, {
            withCredentials: true,
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
            });
        };
        
       
        $scope.saveNote = function() {
              
            var id = $scope.user_email;
            var note = ($('textarea[name="descriere"]').val());
//            var note = CKEDITOR.instances.editor1.getData();
            var general = $scope.general.value ;
            var selectedJob = myCvService.getJobID();
            
            if(selectedJob == "" && general == 0) {
                modalInstance.close();
                var message = '<strong> Alert : !</strong>  Please chose Note Job.';
                Flash.create('warning', message);
                return false;
            }
//            var data1 = new FormData($('input[name^="file"]'));
//            $.each($('input[name^="file"]')[0].files, function (i, file) {
//                data1.append(i, file);
//            });
//            var formData = new FormData();
//            formData.append('section', 'general');
//            formData.append('action', 'previewImg');
//            // Attach file
//            formData.append('image', $('input[type=file]')[0].files[0]); 
//            console.log(data1);
//            var formData = new FormData();
//
//            formData.append('file', $scope.file);
            
            $http({
                url: "/ats/admin/add-note",
                data: {"email": id, "note": note,"general":general,"job":selectedJob},
                method: "POST",
            }).success(function (data, status) {
                modalInstance.close();
                
            $http({
                url: "/ats/admin/get-candidate-by-email",
                method: "POST",
                data: {"email": id}
            }).success(function (data, status) {
                    $scope.note = data.note;
                    var message = '<strong> Success : !</strong>  Successfully Note Added .';
                Flash.create('success', message);
                return false;
            })

                
            });
 
        }
        
        /* check Box array*/
        $scope.lst = [];
        $scope.change = function (list, active) {
            if (active)
                $scope.lst.push(list);
            else
                $scope.lst.splice($scope.lst.indexOf(list), 1);
        };
        
        $scope.exportcvs = function() {
            var list_user = $scope.lst ;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            $http({
                method: 'POST',
                url: '/ats/admin/export-cv',
                data : {"users" : list_user},
                }).success(function(data, status, headers, config) {
                var anchor = angular.element('<a/>');
                anchor.attr({
                href: data.path,
                        target: '_blank',
                        download: data.name
                })[0].click();
        }).
                
            console.log(list_user)
        }
        
        
        $scope.atsMailing = function () {

//
            var list_user = $scope.lst ;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            $http({
                method: 'POST',
                url: '/ats/admin/mail',
                data: {"users": list_user},
            }).success(function (data, status, headers, config) {
                $scope.sender = data.sender ;
                $scope.to = data.to ;
                $scope.subject = data.subject ;
                $scope.message = data.message ;
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/mail',
                    scope: $scope,
                });
                
            });
           


        }
        
        $scope.sendatsmail = function() {

            var note = CKEDITOR.instances.editor3.getData();
            var subject = $('input[name="subject"]').val();
            var bcc = $scope.lst ;
            $http({
                method: 'POST',
                url: '/ats/admin/send-mail',
                data: {"bcc": bcc,"message" : note ,"subject":subject },
            }).success(function (data, status, headers, config) {
                modalInstance.close();
            });
        }
        
        $scope.atsHmMailing = function() {
            
            var list_user = $scope.lst;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            
                $http({
                method: 'POST',
                url: '/ats/admin/hm-mail',
                data: {"hm":1},
            }).success(function (data, status, headers, config) {
                $scope.sender = data.sender;
                $scope.to = data.to;
                $scope.message = data.message;
                $scope.subject = data.subject;
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/hm-mail',
                    scope: $scope,
                });

            });
        }
        
        $scope.sendatshmmail = function () {
            var note = CKEDITOR.instances.editor3.getData();
            var subject = $("input[name='subject']").val();
            var list_user = $scope.lst;
            $http({
                method: 'POST',
                url: '/ats/admin/send-hm-mail',
                data: {"message": note,subject:subject,list_candidate:list_user},
            }).success(function (data, status, headers, config) {
                modalInstance.close();
            });
        }
        
        $scope.importBulkCv = function() {
            modalInstance = $modal.open({
                templateUrl: '/ats/admin/import-cv',
                scope: $scope,
            });
            
        }
        
        $scope.closebulk = function() {
            modalInstance.close();
            $http({
                method: 'POST',
                url: '/ats/admin/remove-bulk-session',
                data: {"remove":true},
                })
        }
        
        $scope.uploadBulkCv = function() {
            modalInstance.close();
            $http({
                method: 'GET',
                url: '/ats/admin/get-cvs',
            }).success(function (data, status, headers, config){
            $http({
                method: 'GET',
                url: '/ats/admin/get-jobs-cv',
            }).success(function (data, status, headers, config){
                $scope.up_cv_jobs = data.jobs;
            });
                if(data.cvs) {
                    $scope.bulkApplyJobSelected = 0;
                    $scope.cvs = data.cvs;
                    modalInstance = $modal.open({
                        templateUrl: '/ats/admin/upload-cvs',
                        scope: $scope,
                    });
                }
                
            });
           
            
        }
//        $("#zoom_mw").elevateZoom({scrollZoom : true});
        $scope.previewCv = function(cv) {
            $scope.view_cv = cv ;
             $("#zoom_mw").elevateZoom({scrollZoom : true});
             $scope.script = $sce.trustAsHtml('<script type="text/javascript">jQuery(document).ready(function() {$("#zoom_mw").elevateZoom();})</script>');
             
             
           
        }
        
        
        $scope.job_cv_name = "Select Jobs";
        $scope.populate_job_cv_name = function (status, value)
        {
            if ($scope.job_cv_name == "")
            {
                $scope.job_cv_name = "Select Jobs";
                $scope.job_cv_name = "{}";
            }
            var app_status;
            var app_status_reason;
            var id_job = value ;
            myCvService.setJobID(value);
            $scope.job_cv_name = status;
            $scope.status_value = value;



        };
        
        $scope.saveBulkCV = function() {
            modalInstance.close();
            var resultArray = [];
            $("table#BulkCvTable tbody tr").each(function (i, v) {
                var innerArray = [];
                $(this).children('td').each(function (ii, vv) {
                    var obj = {};
                    var name = $(this).find('input').data('name');
                    obj[name] = $(this).find('input').val();
//                    console.log(name)
                    innerArray.push(obj);
                });
                resultArray.push(innerArray);
            })
            
            
            $http({
                method: 'POST',
                url: '/ats/admin/save-bulk-cv',
                data : {cv:resultArray,id_job:myCvService.getJobID()}
            }).success(function (data, status, headers, config) {
                // msg
                var message = '<strong> Alert : !</strong>  Candidates added with success.';
                Flash.create('success', message);
                return false;

            });
        }
        
        $scope.bulkApplyJob = function(data) {
            alert(data);
            alert('$scope.selectedLocal: ' + $scope.bulkApplyJobSelected); // returns undefined
        }
        
        
        $scope.showHistoryEmail  = function(data) {
            $http({
                method: 'POST',
                url: '/ats/admin/history',
                data: {data:data}
            }).success(function (data, status) {
                // mail history data
                $scope.email_h = data.email ;
                $scope.content = $sce.trustAsHtml(data.email.content);

                    modalInstance = $modal.open({
                    templateUrl: '/ats/admin/history-email',
                    scope: $scope,
                });
       
                
            });
        }
        
        
    }]);

myApp.controller("jobs_details", ['$scope', '$routeParams', '$location', 'myCvService', '$http', '$document', '$timeout', '$modal', 'Flash', '$attrs', function ($scope, $routeParams, $location, myCvService, $http, $document, $timeout, $modal, Flash, $attrs) {

        var modalInstance;
        var ats_viewed_height = angular.element('.ats-viewed').height();

        var candidates = [];

        $scope.init = function () {
            var id_job = parseInt(window.location.search.split('=')[1]);
            $http({
                url: "/ats/admin/user-application",
                method: "POST",
                data : {"id_job" : id_job}
            }).success(function (data, status) {

                $scope.candidates = data.candidates;
                if (data.me.type == 5) {
                    
//                    $('.div-ats-change-status').remove();
                }
                if ($scope.candidates) {
                    var $first = data.candidates.slice(0,1);
                        if($first.length > 0) {
                          var $first_id = $first[0]['id'];
                    $scope.status_history = [];
                    $http({
                        url: "/ats/admin/get-candidate-job-info",
                        method: "POST",
                        data: {"id": $first_id,"id_job":id_job}
                    }).success(function (data, status) {
//                console.log(data.all_status);
                        $scope.application_removed_status = data.application_removed_status;
                        $scope.candidate = data.candidate;
                        $scope.language = data.language;
                        console.log($scope.language)
                        $scope.applications = data.applications;
                        $scope.application_job = data.application_job;
                        if($scope.applications.length == 0) $scope.applications_no_data = "No data yet";
                        if (data.status_history.length > 0) $scope.status_history = data.status_history;
                        $scope.positions = data.position;
                        if($scope.positions.length == 0) $scope.positions_no_data = "No data yet";
                        $scope.skills = data.skills ;
                        if($scope.skills.length == 0) $scope.skills_no_data = "No data yet"; 
                        $scope.educations = data.educations;
                        if($scope.educations.length == 0) $scope.educations_no_data = "No data yet";
                        $scope.emails = data.emails;
                        $scope.email_history = data.email_history;
                        if($scope.emails.length == 0) $scope.emails_no_data = "No data yet";
                        // if($scope.language.length == 0) $scope.language_no_data = "No data yet";
                        $scope.all_status = data.all_status;
                        $scope.note = data.note;
                        $scope.status = data.last_status ;


                        $timeout(function () {

                            var height = angular.element('#header_').height() - ats_viewed_height;
                            $scope.height = height;

                            var ats_candidadate_tbody = angular.element('.ats-candidadate-tbody').height();
                            $scope.tbody_height = height - (ats_candidadate_tbody + ats_viewed_height);
                            $scope.note_add_height = $scope.height - 60;
                            console.log('height : ' + $scope.height);
                            var app_table_height = angular.element('.app-table-height').height();
                            var app_buton_action = angular.element('.app-buton-action').height();
                            var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action);
                            $scope.tbody_height = "height:" + tbody_height + 'px';

                        });

                    });   
                        }
                    
                    
                    
                    
                    $http({
                        url: "/ats/admin/get-jobs",
                        method: "GET",
                    }).success(function (data, status) {
                        var jobs = data.jobs
                        $scope.jobs = jobs;
                        $scope.filterJobs = $scope.jobs[0];
                    });
                    $http({
                        url: "/ats/admin/get-departments",
                        method: "GET",
                    }).success(function (data, status) {
                        var departments = data.departments
                        $scope.departments = departments;
                        $scope.filterDepartments = departments[0];
                    });
                    $http({
                        url: "/ats/admin/get-skills",
                        method: "GET",
                    }).success(function (data, status) {
                        if (data.skills) {
                            var skilluri = data.skills
//                skilluri.unshift({'title': 'Skills'});
                            $scope.skilluri = skilluri;
                            $scope.filterItem = $scope.skilluri[0];
                        }

                    });
                    $http({
                        url: "/ats/admin/get-status",
                        method: "GET",
                    }).success(function (data, status) {
                        if (data) {
                            var st = data.status
//                    console.log(st);
//                    var $statusIndex = Object.keys(st).sort(function (a, b) {
//                        return b - a
//                    })[0];
                            $scope.statusIndex = data.default;
                            $scope.st = st;
                            $scope.filterStatus = data.default;
                        }
                    });
                }

            });



        }

        window.onload = function () {
            $('.ats-note').css({'height': angular.element('#header_').height() - ats_viewed_height + 'px'});
            var ats_mycsv_table = angular.element('.ats-mycsv-table').height();
            if (ats_mycsv_table < angular.element('#header_').height()) {
                var table_height = angular.element('#header_').height() - ats_mycsv_table;
//                $('.row-with-n-pad').css({'height':table_height+'px'});
            }

            var app_table_height = angular.element('.app-table-height').height();
            var app_buton_action = angular.element('.app-buton-action').height();
            var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action);
            $scope.tbody_height = "height:" + tbody_height + 'px';
//            console.log('header_ '+angular.element('#header_').height());
//            console.log('app_table_height '+app_table_height);
//            console.log('app_buton_action '+app_buton_action);

            $scope.show_add_note = 1;

        };


        $scope.getCandidateInfo = function (id) {
            
            var id_job = parseInt(window.location.search.split('=')[1]);
            $scope.status_history = [];
            $http({
                url: "/ats/admin/get-candidate-job-info",
                method: "POST",
                data: {"id": id,"status":true,"id_job":id_job}
            }).success(function (data, status) {
//                console.log(data.all_status);
                $scope.application_removed_status = data.application_removed_status;
                $scope.candidate = data.candidate;
                $scope.language= data.language;
                $scope.applications = data.applications;
                if (data.status_history.length > 0)
                    $scope.status_history = data.status_history;
                $scope.application_job = data.application_job;
                $scope.positions = data.position;
                $scope.skills = data.skills;
                $scope.educations = data.educations;
                $scope.all_status = data.all_status;
                $scope.note = data.note;
                $scope.email_history = data.email_history;
                if($scope.applications.length <= 0) $scope.applications_no_data = "";
                if($scope.emails.length <= 0) $scope.emails_no_data = "";
                if($scope.skills.length <= 0) $scope.skills_no_data = "";
                if($scope.educations.length <= 0) $scope.educations_no_data = "";
                if($scope.positions.length <= 0) $scope.positions_no_data = "";
                if($scope.language.length <= 0) $scope.language_no_data = "";


                $scope.status = data.last_status ;
                $timeout(function () {
                    $('.ats-note').css({'height':angular.element('#header_').height() - ats_viewed_height+'px'});
                    var height = angular.element('#header_').height() - ats_viewed_height;
                    $scope.height = height;

                    var ats_candidadate_tbody = angular.element('.ats-candidadate-tbody').height();
                    $scope.tbody_height = height - (ats_candidadate_tbody + ats_viewed_height);
                    $scope.note_add_height = $scope.height - 60;
                    console.log('height : ' + $scope.height);
                    var app_table_height = angular.element('.app-table-height').height();
                    var app_buton_action = angular.element('.app-buton-action').height();
                    var tbody_height = angular.element('#header_').height() - (app_table_height + app_buton_action);
                    $scope.tbody_height = "height:" + tbody_height + 'px';

                });
                
                

            });
        }

        $scope.getJobaStatus = function (application_id) {

            $http({
                url: "/ats/admin/get-application-status",
                method: "POST",
                data: {"id": application_id}
            }).success(function (data, status) {
                $scope.status = data.status;
//                        console.log(data);
            });
        };



        $scope.changeApplicationStatus = function () {


            var id = ($('.ats-change-status').attr('data-hash'));
            var id_application = ($('.ats-change-status').attr('unique-app-id'));
            var id_job = ($('.ats-change-status').attr('id-job'));
            $http({
                url: "/ats/admin/get-app-info",
                method: "POST",
                data: {"id_application": id_application,'id_job':id_job,'email':id}
            }).success(function (data, status) {
                $scope.application_job = {'title': data.title,'job_application_id':data.id_application,'id':data.id_job};
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/change-application-job-status',
                    scope: $scope,
                });

//                        console.log(data);
            });
//            console.log(id);

//            if (!id)
//            {
//                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
//                Flash.create('warning', message);
//                return false;
//            }
//            modalInstance = $modal.open({
//                templateUrl: '/ats/admin/change-application-job-status',
//                scope: $scope,
//            });

//            modalInstance.$promise.then(modalInstance.show);
//         
        }

        $scope.close = function () {
//               console.log('close function');
//               monModel.hide();
            modalInstance.close();
//                $scope.test.close();
        }
//         $scope.test = $modal.open({
//            templateUrl: '/ats/admin/change-application-status',
//            scope: $scope,
//        });
//        
        $scope.job_name = "Select Job";
        $scope.populate_job_name = function (name, value, application_id)
        {
//            console.log(value);
            if ($scope.job_name == "")
                $scope.job_name = "Select Job";
            $scope.job_name = name;
            myCvService.setJobID(value);
            myCvService.setJobApplicationID(application_id);
        };


        $scope.status_name = "Change Status";
        $scope.populate_status = function (status, value)
        {
            if ($scope.status_name == "")
            {
                $scope.status_name = "Change Status";
                $scope.status_value = "";
            }
            var app_status;
            var app_status_reason;
//            for (var i in value) {
               myCvService.setStatus(status);
                myCvService.setStatusReason(status);
//            }

            $scope.status_name = value;
            $scope.status_value = status;



        };

        $scope.saveStatus = function () {

            var selected_status = ($('#status-selected').attr('data-status-selected'));
//            console.log('status : '+myCvService.getStatus());
//            console.log('status_reason : '+myCvService.getStatusReason());
//            console.log('jobId : '+myCvService.getJobID());
//            console.log('jobApplicationId : '+myCvService.getJobApplicationID());
//            if (myCvService.getJobApplicationID() == 0) {
//                modalInstance.close()
//                var message = '<strong> Change Status : !</strong>  Please select a job application .';
//                Flash.create('warning', message);
//                return false;
//            }
            var id_application = $("button#job_status_change").attr('data-applicationID');
            var id_job = $("button#job_status_change").attr('data-jobID');
            $http({
                url: "/ats/admin/set-candidate-status",
                method: "POST",
                data: {"status": myCvService.getStatus(),
                    'status_reason': myCvService.getStatusReason(),
                    'id_job': id_job,
                    'id_application': id_application}
            }).success(function(data, status) {
                $scope.applications = data.applications;
                if (data.status_history.length > 0) $scope.status_history = data.status_history;
                $scope.status= data.applications[0].application_status;
                var target = data.data_target;
                var status_class = "";
                console.log(myCvService.getStatus());
                console.log(myCvService.getStatusReason());
                if (myCvService.getStatus() == '13') status_class = "banned";
                if (myCvService.getStatus() == '5') status_class = "hired";
                console.log(status_class);
                $('tr[data-target="' + target + '"]').removeClass('banned hired');
                $('tr[data-target="' + target + '"]').addClass(status_class);
                $('tr[data-target="'+target+'"]').find('a').find('span').addClass(status_class);
            })
            modalInstance.close()
            var message = '<strong> Change Status : !</strong>  Successfully Status changed.';
            Flash.create('success', message);
            return false;
        }




        //////// Date Picker
        $scope.dp_class = "display:none";
        $scope.formatDate = function (date) {
            function pad(n) {
                return n < 10 ? '0' + n : n;
            }

            return date && date.getFullYear()
                    + '-' + pad(date.getMonth() + 1)
                    + '-' + pad(date.getDate());
        };

        $scope.parseDate = function (s) {
            var tokens = /^(\d{4})-(\d{2})-(\d{2})$/.exec(s);

            return tokens && new Date(tokens[1], tokens[2] - 1, tokens[3]);
        };

        $scope.changeClass = function () {
//            console.log('change class proccess');
            if ($scope.dp_class === "display:none")
                $scope.dp_class = "display:inline-block";
            else
                $scope.dp_class = "display:none";
        }

        $scope.closeDropdown = function () {
            $scope.dp_class = "display:none";
            var date = $scope.date;
//            console.log(date);
        }

        $scope.customFilter = function (data)
        {
//            console.log($scope.filterItem);
            if (typeof $scope.filterItem === "undefined") {
                return true;
            } else if ($scope.filterItem.title != "" && $scope.filterItem.title != "Skills") {
                var exist = false;
                angular.forEach(data.skills, function (value, key) {
                    if (value.title === $scope.filterItem.title) {
//                        console.log('exist');
                        exist = true;
//                      return true ;  
                    }
                });
                if (exist === true)
                    return true;
            } else if ($scope.filterItem.title == "Skills") {
                return true;
            } else {
                return false;
            }
        }

        $scope.filterDate = function (data) {
//            console.log('scope date' +$scope.date);
            var last_login = data.last_login.split(' ')[0];
//            console.log(last_login);
            if (last_login <= $scope.date) {
//                console.log('exist');
                return true;
            } else if (typeof $scope.date === "undefined") {
                return true;
            } else {
                return false;
            }
        }


        $scope.filterStatusFunction = function (data) {
//            console.log(data);
//            console.log('filter status ' + $scope.filterStatus);
//            return false ;
            var userStatus = data.status;
            var selectedStatus = $scope.filterStatus;
//            console.log(selectedStatus);
            if (typeof selectedStatus === "undefined") {
                return true;
            } else if (selectedStatus == 0) {
                return true;
            } else {
                var internalStatus = false;
                angular.forEach(userStatus, function (value, key) {
                    if (value.status == $scope.filterStatus.split('.')[0] && value.status_reason == $scope.filterStatus.split('.')[1]) {
                        internalStatus = true;
                    }
                })
                if (internalStatus)
                    return true;

            }
        }

        $scope.filterJobsFunction = function (data) {
//            console.log($scope.filterJobs.id);
            var job_applications = data.job_application;
            if (typeof $scope.filterJobs === "undefined") {
                return true;
            } else if ($scope.filterJobs.id == 0) {
                return true;
            } else {
                var job_status = false;
                angular.forEach(job_applications, function (value, key) {
                    if (value.id_job == $scope.filterJobs)
                    {
                        job_status = true
                    }
                    ;
                });
                if (job_status)
                    return true;
            }
////            if(typeof $scope.filterJobs === "undefined") {
////                return true ;
////            }  else {
////                return false ;
////            }
        }

        $scope.filterDepartmentsFunction = function (data) {
//            console.log(data);
            var userDepartments = data.user_departments;
//            console.log($scope.filterDepartments.id ==0);
            if (typeof $scope.filterDepartments === "undefined") {
                return true;
            } else if ($scope.filterDepartments.id == 0) {
                return true;
            } else {
                var job_status = false;
                angular.forEach(userDepartments, function (value, key) {
                    if (value.id_department == $scope.filterDepartments.id)
                    {
                        job_status = true
                    }
                    ;
                });
                if (job_status)
                    return true;
            }
        }

        $scope.addNote = function () {
            var id = ($('#add-note').attr('data-hash'));
            if (!id)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }

//            $scope.status_name = "Select Jobs";
//            $scope.selectJob = function(job) {$scope.status_name = job;};

            $http({
                url: "/ats/admin/get-application",
                method: "POST",
                data: {email: id}
            }).success(function (data, status) {
                $scope.user_applications = data.user_applications;
                $scope.user_email = data.email;
//                console.log($scope.user_applications);

            });
            modalInstance = $modal.open({
                templateUrl: '/ats/admin/note',
                scope: $scope,
            });
            $scope.general = {
                value: 0
            };
        }
        $scope.status_job_name = "Select Job";
        $scope.setSelected = function (item) {
            myCvService.setJobID(item.id_job);
            $scope.status_job_name = item.title;
        }
        
        $scope.uploadFile = function (files) {
            var fd = new FormData();
            //Take the first selected file
            fd.append("file", files[0]);

            $http.post("/ats/admin/note-upload-file", fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            });
        };


        $scope.saveNote = function () {

            var id = $scope.user_email
            var note = ($('textarea[name="descriere"]').val());
//            var note = CKEDITOR.instances.editor1.getData();
            var general = $scope.general.value;
            var selectedJob = myCvService.getJobID();

            if (selectedJob == "" && general == 0) {
                modalInstance.close();
                var message = '<strong> Alert : !</strong>  Please chose Note Job.';
                Flash.create('warning', message);
                return false;
            }
//            console.log(general);
//            var selectedJob = 123;
            $http({
                url: "/ats/admin/add-note",
                data: {"email": id, "note": note, "general": general, "job": selectedJob},
                method: "POST"
            }).success(function (data, status) {
                modalInstance.close();

                $http({
                    url: "/ats/admin/get-candidate-by-email",
                    method: "POST",
                    data: {"email": id}
                }).success(function (data, status) {
                    $scope.note = data.note;
                    var message = '<strong> Success : !</strong>  Successfully Note Added .';
                    Flash.create('success', message);
                    return false;
                })


            });

        }

        /* check Box array*/
        $scope.lst = [];
        $scope.change = function (list, active) {
            if (active)
                $scope.lst.push(list);
            else
                $scope.lst.splice($scope.lst.indexOf(list), 1);
        };

        $scope.exportcvs = function () {
            var list_user = $scope.lst;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            $http({
                method: 'POST',
                url: '/ats/admin/export-cv',
                data: {"users": list_user},
            }).success(function (data, status, headers, config) {
                var anchor = angular.element('<a/>');
                anchor.attr({
                    href: data.path,
                    target: '_blank',
                    download: data.name
                })[0].click();
            }).
                    console.log(list_user)
        }


        $scope.atsMailing = function () {

//
            var list_user = $scope.lst;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }
            $http({
                method: 'POST',
                url: '/ats/admin/mail',
                data: {"users": list_user,"job-details" : parseInt(window.location.search.split('=')[1])},
            }).success(function (data, status, headers, config) {
                $scope.sender = data.sender;
                $scope.to = data.to;
                $scope.subject = data.subject;
                $scope.message = data.message;
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/mail',
                    scope: $scope,
                });

            });



        }

        $scope.sendatsmail = function () {

            var note = CKEDITOR.instances.editor3.getData();
            var subject = $('input[name="subject"]').val();
            var bcc = $scope.lst;
            $http({
                method: 'POST',
                url: '/ats/admin/send-mail',
                data: {"bcc": bcc, "message": note, "subject": subject},
            }).success(function (data, status, headers, config) {
                modalInstance.close();
            });
        }

        $scope.atsHmMailing = function () {

            var list_user = $scope.lst;
            if (list_user.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }

            $http({
                method: 'POST',
                url: '/ats/admin/hm-mail',
                data: {"hm": 1,"job-details":parseInt(window.location.search.split('=')[1])},
            }).success(function (data, status, headers, config) {
                $scope.sender = data.sender;
                $scope.to = data.to;
                $scope.message = data.message;
                $scope.subject = data.subject;
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/hm-mail',
                    scope: $scope,
                });

            });
        }

        $scope.sendatshmmail = function () {
            var note = CKEDITOR.instances.editor3.getData();
            var subject = $("input[name='subject']").val();
            var list_user = $scope.lst;
            $http({
                method: 'POST',
                url: '/ats/admin/send-hm-mail',
                data: {"message": note, subject: subject, list_candidate: list_user},
            }).success(function (data, status, headers, config) {
                modalInstance.close();
            });
        }
        
        $scope.applicantsCahngeStatus = function () {
            var list_applicants = $scope.lst;
            if (list_applicants.length < 1)
            {
                var message = '<strong> Alert : !</strong>  Please chose a candidate first .';
                Flash.create('warning', message);
                return false;
            }

            $http({
                method: 'GET',
                url: '/ats/admin/applicant-change',
            }).success(function (data, status, headers, config) {
                $scope.job_details_status = data.all_status;
                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/applicant-change-status',
                    scope: $scope,
                });
            });

        }
        
        $scope.changeBulkStatus = function () {
            var id_job = parseInt(window.location.search.split('=')[1]);
            var list_applicants = $scope.lst;

            $http({
                method: 'POST',
                url: '/ats/admin/set-bulk-status',
                data: {
                    "status": myCvService.getStatus(),
                    'status_reason': myCvService.getStatusReason(),
                    'id_job': id_job,
                    'applicants': list_applicants}
            }).success(function (data, status, headers, config) {
                modalInstance.close();
            });

        } 
        
        $scope.showHistoryEmail = function (data) {
            $http({
                method: 'POST',
                url: '/ats/admin/history',
                data: {data: data}
            }).success(function (data, status) {
                // mail history data
                $scope.email_h = data.email;

                modalInstance = $modal.open({
                    templateUrl: '/ats/admin/history-email',
                    scope: $scope,
                });


            });
        }
    }]);