/**
 * Created by westilian on 1/19/14.
 */

(function(){
    var t;
    function size(animate){
        if (animate == undefined){
            animate = false;
        }
        clearTimeout(t);
        t = setTimeout(function(){
            $("canvas").each(function(i,el){
                $(el).attr({
                    "width":$(el).parent().width(),
                    "height":$(el).parent().outerHeight()
                });
            });
            redraw(animate);
            var m = 0;
            $(".chartJS").height("");
            $(".chartJS").each(function(i,el){ m = Math.max(m,$(el).height()); });
            $(".chartJS").height(m);
        }, 30);
    }
    $(window).on('resize', function(){ size(false); });


    function redraw(animation){
        var options = {};
        if (!animation){
            options.animation = false;
        } else {
            options.animation = true;
        }


        var barChartData = {
            labels : ["January","February","March","April","May","June","July"],
            datasets : [
                {
                    fillColor : "#E67A77",
                    strokeColor : "#E67A77",
                    data : [65,59,90,81,56,55,40]
                },
                {
                    fillColor : "#79D1CF",
                    strokeColor : "#79D1CF",
                    data : [28,48,40,19,96,27,100]
                }
            ]

        }

        //var myLine = new Chart(document.getElementById("bar-chart-js").getContext("2d")).Bar(barChartData);


        if (typeof (line_chart_labels) != "undefined")
        {
            var Linedata = {
                labels : line_chart_labels,
                datasets : line_chart_datasets
            }
                                    
            var myLineChart = new Chart(document.getElementById("line-chart-js").getContext("2d")).Line(Linedata,{legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;&nbsp;</span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"});
            var legend = myLineChart.generateLegend();
            
            $('legend').html(legend);    
        }
        


        var pieData = [
            {
                value: 30,
                color:"#E67A77"
            },
            {
                value : 50,
                color : "#D9DD81"
            },
            {
                value : 100,
                color : "#79D1CF"
            }

        ];
        
        if (typeof (pie_chart_labels) != "undefined")
        {
            var pieData = {
                labels : pie_chart_labels,
                datasets : pie_chart_datasets
            }
                                    
            var myPie = new Chart(document.getElementById("pie-chart-js").getContext("2d")).Pie(pieData);
        }
        
        



    }




    size(true);

}());
