(function ($) {
    "use strict";
    $(document).ready(function () {
        /*==Left Navigation Accordion ==*/
        if ($.fn.dcAccordion) {
            $('#nav-accordion').dcAccordion({
                eventType: 'click',
                autoClose: true,
                saveState: true,
                disableLink: true,
                speed: 'slow',
                showCount: false,
                autoExpand: true,
                classExpand: 'dcjq-current-parent'
            });
        }
        /*==Slim Scroll ==*/
        if ($.fn.slimScroll) {
            $('.event-list').slimscroll({
                height: '305px',
                wheelStep: 20
            });
            $('.conversation-list').slimscroll({
                height: '360px',
                wheelStep: 35
            });
            $('.to-do-list').slimscroll({
                height: '300px',
                wheelStep: 35
            });
        }
        /*==Nice Scroll ==*/
        if ($.fn.niceScroll) {


            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $(".leftside-navigation").getNiceScroll().resize();
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();

            $(".right-stat-bar").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

        }

        /*==Easy Pie chart ==*/
        if ($.fn.easyPieChart) {

            $('.notification-pie-chart').easyPieChart({
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                },
                barColor: "#39b6ac",
                lineWidth: 3,
                size: 50,
                trackColor: "#efefef",
                scaleColor: "#cccccc"

            });

            $('.pc-epie-chart').easyPieChart({
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                },
                barColor: "#5bc6f0",
                lineWidth: 3,
                size:50,
                trackColor: "#32323a",
                scaleColor:"#cccccc"

            });

        }

        /*== SPARKLINE==*/
        if ($.fn.sparkline) {

            $(".d-pending").sparkline([3, 1], {
                type: 'pie',
                width: '40',
                height: '40',
                sliceColors: ['#e1e1e1', '#8175c9']
            });



            var sparkLine = function () {
                $(".sparkline").each(function () {
                    var $data = $(this).data();
                    ($data.type == 'pie') && $data.sliceColors && ($data.sliceColors = eval($data.sliceColors));
                    ($data.type == 'bar') && $data.stackedBarColor && ($data.stackedBarColor = eval($data.stackedBarColor));

                    $data.valueSpots = {
                        '0:': $data.spotColor
                    };
                    $(this).sparkline($data.data || "html", $data);


                    if ($(this).data("compositeData")) {
                        $spdata.composite = true;
                        $spdata.minSpotColor = false;
                        $spdata.maxSpotColor = false;
                        $spdata.valueSpots = {
                            '0:': $spdata.spotColor
                        };
                        $(this).sparkline($(this).data("compositeData"), $spdata);
                    };
                });
            };

            var sparkResize;
            $(window).resize(function (e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(function () {
                    sparkLine(true)
                }, 500);
            });
            sparkLine(false);



        }



        if ($.fn.plot) {
            var datatPie = [30, 50];
            // DONUT
            $.plot($(".target-sell"), datatPie, {
                series: {
                    pie: {
                        innerRadius: 0.6,
                        show: true,
                        label: {
                            show: false

                        },
                        stroke: {
                            width: .01,
                            color: '#fff'

                        }
                    }




                },

                legend: {
                    show: true
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },

                colors: ["#ff6d60", "#cbcdd9"]
            });
        }



        /*==Collapsible==*/
        $('.widget-head').click(function (e) {
            var widgetElem = $(this).children('.widget-collapse').children('i');

            $(this)
                .next('.widget-container')
                .slideToggle('slow');
            if ($(widgetElem).hasClass('ico-minus')) {
                $(widgetElem).removeClass('ico-minus');
                $(widgetElem).addClass('ico-plus');
            } else {
                $(widgetElem).removeClass('ico-plus');
                $(widgetElem).addClass('ico-minus');
            }
            e.preventDefault();
        });




        /*==Sidebar Toggle==*/

        $(".leftside-navigation .sub-menu > a").click(function () {
            var o = ($(this).offset());
            var diff = 80 - o.top;
            if (diff > 0)
                $(".leftside-navigation").scrollTo("-=" + Math.abs(diff), 500);
            else
                $(".leftside-navigation").scrollTo("+=" + Math.abs(diff), 500);
        });



        $('.sidebar-toggle-box .fa-bars').click(function (e) {
           $(this).addClass('abc');

            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });

            $('#sidebar').toggleClass('hide-left-bar');
            $('.brand').toggleClass('blue');
            $('header').toggleClass('merge-left');
            $('#ora-logo').toggleClass('white');
            if($('#sidebar').hasClass('hide-left-bar')) {
                $('head').append('<style>.fa-bars:before{color:#FFF !important;}</style>');
            } else {
                 $('head').append('<style>.fa-bars:before{color:#34C3D8 !important;}</style>');
            }
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();
            $('#main-content').toggleClass('merge-left');
            e.stopPropagation();
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });
        $('.toggle-right-box .fa-bars').click(function (e) {
            $('#container').toggleClass('open-right-panel');
            $('.right-sidebar').toggleClass('open-right-bar');
            $('.header').toggleClass('merge-header');

            e.stopPropagation();
        });

        $('.header,#main-content,#sidebar').click(function () {
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }

            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }


        });


        $('.panel .tools .fa').click(function () {
            var el = $(this).parents(".panel").children(".panel-body");
            if ($(this).hasClass("fa-chevron-down")) {
                $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);
            } else {
                $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200); }
        });



        $('.panel .tools .fa-times').click(function () {
            $(this).parents(".panel").parent().remove();
        });

        // tool tips

        $('.tooltips').tooltip();

        // popovers

        $('.popovers').popover();
        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        
        if(!isMobile.any()) {
            $("#job-mobile").parent().hide();
            $("#logout-mobile").parent().hide();
        }


    });    
    $(document).scroll(function(e)
    {
        if (in_scroll(".jobs_list div:last")) 
        {
            $(document).unbind('scroll');
            $.ajax({
                type: "POST",
                url: '/load-more',
                data: 
                { 
                    limit:  jobs_limit, 
                    filters: jobs_params,
                    existing: $('.jobs_list > div').length,
                    json: "true" 
                }
            })
            .done(function( msg ) 
            {
                $(".jobs_list").append(msg);                
                if (msg == '') 
                {
                    $(document).scroll(function(e){});
                }
            });
        };
});

})(jQuery);

function in_scroll(element)
{
    if (typeof jobs_limit != "undefined")
    {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
     
        var elemTop = $(element).offset().top;
        var elemBottom = elemTop + $(element).height();
     
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));    
    }
    
}

$("a.delete_item").click(function()
{
    return confirm(trans_key['confirm_delete_user']+"?");
});
$("a.delete_item_job").click(function()
{
    return confirm(trans_key['confirm_delete_job']+"?");
});
$("a.duplicate_item").click(function()
{
    return confirm(trans_key['confirm_duplicate']+"?");
});

$("a.activate_item").click(function()
{
    return confirm(trans_key['confirm_activate_item']+"?");
});

$(document).ready(function()
{
    $("select.change-city").change(function()
    {
       var id_country = $(this).val();
       
       $("select.changed-city").html("");
       $.post("/ajax/index/get-cities", {"id_country":id_country}, function(data)
       {
            var html = "";
            $.each(data.cities, function(key, value)
            {
               html += "<option value='"+key+"'>"+value+"</option>"; 
            });
            $("select.changed-city").html(html);
       }, 'json'); 
    });
    
    $("select.custom_cities").select2({
        width : "auto",
        dropdownAutoWidth : true,
        // formatNoMatches: function () {
        //             return "No Results Found <a onClick='customDrop("+'cities'+")' href='javascript:;' class='btn btn-danger add-br pull-right'>Add New</a>";
        // },
    });
    $("select.custom_skills").select2({
        width : "auto",
        dropdownAutoWidth : true,
        // formatNoMatches: function () {
        //             return "No Results Found <a onClick='customDrop("+'skills'+")' href='javascript:;' class='btn btn-danger add-br pull-right'>Add New</a>";
        // },
    });
    $("select.custom_departments").select2({
        width : "auto",
        dropdownAutoWidth : true,
        // formatNoMatches: function () {
        //             return "No Results Found <a onClick='customDrop("+'departments'+")' href='javascript:;' class='btn btn-danger add-br pull-right'>Add New</a>";
        // },
    });
    $("select.select2").select2({
        width : "auto",
        dropdownAutoWidth : true,
    });
})

$('body').on('hide.bs.modal', function () 
{
    if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') == false) 
    {
        $('html').addClass('modal-open');
    } 
    else if ($('.modal:visible').size() <= 1) 
    {
        $('html').removeClass('modal-open');
        $(this).removeData('bs.modal');
    }
});
    
$('body').on('show.bs.modal', '.modal', function () 
{
    if ($(this).hasClass("modal-scroll")) 
    {
        $('body').addClass("modal-open-noscroll");
    }                
});

$('body').on('hidden.bs.modal', '.modal', function () 
{
    //alert("here");
    $('#preview-modal').html("");
    $('body').removeClass("modal-open-noscroll");    
});               

$("body").on('click', '[data-toggle="modal"]', function(e) 
{
    e.preventDefault();
    var target = $(this).attr("href");
    $("#preview-modal").load(target, function()
    {
        $("#preview-modal").modal("show");
    });
});

$("body").on('submit', '.ajaxify', function(e) 
{
    e.preventDefault();
    var form = $(this).serializeArray();
    var action = $(this).attr("action");
    if (form.length == 2)
    {
        $.post(action, form, function(data)
        {
            if (data.msg == 'ok')
            {
                $('[data-dismiss="modal"]').click();
                var html = '<div class="row">';
                html += '<div class="col-md-12"><h3>'+data.error+'<h3>';
                
//                $("div.modal-body").html(html);
                
            }
            else
            {
                alert(data.error);
            }
        },'json');
    }
    else if (form.length == 5)
    {
        $.post(action, form, function(data)
        {
            if (data.msg == 'ok')
            {
                var html = '<div class="row">';
                html += '<div class="col-md-12"><h3>We registered your application with success and created your account. Click <a href="#" onclick="window.location.reload()">here</a> to continue<h3>';
                
                $("div.modal-body").html(html);
            }
            else
            {
                alert(data.error);
            }
        },'json');
    }
    else if ($(this).hasClass('contactForm'))
    {
        $.post(action, form, function(data)
        {
            if (data.msg == 'ok')
            {
                $(".contactForm").html(data.text);
            }
            else
            {
                alert(data.error);
            }
        },'json');
    }
    else
    {
        $.post(action, form, function(data)
        {
            if (data.msg == 'ok')
            {
                $(".register-form").addClass("hidden");
                $(".register-user-form").removeClass("hidden");
            }
            else
            {
                alert(data.error);
            }
        },'json');
    }
    return false;
});

$("body").on('submit', '.settings', function(e) 
{
    e.preventDefault();
    var form = $(this).serializeArray();
    var action = $(this).attr("action");
    
    $.post(action, form, function(data)
    {
        if (data.msg == 'ok')
        {
            $("div.alert").removeClass("hidden");
        }
        else
        {
            alert(data.error);
        }
    },'json');
    return false;
});



$("body").on('submit', '.ajaxformprofile', function(e) 
{
    e.preventDefault();
    var form = $(this).serializeArray();
    var action = $(this).attr("action");
    var form_type = $(this).attr("type");
    
    $.post(action, form, function(data)
    {
        if (data.msg == 'ok')
        {
            $("#preview-modal").modal("hide");
            switch (form_type)
            {
                case "alert":
                    window.location.href = "/profile?a";
                break;
                
                default:
                    window.location.reload();
                break;
            }
            
        }        
    },'json');
    return false;
});

$("body").on("click", ".close-modal", function(e)
{
    e.preventDefault();    
    $("#preview-modal").modal("hide");
    $(this).removeData('bs.modal');
    return false;
});

$("body").on('click', '.view-more-xp', function(e) 
{
    e.preventDefault();
    var rel = $(this).attr('rel');
    if ($("#"+rel+"").css('height') == '55px')
    {
        $("#"+rel+"").css("overflow", "visible").css("height", "auto");
        $(this).text("view less");    
    }
    else
    {
        $("#"+rel+"").css("overflow", "hidden").css("height", "55px");
        $(this).text("view more");
    }
    
    return false;
});

$("body").on('change', '.price_configurator #job_credits', function(e) 
{
    e.preventDefault();
    form = $(".price_configurator").serializeArray();
    $.post("/ajax/index/get-package-price", form, function(data)
    {
        $("#total_price").val(data.val);
    }, 'json');
    return false;
});

$("body").on('change', '.price_configurator #search_credits', function(e) 
{
    e.preventDefault();
    form = $(".price_configurator").serializeArray();
    $.post("/ajax/index/get-package-price", form, function(data)
    {
        $("#total_price").val(data.val);
    }, 'json');
    return false;
});

$("body").on('change', '.status_change', function(e) 
{
    e.preventDefault();
    val = $(this).val();
    var rel = $(this).attr("rel");    
    
    $(".status_reason_"+rel+"").html("");
    var html = "";
    $.post("/ajax/index/get-statuses", {"status": val}, function(data)
    {
        $.each(data, function(key, value)
        {
           html += '<option value="'+key+'">'+value+'</option>'; 
        });
        
        $(".status_reason_"+rel+"").html(html);
    }, 'json');
    return false;
});

$("body").on('change', ".filtering", function(e)
{
   e.preventDefault();
   $(this).submit(); 
});

$("body").on("click", "form button.btn-default", function(e)
{
   //$("form.form-horizontal")[0].reset();
   //window.history.back("-1"); 
});

$("body").on("click", ".rate_company", function(e)
{
   var rel = $(this).attr("rel");
   $.post("/rate-company", {"data": rel}, function(data)
   {
        alert(data.msg);
   }, 'json');
   
   return false;
});


$("body").on("click", "div.tooltip_closable a", function(e)
{
   e.preventDefault();
   $(this).parent().addClass("hidden");
   return false; 
});

$("body").on('submit', '.ajaxform', function(e) 
{
    e.preventDefault();
    var form = $(this).serializeArray();
    var action = $(this).attr("action");

    $.post(action, form, function(data)
    {
        if (data.msg == 'ok')
        {
            switch (data.mode)
            {
                case "redirect":
                    window.location.href = data.url;
                break;
            }
        }
        else
        {
            alert(data.error);
        }
    },'json');
    return false;
});

$(document).ready(function()

{
    $('#panel-camp').height($('#panel-app').height());
   $(".ajaxify").submit(function()
   {
        var form = $(this).serializeArray();
        var action = $(this).attr('action');
        
        $.post(action, form, function(data)
        {
            $("#embed_frame").val(data.code);
        }, 'json');
        
        return false;
   }); 
   
   $("#embed_frame").focus(function()
   {
        var $this = $(this);
        $this.select();
        
        $this.mouseup(function() 
        {
            $this.unbind("mouseup");
            return false;
        });
   });
   
   $(".ajaxform").submit(function()
   {
        //alert("here");
   });
});


$("body").on("click", ".ajaxformprofile #is_current", function()
{
    if ($("#end").attr("disabled") == 'disabled')
    {
        $("#end").removeAttr("disabled");     
    }
    else
    {
        $("#end").attr("disabled", "disabled");
    }
    
});

$("body").on("submit", ".recommendForm", function()
{
   var form = $(this).serializeArray();
   var action = $(this).attr("action");
   $.post(action, form, function(data)
   {
        if (data.msg == 'ok')
        {
            window.location.href = data.url;    
        }
        else
        {
            alert(data.text);
        }
        
   }, 'json'); 
   
   return false;
});

$("body").on("click", ".message_check", function()
{
   $(".send_message.space10").text(""); 
});

$("body").on("click", ".personalize", function()
{
   var rel = $(this).attr("rel");
   var field = $("#"+rel+"").html();
   var content = field + " " + $(this).html();
   
   $("#"+rel+"").html(content);
   
});

$("body").on("click", "a.candidate_link", function(e)
{
    e.preventDefault();
    if (confirm('This CV will cost you 1 search credit. Do you want to continue?'))
    {
        var target = $(this).attr("href");
        $("#preview-modal").load(target, function()
        {
            $("#preview-modal").modal("show");
        });    
    }
    
    return false;
    
});

function customDrop(source) {
        
        var source = $(source).attr("id");
        switch(source) {
            case "cities":
                var value = $('div.custom_cities').find('input.select2-input').val();
                break;
            case "skills":
                var value = $('div.custom_skills').find('input.select2-input').val();
                break;
            case "departments":
                var value = $('div.custom_departments').find('input.select2-input').val();
                break;
        }
//        if(confirm("Are you sure for adding custom city to your company ?")) {
            $.ajax({
                data : {value : value , source : source},
                method : "POST",
                url : "/ajax/index/custom-cities",
                success : function(data) {
                    console.log(data);
                    data = jQuery.parseJSON(data);
                    switch(data.source) {
                        case "cities" :
                              var option = new Option(data.title,data.id);
                              option.selected = true;
                              $("#cities").append(option);
                              $("#cities").trigger("change");
                            break;
                        case "skills" :
                              var option = new Option(data.title,data.id);
                              option.selected = true;
                              $("#skills").append(option);
                              $("#skills").trigger("change");
                            break;
                        case "departments" :
                              var option = new Option(data.title,data.id);
                              option.selected = true;
                              $("#departments").append(option);
                              $("#departments").trigger("change");
                            break;
                        
                    }
                  
                }
            })
//        }
}

