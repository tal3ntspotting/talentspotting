(function () {
    var dashboard = angular.module('dashboardAngular', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'flash']);

    dashboard.service('myCvService', function () {
        var myCvService = {
            query: 30,
            cities:[],
            state : "",
        }
        return {
            setSearchQuery: function (query) {
                myCvService.query = query;
            },
            getSearchQuery: function () {
                return myCvService.query
            },
            setCitiesQuery: function (city) {
                myCvService.cities.push(city)
            },
            getCitiesQuery : function() {
              return myCvService.cities;
            },
            setCityState:function(state) {
                myCvService.state = state;
            },
            getCityState : function() {
                return myCvService.state;
            },
            removeCity : function(index) {
                myCvService.cities.splice(index,1);

            },
        }

    });
    dashboard.controller("dashboardController", ['$scope', '$location', 'myCvService', 'Flash', '$http', '$modal',
        function ($scope, $location, myCvService, Flash, $http, $modal) {

             var days = {
                 '30' : "30 days",
                 '90' : "3 months",
                 '180': "6 months",
                 '360': "12 months"
             };
            $scope.dateLabel = days[30];

            /**
             * init Function => Constructer on load
             */
            $scope.init = function () {
                console.log('init function');
                getCitiesFromJobs();
            };

            $scope.init();

            /**
             * update Date In Service Provider
             * @param date
             * @constructor
             */

            $scope.FilterItemsByDate = function (date) {
                $scope.dateLabel = days[date];
                myCvService.setSearchQuery(date)
            };

            /**
             * update Filter City in Services Provider
             * @constructor
             */
            $scope.FilterItemsByCity = function (city) {

                myCvService.setCityState(city);
                var cities = myCvService.getCitiesQuery() ;
                var index = cities.indexOf(city);
                if( index > -1) {
                    console.log('lets remove it');
                    myCvService.removeCity(index);
                } else {
                    myCvService.setCitiesQuery(city)
                }
                // console.log(myCvService.getCitiesQuery());

            };


            /**
             * get All used Cities In Jobs
             */
            function getCitiesFromJobs() {
                $http({
                    url: "/default/admin/get-all-cities-jobs-by-company",
                    method: "GET",
                }).success(function (data, status) {
                    var cities = data.cities;
                    $scope.cities = cities;
                    // console.log($scope.cities)
                });
            }

            /**
             *  Date Watcher
             */
            $scope.$watch( function( ) {
                return myCvService.getSearchQuery();
            }, function( newValue, oldValue ) {
                var search = myCvService.getSearchQuery() ;

                // console.log(search);
                // console.log('Service query was updated!');
                populate()
            });

            /**
             *  City Watcher
             */
            $scope.$watch(function( ) {
                return myCvService.getCityState();
            }, function( newValue, oldValue ) {
                var cities = myCvService.getCitiesQuery() ;

                // console.log(cities);
                // console.log('Service city was updated!');
                populate()
            });


            function populate() {
                var date = myCvService.getSearchQuery();
                var cities = myCvService.getCitiesQuery();
                getDashboardPercent(date,cities);
                getBoxData();
                console.log('update all functions')
                console.log(date)
                console.log(cities)
            }

            function getBoxData()
            {
                $http({
                    url : "/default/admin/get-boxes",
                    method : "GET",
                }).success(function(data,status) {
                    $scope.boxes = data.boxes;

                });
            }


            function getDashboardPercent(date,cities)
            {
                $http({
                    url: "/default/admin/get-percent",
                    method: "POST",
                    data: {date: date, cities: cities},
                }).success(function (data, status) {
                    $scope.data = data;
                });
            }


        }]);
})();