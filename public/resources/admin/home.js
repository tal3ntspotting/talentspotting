jQuery.ajaxSetup({async:false});


var counter = [0, 0, 0, 0, 0, 0];
var total_jobs = new Array();


var all_jobs = [
    'fetch_job(" <?= $this->translate->_(\'fav_similar_jobs\') ?>", "/default/index/favourites-similar", 0, 1);',
    'fetch_job(" <?= $this->translate->_(\'be_first_share\') ?>", "/default/index/shared-jobs", 1, 1);',
    'fetch_job(" <?= $this->translate->_(\'promoted_jobs\') ?>", "/default/index/promoted-jobs", 2, 1);',
    'fetch_job("<?= $this->translate->_(\'Latest Jobs\') ?>", "/default/index/latest-jobs", 3, 1);',
    'fetch_job(" <?= $this->translate->_(\'permanent_jobs\') ?>", "default/index/permanent-jobs", 4, 1);',
    'fetch_job(" <?= $this->translate->_(\'temporary_p_based\') ?>", "/default/index/temporary-jobs", 5, 1);',
]


function    get_total()
{
    total_jobs = new Array();
    $.ajax({
        type: 'POST',
        contentType: "application/x-www-form-urlencoded",
        url: "/default/index/jobs-sections-total",
    }).done(function( d ) {
        for(var i=0;i<d.length;i++)
            total_jobs.push(d[i])
    })
}

function removealljobs(index)
{
    var elements = document.getElementById(index + "job").children;
    counter[index] = 0;
    get_total()
    var i = 3;
    while(elements[i])
        elements[i].remove()
    $('#' + index+"job  .ww").css("display", "block")

    $('#' + index+"job  .www").css("display", "block")

    $('#' + index+"sss  .viewall").replaceWith("<div onclick=\"fetch_all_jobs("+index+")\" class=\"viewall\" id=\"hoverShow1\"><p>"+trans_key.view_all+" <span> &gt;&gt; </span></php><div class=\"_loader\"><img src=\"\\resources\\frontend\\images\\loader.gif\"></div></div>")

}
function option1(e) {
    // $(e).toggleClass("_active");
    return 0;
};

function addToFavorite(id_job)
{
    if(user_type.logged == false)
    {
        window.location.href = "/login";
    }
    var http = new XMLHttpRequest();
    var endpoint_url = '/ajax/index/set-favorite';
    var params = 'id_job='+id_job;
    http.open('POST', endpoint_url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            // alert(http.responseText);
        }
    }
    http.send(params);
    fetch_job("Favourite & Similar Jobs", "/default/index/favourites-similar", 0);
    $("[data-idjb="+id_job+"]").toggleClass("_active");
}
var _sign = 1;

function _()
{
    root.style.setProperty('--sss', "0%")
}

function update_counter(index, sign=1) {
    _sign = 1;
    if(sign == -1)
        _sign = -1;
    counter[index] += 5 * sign;
    eval(jobs[index]);
}
var root = document.documentElement;

function fove() {
    root.style.setProperty('--sss', "-100%");

    for(var y=0;y<$(".span").length;y++)
    {
        if($($(".span")[y]).width() >$($(".card-profile")).width()*0.8 ) {

            $($(".span")[y]).html('<p class="marquee"><span>'+$($("h2 > strong")[y])[0].innerText.toString()+' </span>    </p>');
        }
    }
    _();
}



function fetch_job(title, url, offset_index, all)
{
    var skills, search, id_city, career_level;
    if($("#skills").val())
        skills =  $("#skills").val();
    if($("#search").val())
        search = $("#search").val();
    if($("#cities").val())
        id_city =  $("#cities").val();
    if($("#carreers").val())
        career_level = $("#carreers").val();


    var data = {
        filters: {
            "id_skill": skills,
            "id_city": id_city,
            "career_level": career_level,
            "search": search,
        },
        "offset": counter[offset_index]
    }

    $.ajax({
        type: 'POST',
        contentType: "application/x-www-form-urlencoded",
        url: url,
        data: data
    }).done(function( _data ) {
        data = Object.values(_data);
        id = Object.keys(_data);

        if(data.length == 0)
            return false;


        var html = ``, cardhtml= ``;
        console.log(data)

        html += '<div  class="set">'
        console.log("ZZZZZZZZZZZZZZZ")
        console.log(total_jobs[counter])
        if(!document.getElementById(offset_index+"job") && total_jobs[offset_index]>5)
            html += '<div  id="' + offset_index+  'sss" class="hoverWrapper"><h2>' + title + '</h2><div onclick="fetch_all_jobs('+offset_index+')" class="viewall" id="hoverShow1"><p >'+trans_key.view_all+'<span> >> </span></p><div class="_loader"><img src=\'\\resources\\frontend\\images\\loader.gif\'></div></div></div>';
        else
            html += '<div  id="' + offset_index+  'sss" class="hoverWrapper"><h2>' + title + '</h2></div>'

        if(!document.getElementById(offset_index+"job"))
            html += '<div id="'+offset_index+'job" class="set-job">';
        if(total_jobs[offset_index] > 5)
            html += '<div style="position: absolute;right:-30px"> <i onclick="update_counter(' + offset_index + ')" class="fas www  cls fa-arrow-circle-right"></i> </div>';
        if(total_jobs[offset_index] > 5   )
            html += ' <div style="position: absolute;left:-30px"> <i onclick="update_counter('+offset_index+', -1)" class="fas ww cls fa-arrow-circle-left"></i> </div>';
        html += "<div class='line'>"
        for(var i=0;i<data.length;i++)
        {
            cardhtml += '<div class="card-profile">'
            cardhtml += '<div style="">'
            cardhtml += '<div class="card-topp">';
            if(data[i].has_job_bounty)
            {
                cardhtml += '<div class="cao1 home-bounty"><i class="fas fa-gift"></i><p>'+trans_key.on_hire+'</p></div>';
            }
            if(data[i].in_campaign)
            {
                cardhtml += '<div class="cao home-bounty"><i class="fas fa-bookmark"></i><p>'+trans_key.on_win+'</p></div>';
            }

            cardhtml += '</div>';

            // var image = data[i].in_campaign ? data[i].campaign.bounty_file : data[i].job_public_image;
            var image = data[i].home_image

            cardhtml += '<a style="font-size: unset;margin-left: unset" href="'+data[i].job_url+'"><div style="background-image: url('+ image +'); height:10em;  background-size: cover;   background-position: center;" class="card">';
            cardhtml += '</div></a>';
            cardhtml +=  '<a style="font-size: unset;margin-left: unset" href="'+data[i].job_url+'"><h2 class="caomgiar"><strong><span class="span">'+data[i].title+'</span></strong></h2></a>';


            if("cities" in data[i])
                if(data[i].cities) {
                    var cities = data[i].cities[0].title;
                    for(var j=1;j<data[i].cities.length;j++)
                    {
                        cities += ', ' + data[i].cities[j].title;
                    }
                }
//<i class="fas fa-map-marker-alt"></i>
            //<i class="fas fa-city"></i>
            if(cities)
                cardhtml +=  '<a style="font-size: unset;margin-left: unset" href="'+data[i].job_url+'"><p class="caomgiar"><i class="fas fa-map-marker-alt"></i> <strong class="cit"><span class="span1">'+cities+'</span></strong> </p></a>';
            if("skills" in data[i])
                if(data[i].skills) {
                    if(data[i].skills[0])
                        var skills = data[i].skills[0].title;
                    for(var j=1;j<data[i].skills.length;j++)
                    {
                        skills += ', ' + data[i].skills[j].title;
                    }
                }
            if(skills)
                cardhtml +=  '<a style="font-size: unset;margin-left: unset" href="'+data[i].job_url+'"><p class="caomgiar"><i class="fas fa-boxes"></i> <strong class="ski"><span class="span2">'+skills+'</span></strong> </p></a>';

            cardhtml += '  <div class="idk">';
//<i class="fas fa-boxes"></i>

            var apply_style = "";
            if(data[i].applied) {
                apply_style += "applied"
            }
            if(user_type.logged)
            {
                cardhtml += '<a data-target="#preview-modal" data-toggle="modal" href="' + data[i].apply +  '"><i class="far fa-edit '+apply_style+'"></i></a>';

            } else {

                cardhtml += '<a target="_blank" href="' + data[i].apply +  '"><i class="far fa-edit"></i></a>';

            }
            var is_favourite = (data[i].in_favourite == 1) ? "_active" : "";
            cardhtml += '<a data-idjb="'+data[i].id_job+'" onclick="' + "option1(this);addTof(" + data[i].id_job +  ');return false;" class="'+is_favourite+'"><i class="fas fa-star"></i></a>';

            cardhtml += '<div class="_share"><a href="' +"#"+ '"><i class="fas fa-share-alt"></i></a><div class="social">';

            cardhtml += '<a data-idjb="'+data[i].id_job+'" onClick="homeShareSocial(this)" data-unique-url="'+data[i].social["unique_twitter_url"]+'" href="'+data[i].social["twitter_url"].replace("Check out this great Job posted on Talentspotting",trans_key.social_href)+'"><i class="fab fa-twitter"></i></a>'
            cardhtml += '<a data-idjb="'+data[i].id_job+'" onClick="homeShareSocial(this)" data-unique-url="'+data[i].social["unique_linkedin_url"]+'" href="'+data[i].social["linkedin_url"]+'"><i class="fab fa-linkedin"></i></a>'
            var current_url=window.location.href;
            console.log(current_url+' url')
            if (current_url.indexOf('unicredit') === -1){
                cardhtml += '<a data-idjb="'+data[i].id_job+'" onClick="homeShareSocial(this)" data-unique-url="'+data[i].social["unique_facebook_url"]+'" href="'+data[i].social["facebook_url"]+'" class=""><i class="fab fa-facebook-f"></i></a>'
            }
            cardhtml += '<a data-idjb="'+data[i].id_job+'" onClick="homeShareSocial(this)" target="_blank" data-unique-url="'+data[i].social["unique_email_url"]+'" href="'+data[i].social["email_url"]
                    .replace("Aplică acum",trans_key.job_application)
                    .replace("Salut",trans_key.hello)
                    .replace("Avem o poziție vacantă de",trans_key.social_href)
                    .replace("aici",trans_key.here)
                    // .replace("Aplică acum aici",trans_key.apply)
                +'">' +
                '<i class="fa fa-envelope"></i></a>'

            cardhtml += '</div></div>';

            cardhtml += '<p class="poi"><a href="'+data[i].job_url+'">'+trans_key.more_details+'</a></p>';

            cardhtml += '</div></div></div>';


        }
        var ss = ""
        html +=  cardhtml + "</div>";
        if(!(user_type.logged)) {
            ss = "style=margin-left:0%"
        }


        if(!document.getElementById(offset_index+"job")) {
            $("#content").append(html)
            fove()
        }
        else
        {
            var px = _sign * 150 * -1;
            if(all)
            {
                $('#' + offset_index+"job  .ww").css("display", "none")
                $('#' + offset_index+"job  .www").css("display", "none")
                $('#' + offset_index+"job").append("<div class='line'>" + cardhtml + "</div>")



                $('#' + offset_index+"job > .line > .card-profile").animate({
                        opacity:"100%",
                    },
                    {
                        duration: 1000,
                        complete: function(){
                            $('#' + offset_index+"sss  .viewall").replaceWith("<div onclick=\"removealljobs("+offset_index+")\" class=\"viewall\" id=\"hoverShow1\"><p>Show less <span> &gt;&gt; </span></p><div class=\"_loader\"><img src=\"\\resources\\frontend\\images\\loader.gif\"></div></div>")
                            fove()
                        }
                    });
            }
            else
            {

                $('#' + offset_index+"job > .line > .card-profile").animate({
                        "left":px,
                        opacity:"0",
                    },
                    {
                        duration: 1000,
                        complete: function(){
                            $('#' + offset_index+"job > .line").remove();
                            $('#' + offset_index+"job").append("<div class='line'>" + cardhtml + "</div>")
                            fove()
                        }
                    });
            }


        }



        return ;
    });


}

$(document).keypress(function(event){

    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        get_jobs();
        $('html, body').animate({ scrollTop: 0 }, 'fast');  }
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function homeShareSocial(obj) {
    var id_job = ($(obj).data('idjb'));
    // var social = getParameterByName('source',url);
    if(user_type.logged)
    {
        var id_user = user_type.id_user;
    }


    var url = ($(obj).data('unique-url'));
    var http = new XMLHttpRequest();
    var endpoint_url = '/cshare';
    var params = 'id_user='+id_user+'&id_job='+id_job+'&url='+url;
    // var params = {'id_user' : id_user, 'id_job':id_job,'url='+url};
    http.open('POST', endpoint_url, true);

    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            // alert(http.responseText);
        }
    }
    if(user_type.logged)
    {
        http.send(params);
    }
}

function all_update_counter(index) {
    counter[index] += 5;
    eval(all_jobs[index]);
}

function    fetch_all_jobs(i)
{
    $('#' + i+"sss  ._loader").css("display", "block")


    while(total_jobs[i] > 0)
    {
        all_update_counter(i)
        total_jobs[i]-=5;
    }
}
