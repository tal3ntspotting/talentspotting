<?php

class models_User extends WebApp_Db_Table
{

    public $_settings = array();
    public $_name = 'users';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    protected $_company;
    protected $_content;
    protected $_payments;
    public $id_lang;
    public $_stats;
    public $_parser;
    public $_modelCompany;
    public $translate;

    public function __construct($id_lang = null)
    {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;
//         $this->translate = Zend_Registry::get("Zend_Translate");
        $this->_content = new models_Content;
        $this->_company = new models_Company;
        $this->_parser = new models_Parser;
        $this->_modelCompany = new models_Company;

        $this->_stats = new models_Stats;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }

    public function getUsers($filters = array())
    {

        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $id_user = Zend_Auth::getInstance()->getIdentity()->id;

        $sql = $this->select();

        $sql->from(array('u' => 'users'), array('*',
//            "(SELECT dl.title FROM departments_lang dl, user_departments ud where ud.id_department = dl.id_department AND ud.id_user = u.id) AS departments",
//            "(SELECT cd.title FROM custom_departments cd , user_departments ud where ud.id_department = cd.id AND ud.id_user = u.id AND cd.id_company = {$this->_user->id_company}) AS custom_departments"
        ))->order('id DESC');


        $sql->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("firstname AS adder_firstname", "lastname AS adder_lastname"));

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
        $sql->where("u.type != 3");
        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                switch ($key) {
                    case "type":
                        if ($value == 9) {
                            $sql->where("u.type = 1");
                            $sql->where("u.id_parent > 0");
                        } else {
                            $sql->where("u.type = ?", (int)$value);
                            $sql->where("u.id_parent = 0");
                        }
                        break;
                }
            }
        }

        if (!$allow_candidate_recommend) {
            $sql->where("u.type != 9");
        }
        $sql_result = $this->fetchAll($sql);

        return $sql_result;
    }

    public function getRecommenderCandidates()
    {
        $sql = $this->select()->from(array("u" => "users"), array("*",
            "(SELECT GROUP_CONCAT(dl.title) FROM departments_lang dl, user_departments ud where ud.id_department = dl.id_department AND ud.id_user = u.id GROUP BY ud.id_user) AS departments"))
//                ->join(array("jr" => "job_recommendations"), "jr.id_user = u.id", array())
            ->where("u.id_company =?", $this->_user->id_company)
            ->where("u.type = 9")
            ->group("u.id");
        $result = $this->fetchAll($sql);
        return $result;
        $array_id = [];
        foreach ($result as $key => $value) {
            $array_id[] = $value['id'];
        }
        if (!empty($array_id)) {
            $sql = $this->select()->from(array('u' => 'users'), array('*', "(SELECT GROUP_CONCAT(dl.title) FROM departments_lang dl, user_departments ud where ud.id_department = dl.id_department AND ud.id_user = u.id AND dl.id_lang = {$this->id_lang} GROUP BY ud.id_user) AS departments"))->order('id DESC');
            if ($this->_user->type == 2) {
                $sql->where("id_company = ?", $this->_user->id_company);
                $sql->where("u.id IN (" . implode(",", $array_id) . ")");
            }
            return $this->fetchAll($sql);
        }
    }

    public function getUserById($id = null)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = $this->select()
            ->from(array('u' => 'users'), array('*'))
            ->where("u.id = ? ", $id);

        return $this->fetchRow($sql);
    }

    public function getUserDepartmentById($id)
    {
        $sql = $this->select()
            ->from(array('s' => 'departments'), array('id'))
            ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('title'))
            ->join(array("ud" => "user_departments"), "ud.id_department = s.id", array())
            ->where("sl.id_lang = ?", $this->id_lang)
            ->where("ud.id_user =?", (int)$id)
            ->order('title ASC');

//        if (null !== $departments) {
//            $sql->where("s.id IN (" . implode(",", $departments) . ")");
//        }
        //die($sql->__toString());
        return $this->fetchPairs($sql);
    }

    public function getUserCustomDepartmentById($id)
    {
        $sql = $this->select()
            ->from(array('s' => 'custom_departments'), array('id', "title"))
//                ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('title'))
            ->join(array("ud" => "user_departments"), "ud.id_department = s.id", array())
//                ->where("sl.id_lang = ?", $this->id_lang)
            ->where("ud.id_user =?", (int)$id)
            ->order('s.title ASC');

//        if (null !== $departments) {
//            $sql->where("s.id IN (" . implode(",", $departments) . ")");
//        }
        //die($sql->__toString());
        return $this->fetchPairs($sql);
    }

//sendgrid itnegration => cron once per day
    public function updateBounces()
    {
        $json = file_get_contents("https://api.sendgrid.com/api/unsubscribes.get.json?api_user=anagiurca&api_key=123brains&date=1");
        $arr = json_decode($json, true);
        foreach ($arr as $v) {
//            $email2 .= $v['email'] . " " . $v['created'];
            $email = $v['email'];
            $email2 = str_replace("@", "[at]", $email); //to be deleted on deploy
            $bounced = $v['created'];
            $this->query("UPDATE users SET bounced=1,bounce_date='$bounced',sendgrid_json='$email2' WHERE LOWER(email) = '$email2'");
        }
    }

    public function saveUser($data, $send_mail = false, $id_email = 1)
    {
        $session = new Zend_Session_Namespace("track");
        $new_password_confirm = $data['new_password_confirm'];
        $original_password = $data['original_password'];
        $new_password = $data['new_password'];
        $jobs = $data['jobs'];
        unset($data['new_password_confirm']);
        unset($data['original_password']);
        unset($data['new_password']);
        unset($data['jobs']);

        $department = $data['department'];
        unset($data['department']);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            if ($data['password'] == "") {
                unset($data['password']);
            } else {
                $data['password'] = sha1($data['password']) . $this->_user->salt;
            }
            if (count($data) > 0) {
                $this->update($data, "id = $id");
            }
        } else {
            $password = $data['password'];
            $data['status'] = isset($data['status']) ? $data['status'] : 0;
            $data['hash'] = sha1(time());
            $data['salt'] = uniqid(mt_rand(), false);
            $data['password'] = sha1($data['password']) . $data['salt'];
            $data['created'] = date('Y-m-d H:i:s');
            unset($data['id']);
            $this->insert($data);
            $id = $this->lastInsertId();


            $url = $_SESSION['url']['url'];
            $parts = parse_url($url);

            parse_str($parts['query'], $query);
            $source = ($query['source']);

            if ($source) {
                $sql = $this->select()->from(array("u" => "users"));
                if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
                    $sql->where("SUBSTRING(sys.fn_sqlvarbasetostr(HASHBYTES('MD5','u.id')),3,32) = ?", (string)$session->track)->limit(1);
                } else {
                    $sql->where("MD5(u.id) = ?", (string)$session->track)->limit(1);
                }
                $user_id = $this->fetchRow($sql)['id'];
                $session->track = uniqid();
                $job_id = explode('/', $_SESSION['url']['url'])[2];
                $this->insert(array("id_recommender" => $user_id, "track" => $session->track, 'id_job' => $job_id), "job_recommendations");
            }
            if (isset($session->track) && $session->track != "") {
                $this->update(array("id_user" => $id), "track = '{$session->track}'", "job_recommendations");
//                $this->_stats->setGamificationPoint($id, "job_recommend");
            }
//            $this->_stats->setGamificationPoint($id, "register");
            $data = $this->assignSendData($data, $send_mail, $id_email, $id);
        }
        if ($this->_settings['ats'] && $data['type'] == 5) {
            $this->setatsUserDepartment($id, $department);
        }

        if ($this->_settings['ats'] && $data['type'] == 4) {
            $this->assignJobsForRecuiter($id, $jobs, $department);
        }
        if (isset($department)) {
            $this->delete("id_user = $id", "user_departments");
            foreach ($department as $value) {
                $this->insert(array("id_user" => $id, "id_department" => $value), "user_departments");
            }
        } else {
            $this->delete("id_user = $id", "user_departments");
        }

        $this->saveImage($id);
        return $id;
    }

    public function saveFlowUser($data, $send_mail = false, $id_email = 1)
    {
        $session = new Zend_Session_Namespace("track");
        $new_password_confirm = $data['new_password_confirm'];
        $original_password = $data['original_password'];
        $new_password = $data['new_password'];
        $jobs = $data['jobs'];
        unset($data['new_password_confirm']);
        unset($data['original_password']);
        unset($data['new_password']);
        unset($data['jobs']);

        $department = $data['department'];
        unset($data['department']);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            if ($data['password'] == "") {
                unset($data['password']);
            } else {
                $data['password'] = sha1($data['password']) . $this->_user->salt;
            }
            if (count($data) > 0) {
                $this->update($data, "id = $id");
            }
        } else {
            $password = $data['password'];
            $data['status'] = isset($data['status']) ? $data['status'] : 0;
            $data['hash'] = sha1(time());
            $data['salt'] = uniqid(mt_rand(), false);
            $data['password'] = sha1($password) . $data['salt'];
            $data['first_login'] = date('Y-m-d H:i:s');
            unset($data['id']);
            $id_company = $this->_modelCompany->getCompanyBySubdomainSlug($this->_settings['sitename'])['id'];
            if ($data['id_company'] == 0 || is_null($data['id_company'])) {
                $data['id_company'] = $id_company;
            }
            $this->insert($data);
            $id = $this->lastInsertId();
            $userOnject = Domain_User_UserFactory::createInstance($id);
            $userOnject->setPassword($password);
            $userOnject->sendWelcomeEmail();


            $url = $_SESSION['url']['url'];
            $parts = parse_url($url);

            parse_str($parts['query'], $query);
            $source = ($query['source']);

            if ($source) {
                $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string)$session->track)->limit(1);
                $user_id = $this->fetchRow($sql)['id'];
                $session->track = uniqid();
                $job_id = explode('/', $_SESSION['url']['url'])[2];
                $this->insert(array("id_recommender" => $user_id, "track" => $session->track, 'id_job' => $job_id), "job_recommendations");
            }
            if (isset($session->track) && $session->track != "") {
                $this->update(array("id_user" => $id), "track = '{$session->track}'", "job_recommendations");
            }

//            echo $id_email;die;

//
//            $companie_model = new models_Company;
//            if ($send_mail === true) {
//                $socialModel = new models_Social;
//                $data['company_title'] = $companie_model->getCompanyByUserId($id)['title'];
//
//                $transport = null;
//                if (!empty($this->_settings['email_smtp'])) {
//                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//                }
//                $contentModel = new models_Content;
//                $email = $contentModel->getEmailByTypeCompany($id_email, $data['id_company']);
//
//                $me = $this->getUserById($id);
//                $link = $this->_settings['sitename'] . '/activate?token=' . sha1($me['email']);
//
//                $smarty = new Smarty;
//                $smarty->assign("link", $link);
//                $smarty->assign("user", $data);
//                $smarty->assign("password", $_POST);
//                $smarty->assign("subdomain_url", $this->_settings['sitename']);
//                $smarty->assign("bg_color", $companie_model->getCompanyEmailButtonColor());
//                $smarty->assign("company", $companie_model->getCompanyById($data['id_company']));
//                $emailBody = $smarty->fetch('eval:' . $email['content']);
//                $emailSubject = $smarty->fetch('eval:' . $email['subject']);
//                $mail = new WebApp_Mail();
//                $mail->addTo($data['email'], $data['firstname']);
//                $template = $companie_model->getCompanyEmailTemplate();
//                $smarty->assign('subject', $emailSubject);
//                $smarty->assign('content', $emailBody);
//                $content = $smarty->fetch('eval:' . $template);
//                $mail->setFrom($this->_company->getCompanyById($this->getUserDataByEmail($data['email'])['id_company'])['email'], $this->_company->getCompanyById($this->getUserDataByEmail($data['email'])['id_company'])['title']);
//                $mail->setSubject($emailSubject);
//                $mail->setBodyHtml($content);
//                $mail->send($transport);
//
//            }
        }
        if ($this->_settings['ats'] && $data['type'] == 5 && !empty($department)) {
            $this->setatsUserDepartment($id, $department);
        }

        if ($this->_settings['ats'] && $data['type'] == 4 && !empty($department)) {
            $this->assignJobsForRecuiter($id, $jobs, $department);
        }
        if (isset($department)) {
            $this->delete("id_user = $id", "user_departments");
            foreach ($department as $value) {
                $this->insert(array("id_user" => $id, "id_department" => $value), "user_departments");
            }
        } else {
            $this->delete("id_user = $id", "user_departments");
        }

        $this->saveImage($id);
        return $id;
    }

    public function saveImage($id)
    {
        if ($_FILES['image']['name'] != "") {
            $image = $_FILES['image'];
            $file = $id . "_" . $image['name'];
            WebApp_UploadImage::constrainImage(300, 300, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/user/" . $file);
            $this->update(array("image" => $file), "id = {$id}");

            $this->_stats->setGamificationPoint($id, "upload_image");

            $user = $this->getUserById($id);
            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);

            return $file;
        }
    }

    public function saveImageProfile($id)
    {
        if ($_FILES['file']['name']) {
            $image = $_FILES['file'];
            $file = $id . "_" . $image['name'];
            WebApp_UploadImage::constrainImage(300, 300, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/user/" . $file);
            $this->update(array("image" => $file), "id = {$id}");

            $this->_stats->setGamificationPoint($id, "upload_image");

            $user = $this->getUserById($id);
            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);

            return $file;
        }
    }

    public function saveCoverImage()
    {
        if ($_FILES['file']['name'] != "") {
            $image = $_FILES['file'];
            $file = $this->_user->id . "_" . $image['name'];
            WebApp_UploadImage::constrainImage(300, 300, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/user/" . $file);
            $this->update(array("image" => $file), "id = {$this->_user->id}");

            $this->_stats->setGamificationPoint($this->_user->id, "upload_image");

            $user = $this->getUserById($this->_user->id);
            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);

            return $file;
        }
    }

    public function saveCompanyImage($id)
    {
        $image = $_FILES[0];
        $file = $id . "_" . $image['name'];

        WebApp_UploadImage::constrainImage(250, 250, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/large/" . $file);
        WebApp_UploadImage::constrainImage(150, 150, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/list/" . $file);
        WebApp_UploadImage::constrainImage(50, 50, $image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/thumb/" . $file);
        move_uploaded_file($image['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/original/" . $file);

        $this->update(array("logo" => $file), "id = {$id}", "companies");
        return $file;
    }

    public function deleteUser($id)
    {
        $this->delete("id = {$id}", "users");
    }

    public function getUser()
    {
        return $this->_user;
    }

    public static function userCanEdit()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            return false;
        }

        return Zend_Auth::getInstance()->getIdentity()->permission_type === 'edit';
    }

    public function resetPassword($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", $email);
        $data = $this->fetchRow($sql);
//        $sqldie($sql);
//        print_r($data);die;
        if (!empty($data)) {
            $password = sha1($email . time());
            $this->update(array("password_token" => $password), "id = {$data['id']}");


            $transport = null;
            if (!empty($this->_settings['email_smtp'])) {
                $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }

            $mail = new WebApp_Mail();
            $mail->setFrom("hello@talentspotting.com", $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
            $mail->addTo($email, $data->name);
            $mail->setSubject("{$this->_settings['sitename']} - Password Reset");
            $mail->setBodyFromViewScript("reset.phtml", array("user" => $data, "password" => $password, "settings" => $this->_settings));
            $mail->send($transport);

            return $email;
        } else {
            return "recover_error";
        }
    }

    public function getCampaignsReach()
    {
        return $this->_campaigns->getCampaignsReach();
    }

    public function getAttributesData()
    {
        return $this->_attributes->getAttributesData();
    }

    public function getCampaignsPerformance()
    {
        return $this->_campaigns->getCampaignsPerformance();
    }

    public function authenticate($email, $password)
    {

        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email)->where("password = ?", sha1($password) . $this->_user->salt);
        $user = $this->fetchRow($sql);
        if (!empty($user)) {
            if ($user['id_company'] > 0 && $user['type'] == 2) {
                $this->insert(array("id_user" => $user['id'], "view" => 1), "user_tooltip_view");
            }

            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");
        } else {
            return 0;
        }


        return (array)$user;
    }

    public function authenticateByEmail($email)
    {

        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        $user = $this->fetchRow($sql);

        if (!empty($user)) {
            if ($user['id_company'] > 0 && $user['type'] == 2) {
//                $this->insert(array("id_user" => $user['id'], "view" => 1), "user_tooltip_view");
            }

            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");
        } else {
            return 0;
        }


        return (array)$user;
    }

    public function authenticateLinkedin($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        $user = $this->fetchRow($sql);

        if (!empty($user)) {
            if ($user['id_company'] > 0 && $user['type'] == 2) {
                $this->insert(array("id_user" => $user['id'], "view" => 1), "user_tooltip_view");
            }

            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");
        }


        return (array)$user;
    }

    public function authenticateTwitter($id)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("twitter_id = ?", (int)$id);
        $user = $this->fetchRow($sql);

        if (!empty($user)) {
            if ($user['id_company'] > 0 && $user['type'] == 2) {
                $this->insert(array("id_user" => $user['id'], "view" => 1), "user_tooltip_view");
            }

            $userInfo = (object)$user;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");
        }


        return (array)$user;
    }

    public function reauthenticate($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        $user = $this->fetchRow($sql);

        if (!empty($user)) {
            if ($user['id_company'] > 0 && $user['type'] == 2) {
                $this->insert(array("id_user" => $user['id'], "view" => 1), "user_tooltip_view");
            }

            $userInfo = (object)$user;
            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}", "users");
        }


        return (array)$user;
    }

    public function saveAndRegister($data)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$data['email']);
        $user = $this->fetchRow($sql);

        if (empty($user)) {
            $data['password'] = sha1($data['password']);
            $this->insert($data, "users");

            $data['id'] = $this->lastInsertId();
            $this->_stats->setGamificationPoint($data['id'], "register");
            $this->_stats->setNotification($data['id'], "user", "profile", "Welcome to WorkinAfrica! <br>Start finding your dream job.");
            $userInfo = (object)$data;

            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

            return (array)$data;
        } else {
            return false;
        }
    }

    public function saveFromLinkedin($data)
    {
        $password = $data['password'];
        $data['password'] = sha1($data['password']);
        $this->insert($data, "users");

        $data['id'] = $this->lastInsertId();
        $this->_stats->setGamificationPoint($data['id'], "register");

        if ($data['image'] != "") {
            $this->_stats->setGamificationPoint($data['id'], "upload_image");
        }

        $this->_stats->setNotification($data['id'], "user", "profile", "Welcome to WorkinAfrica! <br>Start finding your dream job.");
        $userInfo = (object)$data;

        Zend_Auth::getInstance()->getStorage()->write($userInfo);
        $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

        $socialModel = new models_Social;
        $emailContent = $socialModel->getEmailByType(1);

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $mail = new WebApp_Mail();
        $mail->addTo($data['email'], $data['firstname']);
        $mail->setFrom($this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject($emailContent['subject']);
        $mail->setBodyFromHTML($emailContent['content'], array("user" => $data, "password" => $password, "settings" => $this->_settings));
        $mail->send($transport);

        return (array)$data;
    }

    public function updateTwitter($data)
    {
        $password = $data['password'];
        $data['password'] = sha1($data['password']);
        $this->update($data, "id = {$this->_user->id}", "users");

        $user = $this->getUserById($this->_user->id);
        $userInfo = (object)$user;

        Zend_Auth::getInstance()->getStorage()->write($userInfo);
        $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $socialModel = new models_Social;
        $emailContent = $socialModel->getEmailByType(1);

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $mail = new WebApp_Mail();
        $mail->addTo($data['email'], $data['firstname']);
        $mail->setFrom($this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject($emailContent['subject']);
        $mail->setBodyFromHTML($emailContent['content'], array("user" => $data, "password" => $password, "settings" => $this->_settings));
        $mail->send($transport);

        return (array)$data;
    }

    public function saveFromTwitter($data)
    {
        $password = $data['password'];
        $data['password'] = sha1($data['password']);
        $this->insert($data, "users");

        $data['id'] = $this->lastInsertId();
        $this->_stats->setGamificationPoint($data['id'], "register");
        $this->_stats->setNotification($data['id'], "user", "profile", "Welcome to WorkinAfrica! <br>Start finding your dream job.");
        $userInfo = (object)$data;

        Zend_Auth::getInstance()->getStorage()->write($userInfo);
        $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");
        return (array)$data;
    }

    public function getUserByEmail($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        $user = $this->fetchRow($sql);

        return (!empty($user));
    }

    public function getUserDataByEmail($email)
    {
        $sql = $this->select()->from(array("u" => "users"), array("*"))->where("email = ?", (string)$email);
        $user = $this->fetchRow($sql);

        return ($user);
    }

    public function getUserDetailsByEmail($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        return $this->fetchRow($sql);
    }

    public function getUserByLinkedinId($id)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("linkedin_id = ?", (string)$id);
        $user = $this->fetchRow($sql);

        return (!empty($user));
    }

    public function getUserPositions($id = null)
    {
        $sql = $this->select()->from(array("up" => "user_positions"));
        $sql->order('up.end DESC');
        if (null !== $id) {
            $sql->where("id_user = ?", (int)$id);
        } else {
            $sql->where("id_user = ?", (int)$this->_user->id);
        }
        return $this->fetchAll($sql);
    }

    public function getUserPositionsByAttribute($id = null, $attributes = [])
    {

        $sql = $this->select()->from(array("up" => "user_positions", $attributes));
        if (null !== $id) {
            $sql->where("id_user = ?", (int)$id);
        } else {
            $sql->where("id_user = ?", (int)$this->_user->id);
        }
        return $this->fetchAll($sql);
    }

    public function getFullUserPositions($id = null)
    {

        $sql = $this->select()->from(array("up" => "user_positions"))
            ->join(array("il" => "industries_lang"), "il.id_industry = up.id_industry", array("title as industry_title"));
//                ->join(array("dl" => "departments_lang"), "up.id_department = dl.id_department", array("title as department_title"));
        if (null !== $id) {
            $sql->where("id_user = ?", (int)$id);
        } else {
            $sql->where("id_user = ?", (int)$this->_user->id);
        }
        $result = $this->fetchRow($sql);


        $query = $this->select()->from(array("ud" => "user_departments", array()))
            ->join(array("cd" => "custom_departments"), "cd.id = ud.id_department", array("title"));
        if (null !== $id) {
            $query->where("ud.id_user = ?", (int)$id);
        } else {
            $query->where("ud.id_user = ?", (int)$this->_user->id);
        }
        $query->group("ud.id_department");
        $custom_departemnt = $this->fetchAll($query);


        $query_ = $this->select()->from(array("ud" => "user_departments", array()))
            ->join(array("dl" => "departments_lang"), "dl.id_department = ud.id_department", array("title"))
            ->where("ud.id_user =?", (int)$id)
            ->group("ud.id_department");
        $departemnt = $this->fetchAll($query_);


        $departemnt = array_merge($departemnt, $custom_departemnt);
        $result['departments'] = $departemnt;
        return $result;
    }

    public function removeUserPositions($id)
    {
        if (!$id) {
            return;
        }
        $id = (int)$id;
        $this->delete(array("id = ?" => $id, "id_user = ?" => $this->_user->id), "user_positions");
    }

    public function removeUserSkill($id)
    {
        if (!$id) {
            return;
        }
        $id = (int)$id;
        $result = $this->delete(array("id_skill = ?" => $id, "id_user = ?" => $this->_user->id), "user_skills");

        if (!$result) {
            $this->delete(array("id = ?" => $id, "id_user = ?" => $this->_user->id), "custom_user_skills");
        }
    }

    public function removeUserLanguage($id)
    {
        if (!$id) {
            return;
        }
        $id = (int)$id;
        $result = $this->delete(array("id = ?" => $id, "id_user = ?" => $this->_user->id), "user_languages");
    }

    public function removeUserCertification($id)
    {
        if (!$id) {
            return;
        }
        $id = (int)$id;
        $this->delete(array("id_certification = ?" => $id, "id_user = ?" => $this->_user->id), "user_certifications");
    }

    public function removeUserEducation($id)
    {
        if (!$id) {
            return;
        }
        $id = (int)$id;
        $this->delete(array("id = ?" => $id, "id_user = ?" => $this->_user->id), "user_education");
    }

    public function getLinkedinProfile($id)
    {
        $sql = $this->select()
            ->from('users')->where('id = ?', (int)$id);
        return $this->fetchRow($sql);
    }

    public function getUserSkills($id = null)
    {
        $id = (null === $id) ? $this->_user->id : $id;
        return $this->_content->getUserSkills($id);
    }

    public function getCustomUserSkills($id = null)
    {
        $id = (null === $id) ? $this->_user->id : $id;
        return $this->_content->getCustomUserSkills($id);
    }

    public function getUserEducation($id = null)
    {
        $sql = $this->select()->from(array("ue" => "user_education"));

        if (null !== $id) {
            $sql->where("id_user = ?", (int)$id);
        } else {
            $sql->where("id_user = ?", (int)$this->_user->id);
        }

        return $this->fetchAll($sql);
    }

    public function getUserCertifications($id = null)
    {
        $id = (null === $id) ? $this->_user->id : $id;
        return $this->_content->getUserCertifications($id);
    }

    public function getUserApplications()
    {
        $jobs = new models_Jobs;
        return $jobs->getUserApplications($this->_user->id);
    }

    public function getCounterAlertJobs($title, $skills, $departments, $cities)
    {
        $sql = $this->select()->from(array('jl' => 'jobs_lang'), array('jl.id', 'jl.slug', 'jl.id_job', 'jl.title'))
            ->join(array('j' => 'jobs'), 'j.id = jl.id_job', ['j.id'])
            ->join(array('co' => 'companies'), "j.id_company = co.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
            ->where("jl.title LIKE '%{$title}%' AND j.id_company = {$this->_user->id_company}");

        if (count($skills) > 0) {
            $sql->join(array('js' => 'job_skills'), 'js.id_job = j.id');
            $sql->join(array('sl' => 'skills_lang'), 'sl.id_skill = js.id_skill', array('slug AS skill_slug'));
            $sql->where("js.id_skill IN (" . implode(",", array_flip($skills)) . ")");
        }
        if (count($departments) > 0) {
            $sql->join(array('jd' => 'job_departments'), 'jd.id_job = j.id');
            $sql->join(array('dl' => 'departments_lang'), 'dl.id_department = jd.id_department', array('slug AS department_slug', 'title AS department_title'));
            $sql->where("dl.id_department IN (" . implode(",", array_flip($departments)) . ")");
        }
        if (count($cities) > 0) {
            $sql->join(array('jc' => 'job_cities'), 'jc.id_job = j.id');
            $sql->where("jc.id_city IN (" . implode(",", array_flip($cities)) . ")");
        }

        return $this->fetchAll($sql);
    }

    public function getUserAlerts()
    {
        $jobs = new models_Jobs;
        return $jobs->getUserAlerts($this->_user->id);
    }

    public function getPositionById($id)
    {
        $sql = $this->select()->from(array("up" => "user_positions"))->where("up.id = ?", (int)$id)->join(array('u' => 'users'), 'up.id_user = u.id', array('location'));
        return $this->fetchRow($sql);
    }

    public function getDepartmentbyUserId($id)
    {
        $sql = $this->select()->from(array("ud" => "user_departments"), array('id', "id_department"))->where("ud.id_user = ?", (int)$id);
        return $this->fetchPairs($sql);
    }

    public function getEducationById($id)
    {
        $sql = $this->select()->from(array("ue" => "user_education"))->where("id = ?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function getCertificationById($id)
    {
        $sql = $this->select()
            ->from(array("ue" => "user_certifications"), array())
            ->join(array("c" => "certifications"), "ue.id_certification = c.id", array("company", "id"))
            ->join(array("cl" => "certifications_lang"), "c.id = cl.id_certification", array("title"))
            ->where("cl.id_lang = ?", $this->id_lang)
            ->where("c.id = ?", (int)$id);

        return $this->fetchRow($sql);
    }

    public function savePosition($data)
    {

        $this->checkCompletedProfile($data, 'history');
//        if ($data['location']) {
        $location = $data['location'];
        unset($data['location']);
//        }
        $data['id_user'] = $this->_user->id;
        $data['start'] = date("Y-m-d", strtotime($data['start']));
        $data['end'] = date("Y-m-d", strtotime($data['end']));

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update($data, "id = $id", "user_positions");
            if ($location !== "") {
                $sql = $this->select()->from(array('u' => 'user_positions'), array('id_user'))->where('id = ?', $id);
                $me = $this->fetchRow($sql)['id_user'];
                $this->update(array('location' => $location), "id = $me", "users");
            }
        } else {
            if ($data['location']) {
                $location = $data['location'];
                unset($data['location']);
            }
            unset($data['id']);
            $data['id_department'] = $data['id_department'][0];

            $this->insert($data, "user_positions");
            $id = $this->lastInsertId();
            $this->_stats->setGamificationPoint($data['id_user'], "job_position");
            if ($location !== "") {
                $sql = $this->select()->from(array('u' => 'user_positions'), array('id_user'))->where('id = ?', $id);
                $me = $this->fetchRow($sql)['id_user'];
                $this->update(array('location' => $location), "id = $me", "users");
            }
        }
        $this->saveUserDepartment($this->_user->id, $data['id_department']);
        return $id;
    }

    public function saveUserDepartment($id, $departments)
    {
        $this->delete("id_user = $id", "user_departments");
        foreach ($departments as $department) {
            $this->insert(array('id_user' => $id, 'id_department' => $department), 'user_departments');
        }
    }

    public function checkCompletedProfile($data, $source)
    {
        $modelJobs = new models_Jobs;

        /**
         * check if job is in campaign by user recommeder
         */
        $id_user = $this->_user->id;

        $sql = $this->select()->from(array("u" => "users"), array("id_parent"))->where("u.id =?", (int)$id_user);
        $data = $this->fetchRow($sql);
        if ($data['id_parent'] > 0) {
            $id_recommender = $data['id_parent'];
            // get the job recommedation 
            $sql = $this->select()->from(array("jr" => "job_recommendations"), array("*"))->where("jr.id_recommender =?", (int)$id_recommender)
                ->where("jr.id_user =?", (int)$id_user);
            $result = $this->fetchRow($sql);
            if (!empty($result)) {
                $id_job = $result['id_job'];
                // chek if job is part of campaign
                $isPartOfCampaign = $modelJobs->jobIsPartOfCampaign($id_job);
                if (!empty($isPartOfCampaign)) {

                    $id_campaign = $isPartOfCampaign['id'];
                    $actionInCampaign = $this->getActionFromCampaignbyId($id_campaign);
                    // completed_profile is included in campaign actions
                    if (in_array("completed_profile", $actionInCampaign)) {
                        // check if user has completed his profile before
                        $isProfileCompleted = $this->checkCompletedProfilebyUserId($id_user);
                        if ($source == "history") {
                            if (!empty($isProfileCompleted)) {
                                $this->updateCompletedHistory($id_user, $id_job);
                            } else {
                                $this->setCompletedHistory($id_user, $id_job);
                            }
                        }
                        if ($source == "education") {
                            if (!empty($isProfileCompleted)) {
                                $this->updateCompletedEducation($id_user, $id_job);
                            } else {
                                $this->setCompletedEducation($id_user, $id_job);
                            }
                        }
                        $this->setCompletedProfileActionPoints($id_user, $id_recommender, "completed_profile", $id_job);

//                        echo '<pre>';
//                        print_r($isPartOfCampaign);
//                        print_r($result);
//                        print_r($actionInCampaign);
//                        print_r($isProfileCompleted);
//                        die;
                    }
                } else {


                    // completed_profile is included in campaign actions
                    // check if user has completed his profile before
                    $isProfileCompleted = $this->checkCompletedProfilebyUserId($id_user);
                    if ($source == "history") {
                        if (!empty($isProfileCompleted)) {
                            $this->updateCompletedHistory($id_user, $id_job);
                        } else {
                            $this->setCompletedHistory($id_user, $id_job);
                        }
                    }
                    if ($source == "education") {
                        if (!empty($isProfileCompleted)) {
                            $this->updateCompletedEducation($id_user, $id_job);
                        } else {
                            $this->setCompletedEducation($id_user, $id_job);
                        }
                    }
                    $this->_stats->setGamificationPoint($id_recommender, "completed_profile", $id_job);

//                        echo '<pre>';
//                        print_r($isPartOfCampaign);
//                        print_r($result);
//                        print_r($actionInCampaign);
//                        print_r($isProfileCompleted);
//                        die;
                }
            } else {
                $sql = $this->select()->from(array("up" => "user_profile"), array("*"))->where("up.id_user =?", (int)Zend_Auth::getInstance()->getIdentity()->id);
                $result = $this->fetchRow($sql);
                if (!empty($result)) {

                    $id_job = $result['id_job'];
                    // chek if job is part of campaign
                    $isPartOfCampaign = $modelJobs->jobIsPartOfCampaign($id_job);
                    if (!empty($isPartOfCampaign)) {

                        $id_campaign = $isPartOfCampaign['id'];
                        $actionInCampaign = $this->getActionFromCampaignbyId($id_campaign);
                        // completed_profile is included in campaign actions
                        if (in_array("completed_profile", $actionInCampaign)) {
                            // check if user has completed his profile before
                            $isProfileCompleted = $this->checkCompletedProfilebyUserId($id_user);
                            if ($source == "history") {
                                if (!empty($isProfileCompleted)) {
                                    $this->updateCompletedHistory($id_user, $id_job);
                                } else {
                                    $this->setCompletedHistory($id_user, $id_job);
                                }
                            }
                            if ($source == "education") {
                                if (!empty($isProfileCompleted)) {
                                    $this->updateCompletedEducation($id_user, $id_job);
                                } else {
                                    $this->setCompletedEducation($id_user, $id_job);
                                }
                            }
                            $this->setCompletedProfileActionPoints($id_user, $id_recommender, "completed_profile", $id_job);

//                        echo '<pre>';
//                        print_r($isPartOfCampaign);
//                        print_r($result);
//                        print_r($actionInCampaign);
//                        print_r($isProfileCompleted);
//                        die;
                        }
                    } else {


                        $isProfileCompleted = $this->checkCompletedProfilebyUserId($id_user);
                        if ($source == "history") {
                            if (!empty($isProfileCompleted)) {
                                $this->updateCompletedHistory($id_user, $id_job);
                            } else {
                                $this->setCompletedHistory($id_user, $id_job);
                            }
                        }
                        if ($source == "education") {
                            if (!empty($isProfileCompleted)) {
                                $this->updateCompletedEducation($id_user, $id_job);
                            } else {
                                $this->setCompletedEducation($id_user, $id_job);
                            }
                        }
                        $this->_stats->setGamificationPoint($id_recommender, "completed_profile", $id_job);
                    }
                }
            }
        }
    }

    public function getActionFromCampaignbyId($id = null)
    {
        $sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), array("id_gamification_action"))
            ->join(array("ga" => "gamifications_actions"), "cga.id_gamification_action = ga.id", array("action"));
        if (!is_null($id)) {
            $sql->where("cga.id_campaign =?", (int)$id);
        }

        return $this->fetchPairs($sql);
    }

    public function checkCompletedProfilebyUserId($id)
    {
        $sql = $this->select()->from(array("up" => "user_profile"), array("*"))
            ->where("up.id_user =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function setCompletedHistory($id, $id_job)
    {
        $data['id_user'] = (int)$id;
        $data['id_job'] = (int)$id_job;
        $data['completed_history'] = (int)1;
        $this->insert($data, "user_profile");
    }

    public function updateCompletedHistory($id, $id_job)
    {
        $data['completed_history'] = (int)1;
        $this->update($data, "id_user = {$id}", "user_profile");
    }

    public function setCompletedEducation($id, $id_job)
    {
        $data['id_user'] = (int)$id;
        $data['id_job'] = (int)$id_job;
        $data['completed_education'] = (int)1;
        $this->insert($data, "user_profile");
    }

    public function updateCompletedEducation($id, $id_job)
    {
        $data['completed_education'] = (int)1;
        $this->update($data, "id_user = {$id}", "user_profile");
    }

    public function setCompletedProfileActionPoints($id_user, $id_recommender, $action, $id_job)
    {
        $sql = $this->select()->from(array("up" => "user_profile"), array("*"))
            ->where("up.id_user =?", (int)$id_user);
        $result = $this->fetchRow($sql);
        if ($result['completed_history'] == 1 && $result['completed_education'] == 1) {
            if ($result['completed_profile'] == 0) {
                $data['completed_profile'] = 1;
                $this->update($data, "id_user = {$id_user}", "user_profile");
                $this->_stats->setCampaignReport($id_recommender, $action, $id_job);
            }
        }
    }

    public function saveSkill($data)
    {

        foreach ($data['id_skill'] as $id_skill) {
            $this->insert(array("id_skill" => $id_skill, "id_user" => $this->_user->id,
                'rating' => $data['rating'], 'skill_years' => $data['years']), "user_skills");
//            $this->_stats->setGamificationPoint($this->_user->id, "skills");
        }
    }

    public function saveLanguge($data)
    {
        $insert['language'] = ucfirst($data['language']);
        $insert['rating'] = $data['rating'];
        $insert['date'] = date('Y-m-d H:i:s');
        $insert['id_user'] = $this->_user->id;
        $this->insert($insert, 'user_languages');
    }

    public function saveEducation($data)
    {
        $this->checkCompletedProfile($data, 'education');
        $data['start'] = date("Y-m-d", strtotime($data['start']));
        $data['end'] = date("Y-m-d", strtotime($data['end']));
        $data['id_user'] = $this->_user->id;
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update($data, "id = {$id}", "user_education");
        } else {
            unset($data['id']);
            $this->insert($data, "user_education");
            $this->_stats->setGamificationPoint($data['id_user'], "education");
        }
        $id = $this->lastInsertId();

        return $id;
    }

    public function saveCertification($data)
    {
        if (isset($data['id']) && $data['id'] > 0) {
            $this->update(array("title" => $data['title'], "slug" => WebApp_Tools::CreateAlias($data['title'])), "id_certification = {$data['id']} AND id_lang = {$this->id_lang}", "certifications_lang");
            $this->update(array("company" => $data['company']), "id = {$data['id']}", "certifications");
        } else {
            $certifications = $this->_content->getCertificationsSelect();
            if (in_array($data['title'], $certifications)) {
                $id_certification = array_search($data['title'], $certifications);
                $this->insert(array("id_user" => $this->_user->id, "id_certification" => $id_certification), "user_certifications");
            } else {
                $this->insert(array("company" => $data['company']), "certifications");
                $id_certification = $this->lastInsertId();

                $this->insert(array("title" => $data['title'], "slug" => WebApp_Tools::CreateAlias($data['title']), "id_lang" => $this->id_lang, "id_certification" => $id_certification), "certifications_lang");
                $this->insert(array("id_user" => $this->_user->id, "id_certification" => $id_certification), "user_certifications");
            }
        }
    }

    public function getAlertById($id)
    {
        $sql = $this->select()->from(array("ua" => "user_alerts"))->where("id = ?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function saveAlert($data)
    {
        $data['id_user'] = $this->_user->id;

        if (!empty($data['skills'])) {
            $conditions['skills'] = $this->_content->getSkillsSelect($data['skills']);
            unset($data['skills']);
        }

        if (!empty($data['companies'])) {
            $conditions['companies'] = $this->_company->getCompaniesSelect($data['companies']);
            unset($data['companies']);
        }

        if (!empty($data['departments'])) {
            $conditions['departments'] = $this->_content->getDepartmentsSelect($data['departments']);
            unset($data['departments']);
        }

        if (!empty($data['cities'])) {
            $conditions['cities'] = $this->_content->getCitiesSelect(null, $data['cities']);
            unset($data['cities']);
        }

        if (!empty($data['countries'])) {
            $conditions['countries'] = $this->_content->getCountriesSelect($data['countries']);
            unset($data['countries']);
        }

        if (!empty($data['industries'])) {
            $conditions['industries'] = $this->_content->getIndustriesSelect($data['industries']);
            unset($data['industries']);
        }

        $data['conditions'] = serialize($conditions);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update($data, "id = $id", "user_alerts");
        } else {
            $data['created'] = date("Y-m-d H:i:s");
            unset($data['id']);
            $this->insert($data, "user_alerts");
            $id = $this->lastInsertId();
        }

        return $id;
    }

    public function getPositionsSelect()
    {
        $sql = $this->select()->from(array("up" => "user_positions"), array("id", "title"));
        return $this->fetchPairs($sql);
    }

    public function searchUsers($filters = array())
    {
        $sql = $this->select()
            ->from(array("u" => "users"))
            ->joinLeft(array("up" => "user_positions"), "u.id = up.id_user", array("title as position"))
            ->joinLeft(array("ue" => "user_education"), "u.id = ue.id_user", array("title as school"))
            ->group("u.id")
            ->where("u.status < 3")
            ->where("u.id != '{$this->_user->id}'")
            ->order("u.id DESC")
            ->order("up.id DESC")
            ->order("ue.id DESC");
        if (!empty($filters)) {
            if (isset($filters['id_skill'])) {
                $sql->join(array("us" => "user_skills"), "u.id = us.id_user");
                $sql->where("us.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
            }

            if (isset($filters['id_certification'])) {
                $sql->join(array("uc" => "user_certifications"), "u.id = uc.id_user");
                $sql->where("uc.id_certification IN (" . implode(",", $filters['id_certification']) . ")");
            }

            if (isset($filters['id_department'])) {
                $sql->join(array("ud" => "user_departments"), "u.id = ud.id_user");
                $sql->where("ud.id_department IN (" . implode(",", $filters['id_department']) . ")");
            }

            if (isset($filters['id_industry'])) {
                $sql->join(array("ui" => "user_industries"), "u.id = ui.id_user");
                $sql->where("ui.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
            }

            if (isset($filters['search']) && $filters['search'] != "") {
                $sql->where("u.firstname LIKE '%{$filters['search']}%' OR u.lastname LIKE '%{$filters['search']}%' OR u.title LIKE '%{$filters['search']}%' OR u.description LIKE '%{$filters['search']}%' OR ue.title LIKE '%{$filters['search']}%' OR ue.degree LIKE '%{$filters['search']}%' OR up.title LIKE '%{$filters['search']}%' OR up.summary LIKE '%{$filters['search']}%'");
            }
        }
        return $this->fetchAll($sql);
    }

    public function getUserApplicationsById($id)
    {
        $sql = $this->select()
            ->from(array('job_app' => 'job_applications'), ['id as title'])
            ->where('job_app.id_user = ' . $id)
            ->joinLeft(array('jobs_lang', 'jobs_lang'), 'job_app.id_job = jobs_lang.id_job');
        $result = $this->fetchAll($sql);
        return $result;
    }

    public function searchUsersMine($filters = array())
    {
        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];

        $sql = $this->select()
            ->from(array("u" => "users"), array("u.id", "u.firstname", "u.lastname", "u.last_login", "u.type", "u.image"
//                    "(SELECT ja.status FROM job_applications ja WHERE ja.id_user = u.id) as status",
//                    "(SELECT ja.status_reason FROM job_applications ja WHERE ja.id_user = u.id) as status_reason",
            ))
            ->joinLeft(array("up" => "user_positions"), "u.id = up.id_user", array("up.title as position", "up.is_current as hired"))
            ->joinLeft(array("ue" => "user_education"), "u.id = ue.id_user", array("ue.title as school"))
            ->joinLeft(array("r" => "job_recommendations"), "r.id_user = u.id AND r.id_recommender = u.id_parent", array("r.date AS recommend_date"))
            ->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("uu.firstname AS recommended_firstname", "uu.lastname AS recommended_lastname"));
        $sql->joinleft(array("c" => "company_unblocked_cvs"), "c.id_user = u.id", array(""));
        if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $sql->group(["u.id", "u.firstname", "u.lastname", "up.title", "up.is_current", "ue.title", "r.date", "uu.firstname", "uu.lastname", "u.last_login"]);
        } else {
            $sql->group(["u.id"]);
        }
        $sql->where("u.status = 1")
            ->where("u.id != '{$this->_user->id}'")
            ->where("u.type = 9")
            ->order("r.date DESC");
        $sql->where("u.id_company = ?", $this->_user->id_company);
//        if (Zend_Auth::getInstance()->type == 2) {
//            $sql->where("u.id_company = ?", $this->_user->id_company);
//        }
        if (!empty($filters)) {
            if (isset($filters['id_skill'])) {
                $sql->join(array("us" => "user_skills"), "u.id = us.id_user");
                $sql->where("us.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
            }

            if (isset($filters['id_certification'])) {
                $sql->join(array("uc" => "user_certifications"), "u.id = uc.id_user");
                $sql->where("uc.id_certification IN (" . implode(",", $filters['id_certification']) . ")");
            }

            if (isset($filters['search']) && $filters['search'] != "") {
                $sql->where("u.firstname LIKE '%{$filters['search']}%' OR u.lastname LIKE '%{$filters['search']}%' OR u.title LIKE '%{$filters['search']}%' OR u.description LIKE '%{$filters['search']}%' OR ue.title LIKE '%{$filters['search']}%' OR ue.degree LIKE '%{$filters['search']}%' OR up.title LIKE '%{$filters['search']}%' OR up.summary LIKE '%{$filters['search']}%'");
            }
        }

        $result = $this->fetchAll($sql);
        return $result;
//        return array_merge($this->searchUsersMineRecommneder($filters), $result);
    }

    public function getApplication($users)
    {
        foreach ($users as $key => &$user) {
            $id_user = (int)$user['id'];
            $users[$key]['application'] = $this->getTotalapplication($id_user);
        }
        return $users;
    }

    public function getUserJobsApplication($users)
    {
        $users = (array)$users;
        foreach ($users as $index => &$user) {
            $users[$index]['jobs'] = $this->getUserApplicationsById($user['id']);
            foreach ($users[$index]['jobs'] as $key => &$job) {
                $users[$index]['jobs'][$key]['job_url'] = $this->getJobUrlById($job['id_job']);
            }
        }
        return $users;
    }

    public function getJobUrlById($id)
    {
        $model_Jobs = new models_Jobs;
        $job = $model_Jobs->getJobById($id);
        return WebApp_Tools::createJobSlug($job);
    }

    public function getTotalapplication($id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['ja.id', 'ja.id_job'])
            ->join(['u' => 'users'], 'u.id=ja.id_user', [])  // more unique id_user in ja than found in users ....!?
            ->where("ja.id_user =?", (int)$id_user);
        $app = $this->fetchALL($sql);
        return count($app);
    }

    public function searchUsersMineRecommneder($filters = array())
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array("u.id", "u.firstname", "u.lastname", "u.last_login",
                "(SELECT ja.status FROM job_applications ja WHERE ja.id_user = u.id) as status",
                "(SELECT ja.status_reason FROM job_applications ja WHERE ja.id_user = u.id) as status_reason",
            ))
            ->joinLeft(array("up" => "user_positions"), "u.id = up.id_user", array("up.title as position", "up.is_current as hired"))
            ->joinLeft(array("ue" => "user_education"), "u.id = ue.id_user", array("ue.title as school"))
            ->joinLeft(array("r" => "job_recommendations"), "r.id_user = u.id AND r.id_recommender = u.id_parent", array("r.date AS recommend_date"))
            ->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("uu.firstname AS recommended_firstname", "uu.lastname AS recommended_lastname"));
        $sql->joinleft(array("c" => "company_unblocked_cvs"), "c.id_user = u.id", array(""));
        $sql->group(["u.id", "u.firstname", "u.lastname", "up.title", "up.is_current", "ue.title", "r.date", "uu.firstname", "uu.lastname", "u.last_login"])
            ->where("u.status = 1")
            ->where("u.id != '{$this->_user->id}'")
            ->where("u.type = 1")
            ->order("r.date DESC");
        $sql->where("u.id_company = ?", $this->_user->id_company);
        if (Zend_Auth::getInstance()->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
        if (!empty($filters)) {
            if (isset($filters['id_skill'])) {
                $sql->join(array("us" => "user_skills"), "u.id = us.id_user");
                $sql->where("us.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
            }

            if (isset($filters['id_certification'])) {
                $sql->join(array("uc" => "user_certifications"), "u.id = uc.id_user");
                $sql->where("uc.id_certification IN (" . implode(",", $filters['id_certification']) . ")");
            }

            if (isset($filters['search']) && $filters['search'] != "") {
                $sql->where("u.firstname LIKE '%{$filters['search']}%' OR u.lastname LIKE '%{$filters['search']}%' OR u.title LIKE '%{$filters['search']}%' OR u.description LIKE '%{$filters['search']}%' OR ue.title LIKE '%{$filters['search']}%' OR ue.degree LIKE '%{$filters['search']}%' OR up.title LIKE '%{$filters['search']}%' OR up.summary LIKE '%{$filters['search']}%'");
            }
        }
        $result = $this->fetchAll($sql);
        return $result;
    }

    public function getUsersSkills()
    {
        return $this->_content->getUsersSkills();
    }

    public function getUsersCertifications()
    {
        return $this->_content->getUsersCertifications();
    }

//    public function getUserConnections($id = null) {
//        $sql = $this->select()
//                ->from(array("uc" => "user_connections"))
//                ->joinLeft(array("u" => "users"), "uc.linkedin_id = u.linkedin_id", array("firstname as user_first", "lastname as user_last", "title as heading"));
//        if (null !== $id) {
//            $sql->where("uc.id_user = ?", $id);
//        }
//
//        return $this->fetchAll($sql);
//    }
//    public function getLinkedinConnections($id = null, $params = array()) {
//
//        $sql = $this->select()
//                ->from(array("uc" => "user_connections"), array("*", "MATCH (title) AGAINST ('{$params['job_title']}' IN BOOLEAN MODE) AS relevance"));
//        if (null !== $id) {
//            $sql->where("uc.id_user = ?", $id);
//        }
//
//        if (isset($params['search'])) {
//            $sql->where("CONCAT(firstname, ' ', lastname) LIKE '%{$params['search']}%' OR CONCAT(lastname, ' ', firstname) LIKE '%{$params['search']}%' OR industry LIKE '%{$params['search']}%' OR location LIKE '%{$params['search']}%' OR title LIKE '%{$params['search']}%'");
//        }
//
//        if (isset($params['industry']) && $params['industry'] != '') {
//            //$sql->ORwhere('industry LIKE  ?', '%'.$params['industry'].'%');
//
//            foreach ($params['industry'] as $industry) {
//                $sql->ORwhere('industry LIKE  ?', '%' . $industry . '%');
//            }
//        }
//        if (isset($params['orderby']))
//            $sql->order("{$params['orderby']} {$params['order']}");
//
//
//        return $this->fetchAll($sql);
//    }

    public function getCompanyById($id)
    {
        return $this->_company->getCompanyById($id);
    }

    public function getCountries()
    {
        return $this->_content->getCountriesSelect();
    }

    public function getCustomCities()
    {
        $sql = $this->select()->from(array("c" => "custom_location"), array("id", "title"))->order("title ASC");


        $sql->where("c.id_user =?", (int)Zend_Auth::getInstance()->getIdentity()->id);

        return $this->fetchPairs($sql);
    }

    public function getCities()
    {
        return $this->_content->getCitiesSelect();
    }

    public function saveCompany($data)
    {
        return $this->_company->saveCompany($data);
    }

    public function getLatestJobPosts()
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getLatestJobPosts();
    }

    public function getUnconfirmedCompanies()
    {
        return $this->_company->getUnconfirmedCompanies();
    }

    public function getBountiesUnpaid()
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getBountiesUnpaid();
    }

    public function getPlatformUsers()
    {
        $sql = $this->select()->from(array("u" => "users"), array("COUNT(id) AS total"))->where("type = 1");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getTodayJobsApplications()
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getTodayJobsApplications();
    }

    public function getTodayJobsApplicationsDetailed($total)
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getTodayJobsApplicationsDetailed($total);
    }

    public function getCurrentJobStatusesGraph()
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getCurrentJobStatusesGraph();
    }

    public function getMonthJobsApplications()
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getMonthJobsApplications();
    }

    public function getCompaniesSelect()
    {
        return $this->_company->getCompaniesSelect();
    }

    public function getCompanies()
    {
        return $this->_company->getCompanies();
    }

    public function getCompaniesJobsCount($id = null, $filters = array())
    {
        return $this->_company->getCompanyJobsCount($id, $filters);
    }

    public function getCompaniesLastWeekApplications($id = null, $filters = array())
    {
        return $this->_company->getCompaniesLastWeekApplications($id, $filters);
    }

    public function getBountiesToApprove($id = null)
    {
        return $this->_company->getBountiesToApprove($id);
    }

//    public function getCompanyApplicationsVsReccomendations($id = null, $filters = array()) {
//        return $this->_company->getCompanyApplicationsVsReccomendations($id, $filters);
//    }
//    public function getCompanyApplicationsEvolution($id, $start, $end, $filters = array()) {
//        return $this->_company->getCompanyApplicationsEvolution($id, $start, $end, $filters);
//    }
//    public function getCompanySuccessRate($id = null) {
//        return $this->_company->getCompanySuccessRate($id);
//    }
//    public function export($data, $file) {
//        $fields = array_keys($data[0]);
//
//        $delimiter = ',';
//        $qualifier = '"';
//        $spacelines = "\r\n";
//
//        $contentArray = array();
//        foreach ($fields as $field) {
//            $contentArray[] = ucwords(str_replace("_", " ", $field));
//        }
//
//        $content[0] = implode(stripslashes($qualifier) . stripslashes($delimiter) . stripslashes($qualifier), $contentArray);
//        $content[0] = stripslashes($qualifier) . $content[0] . stripslashes($qualifier);
//        $content[0] = $content[0] . $spacelines;
//
//        $i = 1;
//        foreach ($data as $row) {
//            $contentArray = array();
//            foreach ($row as $field => $value) {
//                $contentArray[] = str_replace(array("'", '"'), " ", $value);
//            }
//            $content[$i] = implode(stripslashes($qualifier) . stripslashes($delimiter) . stripslashes($qualifier), $contentArray);
//            $content[$i] = stripslashes($qualifier) . $content[$i] . stripslashes($qualifier);
//            $content[$i] = $content[$i] . $spacelines;
//
//            $i++;
//        }
//
//        $content = implode($content);
//
//        $this->setCsvHeaders($file);
//        echo $content;
//        die();
//    }

    public function setCsvHeaders($excel_file_name = '')
    {
        header('Content-type: application/octet-stream; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $excel_file_name);
        header('Pragma: no-cache');
        header('Expires: 0');
        header('Cache-Control: no-store, no-cache');
        header('Content-Transfer-Encoding: binary');
    }

    public function activateCompany($id)
    {
        $this->update(array("status" => 1), "id = {$id}", "companies");

        $sql = $this->select()->from(array("c" => "company_users"))->where("id_company = ?", $id);
        $data = $this->fetchRow($sql);

        if (!empty($data)) {
            $this->update(array("type" => 2, "id_company" => $id, "is_company_admin" => 1), "id = {$data['id_user']}");
            $this->delete("id = {$data['id']}", "company_users");
        }
    }

    public function inactivateCompany($id)
    {
        $this->update(array("status" => 0), "id = {$id}", "companies");
    }

    public function getMyCVs()
    {
        $sql = $this->select()
            ->from(array("c" => "company_unblocked_cvs"), array("id_user", "id_company"))
            ->where("id_company = ?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function buyCv($id)
    {
        $company = $this->getCompanyById($this->_user->id_company);

        if ($company['search_credits'] < 1 || $company['status'] == 0) {
            return "error";
        }

        $credits = $company['search_credits'] - 1;

        $this->insert(array("id_user" => $id, "id_company" => $this->_user->id_company), "company_unblocked_cvs");
        $this->update(array("search_credits" => $credits), "id_company = '{$company['id']}'", "company_credits");
    }

    public function getBillingInfo()
    {
        $modelPayments = new models_Payments;
        return $modelPayments->getBillingInfo();
    }

    public function getBillingInfoById($id)
    {
        $modelPayments = new models_Payments;
        return $modelPayments->getBillingData($id);
    }

    public function setUserCertifications($certfications, $id_user = null)
    {
        $id_user = (null !== $this->_user->id) ? $this->_user->id : $id_user;
        $sql = $this->select()->from(array("uc" => "user_certifications"), array("id_linkedin", "id_user"))->where("id_linkedin IS NOT NULL")->where("id_user = ?", $id_user);
        $data = $this->fetchPairs($sql);

        $dbCertifications = $this->_content->getCertificationsSelect();

        if (!empty($certfications) && $certfications['_total'] > 0) {
            foreach ($certfications['values'] as $certification) {
                if (!array_key_exists($certification['id'], $data)) {
                    if (in_array($certification['name'], $dbCertifications)) {
                        $id_certification = array_search($certification['name'], $dbCertifications);
                    } else {
                        $id_certification = $this->_content->saveCertification(array("title" => $certification['name']));
                    }

                    $this->insert(array("id_user" => $id_user, "id_certification" => $id_certification, "id_linkedin" => $certification['id']), "user_certifications");
                }
            }
        }
    }

    public function setUserEducations($educations, $id_user = null)
    {
        $id_user = (null !== $this->_user->id) ? $this->_user->id : $id_user;
        $sql = $this->select()->from(array("ue" => "user_education"), array("id_linkedin", "id_user"))->where("id_linkedin IS NOT NULL")->where("id_user = ?", (int)$id_user);
        $data = $this->fetchPairs($sql);
        echo '<pre>';
        print_r($educations);
        die;
        if (!empty($educations) && $educations['_total'] > 0) {
            foreach ($educations['values'] as $education) {
                $update['start'] = $education['startDate']['year'];
                $update['end'] = $education['endDate']['year'];
                $update['title'] = $education['schoolName'];
                $update['field'] = $education['fieldOfStudy'];
                $update['activities'] = $education['activities'];
                $update['degree'] = $education['degree'];
                $update['id_user'] = $id_user;
                $update['id_linkedin'] = $education['id'];

                if (!array_key_exists($education['id'], $data)) {
                    $this->insert($update, "user_education");
                } else {
                    $this->update($update, "id_user = {$id_user} AND id_linkedin = '{$education['id']}'", "user_education");
                }
            }
        }
    }

    public function setUserPositions($positions, $id_user = null)
    {
        $id_user = (null !== $this->_user->id) ? $this->_user->id : $id_user;
        $sql = $this->select()->from(array("up" => "user_positions"), array("id_linkedin", "id_user"))->where("id_linkedin IS NOT NULL")->where("id_user = ?", $id_user);
        $data = $this->fetchPairs($sql);

        if (!empty($positions) && $positions['_total'] > 0) {
            foreach ($positions['values'] as $position) {
                $update['is_current'] = (int)$position['isCurrent'];
                $update['company'] = $position['company']['name'];
                $update['summary'] = $position['summary'];
                $update['title'] = $position['title'];
                $update['start'] = $position['startDate']['year'] . "-" . $position['startDate']['month'];
                $update['end'] = $position['endDate']['year'] . "-" . $position['startDate']['month'];
                $update['id_user'] = $id_user;
                $update['id_linkedin'] = $position['id'];

                if (!array_key_exists($position['id'], $data)) {
                    $this->insert($update, "user_positions");
                } else {
                    $this->update($update, "id_user = '{$id_user}' AND id_linkedin = '{$position['id']}'", "user_positions");
                }
            }
        }
    }

    public function setUserSkills($skills, $id_user = null)
    {
        $id_user = (null !== $this->_user->id) ? $this->_user->id : $id_user;
        $sql = $this->select()->from(array("us" => "user_skills"), array("id", "id_user"))->where("id_user = ?", $id_user);
        $data = $this->fetchPairs($sql);

        $dbSkills = $this->_content->getSkillsSelect();

        if (!empty($skills) && $skills['_total'] > 0) {
            foreach ($skills['values'] as $skill) {
                if (!array_key_exists($skill['id'], $data)) {
                    if (in_array($skill['skill']['name'], $dbSkills)) {
                        $id_skill = array_search($skill['skill']['name'], $dbSkills);
                    } else {
                        $id_skill = $this->_content->saveSkill(array("title" => $skill['skill']['name']));
                    }

                    $this->insert(array("id_user" => $id_user, "id_skill" => $id_skill, "id_linkedin" => $skill['id']), "user_skills");
                }
            }
        }
    }

    public function setUserConnections($connections)
    {
        if (!empty($connections) && $connections['_total'] > 0) {
            $this->delete("id_user = {$this->_user->id}", "user_connections");

            foreach ($connections['values'] as $connection) {
                $update['id_user'] = $this->_user->id;
                $update['firstname'] = $connection['firstName'];
                $update['lastname'] = $connection['lastName'];
                $update['location'] = $connection['location']['name'];
                $update['title'] = $connection['headline'];
                $update['linkedin_id'] = $connection['id'];
                $update['industry'] = $connection['industry'];
                $update['image'] = $connection['pictureUrl'];
                $update['profile_url'] = $connection['apiStandardProfileRequest']['url'];


                $this->insert($update, "user_connections");
            }
        }
    }

    public function getUserByToken($token)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("password_token = ?", $token);
        return $this->fetchRow($sql);
    }

    public function getCompanyRating($id)
    {
        return $this->_company->getCompanyRating($id);
    }

    public function updateApplicationStatus($id_application)
    {
        $sql = $this->select()->from(array("ja" => "job_applications"))->where("id = ?", (int)$id_application);
        $application = $this->fetchRow($sql);

        if ($application['status'] == 1 && isset($id_application)) {
            $this->update(array("status" => 2), "id = {$id_application}", "job_applications");
        }
    }

    public function exportCV($id)
    {
        $user = $this->getUserById($id);
        $positions = $this->getUserPositions($id);
        $skills = $this->getUserSkills($id);
        $education = $this->getUserEducation($id);
        $certifications = $this->getUserCertifications($id);

        $file = strtolower($user['firstname'] . "_" . $user['lastname'] . "_cv.pdf");

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/layouts/scripts/partials');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);

        $html = $partialHelper->partial("cv.phtml", array("user" => $user, "positions" => $positions, "skills" => $skills, "education" => $education, "certifications" => $certifications));

        include(ABSOLUTE_PATH . "/library/mpdf/mpdf.php");
        $mpdf = new mPDF('c', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 16, $mgb = 16, $mgh = 9, $mgf = 9, $orientation = 'L');
        $mpdf->WriteHTML($html);
        $mpdf->Output($file, 'D');
    }

    public function saveExportCV($id, $real_path)
    {
        $user = $this->getUserById($id);
        $positions = $this->getUserPositions($id);
        $skills = $this->getUserSkills($id);
        $education = $this->getUserEducation($id);
        $certifications = $this->getUserCertifications($id);

        $file = strtolower($user['firstname'] . "_" . $user['lastname'] . "_cv.pdf");

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/layouts/scripts/partials');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);

        $html = $partialHelper->partial("cv.phtml", array("user" => $user, "positions" => $positions, "skills" => $skills, "education" => $education, "certifications" => $certifications));

        require_once(ABSOLUTE_PATH . "/library/mpdf/mpdf.php");
        $mpdf = new mPDF('c', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 16, $mgb = 16, $mgh = 9, $mgf = 9, $orientation = 'L');
        $mpdf->WriteHTML($html);
        $saved = $real_path . "/" . $file;
        $mpdf->Output($saved, 'F');
        return $saved;;
    }

    public function getTooltipViews()
    {
        $sql = $this->select()->from(array("t" => "user_tooltip_view"), array("COUNT(id) as total"))->where("id_user = ?", $this->_user->id);
        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function authenticateByToken($token)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("SHA1(CONCAT(id,email,password)) = '{$token}'");
        $data = $this->fetchRow($sql);

        if (!empty($data)) {
            $userInfo = (object)$data;
            Zend_Auth::getInstance()->getStorage()->write($userInfo);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

            return $data;
        } else {
            return false;
        }
    }

    public function getUsersApplications($id = null, $start = null, $end = null, $filters = array())
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), array("COUNT(id_user) AS total"))
            ->join(array("u" => "users"), "ja.id_user = u.id", array("CONCAT(firstname, ' ', lastname) AS user"))
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
            ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title as job"))
            ->where("jl.id_lang = ?", $this->id_lang)
            ->group("ja.id_user")
            ->order("total DESC");
        if (null !== $id) {
            $sql->where("ja.id_user = ?", $id);
        }

        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'");
        }

        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'");
        }

        if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
            $sql->where("j.id_company = ?", Zend_Auth::getInstance()->getIdentity()->id_company);
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "referrals":
                            break;

                        case "user_status":
                            if ($value == 'viewed') {
                                $sql->where("ja.status = 1");
                            } else if ($value == 'accepted') {
                                $sql->where("ja.status = 4 AND ja.status_reason = 5");
                            } else if ($value == 'rejected') {
                                $sql->where("ja.status = 2");
                            }
                            break;
                    }
                }
            }
        }

        return $this->fetchAll($sql);
    }

    public function getJobCandidatesPerSource($filters = array())
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getJobCandidatesPerSource(null, $filters);
    }

//    public function getJobCandidatesPerStatus($filters = array()) {
//        $modelJobs = new models_Jobs;
//        return $modelJobs->getJobCandidatesPerStatus(null, $filters);
//    }

    public function getUsersBySkills($filters = array())
    {
        $sql = $this->select()
            ->from(array("u" => "users"))
            ->join(array("us" => "user_skills"), "u.id = us.id_user", array("COUNT(us.id_user) AS total"))
            ->join(array("s" => "skills"), "s.id = us.id_skill")
            ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title AS skill"))
            ->where("sl.title != ''")
            ->where("sl.id_lang = ?", $this->id_lang)
            ->group("s.id")
            ->order("total DESC");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "referrals":
                            break;

                        case "user_status":
                            $sql->join(array("ja" => "job_applications"), "ja.id_user = u.id", array());

                            if ($value == 'viewed') {
                                $sql->where("ja.status = 1");
                            } else if ($value == 'accepted') {
                                $sql->where("ja.status = 4 AND ja.status_reason = 5");
                            } else if ($value == 'rejected') {
                                $sql->where("ja.status = 2");
                            }
                            break;
                    }
                }
            }
        }
        $data = $this->fetchAll($sql);

        return array_slice($data, 0, 10);
    }

    public function getLastWeekUsers()
    {
        $sql = $this->select()->from(array("u" => "users"), array("COUNT(id) AS total"))->where("DATE_SUB(CURDATE(),INTERVAL 1 WEEK) <= updated");
        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getCandidatesSelect()
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array("id", "CONCAT(firstname, ' ', lastname) AS user"))
            ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array());

        if ($this->_user->type == 2) {
            $sql->join(array("j" => "jobs"), "ja.id_job = j.id", array())->where("j.id_company = ?", $this->_user->id_company);
        }

        return $this->fetchPairs($sql);
    }

    public function setCompanyPoster($id)
    {
        if ($_FILES['poster']['name'] != "") {
            move_uploaded_file($_FILES['poster']['name'], ABSOLUTE_PATH . "/public/resources/files/companies/" . $id . "_poster" . $_FILES['poster']['name']);
            return APPLICATION_URL . "/resources/files/companies/" . $id . "_poster" . $_FILES['poster']['name'];
        }
    }

    public function matchUserByEmail($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", (string)$email);
        $data = $this->fetchRow($sql);

        return (int)$data['id'];
    }

    public function generateTrackingNumber()
    {
        return uniqid();
    }

    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }

        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }

        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }

        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function getUsersWithScoring()
    {
        $sql = $this->select()
            ->from(array("g" => "gamification_points"), array("SUM(g.points) AS points", "g.id_user"))
            ->join(array("u" => "users"), "g.id_user = u.id", array("u.id", "u.firstname", "u.lastname", "u.image", "u.type"))
            ->group(["g.id_user", "u.id", "u.firstname", "u.lastname", "u.image", "u.type"]);
        return $this->fetchAll($sql);
    }

    public function getUserGamificationPoints($id)
    {
        $sql = $this->select()
            ->from(array("g" => "gamification_points"))
            ->where("id_user = ?", (int)$id);
        return $this->fetchAll($sql);
    }

    public function getUserGamificationPointsProfile($id)
    {
        $sql = $this->select()
            ->from(array("g" => "gamification_points"), array("action", "SUM(points) AS points"))
            ->where("id_user = ?", (int)$id)
            ->group("action");
        return $this->fetchPairs($sql);
    }

    public function getLeaderboards($id_company, $start, $end)
    {
        return $this->_company->getLeaderboards($id_company, $start, $end);
    }

    public function getLeaderboardsPagination($id_company, $start, $end, $length, $begin, $order_by, $order_value)
    {
        return $this->_company->getLeaderboardsPagination($id_company, $start, $end, $length, $begin, $order_by, $order_value);
    }

    public function getProfileLeaderboards($id_company, $start, $end)
    {
        return $this->_company->getProfileLeaderboards($id_company, $start, $end);
    }

    public function getOptimisedLeaderboards($id_company, $start = null, $end = null)
    {
        return $this->_company->getOptimisedLeaderboards($id_company, $start, $end);
    }

    public function getCompanyRatingDetails($id_company)
    {
        return $this->_company->getCompanyRatingDetails($id_company);
    }

    public function saveCompanyAlert($data)
    {
        if ($data['id'] == 1) {
            $this->update(array("applicants_alert" => $data['applicants_alert']), "id = {$this->_user->id_company}", "companies");
        } else if ($data['id'] == 2) {
            $this->update(array("applicants_status" => $data['applicants_status']), "id = {$this->_user->id_company}", "companies");
        }
    }

    public function getUserRecommendations()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), array("ja.date AS apply_date", "ja.updated as changed", "ja.status", "ja.status_reason"))
//                ->join(array("jr" => "job_recommendations"), "ja.id_user = jr.id_user and ja.id_recommender = jr.id_recommender")
            ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.email", "u.last_login", "u.email"))
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
            ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title AS job"))
            ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
            ->where("ja.id_recommender = ?", (int)$this->_user->id)
//                ->order("jr.date DESC")
//                ->group("jr.id_user")
            ->group(["ja.id_job", "ja.date", "ja.updated", "ja.status", "ja.status_reason", "u.firstname", "u.lastname", "u.email", "u.last_login", "u.email", "jl.title", "c.title"]);
        $data = $this->fetchAll($sql);


        $sql = $this->select()
            ->from(array("jr" => "job_recommendations"), array("j.id", "jr.id_recommender", "jr.id_user", "jr.date", "jr.track"))
            ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
            ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title AS job"))
            ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
            ->joinLeft(array("u" => "users"), "jr.id_user = u.id", array("u.firstname", "u.lastname", "u.last_login"))
            ->where("jr.id_recommender = ?", (int)$this->_user->id)
            ->where("jr.id_user NOT IN (SELECT id_user FROM job_applications WHERE id_recommender = '{$this->_user->id}')")
            ->order("jr.date DESC")
            ->group(["jr.track", "j.id", "jr.id_recommender", "jr.id_user", "jr.date", "jl.title", "c.title", "u.firstname", "u.lastname", "u.last_login"]);
        $none = $this->fetchAll($sql);
//        echo '<pre>';print_r($none);die;
        $result = array_merge($data, (array)$none);
        return $result;
    }

    public function getUserPendingRecommendations()
    {
        $sql = $this->select()
            ->from(array("jr" => "job_recommendations"), ["jr.date"])
            ->join(array("u" => "users"), "jr.id_user = u.id", array("u.id", "u.firstname", "u.lastname", "u.email"))
            ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
            ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title AS job"))
            ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
            ->where("jr.id_recommender = ?", (int)$this->_user->id)
            ->group(["u.id", "u.firstname", "u.lastname", "u.email", "jl.title", "c.title", "jr.date"])
            ->order("jr.date DESC");

        return $this->fetchAll($sql);
    }

    public function importUsersFromCsv($csv)
    {
        $validator = new Zend_Validate_EmailAddress();
        $modelCompany = new models_Company;
        $model = new models_Content();
        $deps = $model->getDepartmentsSelect() + $model->getCustomDepartmentsSelect();
        $types = $this->_settings['user_types'];
        array_walk_recursive($types, function (&$value, $key) {
            return $value = strtolower($value);
        });

        $success=0;
        $errors=0;
        $existing=0;


        foreach ($csv as $line) {
            $user['firstname'] = $line[0];
            $user['lastname'] = $line[1];
            $user['email'] = $line[2];
            $user['phone'] = $line[3];
//            $user['address'] = $line[4];
//            $user['availability'] = $line[5];
//            $user['location'] = $line[6];
//            $user['gender'] = $line[7];
//            $user['title'] = $line[9];
//            echo '<pre>',print_r($user,1),'</pre>';die;
            if ($this->isNotUser($user['email'])) {
                if ($validator->isValid($user['email'])) {
                    $password = $this->generateStrongPassword();
                    $user['salt'] = uniqid(mt_rand(), false);
                    $user['password'] = sha1($password) . $user['salt'];
                    $user['type'] = array_search(trim(strtolower($line[4])), $types);
                    $user['status'] = 1;
                    $user['is_imported'] = 1;
                    $user['created'] = date("Y-m-d H:i:s");

                    if ($this->_user->type == 2) {
                        $user['id_company'] = (int)$this->_user->id_company;
                    } else {
                        $company = $modelCompany->getCompanyByName($line[8]);
                        if ($company['id'] > 0) {
                            $user['id_company'] = (int)$company['id'];
                        } else {
                            $user['id_company'] = (int)$this->_user->id_company;
                        }
                    }
                    array_walk_recursive($deps, function (&$value, $key) {
                        return $value = strtolower(trim($value));
                    });
                    $this->insert($user, "users");
                    $id = $this->lastInsertId();
//                    echo '<pre>',print_r($deps,1),'</pre>';die;
                    $id_department = array_search(strtolower($line[5]), (array)$deps);
                    if (!$id_department) {
                        $newDepratment['id_company'] = $user['id_company'];
                        $newDepratment['title'] = $line[5];
                        $newDepratment['slug'] = strtolower(str_replace(" ", "-", $line[5]));
                        $id_department = $model->setCustomDepartment($newDepratment);
                    }

                    $this->_stats->setGamificationPoint($this->_user->id, "upload_cv");
                    $this->_stats->setNotification($id, "user", "profile", "Welcome to TalentSpotting! <br>Build your talent stream now. Share jobs.");
                    $model->setUserDepartment($id, $id_department);

                    $userObject = Domain_User_UserFactory::createInstance($id);
                    $userObject->setPassword($password);

//                    echo '<pre>',print_r($userObject,1),'</pre>';die;
                    $userObject->sendWelcomeEmail();

//                    $transport = null;
//                    if (!empty($this->_settings['email_smtp'])) {
//                        $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//                        $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//                    }
//
//                    $contentModel = new models_Content;
//                    $email = $contentModel->getEmailByType(27);
//                    $smarty = new Smarty;
//                    $smarty->assign("user", $user);
//                    $smarty->assign("company", $this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['title']);
//                    $smarty->assign("password", array("password" => $password));
//                    $smarty->assign("settings", array("sitename" => $this->_settings['sitename']));
//                    $smarty->assign('subdomain_url', $this->_settings['sitename']);
//
//                    $emailBody = $smarty->fetch('eval:' . $email['content']);
//                    $emailSubject = $smarty->fetch('eval:' . $email['subject']);
//
//                    $mail = new WebApp_Mail();
//                    $mail->addTo($user['email'], $user['firstname']);
//                    $mail->setFrom($this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
//                    $mail->setSubject($emailSubject);
//
//
//                    $company = new models_Company;
//                    $template = $company->getCompanyEmailTemplate($this->_settings['sitename']);
//                    $smarty->assign('subject', $emailSubject);
//                    $smarty->assign('content', $emailBody);
//                    $content = $smarty->fetch('eval:' . $template);
//                    $mail->setBodyHtml($content);
//                    $mail->send($transport);


//                    $position['company'] = $line[8];
//                    $position['title'] = $line[9];
//                    $position['summary'] = $line[10];
//                    $position['start'] = date("Y-m-d", strtotime($line[11]));
//                    $position['is_current'] = 1;
//                    $position['id_user'] = $id;
//
//                    $this->insert($position, "user_positions");
//
//                    $skills = explode(";", $line[12]);
//                    $this->importSkills($id, $skills);

                    $success += 1;
                } else {
                    $errors += 1;
                }
            } else {
                $existing += 1;
            }
        }

        $message = "Success imports: " . (int)$success;
        $message .= "<br>Existing users: " . (int)$existing;
        $message .= "<br>Import errors: " . (int)$errors;

        return $message;
    }

    public function importUsersFromCsvOnPilote($csv)
    {
        $remained_user = $this->getRemainedPiloteUSer();
        if ($remained_user < 50) {
            $allowed_add = 50 - $remained_user;
            $csv = array_splice($csv, 0, $allowed_add + 1);
            $this->importUsersFromCsv($csv);
        } else {
            return "Pilote Allow just 50 Recommenders";
        }
    }

    public function getRemainedPiloteUSer($id)
    {
        $sql = $this->select()->from(array("u" => "users"), array("COUNT(id) AS users"))->where("u.id_company =?", (int)$id);
        return $this->fetchRow($sql)['users'];
    }

    public function isNotUser($email)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("email = ?", $email);
        $data = $this->fetchRow($sql);

        return empty($data);
    }

    public function importSkills($id, $skills)
    {
        if (!empty($skills) && $skills[0] != "") {
            $dbSkills = $this->_content->getSkillsSelect();

            foreach ($skills as $skill) {
                if (in_array($skill, $dbSkills)) {
                    $id_skill = array_search($skill, $dbSkills);
                } else {
                    $id_skill = $this->_content->saveSkill(array("title" => $skill));
                }

                $this->insert(array("id_user" => $id, "id_skill" => $id_skill), "user_skills");
            }
        }
    }

    public function importejobs($url)
    {
        $data = file_get_contents($url);

        $departments = $this->_parser->getDepartmentsEjobs("table tr td table td p", $data);
        $industries = $this->_parser->getIndustriesEjobs("table tr td table td p", $data);
        $location = $this->_parser->getLocationEjobs("table tr td table td p", $data);
        $availability = $this->_parser->getAvailabilityEjobs("table tr td table td p", $data);
        $experience = $this->_parser->getExperienceEjobs("table tr td table tr td[@rowspan='3']", $data);

        if ($location != "" && $this->_user->location == "")
            $user['location'] = trim($location);

        if ($availability != "" && $this->_user->availability == "")
            $user['availability'] = trim($availability);

        if (!empty($user)) {
            $user['id'] = $this->_user->id;
            $this->saveUser($user);
            $this->reauthenticate($this->_user->email);
        }

        $this->updateExperience($experience);
    }

    public function importceevee($url)
    {
        $data = file_get_contents($url);

        $departments = $this->_parser->getDepartmentsBestjobs("div.cv-section-content > div.obi", $data);
        $location = $this->_parser->getLocationBestjobs("div.cv-section-content > div.obi", $data);
        $skills = $this->_parser->getSkillsBestjobs("div.cv-card-inner", $data);
        $image = $this->_parser->getPhotoBestjobs("a#big-pic", $data);
        $education = $this->_parser->getEducationBestjobs("div.stud", $data);
        $experience = $this->_parser->getExperienceBestjobs("div.exp", $data);

        if ($location != "" && $this->_user->location == "")
            $user['location'] = trim($location);

        if ($image != "" && $this->_user->image == "")
            $user['image'] = trim($image);

        if (!empty($user)) {
            $user['id'] = $this->_user->id;
            $this->saveUser($user);
            $this->reauthenticate($this->_user->email);
        }
    }

    public function updateExperience($experience)
    {
        $dbExperience = $this->getUserPositions();

        foreach ((array)$experience as $key => $position) {
            foreach ($dbExperience as $exp) {
                if ($exp['title'] == $position['title'] && $exp['company'] == $position['company']) {
                    unset($experience[$key]);
                }
            }
        }

        foreach ((array)$experience as $data) {
            $this->insert($data, "user_positions");
        }
    }

    public function getUserScoringPoints($id_user = null)
    {
        $id = (null !== $id_user) ? $id_user : $this->_user->id;

        $sql = $this->select()->from(array("g" => "gamification_points"), array("SUM(points) AS points"))->where("id_user = ?", (int)$id);
        $data = $this->fetchRow($sql);
        return (int)$data['points'];
    }

    public function getUserLeaderBoardsProfilePoints($id_user = null)
    {
        $id = (null !== $id_user) ? $id_user : $this->_user->id;
        $sql = $this->select()
            ->from(array("gad" => "campaign_actions_detailed"), array("SUM(value) AS points"))
            ->where("id_recommender = ?", (int)$id);
        $data = $this->fetchRow($sql)['points'];
        return $data;
    }

    public function getSalaryComparison($data)
    {
        return "none";
    }

    public function getMyHobbies()
    {
        $sql = $this->select()->from(array("uh" => "user_hobbies"), array("id_hobby", "id_user"))->where("id_user = ?", $this->_user->id);
        return $this->fetchPairs($sql);
    }

    public function getProfileHobbies()
    {
        $sql = $this->select()
            ->from(array("h" => "hobbies"))
            ->join(array("uh" => "user_hobbies"), "h.id = uh.id_hobby", array())
            ->where("uh.id_user = ?", (int)$this->_user->id);
        return $this->fetchAll($sql);
    }

    public function saveHobby($data)
    {
        $modelContent = new models_Content;
        $hobbies = explode(",", $data['id_hobby']);

        $allHobbies = $modelContent->getHobbiesSelect();
        $myHobbies = $this->getMyHobbies();

        foreach ((array)$hobbies as $hobby) {
            if (in_array($hobby, $allHobbies)) {
                $id = array_search($hobby, $allHobbies);
                if (!array_key_exists($id, $myHobbies)) {
                    $this->insert(array("id_user" => $this->_user->id, "id_hobby" => $id), "user_hobbies");
                }
            } else {
                $this->insert(array("title" => $hobby), "hobbies");
                $id = $this->lastInsertId();
                $this->insert(array("id_user" => $this->_user->id, "id_hobby" => $id), "user_hobbies");
            }
        }
    }

    public function getUserByHash($hash)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("hash = ?", (string)$hash)->where("LENGTH(hash) > 1");
        return $this->fetchRow($sql);
    }

    public function getReferees()
    {
        $sql = $this->select()
            ->from(array("jr" => "job_recommendations"), array("jr.id_recommender"))
            ->join(array("u" => "users"), "jr.id_recommender = u.id", array())
            ->group("jr.id_recommender");

        if ($this->_user->type == 2) {
            $sql->join(array("j" => "jobs"), "jr.id_job = j.id", array());
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }
        //die ($sql->__toString());

        $data = $this->fetchAll($sql);

        return count($data);
    }

    public function getReferrals()
    {
        $sql = $this->select()
            ->from(array("jr" => "job_recommendations"), array("COUNT(jr.id) AS total"));

        if ($this->_user->type == 2) {
            $sql->join(array("j" => "jobs"), "jr.id_job = j.id", array());
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getReferralApplies()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), array("COUNT(ja.id) AS total"))->where("id_recommender > 0");

        if ($this->_user->type == 2) {
            $sql->join(array("j" => "jobs"), "ja.id_job = j.id", array());
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getInternalApplies()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), array("COUNT(ja.id) AS total"))
            ->join(array("u" => "users"), "ja.id_user = u.id", array())
            ->where("id_recommender > 0");

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }


        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getJobsNumber($filters = array())
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array())
            ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("status", "COUNT(jd.id_department) AS total"))
            ->join(array("jd" => "job_departments"), "ja.id_job = jd.id_job", array("id_department"))
            ->join(array("d" => "departments"), "d.id = jd.id_department", array())
            ->join(array("dl" => "departments_lang"), "d.id = dl.id_department", array("title"))
            ->where("dl.id_lang = ?", $this->id_lang)
            ->group("ja.status")
            ->group("d.id")
            ->where("ja.status < 2");

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "id_department":
                            $sql->where("jd.id_department = {$value}");
                            break;

                        case "start_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.start >= '{$date}'");
                            break;

                        case "end_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.end <= '{$date}'");
                            break;

                        case "status":
                            if ($value == 0) {
                                $sql->where("j.end < '" . date("Y-m-d") . "' OR j.status = 0");
                            } else if ($value == 1) {
                                $sql->where("j.end >= '" . date("Y-m-d") . "' AND j.status = 1");
                            }
                            break;
                    }
                }
            }
        }

        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            $stats[$row['id_department']]['title'] = $row['title'];
            if ($row['status'] == 1) {
                $stats[$row['id_department']]['viewed'] = $row['total'];
            } else {
                $stats[$row['id_department']]['new'] = $row['total'];
            }
        }

        return (array)$stats;
    }

    public function getCompanyOverview($filters = array())
    {
//        $visitors = $this->_getCompanyVisitors($filters);
        $visitors = $this->getComapanyAllViews();
        $applicants = $this->_getCompanyApplicants($filters);
        $hires = $this->_getCompanyHires($filters);

        $stats[] = array("name" => 'Visitors', "value" => $visitors);
        $stats[] = array("name" => 'Applicants', "value" => $applicants);
        $stats[] = array("name" => 'Hires', "value" => $hires);

        return $stats;
    }

    public function getComapanyAllViews()
    {
        $model_report = new models_Reports;
        $data = $model_report->getDashboardTopChannels();
        return ($data['facebook']['views'] + $data['linkedin']['views'] + $data['website']['views']);
    }

    /**
     * @param $data
     * @param $send_mail
     * @param $id_email
     * @param $id
     * @return mixed
     * @throws Zend_Mail_Exception
     */
    public function assignSendData($data, $send_mail, $id_email, $id)
    {
//            $this->_stats->setGamificationPoint($id, "register");
        $this->_stats->setNotification($id, "user", "jobs", "Welcome to WorkinAfrica! <br>Start finding your dream job.");
        $this->_stats->setNotification($id, "user", "profile", "Employers will find you faster if you complete your profile! <br>Add more info now!");
        $companie_model = new models_Company;
        if ($send_mail === true) {
            $socialModel = new models_Social;
//                $emailContent = $socialModel->getEmailByType($id_email);
            $data['company_title'] = $companie_model->getCompanyByUserId($id)['title'];

            $transport = null;
            if (!empty($this->_settings['email_smtp'])) {
                $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }
            $contentModel = new models_Content;
            $email = $contentModel->getEmailByType($id_email);

            $me = $this->getUserById($id);
            $link = $this->_settings['sitename'] . '/activate?token=' . sha1($me['email']);

            require('../../library/smarty/Smarty.class.php');
            $smarty = new Smarty;
            $smarty->assign("link", $link);
            $smarty->assign("user", $_POST);
            $smarty->assign("password", $_POST);
            $smarty->assign("settings", $this->_settings);
            $smarty->assign("company", $companie_model->getCompanyById($_POST['id_company']));
            $emailBody = $smarty->fetch('eval:' . $email['content']);
            $emailSubject = $smarty->fetch('eval:' . $email['subject']);
//                echo '<pre>';print_r($data);die;
            $mail = new WebApp_Mail();
            $mail->addTo($_POST['email'], $_POST['firstname']);
            $mail->setFrom($this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['email'], $this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['title']);
            $mail->setSubject($emailSubject);
            $mail->setBodyHtml($emailBody);
            $mail->send($transport);

//                $me = $this->getUserById($id);
//                $link = $this->_settings['sitename'] . '/activate?token=' . sha1($me['email']);
//                $template = "To activate your account please click here <br /><br /> <a href='{$link}'>{$link}</a> ";
//                $mail = new WebApp_Mail();
//                $mail->addTo($_POST['email'], $_POST['firstname']);
//                $mail->setFrom($this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['email'], $this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['title']);
//                $mail->setSubject("Activate Account");
//                $mail->setBodyHtml($template);
//                $mail->send($transport);
        }
        return $data;
    }

    private function _getCompanyVisitors($filters)
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array("id"))
            ->joinLeft(array("jv" => "job_views"), "j.id = jv.id_job", array("COUNT(jv.id) AS total"));

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "id_department":
                            $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array(""));
                            $sql->where("jd.id_department = {$value}");
                            break;

                        case "start_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.start >= '{$date}'");
                            break;

                        case "end_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.end <= '{$date}'");
                            break;

                        case "status":
                            if ($value == 0) {
                                $sql->where("j.end < '" . date("Y-m-d") . "' OR j.status = 0");
                            } else if ($value == 1) {
                                $sql->where("j.end >= '" . date("Y-m-d") . "' AND j.status = 1");
                            }
                            break;
                    }
                }
            }
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    private function _getCompanyApplicants($filters)
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array("j.id"))
            ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("COUNT(ja.id) AS total"))
            ->group("j.id");

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }


        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "id_department":
                            $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array(""));
                            $sql->where("jd.id_department = {$value}");
                            break;

                        case "start_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.start >= '{$date}'");
                            break;

                        case "end_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.end <= '{$date}'");
                            break;

                        case "status":
                            if ($value == 0) {
                                $sql->where("j.end < '" . date("Y-m-d") . "' OR j.status = 0");
                            } else if ($value == 1) {
                                $sql->where("j.end >= '" . date("Y-m-d") . "' AND j.status = 1");
                            }
                            break;
                    }
                }
            }
        }
        $total = 0;
        $data = $this->fetchAll($sql);
        foreach ($data as $key => $value) {
            $total += $value['total'];
        }
        return (int)$total;
    }

    private function _getCompanyHires($filters)
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array("j.id"))
            ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("COUNT(ja.id) AS total"))
            ->where("ja.status = 5")
            ->group("j.id");

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "id_department":
                            $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array(""));
                            $sql->where("jd.id_department = {$value}");
                            break;

                        case "start_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.start >= '{$date}'");
                            break;

                        case "end_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.end <= '{$date}'");
                            break;

                        case "status":
                            if ($value == 0) {
                                $sql->where("j.end < '" . date("Y-m-d") . "' OR j.status = 0");
                            } else if ($value == 1) {
                                $sql->where("j.end >= '" . date("Y-m-d") . "' AND j.status = 1");
                            }
                            break;
                    }
                }
            }
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getPaidBounties($id = null)
    {
        $sql = $this->select()->from(array("b" => "bounties"), array("COUNT(id) as total"))->where("status = 'paid'")->where("id_job IN (SELECT id FROM jobs)");

        if ($this->_user->type == 2) {
            $sql->where("id_company = '{$this->_user->id_company}'");
        }

        if (null !== $id) {
            $sql->where("id_company = '{$id}'");
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getCampaignPaidBounties($id = null)
    {
        $sql = $this->select()->from(array("cb" => "campaign_bounty"), array("COUNT(id) as total"))->where("status = 'paid'");

        if ($this->_user->type == 2) {
            $sql->where("id_company = '{$this->_user->id_company}'");
        }

        if (null !== $id) {
            $sql->where("id_company = '{$id}'");
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getCampaignToPaidBounties($id = null)
    {
        $sql = $this->select()->from(array("cb" => "campaign_bounty"), array("COUNT(id) as total"))->where("status = 'initiated'");

        if ($this->_user->type == 2) {
            $sql->where("id_company = '{$this->_user->id_company}'");
        }

        if (null !== $id) {
            $sql->where("id_company = '{$id}'");
        }

        $data = $this->fetchRow($sql);

        return (int)$data['total'];
    }

    public function getApplicationsCount($id = null, $filters = array())
    {
        $last_date = date("Y-m-d", strtotime("-2 months")) . " 00:00:00";
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), array("id_recommender", "date"))
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
            ->where("ja.date >= '{$last_date}'");
        if (null !== $id) {
            $sql->where("ja.id_user = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "referrals":
                            break;

                        case "user_status":
                            if ($value == 'viewed') {
                                $sql->where("ja.status = 1");
                            } else if ($value == 'accepted') {
                                $sql->where("ja.status = 4 AND ja.status_reason = 5");
                            } else if ($value == 'rejected') {
                                $sql->where("ja.status = 2");
                            }
                            break;
                    }
                }
            }
        }

        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            if (date("Y-m-d", strtotime($row['date'])) == date("Y-m-d", strtotime("-1 day"))) {
                $applications['last_day'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_day'] += 1;
                }
            }

            if (date("Y-m-d", strtotime($row['date'])) == date("Y-m-d", strtotime("-2 day"))) {
                $applications['last_2day'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_2day'] += 1;
                }
            }

            if (date("W", strtotime($row['date'])) == date("W", strtotime("-1 week"))) {
                $applications['last_week'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_week'] += 1;
                }
            }

            if (date("W", strtotime($row['date'])) == date("W", strtotime("-2 week"))) {
                $applications['last_2week'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_2week'] += 1;
                }
            }

            if (date("Y-m", strtotime($row['date'])) == date("Y-m", strtotime("-1 month"))) {
                $applications['last_month'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_month'] += 1;
                }
            }

            if (date("Y-m", strtotime($row['date'])) == date("Y-m", strtotime("-2 months"))) {
                $applications['last_2month'] += 1;
                if ($row['id_recommender'] > 0) {
                    $referrals['last_2month'] += 1;
                }
            }
        }

        return array("applications" => $applications, "referrals" => $referrals);
    }

    public function getCompanyRecommenders()
    {
        $sql = $this->select()
            ->join(array("u" => "users"), "jr.id_recommender = u.id", array("id", "CONCAT(firstname, ' ', lastname) AS name"))
            ->where("u.id_company = ?", (int)$this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getNotAppliedJobs()
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array("*", "(SELECT SUM(shares) FROM jobs_share WHERE id_job = j.id) AS shares_sum",
                "(SELECT COUNT(id) FROM job_recommendations jr WHERE jr.id_job = j.id) AS total_shares"
            ))
            ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title", "slug"))
            ->where("jl.id_lang = ?", $this->id_lang)
            ->where("j.id_company = ?", (int)$this->_user->id_company)
            ->where("j.id NOT IN (SELECT id_job FROM job_applications WHERE id_user = '{$this->_user->id}')")
            ->order("total_shares DESC")
            ->order("shares_sum DESC")
            ->limit(4);
        return $this->fetchAll($sql);
    }

    public function getUploadedCVs()
    {
        $sql = $this->select()
            ->from(array("u" => "user_cv"))
            ->where("id_user = ?", (int)$this->_user->id)
            ->order("id DESC");
        return $this->fetchAll($sql);
    }

    public function getUploadedCvById($id)
    {
        $sql = $this->select()
            ->from(array("u" => "user_cv"))
            ->where("id_user = ?", (int)$id);
        return $this->fetchAll($sql);
    }

    public function uploadCv()
    {
        $file = $_FILES['cv'];

        $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
        $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

        $file_name = $file['name'];
        $file_down = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;
        move_uploaded_file($file['tmp_name'], APPLICATION_PATH . "/storage/cv/{$file_down}");
        $this->insert(array("id_user" => $this->_user->id, "title" => $file['name'], "file" => $file_down, "date" => date("Y-m-d H:i:s")), "user_cv");
    }

    public function renameUploadedFile($file)
    {
        $extension = substr($file, strrpos($file, ".") + 1);
        $file_no_ext = substr($file, 0, strrpos($file, "."));
        $file_down = md5(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;
        return $file_down;
    }

    public function uploadCvNoId()
    {
        if ($_FILES['doc']) {
            $file = $_FILES["doc"];
            $date = date('d-m-Y H:m');

//        echo $file.'doc file'.$_FILES["doc"]["tmp_name"];
            $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
            $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

            $file_name = $file_no_ext . '_' . $date . "." . $extension;
            $file_down = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;

            move_uploaded_file($file['tmp_name'], APPLICATION_PATH . "/storage/cv/{$file_down}");
//            move_uploaded_file($file['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/cv/{$file_down}");

            return ['name' => $file['name'], 'hash' => strtolower($file_down)];
//        $this->insert(array("id_user" => $this->_user->id, "title" => $file['name'], "file" => $file_down, "date" => date("Y-m-d H:i:s")), "user_cv");
        }
    }

    public function uploadCvById($id)
    {
        if (!empty($_FILES)) {
            $file = $_FILES['file'];

            $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
            $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

            $file_name = $file['name'];
            $file_down = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;

            move_uploaded_file($file['tmp_name'], APPLICATION_PATH . "/storage/cv/{$file_down}");
            $this->insert(array("id_user" => $id, "title" => $file['name'], "file" => $file_down, "date" => date("Y-m-d H:i:s")), "user_cv");
        }
    }

    public function getReccomendedApplicationViews()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), ["ja.id_user", "ja.id_job"])
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array("j.id_company", "j.start", "j.end"))
            ->joinLeft(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title", "jl.description"))
            ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.email"))
            ->where("ja.id_recommender = ?", (int)$this->_user->id)
            ->where("ja.status = 2")
            ->group(["ja.id_user", "ja.id_job", "j.id_company", "j.start", "j.end", "jl.title", "jl.description", "u.firstname", "u.lastname", "u.email"]);
        $sql = $this->select()
            ->from(['jr' => 'job_recommendations'], ['viewed', 'id'])
            ->where("jr.id_recommender = ?", (int)$this->_user->id)
            ->where('jr.viewed = 1');
        return $this->fetchAll($sql);
    }

    public function getHiredRecommendations()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"), ["ja.id_user"])
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array("j.id_company", "j.start", "j.end"))
            ->joinLeft(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title", "jl.description"))
            ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.email"))
            ->where("ja.id_recommender = ?", $this->_user->id)
            ->where("ja.status = 5")
            ->group(["ja.id_user", "j.id_company", "j.start", "j.end", "jl.title", "jl.description", "u.firstname", "u.lastname", "u.email"]);
        return $this->fetchAll($sql);
    }

    public function getApplicationViews()
    {
        $sql = $this->select()
            ->from(array("ja" => "job_applications"))
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array("id_company", "start", "end"))
            ->joinLeft(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title", "description"))
            ->join(array("u" => "users"), "ja.id_user = u.id", array("firstname", "lastname", "email"))
            ->where("ja.id_user = ?", $this->_user->id)
            ->where("ja.status = 2");
        return $this->fetchAll($sql);
    }

    public function getUserActiveCampaigns()
    {
        $sql = $this->select()
            ->from(array("c" => "campaigns"))
            ->join(array("cr" => "campaign_reports_detailed"), "c.id = cr.id_campaign", array("action", "value", "date"))
            ->where("cr.id_user = ?", $this->_user->id);

        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            $campaign[$row['id']]['id'] = $row['id'];
            $campaign[$row['id']]['title'] = $row['title'];
            $campaign[$row['id']]['duration'] = ['duration'];
            $campaign[$row['id']]['bounty'] = $row['bounty'];
            $campaign[$row['id']]['reward'] = $row['reward'];
            $campaign[$row['id']]['message'] = $row['message'];
            $campaign[$row['id']]['start'] = $row['start'];
            $campaign[$row['id']]['end'] = $row['end'];
            $campaign[$row['id']]['actions'][] = array("action" => $row['action'], "value" => $row['value'], "date" => $row['date']);

            if ($row['action'] == 'share' || $row['action'] == 'recommendation') {
                $campaign[$row['id']]['virality'] += $row['value'];
            }
        }

        return (array)$campaign;
    }

    public function getCampaignLeaderboards($id_campaign)
    {
        $modelsCampaign = new models_Campaign;
        return $modelsCampaign->getLeaderboards((int)$id_campaign);
    }

    public function removeUserCvByID($id, $cv_id)
    {
        $sql = $this->delete('user_cv', array(
            'id =' . $cv_id,
            'user_id =' . $id,
        ));
    }

    public function getUSerSkillBars()
    {
        $sql = $this->select()
            ->from(array("j" => "jobs"), array("*"))
            ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("status", "status_reason", "date", "id_user"));

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }

        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            $users[] = $row['id_user'];
        }

        if (!empty($users)) {
            $ids = array_unique($users);

            $sql = $this->select()
                ->from(array("us" => "user_skills"), array("us.id_user", "COUNT(us.id_skill) AS total"))
                ->join(array("s" => "skills"), "us.id_skill = s.id", array())
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("sl.title AS skill"))
                ->where("us.id_user IN (" . implode(",", $ids) . ")")
                ->group(["us.id_skill", "us.id_user", "sl.title"])
                ->order("total DESC");
            $data = $this->fetchAll($sql);

            return array_slice($data, 0, 10);
        } else {
            return array();
        }
    }

    public function getJobClosingRate()
    {
        return $this->_stats->getJobClosingRate();
    }

    public function getCandidatesByStatus()
    {
        return $this->_stats->getCandidatesByStatus();
    }

    public function removeUserCvByIDs($user_id, $cv_id)
    {

        $sql = $this->delete("id = {$cv_id}", "user_cv");
        return $sql;
    }

    public function changeStatus($id, $status, $status_reason = 0, $send_admin_hired_email = 0, $send_recommender_hired_email = 0)
    {

        $modelJobs = new models_Jobs;
//        $status_array = explode(".", $status);
        $app_data['status'] = $status;
        $app_data['status_reason'] = $status_reason;
        $app_data['modified_by'] = (int)$this->_user->id;
        $this->update($app_data, "id = '{$id}'", "job_applications");
        $sql = $this->select()->from(array("ja" => "job_applications"))->where("id = ?", (int)$id);
        $application = $this->fetchRow($sql);

        if ($status == 5) {
            if ($application['id_recommender'] > 0) {
                // Setting points if action is setted to succefful hire
                $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("*"))->where("cj.id_job =?", (int)$application['id_job']);
                $jobResult = $this->fetchRow($sql);
//                    if (!empty($jobResult)) {
//                        $this->_stats->setCampaignReport($application['id_recommender'], "successful_hire", $application['id_job'], $application['id_user']);
//                    } else {
//                        $this->_stats->setGamificationPoint($application['id_recommender'], "successful_hire", $application['id_job']);
//                    }

                if ($send_recommender_hired_email) {
                    $this->successuflRecommendation($application['id_recommender'], $id);
                }

                if ($send_admin_hired_email) {
                    $this->successuflRecommendationToAdmin($application['id_recommender'], $id);
                }
            }
            $sql__ = $this->select()->from(['j' => "jobs"], ['*'])->where('j.id =?', (int)$application['id_job']);
            $job_due_date = $this->fetchRow($sql__)['job_ad_lifetime'];

            $bounty_due_date = date("Y-m-d H:i:s", strtotime("+$job_due_date day"));

            $user = ($application['id_recommender'] > 0) ? $application['id_recommender'] : $application['id_user'];
            $this->insert(array("id_job" => $application['id_job'], "id_company" => $this->_user->id_company, "id_user" => $user, 'status' => 'initiated', 'date' => date("Y-m-d H:i:s"), 'due_date' => $bounty_due_date), "bounties");
        }
        $application = $this->fetchRow($sql);
        $applicant = $this->getUserById($application['id_user']);
        return $application['id_user'];
    }

    public function successuflRecommendation($id, $application_id)
    {
        $JobsModel = new models_Jobs;
        $sql = $this->select()->from(array("u" => "users"), array("*"))->where("u.id =?", (int)$id);
        $result = $this->fetchRow($sql);
        $company_name = $this->_company->getCompanyById($result['id_company'])['title'];
        $job_app_id = $JobsModel->getJobApplicationById($application_id)['id_job'];
        $job = $JobsModel->getJobById($job_app_id);

        $id_candidate = $JobsModel->getJobApplicationById($application_id)['id_user'];
        $sql = $this->select()->from(array("u" => "users"), array("*"))->where("u.id =?", (int)$id_candidate);
        $candidate = $this->fetchRow($sql);

        $LoggedUser = Domain_User_UserFactory::createInstance($this->_user->id);
        $LoggedUser->sendSuccesfulEmailToRecommenderForHiredCandidate($result, $candidate, $job, $company_name);
    }

    public function successuflRecommendationToAdmin($id, $application_id)
    {
        $JobsModel = new models_Jobs;
        $sql = $this->select()->from(array("u" => "users"), array("*"))->where("u.id =?", (int)$id);
        $result = $this->fetchRow($sql);
        $job_app_id = $JobsModel->getJobApplicationById($application_id)['id_job'];
        $job = $JobsModel->getJobById($job_app_id);

        $id_candidate = $JobsModel->getJobApplicationById($application_id)['id_user'];
        $sql = $this->select()->from(array("u" => "users"), array("*"))->where("u.id =?", (int)$id_candidate);
        $candidate = $this->fetchRow($sql);

        $LoggedUser = Domain_User_UserFactory::createInstance($this->_user->id);
        $admins = $this->getAllAdminsCompany();
        foreach ($admins as $key => $admin) {
            $LoggedUser->notifyAdminsForSuccesfullHiring($admin, $result, $candidate, $job);
        }
    }

    public function getApplicationById($id)
    {
        $sql = $this->select()->from(array("j" => "job_applications"))->where("id = ?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function getJobLeaderboards($id)
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.firstname', 'u.lastname', 'u.image',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status = 5) AS success',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status = 11) AS fail',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status NOT IN (2,5)) AS pending',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id) AS application_on_reccomend',
                '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND g.id_item =' . $id . ' GROUP BY g.id_user) AS total_points',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id) AS number_recommendation',
                '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source != \'\') AS total_views',
                '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY d.id_user) AS real_views',
            ))
            ->join(array("jr" => "job_recommendations"), "u.id = jr.id_recommender", array("count(jr.id) as refferals"))
            ->group(['u.id', 'u.firstname', 'u.lastname', 'u.image'])
            ->where("u.type != 2 AND u.type != 3")
            ->where("jr.id_job = ?", $id);

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }

        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value) {
            $result[$key]['total_shares'] = $this->getShareByjob($value['id'], $id)['total_shares'];
        }
        return $result;
    }

    public function getShareByjob($id_user, $id_job)
    {
        $sql = $this->select()->from(array("jr" => "jobs_share"), array("count(id) as total_shares"))
            ->where("jr.id_user =?", (int)$id_user)
            ->where("jr.id_job =?", (int)$id_job);
        $resutlt = $this->fetchRow($sql);
        return $resutlt;
    }

    function getUserJobLeaderboards($id)
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.firstname', 'u.lastname', 'u.image',
                '(SELECT COUNT(ja.id) FROM job_applications ja WHERE ja.id_recommender = ' . $this->_user->id . ' AND ja.id_job=' . $id . ' AND ja.status = 5) AS success',
                '(SELECT COUNT(ja.id) FROM job_applications ja WHERE ja.id_recommender = ' . $this->_user->id . ' AND ja.id_job=' . $id . ' AND ja.status = 11) AS fail',
                '(SELECT COUNT(ja.id) FROM job_applications ja WHERE ja.id_recommender = ' . $this->_user->id . ' AND ja.id_job=' . $id . ' AND ja.status NOT IN (2,5)) AS pending',
                '(SELECT COALESCE(SUM(g.points),0) FROM gamification_points g WHERE g.id_user = ' . $this->_user->id . ' AND g.id_item=' . $id . ') AS total_points',
                '(SELECT COUNT(ja.id) FROM job_applications ja WHERE ja.id_recommender =' . $this->_user->id . ' AND ja.id_job=' . $id . ') AS number_recommendation',
                '(SELECT count(js.id) FROM job_views js WHERE js.id_user = ' . $this->_user->id . ' AND js.id_job=' . $id . ' AND js.source != \'\') AS views',
                '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = ' . $this->_user->id . ' AND js.id_job=' . $id . ') AS job_share',
            ));
        $sql->where("u.id=?", (int)$this->_user->id);
        return $this->fetchRow($sql);
    }

    public function getJobLeaderboardsRest()
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.firstname', 'u.lastname', 'u.image',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status = 5) AS success',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status = 11) AS fail',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status NOT IN (2,5)) AS pending',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id) AS application_on_reccomend',
                '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id GROUP BY id_user) AS total_points',
                '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS real_views',
                '(SELECT COUNT(ja.id_recommender) FROM job_applications ja WHERE ja.id_recommender = u.id) AS number_recommendation',
                '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source != \'\') AS total_views',
                '(SELECT SUM(js.shares) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
            ))
            ->join(array("ja" => "job_applications"), "u.id = ja.id_recommender", array())
            ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
            ->join(array("jr" => "job_recommendations"), "u.id = jr.id_recommender", array());

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        return $this->fetchAssoc($sql);
    }

    public function getCampaignDetailedLeaderboards($id)
    {

        $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("id_job"))->where("cj.id_campaign =?", (int)$id);
        $jobs = $this->fetchAll($sql);
//        $result = [] ;
//        foreach ($jobs as $index => $job) {
//
//            $sql = $this->select()->from(array("jr" => "job_recommendations"), array("id_recommender"))
//                    ->where("jr.id_job =?", (int) $job['id_job']);
//            $result[] = $this->fetchAll($sql);
//        }
////    return $result;
//        
//        $arr_user = [];
//        foreach ($result as $key => $value) {
//            if(count($value) > 0) {
//                foreach($value as $val) {
//                    $arr_user[] = $val['id_recommender'];
//                }
//            }
//            
//        }
        $sql = $this->select()->from(['cad' => 'campaign_actions_detailed'], ['id_recommender'])
            ->where('cad.id_campaign =?', $id);
        $arr_user = $this->fetchAll($sql);
        $arr_user_assoc = [];
        foreach ($arr_user as $key => $value) {
            $arr_user_assoc[] = $value['id_recommender'];
        }
//        return ($arr_user_assoc);
        $arr_user = $arr_user_assoc;
//        echo '<pre>';
//        print_r(array_unique(array_filter($arr_user)));
//        die;
        $campaignModel = new models_Campaign;
        $campaign = $campaignModel->getCampaignById($id);

        $campaign_gamification = $campaign['gamification_actions'];
        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.firstname', 'u.lastname', 'u.image',
            ))
            ->joinLeft(array("cad" => "campaign_actions_detailed"), "cad.id_recommender = u.id", array("sum(value) as total_points",
                "(SELECT count(cadd.id) FROM campaign_actions_detailed cadd WHERE cadd.id_campaign = '{$id}' AND cadd.id_recommender = u.id AND cadd.value =1 ) AS real_views"))
            ->group(["u.id", "u.firstname", "u.lastname", "u.image"]);

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
        if (!empty($arr_user)) {
            $inWhere = implode(",", array_unique(array_filter($arr_user)));
            if ($inWhere != "") {
                if (count($arr_user) > 1) {
                    $sql->where("u.id IN (" . implode(",", array_unique(array_filter($arr_user))) . ")");
                } else {
                    $sql->where("u.id =" . implode(",", array_unique(array_filter($arr_user))));
                }
            }
        } else {
            return;
        }

        $sql->where("u.type !=2 AND u.type != 3");
        $data = $this->fetchAll($sql);
        foreach ($data as &$user) {
            $user['refferals'] = $this->getRefferalsByjobInCampaign($user, $jobs)['refferals'];
            $user['total_shares'] = $this->getSharesByjobInCampaign($user, $jobs)['total_shares'];
        }
        return $data;
    }

    public function getRefferalsByjobInCampaign($user, $jobs)
    {
        $jobs_array = [];
        foreach ($jobs as $key => $value) {
            $jobs_array[] = $value['id_job'];
        }
        $sql = $this->select()->from(array("jr" => "job_recommendations"), array("count(id) as refferals"))
            ->where("jr.id_recommender =?", (int)$user['id'])
            ->where("jr.id_job IN (" . implode(",", $jobs_array) . ")");
        $resutlt = $this->fetchRow($sql);

        return $resutlt;
    }

    public function getSharesByjobInCampaign($user, $jobs)
    {
        $jobs_array = [];
        foreach ($jobs as $key => $value) {
            $jobs_array[] = $value['id_job'];
        }
        $sql = $this->select()->from(array("jr" => "jobs_share"), array("count(id) as total_shares"))
            ->where("jr.id_user =?", (int)$user['id'])
            ->where("jr.id_job IN (" . implode(",", $jobs_array) . ")");
        $resutlt = $this->fetchRow($sql);
        return $resutlt;
    }

    public function getCitybyId($id)
    {
        $sql = $this->select()->from(array("c" => "cities"), array("title"))->where("c.id =?", (int)$id);
        return $this->fetchRow($sql)['title'];
    }

    public function getCustomCitybyId($id)
    {
        $sql = $this->select()->from(array("c" => "custom_location"), array("title"))->where("c.id =?", (int)$id);
        return $this->fetchRow($sql)['title'];
    }

    public function saveNewUser($id, $data)
    {
        $session = new Zend_Session_Namespace('flow');

        foreach ($data['skills'] as $id_skill) {
            $this->insert(array("id_skill" => $id_skill, "id_user" => $id), "user_skills");
        }
        if ($session->_CUSTOM_LOCATION) {
            $location = $this->getCustomCitybyId($data['location'][0]);
        } else {
            $location = $this->getCitybyId($data['location'][0]);
        }
        $months = (int)$data['lastjob_lifetime'];
        $end = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime("-$months months"));
        $start = $startDate;

        $this->update(array("firstname" => $data['firstname'], "id_city" => $data['location'][0], "lastname" => $data['lastname'], "phone" => $data['phone'], "location" => $location), "id = {$id}", 'users');
        $this->insert(array('start' => $startDate, 'end' => $end, 'title' => $data['job_title'], "id_user" => $id, 'company' => $data['company']), "user_positions");
    }

    public function setGamificationCompletedProfileById($id)
    {

        $sql = $this->select()
            ->from(array("u" => "users"), array("u.id_parent", "u.linkedin_link"))
            ->join(array("ucv" => "user_cv"), "u.id = ucv.id_user", array("ucv.file"))
            ->where("u.id = ?", (int)$id);
        $result = $this->fetchRow($sql);
        $recommder = $result['id_parent'];
        if ($result["linkedin_link"] != "" || $result['file'] != "") {
            $this->_stats->setGamificationPoint($recommder, "completed_profile");
        }
    }

    public function setExtendedNetworkPoint($id_recommeder, $id_user)
    {
        if ($id_rec = $this->getRecommderByUserId($id_recommeder)) {
            $id_job = explode('/', $_SESSION['url']['url'])[2];
            $action = "extend_network";
            $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))->where("action =?", $action);
            $gamification_id = $this->fetchRow($sql)['id'];


            $_sql = $this->select()->from(array("gp" => "gamification_points"), array("gp.id"))->where("gp.id_user = ?", $id_rec)
                ->where("action = ?", $gamification_id);
            $result = $this->fetchRow($_sql);

            $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("*"))->where("cj.id_job =?", (int)$id_job);
            $jobResult = $this->fetchRow($sql);
            if (!empty($jobResult)) {
                $this->_stats->setCampaignReport($id_rec, $action, $id_job, $id_user);
                $this->_stats->setGamificationPoint($id_rec, $action);
            } else {
                if (empty($result)) {
                    $this->_stats->setGamificationPoint($id_rec, $action);
                }
            }
        }
    }

    public function getRecommderByUserId($id)
    {
        $sql = $this->select()->from(array("u" => "users"), array("u.id_parent"))->where("u.id = ?", (int)$id);
        $data = $this->fetchRow($sql);
        if (!empty($data)) {
            return $data['id_parent'];
        } else {
            return false;
        }
    }

    public function activatedAccountGamificationPoint($id_company, $id_user)
    {
        $key = array_search("activate_account", $this->_settings['gamification']['actions']);

        $sql_object = $this->select()->from(array("gp" => "gamification_points"), array("id"))
            ->where("gp.id_user =?", $id_user)
            ->where("gp.action =?", $key);
        $returned_data = $this->fetchAll($sql_object);


        if (count($returned_data) > 0) {
            return;
        }


        $modelCampaign = new models_Campaign;
        $sql = $this->select()->from(array("u" => "users"), array())
            ->join(array("c" => "campaigns"), "u.id_company = c.id_company", array('id'))
            ->where("c.id_company =?", (int)$id_company)
            ->where("c.status = 1")
            ->group("c.id");

        $data = $this->fetchAll($sql);
        if (count($data) > 0) {
            foreach ($data as $key => $id) {
                $result[] = $id['id'];
            }


            foreach ($result as $id) {
                $actions[] = $modelCampaign->_getActions($id);
            }
            foreach ($actions as $key => $action) {
                foreach ($action as $act) {
                    $arr[] = $act;
                }
            }

            $key = array_search("activate_account", $this->_settings['gamification']['actions']);
            if (in_array($key, $arr)) {
                $this->_stats->setGamificationPoint($id_user, "activate_account");
            }
        }
    }

    public function getViewsById($id)
    {
        $sql = $this->select()->from(array("jv" => "job_views"), array("COUNT(id) as views"))->where("jv.id_user =?", (int)$id)->where("jv.source !=''");
        return $this->fetchRow($sql)['views'];
    }

    public function updateLinkedinAjax($data)
    {
        $this->update(array("linkedin_link" => $data), "id= {$this->_user->id}", "users");
    }

    public function isPilote($id_Company)
    {
        $sql = $this->select()->from(array("c" => "companies"), array("is_pilote"))->where("c.id =?", (int)$id_Company);
        return $this->fetchRow($sql);
    }

    public function isFirstLogin($id)
    {
        $sql = $this->select()->from(array("u" => "users"), array("last_login"))->where("u.id =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function piloteOut($id)
    {
        $this->insert(array("id_company" => $id, "date" => date('Y-m-d H:i:s'), "count" => 1), "pilote");
    }

    public function countPilote($id_company)
    {
        $sql = $this->select()->from(array("u" => "users"), array("COUNT(id) AS limit"))
            ->where("u.id_company =?", $id_company)
            ->where("u.last_login != ?", "NULL")
            ->where("u.type != ?", 2);
        return $this->fetchRow($sql)['limit'];
    }

    public function getUserByCompany()
    {
        $sql = $this->select()->from(array("u" => "users"), array("u.id", "u.firstname", "u.email"))
            ->where("u.id_company =?", $this->_user->id_company)
            ->where("u.status =?", 1);
        return $this->fetchAssoc($sql);
    }

    public function getRecruitersByCompany()
    {
        $sql = $this->select()->from(array("u" => "users"), array("u.id", "u.firstname", "u.email"))
            ->where("u.id_company =?", $this->_user->id_company)
            ->where("u.status =?", 1)
            ->where("u.type =?", 4);
        return $this->fetchAssoc($sql);
    }

    public function setFirstTimeLogin($id)
    {
        $this->update(array("first_login" => date('Y-m-d H:i:s')), "id = {$id}", "users");
    }

    public function setActivateAccountbyCampaign($user)
    {
        $modelJobs = new models_Jobs;
        $me = $user->id;
        $id_recommender = $user->id_parent;

        $sql = $this->select()->from(array("jr" => "job_recommendations"), array("*"))
            ->where("jr.id_recommender =?", (int)$id_recommender)
            ->where("jr.id_user =?", (int)$me);
        $data = $this->fetchRow($sql);
        $id_job = $data['id_job'];
        $isPartOfCampaign = $modelJobs->jobIsPartOfCampaign($id_job);
        if (!empty($isPartOfCampaign)) {
            $id_campaign = $isPartOfCampaign['id'];
            $actionInCampaign = $this->getActionFromCampaignbyId($id_campaign);
            // completed_profile is included in campaign actions
            if (in_array("activate_account", $actionInCampaign)) {
                $isAccountActivated = $this->isAccountActivated($id_recommender, $id_job);
                if (empty($isAccountActivated)) {
                    $this->_stats->setCampaignReport($id_recommender, "activate_account", $id_job);
                }
            }
        }
    }

    public function isAccountActivated($id_recommender, $id_job)
    {
        $sql = $this->select()->from(array("cad" => "campaign_actions_detailed"), array("id"))
            ->where("cad.id_recommender =?", (int)$id_recommender)
            ->where("cad.id_user =?", (int)Zend_Auth::getInstance()->getIdentity()->id)
            ->where("cad.id_job =?", (int)$id_job)
            ->where("cad.action =?", 'activate_account');
        return $this->fetchRow($sql);
    }

    public function setUserCompletedProfile($url)
    {

        $action = "completed_profile";
        $activate_account_action = "activate_account";
        $extend_network_action = "extend_network";

        $id_user = Zend_Auth::getInstance()->getIdentity()->id;
        $id_recommender = ($this->_user->id_parent != "") ? $this->_user->id_parent : Zend_Auth::getInstance()->getIdentity()->id_parent;
        $array = explode("/", $url);
        $key = array_search('jobs', $array) + 1;
        $id_job = $array[$key];
        // check if job part of campaign
        $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("*"))->where("cj.id_job =?", (int)$id_job);
        $jobResult = $this->fetchRow($sql);
        if (!empty($jobResult)) {
            $id_campaign = $jobResult['id_campaign'];
            // 2 - get gamification Action for the campaign 
            $sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), array("id", "id_gamification_action"))->where("cga.id_campaign =?", (int)$id_campaign);
            $campaign_actions = $this->fetchPairs($sql);
            if (!empty($campaign_actions)) {
                $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))->where("ga.action =?", (string)$action);
                $result = $this->fetchRow($sql);
                if (!empty($result)) {
                    // 4 - check if action exist in campaign actions
                    $exist = array_intersect($campaign_actions, $result);
                    if (!empty($exist)) {
                        $this->insert(array("id_user" => $id_user, "completed_history" => 1, "id_job" => $id_job), "user_profile");
                    }
                }

                // check activate_account exist in campaign 
                $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))->where("ga.action =?", (string)$activate_account_action);
                $data = $this->fetchRow($sql);
                if (!empty($data)) {
                    // 4 - check if action exist in campaign actions
                    $_exist = array_intersect($campaign_actions, $data);
                    if (!empty($_exist)) {
                        $this->_stats->setCampaignReport($id_user, $activate_account_action, $id_job);
                    }
                }

                // check if extend_network exist in campaign
                $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))->where("ga.action =?", (string)$extend_network_action);
                $_data = $this->fetchRow($sql);

                if (!empty($_data)) {
                    // 4 - check if action exist in campaign actions
                    $_exist = array_intersect($campaign_actions, $_data);

                    if (!empty($_exist)) {
                        $id_rec = $this->getRecommderByUserId(Zend_Auth::getInstance()->getIdentity()->id_parent);
                        if ($id_rec) {

                            $_sql = $this->select()->from(array("cad" => "campaign_actions_detailed"), array("*"))
                                ->where("cad.id_recommender = ?", (int)$id_rec)
                                ->where("cad.id_user = ?", (int)$id_user)
                                ->where("cad.action = ?", (string)$extend_network_action);
                            $result = $this->fetchRow($_sql);
                            if (empty($result)) {
//                                $this->_stats->setGamificationPoint($id_rec, $action);
                                $this->_stats->setCampaignReport($id_rec, $extend_network_action, $id_job, $id_user);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getRecommenders($id)
    {
        $sql = $this->select()->from(array("c" => "campaign_recommenders"))->where("id_campaign = ?", (int)$id);
        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            $items[] = $row['id_user'];
        }

        return (array)$items;
    }

    public function getDepartments($id)
    {
        $sql = $this->select()->from(array("c" => "campaign_departments"))->where("id_campaign = ?", (int)$id);
        $data = $this->fetchAll($sql);

        foreach ((array)$data as $row) {
            $items[] = $row['id_department'];
        }

        return (array)$items;
    }

    public function getUsersByDepartments($id_department)
    {
        $sql = $this->select()->from(array("u" => "users"), array('id'))
            ->join(array('ud' => "user_departments"), "u.id=ud.id_user", array())
            ->where("u.id_company =?", (int)Zend_Auth::getInstance()->getIdentity()->id_company)
            ->where('id_department =?', (int)$id_department)
            ->where("u.status = 1")
            ->group('u.id');


        return $this->fetchAll($sql);
    }

    public function setUserCustomLocation($data)
    {
        $this->insert($data, "custom_location");
        $last_insert = $this->lastInsertId();
        $sql_object = $this->select()->from(array("c" => "custom_location"), array("id", "title"))->where("id =?", $last_insert);
        $data = $this->fetchRow($sql_object);
        return $data;
    }

    public function setUserCustomSkills($data)
    {
        $this->insert($data, "custom_user_skills");
        $last_insert = $this->lastInsertId();
        $sql_object = $this->select()->from(array("c" => "custom_user_skills"), array("id", "title"))->where("id =?", $last_insert);
        $data = $this->fetchRow($sql_object);
        return $data;
    }

    public function getUsersPagination($filters = array(), $start, $length, $columns, $search)
    {

        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $id_user = Zend_Auth::getInstance()->getIdentity()->id;

        $sql = $this->select();

        if ($this->_user->type == 3) {
            $sql->from(array('u' => 'users'), array('*', '(SELECT COUNT(id_user) FROM user_connections WHERE u.id = id_user) AS linkedin'))->order('id DESC');
        } else {
            $sql->from(array('u' => 'users'), array('*',
                "(SELECT GROUP_CONCAT(dl.title) FROM departments_lang dl, user_departments ud where ud.id_department = dl.id_department AND ud.id_user = u.id AND dl.id_lang = {$this->id_lang} GROUP BY ud.id_user) AS departments",
                "(SELECT cd.title FROM custom_departments cd , user_departments ud where ud.id_department = cd.id AND ud.id_user = u.id AND cd.id_company = {$this->_user->id_company}) AS custom_departments"))->order('id DESC');
        }

        $sql->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("firstname AS adder_firstname", "lastname AS adder_lastname"));

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                switch ($key) {
                    case "type":
                        if ($value == 9) {
                            $sql->where("u.type = 1");
                            $sql->where("u.id_parent > 0");
                        } else {
                            $sql->where("u.type = ?", (int)$value);
                            $sql->where("u.id_parent = 0");
                        }
                        break;
                }
            }
        } else {
            if ($this->_user->type == 2) {
                $sql->where("u.type < 3");
            }
        }
        $sql->limit($start, $length);
        for ($i = 0; $i < count($columns); $i++) {
            $column_name = $columns[$i]['data'];
            $column_value = $columns[$i]['value'];
            switch ($column_name) {
                case("adder_firstname"):
                    $sql->where("uu.firstname LIKE '%{$column_value}%'");
                    break;
                case("adder_lastname"):
                    $sql->where("uu.lastname LIKE '%{$column_value}%'");
                    break;
                case("options"):
                    break;
                case("departments"):
//                     $sql->where("uu.$column_name LIKE '%{$column_value}%'");
                    break;
                default :
                    $sql->where("u.$column_name LIKE '%{$column_value}%'");
            }
        }
        $search_value = trim($search['value']);
        $sql->where("u.firstname LIKE '%{$search_value}%'");
        $sql_result = $this->fetchAll($sql);
        if ($allow_candidate_recommend) {
            $data = $this->getRecommenderCandidates();
            if (empty($data)) {
                return $sql_result;
            } else {

                return array_merge($data, (array)$sql_result);
            }
        } else {
            return $sql_result;
        }
    }

    public function uploadAjaxCv()
    {
        $file = $_FILES['image'];

        $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
        $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

        $file_name = $file['name'];
        $file_down = WebApp_Tools::CreateAlias($file_no_ext) . "." . $extension;


//        move_uploaded_file($file['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/cv/{$file_down}");
        move_uploaded_file($file['tmp_name'], APPLICATION_PATH . "/storage/cv/{$file_down}");
        $this->insert(array("id_user" => $this->_user->id, "title" => $file['name'], "file" => $file_down, "date" => date("Y-m-d H:i:s")), "user_cv");
        $id = $this->lastInsertId();

        $sql = $this->select()->from(array("uc" => "user_cv"), array("*"))
            ->where("id=?", $id);
        return $this->fetchRow($sql);
    }

    public function saveTerms($terms)
    {
        $data['content'] = $terms;
        $data['id_company'] = Zend_Auth::getInstance()->getIdentity()->id_company;
        $data['status'] = 1;
        $sql = $this->select()->from(array("t" => "terms"))
            ->where("t.id_company=?", (int)Zend_Auth::getInstance()->getIdentity()->id_company);
        $result = $this->fetchRow($sql);
        if (empty($result)) {
            $this->insert($data, "terms");
        } else {
            $this->update($data, "id={$result['id']}", "terms");
        }
    }

    public function getTerms()
    {
        $sql = $this->select()->from(array("t" => "terms"))
            ->where("t.id_company=?", (int)Zend_Auth::getInstance()->getIdentity()->id_company);

        return $this->fetchRow($sql)['content'];
    }

    public function userLoggedIn()
    {
        $data['id_user'] = Zend_Auth::getInstance()->getIdentity()->id;
        $data['date'] = date("Y-m-d H:i:s");
        $this->insert($data, "user_login");
    }

    function companyhasTerms()
    {
        $id = $this->_company->getCompanyBySubdomainSlug()['id'];
        $sql = $this->select()->from(array("t" => "terms"), array("*"))
            ->where("t.id_company=?", (int)$id);

        return $this->fetchRow($sql);
    }

    function getUserCv($id)
    {
        $sql = $this->select()->from(array("uc" => "user_cv"), array("file"))
            ->where("uc.id_user =?", (int)$id);

        return $this->fetchAll($sql);
    }

    function getCampaignDetails($id)
    {
        $sql = $this->select()->from(array("c" => "campaigns"))
            ->where("c.id =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function getUserCampaignDetailedLeaderboards($id)
    {
        $campaignModel = new models_Campaign;
        $campaign = $campaignModel->getCampaignById($id);
        $campaign_gamification = $campaign['gamification_actions'];

        $sql = $this->select()
            ->from(array("crd" => "campaign_actions_detailed"), array("*",
                "(SELECT count(u.id) FROM users u WHERE crd.id_recommender = u.id AND u.id_company={$this->_user->id_company}) as active_users_in_campaign"))
            ->join(array("c" => "campaigns"), "crd.id_campaign = c.id", array("title"));
//                $sql->joinLeft(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array("(SELECT COUNT(id) FROM job_applications WHERE id_job = cj.id_job AND id_recommender > 0) AS recommenders"));
//                ->joinLeft(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array("(SELECT COUNT(id) FROM job_applications WHERE id_job = cj.id_job AND id_recommender > 0) AS recommenders"))

        $sql->where("crd.id_campaign =?", (int)$id);
        $sql->where("c.status = 1");

//        if ($this->_user->type == 2) {
        $sql->where("c.id_company = ?", $this->_user->id_company);
//        }
        $result = $this->fetchAll($sql);
//        echo '<pre>';
//        print_r($result);
//        die;
        $data = [];
        foreach ($result as $index => $value) {
            $data[$value['id_campaign']]['id_campaign'] = $value['id_campaign'];
            $data[$value['id_campaign']]['users'][] = $value['id_user'];
            $data[$value['id_campaign']]['title'] = $value['title'];
        }
        foreach ($data as $index => $value) {
            $data[$index]['active_users'] = count($value['users']);
        }

        $users = $data[$id]['users'];
//                echo '<pre>';
//        print_r($result);
//        die;
        $user_data = [];
        foreach ($result as $key => $user) {
            $sql = $this->select()->from(array("u" => "users"), array("firstname", "lastname", "image"))
                ->join(array("crd" => "campaign_actions_detailed"), "crd.id_recommender=u.id", array("sum(value) as points"))
                ->where("u.id =?", (int)$user['id_recommender']);

            $final = $this->fetchRow($sql);

            $user_data[$user['id_recommender']]['points'] = ($final['points'] != null) ? $final['points'] : 0;
            $user_data[$user['id_recommender']]['firstname'] = $final['firstname'];
            $user_data[$user['id_recommender']]['lastname'] = $final['lastname'];
            $user_data[$user['id_recommender']]['image'] = $final['image'];
            $user_data[$user['id_recommender']]['id_user'] = (int)$user['id_recommender'];
        }

        return $user_data;


        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.firstname', 'u.lastname', 'u.image',
//                    "(SELECT count(jv.id) FROM job_views jv WHERE jv.id_user = u.id) AS total_views",
                "(SELECT COUNT(ja.id) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{$id}' AND ja.id_recommender = {$this->_user->id} AND ja.status = 5) AS success",
                "(SELECT COUNT(ja.id) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{$id}' AND ja.id_recommender = {$this->_user->id} AND ja.status = 11) AS fail",
                "(SELECT COUNT(ja.id) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{$id}' AND ja.id_recommender = {$this->_user->id} AND ja.status NOT IN (2,5)) AS pending",
//                    "(SELECT COUNT(ja.id) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{$id}' AND ja.id_recommender > 0) AS application_on_recommend",
                "(SELECT SUM(g.points) FROM gamification_points g, campaign_jobs cj WHERE g.id_item = cj.id_job AND cj.id_campaign = '{$id}' AND g.id_user = {$this->_user->id}) AS total_points",
                '(SELECT count(js.id) FROM job_views js, campaign_jobs cj WHERE js.id_job = cj.id_job AND cj.id_campaign = ' . $id . ' AND js.id_user = ' . $this->_user->id . ' AND js.source!="") AS views',
                '(SELECT count(js.id) FROM jobs_share js, campaign_jobs cj WHERE js.id_job = cj.id_job AND cj.id_campaign = ' . $id . ' AND js.id_user = ' . $this->_user->id . ') AS job_share',
                '(SELECT count(jr.id) FROM job_recommendations jr, campaign_jobs cj WHERE jr.id_job = cj.id_job AND cj.id_campaign = ' . $id . ' ) AS number_recommendation',
            ));
//                ->join(array("gg" => "gamification_points"), "u.id = gg.id_user", array())
//                ->join(array("j" => "jobs"), "gg.id_item = j.id", array())
//                ->join(array("cj" => "campaign_jobs"), "j.id = cj.id_job", array());
        $sql->where("u.id=?", (int)$this->_user->id);
        return $this->fetchRow($sql);

//        if (!empty($campaign_gamification)) {
//            $sql->where("gg.action IN (" . implode(",", $campaign_gamification) . ")");
//        }
//
//        $sql->where("j.id_company = ?", (int) $this->_user->id_company)
//                ->where("cj.id_campaign = ?", (int) $id)
//                ->order("total_points DESC")
//                ->group("u.id");
//
//        if ($this->_user->type == 2) {
//            $sql->where("j.id_company = ?", $this->_user->id_company);
//        }
//        
//        echo '<pre>';
//        print_r($this->fetchAll($sql));die;
        return $this->fetchAll($sql);
    }

    public function setHowitWorksContent($data)
    {
        $this->update(["status" => 1, "content" => $data], "id_company = {$this->_user->id_company}", "hiworks");
    }

    public function disableHowitWorksContent()
    {
        $this->update(["status" => 0, "content" => ""], "id_company={$this->_user->id_company}", "hiworks");
    }

    public function setAboutUs($data)
    {
        $this->update(["about_us" => $data], "id = {$this->_user->id_company}", "companies");
    }

    public function getHowItWorks()
    {
        $external = $this->_settings['external_platform'];


        $sql = $this->select()->from(array("h" => "hiworks"), array("*"));
        if ($external) {
            $id_company = $this->_company->getCompanyBySubdomainSlug()['id'];
            $sql->where("h.id_company =?", (int)$id_company);
        } else {
            $sql->where("h.id_company =?", (int)$this->_user->id_company);
        }


        $sql->where("h.status = 1");
        return $this->fetchRow($sql);
    }

    public function getrateById($id)
    {
        $sql = $this->select()->from(array("n" => "notification"), array("*"))
            ->join(array("u" => "users"), "n.id_user = u.id", array("*"))
            ->where("n.id =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function setViewedRate($id)
    {
        $id = (int)$id;
        $this->update(['viewed' => 1], "id = $id", "notification");
    }

    public function updateRateUser($rate, $id, $text)
    {
        if (is_array($rate) && !empty($rate)) {
            foreach ($rate as $rate) {
                $this->update([$rate['category'] => $rate['value'], 'note' => $text], "id = $id", "notification");
            }
        }
    }

    public function uploadCandidateCv($data)
    {
        $sql = $this->select()->from(array("u" => "users"))->where("u.email =?", (string)$data['email']);

        $user = $this->fetchRow($sql);
        if (empty($user)) {
            $this->insert($data);
            return $this->lastInsertId();
        }
    }

    public function setRecommendationUploadCv($data)
    {
        $sql = $this->insert($data, "job_recommendations");
    }

    public function setApplicationuploadCandidateCv($data)
    {
        $mJob = new models_Jobs();
        $id_source = $mJob->getSourceByName('Upload Cv')['id'];
        $data['source'] = $id_source;
        $sql = $this->insert($data, "job_applications");
    }

    public function uploadFriendCv($id_user)
    {
        $file = $_FILES['file'];
        $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
        $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

        $file_name = $file['name'];
        $file_down = WebApp_Tools::CreateAlias($file_no_ext) . "." . $extension;

        move_uploaded_file($file['tmp_name'], APPLICATION_PATH . "/storage/cv/{$file_down}");
//        move_uploaded_file($file['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/cv/{$file_down}");
        $this->insert(array("id_user" => $id_user, "title" => $file['name'], "file" => $file_down, "date" => date("Y-m-d H:i:s")), "user_cv");
    }

    public function sendEmailTemplate($smarty_data, $id_email, $to)
    {

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $contentModel = new models_Content;
        $email = $contentModel->getEmailByType($id_email);
        $smarty = new Smarty;
        foreach ($smarty_data as $key => $value) {
            $smarty->assign($key, $value);
        }
        $model_company = new models_Company();
        $smarty->assign('subdomain_url', $this->_settings['sitename']);
        $smarty->assign('bg_color', $model_company->getCompanyEmailButtonColor());

        $emailBody = $smarty->fetch('eval:' . $email['content']);
        $emailSubject = $smarty->fetch('eval:' . $email['subject']);
        $template = $model_company->getCompanyEmailTemplate();

        $smarty->assign('subject', $emailSubject);
        $smarty->assign('content', $emailBody);

        $content = $smarty->fetch('eval:' . $template);

        $mail = new WebApp_Mail();
        $mail->addTo($to, $this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['title']);
        $mail->setFrom($this->_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject($emailSubject);
        $mail->setBodyHtml($content);
        if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $mail->send($transport);
        }
    }

    public function call($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $headers = ["content-length: 0"];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); //set headers


        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
    }

    public function registerWithFacebook($data)
    {
        $session = new Zend_Session_Namespace('track');
        $source = new Zend_Session_Namespace('source');
        $track = $session->track;
        $modelJobs = new models_Jobs;
        $user_email = $data->email;
        $user_image = $data->image;
        $user_facebook_id = $data->id;
        $user_firstname = explode(" ", $data->name)[0];
        $user_lastname = explode(" ", $data->name)[1];
        if (isset($source->source) && $source->source != "") {
            $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string)$session->track)->limit(1);
            $id_recommender = $this->fetchRow($sql)['id'];
            $id_parent = $id_recommender;
        } else {
            $id_parent = $modelJobs->getTrackDetails($track)['id_recommender'];
            if ($id_parent == "")
                $id_parent = 0;
        }
        $sql = $this->select()->from(array("u" => "users"), array("*"))
            ->where("u.facebook_id =?", $user_facebook_id);
        $user = $this->fetchRow($sql);

        if (!empty($user)) {
            $user = (object)$user;
            Zend_Auth::getInstance()->getStorage()->write($user);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$user->id}");
            $this->reauthenticate($user->email);
            $id = $this->lastInsertId();
        } else {
            $campaign_track = new Zend_Session_Namespace("campaign_track");
            if (isset($campaign_track->data)) {
                $source_channel = $campaign_track->data['source'];
            } else {
                $source = new Zend_Session_Namespace('source');
                if($source->source > 0)
                {
                    $source_channel = $source->source;
                }
                else {
                    $mJobs = new models_Jobs ;
                    $source_channel = $mJobs->getSourceByName('website')['id'];
                }
            }
            $this->insert(['email' => $user_email, 'firstname' => $user_firstname, "lastname" => $user_lastname, "facebook_id" => $user_facebook_id,

                "status" => 1,'source' => $source_channel, "type" => 9, 'id_company' => $this->_company->getCompanyBySubdomainSlug()['id'],
                'id_parent' => $id_parent, "last_login" => date("Y-m-d H:i:s"), "first_login" => date("Y-m-d H:i:s") ,"created" => date("Y-m-d H:i:s"),'verified' => 1, 'image' => $user_image], 'users');

            $id = $this->lastInsertId();
            if (isset($track) && $track != "") {
                $this->update(array('id_user' => $id), "track = '$track'", "job_recommendations");
            }
            $this->newCandidatePoints($id);
            $this->setExtendedNetworkPoint($id_parent, $id);
            $this->registerWithFacebook($data);
        }
        Zend_Session::namespaceUnset('track');
        Zend_Session::namespaceUnset('source');
        return $id;
    }

    public function registerWithLinkedin($usr)
    {
        $session = new Zend_Session_Namespace('track');
        $source = new Zend_Session_Namespace('source');
        $track = $session->track;
        $modelJobs = new models_Jobs;
        $data['email'] = $usr['emailAddress'];
        $data['firstname'] = $usr['firstName'];
        $data['lastname'] = $usr['lastName'];
        $data['linkedin_id'] = $usr['id'];
        $data['status'] = 1;
        $data['type'] = 9;
        $data['first_login'] = date("Y-m-d H:i:s");
        $data['last_login'] = date("Y-m-d H:i:s");
        $data['created'] = date("Y-m-d H:i:s");
        $data['verified'] = 1;
        $data['id_company'] = $this->_company->getCompanyBySubdomainSlug()['id'];
        if (isset($usr['publicProfileUrl'])) {
            $data['linkedin_link'] = $usr['publicProfileUrl'];
        }
        if (isset($source->source) && $source->source != "") {
            $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string)$session->track)->limit(1);
            $id_recommender = $this->fetchRow($sql)['id'];
            $id_parent = $id_recommender;
        } else {
            $id_parent = $modelJobs->getTrackDetails($track)['id_recommender'];
            if ($id_parent == "")
                $id_parent = 0;
        }
        $data['id_parent'] = $id_parent;

        $sql = $this->select()->from(array("u" => "users"), array("*"))
            ->where("u.linkedin_id =?", $usr['id']);
        $user = $this->fetchRow($sql);
        if (!empty($user)) {
            $user = (object)$user;
            Zend_Auth::getInstance()->getStorage()->write($user);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$user->id}");
            $this->reauthenticate($user->email);
        } else {
            $linkedin_image_url = end($usr['pictureUrls']['values']);
            $linkedin_user_image_name = time() . "_" . $data['linkedin_id'] . ".jpg";
            $linkedin_user_image_path = ABSOLUTE_PATH . "/public/resources/files/user/" . $linkedin_user_image_name;

            try {
                $this->grab_image($linkedin_image_url, $linkedin_user_image_path);
                $data['image'] = $linkedin_user_image_name;
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit();
            }

            $campaign_track = new Zend_Session_Namespace("campaign_track");
            if (isset($campaign_track->data)) {
                $data['source'] = $campaign_track->data['source'];
            } else {
                $source = new Zend_Session_Namespace('source');
                if($source->source > 0)
                {
                    $data['source'] = $source->source;
                }
                else {
                    $mJobs = new models_Jobs ;
                    $data['source'] = $mJobs->getSourceByName('website')['id'];
                }
            }
            $this->insert($data, "users");
            $id = $this->lastInsertId();
            if (isset($track) && $track != "") {
                $this->update(array('id_user' => $id), "track = '$track'", "job_recommendations");
            }
            $this->newCandidatePoints($id);
            $this->setExtendedNetworkPoint($data['id_parent'], $id);
            $this->registerWithLinkedin($usr);
        }
        Zend_Session::namespaceUnset('track');
        Zend_Session::namespaceUnset('source');
        return $id;
    }

    public function registerWithGoogle($usr)
    {
        $session = new Zend_Session_Namespace('track');
        $source = new Zend_Session_Namespace('source');
        $track = $session->track;
        $modelJobs = new models_Jobs;
        $usr = (array)$usr;


        $data['email'] = $usr['email'];
        $data['firstname'] = $usr['givenName'];
        $data['lastname'] = $usr['familyName'];
        $data['google_id'] = $usr['id'];
        $data['status'] = 1;
        $data['type'] = 9;
        $data['first_login'] = date("Y-m-d H:i:s");
        $data['last_login'] = date("Y-m-d H:i:s");
        $data['created'] = date("Y-m-d H:i:s");
        $data['verified'] = 1;
        $data['id_company'] = $this->_company->getCompanyBySubdomainSlug()['id'];
        if (isset($source->source) && $source->source != "") {
            $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string)$session->track)->limit(1);
            $id_recommender = $this->fetchRow($sql)['id'];
            $id_parent = $id_recommender;
        } else {
            $id_parent = $modelJobs->getTrackDetails($track)['id_recommender'];
            if ($id_parent == "")
                $id_parent = 0;
        }

        $data['id_parent'] = $id_parent;

        $google_id = (int)$data['google_id'];


        $sql = $this->select()->from(array("u" => "users"), array("*"))
            ->where("u.google_id = {$usr['id']}");

        $user = $this->fetchRow($sql);


        if (!empty($user)) {
            $user = (object)$user;
            Zend_Auth::getInstance()->getStorage()->write($user);
            $this->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$user->id}");
            $this->reauthenticate($user->email);
        } else {

            $Google_id = $data['google_id'];
            $Google_user_image_url = $usr['picture'];
            $Google_user_image_name = time() . "_" . $Google_id . ".jpg";
            $Google_user_image_path = ABSOLUTE_PATH . "/public/resources/files/user/" . $Google_user_image_name;
            try {
                $this->grab_image($Google_user_image_url, $Google_user_image_path);
                $data['image'] = $Google_user_image_name;
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit();
            }

            $campaign_track = new Zend_Session_Namespace("campaign_track");
            if (isset($campaign_track->data)) {
                $data['source'] = $campaign_track->data['source'];
            } else {
                $source = new Zend_Session_Namespace('source');
                if($source->source > 0)
                {
                    $data['source'] = $source->source;
                } else {
                    $mJobs = new models_Jobs ;
                    $data['source'] = $mJobs->getSourceByName('website')['id'];
                }

            }


            $this->insert($data, "users");
            $id = $this->lastInsertId();
            if (isset($track) && $track != "") {
                $this->update(array('id_user' => $id), "track = '$track'", "job_recommendations");
            }
            $this->newCandidatePoints($id);
            $this->setExtendedNetworkPoint($data['id_parent'], $id);
            $this->registerWithGoogle($usr);
        }
        Zend_Session::namespaceUnset('track');
        Zend_Session::namespaceUnset('source');
        return $id;
    }

    public function grab_image($url, $path)
    {
//        $content = file_get_contents($url);
//        $status = file_put_contents($path, $content);
//        if (!$status)
//            throw new Exception("Upload image is failed : Check folder permission and allo_fopen_url");
        $my_server_img = $url;
        $extenstion = explode('/', getimagesize($my_server_img)['mime'])[1];
        if ($extenstion == "png") {
            $img = imagecreatefrompng($my_server_img);
            imagepng($img, $path);
        } else {
            $img = imagecreatefromjpeg($my_server_img);
            $path = $path;
            imagejpeg($img, $path);
        }
    }

    public function sendNotifyCandidateEmail($data, $candidate_id)
    {
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $user = $this->getUserById($candidate_id);

        $mail = new WebApp_Mail();
        $mail->addTo($user['email'], $user['firstname']);
        $mail->setFrom(Zend_Auth::getInstance()->getIdentity()->email, $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject("notification");
        $mail->setBodyHtml($data);
        $mail->send($transport);
    }

    public function getUserRecommendedDate($users)
    {
        $users = (array)$users;
        foreach ($users as $index => $user) {
            $sql = $this->select()->from([
                "jr" => "job_recommendations",
            ], [
                "*"
            ])
                ->where("jr.id_user =?", (int)$user['id'])
                ->where("jr.id_recommender > 0");
            $result = $this->fetchAll($sql);
            if (!empty($result)) {
                $users[$index]['recommended_date'] = array_shift($result)['date'];
            } else {
                $users[$index]['recommended_date'] = $user['created'];
            }
        }
        return $users;
    }

    public function getUserLastJobStatus($users)
    {
        foreach ($users as $index => $user) {
            $sql = $this->select()->from(['ja' => 'job_applications'], ['ja.id', 'ja.status', 'ja.status_reason', 'ja.date'])
                ->where('ja.id_user =?', (int)$user['id']);
            $result = $this->fetchAll($sql);
            if (!empty($result)) {
                $users[$index]['status'] = end($result)['status'];
                $users[$index]['status_reason'] = end($result)['status_reason'];
            }
        }
        return $users;
    }

    public function getCandidates()
    {
        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];

        $sql = $this->select()
            ->from(array("u" => "users"), array("*",
                "(SELECT status FROM job_applications ja WHERE ja.id_user = u.id ORDER BY updated DESC LIMIT 1) as status",
                "(SELECT status_reason FROM job_applications ja WHERE ja.id_user = u.id ORDER BY updated DESC LIMIT 1) as status_reason",
            ))
            ->joinLeft(array("up" => "user_positions"), "u.id = up.id_user", array("title as position", "is_current as hired"))
            ->joinLeft(array("ue" => "user_education"), "u.id = ue.id_user", array("title as school"))
            ->joinLeft(array("r" => "job_recommendations"), "r.id_user = u.id AND r.id_recommender = u.id_parent", array("date AS recommend_date"))
            ->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("firstname AS recommended_firstname", "lastname AS recommended_lastname"));
        $sql->joinleft(array("c" => "company_unblocked_cvs"), "c.id_user = u.id", array(""));
        $sql->where("u.status = 1")
            ->where("u.id != '{$this->_user->id}'")
            ->where("u.type !=?", (int)2)
            ->order("u.created DESC");;
        $sql->where("u.id_company = ?", $this->_user->id_company);
        if (Zend_Auth::getInstance()->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
        $sql->group('u.id');
        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value) {
            if ($value['last_login'] == "") {
                $result[$key]['last_login'] = "0000-00-00 00:00:00";
            }
        }
        return $result;
//        $data =  array_merge($this->searchUsersMineRecommneder($filters), $result);
//        return  array_map("unserialize", array_unique(array_map("serialize", $result)));
    }

    public function getCandidateHistory($id_user)
    {
        $sql = $this->select()
            ->from(['u' => "users"], ['*',
                "(SELECT count(ja.id) FROM job_applications as ja WHERE ja.id_user = u.id ) as application",
                "(SELECT ja.source From job_applications as ja where ja.id_user = u.id order by ja.date ASC limit 1) as sursa"])
            ->joinLeft(['uu' => "users"], 'u.id_parent = uu.id', ['firstname as recommender_name', 'lastname as recommender_lname'])
            ->joinLeft(['uv' => "user_cv"], 'u.id = uv.id_user', ['file'])
            ->where("u.id =?", (int)$id_user);
        $user = $this->fetchRow($sql);
        $sql = $this->select()->from(['sm' => 'social_media'])->where('sm.id=?', (int)$user['sursa']);
        $user['source_name'] = $this->fetchRow($sql)['title'];
        return $user;
    }

    public function getAllStatus()
    {
        $translate = Zend_Registry::get("Zend_Translate");

        return [
            $translate->_("new") => [0 => 0],
            $translate->_("viewed") => [1 => 0],
            $translate->_("processing") => [4 => 9],
            $translate->_("processing") => [4 => 9],
            $translate->_("moved_to_another_process") => [3 => 0],
            $translate->_("unfit_candidate") => [2 => 1],
            $translate->_("accepted") => [4 => 5],
//            $translate->_("called_for_interview") => [4 => 0],
            $translate->_("offered") => [4 => 0],
            $translate->_("tested") => [4 => 2],
            $translate->_("offer_rejected") => [4 => 6],
            $translate->_("no_answer") => [4 => 7],
            $translate->_("dropped_out") => [4 => 8],
            $translate->_("no_show") => [5 => 1],
            $translate->_("banned") => [5 => 2],
            $translate->_("final_interview") => [5 => 3]
        ];
    }

    public function getNewAllStatus()
    {
        $translate = Zend_Registry::get("Zend_Translate");

        return [
            [0, 0, $translate->_("new")],
            [1, 0, $translate->_("viewed")],
            [4, 9, $translate->_("processing")],
            [3, 0, $translate->_("moved_to_another_process")],
            [2, 1, $translate->_("unfit_candidate")],
            [4, 5, $translate->_("accepted")],
//            [4 , 0,$translate->_("called_for_interview")],
            [4, 0, $translate->_("offered")],
            [4, 2, $translate->_("tested")],
            [4, 6, $translate->_("offer_rejected")],
            [4, 7, $translate->_("no_answer")],
            [4, 8, $translate->_("dropped_out")],
            [5, 1, $translate->_("no_show")],
            [5, 2, $translate->_("banned")],
            [5, 3, $translate->_("final_interview")]

            // 1 : new
            // 2 : viewed
            // 3 : processing
            // 4 : moved_to_another_process
            // 5 : accepted
            // 6 : unfit_candidate
            // 7 : offered
            // 8 : tested
            // 9 : offer_rejected
            // 10 : no_answer
            // 11 : dropped_out
            // 12 : no_show
            // 13 : banned
            // 14 : final_interview
        ];
    }

    public function activateUserAccount($token)
    {
        $token = (string)$token;
        $sql = $this->select()->from(['u' => "users"], ['*']);

        if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $sql->where("sys.fn_varbintohexsubstring(0,HashBytes('SHA1', cast(email as varchar(max))), 1, 0) = ?", $token);
        } else {
            $sql->where("SHA1(email) =?", $token);
        }

        $user = $this->fetchRow($sql);
        if (!empty($user)) {
            if (!$user['verified']) {
                $this->update(['verified' => 1], "id={$user['id']}", "users");
                return true;
            }
            return false;
        }
        return false;
    }

    public function collectUserSkillById($id_user)
    {
        $modelContent = new models_Content;

        $skill = array_merge($modelContent->getUserSkills($id_user), $modelContent->getCustomUserSkills($id_user));
        return $skill;
//        $CustomSkill = $modelContent->getCustomUserSkills($id_user);
    }

    public function collectUserStatusById($id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['status', 'status_reason'])
            ->where("ja.id_user =?", (int)$id_user);
        return $this->fetchAll($sql);
    }

    public function collectUserStatusByJobId($id_user, $id_job)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['status'])
            ->where("ja.id_user =?", (int)$id_user)
            ->where("ja.id_job =?", (int)$id_job);
        return $this->fetchAll($sql);
    }

    public function collectJobsApplicationById($id_user)
    {
        $sql = $this->select()->from([
            'ja' => 'job_applications', ['*']
        ])->where("ja.id_user =?", (int)$id_user);
        return $this->fetchAll($sql);
    }

    public function collectUserDepartmentsById($id_user)
    {
        $sql = $this->select()->from(array("ud" => "user_departments"), array('id', "id_department"))->where("ud.id_user = ?", (int)$id_user);
        return $this->fetchAll($sql);
    }

    public function saveNote($payload)
    {
        $id_recommener = $this->_user->id;
        $email = $payload['email'];
        $id_user = $this->getUserDataByEmail($email)['id'];
        $general = $payload['general'];
        $id_job = $payload['job'];
        $note = strip_tags($payload['note']);
        $date = date("Y-m-d H:i:s");

        $this->insert(['id_user' => $id_user,
            'id_recommender' => $id_recommener,
            'general' => $general,
            'id_job' => $id_job,
            'date' => $date,
            'note' => $note], 'ats_note');
        return $this->lastInsertId();
    }

    public function saveNoteFile($id_note, $file_name)
    {
        $this->update(['file' => $file_name], "id = $id_note", "ats_note");
    }

    public function getNoteByUserId($id_user)
    {
        $sql = $this->select()
            ->from(['n' => "ats_note"], ['*'])
            ->where('n.id_user =?', (int)$id_user)
            ->join(['u' => 'users'], 'n.id_recommender=u.id', ['firstname', 'lastname'])
            ->joinLeft(['jl' => 'jobs_lang'], 'jl.id_job=n.id_job', ['title'])
            ->order("n.date DESC");
        return $this->fetchAll($sql);
    }

    public function setatsUserDepartment($id, $department)
    {
        $this->delete("id_user = $id", "hr_jobs");
        if (is_array($department)) {
            foreach ($department as $value) {
                $this->insert(['id_user' => $id, 'id_department' => $value, 'date' => date('Y-m-d H:i:s')], 'hr_jobs');
            }
        }
    }

    public function assignJobsForRecuiter($id, $jobs, $department)
    {
        $id = (int)$id;
        if (!empty($jobs)) {
            $this->delete('id_user = ' . $id, 'recruiter_job');
            $this->delete('id_user = ' . $id, 'hr_jobs');
            foreach ($jobs as $job) {
                $this->insert(['id_user' => $id, 'id_job' => $job, 'date' => date('Y-m-d H:i:d')], 'recruiter_job');
            }

            foreach ($department as $value) {
                $this->insert(['id_user' => $id, 'id_department' => $value, 'date' => date('Y-m-d H:i:s')], 'hr_jobs');
            }
        } else {
            $this->delete('id_user = ' . $id, 'recruiter_job');
            $this->delete('id_user = ' . $id, 'hr_jobs');
        }
    }

    public function saveUserSignatue($id_user, $signature)
    {
        $this->delete("id_user =" . (int)$id_user, 'user_signature');
        $this->insert(['id_user' => (int)$id_user, 'signature' => $signature, 'date' => date("Y-m-d H:i:d")], 'user_signature');
    }

    public function getSignatureByUserId($id)
    {
        $sql = $this->select()->from(['us' => 'user_signature'], ['us.signature'])->where("us.id_user =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function getHmUsers()
    {
        $sql = $this->select()->from(['u' => 'users'], ['u.firstname', 'u.lastname', 'u.email'])
            ->where("u.id_company =?", (int)$this->_user->id_company)
            ->where("u.type =?", (int)5);
        return $this->fetchAll($sql);
    }

    public function getCandidateJobApplication($id_jobapplication)
    {
        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];

        $sql = $this->select()
            ->from(array("ja" => "job_applications"), ['u.id as id_job'])
            ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.title", "u.status AS user_status", "image", "id", "email"))
            ->where("u.status =?", 1)
            ->where("u.type =9 OR u.type=1 OR u.type=4")
            ->where("ja.id_job =?", (int)$id_jobapplication);

//
//        $sql->where("u.status = 1")
//                ->where("u.id != '{$this->_user->id}'")
//                ->where("u.type = 9")
//                ->where('ja.id =?',(int) $id_jobapplication)
//        ;
//        $sql->where("u.id_company = ?", $this->_user->id_company);
        if (Zend_Auth::getInstance()->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
//        $sql->group('u.id');
        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value) {
            if ($value['last_login'] == "") {
                $result[$key]['last_login'] = "0000-00-00 00:00:00";
            }
        }
        return $result;
    }

    public function getApplicationbyids($id_user, $id_job)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'], ['ja.id'])
            ->where('ja.id_job=?', (int)$id_job)
            ->where('ja.id_user =?', (int)$id_user);
        return $this->fetchRow($sql)['id'];
    }

    public function setjobApplicationStatusHistory($id_job = 0, $id_application, $status, $status_reason, $note)
    {
        $modelJob = new models_Jobs;
        $job_application = $modelJob->getJobApplicationById($id_application);
        $id_job = ($id_job > 0) ? $id_job : $job_application['id_job'];
        $data['modified_by'] = Zend_Auth::getInstance()->getIdentity()->id;
        $data['job_application_id'] = $id_application;
        $data['id_job'] = $id_job;
        $data['id_user'] = $job_application['id_user'];
        $data['id_recommender'] = $job_application['id_recommender'];
        $data['status'] = $status;
        $data['status_reason'] = $status_reason;
        $data['date'] = date("Y-m-d H:i:s");
        $data['notes'] = $note;

        $this->insert($data, 'job_application_status_history');
    }

    public function saveCandidateUser($data, $send_mail = false, $id_email = 1)
    {
        $session = new Zend_Session_Namespace("track");
        $new_password_confirm = $data['new_password_confirm'];
        $original_password = $data['original_password'];
        $new_password = $data['new_password'];
        $jobs = $data['jobs'];
        unset($data['new_password_confirm']);
        unset($data['original_password']);
        unset($data['new_password']);
        unset($data['jobs']);

        $department = $data['department'];
        unset($data['department']);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            if ($data['password'] == "") {
                unset($data['password']);
            } else {
                $data['password'] = sha1($data['password']) . $this->_user->salt;
            }
            if (count($data) > 0) {
                $this->update($data, "id = $id");
            }
        } else {
            $data['password'] = $this->generateStrongPassword();
            $password = $data['password'];
            $data['status'] = isset($data['status']) ? $data['status'] : 0;
            $data['hash'] = sha1(time());
            $data['salt'] = uniqid(mt_rand(), false);
            $data['password'] = sha1($data['password']) . $data['salt'];
            unset($data['id']);
            $this->insert($data);
            $id = $this->lastInsertId();


            $url = $_SESSION['url']['url'];
            $parts = parse_url($url);

            parse_str($parts['query'], $query);
            $source = ($query['source']);

            if ($source) {
                $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string)$session->track)->limit(1);
                $user_id = $this->fetchRow($sql)['id'];
                $session->track = uniqid();
                $job_id = explode('/', $_SESSION['url']['url'])[2];
                $this->insert(array("id_recommender" => $user_id, "track" => $session->track, 'id_job' => $job_id), "job_recommendations");
            }
            if (isset($session->track) && $session->track != "") {
                $this->update(array("id_user" => $id), "track = '{$session->track}'", "job_recommendations");
//                $this->_stats->setGamificationPoint($id, "job_recommend");
            }
//            $this->_stats->setGamificationPoint($id, "register");
            $this->_stats->setNotification($id, "user", "jobs", "Welcome to WorkinAfrica! <br>Start finding your dream job.");
            $this->_stats->setNotification($id, "user", "profile", "Employers will find you faster if you complete your profile! <br>Add more info now!");
            $companie_model = new models_Company;
            if ($send_mail === true) {
                $socialModel = new models_Social;
//                $emailContent = $socialModel->getEmailByType($id_email);
                $data['company_title'] = $companie_model->getCompanyByUserId($id)['title'];

                $transport = null;
                if (!empty($this->_settings['email_smtp'])) {
                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
                }
                $contentModel = new models_Content;
                $email = $contentModel->getEmailByType($id_email);

                $me = $this->getUserById($id);
                $link = $this->_settings['sitename'] . '/activate?token=' . sha1($me['email']);

                $smarty = new Smarty;
                $smarty->assign("candidate", ['firstname' => $data['firstname'], 'lastname' => $data['lastname']]);
                $smarty->assign("link", $link);
                $smarty->assign("user", $data);
                $smarty->assign("password", $password);
                $smarty->assign("settings", $this->_settings);
                $smarty->assign("company", $companie_model->getCompanyById($data['id_company']));
                $emailBody = $smarty->fetch('eval:' . $email['content']);
                $emailSubject = $smarty->fetch('eval:' . $email['subject']);

                $mail = new WebApp_Mail();
                $mail->addTo($data['email'], $data['firstname']);
                $mail->setFrom($this->_company->getCompanyById($this->getUserDataByEmail($data['email'])['id_company'])['email'], $this->_company->getCompanyById($this->getUserDataByEmail($data['email'])['id_company'])['title']);
                $mail->setSubject($emailSubject);
                $mail->setBodyHtml($emailBody);
                $mail->send($transport);

//                $me = $this->getUserById($id);
//                $link = $this->_settings['sitename'] . '/activate?token=' . sha1($me['email']);
//                $template = "To activate your account please click here <br /><br /> <a href='{$link}'>{$link}</a> ";
//                $mail = new WebApp_Mail();
//                $mail->addTo($_POST['email'], $_POST['firstname']);
//                $mail->setFrom($this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['email'], $this->_company->getCompanyById($this->getUserDataByEmail($_POST['email'])['id_company'])['title']);
//                $mail->setSubject("Activate Account");
//                $mail->setBodyHtml($template);
//                $mail->send($transport);
            }
        }
        if ($this->_settings['ats'] && $data['type'] == 5) {
            $this->setatsUserDepartment($id, $department);
        }

        if ($this->_settings['ats'] && $data['type'] == 4) {
            $this->assignJobsForRecuiter($id, $jobs, $department);
        }
        if (isset($department)) {
            $this->delete("id_user = $id", "user_departments");
            foreach ($department as $value) {
                $this->insert(array("id_user" => $id, "id_department" => $value), "user_departments");
            }
        } else {
            $this->delete("id_user = $id", "user_departments");
        }

        $this->saveImage($id);
        return $id;
    }

    public function changeAllNewJobsToview($applications)
    {
        foreach ($applications as $key => $value) {
            if ($value['app_status'] == 0 && $value['status_reason'] == 0) {
                $data['status'] = 1;
                $data['status_reason'] = 0;
                $data['modified_by'] = Zend_Auth::getInstance()->getIdentity()->id;
                $data['updated'] = date("Y-m-d H:i:s");
                $this->update($data, "id = {$value['job_application_id']}", 'job_applications');
                $this->_modelUser->setjobApplicationStatusHistory($value['id_job'], $value['job_application_id'], $data['status'], $data['status_reason']);
            }
        }
    }

    public function checkBannedStatus($candidates)
    {
        foreach ($candidates as $key => &$candidate) {
            $job_applications = $candidate['job_application'];
            foreach ($job_applications as $index => &$candidate) {
                if ($candidate['status'] == 13)
                    $candidates[$key]['is_banned'] = "banned";
                if ($candidate['status'] == 5)
                    $candidates[$key]['is_hired'] = "hired";
            }
        }
        return $candidates;
    }

    public function collectUserEmailsBy($id_user)
    {
        $sql = $this->select()
            ->from(['aeh' => 'ats_emails_history'], ['*'])
            ->join(['u' => 'users'], "u.id = aeh.id_sender", ['firstname', 'lastname'])
            ->where('aeh.id_receiver =?', (int)$id_user);

        return $this->fetchAll($sql);
    }

    public function getEmailTemplateHistory($id)
    {
        $sql = $this->select()->from(['aeh' => 'ats_emails_history'], ['*'])
            ->join(['u' => 'users'], "u.id = aeh.id_sender", ['firstname', 'lastname'])
            ->where("aeh.id =?", (int)$id);
        return $this->fetchRow($sql);
    }

    public function saveNewFlowSignIn($array)
    {

        $req = $array;
        $req['id_company'] = $this->_company->getCompanyBySubdomainSlug()['id'];
//        $req['email'] = "sdsd".rand(1,548787)."sds@gmail.com";
        $req_2 = $array[2];
        unset($req['terms']);
        unset($req['password_confirm']);
        unset($req['password-retype']);
        if (!empty($req['company'])) {
            $company = $req['company'];
            unset($company['password_confirm']);
            if (!$user = $this->getUserByEmail($company['email'])) {
                $userData['firstname'] = $company['firstname'];
                $userData['lastname'] = $company['lastname'];
                $userData['password'] = $company['password'];
                $userData['email'] = $company['email'];
                $userData['type'] = 2;
                $userData['status'] = 1;
                $userData['created'] = date("Y-m-d H:i:s");


                if ($id_user = $this->saveUser($userData, true)) {
                    $companyData['title'] = $company['title'];
                    $companyData['email'] = $company['email'];
                    $companyData['id_country'] = $company['id_country'];
                    $companyData['id_city'] = $company['id_city'];
                    $companyData['description'] = $company['description'];
                    $companyData['address'] = $company['address'];
                    $companyData['phone'] = $company['phone'];
                    $companyData['status'] = 1;

                    $id_company = $this->saveCompany($companyData);
                    $this->_model->update(array("id_company" => $id_company, "is_company_admin" => 1), "id = {$id_user}", "users");

                    //$this->_model->authenticate($userData['email'], $userData['password']);                                         
                    $session = new Zend_Session_Namespace("redirector");

                    $this->_redirect('/profile');
                } else {
                    $msg = $this->translate->_("account-exists");
                }
            } else {
                $msg = $this->translate->_("account-exists");
            }
        } else {
            if (!$user = $this->getUserByEmail($req['email'])) {

                $req['type'] = 9;
                $req['status'] = 1;
                $req['created'] = date("Y-m-d H:i:s");
//                    $req['password'] = uniqid();
                $session = new Zend_Session_Namespace("track");
                $track = $session->track;
                $string = $req['email'];
                $explode = explode("@", $string);
                array_shift($explode);
                $newstring = join('@', $explode);
                $email_domain = explode('.', $newstring)[0];
                $model = new models_Company;
                $slug = $model->getCompanyBySubdomainSlug()['slug'];
                if ($slug == $email_domain ||
                    fnmatch("*$email_domain*", $slug)) {
                    $req['type'] = 1;
                }
                $req['id_company'] = $model->getCompanyBySubdomainSlug()['id'];
                if ($track != "") {
                    $modelJobs = new models_Jobs;
                    $tracking = $modelJobs->getTrackDetails($track);
                    $req['id_company'] = $tracking['id_company'];
                    $req['id_parent'] = $tracking['id_recommender'];
                    if (empty($tracking)) {
                        $tracking = $modelJobs->getHashDetails($track);
                        $req['id_company'] = $tracking['id_company'];
                        $req['id_parent'] = $tracking['id'];
                    }
                }

                $req['firstname'] = $req_2['firstname'];
                $req['lastname'] = $req_2['lastname'];
                $req['lastjob_lifetime'] = $req_2['lastjob_lifetime'];
                $id = $this->saveFlowUser($req, true);
                $language = $req_2['foreign_language'];
//                $this->addUserLanguage($id, $language);
//                $this->insert(['id_user' => $id, 'title' => $req_2['education']], 'user_education');

                if ($req['id_parent'] > 0) {
//                    $campaign_track = new Zend_Session_Namespace("campaign_track");
//                    $this->setExtendedNetworkPoint($req['id_parent'], $id);
//                    $id_view = $campaign_track->data['id_view'];
                }

//                die('sddsd');
                // if id to check that the user is alrady added
                if ($id) {
//                    $this->_stats->newCandidateAction($tracking['id_recommender'], "new_candidates", $tracking['id_job'], $id);
                    if ($req['id_parent'] > 0) {
                        $campaign_track = new Zend_Session_Namespace("campaign_track");
                        $this->setExtendedNetworkPoint($req['id_parent'], $id);
                        $id_job = $tracking['id_job'];
                        if ($id_job < 1) {
                            $id_job = $campaign_track->data['id_job'];
                            $hash = $campaign_track->data['ts'];
                        }
                        $jModel = new models_Jobs;
                        $jobIsPartOfCampain = $jModel->jobIsPartOfCampaign($id_job);
                        $id_view = 0;
                        $action = "new_candidates";
//        $this->_model->setGamificationActionPoints($action, $id_job, $id_user);
                        $id_user = $id;
                        if ($jobIsPartOfCampain && $id_user > 0) {

                            $id_campaign = $jobIsPartOfCampain['id'];
                            $campaign_actions = $jModel->getGamificationActionsByCampaignId($id_campaign);
                            if (!empty($campaign_actions)) {
                                // 3 - Check  the id of $actions
                                $result = $jModel->getActionIDbyName($action);
                                $action_data = $jModel->getActionDataByCampainIdActionId($result['id'], $id_campaign);
                                if (empty($action_data)) return;
                                if ($action_data['active']) {
                                    if ($this->_model->checkValidation($action_data, $hash)) {
                                        $data['id_user'] = $id_user;
                                        $data['value'] = $action_data['points'];
                                        $data['id_campaign'] = $action_data['id_campaign'];
                                        $data['id_job'] = $id_job;
                                        $data['id_action'] = $action_data['id'];
                                        $data['id_job_view'] = $id_view;
                                        $modeStats = new models_Stats;
                                        $modeStats->saveCampaignReport($data);
                                    }
                                } else {
                                    $data['id_user'] = $id_user;
                                    $data['value'] = $action_data['points'];
                                    $data['id_campaign'] = $action_data['id_campaign'];
                                    $data['id_job'] = $id_job;
                                    $data['id_action'] = $action_data['id'];
                                    $data['id_job_view'] = $id_view;
                                    $modeStats = new models_Stats;
                                    $modeStats->saveCampaignReport($data);
                                }
//                                $data['id_user'] = $id_user;
//                                $data['value'] = $action_data['points'];
//                                $data['id_campaign'] = $action_data['id_campaign'];
//                                $data['id_job'] = $id_job;
//                                $data['id_action'] = $action_data['id'];
//                                $data['id_job_view'] = $id_view  ;
//                                $modeStats = new models_Stats;
//                                $modeStats->saveCampaignReport($data);
                            }

                        }
                    }
                }
//die('sddsd');
                /////////////////////////////////////////////

//                $session = new Zend_Session_Namespace('id');
//                $session->id = $id;
//                $session->id_company = $req['id_company'];
//                $this->saveNewUser($id, $req_2);
                $this->authenticateByEmail($req['email']);

//                Zend_Session::destroy('flow');
                //$this->_redirect('/register-email');
//                $this->_flashMessenger->addMessage($this->translate->_("account-pending"));
//                $this->_redirect('/upload');
//                $this->_redirect('/profile');
            } else {
                echo 'account-exists';
//                $msg = $this->translate->_("account-exists");
            }
        }
        return $id;
    }

    public function updateFlowSign($id_user, $linkedin_link)
    {
        $id_user = (int)$id_user;

        if ($linkedin_link != "")
            $this->update(['linkedin_link' => (string)$linkedin_link], "id = " . $id_user, "users");
    }

    public function updateUserById($id, $data)
    {
        $this->update($data, "id=" . (int)$id, "users");
    }

    public function getGamificationResultByIdAndAction($id_user, $action)
    {
        $sql = $this->select()
            ->from(['cad' => 'campaign_actions_detailed'], ['count(cad.id) as ' . $action])
            ->where('cad.id_recommender =?', (int)$id_user)
            ->where('cad.action =?', $action);
//        die($sql);
        return $this->fetchRow($sql)[$action];
    }

    public function importLinkedinconnections($csv)
    {
        $header = $csv[0];
        unset($csv[0]);

        foreach ($csv as $key => $value) {
            $data['firstname'] = $value[0];
            $data['lastname'] = $value[1];
            $data['email'] = $value[2];
            $data['company'] = $value[3];
            $data['position'] = $value[4];
            $data['connected'] = date_format(date_create($value[5]), 'Y-m-d');
            $data['id_user'] = Zend_Auth::getInstance()->getIdentity()->id;

            $userExist = $this->getLinkedinConnectionByEmail($data['email']);
            if (!empty($userExist)) {
                $this->update($data, "id={$userExist['id']}", 'linkedin_connections');
            } else {
                $this->insert($data, 'linkedin_connections');
            }
        }
    }

    public function getLinkedinConnectionByEmail($email)
    {
        $sql = $this->select()
            ->from(['lc' => 'linkedin_connections'], ['*'])
            ->where("lc.email =?", $email)
            ->where("lc.id_user =?", (int)Zend_Auth::getInstance()->getIdentity()->id);
        return $this->fetchRow($sql);
    }

    public function getMyLinkedinConnections()
    {
        $sql = $this->select()
            ->from(['lc' => 'linkedin_connections'], ["*"])
            ->where("lc.id_user=?", (int)$this->_user->id)
            ->where("lc.linked=?", (int)0);
        return $this->fetchAll($sql);
    }

    public function getUserConnectionsLinkedinById($id = null)
    {
        if (is_null($id)) {
            return array();
        }

        $sql = $this->select()
            ->from(array('u' => 'linkedin_connections'), array('*'))
            ->where("u.id = ? ", $id);

        return $this->fetchRow($sql);
    }

    public function uploadCsv($file)
    {
        $extension = substr($file['name'], strrpos($file['name'], ".") + 1);
        $file_no_ext = substr($file['name'], 0, strrpos($file['name'], "."));

        $file_name = $file['name'];
        $file_down = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;

//        move_uploaded_file($file['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/import/{$file_down}");
        move_uploaded_file($file['tmp_name'], APPLICATION_PATH."/storage/import/{$file_down}");
        return $file_down;
    }

    public function getAllAdminsCompany()
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ["*"])
            ->where('u.type = 2')
            ->where('u.id_company =?', (int)Zend_Auth::getInstance()->getIdentity()->id_company);
        return $this->fetchAll($sql);
    }

    public function setFaceBookAccessToken($token)
    {
        $this->update(['facebook_token' => $token], 'id = ' . (int)$this->_user->id, 'users');
    }

    public function setLinkedintoken($token)
    {
        $this->update(['linkedin_token' => $token], 'id = ' . (int)$this->_user->id, 'users');
    }

    public function setJobAutopost($id_job)
    {
        $exist = $this->getAutopostJobById($id_job);
        $data = ['id_user' => $this->_user->id,
            'id_job' => $id_job,
            'date' => date('Y-m-d H:i:s')
        ];
        if (empty($exist)) {
            $this->insert($data, 'autopost_job');
            return $this->lastInsertId();
        }
        return $exist['id'];
    }

    public function setJobLinkedinAutopost($id_job)
    {
        $exist = $this->getAutopostLinkedinJobById($id_job);
        $data = ['id_user' => $this->_user->id,
            'id_job' => $id_job,
            'date' => date('Y-m-d H:i:s')
        ];
        if (empty($exist)) {
            $this->insert($data, 'autopost_linkedin_job');
            return $this->lastInsertId();
        }
        return $exist['id'];
    }

    public function getAutopostJobById($id_job)
    {
        $sql = $this->select()
            ->from(['aj' => 'autopost_job'], ['*'])
            ->where('aj.id_user =?', (int)$this->_user->id)
            ->where('aj.id_job =?', (int)$id_job);

        $result = $this->fetchRow($sql);

        if (empty($result))
            return [];
        return $result;
    }

//    public function getAutopostLinkedinJobById($id_job)
//    {
//        $sql = $this->select()
//                ->from(['aj' => 'autopost_linkedin_job'],['*'])
//                ->where('aj.id_user =?',(int) $this->_user->id)
//                ->where('aj.id_job =?',(int) $id_job);
//        
//        $result = $this->fetchRow($sql);
//        
//        if(empty($result)) return [];
//        return $result;
//    }
    public function getAutopostLinkedinJobById($id_job)
    {
        $sql = $this->select()
            ->from(['aj' => 'autopost_linkedin_job'], ['*'])
            ->where('aj.id_user =?', (int)$this->_user->id)
            ->where('aj.id_job =?', (int)$id_job);

        $result = $this->fetchRow($sql);

        if (empty($result))
            return [];
        return $result;
    }

    public function updateAutoPostDate($id)
    {
        $this->update(['date' => date('Y-m-d H:i:s')], "id={$id}", 'autopost_job');
    }

    public function getCustomDepartment($users)
    {
        $users = (array)$users;
        foreach ($users as $index => &$value) {
            $sql = $this->select()
                ->from(['us' => 'user_departments'], ['*'])
                ->join(['cd' => 'custom_departments'], 'us.id_department = cd.id', ['*'])
                ->where("us.id_user =?", (int)$value['id']);
            $custom = $this->fetchAll($sql);
//                die($sql);
            $users[$index]['custom_department'] = $custom;
        }
        return $users;
    }

    public function updateRateById($id_rate, $data)
    {
        $this->update($data, "id=$id_rate", 'notification');
    }

    public function setInitalRate($data)
    {
        $this->insert($data, 'notification');
    }

    public function newCandidatePoints($id_user)
    {
        $action = "new_candidates";
        $id_user = (int)$id_user;
        $me = $this->getUserById($id_user);
        $cookie = $_COOKIE;
        $index = null;
        foreach ($cookie as $key => $value) {
            if (is_int($key)) {
                $index = $key;
                break;
            }
        }
        if (is_null($index))
            return;
        $data = json_decode($cookie[$index]);
        $id_job = (int)$data->id_job;
        $id_parent = $me['id_parent']; // id_recommender

        $points = $this->_stats->getScoringPoints();
        $sql = $this->select()->from(array("g" => "gamifications_actions"))->where("action = ?", $action);
        $result = $this->fetchRow($sql);
        $this->insert(array("id_user" => $id_parent,
            "action" => $result['id'],
            "points" => $points[$action],
            "id_item" => $id_job,
            "date" => date("Y-m-d H:i:s")), "gamification_points");
        $this->_stats->setCampaignReport($id_parent, $action, $id_job, $id_user);
    }

    public function setViewedRecommendation($track = null)
    {
        if ($track == null)
            return;
        $track = (string)$track;
        $sql = $this->select()
            ->from(['jr' => 'job_recommendations'])
            ->where('jr.track=?', $track);
        $recommendation = $this->fetchRow($sql);
        if (!empty($recommendation)) {
            $this->update(['viewed' => 1], "id = {$recommendation['id']}", 'job_recommendations');
        }
    }

    public function checkIfUserFromComapnyDomain($user)
    {
        $subdomain = explode('.', $_SERVER["SERVER_NAME"]);
        if (in_array("www", $subdomain)) {
            array_shift($subdomain);
        }
        $subdomain = $subdomain[0];
        if (strpos($user['email'], $subdomain) !== false) {
            $model = new models_User;
            $model->update(['type' => 1], 'id=' . $user['id'], 'users');
        }
    }

    public function addUserLanguage($id_user, $languqge)
    {
        $data['id_user'] = $id_user;
        $data['language'] = $languqge;
        $data['date'] = date('Y-m-d H:i:s');
        $sql = $this->insert($data, 'user_languages');
    }

    public function collectUserLang($id_user)
    {
        $sql = $this->select()
            ->from(['ul' => 'user_languages'], ['language'])
            ->where('ul.id_user =?', (int)$id_user);
        return $this->fetchRow($sql)['language'];
    }

    public function collecAlltUserLang($id_user)
    {
        $sql = $this->select()
            ->from(['ul' => 'user_languages'], ['language'])
            ->where('ul.id_user =?', (int)$id_user);
        return $this->fetchAll($sql);
    }

    public function getSettingsUsers($filters = array(), $length = null, $start = null, $search = "")
    {
        $modelCompany = new models_Company;
        $allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $id_user = Zend_Auth::getInstance()->getIdentity()->id;

        $sql = $this->select();

        $sql->from(array('u' => 'users'), array('*', "CONCAT(u.firstname,' ',u.lastname) as username"
//            "(SELECT dl.title FROM departments_lang dl, user_departments ud where ud.id_department = dl.id_department AND ud.id_user = u.id) AS departments",
//            "(SELECT cd.title FROM custom_departments cd , user_departments ud where ud.id_department = cd.id AND ud.id_user = u.id AND cd.id_company = {$this->_user->id_company}) AS custom_departments"
        ))->order('id DESC');


        $sql->joinLeft(array("uu" => "users"), "u.id_parent = uu.id", array("CONCAT(uu.firstname,' ',uu.lastname) as adder_name"));

        if ($this->_user->type == 2) {
            $sql->where("u.id_company = ?", $this->_user->id_company);
        }
        $sql->where("u.type != 3");

        if ($search != "") {
            $str = (string)"u.email LIKE '%{$search}%' OR u.lastname LIKE '%{$search}%'";

//            echo $str;die;
            $sql->where($str);
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                switch ($key) {
                    case "type":
                        if ($value == 9) {
                            $sql->where("u.type = 1");
                            $sql->where("u.id_parent > 0");
                        } else {
                            $sql->where("u.type = ?", (int)$value);
                            $sql->where("u.id_parent = 0");
                        }
                        break;
                }
            }
        }

        if (!$allow_candidate_recommend) {
            $sql->where("u.type != 9");
        }
        $count = count($this->fetchAll($sql));
        $sql->limit($length, $start);
        $sql->order('u.created DESC');

        $sql_result['items'] = $this->fetchAll($sql);
        $sql_result['count'] = $count;
        return $sql_result;
    }

    public function getInductryById($id_industry)
    {
        $sql = $this->select()
            ->from(['i' => 'industries'], [])
            ->join(['il' => 'industries_lang'], 'i.id=il.id_industry', ['title'])
            ->where('i.id =?', (int)$id_industry);
        return $this->fetchRow($sql);
    }

    public function getUserLanguages($id_user)
    {
        $sql = $this->select()
            ->from(['ul' => 'user_languages'], ['language', 'rating as rating_index', 'id'])
            ->joinLeft(['r' => 'rating'], 'r.id=ul.rating', ['title as rating'])
            ->where('ul.id_user =?', (int)$id_user)->order('rating_index DESC');
        return $this->fetchAll($sql);
    }

    public function getUserReference($id_user)
    {
        $sql = $this->select()
            ->from(['ur' => 'user_references'], ['*'])
            ->where('ur.id_user =?', (int)$id_user);
        return $this->fetchAll($sql);
    }

    public function getUserSkillsWithRating($id_user)
    {
        $sql = $this->select()
            ->from(array('s' => 'skills'), array('*'))
            ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title", "slug"))
            ->join(array("us" => "user_skills"), "s.id = us.id_skill", array('skill_years', 'rating as rating_index'))
            ->joinLeft(array('r' => 'rating'), 'r.id=us.rating', ['title as rating'])
            ->where("us.id_user = ?", (int)$id_user)
            ->order('id DESC');

        return $this->fetchAll($sql);
    }

    function getUserLinkCv($id)
    {
        $sql = $this->select()->from(array("uc" => "user_cv"), array("file", "title"))
            ->where("uc.id_user =?", (int)$id)->order("uc.date DESC");

        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value) {
            $cv = '/ajax/index/read-file?cv=' . $value['file'];
//            $cv = '/resources/files/cv/' . $value['file'];
            $result[$key]['link'] = $cv;
        }
        return $result;
    }

    public function DisableJobIfHeadCountReached($id_applications)
    {

        $model_job = new models_Jobs;
        $job = $model_job->getJobApplicationById($id_applications);
        $sql = $this->select()->from(['jhc' => 'job_headcount'], ['headcount'])
            ->where('jhc.id_job =?', (int)$job['id_job']);
        $headount = (int)$this->fetchRow($sql)['headcount'];
        $sql = $this->select()->from(['jp' => 'job_applications'])
            ->where('jp.id_job =?', (int)$job['id_job'])
            ->where('jp.status =5');
        $all_hired_job = count($this->fetchAll($sql));
        if ($headount == $all_hired_job) {
            $model_job->update(['status' => 0], 'id=' . $job['id_job'], 'jobs');
            $sql = $this->select()->from(['ja' => 'job_applications'], ['*'])
                ->where("ja.status NOT IN (" . implode(",", [5]) . ") || ja.status_reason IN (" . implode(",", [2, 4, 3, 11, 16, 21, 28]) . ")")
                ->where("ja.id_job =?", (int)$job['id_job']);
            $result = $this->fetchAll($sql);
            foreach ($result as $key => $application) {
                $userObject = Domain_User_UserFactory::createInstance($application['id_user']);
                $userObject->sendCandidateUnfitStatusChange($application);
                $status = 16;
                $status_reason = 19;
                $this->update(
                    ['status' => $status,
                        'status_reason' => $status_reason],
                    "id={$application['id']}",
                    "job_applications");
                $this->setjobApplicationStatusHistory($job['id_job'], $application['id'], $status, $status_reason);
            }
        }
    }

    public function getArrayFromDate($start, $end)
    {
        $begin = new DateTime($start);
        $end = new DateTime($end);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $array = [];
        foreach ($period as $dt) {
            $array[] = $dt->format("Y-m-d");
        }
        return $array;
    }

    public function calculatePercentFromTwoVal($currentData = null, $lastData = null)
    {
        $newFigure = $lastData;
        $oldFigure = $currentData;
        if (($newFigure != 0)) {
            $percentChange = ($oldFigure / $newFigure - 1) * 100; //current/old-1 to get incremental change percentage
        } else {
//            $percentChange = -($oldFigure * 100);
            $percentChange = "n/a";
        }
//        return number_format($percentChange, 2);
        return $percentChange;;

//        $lastData = 1;
//        $currentData = 20;
        if ($lastData == 0) {
            $val = 100 * $currentData;
            return -$val;
            return number_format(0, 2);
        }
        $percentChange = (1 - ($currentData / $lastData)) * 100;

        return number_format($percentChange, 2);
    }

    public function getStatsCreatedUsers($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['u' => 'users'], ['count(id) as numbers']);
            $sql->where("date_format(u.created,'%Y-%m-%d') <= '{$day}'");
            $sql->where("u.type != 9 ");

            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countCreatedUserByPeriod($start = null, $end = null)
    {

        $sql = $this->select()
            ->from(['u' => 'users'], ['count(id) as numbers']);
        $sql->where("u.type != 9");
//        if (null !== $start) { //we want all users created before period ends (cumulative)
//            $sql->where("u.created >= '{$start}'");
//        }
        if (null !== $end) {
            $sql->where("u.created <= '{$end}'");
        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getEngagedEmployees($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['u' => 'users'], ['count(id) as numbers']);
            $sql->where("date_format(u.first_login,'%Y-%m-%d') <= '{$day}'"); //equal or smaller than date to get cumulated numbers and added non-bounced filter and changed last_login with first_login = total number of users that could share a job ...
            $sql->where("u.type != 9 and (u.bounce_date >= '{$start}' or u.bounce_date is null or u.bounce_date ='')");
//            die($sql);
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countEngagementEmployeeByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(id) as numbers', 'u.last_login']);
//        if (null !== $start) {
//            $sql->where("u.first_login >= '{$start}'"); //first_login instead of las_login
//        }
        if (null !== $end) {
            $sql->where("u.first_login <= '{$end}'"); //first_login instead of las_login
        }
        $sql->where("(u.type != 9 and u.bounce_date >='{$start}') or (u.type != 9 and u.bounce_date is null) or (u.type != 9 and u.bounce_date ='')"); //add bounce_date filter
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getEmployeesEvanghelists($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['js' => 'jobs_share'], ['count(distinct js.id_user) as numbers']);
            $sql->join(['u' => 'users'], 'js.id_user=u.id', []);
            $sql->where("js.date <= '{$day}'"); //equal or smaller than date to get cumulated numbers
            $sql->where("js.date >= '{$start}'"); //equal or bigger than start date 
            $sql->where('u.type < 9');
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countEmployeesEvanghelistsByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['js' => 'jobs_share'], ['count(distinct js.id_user) as numbers']);
        $sql->join(['u' => 'users'], 'u.id=js.id_user', []);
        if (null !== $start) {
            $sql->where("js.date>= '{$start}'");
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'");
        }
        $sql->where('u.type != 9');
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getActiveEmployees($start = null, $end = null)
    {
//        $this->updateBounces(); //set cron to run this sendgrid update function 
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['u' => 'users'], ['count(id) as numbers']);
            $sql->where("u.created <= '{$day}'"); //changed last_login with created => active employees = emplyees with no bounced = 1
            $sql->where("(u.type != 9 and u.bounce_date >'{$day}') or (u.type != 9 and u.bounce_date is null) or (u.type != 9 and u.bounce_date ='')");
//            $sql->where('u.bounced != 1');
//            die($sql);
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countActiveEmployeesByPeriod($start, $end)
    {
//        $subdomain='unicredit';
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(id) as numbers']);
        $sql->where("u.created<='{$end}' and (u.type != 9 and u.bounce_date >'{$start}') or (u.type != 9 and u.bounce_date is null) or (u.type != 9 and u.bounce_date ='')"); //check bounce_date to show actively working employees
//        $sql->where("u.email LIKE ?", "%{$subdomain}%");
//        $sql->where("u.bounced != 1");
//        if (null !== $start) {
//            $sql->where("first_login >= '{$start}'"); // replaced last_login with first_login; no  date format needed
//        }
//        if (null !== $end) {
//            $sql->where("first_login <= '{$end}'"); // replaced last_login with first_login; no  date format needed
//        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getActiveJobs($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['j' => 'jobs'], ['count(j.id) as numbers']);
            $sql->where("date_format(j.end,'%Y-%m-%d') >= '{$day}'");
            $sql->where('j.status = 1');
//            die($sql);
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countActiveJobsByPeriod($start = null, $end = null)
    {

        $sql = $this->select()
            ->from(['j' => 'jobs'], ['count(id) as numbers']);
//        $sql->where("j.status =  1"); 
        if (null !== $start) {
            $sql->where("j.end >= '{$start}'");  //jobs could've ended before end_date => they need to be non-expired
        }
//        if (null !== $end) {
//            $sql->where("date_format(j.end,'%Y-%m-%d') <= '{$end}'");
//        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getJobsShared($start = null, $end = null)
    { //zero results
        $sql = $this->select()
            ->from(['j' => 'jobs'], [])
            ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
            ->joinLeft(['js' => 'jobs_share'], 'js.id_job=jl.id_job', ['count(js.id) as number'])
            ->order('number DESC')//added sorting like in google sheet mockup chart
            ->group('j.id');
        if (null !== $start) {
            $sql->where("js.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'"); //date format removed
        }
        return $this->fetchPairs($sql);
    }

    public function CountSharedJobsByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'], [])
            ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
            ->joinLeft(['js' => 'jobs_share'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
        if (null !== $start) {
            $sql->where("js.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

//    public function getViewedJobs($start = null, $end = null) {
//        $sql = $this->select()
//                ->from(['j' => 'jobs'], []) 
//                ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
//                ->joinLeft(['js' => 'job_views'], 'js.id_job=jl.id_job', ['count(js.id) as number'])
//                ->group('j.id'); 
//        if (null !== $start) {
//            $sql->where("js.date >= '{$start}'");//date format removed
//        }
//        if (null !== $end) {
//            $sql->where("js.date <= '{$end}'");//date format removed
//        }
//        return $this->fetchPairs($sql);
//    }
    public function getViewedJobs($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['j' => 'job_views'], ['count(j.id_job) as number'])
            ->join(['jl' => 'jobs_lang'], 'j.id_job=jl.id_job', ['title'])
            ->group('j.id_job')
            ->order('number DESC');
        if (null !== $start) {
            $sql->where("j.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("j.date <= '{$end}'"); //date format removed
        }
        return $this->fetchPairs($sql);
    }

    public function countViewedJobsByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['j' => 'job_views'], ['count(distinct j.id_job) as number'])
            ->join(['jl' => 'jobs_lang'], 'j.id_job=jl.id_job', ['title']);
        if (null !== $start) {
            $sql->where("j.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("j.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

//
//    public function countViewedJobsByPeriod($start = null, $end = null) {
//        $sql = $this->select()
//                ->from(['j' => 'jobs'], [])
//                ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
//                ->joinLeft(['js' => 'job_views'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
//        if (null !== $start) {
//            $sql->where("js.date >= '{$start}'");//date format removed
//        }
//        if (null !== $end) {
//            $sql->where("js.date <= '{$end}'");//date format removed
//        }
//        return $this->fetchRow($sql)['number'];
//    }

    public function getJobApplications($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(ja.id_job) as numbers'])
            ->join(['jl' => 'jobs_lang'], 'ja.id_job=jl.id_job', ['title'])
            ->group('ja.id_job')
            ->order('numbers DESC');

        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        return $this->fetchPairs($sql);
    }

//
//    public function getJobApplications($start = null, $end = null) {
//        $period = $this->getArrayFromDate($start, $end);
//        $return = [];
//        foreach ($period as $day) {
//            $sql = $this->select()
//                    ->from(['ja' => 'job_applications'], ['count(ja.id) as numbers']);
//            $sql->where("date_format(ja.date,'%Y-%m-%d') = '{$day}'");
//            $result = $this->fetchRow($sql);
//            $data = [
//                'date' => $day,
//                'numbers' => $result['numbers']
//            ];
//            array_push($return, $data);
//        }
//        return $return;
//    }

    public function countJobApplicationsByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_job) as numbers']);
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getJobsViews($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['jv' => 'job_views'], ['count(jv.id) as numbers']);
            $sql->where("jv.date <= '{$day}'"); //date format removed
            $sql->where("jv.date >= '{$start}'"); //date format removed
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function getJobsShares($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['js' => 'jobs_share'], ['count(js.id) as numbers'])
                ->join(['u' => 'users'], 'u.id=js.id_user', [])
                ->where('u.type!=9')
            ;
            $sql->where("js.date <= '{$day}'"); //date format removed
            $sql->where("js.date >= '{$start}'"); //date format removed
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countJobsViews($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['jv' => 'job_views'], ['count(jv.id) as numbers']);
        if (null !== $start) {
            $sql->where("jv.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jv.date <= '{$end}'"); //date format removed
        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countJobsShares($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['js' => 'jobs_share'], ['count(js.id) as numbers'])
            ->join(['u' => 'users'], 'u.id=js.id_user', [])
            ->where('u.type<9')
        ;
        if (null !== $start) {
            $sql->where("js.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'"); //date format removed
        }
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function getPotentialCandidates($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['jv' => 'job_views'], ['count(distinct jv.fingerprint) as numbers']);
            $sql->where("jv.date <= '{$day}'"); //date format removed
            $sql->where("jv.date >= '{$start}'"); //date format removed
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;

//        $sql = $this->select()
//                ->from(['j' => 'jobs'], [])
//                ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
//                ->joinLeft(['js' => 'job_views'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
//        $sql->group('js.id_job');
//        if (null !== $start) {
//            $sql->where("js.date >= '{$start}'");//date format removed
//        }
//        if (null !== $end) {
//            $sql->where("js.date <= '{$end}'");//date format removed
//        }
//        return $this->fetchPairs($sql);
    }

    public function countPotentialCandidatesByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['jv' => 'job_views'], ['count(distinct jv.fingerprint) as number']);

//        $sql = $this->select()
//                ->from(['j' => 'jobs'], [])
//                ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
//                ->joinLeft(['js' => 'job_views'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
//        $sql->group('js.id_job');
        if (null !== $start) {
            $sql->where("jv.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jv.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

    public function getTotalApplications($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['ja' => 'job_applications'], ['count(distinct ja.id) as numbers'])
                ->join(['u' => 'users'], 'u.id=ja.id_user', [])  // more unique id_user in ja than found in users ....!?
            ;
            $sql->where("ja.date <= '{$day}'"); //date format removed
            $sql->where("ja.date >= '{$start}'"); //date format removed
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;


//        $sql = $this->select()
//                ->from(['j' => 'jobs'], [])//not necessary and should've been by date
//                ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])//not necessary
//                ->joinLeft(['ja' => 'job_applications'], 'ja.id_job=jl.id_job', ['count(ja.id) as number']);
//        $sql->group('ja.id_job');
//        if (null !== $start) {
//            $sql->where("date_format(ja.date,'%Y-%m-%d') >= '{$start}'");
//        }
//        if (null !== $end) {
//            $sql->where("date_format(ja.date,'%Y-%m-%d') <= '{$end}'");
//        }
//        return $this->fetchPairs($sql);
    }

    public function countTotalApplicationsByPeriod($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],  ['count(ja.id) as number'])
            ->join(['u' => 'users'], 'u.id=ja.id_user', [])
        ;
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

    public function getUniqueCandidate($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['ja' => 'job_applications'], ['count(distinct ja.id_user) as numbers'])
                ->join(['u' => 'users'], 'u.id=ja.id_user', ['if(u.type = 9, count(distinct u.id),0) as candidates',
                    'if(u.type = 9,0, count(distinct u.id)) as employees'])
                ->group('u.type')
            ;
            $sql->where("ja.date <= '{$day}'"); //date format removed
            $sql->where("ja.date >= '{$start}'");
            $result = $this->fetchAll($sql);
            $numbers=0;
            $candidates=0;
            $employees=0;
            foreach ($result as $r){
                $numbers=$r['numbers']+$numbers;
                $candidates+=$r['candidates'];
                $employees+=$r['employees'];

            }
            $data = [
                'date' => $day,
                'numbers' => $numbers,
                'candidates' => $candidates,
                'employees' => $employees,
            ];
            array_push($return, $data);

        }
        return $return;
    }

    public function getNewCandidateAccounts($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['u' => 'users'], ['count(distinct u.id) as numbers'])
                ->where('u.type = 9');
            $sql->where("u.created <= '{$day}'");
            $sql->where("u.created >= '{$start}'");
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }


    public function getInRecruitingProcessApplications($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['ja' => 'job_applications'], ['count(ja.id) as numbers'])
                ->join(['u' => 'users'], 'u.id=ja.id_user', [])
                ->join(['jh' => 'job_application_status_history'], 'jh.job_application_id=ja.id', ['jh.status', 'jh.date'])
//                ->where('u.type = 9')
                ->where('jh.status not in (1,2,6,16)') //called applicant that moved to next phase in recruiting process
            ;
            $sql->where("jh.date <= '{$day}' and jh.date >= '{$start}' and jh.date <= '{$end}'");
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }


    public function getApplicantsByStatus($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], [])
            ->join(['u' => 'users'], 'u.id=ja.id_user', [])
            ->join(['jh' => 'job_application_status_history'], 'jh.job_application_id=ja.id', ['count(jh.id) as numbers'])
            ->join(['s' => 'status'], 's.id=jh.status', ['s.status as step'])
//            ->where('u.type = 9')
            ->order('s.column_order ASC')
            ->group('jh.status')//                ->where('jh.status not in (1,2,6,16)') //called applicant that moved to next phase in recruiting process
        ;
        $sql->where("jh.date >= '{$start}' and jh.date <= '{$end}'");
        $result = $this->fetchAll($sql);
        return $result;
    }


    public function getCandidateAccounts($start = null, $end = null)
    {
        $period = $this->getArrayFromDate($start, $end);
        $return = [];
        foreach ($period as $day) {
            $sql = $this->select()
                ->from(['u' => 'users'], ['count(distinct u.id) as numbers'])
                ->where('u.type = 9');
            $sql->where("u.created <= '{$day}'");
            $result = $this->fetchRow($sql);
            $data = [
                'date' => $day,
                'numbers' => $result['numbers']
            ];
            array_push($return, $data);
        }
        return $return;
    }

    public function countNewCandidateAccounts($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(distinct u.id) as number'])
            ->where('u.type = 9');
        if (null !== $start) {
            $sql->where("u.created >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("u.created <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }


    public function countInRecruitingProcessApplications($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(ja.id) as number'])
            ->join(['u' => 'users'], 'u.id=ja.id_user', [])
            ->join(['jh' => 'job_application_status_history'], 'jh.job_application_id=ja.id', ['jh.status', 'jh.date'])
//            ->where('u.type = 9')
            ->where('jh.status not in (1,2,6,16)')  //called applicant that moved to next phase in recruiting process
        ;
        if (null !== $start) {
            $sql->where("jh.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jh.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

    public function countApplicantsByStatus($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], [])
            ->join(['u' => 'users'], 'u.id=ja.id_user', [])
            ->join(['jh' => 'job_application_status_history'], 'jh.job_application_id=ja.id', ['count(jh.id) as number'])
//            ->where('u.type = 9')
        ;
        if (null !== $start) {
            $sql->where("jh.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jh.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }


    public function countCandidateAccounts($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(distinct u.id) as number'])
            ->where('u.type = 9');
//        if (null !== $start) {
//            $sql->where("u.created >= '{$start}'"); //date format removed
//        }
        if (null !== $end) {
            $sql->where("u.created <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }


    public function countUniqueCandidate($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_user) as number', 'ja.id_user'])
            ->join(['u' => 'users'], 'u.id=ja.id_user', []); //remove date ja.date and set count distinct of id_users not id only
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        return $this->fetchRow($sql)['number'];
    }

    public function getDashboardTopChannels($start = null, $end = null)
    {

        $sql = $this->select()
            ->from(['sm' => 'social_media'], ['sm.title', 'sm.id as id_source',
//                "(SELECT ja.id_user from job_applications ja where ja.date >= \'{$start}\' and ja.date <= \'{$end}\' group by ja.id_user) as id_user",
//                    "(SELECT COUNT(ja.id) from job_applications ja where sm.id=ja.source group by ja.source ) as applicants"])//as applications and  no date filters were here
                "(SELECT COUNT(distinct ja.id) from job_applications ja inner join users u on u.id=ja.id_user where sm.id=ja.source and ja.date >= '{$start}' and ja.date <= '{$end}' group by ja.source) as applications",
                "(SELECT COUNT(distinct jv.id) from job_views jv  inner join users u on u.id=jv.id_user  where sm.id=jv.source and jv.date >= '{$start}' and jv.date <= '{$end}' group by jv.source) as views"
            ])
//            ->join(['u' => 'users'], 'u.id=sm.id_user', [])
//                ->joinLeft(['jv' => 'job_views'], "sm.id=jv.source", ['COUNT(jv.id) as views'])
//                ->joinLeft(['ja' => 'job_applications'], "sm.id=ja.source", ['COUNT(ja.id) as applications'])
            ->where('sm.title != "internal"')
            ->order('views DESC')
            ->group('sm.id');
//        if (null !== $start) {
//            $sql->where("jv.date >= '{$start}'");//date format removed
//            $sql->where("ja.date >= '{$start}'");//date format removed
//        }
//        if (null !== $end) {
//            $sql->where("jv.date <= '{$end}'");//date format removed
//            $sql->where("ja.date <= '{$end}'");//date format removed
//        }
        $result = $this->fetchAll($sql);
        foreach ($result as $k => &$value) {
            if ($value['applications'] == null) {
                $value['applications'] = 0;
            }

            if ($value['views'] == null) {
                $value['views'] = 0;
            }
        }
        return $result;
    }

    public function addNewUser($user)
    {
        $salt = uniqid();
        $password = "Talentspotting2018!";
        $hashed_password = sha1($password) . $salt;
        $user['password'] = $hashed_password;
        $user['salt'] = $salt;
        $user['created'] = date("Y-m-d H:i:s");
        $user['type'] = 9;
        $user['status'] = 1;
        $user['id_company'] = Zend_Auth::getInstance()->getIdentity()->id_company;
        $this->insert($user, 'users');
        return (int)$this->lastInsertId();
    }

    public function setUserLangauge($language, $id_user)
    {
        foreach ($language as $lang) {
            $data['date'] = date('Y-m-d H:i:s');
            $data['id_user'] = $id_user;
            $data['language'] = $lang['language'];
            $data['rating'] = $lang['rating'];
            $this->insert($data, 'user_languages');
        }
    }

    public function addUserPositions($positions, $id_user = null)
    {
        if (!empty($positions)) {
            foreach ($positions as $position) {
                $position['id_department'] = $position['id_department[]'];
                unset($position['id_department[]']);
                unset($position['location']);
                $position['id_user'] = $id_user;
                $this->insert($position, "user_positions");
            }
        }
    }

    public function addUserEducations($educations, $id_user = null)
    {
        if (!empty($educations)) {
            foreach ($educations as $education) {
                $education['id_user'] = $id_user;
                $this->insert($education, "user_education");
            }
        }
    }

    public function addUserSkills($skills, $id_user = null)
    {

        if (!empty($skills)) {
            foreach ($skills as $skill) {
                $skill['id_user'] = $id_user;
                $skill['id_skill'] = $skill['id_skill[]'];
                $skill['skill_years'] = $skill['years'];
                unset($skill['id_skill[]']);
                unset($skill['years']);
                $this->insert($skill, "user_skills");
            }
        }
    }

    public function getChannelByTitle($title)
    {
        $sql = $this->select()->from(['sc' => 'social_media'], ['*']);
        $sql->where("sc.title =?", $title);
//        echo $title;
        return $this->fetchRow($sql);
    }

    public function addUser($data, $send_mail = false, $id_email = 1)
    {
        $session = new Zend_Session_Namespace("track");
        $new_password_confirm = $data['new_password_confirm'];
        $original_password = $data['original_password'];
        $new_password = $data['new_password'];
        $jobs = $data['jobs'];
        unset($data['new_password_confirm']);
        unset($data['original_password']);
        unset($data['new_password']);
        unset($data['jobs']);

        $department = $data['department'];
        unset($data['department']);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            if ($data['password'] == "") {
                unset($data['password']);
            } else {
                $data['password'] = sha1($data['password']) . $this->_user->salt;
            }
            if (count($data) > 0) {
                $this->update($data, "id = $id");
            }
        } else {
            $password = $data['password'];
            $data['status'] = isset($data['status']) ? $data['status'] : 0;
            $data['hash'] = sha1(time());
            $data['salt'] = uniqid(mt_rand(), false);
            $data['password'] = sha1($data['password']) . $data['salt'];
            $data['created'] = date('Y-m-d H:i:s');
            $data['first_login'] = date('Y-m-d H:i:s');
            unset($data['id']);
            $this->insert($data);
            $id = $this->lastInsertId();


            $url = $_SESSION['url']['url'];
            $parts = parse_url($url);

            parse_str($parts['query'], $query);
            $source = ($query['source']);

            if ($source) {
                $sql = $this->select()->from(array("u" => "users"));
                if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
                    $sql->where("SUBSTRING(sys.fn_sqlvarbasetostr(HASHBYTES('MD5','u.id')),3,32) = ?", (string)$session->track)->limit(1);
                } else {
                    $sql->where("MD5(u.id) = ?", (string)$session->track)->limit(1);
                }
                $user_id = $this->fetchRow($sql)['id'];
                $session->track = uniqid();
                $job_id = explode('/', $_SESSION['url']['url'])[2];
                $this->insert(array("id_recommender" => $user_id, "track" => $session->track, 'id_job' => $job_id), "job_recommendations");
            }
            if (isset($session->track) && $session->track != "") {
                $this->update(array("id_user" => $id), "track = '{$session->track}'", "job_recommendations");
//                $this->_stats->setGamificationPoint($id, "job_recommend");
            }
//            $data = $this->assignSendData($data, $send_mail, $id_email, $id);
            $user = Domain_User_UserFactory::createInstance($id);
            $user->sendWelcomeEmail();

        }
        if ($data['type'] == 5) {
            $this->setatsUserDepartment($id, $department);
        }

        if ($data['type'] == 4) {
            $this->assignJobsForRecuiter($id, $jobs, $department);
        }
        if (isset($department)) {
            $this->delete("id_user = $id", "user_departments");
            foreach ($department as $value) {
                $this->insert(array("id_user" => $id, "id_department" => $value), "user_departments");
            }
        } else {
            $this->delete("id_user = $id", "user_departments");
        }

        $this->saveImage($id);
        return $id;
    }

    public function regenerateUserPassword($password, $id_user)
    {
        $data['salt'] = uniqid();
        $data['password'] = sha1($password) . $data['salt'];
        $this->update($data, "id={$id_user}", 'users');
    }


    public function disableBouncedUser(array $bounced)
    {
        foreach ($bounced as $key => $user) {
            $exist_user = $this->getUserDataByEmail($user['email']);
            if (!empty($exist_user)) {
                $id = $exist_user['id'];
                $bounce_date = date('Y-m-d H:i:s', $user['created']);
                $data['bounced'] = 1;
                $data['bounce_date'] = $bounce_date;
                $data['status'] = 0;
                $this->update($data, "id=$id", 'users');
            }
        }
    }

    public function getSuccefulRecommendersByPeriod($start, $end)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['ja.updated as date'])
            ->join(['u' => 'users'], "u.id=ja.id_recommender", ["CONCAT(firstname, ' ', lastname) AS user"])
            ->join(['j' => 'jobs'], 'j.id=ja.id_job', [])
            ->join(['b' => 'bounties'], 'b.id_job=ja.id_job', [])
            ->where("ja.updated BETWEEN '{$start}' AND '{$end}'")
            ->where("u.type = 1")
            ->where("b.status !=?", (string)'paid')
            ->where("j.bounty_title IS NOT NULL");
        return $this->fetchAll($sql);
    }

    public function getCampaignWinnerByPeriod($start, $end)
    {
        $sql = $this->select()
            ->from(array("u" => "users"), array(
                "CONCAT(firstname, ' ', lastname) AS user"
            ))
            ->join(array("cad" => "campaign_bounty"), "cad.id_user = u.id", array('cad.end as date'))
            ->join(array('c' => 'campaigns'), "c.id=cad.id_campaign", ['title'])
            ->where("cad.status ='paid'")
            ->where("cad.end BETWEEN '{$start}' AND '{$end}'");
        return $this->fetchAll($sql);
    }

    public function countActiveEmployeesByPeriodUserId($start, $end, $id_user)
    {
//        $subdomain='unicredit';
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(id) as numbers']);
        $sql->where("u.created<='{$end}' and (u.type != 9 and u.bounce_date >'{$start}') or (u.type != 9 and u.bounce_date is null) or (u.type != 9 and u.bounce_date ='')"); //check bounce_date to show actively working employees
//        $sql->where("u.email LIKE ?", "%{$subdomain}%");
//        $sql->where("u.bounced != 1");
//        if (null !== $start) {
//            $sql->where("first_login >= '{$start}'"); // replaced last_login with first_login; no  date format needed
//        }
//        if (null !== $end) {
//            $sql->where("first_login <= '{$end}'"); // replaced last_login with first_login; no  date format needed
//        }
        $sql->where("u.id_parent =?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countEngagementEmployeeByPeriodUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ['count(id) as numbers', 'u.last_login']);
//        if (null !== $start) {
//            $sql->where("u.first_login >= '{$start}'"); //first_login instead of las_login
//        }
        if (null !== $end) {
            $sql->where("u.first_login <= '{$end}'"); //first_login instead of las_login
        }
        $sql->where("(u.type != 9 and u.bounce_date >='{$start}') or (u.type != 9 and u.bounce_date is null) or (u.type != 9 and u.bounce_date ='')"); //add bounce_date filter
        $sql->where("u.id_parent =?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countEmployeesEvanghelistsByPeriodUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['js' => 'jobs_share'], ['count(js.id) as numbers']);
        $sql->join(['u' => 'users'], 'u.id=js.id_user', []);
        if (null !== $start) {
            $sql->where("js.date>= '{$start}'");
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'");
        }
        $sql->where('u.type != 9');
        $sql->where("u.id_parent =?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countActiveJobsByPeriodUserId($start = null, $end = null, $id_user = null)
    {

        $sql = $this->select()
            ->from(['j' => 'jobs'], ['count(j.id) as numbers']);
//        $sql->where("j.status =  1");
        if (null !== $start) {
            $sql->where("j.end >= '{$start}'");  //jobs could've ended before end_date => they need to be non-expired
        }
//        if (null !== $end) {
//            $sql->where("date_format(j.end,'%Y-%m-%d') <= '{$end}'");
//        }
        $sql->join(['rj' => 'recruiter_job'], "rj.id_job=j.id", [])
            ->where("rj.id_user=?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function CountSharedJobsByPeriodUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'], [])
            ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
            ->joinLeft(['js' => 'jobs_share'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
        if (null !== $start) {
            $sql->where("js.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'"); //date format removed
        }
        $sql->join(['rj' => 'recruiter_job'], "rj.id_job=j.id", [])
            ->where("rj.id_user=?", (int)$id_user);
        return $this->fetchRow($sql)['number'];
    }

    public function countJobApplicationsByPeriodUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_job) as numbers']);
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        $sql->join(['rj' => 'recruiter_job'], "rj.id_job=ja.id_job", [])
            ->where("rj.id_user=?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countJobsViewsUserIdUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['jv' => 'job_views'], ['count(jv.id) as numbers']);
        if (null !== $start) {
            $sql->where("jv.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jv.date <= '{$end}'"); //date format removed
        }

        $sql->join(['rj' => 'recruiter_job'], "rj.id_job=jv.id_job", [])
            ->where("rj.id_user=?", (int)$id_user);

        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countUniqueCandidateUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_user) as number', 'ja.id_user']); //remove date ja.date and set count distinct of id_users not id only
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }

        $sql->join(['rj' => 'recruiter_job'], "rj.id_job=ja.id_job", [])
            ->where("rj.id_user=?", (int)$id_user);
        return $this->fetchRow($sql)['number'];
    }

    public function countViewersByPeriodUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['jv' => 'job_views'], ['count(jv.id) as numbers']);
        if (null !== $start) {
            $sql->where("jv.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jv.date <= '{$end}'"); //date format removed
        }

        $sql->where("jv.id_user=?", (int)$id_user);

        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function CountSharedJobsByUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'], [])
            ->join(['jl' => 'jobs_lang'], 'j.id=jl.id_job', ['title'])
            ->joinLeft(['js' => 'jobs_share'], 'js.id_job=jl.id_job', ['count(js.id) as number']);
        if (null !== $start) {
            $sql->where("js.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("js.date <= '{$end}'"); //date format removed
        }
        $sql->where("js.id_user=?", (int)$id_user);
        return $this->fetchRow($sql)['number'];
    }

    public function countJobsViewsUserByUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['jv' => 'job_views'], ['count(jv.id) as numbers']);
        if (null !== $start) {
            $sql->where("jv.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("jv.date <= '{$end}'"); //date format removed
        }

        $sql->where("jv.id_user=?", (int)$id_user);

        return (int)$this->fetchRow($sql)['numbers'];
    }


    public function countJobApplicationsByUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_job) as numbers']);
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        $sql->where("ja.id_user=?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function countJobApplicationsByUserIdPeriod($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_job) as numbers']);
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        $sql->where("ja.id_user=?", (int)$id_user);
        return (int)$this->fetchRow($sql)['numbers'];
    }


    public function countHiredApplicationsByUserId($start = null, $end = null, $id_user = null)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'], ['count(distinct ja.id_job) as numbers']);
        if (null !== $start) {
            $sql->where("ja.date >= '{$start}'"); //date format removed
        }
        if (null !== $end) {
            $sql->where("ja.date <= '{$end}'"); //date format removed
        }
        $sql->where("ja.id_user=?", (int)$id_user);
        $sql->where("ja.status=?", (int)5);
        return (int)$this->fetchRow($sql)['numbers'];
    }

    public function checkActivatedUser($email)
    {
        $user = $this->getUserDataByEmail($email);

        if (empty($user)) {
            throw new Exception('System Error , user dosent exist');
        }

        if ($user['verified'] == 1) {
            return true;
        }
        return false;
    }

    public function getAllRecruitersByCompany()
    {
        $sql = $this->select()->from(array("u" => "users"), array("u.id", "u.firstname", "u.email"))
            ->where("u.status =?", 1)
            ->where("u.type =?", 4);
        return $this->fetchAssoc($sql);
    }

    public function getAllAdminsByCompany()
    {
        $sql = $this->select()
            ->from(['u' => 'users'], ["*"])
            ->where('u.type =2')
            ->where('u.status =1');
//            ->where('u.id_company =?', (int) Zend_Auth::getInstance()->getIdentity()->id_company);
        return $this->fetchAll($sql);
    }
}
