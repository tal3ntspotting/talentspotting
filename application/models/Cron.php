<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cron
 *
 * @author edin
 */
class models_Cron extends WebApp_Db_Table {

//put your code here
    private $_settings = array();
    public $_name = 'emails';
    public $_user = '';
    protected $_primary = 'id';
    protected $_company;

    function __construct() {
        parent::__construct();
        $this->getCompanyBySubdomainSlug();
        $this->_company = new models_Company;
    }

    public function getEmailByType($type) {
        $sql = $this->select()
                ->from(array("e" => "emails"))
                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject", "content"))
                ->where("e.type = ?", (int) $type);
        $data = $this->fetchRow($sql);

        return (array) $data;
    }

    public function tipsMailNotification() {
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array())
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->where("e.type =?", 33)
                ->where("u.status =?", 1)
                ->where("u.type =?", 1)
                ->where("c.status =?", 1)
                ->where("u.last_login != ?", "NULL")
                ->where("e.status =?", 1);
        $result = $this->fetchAll($sql);

        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];
            $user_data[$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"]);

            $data[$row['id_company']]['users'] = $this->finalUserFilterMail($user_data, $this->getUserSendedMail($row['id_company']));
        }
        return $data;
    }

    public function updareCronTipsTable($id_user, $id_company) {
        $data['id_user'] = $id_user;
        $data['id_company'] = $id_company;
        $data['status'] = 1;
        $data['date'] = date("Y-m-d H:i:s");
        $this->insert($data, "cron_tips");
    }

    public function updareCronUpdateOnPlatformTable($id_user, $id_company) {
        $data['id_user'] = $id_user;
        $data['id_company'] = $id_company;
        $data['status'] = 1;
        $data['date'] = date("Y-m-d H:i:s");
        $this->insert($data, "cron_platform_updates");
    }

    public function getUserSendedMail($id_company) {
        $sql = $this->select()->from(array("ct" => "cron_tips"), array("ct.id_user"))->where("ct.id_company =?", (int) $id_company)->group("ct.id_user");
        return $this->fetchAssoc($sql);
    }

    public function finalUserFilterMail($_arr, $_arr2) {
        return array_diff_key($_arr, $_arr2);
    }

    public function EmailOnUpdates() {

        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("j" => "jobs"), "j.id_company = c.id", array("id AS job_id", "bounty AS job_bounty", "bounty_file AS job_bounty_file", "end AS job_deadline"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job_title"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array())
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->where("e.type =?", 34)
                ->where("u.status =?", 1)
                ->where("c.status =?", 1)
                ->where("j.status =?", 1)
                ->where("e.status =?", 1);
        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];

            $data[$row['id_company']]['users'][$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"]);

            $data[$row['id_company']]['jobs'][$row['job_id']] = array(
                "job_id" => $row['job_id'],
                "job_bounty" => $row['job_bounty'],
                "job_bounty_file" => $row['job_bounty_file'],
                "job_title" => $row['job_title'],
                "job_deadline" => $row['job_deadline'],
            );
        }
        return $data;
    }

    public function statusRecommend() {
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array("*"))
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->where("e.type =?", 36)
                ->where("u.status =?", 1)
                ->where("u.type !=?", 2)
                ->where("e.status =?", 1)
                ->where("c.status =?", 1);
        $result = $this->fetchAll($sql);

        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];

            $data[$row['id_company']]['users'][$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"]);
        }

        foreach ($data as $key => &$users) {
            foreach ($users['users'] as &$user) {
                $action_last_week = $this->checkUserRecommendationInLastWeek($user['id_user']);
                if (empty($action_last_week)) {
                    unset($users['users'][$user['id_user']]);
                }
            }
        }

        return $data;
    }

    public function checkUserRecommendationInLastWeek($id) {
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 7 days ago
        $sql = $this->select()->from(array("jr" => "job_recommendations"), array("*"))->where("jr.id_recommender =?", (int) $id)
                ->where("date >=?", $date);
        $result = $this->fetchAll($sql);
        if (empty($result)) {
            $sql = $this->select()->from(array("gp" => "gamification_points"), array("*"))->where("gp.id_user =?", (int) $id)
                    ->where("date >=?", $date);
            $result = $this->fetchAll($sql);
        }
        return $result;
    }

    public function adminReport() {
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "type AS user_type", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array("*"))
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->where("e.type =?", 37)
                ->where("u.status =?", 1)
                ->where("u.type =?", 2)
                ->where("e.status =?", 1)
                ->where("c.status =?", 1);
        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];

            $data[$row['id_company']]['users'][$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_type" => $row["user_type"],
                "user_company" => $row["user_company"]);
        }
        foreach ($data as $key => &$value) {
            $value['applications'] = $this->getCompanyApplication($value['id_company']);
            $value['user_reached'] = $this->getUserReached($value['id_company']);
            $value['bountyToPay'] = $this->getBountyToBePaid($value['id_company']);
        }


        return $data;
    }

    public function getCompanyApplication($id) {
        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(applications) AS applications"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array())
                ->where("c.id_company = '{$id}'");

        return $this->fetchRow($sql)['applications'];
    }

    public function getUserReached($id) {
        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(views) AS reached"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array())
                ->where("c.id_company = '{$id}'");

        return $this->fetchRow($sql)['reached'];
    }

    public function getBountyToBePaid($id) {
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 7 days ago
        $sql = $this->select()->from(array("b" => "bounties"), array("count(id) AS bounty"))->where("b.id_company =?", (int) $id)
                ->where("date >=?", $date)
                ->where("b.status !=?", 'paid')
                ->where("b.status !=?", 'canceled');
        return $this->fetchRow($sql)['bounty'];
    }

    public function RecommenderStatusInCampaign() {

        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company",
                    "(SELECT count(id) from users uu WHERE uu.id_company = c.id AND uu.status = 1 ) AS total_users",
                    "(SELECT sum(value) from campaign_reports_detailed crd WHERE crd.id_user = u.id AND crd.id_campaign = ca.id) AS points"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "type AS user_type", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array("*"))
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("ca" => "campaigns"), "ca.id_company = c.id", array("id AS id_campaign", "title AS campaign_title", "end AS campaign_deadline"))
                ->where("ca.status =?", 1)
                ->where("e.type =?", 38)
                ->where("u.status =?", 1)
                ->where("u.type =", 1)
                ->where("e.status =?", 1)
                ->where("c.status =?", 1);
        $result = $this->fetchAll($sql);
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];
            $campaigns[$row['id_campaign']] = array(
                'id_campaign' => $row['id_campaign'],
                'campaign_title' => $row['campaign_title'],
                'campaign_deadline' => $row['campaign_deadline'],
                'total_users' => $row['total_users'],
//                'points' => $this->getUserPlace($row['id_user'],$row['id_campaign']),
                'points' => $row['points'],
                'place' => $this->getUserPointsByCampaignId($row['id_campaign'], $row['id_user'])
            );

            $data[$row['id_company']]['users'][$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_type" => $row["user_type"],
                "user_company" => $row["user_company"],
                "campaigns" => $campaigns
            );
        }
        return $data;
    }

    public function getUserPlace($id_user, $id_campaign) {
        $sql = $this->select()->from(array("crd" => "campaign_reports_detailed"), array("sum(value) AS values"))
                ->where("crd.id_user =?", $id_user)
                ->where("crd.id_campaign =?", $id_campaign);
        return $this->fetchRow($sql)['values'];
    }

    public function getUserPointsByCampaignId($id_campaign, $id_user) {
        $sql = $this->select()->from(array("c" => "companies"), array("(SELECT sum(value) from campaign_reports_detailed crd WHERE crd.id_user = u.id AND crd.id_campaign = ca.id) AS points"))
                ->join(array("ca" => "campaigns"), "ca.id_company = c.id", array("id AS id_campaign"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user",))
                ->order("points DESC")
                ->where("u.status = 1")
                ->where("c.status = 1")
                ->where("ca.status = 1")
                ->where("ca.id =?", $id_campaign);
        $result = $this->fetchAll($sql);
//        $data['result'] = $result ;
        $i = 1;
        foreach ($result as $key => $value) {
            if ($value['id_user'] == $id_user) {
                $data = $key + $i;
            }
        }
//        $data['ids'] = array("id_campaign" =>$id_campaign ,"user" => $id_user  );
        return $data;
    }

    public function setupEmails($id, $company_name, $company_email) {

        $data['id_company'] = $id;
        $data['type'] = 25;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;

        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "apply";
            $subject = <<<'HERE'
                        New application on {$job.title}
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
.class {}
</style>
{/literal} 
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You successfully applied to: {$job.title} @ {$job.company_title}
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Application successfully registered</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Hello {$user.firstname},</p>
                  <br>

                  <p>You successfully applied to : {$job.title} @ {$job.company_title}.</p> <p>You can view the application status here: <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/profile#application">Application History</a></p> 
                   
                 <p>Best of luck and hope you'll get the job you want soon. </p>

                 <p><br>Keep in touch,<br>{$settings.sitename}</p>
                </td>
              </tr>
  <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;

            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 26;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 3;
        $data['email'] = $company_email;
        $data['from'] = $company_name;

        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "company email";
            $subject = <<<'HERE'
                        New application on {$job.title}
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Check your new candidate experience.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">New application on {$job.title}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Dear {$job.company_title},</p>
                  <br>

                  <p>{$user.firstname} {$user.lastname} just applied for a position on one job posted by you ({$job.title}) You can view the application details: <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/job-details/?id={$job.id}">here</a></p> 

                 <p><br>Regards,<br>{$settings.sitename}</p>
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 27;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Recommendation email";
            $subject = <<<'HERE'
Welcome to Talentspotting - Start earning rewards for your involvement in recruitment
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'
<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 32px; font-weight: 400; margin: 0;">Welcome to {$settings.sitename} referral platform - Start earning rewards for your involvement in recruitment</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$user.firstname},</p>
                  <br>

                  
                    <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.

                  </P>
                  <b>Please activate your account <a href="{$link}">here</a>, using the initial password: {$password.password}
                  </b>
                  <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.
                </P>
                <p>If you have any questions or comments, feel free to reach out :)</p>

                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="http://catalyst.talentspotting.com/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://imgh.us/facebook-logo-button_(2).svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://imgh.us/linkedin-logo-button_1.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://imgh.us/house_9.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 28;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {

            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Recommendation email";
            $subject = <<<'HERE'
New recommendation for  {$job.title} by {$user.firstname} {$user.lastname}
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    <a target="_blank" href="{$url}?track={$data.track}">{$job.title}</a> currently open at {$job.company_title}.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">New recommendation for  {$job.title} by {$user.firstname} {$user.lastname}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$user.firstname},</p>
                  <br>

                  
                    <P><a target="_blank" href="{$url}?track={$data.track}">{$job.title}</a> currently open at {$job.company_title}
                    <p>If you’re interested to apply, all you need to do is sign up on {$job.company_title} referral platform and upload your CV.</p>
                  </P>
                <p>This is an automated referral system where your friends can recommend openings from their companies. You can apply easily with a simple one tap sign-in.</p>
                <p>Cheers,<br>{$user.firstname} {$user.lastname}</p>

                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 29;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Bounty Paid";
            $subject = <<<'HERE'
Success - Bounty Paid
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Congratulations {$user.firstname} {$user.lastname}, you just won a bounty!
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Success - Bounty Paid</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Congratulations {$user.firstname} {$user.lastname},</p>
                  <br>

                  <p>You just won the bounty for {if is_numeric($bounty.amount)} {$bounty.amount} {$settings.currencies[1]} {else} {$bounty.amount} {/if} , {$job.title} @ {$company.title}.<br>You can check whe you will receive your reward <a href="{$settings.sitename}/profile#/rewards-view">here.</a></p> 

                 <p><br>Keep up the good work,<br>{$settings.sitename} Team</p>
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 1;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Registration";
            $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You successfully registered on Talentspotting referral platform.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Welcome to {$settings.sitename} Referral Platform</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hello,</p>
                  <br>

                  
                    <P>You successfully registered on {$company.title}'s referral platform. Here it's easier than ever to apply and get the job you want at {$company.title}.
                  </P>
                  <P>If you have any questions or comments, please feel free to reach out!
                  </P>
                  <br />
                  <p>To activate your account please click here</p><a href='{$link}'>{$link}</a> 
                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.talentspotting.com" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 30;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "External Recommender";
            $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">


{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 32px; font-weight: 400; margin: 0;">Welcome to {$settings.sitename} referral platform - Start earning rewards for your involvement in recruitment</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$user.firstname},</p>
                  <br>

                  
                    <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.

                  </P>
                  <b>Please activate your account <a href="{$settings.sitename}/login">here</a>, using the initial password: {$password.password}
                  </b>
                  <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.
                </P>
                <p>If you have any questions or comments, feel free to reach out :)</p>

                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 31;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "new Campaign created";
            $subject = <<<'HERE'
New campaign on Talentspotting / Who’s the best professional in your network?
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Ready to identify the best professionals in your network?
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">New referral campaign on {$settings.sitename} / Who’s the best professional in your network?</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

                  <p>Ready to identify the best professionals in your network? At {$settings.sitename} we’ve launched a new referral campaign - {$campaign}.</p> 
                    <p>Here are the jobs and rewards involved, and the actions you can take to win.</p>
                    <p>Make sure you share the jobs and identify quality candidates by March 1st to qualify for the awards.</p>

                </td>
              </tr>


              <!-- TABLE -->
              <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 10px 10px 50px 10px;" word-break: break-all >
                     
<table>
            <thead>
                <tr>
                    <th width="25%">Job</th>
                    <th width="25%">Actions to be rewarded</th>
                    <th width="25%">Reward</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$jobs key=key item=item}
                <tr>
                    <td> <a target="_blank" href="{$item.url}" target="_blank">{$item.title}</a></td>
                    <td>
                        {foreach from=$gamification item=action}
                        {$action}
                        {/foreach}
                    </td>
                    <td>{$bounty}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
                </td>
              </tr>
            </table>
        <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <b>How does it work? </b><p>Share the opportunities above with your network and identify the best professionals who would qualify for them.
Track your progress in the Recommendations section of your account.<br>The winner will be the recommender with the most collected (views/shares etc).<br>Track your progress in the Leaderboard section of your account.</p>
                </td>
              </tr>

             <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }


        $data['id_company'] = $id;
        $data['type'] = 32;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {

            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Registration";
            $subject = <<<'HERE'
Grab your reward! Your candidate just got a job on Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'
   

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Congratulations! You are a gifted talent hunter. {$username} has just been marked as a hired candidate at <a href="{$settings.sitename}">{$company}</a>.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Grab your reward! Your candidate just got a job at <a href="{$settings.sitename}">{$company}</a></h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

                  <p>Congratulations! You are a gifted talent hunter. {$username} has just been marked as a hired candidate at <a href="{$settings.sitename}">{$company}</a>.</p> 
                    <P>You are now entitled to collect the bounty - {$job.bounty} Check the Bounties section to see when it is due.
                  </P>
                  <P>If you have any questions, please get in touch with your HR colleagues.
                  </P>
                  <P>Now that you've seen how it works, do check other opportunities on our referral platform
                    and keep building amazing teams for your company , <a href="{$settings.sitename}">{$company}</a>.
                  </P>
                </td>
              </tr>

  <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.talentspotting.com" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 23;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Custom Reminder";
            $subject = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">{$message}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p>{$message}</p>
                </td>
              </tr>

              <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $content = <<<'HERE'
{$message}
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 33;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Tips & tricks to earn rewards";
            $subject = <<<'HERE'
Talentspotting - Tips & tricks to earn rewards
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'


<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    We figured you might welcome a few tips and tricks to be successful fast on Talentspotting. Here goes!
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Talentspotting - Tips & tricks to earn rewards</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

                  <p>We figured you might welcome a few tips and tricks to be successful fast on Talentspotting. Here goes!</p> 
                    <ol>
                        <li>Use all recommendation channels. Share opportunities with all your networks - Facebook, LinkedIn, Twitter.</li>
                        <li>Always think - Who is the best professional you've ever worked with? This will make it easier for you come up with the most suitable people in your network.</li>
                        <li>Drop a personal line when recommending the job to someone. All your recommendations will go out with pre-defined messages so you dont spend time writing promotion line for the jobs. However, we believe it will help if you use a personal line in the message why you love the company or think that the job is suited for your friend</li>
                    </ol>
                    <P>Most of the actions above imply just clicks, so just do them and start getting your rewards.</P>
                </td>
              </tr>
 <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html> 

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }
        $data['id_company'] = $id;
        $data['type'] = 34;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Updates On platform";
            $subject = <<<'HERE'
What’s new on Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Here you can see whats new in Talentspotting.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">What’s new on Talentspotting</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

                  <p>Ready to identify the best professionals in your network? Here are all the opportunities that can benefit from your help and the rewards for each of them :)</p> 
                    <p>Make sure you share the jobs and identify quality candidates to qualify for the awards.</p>
                </td>
              </tr>


              <!-- TABLE -->
              <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 10px 10px 50px 10px;" word-break: break-all >
        <table style="width: 80%">
            <thead>
                <tr>
                    <th width="33%">Job</th>
                    <th width="33%">Reward</th>
                    <th width="33%">Deadline</th>
                </tr>
            </thead>
            <tbody>
                 {foreach from=$jobs key=key item=item}
                <tr>
                    <td>{$item.job_title}</td>
                    <td>
                        {if ($item.job_bounty_file == '')}
                            {$item.job_bounty} &euro;
                        {else}
                        {$item.job_bounty} <br />
                        <img src="{$settings.sitename}/resources/files/bounties/{$item.job_bounty_file}" alt="" />
                            
                        {/if}
                      
                    </td>
                    <td>{$item.job_deadline}</td>
                </tr>


                {/foreach}
            </tbody>
        </table>
        <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <b>How does it work? </b><p>Share the opportunities above with your network and identify the best professionals who would qualify for them.
Track your progress in the Recommendations section of your account.</p>
                </td>
              </tr>
 <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 36;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Status update on recommended candidates";
            $subject = <<<'HERE'
Your results on Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Status update on recommended candidates.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Your results on our referral platform</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

                  <b>Here are your current <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/profile#recommendations">results</a></b> 
                    <P>There may be even more rewards to be grabbed and candidates in your network to be identified. Increase your chances of building strong teams for your company and earning rewards - check current vacancies on {$settings.sitename} referral platform and let your contacts know about them first.
                  </P>
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }
        $data['id_company'] = $id;
        $data['type'] = 37;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Admin Report";
            $subject = <<<'HERE'
Your Talentspotting Activity
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Things are moving forward with your referral program!
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Your Talentspotting Activity</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Hi {$username},</p>
                  <br>
                  <p>Things are moving forward with your referral program! Check out the latest:</p>
                  <ul>
                    <li>{$applications} : new applicants in the last week.</li>
                    <li>{$reached} : users reached.</li>
                    <li>{$bounty} : bounties to be paid.</li>
                  </ul>

                  <p>View your applicants and boost your referral program some more!</p>
              </td></tr>
            
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>


HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }
        $data['id_company'] = $id;
        $data['type'] = 38;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {

            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Recommender status in campaign";
            $subject = <<<'HERE'
See your position in current campaigns and earn more points
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'
<html>
    <head>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                text-align: center ;
            }
        </style>
    </head>
    <body>


        Hi {$username},<br />
        <br />
        <br />
        Ta-dam!! Thought you'd like to know your position in current Talentspotting campaigns and how you can  <br />
        boost your chances of recommending quality candidates and earning superrewards!  <br />
        <br /> 
        <table>
            <thead>
                <tr>
                    <th width="33%">Campaign</th>
                    <th width="33%">Place</th>
                    <th width="33%">Deadline</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$campaigns key=key item=item}
                <tr>
                    <td>{$item.campaign_title}</td>
                    <td>{$item.place} out of {$item.total_users}</td>
                    <td>{$item.campaign_deadline}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        <br />
        <br />
        Share the opportunities above with your network and identify the best professionals who would qualify for  <br />
        them. Track your progress in the Leaderboard section of your account. <br />
        <br />
        <br />
        <br />
        Best of luck<br />
        Adi<br />

        <img src="https://talentspotting.com/resources/files/images/mail_logo.png" alt="Talentspotting" />
    </body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 39;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Application status changed";
            $subject = <<<'HERE'
Application status changed
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'
<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Your candidates are moving forward
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Application status changed</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Hi {$recommender.firstname},</p>
                  <br>

                  <p>Thumbs up for your activity!<br>Your candidates are moving forward - check out their status in the hiring process <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/profile#recommendations">here</a>.</p> 

                  <p>There may be even more rewards to be grabbed and candidates in your network to be identified :)</p>

                  <p>Increase your chances of building strong teams for your company and earning rewards.Check current vacancies on Talentspotting and let your contacts know about them first.</p>

                 <p><br>Best of luck,<br>{$settings.sitename}</p>
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 40;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Application status changed";
            $subject = <<<'HERE'
Application status changed
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You are moving forward
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Application status changed</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Hi {$user.firstname},</p>
                  <br>

                  <p>Thumbs up for your activity!<br>You are moving forward - check out your status <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/profile#application">here</a> in the hiring process for {$job.title}.</p> 

                 <p><br>Best of luck,<br>{$settings.sitename}</p>
                </td>
              </tr>

              <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="h{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 41;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 2;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Mail admin upload cv";
            $subject = <<<'HERE'
Mail admin upload cv
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    {$user.firstname} {$user.lastname} just uploaded a CV for {$job.title}
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">New CV upload for one of your jobs</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
Dear {$company_title},</p>
                  <br>

                  <p>{$user.firstname} {$user.lastname} just uploaded the CV for candidate {$candidate.firstname} {$candidate.lastname}
for a position on one job posted by you ({$job.title}) You can view the application details:  <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="{$settings.sitename}/job-details/?id={$id_job}">here</a></p> 
                   
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 42;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Mail candidat upload cv";
            $subject = <<<'HERE'
Hi {$candidate.firstname} , i just uploaded your CV for {$company.title}
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Check your application status
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Hi {$candidate.firstname} , i just uploaded your CV for {$company.title}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                    Hi {$candidate.firstname},</p>
                  <br>

                  <p>Check your application status anytime at {$settings.sitename}/login by logging in with
the e-mail address you're reading this message on and the password <b>{$password} </b>to see your application status.</p> 
                   
                 <p>I'm glad you're interested in this role! Please let me know if you'd like me to tell you anything else about it or the opportunities at {$company.title}.</p>

                 <p><br>Keep in touch,<br>{$user.firstname} {$user.lastname}</p>
                </td>
              </tr>

             <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }
        $data['id_company'] = $id;
        $data['type'] = 43;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Job Reminder";
            $subject = <<<'HERE'
Reminder about the recommendation for {$job.title}
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Did you think about the {$job.title} currently open at {$job.company_title}.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Reminder about the recommendation for {$job.title}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$user.firstname},</p>
                  <br>

                  
                    <P>Quick follow up on the recommendation for {$job.title} position within {$job.company_title}. Hope you get the chance to review it and get excited about it. I’m here if you need additional info.</p>
                <p>Cheers,<br>{$recommender.recommender_firstname} {$recommender.recommender_lastname} <br />{$email}</p>

                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.talentspotting.com" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 44;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 1;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Jobs Reminder";
            $subject = <<<'HERE'
What's new in {$company} jobs - open for referral on Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'
   
   <!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">

    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Ready to identify the best professionals in your network?
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">What's new in {$company} jobs - open for referral on Talentspotting</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$username},</p>
                  <br>

          <p>Ready to identify the best professionals in your network? Here are all the new <a href="{$settings.sitename}">{$company}</a> opportunities that can benefit from your help this week and the rewards you can get for them :)</p>
            <p>Make sure you share the jobs below on Talentspotting and identify quality candidates in time to qualify for the awards.</p>

                </td>
              </tr>


              <!-- TABLE -->
              <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 10px 10px 50px 10px;" word-break: break-all >
                     
<table style="border: 1px solid black">
            <thead>
                <tr>
                    <th style="border: 1px solid black" width="25%">Job</th>
                    <th style="border: 1px solid black" width="25%">Reward</th>
                    <th style="border: 1px solid black" width="25%">Deadline</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$jobs key=key item=item}
                <tr>
                    <td style="border: 1px solid black"> <a target="_blank" href="{$item.url}" target="_blank">{$item.title}</a></td>
                    <td style="border: 1px solid black">
                        {$item.bounty}
                    </td>
                    <td style="border: 1px solid black">{$item.end}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
                </td>
              </tr>
            </table>
        <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 800px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p><b>How does it work? </b> Log in, share the opportunities above with your network and identify the best professionals who would qualify for them. <br />
                  Track your progress in the Recommendations section of your account.</p>
                </td>
              </tr>

             <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
            src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>

HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }

        $data['id_company'] = $id;
        $data['type'] = 45;
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = 9;
        $data['email'] = $company_email;
        $data['from'] = $company_name;
        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] = 1;
            $_data['title'] = "Registration";
            $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
            $_data['subject'] = $subject;
            $content = <<<'HERE'

<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    You successfully registered on Talentspotting referral platform.
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">Hi {$candidate.firstname} , you are invited {$company.title} job platform</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">Hi {$candidate.firstname},</p>
                  <br>

                  
                    <P>Check our jobs or your application status anytime at {$settings.sitename}/login by logging in with the e-mail address you're reading this message on and the password {$password}.
                  </P>
                  <P><strong>Please let me know if you'd like me to tell you anything else about it or the opportunities at {$company.title}.</strong>
                  </P>
                  <br />
                  <p>Keep in touch,<br />
<strong>{$user.firstname} {$user.lastname}</strong></p> 
                </td>
              </tr>
                <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://imgh.us/Screen_Shot_2017-02-20_at_15.31.48.png" alt="Sing in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/facebook.svg"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/linkedin.svg"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="{$settings.sitename}/resources/frontend/emails/house.svg"></a><br>
            <br>
            <a href="http://www.talentspotting.com" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
            $_data['content'] = $content;
            $this->insert($_data, 'emails_lang');
        }
    }

    public function CampaignBounty() {
        $today = date('Y-m-d');
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("ca" => "campaigns"), "ca.id_company = c.id", array("id AS id_campaign", "title AS campaign_title", "end AS campaign_deadline", "status as campaign_status", "bounty"))
                ->where("ca.status =?", 1)
                ->where("c.status =?", 1)
                ->where("ca.end <=?", $today);
        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['campaigns'][$row['id_campaign']]['id_campaign'] = $row['id_campaign'];
            $data[$row['id_company']]['campaigns'][$row['id_campaign']]['campaign_title'] = $row['campaign_title'];
            $data[$row['id_company']]['campaigns'][$row['id_campaign']]['campaign_deadline'] = $row['campaign_deadline'];
            $data[$row['id_company']]['campaigns'][$row['id_campaign']]['campaign_status'] = $row['campaign_status'];
            $data[$row['id_company']]['campaigns'][$row['id_campaign']]['bounty'] = $row['bounty'];
        }
        return $data;
    }

    public function setCampaignBounty($data) {
        $this->insert($data, 'campaign_bounty');
    }

    public function disableCampaignBounty($id) {
        $this->update(array('status' => 0), "id = {$id}", 'campaigns');
    }

    public function trackTimeLine() {
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company", "track_timeline", "created"))
                ->join(array("u" => "users"), "u.id_company=c.id", array("id AS id_user", "id_parent"))
                ->where("u.status =?", 1)
                ->where("u.id_parent !=?", 0)
                ->where("c.track_timeline !=?", 0)
                ->where("c.status =?", 1);

        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['created'] = $row['created'];
            $data[$row['id_company']]['track_timeline'] = $row['track_timeline'];
            $data[$row['id_company']]['users'][$row['id_user']]['id_recommender'] = $row['id_parent'];
            $data[$row['id_company']]['users'][$row['id_user']]['id_user'] = $row['id_user'];
        }
        return $data;
    }

    public function IsUserHired($id) {
        $sql = $this->select()->from(array("ja" => "job_applications"), array("*"))
                ->where("ja.id_user =?", (int) $id)
                ->where("ja.status_reason =?", 5);

        return $this->fetchAll($sql);
    }

    public function removeParent($id) {
        $this->update(array("id_parent" => 0), "id = {$id}", "users");
    }

    public function rapport() {
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 7 days ago
//         echo $date ;die;
        $date = $this->quote($date);
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id) AS user_total",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND last_login IS NOT NULL AND last_login >= $date) AS active_users",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND last_login IS NULL AND last_login >= $date) AS inactive_users",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=9 AND last_login >= $date) AS number_of_candidates",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=2) AS admin_companie",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=1 AND last_login >= $date) AS recommenders",
                    "(SELECT count(jv.id) FROM job_views AS jv INNER JOIN jobs as j on j.id=jv.id_job WHERE j.id_company=c.id AND date >= $date) AS job_views",
                    "(SELECT count(ja.id) FROM job_applications AS ja INNER JOIN jobs as j on j.id=ja.id_job WHERE j.id_company=c.id AND date >= $date) AS number_of_application",
                    "(SELECT count(ul.id) FROM user_login AS ul INNER JOIN users as u on u.id=ul.id_user WHERE u.id_company=c.id AND u.type =1 AND date >= $date) AS number_of_accesare",
                    "(SELECT count(ul.id) FROM user_login AS ul INNER JOIN users as u on u.id=ul.id_user WHERE u.id_company=c.id AND u.type =9 AND date >= $date) AS number_of_accesare_candidate",
                    "(SELECT count(js.id) FROM jobs_share AS js ,users as u WHERE u.id=js.id_user AND date >= $date AND u.type=1) AS job_shares_recommenders",
                    "(SELECT count(js.id) FROM jobs_share AS js ,users as u WHERE u.id=js.id_user AND date >= $date AND u.type=9) AS job_shares_candidate",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND date >= $date AND type!=9) AS working_recommeners",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND date >= $date AND type=9) AS working_cansiste_as_recommeners",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND date >= $date AND type=1 AND jr.id_user > 0) AS new_registred_candidate_by_recommenders",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND date >= $date AND type=9 AND jr.id_user > 0) AS new_registred_candidate_by_candidate",))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "firstname", "lastname"));

        $result = $this->fetchAll($sql);

        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['user_total'] = $row['user_total'];
            $data[$row['id_company']]['active_users'] = $row['active_users'];
            $data[$row['id_company']]['inactive_users'] = $row['inactive_users'];
            $data[$row['id_company']]['number_of_candidates'] = $row['number_of_candidates'];
            $data[$row['id_company']]['admin_companie'] = $row['admin_companie'];
            $data[$row['id_company']]['recommenders'] = $row['recommenders'];
            $data[$row['id_company']]['job_views'] = $row['job_views'];
            $data[$row['id_company']]['number_of_application(numar applicari)'] = $row['number_of_application'];
            $data[$row['id_company']]['number_of_applicants(numar applicanti)'] = $this->getApplicantsbyCompany($row['id_company']);
            $data[$row['id_company']]['number_of_accesare(recommeder) => login'] = $row['number_of_accesare'];
            $data[$row['id_company']]['number_of_accesare(candidate) => login'] = $row['number_of_accesare_candidate'];
            $data[$row['id_company']]['job_shares_recommenders'] = $row['job_shares_recommenders'];
            $data[$row['id_company']]['job_shares_candidate'] = $row['job_shares_candidate'];
            $data[$row['id_company']]['working_recommeners'] = $row['working_recommeners'];
            $data[$row['id_company']]['Working recommenders(from candidates)'] = $row['working_cansiste_as_recommeners'];
            $data[$row['id_company']]['new_registred_candidate_by_recommenders'] = $row['new_registred_candidate_by_recommenders'];
            $data[$row['id_company']]['new_registred_candidate_by_candidate( Cu dreptul de recommend )'] = $row['new_registred_candidate_by_candidate'];
        }

        return $data;
    }

    public function rapportTotal() {
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 7 days ago
//         echo $date ;die;
        $date = $this->quote($date);
        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id) AS user_total",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND last_login IS NOT NULL) AS active_users",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND last_login IS NULL) AS inactive_users",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=9) AS number_of_candidates",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=2) AS admin_companie",
                    "(SELECT count(id) FROM users AS u WHERE u.id_company=c.id AND u.type=1) AS recommenders",
                    "(SELECT count(jv.id) FROM job_views AS jv INNER JOIN jobs as j on j.id=jv.id_job WHERE j.id_company=c.id) AS job_views",
                    "(SELECT count(ja.id) FROM job_applications AS ja INNER JOIN jobs as j on j.id=ja.id_job WHERE j.id_company=c.id ) AS number_of_application",
                    "(SELECT count(ul.id) FROM user_login AS ul INNER JOIN users as u on u.id=ul.id_user WHERE u.id_company=c.id AND u.type =1 ) AS number_of_accesare",
                    "(SELECT count(ul.id) FROM user_login AS ul INNER JOIN users as u on u.id=ul.id_user WHERE u.id_company=c.id AND u.type =9 ) AS number_of_accesare_candidate",
                    "(SELECT count(js.id) FROM jobs_share AS js ,users as u WHERE u.id=js.id_user AND u.type=1) AS job_shares_recommenders",
                    "(SELECT count(js.id) FROM jobs_share AS js ,users as u WHERE u.id=js.id_user AND u.type=9) AS job_shares_candidate",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND type!=9) AS working_recommeners",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND type=9) AS working_cansiste_as_recommeners",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND type=1 AND jr.id_user > 0) AS new_registred_candidate_by_recommenders",
                    "(SELECT count(jr.id) FROM job_recommendations AS jr INNER JOIN users as u on u.id=jr.id_recommender WHERE u.id_company=c.id AND type=9 AND jr.id_user > 0) AS new_registred_candidate_by_candidate",))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "firstname", "lastname"));

        $result = $this->fetchAll($sql);

        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['user_total'] = $row['user_total'];
            $data[$row['id_company']]['active_users'] = $row['active_users'];
            $data[$row['id_company']]['inactive_users'] = $row['inactive_users'];
            $data[$row['id_company']]['number_of_candidates'] = $row['number_of_candidates'];
            $data[$row['id_company']]['admin_companie'] = $row['admin_companie'];
            $data[$row['id_company']]['recommenders'] = $row['recommenders'];
            $data[$row['id_company']]['job_views'] = $row['job_views'];
            $data[$row['id_company']]['number_of_application(numar applicari)'] = $row['number_of_application'];
            $data[$row['id_company']]['number_of_applicants(numar applicanti)'] = $this->getApplicantsbyCompany($row['id_company']);
            $data[$row['id_company']]['number_of_accesare(recommeder) => login'] = $row['number_of_accesare'];
            $data[$row['id_company']]['number_of_accesare(candidate) => login'] = $row['number_of_accesare_candidate'];
            $data[$row['id_company']]['job_shares_recommenders'] = $row['job_shares_recommenders'];
            $data[$row['id_company']]['job_shares_candidate'] = $row['job_shares_candidate'];
            $data[$row['id_company']]['working_recommeners'] = $row['working_recommeners'];
            $data[$row['id_company']]['Working recommenders(from candidates)'] = $row['working_cansiste_as_recommeners'];
            $data[$row['id_company']]['new_registred_candidate_by_recommenders'] = $row['new_registred_candidate_by_recommenders'];
            $data[$row['id_company']]['new_registred_candidate_by_candidate( Cu dreptul de recommend )'] = $row['new_registred_candidate_by_candidate'];
        }

        return $data;
    }

    function getApplicantsbyCompany($id) {
        $sql = $this->select()->from(array("ja" => "job_applications"), array("count(ja.id) as applicants"))
                ->join(array("u" => "users"), "ja.id_user=u.id", array(""))
                ->group("u.id")
                ->where("u.id_company =?", $id);
        return count($this->fetchAll($sql));
    }

    public function disableExpireJobs() {
        $sql = $this->select()->from(array("j" => "jobs"), array("id", "end"))
                ->where("j.status = 1");
        $all_Jobs = $this->fetchAll($sql);
        foreach ($all_Jobs as $key => $job) {
            if (strtotime($job['end']) < strtotime(date('Y-m-d'))) {
                $this->disableJob($job['id']);
            }
        }
        $update = new models_Update ;
        for($i=0;$i<6;$i++){
            sleep(10);
            $update->upContent();
        }
    }

    public function enableJobs() {
        $sql = $this->select()->from(array("j" => "jobs"), array("id", "start"))
            ->where("j.status = 0");
        $all_Jobs = $this->fetchAll($sql);
        foreach ($all_Jobs as $key => $job) {
            if (strtotime($job['start']) == strtotime(date('Y-m-d'))) {
                $this->enableJobById($job['id']);
            }
        }
    }

    public function enableJobById($id_job)
    {
        $this->update(['status' => 1], "id=$id_job", "jobs");
    }

    public function enableCampaigns($_settings_array) {
        $sql = $this->select()->from(array("c" => "campaigns"), array("id", "start"))
            ->where("c.status = 0");
        $all_campaigns = $this->fetchAll($sql);
        foreach ($all_campaigns as $key => $campaign) {
            if (strtotime($campaign['start']) == strtotime(date('Y-m-d'))) {
                $this->enableCampaignById($campaign['id'],$_settings_array);
            }
        }
    }

    public function enableCampaignById($id,$_settings_array)
    {
        $this->update(['status' => 1], "id=$id", "campaigns");
        $model_campaign = new models_Campaign();
        $campaign = $model_campaign->getCampaignById($id);
        $mUser =  new models_User ;
        $users = $mUser->getAllRecruitersByCompany();
        foreach($users as $index => $user)
        {
            $user_array = (object)$mUser->getUserById($user['id']);
            Zend_Auth::getInstance()->getStorage()->write($user_array);
            $userObject = Domain_User_UserFactory::createInstance($user['id']);
            $userObject->setSettings($_settings_array);
            $userObject->sendNewCampaignEmail($campaign,$campaign['jobs'],$campaign['gamification_actions']);
        }

        $users = $mUser->getAllAdminsByCompany();
        foreach($users as $index => $user)
        {
            $user_array = (object)$mUser->getUserById($user['id']);
            Zend_Auth::getInstance()->getStorage()->write($user_array);
            $userObject = Domain_User_UserFactory::createInstance($user['id']);
            $userObject->setSettings($_settings_array);
            $userObject->sendNewCampaignEmailForAdmin($campaign,$campaign['jobs'],$campaign['gamification_actions']);
        }


    }

    public function disableJob($id_job) {
        $this->update(['status' => 0], "id=$id_job", "jobs");
        $sql = $this->select()->from(['ja' => 'job_applications'],['*'])
            ->where("ja.status NOT IN (" . implode(",", [5]) . ") || ja.status_reason IN (" . implode(",", [2,4,3,11,16,21,28]) . ")")
            ->where("ja.id_job =?",(int) $id_job);
        $result = $this->fetchAll($sql);
//        foreach($result as $key => $application)
//        {
//            $userObject = Domain_User_UserFactory::createInstance($application['id_user']);
//            $userObject->sendCandidateUnfitStatusChange($application);
//            $status = 16;
//            $status_reason = 19 ;
//            $this->update(
//                ['status' => $status,
//                    'status_reason' => $status_reason],
//                "id={$application['id']}",
//                "job_applications");
//            $mUser = new models_User;
//            $user = $mUser->getUserDataByEmail("cron@talentspotting.com");
//            $user = (object) $user ;
//            Zend_Auth::getInstance()->getStorage()->write($user);
//            $mUser->setjobApplicationStatusHistory($id_job, $application['id'], $status, $status_reason);
//        }
    }

    public function getExpiredCampaigns() {
        $sql = $this->select()->from(array("c" => "campaigns"), array("id", 'end', 'title'))
                ->where("c.status = 1");
        return $this->fetchAll($sql);
    }

    public function reminderJobs() {

        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array())
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->joinLeft(['uu' => "users"], "uu.id = u.id_parent", ['firstname as recommender_firstname', 'lastname as recommender_lastname'])
                ->where("e.type =?", 43)
                ->where("u.status =?", 1)
                ->where("u.type =?", 9)
                ->where("c.status =?", 1)
                ->where("e.status =?", 1);
        $result = $this->fetchAll($sql);

        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];
            $user_data[$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"],
                "recommender_firstname" => $row["recommender_firstname"],
                "recommender_lastname" => $row["recommender_lastname"]);

            $data[$row['id_company']]['users'] = $this->finalUserFilterMail($user_data, $this->getUserSendedMail($row['id_company']));
            $data[$row['id_company']]['jobs'] = $this->getNewJobsLast_24($row['id_company']);
        }
        return $data;
    }

    public function getNewJobsLast_24($id_company) {
        $jobModel = new models_Jobs;
        $date = date('Y-m-d H:i:s', time() - (1 * 86400)); // 1 day ago
        $sql = $this->select()
                ->from(['j' => 'jobs'], ['id'])
                ->where('j.id_company =?', (int) $id_company)
                ->where('j.status = 1')
                ->where('j.created_at >=?', $date);
        $jobs = $this->fetchAll($sql);
        foreach ($jobs as $index => $id_job) {
            $data[] = $jobModel->getJobById($id_job['id']);
        }
        return $data;
    }

    public function getNewJobsLast_7($id_company) {
        $jobModel = new models_Jobs;
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 1 day ago
        $sql = $this->select()
                ->from(['j' => 'jobs'], ['id'])
                ->where('j.id_company =?', (int) $id_company)
                ->order('end DESC')
                ->where('j.status = 1');
//                ->where('j.created_at >=?',$date);
        $jobs = $this->fetchAll($sql);
        foreach ($jobs as $index => $id_job) {
            $data[] = $jobModel->getJobById($id_job['id']);
        }
        return $data;
    }

    public function reminder_7_Jobs() {

        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array())
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company"))
                ->joinLeft(['uu' => "users"], "uu.id = u.id_parent", ['firstname as recommender_firstname', 'lastname as recommender_lastname'])
                ->where("e.type =?", 44)
                ->where("u.status =?", 1)
                ->where("u.type =?", 1)
                ->where("c.status =?", 1)
//                ->group("u.id")
                ->where("e.status =?", 1);
        $result = $this->fetchAll($sql);


        $data = [];
        foreach ($result as $key => $row) {
            $data['company']['id_company'] = $row['id_company'];
            $data['company']['title_company'] = $row['title_company'];
            $data['company']['logo_company'] = $row['logo_company'];
            $data['company']['email_company'] = $row['email_company'];
            $data['company']['email_title'] = $row['email_title'];
            $data['company']['email_subject'] = $row['email_subject'];
            $data['company']['email_content'] = $row['email_content'];
            $user_data[$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"],
                "recommender_firstname" => $row["recommender_firstname"],
                "recommender_lastname" => $row["recommender_lastname"]);

            $data[$row['id_company']]['users'] = $this->finalUserFilterMail($user_data, $this->getUserSendedMail($row['id_company']));
            $data[$row['id_company']]['jobs'] = $this->getNewJobsLast_7($row['id_company']);
        }
        return $data;
    }

    public function getAllUsersByCompany($email_type = null, $user_type = null) {


        $sql = $this->select()->from(array("c" => "companies"), array("id AS id_company", "title AS title_company", "logo AS logo_company", "email AS email_company"))
//                ->join(array("j" => "jobs"), "j.id_company = c.id", array("id AS job_id", "bounty AS job_bounty", "bounty_file AS job_bounty_file", "end AS job_deadline"))
//                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job_title"))
                ->join(array("e" => "emails"), "e.id_company = c.id", array())
                ->join(array("el" => "emails_lang"), "el.id_email = e.id", array("title AS email_title", "subject AS email_subject", "content AS email_content"))
                ->join(array("u" => "users"), "u.id_company = c.id", array("id AS id_user", "UNIX_TIMESTAMP(u.created) AS created", "firstname", "lastname", "email AS user_email", "id_company AS user_company", "first_login"))
                ->where("e.type =?", (int) $email_type)
                ->where("u.status =?", 1)
                ->where("u.type =?", (int) $user_type)
                ->where("c.status =?", 1)
//                ->where("j.status =?", 1)
                ->where("e.status =?", 1);
        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_company']]['id_company'] = $row['id_company'];
            $data[$row['id_company']]['title_company'] = $row['title_company'];
            $data[$row['id_company']]['logo_company'] = $row['logo_company'];
            $data[$row['id_company']]['email_company'] = $row['email_company'];
            $data[$row['id_company']]['email_title'] = $row['email_title'];
            $data[$row['id_company']]['email_subject'] = $row['email_subject'];
            $data[$row['id_company']]['email_content'] = $row['email_content'];

            $data[$row['id_company']]['users'][$row['id_user']] = array(
                "id_user" => $row['id_user'],
                "created" => $row['created'],
                "firstname" => $row['firstname'],
                "lastname" => $row['lastname'],
                "user_email" => $row["user_email"],
                "user_company" => $row["user_company"],
                "first_login" => $row["first_login"]);

//            $data[$row['id_company']]['jobs'][$row['job_id']] = array(
//                "job_id" => $row['job_id'],
//                "job_bounty" => $row['job_bounty'],
//                "job_bounty_file" => $row['job_bounty_file'],
//                "job_title" => $row['job_title'],
//                "job_deadline" => $row['job_deadline'],
//            );
        }
        return $data;
    }

    public function getAlljobsApplications() {
        $modelJob = new models_Jobs;
        $data = $modelJob->getAllJobs();
        foreach ($data as $index => &$job) {
            $data[$index]['applicants'] = $this->getJobApplicants($job['id']);
            $data[$index]['new_applicants'] = $this->getJobNewApplicants($job['id']);
            $data[$index]['url'] = WebApp_Tools::createJobSlug($job);
        }
        return $data;
    }

    public function getJobApplicants($id_job) {
        $sql = $this->select()
                ->from(['ja' => 'job_applications'], ['count(ja.id) as total'])
                ->where("ja.id_job =?", (int) $id_job);
        return $this->fetchRow($sql)['total'];
    }

    public function getJobNewApplicants($id_job) {
        $sql = $this->select()
                ->from(['ja' => 'job_applications'], ['count(ja.id) as total'])
                ->where("ja.id_job =?", (int) $id_job)
                ->where("ja.status =?", (int) 0)
                ->where("ja.status_reason =?", (int) 0);
        return $this->fetchRow($sql)['total'];
    }

    public function jobsAndCampaignToExpire() {
        $next_week = date('Y-m-d', strtotime(' + 5 days')); // The following source code will add 5 days to current date.
        $modelJob = new models_Jobs;
        $modelCampaign = new models_Campaign;
        $data = $modelJob->getAllJobs();
        $campaigns = $this->getAllactiveCampaign();

        $jobs_to_expire = [];
        $campaign_to_expire = [];

        foreach ($data as $index => &$job) {
            if (strtotime($job['end']) <= strtotime($next_week)) {
                $jobs_to_expire[] = $data[$index];
            }
        }

        foreach ($campaigns as $index => &$campaign) {
            if (strtotime($campaign['end']) <= strtotime($next_week)) {
                $campaign_to_expire[] = $campaigns[$index];
            }
        }

        return ['jobs' => $jobs_to_expire, 'campaigns' => $campaign_to_expire];
    }

    public function getAllactiveCampaign() {
        $sql = $this->select()
                ->from(['c' => 'campaigns'], ['*'])
                ->where('c.status =?', 1);
        return $this->fetchAll($sql);
    }

    public function getCompanyBySubdomainSlug() {
        $params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "development", $options);
        $subdomain = explode('.', APPLICATION_URL);
        if (in_array("www", $subdomain)) {
            array_shift($subdomain);
        }
        $subdomain = $subdomain[0];
        $parts = explode("//", $subdomain);
        $subdomain = $parts[1];
        $sql = $this->select()->from(array("c" => "companies"), array("*"))->where("c.slug =?", $subdomain);
        $result = $this->fetchRow($sql);
        if (empty($result)) {
            $sql = $this->select()->from(array("c" => "companies"), array("*"))->where("c.slug =?", $params->resources->Settings->subdomain->$subdomain);
            $result = $this->fetchRow($sql);
        }
        $this->_company = $result;
        return $result;
    }

    public function getUserRecommendations($id_user) {
        $modelJob = new models_Jobs;
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("ja.date AS apply_date", "ja.updated as changed", "ja.status", "ja.status_reason", "ja.id as id_application"))
//                ->join(array("jr" => "job_recommendations"), "ja.id_user = jr.id_user and ja.id_recommender = jr.id_recommender")
                ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.email", "u.last_login", "u.email"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array('id as id_job'))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title AS job"))
                ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
                ->where("ja.id_recommender = ?", (int) $id_user)
//                ->order("jr.date DESC")
//                ->group("jr.id_user")
                ->group(["ja.id_job", "ja.date", "ja.updated", "ja.status", "ja.status_reason", "u.firstname", "u.lastname", "u.email", "u.last_login", "u.email", "jl.title", "c.title"]);
        $data = $this->fetchAll($sql);


        $sql = $this->select()
                ->from(array("jr" => "job_recommendations"), array("j.id", "jr.id_recommender", "jr.id_user", "jr.date", "jr.track"))
                ->join(array("j" => "jobs"), "jr.id_job = j.id", array('id as id_job'))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title AS job"))
                ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
                ->joinLeft(array("u" => "users"), "jr.id_user = u.id", array("u.firstname", "u.lastname", "u.last_login"))
                ->where("jr.id_recommender = ?", (int) $id_user)
                ->where("jr.id_user NOT IN (SELECT id_user FROM job_applications WHERE id_recommender = '{$id_user}')")
                ->order("jr.date DESC")
                ->group(["jr.track", "j.id", "jr.id_recommender", "jr.id_user", "jr.date", "jl.title", "c.title", "u.firstname", "u.lastname", "u.last_login"]);
        $none = $this->fetchAll($sql);
//        echo '<pre>';print_r($none);die;
        $result = array_merge($data, (array) $none);
        if (!empty($result)) {
            foreach ($result as $key => &$value) {
                $job = $modelJob->getJobById($value['id_job']);
                $value['url'] = WebApp_Tools::createJobSlug($job);
                $value['refferals_number'] = $this->countRefferalByUserJob($value['id_job'], $id_user);
            }
        }
        return $result;
    }

    public function countRefferalByUserJob($id_job, $id_user) {
        $sql = $this->select()
                ->from(['jr' => 'job_recommendations'], ['count(jr.id) as total'])
                ->where('jr.id_job =?', (int) $id_job)
                ->where('jr.id_recommender =?', (int) $id_user);
        return $this->fetchRow($sql)['total'];
    }

    public function getNewAllStatus() {

        return [
            [0, 0, "new"],
            [1, 0, "viewed"],
            [4, 9, "processing"],
            [3, 0, "moved_to_another_process"],
            [2, 1, "unfit_candidate"],
            [4, 5, "accepted"],
            [4, 0, "offered"],
            [4, 2, "tested"],
            [4, 6, "offer_rejected"],
            [4, 7, "no_answer"],
            [4, 8, "dropped_out"],
            [5, 1, "no_show"],
            [5, 2, "banned"],
            [5, 3, "final_interview"]
        ];
    }

    public function getUserByType($type) {
        $sql = $this->select()
                ->from(['u' => 'users'], ['*'])
                ->where("u.type =?", (int) $type)
                ->where("u.status = 1");
        return $this->fetchAll($sql);
    }

//    public function getEmailByType($type) {
//        $sql = $this->select()
//                ->from(array("e" => "emails"))
//                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject", "content"))
//                ->where("e.type = ?", (int) $type);
//            $sql->where("e.id_company =?", (int) $this->_company['id']);
//
//        $data = $this->fetchRow($sql);
//        return (array) $data;
//    }
    
    public function report() {

        $model_company = new models_Company;
        $params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
        $sitename = $params->resources->Settings->sitename;
        $disallowed = array('http://', 'https://');
        $slug = "";
        foreach($disallowed as $d) {
            if(strpos($sitename, $d) === 0) {
                $slug = str_replace($d, '', $sitename);
            }
        }
        $id_company = $model_company->getCompanyBySubdomainSlug($slug)['id'];

        $sql = $this->select()
                ->from(['jv' => 'job_views'],['count(id) as total_unique_view']);
        $data['total_unique_view'] = $this->fetchRow($sql)['total_unique_view'] ;
        
        
        $sql = $this->select()
                ->from(['ja' => 'job_applications'],['count(id) as applications']);
        $data['applications'] = $this->fetchRow($sql)['applications'] ;
        
        
        $sql = $this->select()
                ->from(['ja' => 'job_applications'],['count(id) as hired'])
                ->where("ja.status = 4")
                ->where("ja.status_reason = 5");
        $data['hired'] = $this->fetchRow($sql)['hired'] ;
        
        
        
        $sql = $this->select()
                ->from(['ja' => 'job_applications'],['*'])
                 ->joinLeft(['u' => 'users'], "u.id = ja.id_recommender",['*']);
        $data['applicants'] = $this->fetchAll($sql);
        
        $sql = $this->select()
                ->from(['cad' => 'campaign_actions_detailed'],['*','sum(value) as new_users'])
                ->joinLeft(['u' => 'users'], "u.id = cad.id_recommender",['*'])
                ->where("cad.action = 'new_candidates'")
                ->group("cad.id_recommender")
                ->order("new_users DESC")
                ->limit(3);
        $data['top_referrals'] = $this->fetchAll($sql);

        $data['top_channel'] = $this->getDashboardTopChannels($id_company);
        
         $sql = $this->select()
                ->from(['jr' => 'job_recommendations'],['*'])
                 ->join(['u' => 'users'], "u.id = jr.id_recommender",['*','count(id_user) as referals'])
                 ->group(['jr.id_recommender'])
                 ->order("referals DESC")
                 ->limit(10);
        $data['top_recommenders'] = $this->fetchAll($sql) ;
        $data['status'] = $this->getNewAllStatus() ;
        
        
        return $data ;        
    }
    
     public function getDashboardTopChannels($id_company) {

        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("COUNT(ja.id) AS applications", "source"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $id_company)
                ->group("ja.source")
                ->order("applications DESC")
                ->limit(3);

        $_sql = $this->select()
                ->from(array("jv" => "job_views"), array("COUNT(jv.id) AS views",
                    "source"))
                ->join(array("j" => "jobs"), "jv.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $id_company)
                ->where("jv.source !=''")
                ->group("jv.source")
                ->limit(3);
        
        $data = $this->fetchAll($sql);
        $_data = $this->fetchAll($_sql);
        $counter = count($data);
        $_counter = count($_data);

        for ($i = 0; $i <= $counter; $i++) {
            $arr = array_shift($data);
            $results[$arr['source']] = $arr;
        }

        for ($i = 0; $i <= $_counter; $i++) {
            $arr = array_shift($_data);
            $_results[$arr['source']] = $arr;
        }


        $combined = array_merge_recursive($_results, $results);

        return $combined;
    }
    
    public function setupAutomatUser($data)
    {
//        $data = [
//            'subdomain' => 'testy',
//            'email' => 'jeddey.seifeddine@talentspotting.com',
//            'firstname' => 'edin',
//            'lastname' => 'jeddey',
//            'phone' => '0720802433',
//        ];
        $this->insert(['title' => $data['subdomain'],
                       'slug' => $data['subdomain'],
                       'email' => $data['email']],'companies');
        
        $id = $this->lastInsertId(); 
        $password = uniqid();
        $this->insert([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'id_company' => $id,
            'password' => sha1($password),
            'status' => 1,
            'type' => 2
            
        ],'users');
        $update = new models_Update;
        $model_company = new models_Company;
        
        
                $params = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);

        $sitename = $params->resources->Settings->sitename;

        $transport = null;
        $config = ['auth' => "login",
            'username' => $params->resources->Settings->email_smtp->username,
            'password' => $params->resources->Settings->email_smtp->password
        ];
        $transport = new Zend_Mail_Transport_Smtp($params->resources->Settings->email_smtp->host, $config);

        $mail = new WebApp_Mail();
        $mail->setFrom('hello@talentspotting.com');
        $mail->setSubject('Created Talentspotting account');

        $mail->addTo($data['email']);
        $mail->setBodyHtml("user : {$data['email']} , passwd : $password");
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            {
            $mail->send($transport);
            }





        $model_company->addEmailTemplates($id);
        $update->newEmails();
    }
    
    public function getAutopostJobs()
    {
        $jobModel = new models_Jobs;
        $companyModel = new models_Company;
        $date = date('Y-m-d H:i:s', time() - (7 * 86400));
        $sql = $this->select()
                ->from(['aj' => 'autopost_job'],['*'])
                ->join(['u' => 'users'], 'aj.id_user = u.id',
                        ['facebook_token','email'])
                ->where('aj.date <= ?', $date);
        $result = $this->fetchAll($sql);
        foreach($result as $index => $value)
            {
                $job = $jobModel->getJobById($value['id_job']);
                $result[$index]['job'] = $job;
                $result[$index]['company'] = $companyModel->getCompanyById($job['id_company']);
                $url = WebApp_Tools::createJobSlug($job)
                        ."?track=".  md5($value['id_user'])
                        ."&source=".  md5('facebook');
                $result[$index]['share_link'] = $url;
                
            }
        return $result;
        
    }
    public function getAutopostLinkedinJobs()
    {
        $jobModel = new models_Jobs;
        $companyModel = new models_Company;
        $date = date('Y-m-d H:i:s', time() - (7 * 86400));
        $sql = $this->select()
                ->from(['aj' => 'autopost_linkedin_job'],['*'])
                ->join(['u' => 'users'], 'aj.id_user = u.id',
                        ['linkedin_token','email'])
                ->where('aj.date <= ?', $date);
        $result = $this->fetchAll($sql);
        foreach($result as $index => $value)
            {
                $job = $jobModel->getJobById($value['id_job']);
                $result[$index]['job'] = $job;
//                $result[$index]['company'] = $companyModel->getCompanyById($job['id_company']);
                $url = WebApp_Tools::createJobSlug($job)
                        ."?track=".  md5($value['id_user'])
                        ."&source=".  md5('facebook');
                $result[$index]['share_link'] = $url;
                
            }
        return $result;
        
    }
    
    public function removeFacebookToken($id_user)
    {
        $this->update(['facebook_token' => NULL], "id={$id_user}",'users');
    }

    public function getNotLoggedUsers()
    {
        $date = date('Y-m-d H:i:s', time() - (7 * 86400));
        $sql = $this->select()
            ->from(['c' => 'companies'],[])
            ->join(['u' => 'users'],'c.id=u.id_company',['*'])
            ->where('u.first_login IS NULL')
            ->where('u.type = 1')
            ->where('u.created <= DATE(NOW()) - INTERVAL 7 DAY');
        $data['users'] =  $this->fetchAll($sql);

        $sql = $this->select()
            ->from(['e' => 'emails'],['*'])
            ->join(['el'=>'emails_lang'],'e.id=el.id_email',['*'])
            ->where('e.type=63');
        $data['email'] = $this->fetchRow($sql);
        $id_company = $data['email']['id_company'];

        $sql = $this->select()
            ->from(['c' => 'companies'],['*'])
            ->where('c.id =?',(int) $id_company);
        $company = $this->fetchRow($sql);
        $data['company'] = $company;
        return $data;
    }

    public function logCron($data)
    {
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'emails_log');
    }
    

}


?>