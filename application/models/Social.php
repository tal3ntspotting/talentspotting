<?php
class models_Social extends WebApp_Db_Table
{
	private $_settings = array();
	public $_name = 'users';
	public $_user = '';
	protected $_primary = 'id';
	public $cache;
    public $id_lang;
    public $translate;
    protected $_stats;

	public function __construct($id_lang = null)
	{
		parent::__construct();
		$this->cache = new WebApp_Cache($this);

		$user = Zend_Auth::getInstance()->getIdentity();
		$this->_user = $user;
        
        $this->_stats = new models_Stats;
        
		if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap')))
		{
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
			$this->_settings =  $bootstrap['resources']['Settings'];
		}
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
	}

	public function recommentByMessage($connections, $id)
    {
        if (!empty($connections))
        {
            $sql = $this->select()->from(array("u" => "user_connections"))->where("id IN (".implode(",", $connections).")");
            $data = $this->fetchAll($sql);

            if (!empty($data))
            {
                $user = (array) $this->_user;
                $job = $this->getJobById($id);
                $track = uniqid();
                
                try
                {
                    $client = $this->initializeLinkedin($user);
                    foreach ($data as $row)
                    {
                        $items[] = array("person" => array("_path" => "/people/{$row['linkedin_id']}"));
                        $recommendation = array("id_recommender" => $this->_user->id, "id_job" => $id, "linkedin_id" => $row['linkedin_id'], "name" => $row['firstname']." ".$row['lastname'], "track" => $track, "date" => date("Y-m-d H:i:s"));
                        $this->addRecommendation($recommendation);
                        
                        $names[] = $row['firstname']." ".$row['lastname'];    
                        $this->_stats->setGamificationPoint($this->_model->_user->id, "invite_friends");                    
                    }                                                            
                    //$items[] = array("person" => array("_path" => "/people/0IbO1QUYlB"));
                    $linkedin_message = json_encode(array("recipients" => array("values" => $items), "subject" => "New job recommendation", "body" => "Hello, \n {$user['firstname']} {$user['lastname']} just recommended you to {$job['title']} @ {$job['company_title']}.\n\nYou can view the job details here: ".APPLICATION_URL."/".WebApp_Tools::createJobSlug($job)."?track={$track}"));
                    
                    try
                    {
                        $this->postLinkedinMessage($client, $linkedin_message);
                        $message = sprintf($this->translate->_("message_post_successfull"), implode(",", $names));
                    }
                    catch (Exception $e)
                    {
                        $message = $this->translate->_("error_posting_message");
                    }
                }
                catch (Exception $e)
                {
                    $message = $this->translate->_("no_linked_token_available");
                }
            }
            else
            {
                $message = $this->translate->_("no_connections_selected");
            }
        }
        else
        {
            $message = $this->translate->_("no_connections_selected");
        }
        
        return $message;
    }
    
    public function recommendPlatformByMessage($connections)
    {
        if (!empty($connections))
        {
            $sql = $this->select()->from(array("u" => "user_connections"))->where("id IN (".implode(",", $connections).")");
            $data = $this->fetchAll($sql);

            if (!empty($data))
            {
                $user = (array) $this->_user;
                try
                {                    
                    foreach ($data as $row)
                    {
                        $items[] = array("person" => array("_path" => "/people/{$row['linkedin_id']}"));
                        $names[] = $row['firstname']." ".$row['lastname'];                        
                        $this->_stats->setGamificationPoint($this->_model->_user->id, "invite_friends");
                        
                    }                                                            
                    //$items[] = array("person" => array("_path" => "/people/0IbO1QUYlB"));
                    $linkedin_message = json_encode(array("recipients" => array("values" => $items), "subject" => "Invitation to {$this->_settings['sitename']}", "body" => "Hello, \n {$user['firstname']} {$user['lastname']} just invited you to {$this->_settings['sitename']}."));
                    
                    try
                    {
                        $this->postLinkedinMessage($user, $linkedin_message);
                        $message = sprintf($this->translate->_("message_post_successfull"), implode(",", $names));
                    }
                    catch (Exception $e)
                    {
                        $message = $this->translate->_("error_posting_message");
                    }
                }
                catch (Exception $e)
                {
                    $message = $this->translate->_("no_linked_token_available");
                }
            }
            else
            {
                $message = $this->translate->_("no_connections_selected");
            }
        }
        else
        {
            $message = $this->translate->_("no_connections_selected");
        }
        
        return $message;
    }
    
    public function recommendEmail($connections)
    {

        if (!empty($connections))
        {
            $user = (array) $this->_user;
            
            $emailContent = $this->getEmailByType(21);
            if (!empty($emailContent))
            {
                $mail = new WebApp_Mail();
                foreach ($connections as $row)
                {
                    $mail->addTo($row, $row);        
                    $mail->setFrom($modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_settings['sitename']);
                    $mail->setSubject($emailContent['subject']);
                    $user['email'] = $row;
                    $mail->setBodyFromHTML($emailContent['content'], array("data" => $user));
                    $mail->send($transport);    
                    
                    $this->_stats->setGamificationPoint($this->_model->_user->id, "invite_friends");                    
                }
                
                $message = sprintf($this->translate->_("message_post_successfull"), implode(",", $connections));    
            }
            else
            {
                $message = $this->translate->_("no_email_selected");
            }
        }
        else
        {
            $message = $this->translate->_("no_connections_selected");
        }
        
        return $message;
    }
    
    public function initializeLinkedin($user)
    {
        $token = unserialize($user['linkedin_token']);
        $options = array('consumerKey' => $this->_settings['linkedin']['api_key'],'consumerSecret' => $this->_settings['linkedin']['secret']);
        $client = $token->getHttpClient($options);
        
        return $client;
    }
    
    public function postLinkedinMessage($user, $message)
    {
        $client = new Zend_Http_Client("https://api.linkedin.com");
                    
        $client->setUri('http://api.linkedin.com/v1/people/~/mailbox');
        $client->setMethod(Zend_Http_Client::POST);
        $client->setHeaders("Authorization", "Bearer {$user['linkedin_token']}");
        $client->setHeaders("x-li-format", "json");
        $client->setRawData($message);
        
        $response = $client->request();        
    }
    
    public function getJobById($id)
    {
        $modelJobs = new models_Jobs;
        return $modelJobs->getJobById($id);
    }
    
    public function addRecommendation($data)
    {
        $sql = $this->select()->from(array("j" => "job_recommendations"))->where("id_recommender = ?", $data['id_recommender'])->where("linkedin_id = ?", $data['linkedin_id'])->where("id_job = ?", $data['id_job']);
        $result = $this->fetchAll($sql);
        if (empty($result))
        {
            $this->insert($data, "job_recommendations");
            $this->_stats->setGamificationPoint($this->_model->_user->id, "job_recommend");
        }
    }
    
    public function getEmailByType($type)
    {
        $sql = $this->select()
                    ->from(array("e" => "emails"))
                    ->joinLeft(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "id_lang", "subject", "content"))
                    ->where("e.type = ?", $type);
        $data = $this->fetchAll($sql);
        
        foreach ($data as $row)
        {
            $email[$row['id_lang']]['title'] = $row['title'];
            $email[$row['id_lang']]['subject'] = $row['subject'];
            $email[$row['id_lang']]['content'] = $row['content'];             
        }
        
        if (array_key_exists($this->id_lang, $email))
        {
            return $email[$this->id_lang];
        }
        else if (!empty($email))
        {
            return $email[key($email)];
        }
        else
        {
            return false;
        }
    }
    
    public function createShortUrl($url)
    {
        //$url = APPLICATION_URL.$url;
        
        if (strpos($url, "/jobs/") !== false)
        {
            $track = md5((int) $this->_user->id);            
            //$url .= "?track=".$track;
        }
        
        return str_replace("?trk", "?track", $url);

        $client = new Zend_Http_Client($this->_settings['google']['short_url']);
        $client->setParameterGet("key", $this->_settings['google']['api_key_short']);
        
        $content = array("longUrl" => $url);
        $client->setRawData(json_encode($content), "application/json");
        
        $request = $client->request("POST");
        $response = json_decode($request->getBody());

        if ($response->id)
        {
            return $response->id;
        }
        else
        {
            return $url;
        }
    }
}