<?php

class models_Reports extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'users';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    public $id_lang;
    public $translate;
    protected $_stats;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $this->translate = Zend_Registry::get("Zend_Translate");

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }

    public function getDashboardsNewApplications() {
        
        $limit = 5 ;
        
        
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("COUNT(id_user) AS new_applications", 
                    "(SELECT COUNT(id) FROM job_applications WHERE id_job = ja.id_job GROUP BY id_job) AS total"
                   ,"(SELECT COUNT(id) FROM job_applications WHERE id_job = ja.id_job AND status = 1 GROUP BY id_job) AS new_application"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array('j.id'))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("jl.title"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where('j.status =1')
                ->where("ja.date > '" . date("Y-m-d H:i:s", strtotime("-4 months")) . "'")
                ->group(["ja.id_job","j.id","jl.title"]);

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }
        $sql->limit($limit);
        $job_application =  $this->fetchAll($sql);
        
        $counted_data = count($job_application);
        if($counted_data < $limit) {
            $rest_limit = $limit - $counted_data ;
            $sql = $this->select()
                    ->from(array("j" =>"jobs"),array("(SELECT COUNT(id) FROM job_applications ja WHERE j.id = ja.id_job GROUP BY id_job) AS total",
                        "(SELECT COUNT(id) FROM job_applications ja WHERE j.id = ja.id_job AND status = 0 GROUP BY id_job) AS new_application",'id'))
                    ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job",array("title"))
                    ->order("j.updated DESC")
                    ->where('j.status =1')
                    ->where("j.id_company = '{$this->_user->id_company}'")
                    ->limit($rest_limit);
                    foreach($job_application as $key => $value) 
                    {
                        $sql->where("j.id != {$value['id']}");
                    }
                    
                    $data = $this->fetchAll($sql);
                    return array_merge($data,$job_application);
                    
        } else if( $counted_data == $limit) return $job_application ;
        
    }

    public function getDashboardCampaignsInfo() {
        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(views) AS reached"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array());

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("c.id_company = '{$this->_user->id_company}'");
        }

        $data = $this->fetchRow($sql);

        $report['reached'] = (int) $data['reached'];

        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(recommendations) AS recommendations"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array());

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("c.id_company = '{$this->_user->id_company}'");
        }

        $data = $this->fetchRow($sql);

        $report['recommendations'] = (int) $data['recommendations'];

        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(shares) AS shares"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array());

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("c.id_company = '{$this->_user->id_company}'");
        }

        $data = $this->fetchRow($sql);

        $report['shares'] = (int) $data['shares'];

        $sql = $this->select()
                ->from(array("cr" => "campaign_reports"), array("SUM(applications) AS applications"))
                ->join(array("c" => "campaigns"), "cr.id_campaign = c.id", array());

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->where("c.id_company = '{$this->_user->id_company}'");
        }

        $data = $this->fetchRow($sql);

        $report['applications'] = (int) $data['applications'];

        return $report;
    }

    public function getDashboardLeaderboardsInfo() {
        $sql = $this->select()
                ->from(array("cj" => "campaign_jobs"), array())
                ->join(array("c" => "campaigns"), "cj.id_campaign = c.id", array())
                ->join(array("jl" => "jobs_lang"), "cj.id_job = jl.id_job", array("title AS job"))
                ->join(array("j" => "job_recommendations"), "cj.id_job = j.id_job", array("COUNT(j.id) AS recommendations"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("c.id_company = ?", (int) $this->_user->id_company)
                ->order("recommendations DESC")
                ->group("jl.title");

        return $this->fetchPairs($sql);
    }


    public function getnumjobs() {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array("j.id"))
                ->join(array('jl' => 'jobs_lang'), "jl.id_job = j.id", array("jl.title AS job"))
                ->join(array('jr' => 'job_recommendations'), "jr.id_job = j.id", array("COUNT(jr.id) AS recommendations"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("j.status =?", 1)
                ->order("recommendations DESC")
                ->group(["j.id"]);
        $data = $this->fetchRow($sql);
        return array('job' => $data, 'id' => $data['id']);
    }

    public function RecommenderNumforjobById($id) {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("id_company", "id as id_job"))
                ->join(array("jr" => "job_recommendations"), "jr.id_job = j.id", array("COUNT(jr.id) AS recommenders"))
                ->where("jr.id_job =?", (int) $id)
                ->where("j.id_company =?", (int) $this->_user->id_company)
                ->order("recommenders DESC")
                ->group(["j.id","id_company","id_job","jr.id"]);
        return $this->fetchRow($sql);
    }

    public function topDepartmentRecommenders() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("id_company", "(SELECT COUNT(id) FROM job_recommendations WHERE id_job = j.id) AS recommenders"))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title AS title_job", "slug AS slug_job", "id_job"))
                ->join(array("jd" => "job_departments"), "j.id = jd.id_job", array("id_department"))
                ->join(array("dl" => "departments_lang"), "jd.id_department = dl.id_department", array("title AS title_department"))
                ->where("j.id_company =?", (int) $this->_user->id_company)
                ->where("jl.id_lang = ?", $this->id_lang)
                ->order("recommenders DESC")
                ->limit(1);
//                ->join(array("dl" => "departments_lang"),"d.id = dl.id_department")
//                ->join(array("jd" => "job_departments")," d.id = jd.id_department");

        $result = $this->fetchRow($sql);
        if (($result['recommenders'] > 0)) {
            return $result;
        } else {
            return;
        }
    }

    public function recommendersTotal($id_company) {

        
        $sql = $this->select()->from(array('jr' => "job_recommendations"),array("jr.id","jr.id_recommender"))
                ->join(array("u" => "users"),"u.id = jr.id_recommender",array())
                ->group(["jr.id_recommender"])
                ->where("u.id_company =?" ,(int) $id_company );
//                ->where("j.status =?",1);
        $data = $this->fetchAll($sql);
        $count = count($data);
        $result['recommendersTotal'] =  $count ;
        if ($result['recommendersTotal'] > 0) {
            return $result;
        } else {
            return;
        }
    }

    public function getDashboardEmployeesEngagement() {
        $sql = $this->select()
                ->from(array("u" => "users"), array("COUNT(u.id) AS total"))
                ->where("id_company = ?", (int) $this->_user->id_company);
        $data = $this->fetchRow($sql);

        $report['total'] = (int) $data['total'];

        $sql = $this->select()
                ->from(array("u" => "users"), array("COUNT(u.id) AS total"))
                ->where("id_company = ?", (int) $this->_user->id_company)
                ->where("status = 1")
                ->where("u.last_login IS NOT NULL");
        $data = $this->fetchRow($sql);

        $report['active'] = (int) $data['total'];

        return $report;
    }

    public function getDashboardTopChannels() {

        $sql = $this->select()
            ->from(['sm' => 'social_media'],['sm.title','sm.id as id_source',
                "(SELECT COUNT(ja.id) from job_applications ja where sm.id=ja.source group by ja.source ) as applicants"])
            ->joinLeft(['jv'=>'job_views'],"jv.source=sm.id",['COUNT(jv.id) as views'])
            ->where('sm.title != "internal"')
            ->group('sm.id');
        $result = $this->fetchAll($sql);
        foreach ($result as $k => &$value)
        {
            if($value['applicants'] == "") $value['applicants'] = 0;
            if($value['views'] == "") $value['views'] = 0;
        }
        return $result;

        $sql = $this->select()
                ->from(array("jv" => "job_views"), array("COUNT(jv.id) AS views"))
                ->join(array("j" => "jobs"), "jv.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("jv.source ='website'");

        $_sql = $this->select()
                ->from(array("jv" => "job_views"), array("COUNT(jv.id) AS views",
                    "source"))
                ->join(array("j" => "jobs"), "jv.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("jv.source !=''")
                ->group("jv.source")
                ->limit(3);
        
        $data = $this->fetchRow($sql);
        $_data = $this->fetchAll($_sql);
        
        $map = [];
        foreach($_data as $key => $value) {
            $sql = $this->select()->from(['ja' => 'job_applications'],
                                         ['count(id) as applications'])
                    ->where('ja.source =?', (string) $value['source']);
            $app = $this->fetchRow($sql)['applications'];
            $map[$value['source']] = [
                'views' => $value['views'] ,
                   'applications' => $app];
           
            
        }
        
         $sql = $this->select()->from(['ja' => 'job_applications'], 
                 ['count(id) as applications'])
                ->where('ja.source =?', (string) "website");
        $applications = $this->fetchRow($sql)['applications'];
        $data['applications'] = $applications;
        $map['website'] = $data;
        return $map ;
        
//        $counter = count($data);
//        $_counter = count($_data);
//
//        for ($i = 0; $i <= $counter; $i++) {
//            $arr = array_shift($data);
//            $results[$arr['source']] = $arr;
//        }
//
//        for ($i = 0; $i <= $_counter; $i++) {
//            $arr = array_shift($_data);
//            $_results[$arr['source']] = $arr;
//        }
//
//
//        $combined = array_merge_recursive($_results, $results);
//
//        return $combined;
    }

    public function getDashboardTopViewedJobs() {
        $modelJobs = new models_Jobs;
        $sql = $this->select()
                ->from(array("jv" => "job_views"), array("COUNT(jv.id) AS total","jv.id_job"))
                ->join(array("j" => "jobs"), "jv.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("jv.source != ''")
                ->group(["j.id","jv.id_job"])
                ->order("total DESC")
                ->limit(5);

        $jobs_views =  $this->fetchAll($sql);
        foreach($jobs_views as $key => $job) {
            $jobs_views[$key]['title'] = $modelJobs->getJobById($job['id_job'])['title'];
        }
        return $jobs_views ;
    }

    public function getDashboardTopRefferals() {
        $sql = $this->select()
                ->from(array("jr" => "job_recommendations"), array("COUNT(jr.id) AS total"))
                ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
                ->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())
                ->join(array("dl" => "departments_lang"), "jd.id_department = dl.id_department", array("dl.title"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->group(["dl.id_department"])
                ->order("total DESC");
        $all = array_merge($this->getCustomDashboardTopRefferals(),$this->fetchAll($sql));
        usort($all, function($a,$b)
        {
            return $b['total'] - $a['total'];
        });
        $result = array_slice($all, 0, 5, true);
        return $result;
    }
    public function getCustomDashboardTopRefferals() {
        $sql = $this->select()
                ->from(array("jr" => "job_recommendations"), array("COUNT(jr.id) AS total"))
                ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
                ->join(array("jd" => "custom_job_departments"), "j.id = jd.id_job", array())
                ->join(array("dl" => "custom_departments"), "jd.id_department = dl.id", array("dl.title"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->group(["jd.id_department"])
                ->order("total DESC");
        return $this->fetchAll($sql);
    }

    public function getTopJobRefferals() {
        $sql = $this->select()
                ->from(array("jr" => "job_recommendations"), array("COUNT(jr.id) AS total"))
                ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("jl.title"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->group("jr.id_job")
                ->order("total DESC")
                ->limit(6);
        return $this->fetchAll($sql);
    }
    
    public function getDashboardCampaignsViewsInfo() {
        $sql = $this->select()->from(['cad' => 'campaign_actions_detailed'],['count(id) as views'])
                ->where('cad.action =?','views');
        return $this->fetchRow($sql);
    }
    
    public function getDashboardCampaignsSharesInfo() {
        // get all jobs in campaign
        $sql = $this->select()->from(['cj' => 'campaign_jobs'],['id_job']);
        $jobs = array_map("unserialize", array_unique(array_map("serialize", $this->fetchAll($sql))));
        $array = array_column($jobs, 'id_job');
        if(!empty($array)) {
            // sum shares from campaign actions detailed
            $SQL = $this->select()->from(['js' => 'jobs_share'], ['sum(shares) as shares']);
            $SQL->where("js.id_job IN (" . implode(",", $array) . ")");

            return $this->fetchRow($SQL);
        } else {
            return ['shares' => 0];
        }
        
    }
}
