<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cron
 *
 * @author edin
 */
class models_Update extends WebApp_Db_Table {

//put your code here
    private $_settings = array();
    public $_name = 'emails';
    public $_user = '';
    protected $_primary = 'id';

    public function addSlugCampaignColumn()
    {
        $sql = "ALTER TABLE `campaigns` ADD `slug` VARCHAR(250) NULL DEFAULT NULL AFTER `title`;"
            . "ALTER TABLE `users` ADD `source` VARCHAR(250) NULL DEFAULT NULL AFTER `lastname`;";
        $this->query($sql);
    }

    public function getSlugForCampaign()
    {
        $sql = $this->select()->from(['c' => 'campaigns'], ['*']);
        $data = $this->fetchAll($sql);
        return $data;
    }

    public function updateSlugForCampaign($value)
    {
        $this->update(['slug' => WebApp_Tools::CreateAlias($value['title'])], 'id = ' . $value['id'], 'campaigns');
    }

    public function autoIncrementForCustomTables()
    {
        $sql[] = "ALTER TABLE `custom_cities` AUTO_INCREMENT = 100000;";
        $sql[] = "ALTER TABLE `custom_departments` AUTO_INCREMENT = 100000;";
        $sql[] = "ALTER TABLE `custom_skills` AUTO_INCREMENT = 100000;";
        $sql[] = "ALTER TABLE `custom_location` AUTO_INCREMENT = 100000;";
        $sql[] = "ALTER TABLE `custom_user_skills` AUTO_INCREMENT = 100000;";
        foreach ($sql as $sql)
        {
            $this->query($sql);
        }
    }

    public function newEmails()
    {
        $cron = new models_Cron;
        $company = new models_Company;
        $result = $company->getCompanies();

        foreach ($result as $index => $company)
        {
            $id_company = $company['id'];
            $company_name = $company['title'];
            $company_email = $company['email'];
            

            $data['id_company'] = $id_company;
            $data['type'] = 46;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Set up your Talentspotting account";
                $subject = <<<'HERE'
                        Set up your Talentspotting account
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
      		  <p>Are you ready to start using Talentspotting to recruit fast and easy? <br>
Set up your company’s account in 30 seconds. </p> 

                  <p>You’re an Administrator for your company’s account, which means you can see all referral data, add and delete jobs, track referral rewards, add campaigns, and more. </p>

                  <b>How Does It Work? </b>

                  <ol>
                    <li>First, make sure to log in {$settings.sitename} with your company e-mail address and password. </li>
                    <li>Next, click Settings > Company<br>Check appropriate boxes to ensure whether Candidates and Admins can also recommend, and whether Recommenders can also apply to jobs on Talentspotting.<br>Select referral track timeline – how long you want a Candidate to be connected to their Recommender. Within that period, if a Candidate applies for another job, their Recommender will be eligible for a referral bounty.</li>
                    <li>Click Settings > Users<br>Import all the Recommenders in your company via the Import from CSV button<br>Add Users one by one and assign them roles – Admins, Recommenders, Candidates via the Add Users button <br>Add Job Title, Cities, Departments, Skills, Description and Requirements.</li> 
                    <li>Check out Settings > Email templates<br>Those are all the notifications your users will receive, depending on their roles. You can edit their content at any time. </li>
                  </ol>

                  <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>



			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }



            $data['id_company'] = $id_company;
            $data['type'] = 47;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Super easy way to create jobs on Talentspotting";
                $subject = <<<'HERE'
                        Super easy way to create jobs on Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
      		  <p>Want to get referrals rolling into your Talentspotting account? <br>Upload your open jobs first. </p> 

                  <b>How Does It Work? </b>

                  <ol>

                    <li>First, make sure to log in on {$settings.sitename} with your company e-mail address and password.</li>
                    <li>Next, click Job Management > Add new job.</li>
                    <li>Add Job Title, Cities, Departments, Skills, Description and Requirements.</li>
                    <li>Set the Bounty for colleagues who refer a successfully hired candidate<br> If you write a number, it’ll convert to Euro. If you give out a prize, write its name and upload a picture of the prize.<br>Don’t forget to choose the bounty due date, otherwise we’ll be unable to remind you when to give out rewards to your successful recommenders.</li>
                    <li>Select job Start and End Date<br>Once the end date passes, this job will be automatically deactivated, unless you adjust the date later on.</li>
                    <li>Pin to top<br>If this is a hot job, keep it on top of all other jobs by pressing the Pin to top button.</li>

                  </ol>

                 <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }





            $data['id_company'] = $id_company;
            $data['type'] = 48;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Invite your colleagues on Talentspotting";
                $subject = <<<'HERE'
                        Invite your colleagues on Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>To complete your Talentspotting set up, invite all your colleagues to recommend on Talentspotting.</p>
            <b>How Does It Work? </b>

                  <ol>

                    <li>First, make sure to log in {$settings.sitename} with your company e-mail address and password. Follow the instructions here</li>
                    <li>Next, click Settings > Users.</li>
                    <li>Import all the Recommenders in your company<br>Click the Import from CSV button, download our template, fill it in with your users data (first name, last name, e-mail, role – Admin or Recommender) and upload it. All those invited will receive an automatic notification by mail and will be able to login into Talentspotting to refer.</li>
                    <li>Add new users<br>For users you want to add one-by-one just press the Add New Users button, fill in their First Name, Last Name, E-mail, assign them roles – Admins or Recommenders – and hit Save. All those invited will receive an automatic notification by mail and will be able to login into Talentspotting to refer.</li>
                    <li>See active users<br>You can track all active users in the Settings > Users section<br>For more details, learn all about setting up your Talentspotting account.</li>

                  </ol>

           <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>


                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
             
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }



            $data['id_company'] = $id_company;
            $data['type'] = 49;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "The ONE thing you must do to get results on Talentspotting ";
                $subject = <<<'HERE'
                        The ONE thing you must do to get results on Talentspotting 
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},</p>
        	<br>
        	<br>
        	<p>Now that you’ve set up your Talentspotting account and started receiving referrals, you probably wonder what you should be doing next.</p> 

                  <p>We have plenty of tips and tricks in store but today we’ll start with the ONE thing you must do to ensure spectacular referral results:<br><b>Change candidates’ status as fast as possible! </b></p>

                  <p>That’s it! </p>

                  <p>As soon as you receive an applicant notification from us, log in on Talentspotting, go to Job Management, select the job your candidate applied for, and change their recruitment status.</p>

                  <p>Once you do that, both the Candidate and their Recommender get an automatic notification. This will ensure a high level of engagement year-round and will show your colleagues that their involvement matters.</p>
                  
                  <br>
                  <b>Want to build a top-class referral programme?</b>
                  <p>Process referred candidates first. And let everybody know it.<br>If you can also commit to processing referred candidates within a specific period of time (48h, 72h, 7 days, etc.), you’ll reap the benefits of a highly engaged referral community.</p>
                  <p>Remember! Hundreds of recommenders have said that Transparency is the no. <b>#2</b> referral motivator, right after Simplicity.</p>



                 <p><br>So change your candidates’ status now & happy recruiting!<br>{$settings.sitename}</p>

			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }





            $data['id_company'] = $id_company;
            $data['type'] = 50;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Top your next recruitment peak with Talentspotting";
                $subject = <<<'HERE'
                        Top your next recruitment peak with Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	 <b>Want to meet your KPIs faster? </b>

                  <p>One of the quickest ways to dive in is to use Talentspotting campaigns for your recruitment peak: Choose your hot jobs, assign the referral objectives you want to reward and set bounties for the winners.</p>

                  <p>You can even target certain departments or people to build relevant engagement for any objective. Many of our first-time users say this was a quick way for them to experience Talentspotting’s benefits. Hope it helps you as well!</p>

                  <p>Once you login, follow these steps:</p>

                  <ol>
                    <li>Click Campaigns > Add new Campaign</li>
                    <li>Add a name for Campaign<br>For example: "Get extra…"</li>
                    <li>Select Jobs you want to feature<br>Make sure to upload these jobs to your account first </li>
                    <li>Set bounties for the winners</li>
                    <li>Assign referral objectives<br>Reward colleagues who get the most Views, New Applicants, Successful Hires, etc. for the Jobs you featured </li>
                    <li>Target your campaign<br>If your hot jobs are niched, you can activate the campaign only for colleagues in a certain city, department or by name</li>
                    <li>Choose campaign duration</li>
                    <li>Click Save & Activate</li>
                  </ol>

                  <p>All your Recommenders will get notified by e-mail </p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id_company;
            $data['type'] = 52;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Wow! New applicants on Talentspotting";
                $subject = <<<'HERE'
                        Wow! New applicants on Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>Nice work out there this week! Here’s a quick recap of your applicants on Talentspotting.</p>
                  <p>Make sure you change their status in the hiring process. Priority processing of referred candidates helps you boost the results of your referral programme and keeps your recommenders engaged.</p>


{$table}




                  <p>Track your progress in the Job Management section of your account.</p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }







            $data['id_company'] = $id_company;
            $data['type'] = 53;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "These jobs & campaigns are about to expire";
                $subject = <<<'HERE'
                        These jobs & campaigns are about to expire
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname},</p>
        	<br>
        	<br>
        	<p>Here are all the Talentspotting jobs and campaigns due to expire next week. If you make no changes, they will be automatically deactivated after the expiry date. </p>

 {$table}




                  <p>Edit job and campaign details if you want to keep them visible for your Recommenders after the current expiry date.</p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>

			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }






            $data['id_company'] = $id_company;
            $data['type'] = 54;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Your monthly Talentspotting report";
                $subject = <<<'HERE'
                        Your monthly Talentspotting report
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	    Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>Nice work out there this month! Here’s a quick recap of your activity on Talentspotting.</p>
                  


                  {$table}




                  <p>Keep up the good work and please let us know if you need help with your referral strategy at <a href="mailto:hello@talentspotting.com.">hello@talentspotting.com.</a> </p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }





            $data['id_company'] = $id_company;
            $data['type'] = 55;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Your campaign is closed! The winner is";
                $subject = <<<'HERE'
                        Your campaign is closed! The winner is
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
        	<p><b>Campaign "{$campaign.title}" is now closed. The top 3 winners by points are:</b></p>
                  <ul>
                    {foreach from=$winners key=key item=item}
                    {$key + 1 }. {$item.firstname} {$item.firstname} : {$item.total_points} <br />
                    {/foreach} 
                  </ul>
            <br>
            <p>Don't forget to give them their award and congratulate them for the good work!</p>
                  
            <p>For more details, please access Analytics - Leaderboard - Campaigns in your account. </p>
			<br>
			<p>Activate another campaign in <a href="{$settings.sitename}/view-campaigns">Campaigns</a>  - Add new Campaign. </p>
			<br>
			<p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<br><br>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id_company;
            $data['type'] = 58;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Talentspotting Tip 3:5 Gamification Points";
                $subject = <<<'HERE'
                        Talentspotting Tip 3:5 Gamification Points
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'


<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},<br>
        	<br>
        	<br>
      		    <p>At Talentspotting we think all your referral efforts deserve to be rewarded, even if your candidate is hired or not. </p>
                 

                 <p><p>That’s why all your actions get gamification points:</p><ul>
                  <li>1 view = 1 point </li>
                  <li>Active account = 10 points</li>
                  <li>Candidate registered on Talentspotting = 10 points</li>
                  <li>Candidate applied on Talentspotting = 20 points</li>
                  <li>Candidate on Talentspotting is hired = 50 points</li>
                 </ul></p>


                 <p>If recruiters activate a referral Campaign, you can win the campaign bounty on top of the regular rewards by getting the most points for that campaign’s objectives, e.g. views, new candidates registered, new applicants, etc.</p>

                 <p><b>Don’t miss out!</b> See your ranking and how many points you still need to top your company’s referral Leaderboard in the <a href="{$settings.sitename}/profile#/leaderboards">Campaigns & Leaderboards</a> section of your account. </p>

                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id_company;
            $data['type'] = 60;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Talentspotting tip 5:5 Fastest way to refer a friend";
                $subject = <<<'HERE'
                        Talentspotting tip 5:5 Fastest way to refer a friend
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname},<br>
        	<br>
        	<br>
      		    <p>Time is important - so why waste it switching back and forth between an endless sea of tabs and e-mails? Talentspotting lets you refer in a few seconds and brings all your referral data in one place.</p>
                  
                  <p>Here’s how:
                    <ol>
                      <li><b>Refer even without a CV</b><br>Copy your unique job URL and send it to your friends via e-mail, social media, Whatsapp, sms or simply by posting it to your social accounts. Try it on mobile too!</li>
                      <li><b>Referral tracking</b><br>Click Recommendations to see all the people you've invited to apply for your Company's jobs on Talentspotting. If they activated their account, their name will show up next to their e-mail address.</li>
                      <li><b> Know what your candidates are up to</b><br>We send you e-mail notifications every time Recruiters move your referred candidates further along in the recruitment process.</li>
                      <li><b>Never miss out on a reward!</b><br>You'll get notified about new jobs open for referral as soon as they are posted. If you refer and your candidate is hired, you’ll see your reward and its due date in the Rewards section.</li>
                      <li><b>Know what else you can do to get rewards</b><br>Track your performance in referral campaigns and rise to the top of the recommender Leaderboard by accumulating more gamification points. To get many points fast try sharing jobs on social media.</li>
                    </ol>
                  </p>  
                  <p>Refer fast and easy from your Talentspotting account!</p>


                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id_company;
            $data['type'] = 62;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID)
            {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Your referred candidates ";
                $subject = <<<'HERE'
                        Your referred candidates 
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	    Hi,<br>
        	<br>
        	<br>
        	<p>Nice work out there this week! Here’s a quick recap of your applicants on Talentspotting.</p>

                 {$table}
                  
                  <p>Send reminders to friends who haven’t applied yet in the <a href="{$settings.sitename}/profile#/recommendations"><b>Recommendations</b></a> section of your account.</p>

                  <p>Your position in the Talentspotting Leaderboard: {$leaderboard.position} </p>

                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
        }

    }

    public function addLinkedinConnectionsTables()
    {
        $sql[] = "CREATE TABLE IF NOT EXISTS `linkedin_connections` (
                `id` int(11) NOT NULL,
                `firstname` varchar(200) DEFAULT NULL,
                `lastname` varchar(200) DEFAULT NULL,
                `email` varchar(200) DEFAULT NULL,
                `company` varchar(200) DEFAULT NULL,
                `position` varchar(200) DEFAULT NULL,
                `connected` date NOT NULL,
                `id_user` int(11) NOT NULL DEFAULT '0',
                `linked` int(1) NOT NULL DEFAULT '0'
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $sql[] = "ALTER TABLE `linkedin_connections` ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `linkedin_connections` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        foreach ($sql as $sql)
        {
            $this->query($sql);
        }
    }

    public function updateErrorTextInEmails($error = null, $text = null)
    {
        if ($error == null || $text == null)
        {
            throw new Exception('params not setted ');
        }
        $model_company = new models_Company;
        $companies = $model_company->getCompanies();

        foreach ($companies as $key => &$value)
        {
            $companies[$key]['emails_template'] = $this->getEmailsTemplateByCompanyId($value['id']);
            $mails_template = $this->getEmailsTemplateByCompanyId($value['id']);
            foreach ($mails_template as $key => $email)
            {
                $content = $email['content'];
                $corrected_content = str_replace($error, $text, $content);

                $this->updateEmailContentFromErrors(['content' => $corrected_content], $email['id_emails_lang']);
            }
        }
    }

    public function getEmailsTemplateByCompanyId($id_cpmpany)
    {
        $sql = $this->select()
            ->from(['e' => 'emails'], ['e.id as id_emails', 'e.*'])
            ->join(['el' => 'emails_lang'], 'e.id = el.id_email', ['el.id as id_emails_lang', 'el.*'])
            ->where('e.id_company =?', (int) $id_cpmpany);
        return $this->fetchAll($sql);
    }
    public function getEmailsTemplateByTypeCompanyId($id_cpmpany,$type)
    {
        $sql = $this->select()
            ->from(['e' => 'emails'], ['e.id as id_emails', 'e.*'])
            ->join(['el' => 'emails_lang'], 'e.id = el.id_email', ['el.id as id_emails_lang', 'el.*'])
            ->where('e.id_company =?', (int) $id_cpmpany)
            ->where('e.type =?', (int) $type);
        return $this->fetchAll($sql);
    }

    public function updateEmailContentFromErrors($data, $id)
    {
        $this->update($data, "id = {$id}", 'emails_lang');
    }

    public function addRequireStep3FieldsTable()
    {
        $sql = "ALTER TABLE `companies` ADD `signin` INT(1) NOT NULL DEFAULT '1' AFTER `is_pilote`";
        $this->query($sql);
    }

    public function UpdateBountyFieldToInt()
    {
        $sql = "ALTER TABLE `jobs` CHANGE `job_ad_lifetime` `job_ad_lifetime` INT(4) NOT NULL;";
        $this->query($sql);
    }

    public function setupAutopostTable()
    {
        $sql[] = "CREATE TABLE IF NOT EXISTS `autopost_job` (
                `id` int(11) NOT NULL,
                `id_user` int(11) NOT NULL,
                `id_job` int(11) NOT NULL,
                `date` datetime NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $sql[] = "ALTER TABLE `autopost_job`
                 ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `autopost_job`
                  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $sql[] = "ALTER TABLE `autopost_job` ADD UNIQUE(`id_job`);";

        foreach ($sql as $sql)
        {
            $this->query($sql);
        }
    }

    public function setupGoogeanalytics()
    {
        $sql = "ALTER TABLE `companies` ADD `analytics` "
            . "VARCHAR(100) NOT NULL AFTER `track_timeline`;";
        $this->query($sql);
    }

    public function setupAllAdminReceiveMails()
    {
        $sql = "ALTER TABLE `companies` ADD `notification` "
            . "INT(1) NOT NULL DEFAULT '1' AFTER `analytics`;";
        $this->query($sql);
    }

    public function fixBountyDueDate()
    {
        $sql = $this->select()
            ->from(['b' => 'bounties'],['*']);
        $bounties = $this->fetchAll($sql);
        foreach($bounties as $index => $value)
        {
            if($value['due_date'] == "0000-00-00 00:00:00")
            {
                $id_bounty = $value['id'] ;
                $id_job = $value['id_job'] ;
                $id_user = $value['id_user'] ;
                $jobData = $this->getJobById($id_job);
                $job_ad_lifetime = $jobData['job_ad_lifetime'];
                $job_application = $this->getJobApplicationByIdJobIdUser($id_job, $id_user);
                if(empty($job_application)) continue ;
                $bounty_due_date = date("Y-m-d H:i:s", strtotime("{$job_application['updated']} + $job_ad_lifetime days"));
                echo 'application updated :'.$job_application['updated']. PHP_EOL;
                echo 'job_ad_lifetime :'.$job_ad_lifetime. PHP_EOL;
                echo 'bounty due date :'.$bounty_due_date. PHP_EOL;
                $this->updateBountyDateById($id_bounty,$bounty_due_date);

            }
        }

    }

    public function getJobById($id_job)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'], ['*'])
            ->where('j.id =?',(int) $id_job);
        return $this->fetchRow($sql);
    }
    public function getJobApplicationByIdJobIdUser($id_job,$id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'])
            ->where('ja.id_job =?',(int) $id_job)
            ->where('ja.id_recommender =?',(int) $id_user)
            ->where('ja.status =?',(int) 4)
            ->where('ja.status_reason =?',(int) 5);
        return $this->fetchRow($sql);
    }
    public function updateBountyDateById($id,$due_date)
    {
        $this->update(['due_date' => $due_date],"id = $id", 'bounties');
    }
    public function indexing()
    {
        $sql = [];
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`id_job`);";
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`date`);";
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`id_recommender`);";
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`id_user`);";
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`status`);";
        $sql[] = "ALTER TABLE `job_applications` ADD INDEX(`status_reason`);";
        $sql[] = "ALTER TABLE `campaign_actions_detailed` ADD INDEX(`id_campaign`, `id_recommender`, `id_user`, `id_job`, `action`, `value`);";
        $sql[] = "ALTER TABLE `job_views` ADD INDEX(`id_job`, `id_user`, `source`, `fingerprint`);";
        $sql[] = "ALTER TABLE `job_recommendations` ADD INDEX(`id_recommender`, `id_user`, `id_job`, `email`, `track`, `date`);";
        $sql[] = "ALTER TABLE `jobs_share` ADD INDEX(`id_user`, `id_job`, `shares`, `views`, `date`);";
        $sql[] = "ALTER TABLE `gamification_points` ADD INDEX(`id_user`, `id_item`, `action`, `date`);";
        foreach ($sql as $sql)
        {
            $this->query($sql);
        }
    }
    public function testReminderJob()
    {
        $CronModel = new models_Cron;
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
        $smarty = new Smarty;
        $sitename = $config->resources->Settings->sitename;

        $transport = null;
        $config = array('auth' => "login", 'username' => "hello@workinafrica.com", 'password' => "Sda09i2iRHz77tHgDv561Q");
        $transport = new Zend_Mail_Transport_Smtp("smtp.mandrillapp.com", $config);
        $result = $CronModel->reminder_7_Jobs();
//        echo '<pre>';
//        print_r($result);
//        die;

        foreach ($result as $key => $value)
        {
            $users = $value['users'];
//            $users = [
//                ['firstname' => 'Seifeddine',
//                'lastname' => 'Jeddey',
//                'user_email' => 'jeddey.seifeddine@gmail.com']
//            ];

            $jobs = $value['jobs'];

            foreach ($jobs as $key => $job)
            {
                $jobs[$key]['url'] = WebApp_Tools::createJobSlug($job);
            }

//    echo '<pre>';print_r($jobs);die; 

            $view = new Zend_View();
            $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
            $partialHelper = new Zend_View_Helper_Partial();
            $partialHelper->setView($view);
            $table = $partialHelper->partial('reminder_jobs.phtml', ['data' => $jobs]);



//                        echo '<pre>';
//            print_r($users);
//            die;
            foreach ($users as $index => $user)
            {
                $userName = $user['firstname'];
                $userLastName = $user['lastname'];
                $userEmail = 'jeddey.seifeddine@gmail.com';
//                $userEmail = $user['user_email'];

                $contentModel = new models_Content;
                $email = $contentModel->getEmailByType(44);
                $smarty = new Smarty;
                $smarty->assign("username", $userName);
                $smarty->assign("jobs", $jobs);
                $smarty->assign("table", $table);
                $smarty->assign("settings", $config->resources->Settings);
                $smarty->assign("settings", ['sitename' => $sitename]);
                $smarty->assign("company", $result['company']['title_company']);
                $sub = 'What\'s new in {$company} jobs - open for referral on Talentspotting';
                $subject = $smarty->fetch('eval:' . $result['company']['email_subject']);
                $resource = $smarty->fetch('eval:' . $result['company']['email_content']);

                $mail = new WebApp_Mail();
                $mail->setFrom($result['company']['email_company'], $sitename);
                $mail->setSubject($subject);
                $mail->addTo($userEmail, $companyEmail);
                $mail->setBodyHtml($resource);
                if (filter_var($userEmail, FILTER_VALIDATE_EMAIL))
                {
                    $mail->send($transport);
                    die;
                } else {
                    continue;
                }
            }
        }
    }
    public function removeDuplicatedIndexing()
    {
        $sql = [];
        $sql[] = "ALTER TABLE job_applications DROP INDEX date_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX date_3;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_job_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_job_3;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_recommender_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_recommender_3;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_user_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX id_user_3;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX status_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX status_3;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX status_reason_2;";
        $sql[] = "ALTER TABLE job_applications DROP INDEX status_reason_3;";
        $sql[] = "ALTER TABLE campaign_actions_detailed DROP INDEX id_campaign_2;";
        $sql[] = "ALTER TABLE campaign_actions_detailed DROP INDEX id_campaign_3;";
        $sql[] = "ALTER TABLE job_views DROP INDEX id_job_2;";
        $sql[] = "ALTER TABLE job_views DROP INDEX id_job_3;";
        $sql[] = "ALTER TABLE job_recommendations DROP INDEX id_recommender_2;";
        $sql[] = "ALTER TABLE job_recommendations DROP INDEX id_recommender_3;";
        $sql[] = "ALTER TABLE jobs_share DROP INDEX id_user_2;";
        $sql[] = "ALTER TABLE gamification_points DROP INDEX id_user_2;";

        foreach ($sql as $sql)
        {
            $this->query($sql);
        }
    }

    public function updateRecommenderStatsuInCampagnBrand()
    {
        $content = <<<'HERE'
                
<!DOCTYPE html>
<html>
<head>
<title>Welcome to Talentspotting! Here's what to expect</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

    /* RESET STYLES */
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */

</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Thanks for choosing us as your referral tool
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://talentspotting.com/resources/frontend/_landing/images/logo2.png" height="200" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">See your position in current campaigns and earn more points</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p style="margin: 0;">
                     Hi {$username},</p>
                  <br>

                  <p>Ta-dam!! Thought you'd like to know your position in current Talentspotting campaigns and how you can 
boost your chances of recommending quality candidates and earning superrewards! </p> 

                {$table}

                     

                 <p><br>Best of luck,<br>{$settings.sitename}</p>
                </td>
              </tr>

               <tr><td bgcolor="#ffffff" align="center" style="padding-bottom:50px">
                    <a href="{$settings.sitename}/login" style="background-color: #7dcc93; max-width: 100px; margin: 0 auto; padding: 10px 20px; border-radius: 3px; color: white; text-decoration: none; text-decoration-color: #7dcc93; font-family: 'Open Sans', Arial;">Sign in</a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://dev.talentspotting.com/resources/files/images/facebook-logo-button.png"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://dev.talentspotting.com/resources/files/images/linkedin-logo-button.png
"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://dev.talentspotting.com/resources/files/images/house.png"></a><br>
            <br>
            <a href="http://www.totalsoft.ro" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2017</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            
            </center></body>
</html>

HERE;
        $sql = $this->select()->from(['e' => 'emails'],['*'])
            ->where('e.type =?',(int) 38);
        $row = $this->fetchRow($sql);
        if(!empty($row)) $this->update(['content' => $content],'id_email='.$row['id'],'emails_lang' );

    }

    public function updateErrorTextInEmailsSubject($error = null, $text = null)
    {
        $error = <<<HERE
    $error
HERE;
        $text = <<<HERE
    $text
HERE;

        if ($error == null || $text == null)
        {
            throw new Exception('params not setted ');
        }
        $model_company = new models_Company;
        $companies = $model_company->getCompanies();

        foreach ($companies as $key => &$value)
        {
            $companies[$key]['emails_template'] = $this->getEmailsTemplateByCompanyId($value['id']);
            $mails_template = $this->getEmailsTemplateByCompanyId($value['id']);
            foreach ($mails_template as $key => $email)
            {
                $content = $email['subject'];
                $corrected_content = str_replace($error, $text, $content);

                $this->updateEmailContentFromErrors(['subject' => $corrected_content], $email['id_emails_lang']);
            }
        }
    }

    public function RemoveOldEmails()
    {
        $sql = $this->select()
            ->from(['e' => 'emails'])
            ->where('e.type = 45');
        $emails = $this->fetchRow($sql);
        if(!empty($emails))
        {
            $id = $emails['id'];
            $this->delete("id = $id", 'emails');
            $this->delete("id_email = $id", 'emails_lang');
        }

        $sql = $this->select()
            ->from(['e' => 'emails'])
            ->where('e.type = 34');
        $emails = $this->fetchRow($sql);
        if(!empty($emails))
        {
            $id = $emails['id'];
            $this->delete("id = $id", 'emails');
            $this->delete("id_email = $id", 'emails_lang');
        }

        $sql = $this->select()
            ->from(['e' => 'emails'])
            ->where('e.type = 36');
        $emails = $this->fetchRow($sql);
        if(!empty($emails))
        {
            $id = $emails['id'];
            $this->delete("id = $id", 'emails');
            $this->delete("id_email = $id", 'emails_lang');
        }


    }

    public function ChangeDirection()
    {
        $this->update(['direction' => 1],'type=39','emails');
        $this->update(['direction' => 2],'type=37','emails');
    }

    public function updateSubiectByEmailType($type,$subject= null)
    {
        $model_company = new models_Company;
        $companies = $model_company->getCompanies();
        foreach ($companies as $key => &$value)
        {
            $companies[$key]['emails_template'] = $this->getEmailsTemplateByTypeCompanyId($value['id'],$type);
            $mails_template = $this->getEmailsTemplateByTypeCompanyId($value['id'],$type);
            foreach ($mails_template as $key => $email)
            {
                $this->updateEmailContentFromErrors(['subject' => $subject], $email['id_emails_lang']);
            }
        }
    }

    public function activejob($sql)
    {
        $this->query($sql);
    }
    public function createLinkedinTableAutoPost()
    {
        $query[] = <<<'HERE'
            CREATE TABLE if not exists `autopost_linkedin_job` (
           `id` int(11) NOT NULL,
           `id_user` int(11) NOT NULL,
           `id_job` int(11) NOT NULL,
           `date` datetime NOT NULL
         ) ENGINE=InnoDB DEFAULT CHARSET=latin1;         

HERE;
        $query[] = "ALTER TABLE `autopost_linkedin_job` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_job` (`id_job`);";
        $query[] = "ALTER TABLE `autopost_linkedin_job` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;";
        foreach ($query as $q)
        {
            $this->query($q);
        }

    }
    public function DropUniqueKeys()
    {
        $sql[] = "alter table `autopost_job` drop index id_job;";
        $sql[] = "alter table `autopost_linkedin_job` drop index id_job;";
        foreach ($sql as $q)
        {
            $this->query($q);
        }
    }

    public function addNotRatedColumn()
    {
        $sql = "ALTER TABLE `notification` ADD `ignore_rate` INT(1) NOT NULL DEFAULT '0' AFTER `technical_skills`;";
        $this->query($sql);
    }
    public function addStatustoDb()
    {
        $query[] = <<<'HERE'
        CREATE TABLE IF NOT EXISTS `status` (
            `id` int(10) NOT NULL,
            `status` varchar(250) NOT NULL,
            `slug` varchar(250) NOT NULL,
            `date` datetime NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
HERE;
        $query[] = <<<'HERE'
                INSERT INTO `status` (`id`, `status`, `slug`, `date`) VALUES
                (1, 'New', 'new', '2018-02-13 00:00:00'),
                (2, 'Viewed', 'viewed', '2018-02-13 00:00:00'),
                (3, 'Processing', 'processing', '2018-02-13 00:00:00'),
                (4, 'Moved to another process', 'moved_to_another_process', '2018-02-13 00:00:00'),
                (5, 'hired', 'Hired', '2018-02-13 00:00:00'),
                (6, 'Unfit candidate', 'unfit_candidate', '2018-02-13 00:00:00'),
                (7, 'Interview', 'interview', '2018-02-13 00:00:00'),
                (8, 'tested', 'Tested', '2018-02-13 00:00:00'),
                (9, 'Offer rejected', 'offer_rejected', '2018-02-13 00:00:00'),
                (10, 'no_answer', 'No answer', '2018-02-13 00:00:00'),
                (11, 'Dropped out', 'dropped_out', '2018-02-13 00:00:00'),
                (12, 'No show', 'no_show', '2018-02-13 00:00:00'),
                (13, 'Banned', 'banned', '2018-02-13 00:00:00'),
                (14, 'Final interview', 'final_interview', '2018-02-13 00:00:00');
HERE;
        $query[] = 'ALTER TABLE `status` ADD PRIMARY KEY (`id`);';
        $query[] = 'ALTER TABLE `status` MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;';
        foreach($query as $q) $this->query($q);

    }
    public function changeStatusInOldDbToNew()
    {
        $sql = $this->select()->from(['ja' => 'job_applications']);
        $data = $this->fetchAll($sql);

        foreach($data as $key => $value) {

            if ($value['status'] == 0 && $value['status_reason'] == 0) $status = 1;
            if ($value['status'] == 1 && $value['status_reason'] == 0) $status = 2;
            if ($value['status'] == 4 && $value['status_reason'] == 9) $status = 3;
            if ($value['status'] == 3 && $value['status_reason'] == 0) $status = 4;
            if ($value['status'] == 2 && $value['status_reason'] == 1) $status = 6;
            if ($value['status'] == 4 && $value['status_reason'] == 5) $status = 5;
            if ($value['status'] == 4 && $value['status_reason'] == 0) $status = 7;
            if ($value['status'] == 4 && $value['status_reason'] == 2) $status = 8;
            if ($value['status'] == 4 && $value['status_reason'] == 6) $status = 9;
            if ($value['status'] == 4 && $value['status_reason'] == 7) $status = 10;
            if ($value['status'] == 4 && $value['status_reason'] == 8) $status = 11;
            if ($value['status'] == 5 && $value['status_reason'] == 1) $status = 12;
            if ($value['status'] == 5 && $value['status_reason'] == 2) $status = 13;
            if ($value['status'] == 5 && $value['status_reason'] == 3) $status = 14;

            $this->update(['status' => $status], "id={$value['id']}",'job_applications');

        }
    }
    public function setRequiredProfileCompleted()
    {
        $sql = "ALTER TABLE `companies` ADD `completed_profile` INT(1) NOT NULL DEFAULT '1' AFTER `is_pilote`";
        $this->query($sql);
    }
    public function removeCompletedProfile()
    {
        $this->delete('id=5','gamifications_actions');
    }
    public function changeAcceptedStatusToHired()
    {
        $this->update([
            'Status' => 'hired',
            'slug'   => 'Hired'

        ],'id=5','status');
    }

    public function addNewConfig()
    {
        $this->query("ALTER TABLE `companies` ADD `upload_cv` INT(1) NOT NULL DEFAULT '1' AFTER `notification`;");
        $this->query("ALTER TABLE `companies` ADD `import_linkedin` INT(1) NOT NULL DEFAULT '1' AFTER `upload_cv`;");
        $this->query("ALTER TABLE `companies` ADD `job_departments` INT(1) NOT NULL DEFAULT '1' AFTER `import_linkedin`;");
        $this->query("ALTER TABLE `companies` ADD `job_skills` INT(1) NOT NULL DEFAULT '1' AFTER `job_departments`;");
    }

    public function addHeatmapField()
    {
        $this->query("ALTER TABLE `companies` ADD `heatmap` TEXT NULL DEFAULT NULL AFTER `analytics`;");
    }

    public function addFooterField()
    {
        $this->query("ALTER TABLE `companies` ADD `footer` TEXT NULL DEFAULT NULL AFTER `heatmap`;");
    }

    public function addFooterContent()
    {
        $content = <<<'HERE'
        <div class="container">
            <p>© Talentspotting 2017. All rights reserved</p>
            <a class="tms-cls" href="/terms-condition" data-target="#myModal" data-toggle="modal">Terms and Conditions</a>
        </div>
HERE;
        $sql = $this->select()
            ->from(['c' => 'companies'],['*']);
        $result = $this->fetchAll($sql);

        foreach ($result as $key => $item) {
            $this->update(['footer' => $content] ,"id=".$item['id'],'companies');
        }
    }

    public function registerTextField()
    {
        $this->query("ALTER TABLE `companies` ADD `register_text` TEXT NULL DEFAULT NULL AFTER `footer`;");
    }

    public function addUserToPlatform()
    {
        $users = [
            ['email' => 'masteradmin@talentspotting.com','type' => 3,'status' => 1 ,
                'firstname' => 'Master' , 'lastname' => 'Admin',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
            ['email' => 'recruiter@talentspotting.com','type' => 4,'status' => 1 ,
                'firstname' => 'Internal' , 'lastname' => 'Recruiter',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
            ['email' => 'manager@talentspotting.com','type' => 5,'status' => 1 ,
                'firstname' => 'Hiring' , 'lastname' => 'Manager',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
            ['email' => 'recommender@talentspotting.com','type' => 1,'status' => 1 ,
                'firstname' => 'Internal' , 'lastname' => 'Recommender',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
            ['email' => 'admin@talentspotting.com','type' => 2,'status' => 1 ,
                'firstname' => 'Admin' , 'lastname' => 'Company',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
            ['email' => 'candidat@talentspotting.com','type' => 9,'status' => 1 ,
                'firstname' => 'Internal' , 'lastname' => 'Candidate',
                'password' => '664819d8c5343676c9225b5ed00a5cdc6f3a1ff3123',
                'salt' =>'123'],
        ];
        $sql = $this->select()
            ->from(['c' => 'companies'],['*']);
        $companies = $this->fetchAll($sql);
        $model = new models_User ;
        if(!empty($companies))
        {
            foreach($companies as $key => $company)
            {
                $this->delete("id_company = {$company['id']} AND type=3",'users');
                foreach($users as $k => $user)
                {
                    $to_remove = $model->getUserDataByEmail($user['email']);
                    if(!empty($to_remove)) $this->delete("id = {$to_remove['id']}",'users');
                    $user['id_company'] = $company['id'];
                    $this->insert($user,'users');
                }
            }
        }

    }
    public function setViewdRecommendation()
    {
        $query = "ALTER TABLE `job_recommendations` ADD `viewed` INT(1) NOT NULL DEFAULT '0' AFTER `email`;";
        $this->query($query);

    }

    public function addSigninField()
    {
        $query = "ALTER TABLE `companies` ADD `signin_text` TEXT NOT NULL AFTER `register_text`;";
        $this->query($query);

    }
    public function updateSigninText()
    {
        $sql = $this->select()
            ->from(['c' => 'companies'],['id']);
        $c = $this->fetchAll($sql);
        $str = "Register for a Talentspotting account to apply to awesome new jobs";
        foreach ($c as $key => $value)
        {
            $this->update(['signin_text' => $str],"id={$value['id']}",'companies');
        }
    }

//    public function Changerequired()
//    {
//        $sql = [];
//        $sql[] = "ALTER TABLE `cities` CHANGE `id_country` `id_county` INT(11) NOT NULL DEFAULT '0';";
//        $sql[] = "CREATE TABLE `counties` (
//  `id` int(11) NOT NULL,
//  `id_country` int(11) NOT NULL,
//  `title` varchar(200) NOT NULL,
//  `slug` varchar(200) NOT NULL
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
//        $sql[] = "ALTER TABLE `counties` ADD PRIMARY KEY (`id`);";
//        $sql[] = "ALTER TABLE `counties`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;";
//
//        foreach($sql as $q)
//        {
//
//        }
//    }

    public function addJobImageField()
    {
        $sql = "ALTER TABLE `jobs` ADD `job_public_image` VARCHAR(200) NULL DEFAULT NULL AFTER `bounty_file`;";
        $this->query($sql);
    }

    public function userTable()
    {

        $query[] = <<<'HERE'
                    CREATE TABLE `user_languages` (
                `id` int(11) NOT NULL,
              `id_user` int(11) NOT NULL,
              `language` varchar(250) NOT NULL,
              `date` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;       

HERE;
        $sql[] = "ALTER TABLE `user_languages` ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `user_languages` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";

        $sql[] = "ALTER TABLE `users` ADD `lastjob_lifetime` VARCHAR(30) NULL DEFAULT NULL AFTER `billing_company`;";
        foreach($sql as $q)
        {
            $this->query($q);
        }


    }

    public function updateStatus()
    {
        $sql = $this->select()
            ->from(['s' => 'status'],array('*'));
        $status = $this->fetchAll($sql);
        foreach($status as $key => $status)
        {
            $data = array();
            $data['slug'] = str_replace('','_',strtolower($status['slug']));
            $this->update($data,"id={$status['id']}",'status');

            if($status['slug'] == "tested")
            {
                $this->update(['status' => 'Test','slug'=>'test'],"id={$status['id']}",'status');
            }
        }

        $data['status'] = "Not interested";
        $data['slug'] = "not_interested";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "End";
        $data['slug'] = "end";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "Background Check";
        $data['slug'] = "background_check";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "Interview";
        $data['slug'] = "interview";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "Assesed";
        $data['slug'] = "assesed";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "Test Planned";
        $data['slug'] = "test_planned";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');

        $data['status'] = "Interview Planed";
        $data['slug'] = "interview_planed";
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'status');
    }

    public function deleteStatus()
    {
        $sql = $this->select()
            ->from(['s' => 'status'],array('*'));
        $status = $this->fetchAll($sql);
        foreach($status as $key => $status)
        {
            if($status['slug'] == "dropped_out")
            {
                $this->delete("id={$status['id']}",'status');
            }

            if($status['slug'] == "offered")
            {
                $this->delete("id={$status['id']}",'status');
            }

            if($status['slug'] == "processing")
            {
                $this->delete("id={$status['id']}",'status');
            }
        }
    }

    public function addActiveToStatus()
    {
        $sql = "ALTER TABLE `status` ADD `active` INT(1) NOT NULL DEFAULT '1' AFTER `slug`;";
        $this->query($sql);
    }

    public function createSocialMediaTable()
    {
        $query[] = <<<'HERE'
                CREATE TABLE `social_media` (
                  `id` int(11) NOT NULL,
                  `title` varchar(100) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;      

HERE;
        $query [] = "ALTER TABLE `social_media`   ADD PRIMARY KEY (`id`);";
        $query [] = "ALTER TABLE `social_media` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        foreach($query as $q)
        {
            $this->query($q);
        }

    }

    public function updateSourceColumn()
    {
        $sql = "ALTER TABLE `users` CHANGE `source` `source` INT(1) NULL DEFAULT '0';";
        $this->query($sql);
    }

    public function updateUserSource()
    {
        $sql = "UPDATE `users` SET `source` = '0'";
        $this->query($sql);
    }

    public function updt()
    {
        $this->query("ALTER TABLE `job_applications` CHANGE `source` `source` INT(1) NOT NULL DEFAULT '0';");
    }

    public function oldSourcesInTables()
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['*']);
        $applications = $this->fetchAll($sql);
        $this->updt();
        foreach($applications as $index => $application)
        {
            $old_source = $application['source'];
            if($old_source  != "")
            {
                $sql = $this->select()->from(['sm' => 'social_media'], ['*'])
                    ->where('sm.title =?', $old_source);
                $result = $this->fetchRow($sql);
                if(!empty($result)) {
                    $new_source = $result['id'];
                    $this->update(['source' => $new_source], "id={$application['id']}", "job_applications");
                }
            }
        }

        $sql = $this->select()
            ->from(['jv' => 'job_views'],['*']);
        $job_views = $this->fetchAll($sql);
        foreach($job_views as $index => $view)
        {
            $old_source = $view['source'];
            if($old_source != "")
            {
                $sql = $this->select()->from(['sm' => 'social_media'], ['*'])
                    ->where('sm.title =?', $old_source);
                $result = $this->fetchRow($sql);
                if(!empty($result))
                {
                    $new_source = $result['id'];
                    $this->update(['source' => $new_source], "id={$view['id']}", "job_views");
                }
            }
        }

    }

    public function setupRecEmailForLogin()
    {
        $sql = $this->select()
            ->from(['c' => 'companies'],['*']);
        $comapnies = $this->fetchAll($sql);

        foreach($comapnies as $i => $c)
        {
            $sql = $this->select()
                ->from(['e' => 'emails'],['*'])->where('e.type = 63');
            $result = $this->fetchRow($sql);
            if(!empty($result)) continue;

            $data['id_company'] = $c['id'];
            $data['type'] = 63;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $c['email'];
            $data['from'] = $c['title'];

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "apply";
                $subject = <<<'HERE'
                        Join us on {$sitename}
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hello {$user.firstname} <br>
        	<br>
        	<br>
      		  <p>Activate your account in {$sitename}  and SHARE {$company} jobs in your network. </p> 
                  <p>You already received username & password from us, but if you can't find them you can always reset your password 
                  <a href="{$sitename}/login">here</a>.</p>
                   <p>  Choose friends to spend your working time and win bounties if your referred friends get hired! </p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
                $_data['content'] = $content;

                $this->insert($_data, 'emails_lang');
            }
        }
    }

    public function createRecommenderTable()
    {
        $sql[] = "CREATE TABLE `recommender_reminder` (
  `id` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";
        $sql[] = "ALTER TABLE `recommender_reminder`
  ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `recommender_reminder`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;";
        foreach($sql as $q)
        {
            $this->query($q);
        }
    }

    public function addFieldToJobApplication()
    {
        $q = "ALTER TABLE `job_applications` ADD `removed` INT(1) NOT NULL DEFAULT '0' AFTER `source`;";
        $this->query($q);
    }

    public function createLogTableForEmail()
    {
        $sql = [];
        $sql[] = "CREATE TABLE `emails_log` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email_user` varchar(250) NOT NULL,
  `email_type` int(11) NOT NULL,
  `cron_file` varchar(250) NOT NULL,
  `conditions` varchar(250) NOT NULL,
  `subject` text,
  `content` text,
  `send` int(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $sql[] = "ALTER TABLE `emails_log`
  ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `emails_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        foreach($sql as $q)
        {
            $this->query($q);
        }

    }

    public function fixEmailLogTable()
    {
        $this->query("ALTER TABLE `emails_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
    }

    public function installBrandingTable()
    {
        $query[] = <<<'HERE'
CREATE TABLE `branding` (
  `id` int(2) NOT NULL,
  `class_name` varchar(57) DEFAULT NULL,
  `value` varchar(7) DEFAULT NULL,
  `style` varchar(200) NOT NULL,
  `id_category` int(4) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
HERE;

    $query[] = <<<'HERE'
INSERT INTO `branding` (`id`, `class_name`, `value`, `style`, `id_category`, `name`) VALUES
(24, 'branding-navbar-text > .clearfix > ul > li > ul > li > a', '#3ac5d9', 'color', 1, 'Dropdown Items Text'),
(23, 'branding-navbar-background-color > .clearfix > ul > li > ', '#ffffff', 'background-color', 1, 'Dropdown Items Background'),
(13, 'branding-left-menu > ul > li > a:focus ', '#30677b', 'background-color', 2, 'Background focus'),
(12, 'branding-left-menu > ul > li > a:hover ', '#30677b', 'background-color', 2, 'Background hover'),
(9, 'branding-left-menu > ul > li > a', '#aeb2b7', 'color', 2, 'Text'),
(7, 'branding-brand-button-background-color:focus', '#2fb0c2', 'background-color', 3, 'Background focus'),
(6, 'branding-brand-button-text:focus', '#ffffff', 'color', 3, 'Text focus'),
(5, 'branding-brand-button-background-color:hover', '#2fb0c2', 'background-color', 3, 'Background hover'),
(4, 'branding-brand-button-text:hover', '#ffffff', 'color', 3, 'Text hover'),
(3, 'branding-brand-button-background-color', '#7dcc93', 'background-color', 3, 'Background'),
(22, 'branding-navbar-background-color > .clearfix > ul > li > ', '#3ac5d9', 'background-color', 1, 'Dropdown Background'),
(29, 'branding-navbar-text:focus', '#ffffff', 'color', 1, 'Navbar Buttons Text Focus'),
(21, 'branding-navbar-background-color > .clearfix > ul > li:fo', '#3ac5d9', 'background-color', 1, 'Navbar Buttons Background Focus'),
(20, 'branding-navbar-text:hover', '#ffffff', 'color', 1, 'Navbar Buttons Text Hover'),
(18, 'branding-navbar-background-color > .clearfix > ul > li:ho', '#3ac5d9', 'background-color', 1, 'Navbar Buttons Background Hover'),
(17, 'branding-navbar-text', '#ffffff', 'color', 1, 'Navbar Buttons Text'),
(16, 'branding-navbar-background-color > .clearfix > ul > li', '#00c9dd', 'background-color', 1, 'Navbar Buttons Background'),
(15, 'branding-navbar-text', '#ffffff', 'color', 1, 'Text'),
(14, 'branding-navbar-background-color', '#886ca6', 'background-color', 1, 'Background'),
(11, 'branding-left-menu > ul > li > a:focus', '#ffffff', 'color', 2, 'Text focus'),
(10, 'branding-left-menu > ul > li > a:hover', '#ffffff', 'color', 2, 'Text hover'),
(8, 'branding-left-menu', '#5f9fff', 'background-color', 2, 'Background'),
(2, 'branding-brand-button-text', '#ffffff', 'color', 3, 'Text'),
(1, 'branding-brand-button-border-color', '#7dcc93', 'border-color', 3, 'Border'),
(25, 'branding-navbar-background-color > .clearfix > ul > li > ', '#ffffff', 'background-color', 1, 'Dropdown Items Background Hover'),
(26, 'branding-navbar-text > .clearfix > ul > li > ul > li > a:', '#ffffff', 'color', 1, 'Dropdown Items Text Hover'),
(27, 'branding-table-header-top', '#2fb0c2', 'background-color', 4, 'Background'),
(28, 'branding-table-header-top > span branding-table-header-to', '#ffffff', 'color', 4, 'Text'),
(30, 'branding-navbar-background-color > .clearfix > ul > li > ', '#2fb0c2', 'background-color', 4, 'background-color');  
HERE;

    $query[] = <<<'HERE'
          ALTER TABLE `branding`
          ADD UNIQUE KEY `id` (`id`);
HERE;
  $query[] = <<<'HERE'
    ALTER TABLE `branding`
      MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
HERE;
  foreach($query as $q)
  {
      $this->query($q);
  }
    }

    public function installBrandingCategory()
    {
        $query[] = <<<'HERE'
CREATE TABLE `branding_category` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

HERE;
        $query[] = <<<'HERE'
INSERT INTO `branding_category` (`id`, `title`, `description`) VALUES
(1, 'Top (navigation) menu', 'Choose your favourite brand colors for the main top-menu element (navbar).\r\n'),
(2, 'Side menu', 'Choose your favourite brand colors for the left side menu element.\r\n'),
(3, 'Buttons', 'Choose your favourite brand colors for most important buttons.\r\n'),
(4, 'Table Headers', 'Choose your favourite brand colors for table headers.\r\n');
HERE;
        $query[] = <<<'HERE'
ALTER TABLE `branding_category`
  ADD PRIMARY KEY (`id`);

HERE;

        $query[] = <<<'HERE'
ALTER TABLE `branding_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

HERE;
        foreach($query as $q)
        {
            $this->query($q);
        }
    }

    public function addStatusOptions()
    {
        $query[] = <<<'HERE'
CREATE TABLE `status_options` (
  `id` int(10) NOT NULL,
  `id_status` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `public_title` text CHARACTER SET utf8 NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `sla` int(4) NOT NULL DEFAULT '48',
  `color` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


HERE;
        $query[] = <<<'HERE'

INSERT INTO `status_options` (`id`, `id_status`, `status`, `slug`, `public_title`, `active`, `date`, `sla`, `color`) VALUES
(1, 2, 'To be called', 'to_be_called', 'Viewed', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(2, 2, 'Unfit', 'unfit', 'Viewed', 1, '0000-00-00 00:00:00', 48, '#666666'),
(3, 6, 'Not interested', 'not_interested', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(4, 6, 'Unfit', 'unfit', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(5, 6, 'No answer', 'no_answer', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(6, 6, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(7, 6, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(8, 8, 'No show', 'no_show', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(9, 8, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(10, 8, 'Not passed', 'not_passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(11, 8, 'Not interested', 'not_interested', 'Process closed', 1, '0000-00-00 00:00:00', 48, '#666666'),
(12, 8, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(13, 19, 'No show', 'no_show', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(14, 19, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(15, 19, 'Not passed', 'not_passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(16, 19, 'Not interested', 'not_interested', 'Process closed', 1, '0000-00-00 00:00:00', 48, '#666666'),
(17, 19, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(18, 7, 'No show', 'no_show', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(19, 7, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(20, 7, 'Not passed', 'not_passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(21, 7, 'Not interested', 'not_interested', 'Process closed', 1, '0000-00-00 00:00:00', 48, '#666666'),
(22, 7, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(23, 17, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(24, 17, 'Not passed', 'not_passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(25, 14, 'No show', 'no_show', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(26, 14, 'Passed', 'passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(27, 14, 'Not passed', 'not_passed', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(28, 14, 'Not interested', 'not_interested', 'Process closed', 1, '0000-00-00 00:00:00', 48, '#666666'),
(29, 14, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(30, 9, 'No answer', 'no_answer', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666'),
(31, 9, 'Offer accepted', 'offer_accepted', 'In process', 1, '0000-00-00 00:00:00', 48, '#00b489'),
(32, 9, 'Offer rejected', 'offer_rejected', 'In process', 1, '0000-00-00 00:00:00', 48, '#e06666'),
(33, 9, 'Moved to another process', 'moved_to_another_process', 'In process', 1, '0000-00-00 00:00:00', 48, '#666666');
HERE;
        $query[] = <<<'HERE'
ALTER TABLE `status_options`
  ADD PRIMARY KEY (`id`);


HERE;
        $query[] = <<<'HERE'
ALTER TABLE `status_options`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

HERE;
        foreach($query as $q)
        {
            $this->query($q);
        }
    }

    public function updateCompaniesDate()
    {
        $query[] = "ALTER TABLE `companies` ADD `launch_date` DATE NULL DEFAULT NULL AFTER `job_skills`;";
        $query[] = "ALTER TABLE `companies` ADD `launch_time` TIME NULL DEFAULT NULL AFTER `launch_date`;";
        foreach($query as $q)
        {
            $this->query($q);
        }
    }

    public function removeUnesesEmail()
    {
        $query = <<<'HERE'
delete from `emails` where type not in (1,
23,
25,
26,
27,
28,
30,
31,
32,
33,
37,
39,
40,
41,
42,
43,
44,
46,
47,
48,
49,
50,
52,
53,
54,
55,
58,
60,
62,
63);
HERE;
    $this->query($query);

    }


    public function createSkillsNewFields()
    {
        $sql[] = "ALTER TABLE `user_skills` ADD `skill_years` DECIMAL(4,1) NULL AFTER `id_linkedin`;";
        $sql[] = "ALTER TABLE `user_skills` ADD `rating` INT(2) NULL AFTER `skill_years`;";
        foreach($sql as $q)
        {
            $this->query($q);
        }
    }

    public function addGrossSalaryField()
    {
        $sql = "ALTER TABLE `user_positions` ADD `monthly_gross_pay_slip` INT(11) NULL AFTER `end`;";
        $this->query($sql);
    }

    public function addBiography()
    {
        $sql = "ALTER TABLE `users` ADD `biography` LONGTEXT NULL AFTER `job_type`;";
        $this->query($sql);
    }

    public function addRatingInUserLanguage()
    {
        $sql = "ALTER TABLE `user_languages` ADD `rating` INT(2) NULL AFTER `date`;";
        $this->query($sql);
    }

    public function addSocialToUser()
    {
        $sql = "ALTER TABLE `users` ADD `linkedin_url` VARCHAR(200) NULL AFTER `terms`, ADD `facebook_url` VARCHAR(200) NULL AFTER `linkedin_url`, ADD `twitter_url` VARCHAR(200) NOT NULL AFTER `facebook_url`;";
        $this->query($sql);
    }

    public function addJobVacancy()
    {
        $sql = "ALTER TABLE `job_cities` ADD `vacancies` INT NULL COMMENT 'number of required people to hire' AFTER `id_city`;";
        $this->query($sql);
    }

    public function createEmployReferrenceTable()
    {
        $query = "CREATE TABLE `user_references` ( `id` int(11) NOT NULL, `id_user` int(11) NOT NULL COMMENT 'candidate user', `referee_first_name` text NOT NULL COMMENT 'referee ', `referee_last_name` text NOT NULL COMMENT 'referee', `referee_email` varchar(200) NOT NULL, `referee_phone` varchar(15) NOT NULL, `referee_current_position` varchar(200) NULL, `referee_current_company` varchar(200) NULL, `collaboration_background` longtext NULL COMMENT 'when and where they worked together', `user_result_orientation` int(11) NULL COMMENT '1-5 stars', `user_collaborative_skills` int(11) NULL COMMENT '1-5 stars', `user_solution_finder` int(11) NULL COMMENT '1-5 stars', `user_team_work` int(11) NULL COMMENT '1-5 stars', `user_technical_skills` int(11) NULL COMMENT '1-5 stars', `user_reference_message` longtext NULL, `referee_accept_contact` tinyint(1) NOT NULL,`request_date` TIMESTAMP NOT NULL,`updated` TIMESTAMP NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='external references'";
        $this->query($query);
    }

    public function addLeavingDate()
    {
        $query = "ALTER TABLE `users` ADD `leaving_date` DATE NULL COMMENT 'date when employee leaves the company' AFTER `twitter_url`;";
        $this->query($query);
    }

    public function createRatingTable()
    {
        $query = "CREATE TABLE `rating` ( `id` INT NOT NULL AUTO_INCREMENT , `title` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->query($query);
    }

    public function populateRatingTabel()
    {
        $sql = "INSERT INTO `rating` (`id`, `title`) VALUES (1, 'Beginner'), (2, 'Intermediate'), (3, 'Advanced'), (4, 'Expert')";
        $this->query($sql);
    }

    public function updateNotificationTable()
    {
        $sql = "ALTER TABLE `notification` CHANGE `result_orientation` `result_orientation` TINYINT(1) NULL, CHANGE `culture_fit` `culture_fit` TINYINT(1) NULL, CHANGE `technical_skills` `technical_skills` TINYINT(1) NULL;";
        $this->query($sql);
    }

    public function updateUserSkillsUserPositions()
    {
        $sql[] = "ALTER TABLE `user_positions` CHANGE `end` `end` DATE NULL DEFAULT NULL;";
        foreach($sql as $q)
        {
            $this->query($q);
        }

    }

    public function createSubDEpartemntsTable()
    {
        $sql[] = "CREATE TABLE `sub_departments` 
( `id` INT NOT NULL ,  
`id_department` INT NOT NULL ,  
`title` VARCHAR(300) NOT NULL ,  
`slug` VARCHAR(300) NOT NULL ) 
ENGINE = InnoDB;";
        $sql[] = "ALTER TABLE `sub_departments` ADD INDEX(`id`);";
        $sql[] = "ALTER TABLE `sub_departments` ADD PRIMARY KEY(`id`);";
        foreach ($sql as $q) {
            $this->query($q);
        }
    }

    public function createSubSubDEpartemntsTable()
    {
        $sql = "CREATE TABLE `sub_sub_departments` ( `id` INT NOT NULL , `id_sub_department` INT NOT NULL ,  `title` VARCHAR(300) NOT NULL ,  `slug` VARCHAR(300) NOT NULL ) ENGINE = InnoDB; ";
        $this->query($sql);
    }

    public function createSubDepartmentJob()
    {
        $sql ="CREATE TABLE `job_sub_departments` (
  `id` int(11) NOT NULL,
  `id_job` int(11) NOT NULL,
  `id_sub_department` int(11) NOT NULL
) ENGINE=InnoDB ;
";
        $this->query($sql);
    }

    public function createSubSubDepartmentJob()
    {
        $sql ="CREATE TABLE `job_sub_sub_departments` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `id_job` INT(11) NOT NULL , `id_sub_sub_department` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->query($sql);
    }

    public function createProjectTable()
    {
        $sql = "CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB ;
";
        $this->query($sql);
    }

    public function createJobProjectTable()
    {
        $sql = "CREATE TABLE `job_projects` (
  `id` int(11) NOT NULL,
  `id_job` int(11) NOT NULL DEFAULT '0',
  `id_project` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
";  $this->query($sql);
    }

    public function installIobHeadCount()
    {
        $sql ="CREATE TABLE `job_headcount` (
  `id` int(11) NOT NULL,
  `id_job` int(11) NOT NULL,
  `headcount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";
        $this->query($sql);
    }

    public function createBounceFields()
    {
        $sql[] = "ALTER TABLE `users` ADD `bounced` INT(1) NOT NULL DEFAULT '0' AFTER `terms`;";
        $sql[] = "ALTER TABLE `users` ADD `bounce_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `bounced`;";
        foreach($sql as $q)
        {
        $this->query($q);
        }
    }

    public function addInstaTouser()
    {
        $sql = "ALTER TABLE `users` ADD `instagram_url` VARCHAR(200) NULL DEFAULT NULL AFTER `twitter_url`;";
        $this->query($sql);
    }

    public function reparseSwaggerJson()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production", $options);
        $public = APPLICATION_PATH.'/../public/';
        $swaggerFile = $public.'/swagger.json';
        $url = preg_replace("(^https?://)", "", $config->resources->Settings->sitename );
        $parsed_string = file_get_contents($swaggerFile);
        $parsed_string = str_replace("lh.talentspotting.com",$url,$parsed_string);
        $parsed_string = str_replace('["http","https"]','["https","http"]',$parsed_string);
        $fhandle = fopen($swaggerFile,"w+");
        fwrite($fhandle,$parsed_string);
        fclose($fhandle);
        echo $parsed_string;
    }


    public function addJobPeriode()
    {
        $sql = "ALTER TABLE `jobs` ADD `job_periode` INT(10) NOT NULL DEFAULT '0' AFTER `job_ad_lifetime`;";
        $this->query($sql);
    }

    public function addidJobTohrManager()
    {

        $sql = "ALTER TABLE `hr_jobs` ADD `id_job` INT(10) NOT NULL DEFAULT '0' AFTER `id_department`;";
        $this->query($sql);
    }

    public function addRole()
    {
        $sql = "ALTER TABLE `jobs_lang` ADD `role` LONGTEXT NOT NULL AFTER `responsabilities`;";
        $this->query($sql);
    }

    public function addBountyTitleField()
    {
        $sql = "ALTER TABLE `jobs` ADD `bounty_title` VARCHAR(150) NULL DEFAULT NULL AFTER `bounty_file`;";
        $this->query($sql);
    }

    public function installCurrenciesTable()
    {
        $sql = "CREATE TABLE `currencies` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `title` VARCHAR(50) NOT NULL , `code` VARCHAR(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->query($sql);
        $sql = "INSERT INTO `currencies` (`id`, `title`, `code`) VALUES (NULL, 'euro', 'EURO'), (NULL, 'dollars', 'USD'), (NULL, 'ron', 'RON');";
        $this->query($sql);
    }

    public function addBountyTypePayment()
    {
        $sql ="ALTER TABLE `jobs` ADD `bounty_payment_type` INT(2) NOT NULL DEFAULT '0' AFTER `bounty_file`;";
        $this->query($sql);
    }

    public function installPaymentTypeTable()
    {
        $sql[] = "CREATE TABLE `payment_type` (
  `id` int(10) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $sql[] = "ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);";
        $sql[] = "ALTER TABLE `payment_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;";
        foreach($sql as $q)
        {
            $this->query($sql);
        }
    }

    public function installTypeRows()
    {
        $sql = "INSERT INTO `payment_type` (`id`, `title`) VALUES (NULL, 'Gross Pay'), (NULL, 'Net Pay');";
        $this->query($sql);
    }

    public function installJobTypeTable()
    {
        $sql = "CREATE TABLE `job_types` ( `id` INT(2) NOT NULL AUTO_INCREMENT , `title` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
";
        $this->query($sql);
        $sql = "INSERT INTO `job_types` (`id`, `title`) VALUES (NULL, 'Part Time'), (NULL, 'Full Time');
";
        $this->query($sql);
    }

    public function installJobPeriodeTable()
    {
        $sql = "CREATE TABLE `test`.`job_periodes` ( `id` INT(2) NOT NULL AUTO_INCREMENT , `title` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->query($sql);
        $sql = "INSERT INTO `job_periodes` (`id`, `title`) VALUES (NULL, 'Permanent contract'), (NULL, 'Fixed-term contract'), (NULL, 'Project based contract');";
        $this->query($sql);

    }



    public function updateNewEmail()
    {
        $sql = $this->select()
            ->from(['c' => 'companies'], ['*']);
        $comapnies = $this->fetchAll($sql);

        foreach ($comapnies as $i => $c) {
            $sql = $this->select()
                ->from(['e' => 'emails'], ['*'])->where('e.type = 64');
            $result = $this->fetchRow($sql);
            if (!empty($result)) continue;

            $data['id_company'] = $c['id'];
            $data['type'] = 64;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $c['email'];
            $data['from'] = $c['title'];

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Welcome to Talentspotting";
                $subject = <<<'HERE'
                        Welcome to Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;margin-bottom:20px">
              <tr>
                <td><br /> Dear {$firstname},
                  <br /> &nbsp;
                  <p>Your {$role} account is ready for your first signin, using credentials:</p>
                  <ul>
                    <li>username: <b>{$email}</b></li>
                    <li>password: <b>{$password}</b></li>
                  </ul>

                  <p>This {$role} role is very important, being able to:</p>
                  <ol>
                    <li>manage platform look & feel</li>
                    <li>manage users & roles</li>
                    <li>manage & share jobs</li>
                    <li>manage campaigns</li>
                    <li>manage candidates</li>
                    <li>study complete reports and decide future actions</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="padding: 10px;">
                  <a class="button" style="background-color:{$bg_color}" href="{$sitename}" target="_blank">Setup your social referral tool</a>
                </td>
              </tr>
							<tr>
								<td>
									<p>Build Your Own Talent Stream!</p>
								</td>
							</tr>
            </table>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $c['id'];
            $data['type'] = 65;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $c['email'];
            $data['from'] = $c['title'];

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Welcome to Talentspotting";
                $subject = <<<'HERE'
                        Welcome to Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = <<<'HERE'
   <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;margin-bottom:20px">
              <tr>
                <td><br /> Dear {$firstname},
                  <br /> &nbsp;
                  <p>Your {$role} account is ready for your first signin, using credentials:</p>
                  <ul>
                    <li>username: <b>{$email}</b></li>
                    <li>password: <b>{$password}</b></li>
                  </ul>

                  <p>This {$role} role is very important, being able to:</p>
                  <ol>
                    <li>manage platform look & feel</li>
                    <li>manage users & roles</li>
                    <li>manage & share jobs</li>
                    <li>manage campaigns</li>
                    <li>manage candidates</li>
                    <li>study complete reports and decide future actions</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="padding: 10px;">
                  <a class="button" style="background-color:{$bg_color}" href="{$sitename}" target="_blank">Setup your social referral tool</a>
                </td>
              </tr>
              <tr>
                <td>
                  <p>Build Your Own Talent Stream!</p>
                </td>
              </tr>
  </table>
HERE;
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
        }
    }


    public function addNewTables()
    {
       $query = "
DROP TABLE IF EXISTS `job_periodes` ;
CREATE TABLE `job_periodes` (
 `id` int(2) NOT NULL,
 `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `job_periodes` ADD PRIMARY KEY (`id`);
ALTER TABLE `job_periodes` MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `job_periodes`(`id`, `title`) SELECT `id`, `title` FROM `test`.`job_periodes`;
DROP TABLE IF EXISTS `job_types` ;
CREATE TABLE `job_types` (
 `id` int(2) NOT NULL,
 `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `job_types` ADD PRIMARY KEY (`id`);
ALTER TABLE `job_types` MODIFY `id` int(2) NOT NULL AUTO_INCREMENT;

SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `job_types`(`id`, `title`) SELECT `id`, `title` FROM `test`.`job_types`;
DROP TABLE IF EXISTS `payment_type` ;
CREATE TABLE `payment_type` (
 `id` int(10) NOT NULL,
 `title` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `payment_type` ADD PRIMARY KEY (`id`);
ALTER TABLE `payment_type` MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `payment_type`(`id`, `title`) SELECT `id`, `title` FROM `test`.`payment_type`;";
      $this->query($query);
    }


    public function jobsTable()
    {

        $sql = 'update jobs j set j.bounty_title = j.bounty where j.bounty_title="";';
        $this->query($sql);

        $sql = 'update jobs j set j.bounty_title = j.bounty where j.bounty_title IS NULL;';
        $this->query($sql);

        $sql = $this->select()->from(['j' => 'jobs'],['*']);
        $result = $this->fetchAll($sql);
        foreach($result as $key => $job)
        {
            $new_bounty  = preg_replace('/\D/', '', $job['bounty']);
            $this->update(['bounty' => $new_bounty],"id={$job['id']}", 'jobs');
        }

    }

    public function changeBountyType()
    {
        $sql = "ALTER TABLE `jobs` CHANGE `bounty` `bounty` INT(10) NULL DEFAULT '0';";
        $this->query($sql);
    }


    public function setLanguageTable()
    {
        $sql[] = "CREATE TABLE `languages` (
  `id` int(10) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $sql[] = "ALTER TABLE `languages`  ADD PRIMARY KEY (`id`);
";
        $sql[] = "INSERT INTO `languages` (`id`, `title`) VALUES
(1, 'en'),
(2, 'ro'),
(3, 'fr');";
        foreach($sql as $sql )
        {
            $this->query($sql);
        }
    }

    public function addDefaultLangauge()
    {
        $sql[] = "ALTER TABLE `companies` ADD `default_language` INT(10) NOT NULL DEFAULT '1' AFTER `id_industry`;";
//        $sql[] = "ALTER TABLE `companies` CHANGE `default_language` `default_language` INT(10) NOT NULL DEFAULT '1';";

        foreach($sql as $sql )
        {
            $this->query($sql);
        }
    }

    public function coreectChannel()
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['*'])
            ->where('ja.source = 0');
        $result = $this->fetchAll($sql);


        $source = "Website" ;
        $sql = $this->select()->from(['sm' => 'social_media'],['id'])
            ->where("sm.title like '%$source%'");
        $id_source = $this->fetchRow($sql)['id'];

        foreach($result as $k => $v)
        {
            $this->update( ['source' => $id_source],"id={$v['id']}",'job_applications');
        }

    }

    public function updateClassnameLenght()
    {
        $query = "ALTER TABLE `branding` CHANGE `class_name` `class_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL";
        $this->query($query);
    }

    public function createVerifiedField()
    {
        $query = "ALTER TABLE `users` ADD `verified` INT(1) NOT NULL DEFAULT '0' AFTER `bounce_date`;";
        $this->query($query);

        $sql = $this->select()->from(['u' => 'users'])->where("u.status =?",(int) 1);
        $result  = $this->fetchAll($sql);
        foreach ($result as $index => $user) {
            $this->update(['verified' => 1] ,"id={$user['id']}",'users');
        }
    }

    public function createLink()
    {
        $var = '[{"link":"#","label":" EN "},{"link":"/en","label":" English"},{"link":"/ro","label":" Romana"},{"link":"/job-details?id=1226&id_app=4","label":" qqqqqqq qqqqqqq 2 weeks Test "},{"link":"/job-details?id=1226&id_app=5","label":" yyyyyyyyyyyy yyyyyyyyyyy 2 weeks Test "},{"link":"/job-details?id=1226&id_app=6","label":" ewrwerwere wrwerwerw 2 weeks Test "},{"link":"/view-jobs","label":"View Jobs"},{"link":"#","label":" 4 "},{"link":"/users/admin/rate?id=NzE=","label":" Monica Bontu Sales Representative (HR Services) - Douala "},{"link":"/users/admin/rate?id=NzI=","label":" Monica Bontu Product Manager "},{"link":"/users/admin/rate?id=Nzk=","label":" bdbdbd rrrrrrrrrrr "},{"link":"/users/admin/rate?id=ODA=","label":" djddj djdjd rrrrrrrrrrr "},{"link":"#","label":" Admin Company "},{"link":"/profile","label":"Profile"},{"link":"/dashboard","label":"Admin"},{"link":"/home","label":" Jobs"},{"link":"/logout","label":" Log Out"},{"link":"/dashboard","label":" Dashboard "},{"link":"javascript:;","label":" Companies "},{"link":"/view-companies","label":"View Companies"},{"link":"/edit-company","label":"Add Company"},{"link":"/view-jobs","label":" Job Management "},{"link":"/view-jobs","label":" View Jobs "},{"link":"/edit-job","label":"Add Job"},{"link":"/applications","label":"View applications"},{"link":"/users/admin/mycvs","label":" Candidates "},{"link":"/users/admin/search","label":"Search CVs"},{"link":"/users/admin/mycvs","label":"My CVs"},{"link":"/view-campaigns","label":" Campaigns "},{"link":"/bounties","label":" Bounties "},{"link":"javascript:;","label":" Analytics "},{"link":"/analytics","label":"Stats"},{"link":"/leaderboards","label":"Leaderboards"},{"link":"javascript:;","label":" Settings "},{"link":"/users","label":"Users"},{"link":"/view-billing","label":"Company"},{"link":"/view-template","label":"Email Templates"},{"link":"/view-jobs","label":" Job Management "},{"link":"/view-jobs","label":" View Jobs "},{"link":"/applications","label":"View applications"},{"link":"javascript:;","label":" Candidates "},{"link":"/users/admin/mycvs","label":"My CVs"},{"link":"/bounties","label":"View Bounties"},{"link":"/view-bounties","label":"View Bounties"},{"link":"/view-documents","label":"View bounties documents"},{"link":"javascript:;","label":" Settings "},{"link":"/companies/admin/brand","label":"brand"},{"link":"/view-users","label":"View Users"},{"link":"/edit-user","label":"Add User"},{"link":"/users/admin/import","label":"Import Users from File"},{"link":"/view-billing","label":"Company & Billing"},{"link":"/company","label":"Company Details"},{"link":"/users/admin/company-events","label":"Events Virtual Booths"},{"link":"javascript:;","label":" Scoring "},{"link":"/view-scoring","label":"View users scoring"},{"link":"/edit-points","label":"Edit scoring points"},{"link":"/leaderboards","label":"Leaderboards"},{"link":"javascript:;","label":" Static Content "},{"link":"/view-skills","label":"View Skills"},{"link":"/view-certifications","label":"View Certifications"},{"link":"/view-countries","label":"View Countries"},{"link":"/view-cities","label":"View Cities"},{"link":"/view-industries","label":"View Industries"},{"link":"/view-departments","label":"View Departments"},{"link":"/logout","label":" Logout"},{"link":"/login","label":"Sign In"},{"link":"/","label":"Features"},{"link":"/login","label":"Login"},{"link":"/demo","label":"Demo"},{"link":"/login","label":"Sign In"},{"link":"/","label":"Features"},{"link":"/login","label":"Login"},{"link":"/demo","label":"Demo"}]';
        $var = json_decode($var,true);
        foreach($var as $key => $value)
        {
            $data['link'] = $value['link'];
            $data['label'] = $value['label'];
            $data['icon'] = "";
            $this->insert($data,'acl');
        }
        echo '<pre>';
        print_r($var);
        die;
    }

    public function inserttableaclmatrix()
    {
        $types = [1,2,3,4,5,9];
        $sql = $this->select()->from(['a' => 'acl']);
        $result = $this->fetchAll($sql);
        foreach ($result as $index => $item) {
           foreach($types as $type)
           {
               $data['type'] = $type;
               $data['id_acl'] = $item['id'];
               $data['status'] = 0;
               $this->insert($data,'acl_matrix');

           }
        }
    }

    public function updateLang()
    {


        $tr = [
            "start_date" => "Start date",
            "end_date" => "End Date",
            "campaign_name" => "Campaign Name",
            "campaign_jobs" => "Campaign Jobs",
            "campaign_actions" => "Campaign Action",
            "status" => "Status",
            "1st_place"  => "1st place",
            "2nd_place" => "2nd place",
            "3rd_place" => "3rd place",
            "actions" => "Actions",
            "filter" => "Filter",
            "active" => "Active",
            "expired" => "Expired",
            "paused" => "Paused",
            "export" => "Export",
            "add_campaign" => "Add campaign",
            "from" => "From",
            "to" => "To",
            "step_1_objectives" => "Step 1: Objectives",
            "step_2_jobs" => "Step 2: Jobs",
            "step_3_rules" => "Step 3: Campaign Rules",
            "step_4_reward" => "Step 4: Reward",
            "campaign_name" => "Campaign Name",
            "campaign_objectives" => "Campaign objectives",
            "campaign_objectives_desc" => "Let’s get started! Select the objective that best fits your goals below.",
            "employee_engagement" => "Employee engagement",
            "candidates_consideration" => "Candidates Consideration",
            "recruitment" => "Recruitment",
            "as many job shares as possible" => "As many job shares as possible",
            "as many job views as possible" => "As many job views as possible",
            "as many candidates as possible" => "As many candidates as possible",
            "as many applicants as possible" => "As many applicants as possible",
            "as many hired people as possible" => "As many hired people as possible",
            "campaign jobs" => "Campaign Jobs",
            "campaign_promote_desc" => "Let's choose what jobs we want to promote with this campaign",
            "geo_filter" => "Geographical filters",
            "countries" => "Countries",
            "counties" => "Counties",
            "cities" => "Cities",
            "departments_filters" => "Departments filters",
            "department" => "Departments",
            "sub_department" => "Sub Departments",
            "sub_sub_department" => "Sub Sub Departments",
            "filter_by_project" => "Filter by projects",
            "select_jobs_in_campaign" => "Select jobs in camapign",
            "jobs" => "Jobs",
            "campaign_rules" => "Campaign Rules",
            "campaign_rules_descision" => "It's time to decide campaign rules that will allow your employees to collect points and advance in campaign leaderboard.",
            "important_dates" => "Important dates",
            "start_of_actions" => "Start of actions",
            "winner_announcement" => "Winner announcement",
            "reward_redeem" => "Reward redeem",
            "validations" => "Validations",
            "check_if_suitable" => "Check if suitable",
            "action_values" => "Action Values",
            "set_objective_value" => "Set objective value",
            "good_to_know" => "Good to know",
            "campaign_heros" => "The place to explain even more details that count when you decide who will be this campaign's heroes",
            "campaign_reward" => "Campaign Reward",
            "employees_win" => "Let's define what your employees are going to win",
            "prize_photo" => "Prize Photo",
            "allowed_extension" => ".png, .jpg, .gif allowed",
            "prize_details" => "Prize Details",
            "prize_matter" => "Prizes matter, so do not spare any interesting facts about it!",
            "name" => "Name",
            "description" => "Description",
            "value" => "Value",
            "currency" => "Currency",
            "pay" => "Pay",
            "previous" => "Previous",
            "next" => "Next",
            "leaderboards" => "Leaderboards",
            "campaign_end" => "%u Campaigns get closer to the deadline",
            "general_score" => "General score",
            "general_results" => "General results",
        ];
        foreach ($tr as $key => $item) {
            $data['key'] = $key;
            $data['value'] = $item;
            $data['lang'] = 'en';
            $data['id_lang'] = 1;
            $this->insert($data,'language_translate');
        }
    }


    public function upsocial()
    {
        $sql = "truncate table social_media";
        $this->query($sql);
        $str = "INSERT INTO `social_media` (`id`, `title`, `url`)
VALUES
	(1,'Ejobs','https://ejobs.ro'),
	(2,'BestJobs','https://bestjobs.ro'),
	(3,'HiPo','https://hipo.ro'),
	(4,'Internal',NULL),
	(5,'LinkedIn',NULL),
	(6,'Facebook',NULL),
	(7,'Twitter',NULL),
	(8,'Email','mailto:'),
	(9,'Whatsapp','https://api.whatsapp.com/send'),
	(10,'MyJob','https://myjob.ro'),
	(12,'Website',NULL),
	(13,'google-plus','https://plus.google.com/'),
	(14,'Instagram','https://instagram.com'),
	(15,'SMS','sms://'),
	(16,'Link',NULL),
	(17,'Network',NULL),
	(18,'Upload Cv',NULL);";
        $this->query($str);

        $sql = "update job_applications set source=18 where source=0";
        $this->query($sql);
    }

    public function upEmails()
    {
        $types = [
            97,
            25,
            66,
            26,
            27,
            28,
            31,
            65,
            32,
            68,
            39,
            40,
            41,
            42,
            64,
            67,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            85,
            86,
            87,
            88,
            89,
            90,
            91,
            92,
            93,
            94,
            95,
            69,
            23,
            24,
            96
        ];
        foreach ($types as $type)
        {
            $sql = $this->select()->from(['e' => 'emails'],['*'])
                ->join(['el' => 'emails_lang'],'el.id_email=e.id',[])
                ->where('e.type =?',(int) $type);
            $email = $this->fetchRow($sql) ;

            if(empty($email))
            {
                $query = "SELECT `e`.*, `el`.* FROM `test`.`emails` AS `e`
                     INNER JOIN `test`.`emails_lang` AS `el` ON el.id_email=e.id WHERE (e.type =$type)";
                $result = $this->fetchRow($query);

                $sql = $this->select()->from(['c' => 'companies'],['*']);
                $companies = $this->fetchAll($sql);

                foreach ($companies as $index => $company)
                {
                    $this->updateEmailContent($result,$company);
                }


            }
        }
    }

    public function updateEmailContent($result,$company)
    {

        $data['id_company'] = $company['id'];
        $data['type'] = $result['type'];
        $data['status'] = 1;
        $data['sent'] = 1;
        $data['frequency'] = 1;
        $data['direction'] = $result['direction'];
        $data['email'] = $company['email'];
        $data['from'] = $company['title'];

        $this->insert($data, "emails");
        $lastID = $this->lastInsertId();
        if ($lastID) {
            $_data['id_email'] = $lastID;
            $_data['id_lang'] =  $result['id_lang'];
            $_data['title'] =  $result['title'];
            $_data['subject'] = $result['subject'];
            $_data['content'] = $result['content'];
            $this->insert($_data, 'emails_lang');
        }
    }

    public function upContent()
    {
        $sql = $this->select()->from(['gp' => 'gamifications_actions'],['*'])->where('gp.action = "completed_profile"');
        $completed_profile = $this->fetchRow($sql);

        $sql = $this->select()->from(['gp' => 'gamifications_actions'],['*'])->where('gp.action = "activate_account"');
        $activate_account = $this->fetchRow($sql);

        $sql = $this->select()->from(['gp' => 'gamifications_actions'],['*'])->where('gp.action = "new_candidates"');
        $new_candidates = $this->fetchRow($sql);

        $sql = $this->select()->from(['gp' => 'gamifications_actions'],['*'])->where('gp.action = "new_applicants"');
        $new_applicants = $this->fetchRow($sql);

//        echo '<pre>';
//        print_r($completed_profile);
//        print_r($activate_account);
//        die;

        $sql = $this->select()->from(['u' => 'users'],['*'])->where('u.id_parent > 0');
        $users = $this->fetchAll($sql);
        $groupped = [];
        foreach($users as $key => $user)
        {
            $groupped[$user['id_parent']][] = $user;
        }

        foreach ($groupped as $i => $user)
        {
//            $this->delete("id_user=$i && action={$activate_account['id']}",'gamification_points');
//            $this->delete("id_user=$i && action={$completed_profile['id']}",'gamification_points');
//            $this->delete("id_user=$i && action={$new_candidates['id']}",'gamification_points');
//            $this->delete("id_user=$i && action={$new_applicants['id']}",'gamification_points');
            // delete activted & completed;
            $this->delete(array("id_user = ?" => $i, "action = ?" => $activate_account['id']),'gamification_points');
            $this->delete(array("id_user = ?" => $i, "action = ?" => $completed_profile['id']),'gamification_points');
            $this->delete(array("id_user = ?" => $i, "action = ?" => $new_candidates['id']),'gamification_points');
            $this->delete(array("id_user = ?" => $i, "action = ?" => $new_applicants['id']),'gamification_points');
            foreach($user as $u)
            {
                // set new candidate
                $this->insert([
                    'id_user' => $i,
                    'action' => $new_candidates['id'],
                    'id_item' => 0,
                    'points' => $new_candidates['points'],
                    'date' => $u['created'],

                ],'gamification_points');


                if($u['first_login'] != "")
                {
                    // set activated account
                    $this->insert([
                        'id_user' => $i,
                        'action' => $activate_account['id'],
                        'id_item' => 0,
                        'points' => $activate_account['points'],
                        'date' => $u['created'],

                    ],'gamification_points');
                }

                $sql = $this->select()->from(['uc' => 'user_cv'],['*'])->where('uc.id_user =?',(int) $u['id']);
                $res = $this->fetchRow($sql);

                if((!empty($res) || $u['linkedin_link'] != ""))
                {
                    // set completed profile
                    $this->insert([
                        'id_user' => $i,
                        'action' => $completed_profile['id'],
                        'id_item' => 0,
                        'points' => $completed_profile['points'],
                        'date' => $u['created'],

                    ],'gamification_points');
                }
            }
            $sql = $this->select()->from(['ja' => 'job_applications'],['*'])
                ->where('ja.id_recommender =?',(int) $i)
                ->group('ja.id_user');
            $unique_app = $this->fetchAll($sql);
            if(!empty($unique_app))
            {
                foreach ($unique_app as $index => $item) {
                    // set unique application
                    $this->insert([
                        'id_user' => $i,
                        'action' => $new_applicants['id'],
                        'id_item' => 0,
                        'points' => $new_applicants['points'],
                        'date' => $u['created'],

                    ],'gamification_points');
                }
            }

        }

        $filters = [];
        $offset = 0;
        $mCompany = new models_Company ;
        $data = $mCompany->getLeaderboardsInfo($filters,$offset)['referral'];
        foreach($data as $i => &$value)
        {
            $total = 0;
            foreach($value as $key => $val)
            {
                $action = $key;
                if($this->_modelCompany->existAction($action))
                {
                    $value[$key] = $this->_modelCompany->collectGamificationInfo($value['id'],$action);
                    $total += $value[$key]['points'];
                }
            }
            $value['total_points'] = $total;
        }

        $cache = WebApp_Cache_Memcache::getInstance();
        $cache->set('leaderboard',$data);


    }

    public function fixlang()
    {
        $sql = $this->select()->from(['lt' => 'language_translate'])->where('lt.id_lang =1');
        $en = $this->fetchAll($sql);
        foreach($en as $key => $value)
        {
            $array = [
                ['lang' => 'ro' , 'id_lang' => 2],
                ['lang' => 'hu' , 'id_lang' => 4],
                ['lang' => 'fr' , 'id_lang' => 3],
            ];

            foreach($array as $i => $v)
            {
                $sql = $this->select()->from(['lt' => 'language_translate'],['*'])
                    ->where('lt.lang =?',$v['lang'])
                    ->where('lt.key =?',$value['key'])
                    ->where('lt.id_lang =?',(int) $v['id_lang']);
                $r = $this->fetchRow($sql);

                if(empty($r))
                {
                    $data = [
                      'key' => $value['key'],
                      'value' => $value['value'],
                      'lang' => $v['lang'],
                      'id_lang' => $v['id_lang'],
                    ];

                    $id = $this->insert($data,'language_translate');
//                    echo '<pre>';
//                    print_r($id);
//                    print_r($data);
//                    die;
                }
            }
        }
    }
}



?>