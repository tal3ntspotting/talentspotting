<?php

class models_Content extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'users';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    public $id_lang;
    public $_stats;
    public $_modelCompany;
    public $translate;
    protected static $_instance = null;
    protected $_legend = array('[user.firstname]', '[user.lastname]', '[user.email]', '[user.address]', '[user.bank]', '[user.iban]', '[user.cui]', '[user.j]', '[user.company]',
        '[bounty.amount]', '[bounty.company]', '[job.title]', '[job.company]', '[site_name]', '[search.filters]', '[page.title]', '[company.title]',
        '[company.description]', '[news.title]', '[news.description]', '[quiz.title]', '[quiz.description]', '[quiz.profile]', '[user.firstname]', '[message]');

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);
        $this->_modelCompany = new models_Company;
        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;

        $this->_stats = new models_Stats;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
        $lang = Zend_Registry::get('_LANG_DETAILS');
        $this->translate = Zend_Registry::get("Zend_Translate");
    }

    public function getSkills() {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('*', '(SELECT COUNT(js.id_skill) FROM job_skills js, jobs j WHERE js.id_job = j.id AND s.id = js.id_skill) AS active_jobs', '(SELECT COUNT(us.id_skill) FROM user_skills us, users u WHERE us.id_user = u.id AND s.id = us.id_skill) AS active_users'))
                ->join(array('sl' => 'skills_lang'), "s.id = sl.id_skill", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getSkillsInAlerts() {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('*'))
                ->join(array('sl' => 'skills_lang'), "s.id = sl.id_skill", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("s.in_alerts = 1")
                ->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getSkillsSelect($skills = null) {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('id'))
                ->join(array('sl' => 'skills_lang'), "s.id = sl.id_skill", array('title'))
//                ->where("sl.id_lang = ?", (int) $this->id_lang)
                ->where("title != ''")
                ->order('sl.title ASC');
        if (null !== $skills) {
            $sql->where("s.id IN (" . implode(",", $skills) . ")");
        }
        return $this->fetchPairs($sql);
    }

    public function getCustomSkillsSelect($skills = null) {
        $sql = $this->select()
                ->from(array('s' => 'custom_skills'), array('id', "title"))
                ->order('s.title ASC');
        if (null !== $skills) {
            $sql->where("s.id IN (" . implode(",", $skills) . ")");
        }
        $sql->where("s.id_company =?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getSkillsSelectImport() {
        $sql = $this->select()->from(array('sl' => 'skills_lang'), array('id', 'title'));
        return $this->fetchPairs($sql);
    }

    public function getSkillByLangId($id) {
        $sql = $this->select()->from(array('sl' => 'skills_lang'), array('id_skill'))->where("id = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['id_skill'];
    }

    public function getSkillsSlugSelect() {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('id'))
                ->join(array('sl' => 'skills_lang'), "s.id = sl.id_skill", array('slug'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchPairs($sql);
    }

    public function getCertifications() {
        $sql = $this->select()
                ->from(array('s' => 'certifications'), array('*'))
                ->join(array('sl' => 'certifications_lang'), "s.id = sl.id_certification", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getCertificationsSelect() {
        $sql = $this->select()
                ->from(array('s' => 'certifications'), array('id'))
                ->join(array('sl' => 'certifications_lang'), "s.id = sl.id_certification", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchPairs($sql);
    }

    public function getCountries() {
        $sql = $this->select()->from(array('s' => 'countries'), array('*'))->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getCities() {
        $sql = $this->select()
                ->from(array('s' => 'cities'), array('*'))
                ->joinLeft(array('c' => 'countries'), "s.id_country = c.id", array("title AS country"))
                ->order('s.id DESC');
        return $this->fetchAll($sql);
    }

    public function getIndustries() {
        $sql = $this->select()
                ->from(array('s' => 'industries'), array('*'))
                ->join(array('sl' => 'industries_lang'), "s.id = sl.id_industry", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getIndustriesSelect($industries = null) {
        $sql = $this->select()
                ->from(array('s' => 'industries'), array('id'))
                ->join(array('sl' => 'industries_lang'), "s.id = sl.id_industry", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('title ASC');

        if (null !== $industries) {
            $sql->where("s.id IN (" . implode(",", $industries) . ")");
        }
        return $this->fetchPairs($sql);
    }

    public function getIndustriesSelectImport() {
        $sql = $this->select()->from(array('il' => 'industries_lang'), array('id', 'title'));
        return $this->fetchPairs($sql);
    }

    public function getIndustryByLangId($id) {
        $sql = $this->select()->from(array('il' => 'industries_lang'), array('id_industry'))->where("id = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['id_industry'];
    }

    public function getIndustriesSlugSelect() {
        $sql = $this->select()
                ->from(array('s' => 'industries'), array('id'))
                ->join(array('sl' => 'industries_lang'), "s.id = sl.id_industry", array('slug'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchPairs($sql);
    }

    public function getDepartments() {
        $sql = $this->select()
                ->from(array('s' => 'departments'), array('*'))
                ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchAll($sql);
    }

    public function getDepartmentsSelect($departments = null) {
        $sql = $this->select()
                ->from(array('s' => 'departments'), array('id'))
                ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('title'))
//                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('title ASC');

        if (null !== $departments) {
            $sql->where("s.id IN (" . implode(",", $departments) . ")");
        }
        $data = $this->fetchPairs($sql);

        return $data;
    }

    public function getCustomDepartmentsSelect($departments = null) {
        $sql = $this->select()
                ->from(array('s' => 'custom_departments'), array('id', 'title'))
                ->order('title ASC');

        if (null !== $departments) {
            $sql->where("s.id IN (" . implode(",", $departments) . ")");
        }
        //die($sql->__toString());
        $sql->where("s.id_company =?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getDepartmentsSelectImport($departments = null) {
        $sql = $this->select()->from(array('sl' => 'departments_lang'), array('id', 'title'));
        return $this->fetchPairs($sql);
    }

    public function getDepartmentByLangId($id) {
        $sql = $this->select()->from(array('sl' => 'departments_lang'), array('id_department'))->where("id = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['id_department'];
    }

    public function getDepartmentsSlugSelect() {
        $sql = $this->select()
                ->from(array('s' => 'departments'), array('id'))
                ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('slug'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');
        return $this->fetchPairs($sql);
    }

    public function getHeaderPages($visible = null) {
//        $sql = $this->select()
//                ->from(array('s' => 'pages'), array('*'))
//                ->join(array('sl' => 'pages_lang'), "s.id = sl.id_page", array('title', 'slug'))
//                ->where("sl.id_lang = ?", $this->id_lang)
//                ->order('id DESC');
//        if (null !== $visible) {
//            $sql->where("s.visible = ?", $visible);
//        }
//        $data = $this->fetchAll($sql);
//
//        foreach ((array) $data as $row) {
//            $pages[$row['id']]['title'] = $row['title'];
//            $pages[$row['id']]['slug'] = $row['slug'];
//        }
//
//        return (array) $pages;
    }

    public function getTaglinePages($visible = null) {
//        $sql = $this->select()
//                ->from(array('s' => 'pages'), array('*'))
//                ->join(array('sl' => 'pages_lang'), "s.id = sl.id_page", array('title', 'slug'))
//                ->where("sl.id_lang = ?", $this->id_lang)
//                ->where("s.tagname != ''")
//                ->order('id DESC');
//        if (null !== $visible) {
//            $sql->where("s.visible = ?", $visible);
//        }
//        $data = $this->fetchAll($sql);
//
//        foreach ((array) $data as $row) {
//            $pages[$row['tagname']]['title'] = $row['title'];
//            $pages[$row['tagname']]['slug'] = $row['slug'];
//        }
//
//        return (array) $pages;
    }

    /*
      details
     */

    public function getSkillById($id) {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('*'))
                ->join(array('sl' => 'skills_lang'), "s.id = sl.id_skill", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("s.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getCertificationById($id) {
        $sql = $this->select()
                ->from(array('s' => 'certifications'), array('*'))
                ->join(array('sl' => 'certifications_lang'), "s.id = sl.id_certification", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("s.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getCountryById($id) {
        $sql = $this->select()->from(array('s' => 'countries'), array('*'))->where("id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getCityById($id) {
        $sql = $this->select()->from(array('s' => 'cities'), array('*'))->where("id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getIndustryById($id) {
        $sql = $this->select()
                ->from(array('s' => 'industries'), array('*'))
                ->join(array('sl' => 'industries_lang'), "s.id = sl.id_industry", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("s.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getDepartmentById($id) {
        $sql = $this->select()
                ->from(array('s' => 'departments'), array('*'))
                ->join(array('sl' => 'departments_lang'), "s.id = sl.id_department", array('title'))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("s.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    /*
      save
     */

    public function saveSkill($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update(array("id" => $id, "in_alerts" => $data['in_alerts']), "id = {$id}", "skills");
            $this->update(array("title" => $data['title'], "slug" => $slug), "id_skill = {$id} AND id_lang = {$this->id_lang}", "skills_lang");
        } else {
            $this->insert(array("active_jobs" => 0, "in_alerts" => $data['in_alerts']), "skills");
            $id = $this->lastInsertId();

            $this->insert(array("title" => $data['title'], "id_skill" => $id, "id_lang" => $this->id_lang, "slug" => $slug), "skills_lang");
        }

        return $id;
    }

    public function saveCertification($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update(array("company" => (string) $data['company']), "id = {$id}", "certifications");
            $this->update(array("title" => $data['title'], "slug" => $slug), "id_certification = {$id} AND id_lang = {$this->id_lang}", "certifications_lang");
        } else {
            $this->insert(array("company" => (string) $data['company']), "certifications");
            $id = $this->lastInsertId();

            $this->insert(array("title" => $data['title'], "id_certification" => $id, "id_lang" => $this->id_lang, "slug" => $slug), "certifications_lang");
        }

        return $id;
    }

    public function saveCountry($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);
            $data['slug'] = $slug;
            $this->update($data, "id = {$id}", "countries");

        } else {
            $data['slug'] = $slug;
            $this->insert($data, "countries");
            $id = $this->lastInsertId();
        }

        return $id;
    }

    public function saveCity($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);
            $data['slug'] = $slug;
            $this->update($data, "id = {$id}", "cities");
        } else {
            $data['slug'] = $slug;
            $this->insert($data, "cities");
            $id = $this->lastInsertId();
        }

        return $id;
    }

    public function saveIndustry($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update(array("id" => $id), "id = {$id}", "industries");
            $this->update(array("title" => $data['title'], "slug" => $slug), "id_industry = {$id} AND id_lang = {$this->id_lang}", "industries_lang");
        } else {
            $this->insert(array("active_jobs" => 0), "industries");
            $id = $this->lastInsertId();

            $this->insert(array("title" => $data['title'], "id_industry" => $id, "id_lang" => $this->id_lang, "slug" => $slug), "industries_lang");
        }
        return $id;
    }

    public function saveDepartment($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update(array("id" => $id), "id = {$id}", "departments");
            $this->update(array("title" => $data['title'], "slug" => $slug), "id_department = {$id} AND id_lang = {$this->id_lang}", "departments_lang");
        } else {
            $this->insert(array("active_jobs" => 0), "departments");
            $id = $this->lastInsertId();

            $this->insert(array("title" => $data['title'], "id_department" => $id, "id_lang" => $this->id_lang, "slug" => $slug), "departments_lang");
        }

        return $id;
    }

    /*
      misc
     */

    public function getCountriesSelect($countries = null) {
        $sql = $this->select()->from(array("c" => "countries"), array("id", "title"))->order("title ASC");

        if (null !== $countries) {
            $sql->where("c.id IN (" . implode(",", $countries) . ")");
        }

        return $this->fetchPairs($sql);
    }

    public function getCountiesSelect($countries = null) {
        $sql = $this->select()->from(array("c" => "counties"), array("id", "title"))->order("title ASC");

        if (null !== $countries) {
            $sql->where("c.id IN (" . implode(",", $countries) . ")");
        }

        return $this->fetchPairs($sql);
    }

    public function getCitiesSelect($id_country = null, $cities = null) {
        $sql = $this->select()->from(array("c" => "cities"), array("id", "title"))->order("title ASC");
        if (null !== $id_country) {
            if (is_array($id_country)) {
                $sql->where("id_county IN (" . implode(",", $id_country) . ")");
            } else {
                $sql->where("id_county = ?", $id_country);
            }
        }

        if (null !== $cities) {
            $sql->where("c.id IN (" . implode(",", $cities) . ")");
        }

        return $this->fetchPairs($sql);
    }

    public function getSetectedCitiesJobs($id_country = null, $cities = null) {
        $sql = $this->select()->from(array("c" => "cities"), array("id", "title"))->order("title ASC")
            ->distinct();
        if (null !== $id_country) {
            if (is_array($id_country)) {
                $sql->where("id_county IN (" . implode(",", $id_country) . ")");
            } else {
                $sql->where("id_county = ?", $id_country);
            }
        }

        if (null !== $cities) {
            $sql->where("c.id IN (" . implode(",", $cities) . ")");
        }
        $sql->join(['js' => 'job_cities'],'js.id_city=c.id',[]);

        return $this->fetchAll($sql);
    }

    public function getCustomCitiesSelect($id_country = null, $cities = null) {
        $sql = $this->select()->from(array("c" => "custom_cities"), array("id", "title"))->order("title ASC");
        if (null !== $id_country) {
            if (is_array($id_country)) {
                $sql->where("id_country IN (" . implode(",", $id_country) . ")");
            } else {
                $sql->where("id_country = ?", $id_country);
            }
        }

        if (null !== $cities) {
            $sql->where("c.id IN (" . implode(",", $cities) . ")");
        }
        $sql->where("c.id_company =?", $this->_user->id_company);

        return $this->fetchPairs($sql);
    }

    public function getJobsSelect() {
        $sql = $this->select()->from(array('jbs' => 'jobs'), array('jbs.id'))
                ->join(array('jbsl' => 'jobs_lang'), "jbs.id = jbsl.id_job", array('jbsl.title'));
//                              ->group('jbs.id');
        return $this->fetchPairs($sql);
    }

    public function getGamification() {
        $sql = $this->select()->from(array('gam' => 'gamifications_actions'), array('gam.id', 'gam.action'));
        return $this->fetchPairs($sql);
    }

    public function getCitiesSlugSelect($id_country = null) {
        $sql = $this->select()->from(array("c" => "cities"), array("id", "slug"))->order("title ASC");
        if (null !== $id_country) {
            $sql->where("id_country = ?", $id_country);
        }
        return $this->fetchPairs($sql);
    }

    public function getCountriesSlugSelect() {
        $sql = $this->select()->from(array("c" => "countries"), array("id", "slug"))->order("title ASC");
        return $this->fetchPairs($sql);
    }

    public function getUserSkills($id) {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('*'))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title", "slug"))
                ->join(array("us" => "user_skills"), "s.id = us.id_skill", array('skill_years','rating'))
                ->where("us.id_user = ?", (int) $id)
                ->order('id DESC');

        return $this->fetchAll($sql);
    }

    public function getUserSkillsByAttributes($id,$attributes = []) {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array())
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", $attributes)
                ->join(array("us" => "user_skills"), "s.id = us.id_skill", array())
                ->where("us.id_user = ?", (int) $id)
                ->order('id DESC');

        return $this->fetchAll($sql);
    }

    public function getCustomUserSkills($id) {
        $sql = $this->select()
                ->from(array('cus' => 'custom_user_skills'), array('id', "title"))
                ->where("cus.id_user = ?", (int) $id)
                ->order('title DESC');

        return $this->fetchAll($sql);
    }

    public function getCustomUserSkillsSelect() {
        $sql = $this->select()
                ->from(array('cus' => 'custom_user_skills'), array('id', "title"))
                ->where("cus.id_user = ?", (int) Zend_Auth::getInstance()->getIdentity()->id)
                ->order('title DESC');

        return $this->fetchPairs($sql);
    }

    public function getUsersSkills() {
        $sql = $this->select()
                ->from(array('s' => 'skills'), array('*'))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title", "slug"))
                ->join(array("us" => "user_skills"), "s.id = us.id_skill", array("id_user"))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->order('id DESC');

        $data = $this->fetchAll($sql);
        foreach ((array) $data as $row) {
            $skills[$row['id_user']][] = $row['title'];
        }

        return $skills;
    }

    public function getUserCertifications($id) {
        $sql = $this->select()
                ->from(array('c' => 'certifications'), array('c.id'))
                ->join(array("cl" => "certifications_lang"), "c.id = cl.id_certification", array("cl.title", "cl.slug"))
                ->join(array("uc" => "user_certifications"), "c.id = uc.id_certification", array())
                ->where("uc.id_user = ?", (int) $id)
                ->order('id DESC');

        return $this->fetchAll($sql);
    }

    public function getUsersCertifications() {
        $sql = $this->select()
                ->from(array('c' => 'certifications'), array('*'))
                ->join(array("cl" => "certifications_lang"), "c.id = cl.id_certification", array("title", "slug"))
                ->join(array("uc" => "user_certifications"), "c.id = uc.id_certification", array("id_user"))
                ->where("cl.id_lang = ?", $this->id_lang)
                ->order('id DESC');

        $data = $this->fetchAll($sql);
        foreach ((array) $data as $row) {
            $certifications[$row['id_user']][] = $row['title'];
        }

        return $certifications;
    }

    public function getSkillsIDontHave() {
        $sql = $this->select()
                ->from(array("s" => "skills"), array("id"))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title"))
                ->where("s.id NOT IN (SELECT id_skill FROM user_skills WHERE id_user = '{$this->_user->id}')")
                ->order('title ASC');

        return $this->fetchPairs($sql);
    }

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function export($data, $file) {
        $modelUser = new models_User;
        return $modelUser->export($data, $file);
    }

    public function getBountyDocuments() {
        $sql = $this->select()
                ->from(array("b" => "bounty_documents"))
                ->join(array("bl" => "bounty_documents_lang"), "b.id = bl.id_bounty_document", array("title", "slug", "content"))
                ->join(array("u" => "users"), "b.id_user = u.id", array("CONCAT(firstname, ' ', lastname) AS name"))
                ->where("bl.id_lang = ?", $this->id_lang);

        return $this->fetchAll($sql);
    }

    public function getDocumentById($id) {
        $sql = $this->select()
                ->from(array("b" => "bounty_documents"))
                ->join(array("bl" => "bounty_documents_lang"), "b.id = bl.id_bounty_document", array("title", "slug", "content", "id_lang"))
                ->where("bl.id_lang = ?", $this->id_lang)
                ->where("b.id = ?", (int) $id);

        return $this->fetchRow($sql);
    }

    public function saveDocument($data) {
        $slug = WebApp_Tools::CreateAlias($data['title']);
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update(array("date" => date("Y-m-d H:i:s")), "id = {$id}", "bounty_documents");
            $this->update(array("title" => $data['title'], "content" => $data['content'], "slug" => $slug), "id_bounty_document = {$id} AND id_lang = {$this->id_lang}", "bounty_documents_lang");
        } else {
            $this->insert(array("id_user" => $this->_user->id, "date" => date("Y-m-d H:i:s")), "bounty_documents");
            $id = $this->lastInsertId();

            $this->insert(array("title" => $data['title'], "content" => $data['content'], "id_bounty_document" => $id, "id_lang" => $this->id_lang, "slug" => $slug), "bounty_documents_lang");
        }

        return $id;
    }

    public function downloadDocument($id, $id_bounty) {
        $modelPayments = new models_Payments;

        $document = $this->getDocumentById($id);
        $bounty = $modelPayments->getBountyDetailed($id_bounty);
        $user = (array) $this->_user;

        $content = $this->_personalize($document['content'], array("user" => $user, "bounty" => $bounty));
        $file = $document['slug'] . ".pdf";

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . '/layouts/scripts/partials');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);

        $html = $partialHelper->partial("document.phtml", array("content" => $content));

        include(ABSOLUTE_PATH . "/library/mpdf/mpdf.php");
        $mpdf = new mPDF('c', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 15, $mgt = 16, $mgb = 16, $mgh = 9, $mgf = 9, $orientation = 'L');
        $mpdf->WriteHTML($html);
        $mpdf->Output($file, 'D');
    }

    public function _personalize($content, $params) {
        foreach ($this->_legend as $variable) {
            if (stripos($content, $variable) !== false) {
                switch ($variable) {
                    case "[user.firstname]":
                        $content = str_replace($variable, $params['user']['firstname'], $content);
                        break;

                    case "[user.lastname]":
                        $content = str_replace($variable, $params['user']['lastname'], $content);
                        break;

                    case "[user.email]":
                        $content = str_replace($variable, $params['user']['email'], $content);
                        break;

                    case "[user.address]":
                        $content = str_replace($variable, $params['user']['billing_address'], $content);
                        break;

                    case "[user.company]":
                        $content = str_replace($variable, $params['user']['billing_company'], $content);
                        break;

                    case "[user.bank]":
                        $content = str_replace($variable, $params['user']['billing_bank'], $content);
                        break;

                    case "[user.iban]":
                        $content = str_replace($variable, $params['user']['billing_iban'], $content);
                        break;

                    case "[user.cui]":
                        $content = str_replace($variable, $params['user']['billing_cui'], $content);
                        break;

                    case "[user.j]":
                        $content = str_replace($variable, $params['user']['billing_j'], $content);
                        break;

                    case "[bounty.amount]":
                        $content = str_replace($variable, $params['bounty']['amount'] . " " . $this->_settings['currencies'][1], $content);
                        break;

                    case "[bounty.company]":
                        $content = str_replace($variable, $params['bounty']['company'], $content);
                        break;

                    case "[site_name]":
                        $content = str_replace($variable, $this->_settings['sitename'], $content);
                        break;

                    case "[job.title]":
                        $content = str_replace($variable, $params['job']['title'], $content);
                        break;

                    case "[job.company]":
                        $content = str_replace($variable, $params['job']['company_title'], $content);
                        break;

                    case "[company.title]":
                        $content = str_replace($variable, $params['company']['title'], $content);
                        break;

                    case "[company.description]":
                        $content = str_replace($variable, $params['company']['description'], $content);
                        break;

                    case "[news.title]":
                        $content = str_replace($variable, $params['news']['title'], $content);
                        break;

                    case "[news.description]":
                        $content = str_replace($variable, $params['news']['description'], $content);
                        break;

                    case "[quiz.title]":
                        $content = str_replace($variable, $params['quiz']['title'], $content);
                        break;

                    case "[quiz.description]":
                        $content = str_replace($variable, $params['quiz']['description'], $content);
                        break;

                    case "[quiz.profile]":
                        $content = str_replace($variable, $params['quiz_profile']['title'], $content);
                        break;

                    case "[message]":
                        $content = str_replace($variable, $params['message'], $content);
                        break;

                    case "[search.filters]":
                        $filters = $params['search'];

                        foreach ((array) $filters as $filter) {
                            $f[] = @implode("& ", $filter);
                        }

                        if (!empty($f)) {
                            $content = str_replace($variable, implode(", ", $f), $content);
                        }
                        break;
                }
            }
        }

        return $content;
    }

    public function saveUserDepartments($data) {
        foreach ((array) $data as $id) {
            $this->delete("id_user = {$this->_user->id} AND id_department = {$id}", "user_departments");
            $this->insert(array("id_user" => $this->_user->id, "id_department" => $id), "user_departments");
        }
    }

    public function saveUserIndustries($data) {
        foreach ((array) $data as $id) {
            $this->delete("id_user = {$this->_user->id} AND id_industry = {$id}", "user_industries");
            $this->insert(array("id_user" => $this->_user->id, "id_industry" => $id), "user_industries");
        }
    }

    public function saveUserSkills($data) {
        foreach ((array) $data as $id) {
            $this->delete("id_user = {$this->_user->id} AND id_skill = {$id}", "user_skills");
            $this->insert(array("id_user" => $this->_user->id, "id_skill" => $id), "user_skills");
        }
    }

    public function getSeoTitlesFront($page_type, $object_type) {
//        $session = new Zend_Session_Namespace("active_subdomain");
//        if ($session->active_subdomain > 0) {
//            $id_subdomain = $session->active_subdomain;
//        }
//
//        $sql = $this->select()
//                ->from(array("s" => "seo"))
//                ->join(array("sl" => "seo_lang"), "s.id = sl.id_seo", array("value"))
//                ->where("sl.id_lang = ?", $this->id_lang)
//                ->where("s.page = ?", $page_type)
//                ->where("s.type = ?", $object_type)
//                ->where("s.id_subdomain = ?", (int) $id_subdomain);
//        $data = $this->fetchRow($sql);
//
//        if (empty($data)) {
//            $sql = $this->select()
//                    ->from(array("s" => "seo"))
//                    ->join(array("sl" => "seo_lang"), "s.id = sl.id_seo", array("value"))
//                    ->where("sl.id_lang = ?", $this->id_lang)
//                    ->where("s.page = ?", $page_type)
//                    ->where("s.type = ?", $object_type);
//            $data = $this->fetchRow($sql);
//
//            return (string) $data['value'];
//        } else {
//            return (string) $data['value'];
//        }
    }

//    public function getImageAltsFront($object_type) {
//        $session = new Zend_Session_Namespace("active_subdomain");
//        if ($session->active_subdomain > 0) {
//            $id_subdomain = $session->active_subdomain;
//        }
//
//        $sql = $this->select()
//                ->from(array("i" => "image_alts"))
//                ->join(array("il" => "image_alts_lang"), "i.id = il.id_image_alt", array("value"))
//                ->where("il.id_lang = ?", $this->id_lang)
//                ->where("i.type = ?", $object_type)
//                ->where("i.id_subdomain = ?", (int) $id_subdomain);
//        $data = $this->fetchRow($sql);
//
//        if (empty($data)) {
//            $sql = $this->select()
//                    ->from(array("i" => "image_alts"))
//                    ->join(array("il" => "image_alts_lang"), "i.id = il.id_image_alt", array("value"))
//                    ->where("il.id_lang = ?", $this->id_lang)
//                    ->where("i.type = 1");
//
//            $data = $this->fetchRow($sql);
//
//            return (string) $data['value'];
//        } else {
//            return (string) $data['value'];
//        }
//    }

    public function getImageAltFront($key) {
//        $session = new Zend_Session_Namespace("active_subdomain");
//        if ($session->active_subdomain > 0) {
//            $id_subdomain = $session->active_subdomain;
//        }
//
//        $sql = $this->select()
//                ->from(array("i" => "image_alts"))
//                ->join(array("il" => "image_alts_lang"), "i.id = il.id_image_alt", array("value"))
//                ->where("il.id_lang = ?", $this->id_lang)
//                ->where("i.photo = ?", $key)
//                ->where("i.id_subdomain = ?", (int) $id_subdomain);
//        $data = $this->fetchRow($sql);
//
//        if (empty($data)) {
//            return "here";
//            $sql = $this->select()
//                    ->from(array("i" => "image_alts"))
//                    ->join(array("il" => "image_alts_lang"), "i.id = il.id_image_alt", array("value"))
//                    ->where("il.id_lang = ?", $this->id_lang)
//                    ->where("i.photo = 'default'");
//
//            $data = $this->fetchRow($sql);
//
//            return (string) $data['value'];
//        } else {
//            return (string) $data['value'];
//        }
    }

    public function getCountryByIp($ip) {
        $sql = $this->select()->from(array("g" => "geoip"))->where("ip_long_start <= ?", $ip)->where("ip_long_end >= ?", $ip);
        $data = $this->fetchRow($sql);

        return (string) $data['country_code'];
    }

    public function getHobbiesSelect() {
        $sql = $this->select()->from(array("h" => "hobbies"), array("id", "title"));
        return $this->fetchPairs($sql);
    }

    public function getGamificationPage() {
        $sql = $this->select()
                ->from(array("p" => "pages"))
                ->join(array("pl" => "pages_lang"), "p.id = pl.id_page", array("content", "title"))
                ->where("pl.id_lang = ?", $this->id_lang)
                ->where("p.tagname = 'gamification'");
        return $this->fetchRow($sql);
    }

    public function searchSkills($key) {
        $sql = $this->select()
                ->from(array("s" => "skills"))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title"))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("sl.title LIKE '%{$key}%'");
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $skills[] = $row['id'];
        }

        return (array) $skills;
    }

    public function searchSkillsAjax($key) {
        $sql = $this->select()
                ->from(array("s" => "skills"))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title"))
                ->where("sl.id_lang = ?", $this->id_lang)
                ->where("sl.title LIKE '{$key}%'");
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $skills[$row['id']] = $row['title'];
        }

        return (array) $skills;
    }

    public function getSalaryJobTitles() {
        $sql = $this->select()->from(array("s" => "salary_job_titles"), array("id", "title"))->order("title ASC");
        return $this->fetchPairs($sql);
    }

    public function sendContact($data) {
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $mail = new WebApp_Mail();
        $mail->addTo("adrian.vasilescu@brainspotting.ro", "Adi");
        $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_settings['sitename']);
        $mail->setSubject("Demo contact form");
        $mail->setBodyFromViewScript("contact.phtml", array("data" => $data, "settings" => $this->_settings));
        $mail->send($transport);
    }

    public function sendReminder($data) {

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $modelsCompany = new models_Company;
        $company = $modelsCompany->getCompanyById($this->_user->id_company);

        $email = $this->getEmailByType(23);
        $content = $this->_personalize($email['content'], array("message" => $data['message']));

        $smarty = new Smarty;
        $smarty->assign("message", $data['message']);
        $smarty->assign("settings", $this->_settings);

        $emailBody = $smarty->fetch('eval:' . $email['content']);
        $emailSubject = $smarty->fetch('eval:' . $email['subject']);

        $mail = new WebApp_Mail;
        $mail->addTo($data['email']);
        $mail->setFrom($company['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        if ($data['subject'] == "") {
            $mail->setSubject($emailSubject);
        } else {
            $mail->setSubject($data['subject']);
        }

        $mail->setBodyHtml($emailBody);
        $mail->send($transport);
    }

    public function sendBoost($data) {
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $modelsCompany = new models_Company;
        $company = $modelsCompany->getCompanyById($this->_user->id_company);

        $email = $this->getEmailByType(24);
        $content = $this->_personalize($email['content'], array("message" => $data['message']));

        $mail = new WebApp_Mail;
        $mail->addTo("jeddey.seifeddine@gmail.com");
        $mail->setFrom($company['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject("BoostCampaign");
        $mail->setBodyFromHTML($content);
        $mail->send($transport);
    }

    public function getEmailByType($type) {
        $sql = $this->select()
                ->from(array("e" => "emails"))
                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject", "content","id_email"))
                ->where("e.type = ?", (int) $type)
                ->where("e.status = 1");
        if (Zend_Auth::getInstance()->getIdentity()->type === 3) {
            $sql->where("e.id_company =?", (int) $_POST['id_company']);
        } else {
            if (isset($_POST['id_company']) && $_POST['id_company'] != "") {
                $sql->where("e.id_company =?", (int) $_POST['id_company']);
            } else {
                if(!Zend_Auth::getInstance()->hasIdentity())
                {
                    $id_company = $this->_modelCompany->getCompanyBySubdomainSlug($this->_settings['sitename'])['id'];
                    $sql->where("e.id_company =?", (int) $id_company);
                } else
                {
                    $sql->where("e.id_company =?", (int) Zend_Auth::getInstance()->getIdentity()->id_company);
                }
            }
        }
        $data = $this->fetchRow($sql);
        return (array) $data;
    }
    public function getEmailByTypeCompany($type,$id_company) {
        $sql = $this->select()
                ->from(array("e" => "emails"))
                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject", "content"))
                ->where("e.type = ?", (int) $type);
            $sql->where("e.id_company =?", (int) $id_company);

        $data = $this->fetchRow($sql);
        return (array) $data;
    }

    public function setUserDepartment($id, $id_department) {
        $data['id_user'] = $id;
        $data['id_department'] = $id_department;
        $this->insert($data, "user_departments");
    }

    public function setCustomDepartment($data) {
        $this->insert($data, "custom_departments");
        return $this->lastInsertId();
    }

    public function getEmails() {
        $sql = $this->select()
                ->from(array("e" => "emails"))
                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject"))
                ->where("el.id_lang = ?", $this->id_lang);
        if ($this->_user->type == 2) {
            $sql->where("e.id_company = ?", $this->_user->id_company);
        }
        return $this->fetchAll($sql);
    }

    public function getEmailByIdEdit($id) {
        $sql = $this->select()
                ->from(array("e" => "emails"))
                ->join(array("el" => "emails_lang"), "e.id = el.id_email", array("title", "subject", "id_lang", "content"))
                ->where("e.id = ?", (int) $id)
                ->where("el.id_lang = ?", $this->id_lang);
        return $this->fetchRow($sql);
    }

    public function saveEmail($data) {
        if ($this->_user->type == 2) {
            $data['id_company'] = $this->_user->id_company;
        }
        $data['title'] = "";
        $email['id_company'] = $data['id_company'];
        $email['status'] = $data['status'];
//        $email['frequency'] = $data['frequency'];
        $email['direction'] = $data['direction'];
//        $email['email'] = $data['email'];
//        $email['from'] = $data['from'];

        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);
            $this->update($email, "id = {$id}", "emails");
            $this->delete("id_email = '{$id}' AND id_lang = '{$this->id_lang}'", "emails_lang");
        } else {
            $this->insert($email, "emails");
            $id = $this->lastInsertId();
        }
        $this->insert(array("title" => $data['title'], "content" => $data['content'], "id_email" => $id, "id_lang" => $this->id_lang, "subject" => $data['subject']), "emails_lang");

        return $id;
    }

    public function getDashboardStyles() {
        $sql = $this->select()
                ->from(['ds' => "dashboard_styles"], ['*']);
        return $this->fetchAll($sql);
    }

    public function addStyle($db, $style) {
        $this->update(['style' => $style['value']], "target = '{$db}'", "dashboard_styles");
    }

    public function buildStyles() {
        $sql = $this->select()->from(['db' => "dashboard_styles"], ['db.target', 'db.style']);
        $styles = $this->fetchAll($sql);
        return $styles;
    }

    public function updateShareimage($file_name) {
        $this->update(["logo" => $file_name], 'id = ' . (int) $this->_user->id_company, "companies");
    }

    public function updateLandingimage($file_name) {
        $this->update(["landing" => $file_name], 'id = ' . (int) $this->_user->id_company, "companies");
    }

    public function updateSocial($key, $value) {
        $this->update([$key => $value], 'id = ' . (int) $this->_user->id_company, "companies");
    }

    public function getRecruiterSelect($id_country = null, $cities = null) {
        $sql = $this->select()->from(array("u" => "users"), array("u.id", "CONCAT(u.lastname, ' ', u.firstname)"))
                ->where("u.id_company =?",(int) Zend_Auth::getInstance()->getIdentity()->id_company)
                ->where("u.type =?",(int) 4);


        return $this->fetchPairs($sql);
    }

    public function getApplicationByDateAndCities($start_date,$end_date)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['count(id) as applications'])
            ->where('ja.date <=?',$start_date)
            ->where('ja.date >=?',$end_date);
//        die($sql);
        return $this->fetchRow($sql);
    }
    public function getBoxesContent()
    {
        $sql = $this->select()
            ->from(['c' => 'companies'],[])
            ->join(['j' => 'jobs'],"j.id_company=c.id",[])
            ->join(['ja' => 'job_applications'],"j.id=ja.id_job",['count(ja.id) as applicants'])
            ->where('ja.status = 1') // new status
            ->group('ja.id_user');
        return $this->fetchRow($sql);

    }

    public function getCountrySelect($id_country = null)
    {
        $sql = $this->select()->from(array("c" => "countries"), array("id", "title"))
            ->order("title ASC");
        if (null !== $id_country) {
            if (is_array($id_country)) {
                $sql->where("id_country IN (" . implode(",", $id_country) . ")");
            } else {
                $sql->where("id_country = ?", $id_country);
            }
        }


        return $this->fetchPairs($sql);
    }

    public function getCountiesByCountryId($id_country = null)
    {
        $sql = $this->select()
            ->from(['c'=> 'counties'],array('id','title'))
            ->where('c.id_country=?',$id_country);
        return $this->fetchPairs($sql);
    }

    public function getCountiesByCountryByArray($id_country = array())
    {
        $sql = $this->select()
            ->from(['c'=> 'counties'],array('id','title'))
            ->where("c.id_country IN (" . implode(",", $id_country) . ")");
        return $this->fetchPairs($sql);
    }

    public function getCitiesByCountyId($id_county = null)
    {
        $sql = $this->select()
            ->from(['c' => 'cities'],array('id','title'))
            ->where('c.id_county=?',(int) $id_county);
        return $this->fetchPairs($sql);
    }
    public function getCitiesByCountyByList($id_counties = array())
    {
        $sql = $this->select()
            ->from(['c' => 'cities'],array('id','title'))
            ->where("c.id_county IN (" . implode(",", $id_counties) . ")");
        return $this->fetchPairs($sql);
    }

    public function getSources()
    {
        $sql = $this->select()
            ->from(['s' => 'social_media'],['id','title']);
        $result =  $this->fetchPairs($sql);
//        array_walk_recursive($result, function(&$value) {
//            $value = strtolower($value);
//        });
        $sources = [];
        foreach($result as $key => $res)
        {
            $sources[] = ['id' => $key,'title' => $res];
        }

        array_unshift($sources,['id' => 0 , 'title' => 'Source' ]);
        return $sources;
    }


    public function getSubDepartmentsByDepartmentId($id_department)
    {
        $sql = $this->select()
            ->from(['sd' => 'sub_departments'],['id','title'])
            ->where('sd.id_department =?',(int) $id_department);
        return $this->fetchPairs($sql);
    }

    public function getSubDepartmentsByDepartmentList($id_departments = array())
    {
        $sql = $this->select()
            ->from(['sd' => 'sub_departments'],['id','title'])
            ->where("sd.id_department IN (" . implode(",", $id_departments) . ")");
        return $this->fetchPairs($sql);
    }

    public function getSubSubDepartmentsByDepartmentId($id_sub_department)
    {
        $sql = $this->select()
            ->from(['sd' => 'sub_sub_departments'],['id','title'])
            ->where('sd.id_sub_department =?',(int) $id_sub_department);
        return $this->fetchPairs($sql);
    }

    public function getSubSubDepartmentsByDepartmentByList($id_sub_departments = array())
    {
        $sql = $this->select()
            ->from(['sd' => 'sub_sub_departments'],['id','title'])
            ->where("sd.id_sub_department IN (" . implode(",", $id_sub_departments) . ")");
        return $this->fetchPairs($sql);
    }

    public function getSubDepartmentsSelectByIds($sub_department)
    {
        $sub_department= (array) $sub_department;
        $sql = $this->select()->from(array("sd" => "sub_departments"), array("id", "title"))
            ->order("title ASC");
        if(!empty($sub_department))
        {
            $sql->where("sd.id_department IN (" . implode(",", $sub_department) . ")");
        }
        return $this->fetchPairs($sql);
    }

    public function getSubSubDepartmentsSelectByIds($sub_sub_department)
    {
        $sub_sub_department = (array) $sub_sub_department;
        $sql = $this->select()->from(array("ssd" => "sub_sub_departments"), array("id", "title"))
            ->order("title ASC");
        if(!empty($sub_sub_department))
        {
            $sql->where("ssd.id_sub_department IN (" . implode(",", $sub_sub_department) . ")");
        }
        return $this->fetchPairs($sql);
    }

    public function getCitiesFromSelected($cities)
    {
        $cities = (array) ($cities);
        $sql = $this->select()
            ->from(['c' => 'cities'],['id','title']);
        if(!empty($cities))
        {
            $sql->where("c.id_county IN (" . implode(",", $cities) . ")")->order("title ASC");
        }
        $result = $this->fetchPairs($sql);
        return $result ;
    }

    public function getProjectSelect()
    {
        $sql = $this->select()
            ->from(['p' => 'projects'],['id','title']);
        return  $this->fetchPairs($sql);
    }

    public function disableBouncedUsers($users)
    {
        $model = new models_User ;
        foreach($users as $index => $user)
        {
            $email = $user['email'];
            $timestamp = $user['timestamp'];
            $bounced_date = date('Y-m-d H:i:s',$timestamp);
            $data = [
              'status' => 0,
              'bounced' => 1,
              'bounce_date' => $bounced_date
            ];
            if($model->getUserDataByEmail($email)) $this->update($data,'email='.$email,'users');
        }
    }
    protected function _fetch(Zend_Db_Table_Select $select)
    {
        return parent::_fetch($select); // TODO: Change the autogenerated stub
    }

    public function getRatingSelect()
    {
        $sql = $this->select()
            ->from(['r' => 'rating'],['id','title']);
        return $this->fetchPairs($sql);
    }

    public function generateExport($start,$end)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'],['j.id as id_job','DATE_FORMAT(j.created_at," %d-%m-%Y") as job_created_at',
                'DATE_FORMAT(j.start," %d-%m-%Y") as job_start'])
//            ->from(['ja' => 'job_applications'],[
//                'ja.id_user',
//                "(SELECT COUNT(ja.id) from job_applications as ja WHERE j.id=ja.id_job) as candidates_no",
//                "(SELECT COUNT(jash.id) from job_application_status_history as jash where j.id=jash.id_job
//                AND jash.status IN(7,14,9,17,19) ) as shortlisted_candidates_no",
//                "(SELECT COUNT(jashh.id) from job_application_status_history as jashh where j.id=jashh.id_job
//                AND jashh.status=8) as test_candidates_no",
//                "(SELECT COUNT(jashhh.id) from job_application_status_history as jashhh where j.id=jashhh.id_job
//                AND jashhh.status IN(7,14) ) as interview_candidates_no",
//                "(SELECT COUNT(jashhhh.id) from job_application_status_history as jashhhh where j.id=jashhhh.id_job
//                AND jashhhh.status=9) as offer_candidates_no"
//            ])
            ->join(['jl' => 'jobs_lang'],'jl.id_job=j.id',['jl.title as job_title'])
//            ->join(['j' => 'jobs'],'ja.id_job=j.id',['j.id as id_job','DATE_FORMAT(j.created_at," %d %b %Y") as job_created_at',
//                'DATE_FORMAT(j.start," %d %b %Y") as job_start'])
            ->joinLeft(['jp' => 'job_projects'],'jp.id_job=j.id',[])
            ->joinLeft(['p' => 'projects'],'p.id=jp.id_project',['p.title as project_title'])
            ->joinLeft(['jh' => 'job_headcount'],'jh.id_job=j.id',['jh.headcount as vacancies'])
            ->joinLeft(['jd' => 'job_departments'],'jd.id_job=j.id',[])
            ->joinLeft(['dl' => 'departments_lang'],'dl.id_department=jd.id_department',['dl.title as department'])
            ->joinLeft(['jsd' => 'job_sub_departments'],'jsd.id_job=j.id',[])
            ->joinLeft(['sd' => 'sub_departments'],'sd.id=id_sub_department',['sd.title as sub_department'])
            ->joinLeft(['jssd' => 'job_sub_sub_departments'],'jssd.id_job=j.id',[])
            ->joinLeft(['ssd' => 'sub_sub_departments'],'ssd.id=jssd.id_sub_sub_department',['sd.title as sub_sub_department'])
            ->joinLeft(['jr' => 'recruiter_job'],'jr.id_job=j.id',[])
            ->joinLeft(['u' => 'users'],'u.id=jr.id_user',["CONCAT(u.firstname, ' ', u.lastname) as assigned_recruiter"])
            ->joinLeft(['ja' => 'job_applications'],'ja.id_job=j.id',['ja.id_user'])
            ->joinLeft(['jc' => 'job_cities'],'jc.id_job=j.id',[])
            ->joinLeft(['ct' => 'cities'],'ct.id=jc.id_city',['ct.title as job_city'])
            ->group(["j.id","ja.id_user"])
            ->where('ja.date >=?',$start)
            ->where('ja.date <=?',$end)
//            ->where('j.id_company =?',(int) Zend_Auth::getInstance()->getIdentity()->id_Company)
        ;
        $result =  $this->fetchAll($sql);
//        echo '<pre>',print_r($result,1),'</pre>';die;
        foreach($result as $index => &$job)
        {
            $id_candidates = [] ;
            $id_job = $job['id_job'];
            $id_user = $job['id_user'];
            $result[$index]['offer_date'] = $this->collectStatusDateByJobAndStatus($id_job,$id_user,9,$start,$end)['date'];
            $result[$index]['offer_accepted_dates'] = $this->collectStatusDateByJobAndSubStatus($id_job,$id_user,31,$start,$end)['date'];
            $result[$index]['hire_dates'] = $this->collectStatusDateByJobAndStatus($id_job,$id_user,5,$start,$end)['date'];
            $result[$index]['offer_rejectedd_dates'] = $this->collectStatusDateByJobAndSubStatus($id_job,$id_user,32,$start,$end)['date'];

            $candidates_no = $this->CountCandidateApplicationNumber($id_job,$id_user,$start,$end)['candidates_no'];
            $candidate_application_date = $this->getApplicationDate($id_job,$id_user);
            $candidate_hiring_date = $this->getHiringDate($id_job,$id_user);
//            echo $candidate_hiring_date;die;
            $recommender=$this->getCandidateRecommender($id_job,$id_user);
            $candidate_recommender = strlen($recommender['recommender'])>0? $recommender['recommender'] :"-";
            $recommender_job_share_date = strlen($recommender['share_date'])>0? $recommender['share_date'] :"-";
            $chn=$recommender['share_channel_id'];
            $candidate_id_application=$recommender['id_application'];
            $type=$recommender['type'];


            $targeted=array(8,15,18);
            $extended=array(5,6,7,9,14,16,17);
            $source='non-referral';
            if (in_array($chn, $targeted) and $type == 1){
                $source ='targeted referral';
            } elseif (in_array($chn, $extended)  and $type == 1){
                $source ='extended referral';
            }

            $candidate_recommender_email = strlen($this->getCandidateRecommenderEmail($id_job,$id_user))>0? $this->getCandidateRecommenderEmail($id_job,$id_user) :"-";
            $candidate_info = $this->getCandidateInfo($id_job,$id_user);

            $recommender_job_id_share = strlen($recommender['id_share'])>0? $recommender['id_share'].'_'.$id_job.'_'.$id_user.'_'.$candidate_application_date :"-";

//            echo $candidate_info['candidate_email'].'=email<br>'. $candidate_info['linkedin_link'].'=link<br>'.$id_job.'=id job<br>'.$id_user.'=id user'.'<br>status='.$candidate_info['status_title'].'<br>sub-status='.$candidate_info['sub_status_title'];
//            die;
//	        echo $candidate_application_date.' id_job='.$id_job.' id_user='.$id_user;die;
            $slc=$this->getShortlistedCandidates($id_job,$id_user,[7,14,9,17,19],$start,$end)['shortlisted_candidates_no'];
            $shortlisted_candidates_no = (isset($slc)? $slc :"-");
            $test_candidates_no = $this->getTestedCandidate($id_job,$id_user,8,$start,$end)['test_candidates_no'];
            $interview_candidates_no = $this->getInterviewCandidate($id_job,$id_user,[7,14],$start,$end)['interview_candidates_no'];
            $offer_candidates_no = $this->getOfferCandidate($id_job,$id_user,9,$start,$end)['offer_candidates_no'];

            $result[$index]['share_channel'] = $recommender['share_channel_name'] ;
            $result[$index]['candidates_no'] = ($candidates_no  > 0) ? 1 : 0 ;
            $result[$index]['candidates_application_date'] = (isset($candidate_application_date))?$candidate_application_date:"-"  ;
            $result[$index]['candidate_hiring_date'] = $candidate_hiring_date;
            $result[$index]['candidate_email'] = (strlen($candidate_info['candidate_email'])>0)?$candidate_info['candidate_email']:"-" ;
            $result[$index]['candidate_linkedin'] = (strlen($candidate_info['linkedin_link'])>0)?$candidate_info['linkedin_link']:"-" ;
            $result[$index]['candidate_current_status'] = (strlen($candidate_info['status_title'])>0)?$candidate_info['status_title']:"-" ;
            $result[$index]['candidate_current_sub_status'] = (strlen($candidate_info['sub_status_title'])>0)?$candidate_info['sub_status_title']:"-" ;
            $result[$index]['recommender_job_id_share'] = $recommender_job_id_share ;
            $result[$index]['recommender_job_share_date'] = $recommender_job_share_date ;
            if ($chn=$recommender['share_channel_id'] == 18){
                $result[$index]['recommender_job_share_date'] = $result[$index]['candidates_application_date'];
                $result[$index]['recommender_job_id_share']=strlen($recommender['id_share'])>0? $recommender['id_share'].'_'.$id_job.'_'.$id_user.'_'.$candidate_application_date :$candidate_id_application.'_'.$id_job.'_'.$id_user.'_'.$candidate_application_date;
            }
            $result[$index]['candidate_recommender'] = $candidate_recommender ;
            $result[$index]['recommender_email'] = $candidate_recommender_email ;
            $result[$index]['source_type'] = $source ;
            $result[$index]['shortlisted_candidates_no'] = ($shortlisted_candidates_no  > 0) ? 1 : 0 ;
            $result[$index]['test_candidates_no'] = ($test_candidates_no  > 0) ? 1 : 0 ;
            $result[$index]['interview_candidates_no'] = ($interview_candidates_no  > 0) ? 1 : 0 ;
            $result[$index]['offer_candidates_no'] = ($offer_candidates_no > 0) ? 1 : 0 ;
            $candidate_info = $this->collecetUserInfo($id_user,$id_job);
//            echo '<pre>',print_r($candidate_info,1),'</pre>';die;
            foreach($candidate_info as $key => $value)
            {
                $result[$index][$key] = $value;

            }
            uksort($result[$index],function ($a,$b){
                $order = [
                    "department" => 0,
                    "sub_department" => 1,
                    "sub_sub_department" => 2,
                    "project_title" => 3,
                    "job_title" => 4,
                    "id_job" => 5,
                    "vacancies" => 6,
                    "assigned_recruiter" => 7,
                    "job_created_at" => 8,
                    "job_start" => 9,
                    "candidates_no" => 10,
                    "shortlisted_candidates_no" => 11,
                    "test_candidates_no" => 12,
                    "interview_candidates_no" => 13,
                    "offer_candidates_no" => 14,
                    "offer_date" => 15,
                    "offer_accepted_dates" => 16,
                    "offer_rejectedd_dates" => 17,
                    "hire_dates" => 18,
                    "candidates_application_date" => 19,
                    "firstname" => 20,
                    "lastname" => 21,
                    "current_position" => 22,
                    "company" => 23 ,
                    "no_month_in_current_position" => 24,
                    "main_industry" => 25,
                    "2nd_industry" => 26,
                    "main_field_of_experience" => 27,
                    "no_of_years_in_the_main_field" => 28,
                    "2nd_field_of_experience" => 29,
                    "no_of_years_in_the_2nd_field" => 30,
                    "3nd_field_of_experience" => 31,
                    "no_of_years_in_the_3nd_field" => 32,
                    "previous_job_name" => 33,
                    "main_field_of_education" => 34,
                    "2nd_field_of_education" => 35,
                    "level_of_education" => 36,
                    "1st_foreign_language" => 37,
                    "2nd_foreign_language" => 38,
                    "source_of_cv" => 39,
                    "gender" => 40,
                    "time_to_offer" => 41,
                    "time_to_hire" => 42,
                    "id_user" => 43,
                    "title" => 44,
                    "id_user_positions" => 45,
                    "candidate_current_status" => 46,
                    "candidate_current_sub_status" => 47,
                    "candidate_hiring_date" => 48,
                    "candidate_email" => 49,
                    "candidate_linkedin" => 50,
                    "recommender_job_id_share" => 51,
                    "recommender_job_share_date" => 52,
                    "candidate_recommender" => 53,
                    "recommender_email" => 54,
                    "source_type" => 55,
                    "share_channel" => 56,
                ];
                return $order[$a] - $order[$b];
            });
//            function cmp($a, $b) {
//                $order = Array( 'LOW' => 0, 'MEDIUM' => 1, 'HIGH' => 2 );
//                return $order[$a] - $order[$b];
//            }

//            $job['candidates_no']  = ($job['candidates_no'] > 0) ? 1 : 0 ;
//            $job['shortlisted_candidates_no']  = ($job['shortlisted_candidates_no'] > 0) ? 1 : 0 ;
//            $job['test_candidates_no']  = ($job['test_candidates_no'] > 0) ? 1 : 0 ;
//            $job['interview_candidates_no']  = ($job['interview_candidates_no'] > 0) ? 1 : 0 ;
//            $job['offer_candidates_no']  = ($job['offer_candidates_no'] > 0) ? 1 : 0 ;
//
//            $ids = WebApp_Tools::collectValuesByKey('id_user',$result[$index]['offer_date']);
//            if(!empty($ids)) $id_candidates[] = $ids;
//            $ids = WebApp_Tools::collectValuesByKey('id_user',$result[$index]['offer_accepted_dates']);
//            if(!empty($ids)) $id_candidates[] = $ids;
//            $ids = WebApp_Tools::collectValuesByKey('id_user',$result[$index]['hire_dates']);
//            if(!empty($ids)) $id_candidates[] = $ids;
//            $ids = WebApp_Tools::collectValuesByKey('id_user',$result[$index]['offer_rejectedd_dates']);
//            if(!empty($ids)) $id_candidates[] = $ids;
//            $mappedCandidates = WebApp_Tools::mapMuliToAssoc($id_candidates);
//            if(!empty($mappedCandidates))
//            {
//                foreach($mappedCandidates as $user)
//                {
//                    $result[$index]['candidates'][] =  $this->collecetUserInfo($user,$id_job);
//                }
//            }


        }
        foreach($result as &$res)
        {
            foreach($res as $key => &$value)
            {
                if($value == "" && !is_int($value)) $value = "-";
            }
        }
        return $result;
    }

    public function collectStatusDateByJobAndStatus($id_job,$id_user,$id_status,$start,$end)
    {
        $sql = $this->select()
            ->from(['jahs' => 'job_application_status_history'],['DATE_FORMAT(jahs.date," %d-%m-%Y") as date'])
            ->join(['u' => 'users'],'u.id=jahs.id_user',["CONCAT(u.firstname, ' ', u.lastname) as candidate",
                "u.id as id_user"])
            ->where("jahs.date >=?", $start)
            ->where("jahs.date <=?", $end)
            ->where("jahs.id_job =?",(int) $id_job)
            ->where("jahs.id_user =?",(int) $id_user)
            ->where("jahs.status =?",(int) $id_status);
        return $this->fetchrow($sql);
    }

    public function collectStatusDateByJobAndSubStatus($id_job,$id_user,$id_status,$start,$end)
    {
        $sql = $this->select()
            ->from(['jahs' => 'job_application_status_history'],['DATE_FORMAT(jahs.date," %d-%m-%Y") as date'])
            ->join(['u' => 'users'],'u.id=jahs.id_user',["CONCAT(u.firstname, ' ', u.lastname) as candidate",
                "u.id as id_user"])
            ->where("jahs.date >=?",$start)
            ->where("jahs.date <=?",$end)
            ->where("jahs.id_job =?",(int) $id_job)
            ->where("jahs.id_user =?",(int) $id_user)
            ->where("jahs.status_reason =?",(int) $id_status);
        return $this->fetchrow($sql);
    }

    public function collecetUserInfo($id_user,$id_job)
    {
        $id_user = (int) $id_user;
        $sql = $this->select()
            ->from(['u' => 'users'],['u.firstname','u.lastname','u.id as id_user','gender']);
        $sql->joinLeft(['up' => 'user_positions'],'up.id_user=u.id',['up.title','up.company',
            'up.id as id_user_positions']);
        $sql->where('u.id =?',$id_user);
//        $sql->where('up.is_current=?',1);
        $user =  $this->fetchRow($sql);
        $user['firstname']=isset($user['firstname'])? $user['firstname'] : "-";
        $user['lastname']=isset($user['lastname'])? $user['lastname'] : "-";
        $user['gender']=isset($user['gender'])? $user['gender'] : "-";
        $user['title']=isset($user['title'])? $user['title'] : "-";
        $user['company']=isset($user['company'])? $user['company'] : "-";
        $user['no_month_in_current_position'] = $this->calculateCurrentPositionMonths($user['id_user_positions']);
        $user['id_user_positions'] = $this->calculateCurrentPositionMonths($user['id_user_positions']);
        $industires = $this->calculateUserPosition($user['id_user']);
        $user['main_industry'] = (isset($industires[0])) ? $industires[0]  : "-" ;
        $user['2nd_industry'] = (isset($industires[1])) ? $industires[1]  : "-" ;
        $skills = $this->calculateSkills($user['id_user']);
        $user['main_field_of_experience'] = (isset($skills[0]['title'])) ? $skills[0]['title']  : "-" ;
        $user['no_of_years_in_the_main_field'] = (isset($skills[0]['skill_years'])) ? round($skills[0]['skill_years'])  : "-" ;
        $user['2nd_field_of_experience'] = (isset($skills[1]['title'])) ? $skills[1]['title']  : "-" ;
        $user['no_of_years_in_the_2nd_field'] = (isset($skills[1]['skill_years'])) ? round($skills[1]['skill_years'])  : "-" ;
        $user['3nd_field_of_experience'] = (isset($skills[2]['title'])) ? round($skills[2]['title'])  : "-" ;
        $user['no_of_years_in_the_3nd_field'] = (isset($skills[2]['skill_years'])) ? round($skills[2]['skill_years'])  : "-" ;
        $previous_last_position = $this->getPreviousLastJobs($user['id_user'])[1]['title'];
        $user['previous_job_name'] = (isset($previous_last_position)) ? $previous_last_position  : "-"  ;
        $educations = $this->getEducations($user['id_user']);
        $user['main_field_of_education'] = (isset($educations[0]['title'])) ? $educations[0]['title']  : "-" ;
        $user['2nd_field_of_education'] = (isset($educations[1]['title'])) ? $educations[1]['title']  : "-" ;
        $user['level_of_education'] = (isset($educations[0]['field'])) ? $educations[0]['field']  : "-" ;
        $languages = $this->getUserLanguages($user['id_user']);
        $user['1st_foreign_language'] = (isset($languages[0]['language'])) ? $languages[0]['language']  : "-" ;
        $user['2nd_foreign_language'] = (isset($languages[1]['language'])) ? $languages[1]['language']  : "-" ;
        $user['source_of_cv'] = (isset($this->getApplicationSource($id_user,$id_job)['title'])) ?
            $this->getApplicationSource($id_user,$id_job)['title'] : "-";
        $user['gender'] = ($user['gender'] ==='M') ? "Male" : "Female";
        $tto=$this->caculateDaysBetweenJobApplicationDateAndHistoryStatus($id_job,$user['id_user'],9);
        $tth=$this->caculateDaysBetweenJobApplicationDateAndHistoryStatus($id_job,$user['id_user'],5);
        $user['time_to_offer'] = (isset($tto)) ? $tto : "-" ;
        $user['time_to_hire'] = (isset($tth)) ? $tth : "-" ;
        return $user;

    }

    public function calculateCurrentPositionMonths($id_position)
    {
        $id_position = (int) $id_position ;
        $sql = $this->select()
            ->from(['up' => 'user_positions'],['*'])
            ->where("up.id =?",$id_position);
        $result = $this->fetchRow($sql);
        $d1 = new DateTime(date("Y-m-d"));
        $d2 = new DateTime($result['start']);
        $diff = $d2->diff($d1);
        $months = round($diff->days/30);
        return $months;
    }

    public function calculateUserPosition($id_user)
    {
        $model_user = new models_User;
        $positions = $model_user->getUserPositions($id_user);
        $companies = [];
        $industries = [];
        foreach ($positions as $index => &$position) {
            $d1 = new DateTime($position['end']);
            $d2 = new DateTime($position['start']);
            $diff = $d2->diff($d1);
            $positions[$index]['years'] = $diff->y;
            if ($position['is_current']) {
                $d1 = new DateTime(date('Y-m-d'));
                $diff = $d2->diff($d1);
                $positions[$index]['years'] = $diff->y;
                $positions[$index]['end'] = date('Y-m-d');
            }
            $industries[$position['id_industry']]['id_industry'] = $position['id_industry'];
            $industries[$position['id_industry']]['id_user'] = $position['id_user'];
            $industries[$position['id_industry']]['years'] = $position['years'];

        }
        usort($positions,  function($a, $b) {
            return $a['years'] < $b['years'];
        });
        usort($industries,  function($a, $b) {
            return $a['years'] < $b['years'];
        });

        $ind = [];
        foreach ($industries as $index => $industry) {
            $sql = $this->select()
                ->from(['il' => 'industries_lang'],['il.title','il.id'])
                ->where('il.id =?',$industry['id_industry']);
            $ind[] = $this->fetchRow($sql)['title'];
        }
        return $ind;
//        echo '<pre>';
//        print_r($industries);
//        print_r($ind);
//        print_r($positions);
//        die;
    }

    public function calculateSkills($id_user)
    {
        $model_user = new models_User ;
        $result = $model_user->getUserSkillsWithRating($id_user);
        usort($result,  function($a, $b) {
            return $a['skill_years'] < $b['skill_years'];
        });
        return $result;
    }

    public function getPreviousLastJobs($id_user)
    {
        $model_user = new models_User ;
        $result = $model_user->getUserPositions($id_user);
        return $result;
    }

    public function getEducations($id_user)
    {
        $model_user = new models_User ;
        $educations  = $model_user->getUserEducation($id_user);
        foreach ($educations as $index => &$education)
        {
            $d1 = new DateTime($education['end']);
            $d2 = new DateTime($education['start']);
            $diff = $d2->diff($d1);
            $educations[$index]['years'] = $diff->y;
            $educations[$index]['graduation'] = $education['end'];
        }
        return $educations ;
    }

    public function getUserLanguages($id_user)
    {
        $model_user =  new models_User ;
        $result = $model_user->getUserLanguages($id_user) ;
        return $result;
    }

    public function getApplicationSource($id_user,$id_job)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],[])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
        ->join(['sm' => 'social_media'],'sm.id=ja.source',['title']);
        $result = $this->fetchRow($sql);
        return $result;
    }

    public function getApplicationDate($id_job,$id_user)
    {
        $sql = $this->select()
	        ->from(['ja' => 'job_applications'],['ja.date'])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
        ;
	    $result = $this->fetchRow($sql);
	    $date = new DateTime($result['date']);
	    $d=$date->format('d-m-Y');
	    return $d;
    }


    public function getHiringDate($id_job,$id_user)
    {
        $sql = $this->select()
	        ->from(['ja' => 'job_applications'],['ja.hiring_date'])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
            ->where('ja.hiring_date is not null ')
        ;
	    $result = $this->fetchRow($sql);
	    if ($result['hiring_date'] >'2016-01-01'){
	        $date = new DateTime($result['hiring_date']);
            $d=$date->format('d-m-Y');
        } else {
            $d='-';
        }

	    return $d;
    }


    public function getCandidateRecommender($id_job,$id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['ja.id_recommender','ja.id as id_application'])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
            ->join(['u' => 'users'],'u.id=ja.id_recommender',['CONCAT(firstname," ",lastname) as recommender','u.type'] )
            ->joinLeft(['js' => 'jobs_share'],'js.id_user=ja.id_recommender and js.id_job = ja.id_job',['js.id as id_share','js.date as share_date','js.id_source as share_channel'] )
            ->joinLeft(['sm' => 'social_media'],'sm.id=ja.source',['sm.title as share_channel_name', 'sm.id as share_channel_id'] )
        ;
        $result = $this->fetchRow($sql);
        return $result;
    }


    public function getCandidateRecommenderEmail($id_job,$id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['ja.id_recommender'])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
            ->join(['u' => 'users'],'u.id=ja.id_recommender',['u.email as recommender_email'] )
        ;
        $result = $this->fetchRow($sql);
        return $result['recommender_email'];
    }



    public function getCandidateInfo($id_job,$id_user)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['ja.id_user'])
            ->where('ja.id_user =?',(int) $id_user)
            ->where('ja.id_job =?',(int) $id_job)
            ->join(['u' => 'users'],'u.id=ja.id_user',['u.email as candidate_email','u.linkedin_link as linkedin','ja.status as status','ja.status_reason as sub-status'])
            ->joinLeft(['s'=> 'status'], 'ja.status = s.id', ['s.status as status_title'])
            ->joinLeft(['so'=> 'status_options'], 'ja.status_reason = so.id and ja.status=so.id_status', ['so.status as sub_status_title'])
        ;
        $result = $this->fetchRow($sql);
        return $result;
    }


    public function caculateDaysBetweenJobApplicationDateAndHistoryStatus($id_job,$id_user,$status)
    {
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['ja.date'])
            ->where('ja.id_job =?',(int) $id_job)
            ->where('ja.id_user =?',(int) $id_user);
        $result = $this->fetchRow($sql);
        if(empty($result)) return '-' ;
        $sql = $this->select()->from(['jash' => 'job_application_status_history'],['jash.date'])
            ->where('jash.id_user =?',(int) $id_user)
            ->where('jash.id_job =?',(int) $id_job)
            ->where('jash.status =?',$status);
        $result_history = $this->fetchRow($sql);
        if(empty($result_history)) return '-';
        $d1 = new DateTime($result['date']);
        $d2 = new DateTime($result_history['date']);
        $diff = $d2->diff($d1);
        return $diff->days;
    }

    public function CountCandidateApplicationNumber($id_job,$id_user,$start,$end)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['count(ja.id) as candidates_no'])
            ->where('ja.date >=?',$start)
            ->where('ja.date <=?',$end)
            ->where('ja.id_job =?',(int) $id_job)
            ->where('ja.id_user =?',(int) $id_user);
        return $this->fetchRow($sql);
    }

    public function getShortlistedCandidates($id_job,$id_user,$status,$start,$end)
    {
        $sql = $this->select()->from(['jash' => 'job_application_status_history'],
            ['count(id) as shortlisted_candidates_no'])
            ->where('jash.date >=?',$start)
            ->where('jash.date <=?',$end)
            ->where('jash.id_job =?',(int) $id_job)
            ->where('jash.id_user =?',(int) $id_user);
        $sql->where("jash.status IN (" . implode(",", $status) . ")");
        return $this->fetchRow($sql);
    }

    public function getTestedCandidate($id_job,$id_user,$status,$start,$end)
    {
        $sql = $this->select()->from(['jash' => 'job_application_status_history'],
            ['count(id) as test_candidates_no'])
            ->where('jash.date >=?',$start)
            ->where('jash.date <=?',$end)
            ->where('jash.id_job =?',(int) $id_job)
            ->where('jash.id_user =?',(int) $id_user);
        $sql->where("jash.status =?",$status);
        return $this->fetchRow($sql);
    }

    public function getInterviewCandidate($id_job,$id_user,$status,$start,$end)
    {
        $sql = $this->select()->from(['jash' => 'job_application_status_history'],
            ['count(id) as interview_candidates_no'])
            ->where('jash.date >=?',$start)
            ->where('jash.date <=?',$end)
            ->where('jash.id_job =?',(int) $id_job)
            ->where('jash.id_user =?',(int) $id_user);
        $sql->where("jash.status IN (" . implode(",", $status) . ")");
        return $this->fetchRow($sql);
    }

    public function getOfferCandidate($id_job,$id_user,$status,$start,$end)
    {
        $sql = $this->select()->from(['jash' => 'job_application_status_history'],
            ['count(id) as offer_candidates_no'])
            ->where('jash.date >=?',$start)
            ->where('jash.date <=?',$end)
            ->where('jash.id_job =?',(int) $id_job)
            ->where('jash.id_user =?',(int) $id_user);
        $sql->where("jash.status =?",$status);
        return $this->fetchRow($sql);
    }

    public function getHiringManagers()
    {
        $sql = $this->select()
            ->from(['u' => 'users'],array('id',"CONCAT(u.firstname, ' ',u.lastname) AS name"))
            ->where("u.type = 5");
        return $this->fetchPairs($sql);
    }

    public function getJobTypes()
    {
        $sql = $this->select()->from(['jt' => 'job_types'],['id','title']);
        return $this->fetchPairs($sql);
    }

    public function getJobCareers()
    {
        $data = $this->_settings['career_levels'];
        return $data ;
    }

    public function getJobPeriode()
    {
        $sql = $this->select()->from(['jp' => 'job_periodes'],['id','title']);
        return $this->fetchPairs($sql);
    }

    public function getCurrenciesSelect()
    {
        $sql = $this->select()->from(['c' => 'currencies'],['id','code']);
        return $this->fetchPairs($sql);
    }

    public function getPaymentTypesSelect()
    {
        $sql = $this->select()->from(['pt' => 'payment_type'],['id','title']);
        return $this->fetchPairs($sql);
    }


    public function getTranslateById($id_lang = null)
    {
        $sql = $this->select()->from(['tl' => 'language_translate'],['*']);
        if(!is_null($id_lang))
        {
            $sql->where('tl.id_lang =?',(int) $id_lang);
        }
        return $this->fetchAll($sql);
    }

    public function getTranslateByIdSearch($id_lang = null,$search = null)
    {
        $sql = $this->select()->from(['tl' => 'language_translate'],['*']);
        if(!is_null($id_lang))
        {
            $sql->where('tl.id_lang =?',(int) $id_lang);
        }

        if(!is_null($search))
        {
            $sql->where("tl.key LIKE '%$search%' or tl.value LIKE '%$search%'");
        }
//        die($sql);
        return $this->fetchAll($sql);
    }

    public function saveLanguage($languages)
    {
        foreach($languages as $key => $lang)
        {
            $this->update(['value' => $lang['value']],"id={$lang['id']}","language_translate");
        }
    }

    public function getBrandColorButton($class)
    {
            $sql = $this->select()->from(['b' => 'branding'],['value'])
            ->where("b.class_name =?",$class);
            return $this->fetchRow($sql)['value'];
    }

    public function getPublicStatusByStatusId($status)
    {
        $sql = $this->select()->from(['s' => 'status'],['public_title'])
            ->where("s.id =?" ,(int) $status);
        return $this->fetchRow($sql);
    }

    public function getPublicStatusByStatusIdFromApplicationHistory($applications,$status)
    {
        $current_public_status = $this->getPublicStatusByStatusId($applications['status'])['public_title'];
        $last_public_status = $this->getPublicStatusByStatusId($status)['public_title'];
        if($current_public_status != $last_public_status) return true ;
        return false ;
    }

    public function getGamifications()
    {
        $sql = $this->select()->from(['g' => 'gamifications_actions'],['id as id_action','action as title',
            'points','campaign_section','title as text','checkbox_title'])
        ->where('g.action_order > 0')->order('g.action_order ASC');
        $result =  $this->fetchAll($sql);
        foreach ($result as $index => &$item) {

            $item['validation'] = [] ;
            $sql = $this->select()->from(['cga' => 'gamification_validation'],[])
                ->joinLeft(['v' => 'validation'],"v.id=cga.id_validation",['v.title','v.id'])
                ->where('cga.id_action =?',(int) $item['id_action']);
//            die($sql);
            $data = $this->fetchAll($sql) ;

            $item['validation'] = $data ;
        }
        foreach ($result as $index => &$item) {
            $item['text'] = ($this->translate->_($item['text']));
            $item['checkbox_title'] = ($this->translate->_($item['checkbox_title']));
        }

        return $result;
    }

    public function getValidations()
    {
        $sql = $this->select()->from(['v' => 'validation'],['id as id_validation','title']);
        return $this->fetchAll($sql);
    }

    public function getCampaignLocations()
    {
        // select all existing jobs in campaign campaigns

        $sql = $this->select()->from(['cp' => 'campaign_jobs'],['*']);
        $result = $this->fetchAll($sql);
        $campaign_jobs = [];
        if(!empty($result))
        {
            foreach ($result as $index => $item) {
                $campaign_jobs[] = $item['id_job'];
            }
        }

        $sql = $this->select()->from(['j' => 'jobs'],['id'])->where('j.status =1');
        if(!empty($campaign_jobs))
        {
            $sql->where("j.id NOT IN (" . implode(",", array_unique($campaign_jobs)) . ")");
        }

        $jobs = $this->fetchAll($sql);

        $jobs_cities = [];
        $jobs_counties = [];
        foreach ($jobs as $index => $job) {
            $cities = $this->__getJobCities($job['id']);
            if(!empty($cities))
            {
                foreach ($cities as $city)
                {
                    $jobs_cities[] = $city;
                }
            }
            $counties = $this->__getCountiesByJobId($job['id']);
            if(!empty($counties))
            {
                foreach ($counties as $county)
                {
                    $jobs_counties  [] = $county;
                }
            }
        }
        $job_countries = [] ;
        $jobs_cities = array_map("unserialize", array_unique(array_map("serialize", $jobs_cities)));
        $jobs_counties = array_map("unserialize", array_unique(array_map("serialize", $jobs_counties)));
        foreach($jobs_counties as $key => $value)
        {
            $job_countries[] = $this->getCountryById($value['id_country']);
        }
        $job_countries = array_map("unserialize", array_unique(array_map("serialize", $job_countries)));

        $data['countries'] = $job_countries;
        $data['counties'] = $jobs_counties;
        $data['cities'] = $jobs_cities;
        return $data;

    }

    private function __getJobCities($id) {
        $sql = $this->select()
            ->from(array("c" => "cities"), array("id", "title",'id_county'))
            ->join(array("jc" => "job_cities"), "c.id = jc.id_city",array())
            ->where("jc.id_job = ?", $id);
        return ($this->fetchAll($sql));
    }

    public function __getCountiesByJobId($id)
    {
        $counties = [];
        $cities = $this->getJobCities($id);
//        echo '<pre>'; print_r($cities);die;
        foreach ($cities as $key => $city) {
            $sql = $this->select()
                ->from(['c' => 'cities'],[])
                ->join(['cc' => 'counties'],"cc.id=c.id_county",array('title','cc.id','id_country'))
                ->where('c.id=?',(int) $key);
            $counties[] = $this->fetchRow($sql);
        }
        return (array_values(array_unique($counties)));
        return $counties;
    }

    public function getJobCities($id) {
        $sql = $this->select()->from(array("jc" => "job_cities"), array("id_city", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getUserMenu()
    {
        $type = Zend_Auth::getInstance()->getIdentity()->type ;

        $sql = $this->select()->from(['a' => 'acl'],['*'])
            ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
            ->where("a.id_parent =?", 0)
            ->where("acm.type =?", (int) $type)
            ->where('acm.left_menu_order IS NOT NULL')
            ->where('a.category ="side-left-menu"')
            ->where("acm.status =?", (int) 1)->order('acm.left_menu_order ASC');

        $data = [] ;
        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value)
        {
            $sql = $this->select()->from(['a' => 'acl'],['*'])
                ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
                ->where("a.id_parent =?", $value['id_acl'])
                ->where("acm.type =?", (int) $type)
                ->where('acm.left_menu_order IS NOT NULL')
                ->where("acm.status =?", (int) 1)->order('acm.left_menu_order ASC');
            $res = $this->fetchAll($sql);
            $submenu = (!empty($res)) ? $res : [] ;
            $value['submenu'] = $submenu;
        }
       return $result;

    }

    public function setFavoriteJob($id_job)
    {
        $data['id_user'] = Zend_Auth::getInstance()->getIdentity()->id;
        $data['id_job'] = $id_job ;
        $data['created_at'] = date('Y-m-d H:i:s');
        $sql = $this->select()->from(['f' => 'favorites'],['*'])
            ->where("f.id_user =?",$data['id_user'])
            ->where("f.id_job =?",$data['id_job']);
        $result = $this->fetchRow($sql);
        if(!empty($result))
        {
            $this->delete('id='.$result['id'],'favorites');
            exit;
        }
        $this->insert($data,'favorites');
        return $this->lastInsertId();
    }

    public function getFavorites()
    {
        $sql = $this->select()->from(['f' => 'favorites'],['*']);
        $result = $this->fetchAll($sql);
        return $result;
    }

    public function deleteFavoriteById($id_job)
    {
        $id_user = (int) Zend_Auth::getInstance()->getIdentity()->id ;
        $this->delete("id_user = {$id_user} AND id_job= {$id_job}", "user_departments");
    }

    public function existAclDb($uri)
    {
        if($uri == "") return false;
        $sql = $this->select()->from(['a' => 'acl'],['*'])->where('a.link =?',(string) $uri);
        $result =  $this->fetchRow($sql);
        if(!empty($result)) return true;
        return false;
    }

    public function getLangaugesByCode($lang)
    {
        $sql = $this->select()->from(['lt' => 'language_translate'],['key','value'])
            ->where('lt.lang =?', $lang);
        return $this->fetchAll($sql);
    }

    public function checkDbAcl($uri)
    {
        if($uri == "") return false;
        $sql = $this->select()->from(['a' => 'acl'],['*'])->where('a.link =?',(string) $uri)
        ->join(['aclm' => 'acl_matrix'],'aclm.id_acl=a.id',[])->where('aclm.status =1')
        ->where("aclm.type =?",(int) Zend_Auth::getInstance()->getIdentity()->type);
        $result =  $this->fetchRow($sql);
        if(!empty($result)) return true;
        return false;
    }

    public function getCampaignStatus()
    {
        $sql = $this->select()->from(['cs' => 'campaign_status'],['*']);
        return $this->fetchAll($sql);
    }

    public function getCampaignStatusPairs()
    {
        $sql = $this->select()->from(['cs' => 'campaign_status'],['*']);
        return $this->fetchPairs($sql);
    }

    public function getApplyAcl($link)
    {
        $sql = $this->select()->from(['a' => 'acl'],['*'])
        ->where('a.link =?', (string) $link);
        return $this->fetchRow($sql);
    }

    public function getDepartmentBySubDepsId($id_sub)
    {
        $sql = $this->select()->from(['sp' => 'sub_departments'],['*'])
            ->where('sp.id =?',(int) $id_sub);
        return $this->fetchRow($sql);
    }

    public function getSubDepartmentBySubSubDepsId($id_sub_sub)
    {
        $sql = $this->select()->from(['sp' => 'sub_sub_departments'],['*'])
            ->where('sp.id =?',(int) $id_sub_sub);
        return $this->fetchRow($sql);
    }

}
    