<?php

class models_Stats extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'users';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    protected $_company;
    protected $_content;
    protected $_payments;
    public $id_lang;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }


    public function getAllUsersPositions() {
        $sql = $this->select()->from(array("p" => "user_positions"));
        return $this->fetchAll($sql);
    }

    public function getScoringPoints() {
        $sql = $this->select()->from(array("g" => "gamifications_actions"), array("action", "points"));
        return $this->fetchPairs($sql);
    }

    public function saveScoringPoints($data) {
        $this->query("TRUNCATE TABLE `gamifications_actions`");
        foreach ($data as $key => $value) {
            $this->insert(array("action" => $key, "points" => $value), "gamifications_actions");
        }
    }

    public function setGamificationPoint($id_user, $action, $id_item = 0) {
        $points = $this->getScoringPoints();

        $sql = $this->select()->from(array("g" => "gamifications_actions"))->where("action = ?", $action);
        $data = $this->fetchRow($sql);

        $this->insert(array("id_user" => $id_user, "action" => $data['id'], "points" => $points[$action], "id_item" => $id_item, "date" => date("Y-m-d H:i:s")), "gamification_points");
    }

    public function setNotification($id, $type, $url, $message) {
        $this->insert(array("id_company" => $id, "target" => $type, "url" => $url, "message" => $message), "notifications");
    }

    public function getCurrentNotifications() {
        $sql = $this->select()
                ->from(array("n" => "notifications"), array("id", "message", "id_company", "date", "url"))
                ->where("viewed = 0")
                ->where("n.type = 'notification'")
                ->limit(20);

        if ($this->_user->type == 2) {
            $sql->where("id_company = ?", $this->_user->id_company);
            $sql->where("target = 'company'");
        } else {
            $sql->where("id_company = ?", $this->_user->id);
            $sql->where("target = 'user'");
        }

        $data = $this->fetchAll($sql);
        if (empty($data)) {
            $sql = $this->select()
                    ->from(array("n" => "notifications"), array("id", "message", "id_company", "date", "url"))
                    ->where("n.type = 'notification'")
                    ->limit(20);

            if ($this->_user->type == 2) {
                $sql->where("id_company = ?", $this->_user->id_company);
                $sql->where("target = 'company'");
            } else {
                $sql->where("id_company = ?", $this->_user->id);
                $sql->where("target = 'user'");
            }

            $data = $this->fetchAll($sql);
            if (empty($data)) {
                return array();
            } else {
                return $data;
            }
        } else {
            return $data;
        }
    }

    public function getJobClosingRate() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array())
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("status", "status_reason"))
                ->join(array("jd" => "job_departments"), "j.id = jd.id_job", array("id_department"))
                ->join(array("dl" => "departments_lang"), "jd.id_department = dl.id_department", array("title as department"));
        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5
            ) {
            $sql->where("j.id_company = ?", (int) $this->_user->id_company);
        }

        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $stats[$row['id_department']]['title'] = $row['department'];
            $stats[$row['id_department']]['total'] += 1;
            if ($row['status'] == 5) {
                $stats[$row['id_department']]['success'] += 1;
            }
        }

        return $stats;
    }

    public function getCandidatesByStatus() {
        $sql = $this->select()->from(array("ja" => "job_applications"), array("ja.status", "COUNT(ja.id_user) AS total"))->group("ja.status");

        if ($this->_user->type == 2 || $this->_user->type == 4 || $this->_user->type == 5) {
            $sql->join(array("j" => "jobs"), "j.id = ja.id_job", array())
                    ->where("j.id_company = ?", (int) $this->_user->id_company);
        }

        return $this->fetchAll($sql);
    }

    public function setCampaignReport($id_recommender, $action, $id_job,$id_user = null) {
        // 1 - check if the job is in campaign
        $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("*"))->where("cj.id_job =?", (int) $id_job);
        $jobResult = $this->fetchRow($sql);
        if (!empty($jobResult)) {
            $id_campaign = $jobResult['id_campaign'];
            // 2 - get gamification Action for the campaign 
            $sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), array("id", "id_gamification_action"))->where("cga.id_campaign =?", (int) $id_campaign);
            $campaign_actions = $this->fetchPairs($sql);
            if(!empty($campaign_actions)) {
                 // 3 - Check  the id of $actions
                $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))->where("ga.action =?", (string) $action);
                $result = $this->fetchRow($sql);
                if(!empty($result)) {
                      // 4 - check if action exist in campaign actions
                    $exist = array_intersect($campaign_actions, $result);
                    if(!empty($exist)) {
                        $scoring = $this->getScoringPoints();
                        $points = $scoring[$action];
                        $data['id_campaign'] = $id_campaign;
                        $data['id_recommender'] = (int) $id_recommender;
                        if($data['id_recommender'] == "") {
                            $userModel = new models_User;
                            $data['id_recommender'] = $userModel->getRecommderByUserId(Zend_Auth::getInstance()->getIdentity()->id);
                        }
                        $data['id_user'] = Zend_Auth::getInstance()->getIdentity()->id;
                        if($data['id_user'] == "" || $data['id_user'] == null) {
                            $data['id_user'] = $id_user ;
                        }
                        $data['action'] = $action;
                        $data['value'] = $points;
                        $data['id_job'] = $id_job;
                        $data['date'] = date("Y-m-d H:i:s");
                        $this->insert($data,"campaign_actions_detailed");
                    }

                }
              
            } 
           
        }
    }


    public function saveCampaignReport($data)
    {
        $data['date'] = date('Y-m-d H:i:s');
        $this->insert($data,'campaign_actions_detailed');
    }
    
    public function newCandidateAction($id_recommender, $action,$id_job,$id) {
        $modelUser = new models_User;
        $url = new Zend_Session_Namespace('url');
        $array = explode("/", $url->url);
        $key = array_search('jobs', $array) + 1;
        $id_job = $array[$key];
        $id_recommender = $modelUser->getRecommderByUserId($id);
        $sql = $this->select()->from(array("cj" => "campaign_jobs"), array("*"))->where("cj.id_job =?", (int) $id_job);
        $jobResult = $this->fetchRow($sql);
        if(!empty($jobResult)) {
            $this->setCampaignReport($id_recommender, $action,$id_job,$id);
        } else {
            $stats = new models_Stats;
            $stats->setGamificationPoint($id_recommender, $action, $id_job);
        }
        
    }

    public function setActivateAccountPoint($id_user,$action)
    {
        $model_company = new models_Company;
        $id_company = (int) $this->_user->id_company;
        $candidateAccess = $model_company->getCompaniesCandidateRecommend($id_company)['allow_candidate_recommend'];
        if(!Zend_Auth::getInstance()->getIdentity()->type == 1) return;
        if(Zend_Auth::getInstance()->getIdentity()->type == 9 && !$candidateAccess) return;

        // id action
        $sql = $this->select()->from(array("g" => "gamifications_actions"))->where("action = ?", $action);
        $data = $this->fetchRow($sql);

        $id_action = $data['id'];

        // get All Campaigns
        $date = date('Y-m-d H:i:s', time() - (7 * 86400)); // 7 days ago
        $sql = $this->select()
                    ->from(['c' => 'campaigns'],array('*'))
                    ->where('c.status=1')
                    ->where("c.start >=?", $date);
        $result = $this->fetchAll($sql);
        foreach ($result as $key => $value)
        {
            $id_campaign = $value['id'];
            $sql = $this->select()
                        ->from(['cg' => 'campaign_gamification_actions'],['id','id_gamification_action'])
                        ->where('cg.id_campaign=?',$id_campaign);
            $gamification_action = $this->fetchPairs($sql);
            if(in_array($id_action,$gamification_action))
            {
                $scoring = $this->getScoringPoints();
                $points = $scoring[$action];
                $data_to_insert['id_campaign'] = $id_campaign;
                $data_to_insert['id_user'] = 0;
                $data_to_insert['id_recommender'] = $id_user;
                $data_to_insert['action'] = $action;
                $data_to_insert['value'] = $points;
                $data_to_insert['id_job'] = 0;
                $data_to_insert['date'] = date("Y-m-d H:i:s");
                $this->insert($data_to_insert,"campaign_actions_detailed");
            }

        }

    }
}
