<?php

class models_Company extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'companies';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    public $id_lang;
    public $_stats;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;
        $this->_stats = new models_Stats;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }

    public function getCompanies() {
        $sql = $this->select()
                ->from(array('c' => 'companies'), array('*'))
                ->joinLeft(array('co' => 'countries'), "c.id_country = co.id", array('title AS country'))
                ->joinLeft(array('ci' => 'cities'), "c.id_city = ci.id", array('title AS city'))
                ->where("status < 2")
                ->order('id DESC');

        return $this->fetchAll($sql);
    }

    public function getCompaniesSelect($companies = null) {
        $sql = $this->select();
        $sql->from(array('c' => 'companies'), array('id', 'title'))->order('id DESC');

        if (null !== $companies) {
            $sql->where("c.id IN (" . implode(",", $companies) . ")");
        }

        if ($this->_user->type == 2) {
            $sql->where("id = ?", $this->_user->id_company);
        }

        return $this->fetchPairs($sql);
    }

    public function getCompaniesSlugSelect($companies = null) {
        $sql = $this->select();
        $sql->from(array('c' => 'companies'), array('id', 'slug'))->order('id DESC');

        if (null !== $companies) {
            $sql->where("c.id IN (" . implode(",", $companies) . ")");
        }

        if (@$this->_user->type == 2) {
            $sql->where("id = ?", $this->_user->id_company);
        }
        return $this->fetchPairs($sql);
    }

    public function getCompaniesAllowExternal($company) {

        $sql = $this->select()
                ->from(array('c' => 'companies'), array('allow_external'))
                ->where("c.id = ? ", (int) $company);
        return $this->fetchRow($sql);
    }

    public function getCompaniesEmployeeApply($company) {

        $sql = $this->select()
                ->from(array('c' => 'companies'), array('allow_employee_apply'))
                ->where("c.id = ? ", (int) $company);
        return $this->fetchRow($sql);
    }

    public function getCompaniesCandidateRecommend($company = null) {
        $external = $this->_settings['external_platform'];
        
        $sql = $this->select();
                $sql->from(array('c' => 'companies'), array('allow_candidate_recommend'));
                
                if($external) {
                    $id_company = $this->getCompanyBySubdomainSlug()['id'];
                    $sql->where("c.id = ? ", (int) $id_company);
                } else {
                    $sql->where("c.id = ? ", (int) $company);
                }
                
        return $this->fetchRow($sql);
    }

    public function getCompaniesSlugSelectWithJobs($companies = null) {
        $sql = $this->select()
                ->from(array('c' => 'companies'), array('id', 'slug'))
                ->join(array("j" => "jobs"), "c.id = j.id_company", array())
                ->order('id DESC');

        if (null !== $companies) {
            $sql->where("c.id IN (" . implode(",", $companies) . ")");
        }

        if (@$this->_user->type == 2) {
            $sql->where("id = ?", $this->_user->id_company);
        }

        $sql->where("c.status = 1");

        return $this->fetchPairs($sql);
    }

    public function getCompaniesSlugSelectHomeWithJobs($companies = null) {
        $sql = $this->select()
                ->from(array('c' => 'companies'), array('slug', 'title'))
                ->join(array("j" => "jobs"), "c.id = j.id_company", array())
                ->order('c.id DESC');

        if (null !== $companies) {
            $sql->where("c.id IN (" . implode(",", $companies) . ")");
        }

        if (@$this->_user->type == 2) {
            $sql->where("c.id = ?", $this->_user->id_company);
        }

        $sql->where("c.status = 1");

        return $this->fetchPairs($sql);
    }

    public function getCompanyById($id = null) {
        $sql = $this->select()
                ->from(array('c' => 'companies'), array('*'))
                ->joinLeft(array('cc' => 'company_credits'), "c.id = cc.id_company", array('credits', 'job_credits', 'search_credits', 'highlight_credits'))
                ->joinLeft(array('t' => 'terms'), "c.id = t.id_company", array('*'))
                ->where("c.id = ? ", (int) $id);

        return $this->fetchRow($sql);
    }

    public function getCompanyBySlug($slug = null) {
        $sql = $this->select()
                ->from(array('c' => 'companies'), array('*'))
                ->joinLeft(array('cc' => 'company_credits'), "c.id = cc.id_company", array('credits', 'job_credits', 'search_credits'))
                ->joinLeft(array('i' => 'industries'), "c.id_industry = i.id", array())
                ->joinLeft(array('il' => 'industries_lang'), "i.id = il.id_industry AND il.id_lang = {$this->id_lang}", array("title AS industry"))
                ->joinLeft(array('ci' => 'cities'), "c.id_city = ci.id", array("title AS city"))
                ->joinLeft(array('co' => 'countries'), "c.id_country = co.id", array("title AS country"))
                ->where("c.slug = ? ", (string) $slug);

        return $this->fetchRow($sql);
    }

    public function saveCompany($data) {
        
        unset($data['redir']);

//        $data['slug'] = WebApp_Tools::CreateAlias($data['title']);

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update($data, "id = $id", "companies");
            $this->_stats->setGamificationPoint($this->_user->id, "company_description");
        } else {
            $data['created'] = date("Y-m-d H:i:s");
            unset($data['id']);
            array_walk_recursive($data, function(&$value , &$key) {
               return $value = ctype_digit($value) ? (int) $value : $value; 
            });
            $this->insert($data, "companies");
            $id = $this->lastInsertId();
            $this->insert(array("id_company" => $id, "search_credits" => 999, "job_credits" => 9999, "search_credits" => 9999, "highlight_credits" => 9999, "credits" => 9999, "added" => date("Y-m-d H:i:s")), "company_credits");
            $this->_stats->setGamificationPoint($this->_user->id, "register_company");
            $this->_stats->setNotification($this->_user->id, "user", "company", "Welcome to WorkinAfrica!<br>Start creating your company profile.<br>");
        }

        $this->_updateCompanyPhoto($id);
        $this->_updateCompanyCovers($id);


        return $id;
    }

    private function _updateCompanyPhoto($id) {

        if ($_FILES['logo']['name'] != "") {
            $file = $id . "_" . $_FILES['logo']['name'];
            WebApp_UploadImage::constrainImage(250, 250, $_FILES['logo']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/large/" . $file);
            WebApp_UploadImage::saveConstrainedFixedImage(150, 53, $_FILES['logo']['tmp_name'], "right", ABSOLUTE_PATH . "/public/resources/files/companies/list/" . $file);
            WebApp_UploadImage::constrainImage(50, 50, $_FILES['logo']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/thumb/" . $file);
            move_uploaded_file($_FILES['logo']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/original/" . $file);

            $this->update(array("logo" => $file), "id = {$id}");

            $this->_stats->setGamificationPoint($this->_user->id, "upload_company_logo");
        }
    }

    private function _updateCompanyCovers($id) {

        if ($_FILES['cover_1']['name'] != "") {
            $file = $id . "_" . $_FILES['cover_1']['name'];
            WebApp_UploadImage::saveConstrainedFixedImage(1084, 221, $_FILES['cover_1']['tmp_name'], "right", ABSOLUTE_PATH . "/public/resources/files/companies/cover/" . $file);
            move_uploaded_file($_FILES['cover_1']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/original/" . $file);

            $this->update(array("cover_1" => $file), "id = {$id}", "companies");

            $this->_stats->setGamificationPoint($this->_user->id, "upload_company_logo");
        }
    }

    public function getUnconfirmedCompanies() {
        $sql = $this->select()->from(array("c" => "companies"), array("COUNT(id) AS total"))->where("status = '0'");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getCompanyJobsCount($id = null, $filters = array()) {
        $sql = $this->select()->from(array("j" => "jobs"), array("COUNT(*) as total", "j.id_company"))->group("j.id_company");
        if (null !== $id) {
            $sql->where("j.id_company = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        return $this->fetchAll($sql);
    }

    public function getCompaniesLastWeekApplications($id = null, $filters = array()) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("COUNT(ja.id) as total"))
                ->where("ja.date BETWEEN DATE_SUB(NOW(),INTERVAL 1 WEEK) AND NOW()")
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array("id_company"));
        if (null !== $id) {
            $sql->where("j.id_company = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }
        return $this->fetchAll($sql);
    }

    public function getBountiesToApprove($id) {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("count(id) as total"));
        $sql->where('b.status != "paid"');
        if (null !== $id) {
            $sql->where("b.id_company = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("b.id_company = ?", $this->_user->id_company);
        }
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }


    public function getCompanyRating($id) {
        $sql = $this->select()->from(array("c" => "companies"), array("rating"))->where("id = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['rating'];
    }

    public function getLeaderboards($id_company, $start = null, $end = null) {
        $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
        $end = (null !== $end) ? $end : date("Y-m-d");



        $modeUser = new models_User;
        $data = $modeUser->getJobLeaderboardsRest();
        
        $dateColumn = 'DATE(g.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(g.date as DATE)';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.firstname', 'u.lastname', 'u.image',
            '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_user) AS total_points',
            '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source!=\'\') AS total_views',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id) AS total_recommendation',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id AND jr.id_user !=0) AS account_created',
            '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
            '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS real_views',
            '(SELECT count(ja.id) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status=5) AS hires',
        ));
        $sql->where("u.type != 2 AND u.type != 3");

        if (null !== $id_company) {
            $sql->where("u.id_company = ?", $id_company);
        }
        $sql->order("total_points DESC");
        $sql->limit(10);

        $leaderboards['referral'] = $this->fetchAssoc($sql);

        $dateColumn = 'DATE(cad.date)';
        $sumColumn = 'SUM(cad.value)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(cad.date as DATE)';
            $sumColumn = 'SUM(CAST(cad.value as INT))';
        }
        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.firstname', 'u.lastname', 'u.image',
            '(SELECT '.$sumColumn.' FROM campaign_actions_detailed cad WHERE cad.id_recommender = u.id AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_recommender) AS total_points',
            '(SELECT count(d.id) FROM campaign_actions_detailed d WHERE d.id_recommender = u.id AND d.action=\'views\') AS real_views',

        ));
        $sql->where("u.type != 2 AND u.type != 3");


        if (null !== $id_company) {
            $sql->where("u.id_company = ?", $id_company);
        }
        $sql->order("total_points DESC");
       
        $leaderboards['campaign'] = $this->fetchAssoc($sql);



        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 4 AND ja.status_reason = 5 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS success',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS pending',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS application_on_recommend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
               ->order(new Zend_Db_Expr("total_points DESC"));
        
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }        
                
        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }


        $leaderboards['user'] = $this->fetchAssoc($sql);

        $i = 1;
        foreach ($leaderboards['user'] as $id_user => $value) {
            if ($id_user == $this->_user->id) {
                $leaderboards['place'] = $i;
            }
            $i++;
        }
        if ($leaderboards['place'] == 0) {
            $sql = $this->select()->from(array("u" => "users"), array("count(id) as place"))->where("u.id_company =?", $this->_user->id_company);
            $leaderboards['place'] = $this->fetchRow($sql)['place'];
        }
        
        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 4 AND ja.status_reason = 5 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS success',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS pending',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS application_on_reccomend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND g.action IN (4,5,6) AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\'  GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->join(array("c" => "companies"), "u.id_company = c.id", array("title AS company"))
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
                ->order("total_points DESC");
                
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }        

        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }

        $leaderboards['company'] = $this->fetchAssoc($sql);


        return $leaderboards;
    }

    public function getOptimisedLeaderboards($id_company, $start = null, $end = null) {
        $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
        $end = (null !== $end) ? $end : date("Y-m-d");


         $dateColumn = 'DATE(ja.date)';
        if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    '(SELECT COUNT(ja.id_user)  FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 4 AND ja.status_reason = 5 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS success',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = j.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS pending',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = j.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS application_on_recommend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\'  GROUP BY id_user) AS total_points',
                    
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array("ja.id_user"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array("j.id"))
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
                ->order(new Zend_Db_Expr("total_points DESC"));
                if (!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
                    $sql->having("total_points > 0");
                }

        if (null !== $id_company) {
            $sql->where("j.id_company = ?", (int) $id_company);
        }


        $leaderboards['user'] = $this->fetchAssoc($sql);
        

        $i = 1;
        foreach ($leaderboards['user'] as $id_user => $value) {
            if ($id_user == $this->_user->id) {
                $leaderboards['place'] = $i;
            }
            $i++;
        }
        if ($leaderboards['place'] == 0) {
            $sql = $this->select()->from(array("u" => "users"), array("count(id) as place"))->where("u.id_company =?", $this->_user->id_company);
            $leaderboards['place'] = $this->fetchRow($sql)['place'];
        }

        $data = $this->getProfileLeaderboards($this->_model->_user->id_company)['referral'];
        usort($data, function($a, $b)
        {
            return $a['gamification_points'] < $b['gamification_points'];
        });
        $read_place ; 
        foreach ($data as $key => $value) {
            if($value['firstname'] == $this->_user->firstname && $value['lastname'] == $this->_user->lastname) {
                $read_place = $key ;
            }
        }
        $leaderboards['place'] = 1 + $read_place ;
        
        return $leaderboards;
    }

    public function getCompanyRatingDetails($id) {
        $sql = $this->select()
                ->from(array("r" => "companies_rating"))
                ->join(array("j" => "jobs"), "r.id_job = j.id", array())
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title AS job"))
                ->join(array("u" => "users"), "r.id_user = u.id", array("CONCAT(firstname, ' ', lastname) AS username"))
                ->where("r.id_company = ?", (int) $id)
                ->where("jl.id_lang = ?", $this->id_lang);
        return $this->fetchAll($sql);
    }

    public function getCompanyRatings($id, $limit = 2) {
        $sql = $this->select()
                ->from(array("r" => "companies_rating"))
                ->join(array("c" => "companies"), "r.id_company = c.id", array('slug AS company_slug', 'title AS company_title'))
                ->join(array("u" => "users"), "r.id_user = u.id", array('firstname', 'lastname', 'image AS user_image'))
                ->where("r.id_company = ?", (int) $id)
                ->order("r.worked DESC")
                ->limit($limit);
        return $this->fetchAll($sql);
    }

    public function setRating($params, $id_company, $id_job) {
        $data['id_company'] = $id_company;
        $data['id_job'] = $id_job;
        $data['id_user'] = $this->_user->id;
        $data['value'] = $params['scoretotal'];
        $data['rating_work'] = $params['scorework'];
        $data['rating_career'] = $params['scorecareer'];
        $data['rating_benefits'] = $params['scorecomp'];
        $data['rating_balance'] = $params['scorelife'];
        $data['title'] = $params['title'];
        $data['job_title'] = $params['job_title'];
        $data['time'] = $params['time'];
        $data['anonymous'] = $params['anonymous'];
        $data['content'] = $params['content'];

        $this->insert($data, "companies_rating");
        $this->_stats->setGamificationPoint($this->_user->id, "review_company");
        $sql = $this->select()->from(array("c" => "companies_rating"), array("avg(value) AS rating", "avg(rating_work) AS rating_work", "avg(rating_career) AS rating_career", "avg(rating_benefits) AS rating_benefits", "avg(rating_balance) AS rating_balance"))->where("id_company = ?", (int) $id_company)->where("id_user > 0");
        $data = $this->fetchRow($sql);

        if (!empty($data)) {
            $this->update($data, "id = '{$id_company}'", "companies");
        }
    }



    public function updateCompany($data, $id) {
        $this->update($data, "id = '{$id}'", "companies");
    }

    public function getCompanyHighlightedJobsCount($id_company) {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('COUNT(j.id) AS total'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array())
                ->where("j.id_company = ?", $id_company)
                ->where("j.is_promoted = 1");
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function getCompanyCovers($id) {
        $sql = $this->select()->from(array("c" => "companies"))->where("id = ?", (int) $id);
        $data = $this->fetchRow($sql);

        if ($data['cover_1'] != "") {
            $covers[] = $data["cover_1"];
        }


        if (empty($covers)) {
            for ($i = 1; $i < 5; $i++) {
                $covers[] = "cover_{$i}.jpg";
            }
        }

        return $covers;
    }

    public function getCompanyJobApplications($id) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("COUNT(ja.id) AS total"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function getCompanyJobRecommendations($id) {
        $sql = $this->select()
                ->from(array("ja" => "job_recommendations"), array("COUNT(ja.id) AS total"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("j.id_company = ?", (int) $id);
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function getCompanyActiveJobs($id) {
        $sql = $this->select()
                ->from(array("j" => "jobs"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title", "slug"))
                ->where("j.id_company = ?", (int) $id)
                ->where("jl.id_lang = ?", $this->id_lang);
        return $this->fetchAll($sql);
    }

    public function searchCompany($key) {
        $sql = $this->select()->from(array("c" => "companies"))->where("title LIKE '%{$key}%'");
        return $this->fetchAll($sql);
    }

    public function getDaysLeft($id_company) {
        $sql = $this->select()->from(array("c" => "company_credits"))->where("id_company = ?", (int) $id_company)->order("id DESC")->limit(1);
        $data = $this->fetchRow($sql);
        return intval((strtotime("+30 days", strtotime($data['added'])) - time()) / 86400);
    }

    public function getCompanyByName($name) {
        $sql = $this->select()->from(array("c" => "companies"))->where("title LIKE '%{$name}%'")->order("id DESC")->limit(1);
        return $this->fetchRow($sql);
    }

    private function _getRecommendations($id) {
        $sql = $this->select()
                ->from(array("jr" => "job_recommendations"), array("COUNT(jr.id) AS total"))
                ->join(array("j" => "jobs"), 'jr.id_job = j.id', array("id_company"));
        if (null !== $id) {
            $sql->where("j.id_company = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    private function _getRecommendedApplications($id) {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array())
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("COUNT(ja.id) AS total"))
                ->where("ja.id_recommender > 0");
        if (null !== $id) {
            $sql->where("j.id_company = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function getCompanyByUserId($id) {
        $sql = $this->select()->from(array("u" => "users"), array())
                ->join(array("c" => "companies"), "u.id_company = c.id", array("*"));

        $data = $this->fetchRow($sql);
        if (!empty($data)) {
            return $data;
        }
    }

    public function addEmailTemplates($id) {
        $setup_path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'setup_emails';
        $cron_setup_path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'setup_crons';
        $setup_path_hu = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'setup_emails'.DIRECTORY_SEPARATOR.'hu';
        $cron_setup_path_hu = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'setup_crons'.DIRECTORY_SEPARATOR.'hu';
        $sql = $this->select()->from(array("e" => "emails"), array("*"))->where("e.id_company =?", (int) $id);
        $result = $this->fetchAll($sql);
        $company_email = $this->getCompanyById($id)['email'];
        $company_name = $this->getCompanyById($id)['title'];
        $cron = new models_Cron;
        if (count($result) < 1) {

            $data['id_company'] = $id;
            $data['type'] = 97;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "New campaign created";
                $subject = <<<'HERE'
                        New campaign created
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'97.phtml');
                $_data['content'] = $content;

                $this->insert($_data, 'emails_lang');
            }



            $data['id_company'] = $id;
            $data['type'] = 25;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "apply";
                $subject = <<<'HERE'
                        Your application was successful
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'25.phtml');
                $_data['content'] = $content;

                $this->insert($_data, 'emails_lang');
            }

//            $data['id_company'] = $id;
//            $data['type'] = 25;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 9;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;

//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "apply";
//                $subject = <<<'HERE'
//                        Sikeresen pályázott
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'25.phtml');
//                $_data['content'] = $content;
//
//                $this->insert($_data, 'emails_lang');
//            }


            // changed
            $data['id_company'] = $id;
            $data['type'] = 66;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "company email";
                $subject = <<<'HERE'
Your candidate has just applied to a job!
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'66.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

//
//            $data['id_company'] = $id;
//            $data['type'] = 66;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 1;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "company email";
//                $subject = <<<'HERE'
//A jelöltje éppen most jelentkezett egy munkára!
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'66.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }

            // changed
            $data['id_company'] = $id;
            $data['type'] = 26;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 3;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "company email";
                $subject = <<<'HERE'
                        New application for {$job.title}
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'26.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

//            $data['id_company'] = $id;
//            $data['type'] = 26;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 3;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "company email";
//                $subject = <<<'HERE'
//                        Új alkalmazás a {$job.title} állásra
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'26.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }

            /**
             *  changed to the new email journey
             */
            $data['id_company'] = $id;
            $data['type'] = 27;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Recommendation email";
                $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'27.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 28;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {

                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Recommendation email";
                $subject = <<<'HERE'
New recommendation for  {$job.title} by {$user.firstname} {$user.lastname}
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'28.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

//            $data['id_company'] = $id;
//            $data['type'] = 28;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 9;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "Recommendation email";
//                $subject = <<<'HERE'
//Új állás {$job.title} ajánlása {$user.firstname} {$user.lastname} számára.
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'28.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }





            // changed
            $data['id_company'] = $id;
            $data['type'] = 31;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "new Campaign created";
                $subject = <<<'HERE'
{$campaign} campaign just started! Share promoted jobs.
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'31.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

//
//            $data['id_company'] = $id;
//            $data['type'] = 31;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 1;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "new Campaign created";
//                $subject = <<<'HERE'
//{$campaign} campaign just started! Share promoted jobs.
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'31.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }

            // changed
            $data['id_company'] = $id;
            $data['type'] = 65;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "new Campaign created";
                $subject = <<<'HERE'
Your campaign just started! Fingers crossed.
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'65.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

//
//            $data['id_company'] = $id;
//            $data['type'] = 65;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 2;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "new Campaign created";
//                $subject = <<<'HERE'
//Kampánya már megkezdődött! Keresztezett ujjak.
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'65.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }

            // changed
            $data['id_company'] = $id;
            $data['type'] = 32;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {

                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Registration";
                $subject = "Your candidate has been hired";
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'32.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            // changed
//            $data['id_company'] = $id;
//            $data['type'] = 32;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 1;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "Registration";
//                $subject = "Az ön jelöltjét felvették";
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'32.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }

            // changed
            $data['id_company'] = $id;
            $data['type'] = 68;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {

                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Admin notification on success hire";
                $subject = <<<'HERE'
Awesome! You just hired a candidate
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'68.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            // changed
//            $data['id_company'] = $id;
//            $data['type'] = 68;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 2;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 1;
//                $_data['title'] = "Admin notification on success hire";
//                $subject = <<<'HERE'
//Csodálatos! Épp most vett fel egy jelöltet
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'68.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }



            $data['id_company'] = $id;
            $data['type'] = 39;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
             if ($lastID) {
                  $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Status change feedback";
                $subject = <<<'HERE'
Status change feedback

HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'39.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
//
//            $data['id_company'] = $id;
//            $data['type'] = 39;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 9;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "Status change feedback";
//                $subject = <<<'HERE'
//Állapotváltás visszajelzése
//
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'39.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }
            
            $data['id_company'] = $id;
            $data['type'] = 40;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Application status changed";
                $subject = <<<'HERE'
Application status changed
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'40.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
//
//
//            $data['id_company'] = $id;
//            $data['type'] = 40;
//            $data['status'] = 1;
//            $data['sent'] = 1;
//            $data['frequency'] = 1;
//            $data['direction'] = 9;
//            $data['email'] = $company_email;
//            $data['from'] = $company_name;
//            $this->insert($data, "emails");
//            $lastID = $this->lastInsertId();
//            if ($lastID) {
//                $_data['id_email'] = $lastID;
//                $_data['id_lang'] = 4;
//                $_data['title'] = "Application status changed";
//                $subject = <<<'HERE'
//Az alkalmazás állapota frissült
//HERE;
//                $_data['subject'] = $subject;
//                $content = file_get_contents($setup_path_hu.DIRECTORY_SEPARATOR.'40.phtml');
//                $_data['content'] = $content;
//                $this->insert($_data, 'emails_lang');
//            }
            
            
            
            $data['id_company'] = $id;
            $data['type'] = 41;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Mail admin upload cv";
                $subject = <<<'HERE'
New Referral on Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'41.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
            

            
             $data['id_company'] = $id;
            $data['type'] = 42;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;
            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Mail candidat upload cv";
                $subject = <<<'HERE'
Hi {$candidate.firstname} , I just recommended you for a job at {$company.title}
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'42.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }
            
            
            
            
            
            


            $data['id_company'] = $id;
            $data['type'] = 64;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Welcome to Talentspotting";
                $subject = <<<'HERE'
                        Welcome to Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'64.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 67;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Welcome to Talentspotting";
                $subject = <<<'HERE'
                        Welcome to Talentspotting
HERE;
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'67.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 70;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Welcome to Talentspotting";
                $subject = "Create jobs and be the first to share";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'70.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 71;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "tips & tricks tutorial on share a job";
                $subject = "Share a job @ 1 click away";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'71.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 72;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "close vacancies & engage platform users";
                $subject = "Update Candidate status @one drag away";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'72.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 73;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "engage admin to change candidate status";
                $subject = "Recognise and reward on hired";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'73.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 74;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "increase employees interest to share specific jobs";
                $subject = "Create campaigns";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'74.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 75;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "increase future engagement in campaigns";
                $subject = "Reward campaign winners";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'75.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 76;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "improve Talentspotting experience";
                $subject = "Time for your feedback";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'76.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 77;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage admin to announce & reward a campaign's winner ";
                $subject = "Your campaign is closed! The winner is";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'77.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 78;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Monthly activity overview";
                $subject = "Your monthly Talentspotting report";
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'78.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 79;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Process candidates";
                $subject = '{$total_active_jobs} active jobs available and {$candidates} candidates to process';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'79.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 80;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage recruiters to view candidates";
                $subject = 'Wow! New applicants on Talentspotting';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'80.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 81;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage admin to add new jobs/remove expired ones, prolongue campaign if needed";
                $subject = 'These jobs & campaigns are about to expire';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'81.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 82;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 2;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage admin to add pay rewards by sending remidner to payrol department";
                $subject = 'Bounties to be redeemed';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'82.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 83;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "create jobs";
                $subject = 'Create jobs and be the first to share';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'83.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 84;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "tips & tricks tutorial on share a job";
                $subject = 'Share a job @ 1 click away';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'84.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 85;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "close vacancies & engage platform users";
                $subject = 'Update Candidate status @one drag away';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'85.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 86;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "improve Talentspotting experience";
                $subject = 'Time for your feedback';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'86.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 87;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Monthly activity overview";
                $subject = 'Your monthly Talentspotting report';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'87.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 88;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Process candidates";
                $subject = '{$total_active_jobs} active jobs available and {$candidates} candidates to process';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'88.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 89;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 4;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage recruiters to view candidates";
                $subject = 'Wow! New applicants on Talentspotting';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'89.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 90;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "learn how to share jobs and try one right now";
                $subject = 'Share a job @ 1 click away';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'90.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 91;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "improve Talentspotting experience";
                $subject = 'Time for your feedback';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'91.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 92;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "get new jobs from prior email (7 days ago)";
                $subject = 'New jobs in {$company} waiting to be shared';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'92.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 93;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "engage user even more";
                $subject = 'Your monthly Talentspotting report';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'93.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 94;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 1;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "recommendation reminder";
                $subject = 'Reminder about the recommendation for {$job}';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'94.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 95;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Check available jobs";
                $subject = 'New jobs in {$company} that match your skills';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'95.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 69;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage, checking applications status";
                $subject = 'Job Application Status has been updated to: offered';
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'69.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 23;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage, checking applications status";
                $subject = 'Job Application Status has been updated to: hired';
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'23.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $data['id_company'] = $id;
            $data['type'] = 24;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Engage, while updating status";
                $subject = 'Job Application Status Follow Up';
                $_data['subject'] = $subject;
                $content = file_get_contents($setup_path.DIRECTORY_SEPARATOR.'24.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }

            $data['id_company'] = $id;
            $data['type'] = 96;
            $data['status'] = 1;
            $data['sent'] = 1;
            $data['frequency'] = 1;
            $data['direction'] = 9;
            $data['email'] = $company_email;
            $data['from'] = $company_name;

            $this->insert($data, "emails");
            $lastID = $this->lastInsertId();
            if ($lastID) {
                $_data['id_email'] = $lastID;
                $_data['id_lang'] = 1;
                $_data['title'] = "Join us";
                $subject = 'Join us on {$subdomain}';
                $_data['subject'] = $subject;
                $content = file_get_contents($cron_setup_path.DIRECTORY_SEPARATOR.'96.phtml');
                $_data['content'] = $content;
                $this->insert($_data, 'emails_lang');
            }


            $terms = file_get_contents(APPLICATION_PATH
                . DIRECTORY_SEPARATOR
                . 'layouts'
                . DIRECTORY_SEPARATOR
                . 'terms'.DIRECTORY_SEPARATOR.'terms.phtml');
            $terms = htmlspecialchars($terms, ENT_QUOTES, 'UTF-8');
            $this->insert(["id_company" => $id ,"status" => 1, "content" => $terms ],'terms');
            $this->insert(["id_company" => $id ,"status" => 1, "content" => " How it works" ],"hiworks");
        }
    }

    public function getCompanyByJobId($id_job) {
        $sql = $this->select()->from(array("j" => "jobs"))
                ->join(array("c" => "companies"), "j.id_company = c.id", array("*"))
                ->where("j.id =?", (int) $id_job);
        return $this->fetchRow($sql);
    }

    public function getCompanyBySubdomainSlug($domain = null) {
        $site = ($domain == null) ? $_SERVER["SERVER_NAME"] : $domain;
        if($site == "") $site = $this->_settings['sitename'];
        $subdomain = explode('.', $site);
        if (in_array("www", $subdomain)) {
            array_shift($subdomain);
        }
        $subdomain = preg_replace('#^https?://#', '', $subdomain[0]);
        $sql = $this->select()->from(array("c" => "companies"), array("*"))->where("c.slug =?", $subdomain);
        $result = $this->fetchRow($sql);
        if (empty($result)) {
            $sql = $this->select()->from(array("c" => "companies"), array("*"))->where("c.slug =?", $subdomain);
            $result = $this->fetchRow($sql);
        }
        return $result;
    }

    function getTermsAndConditionByComapnyId($id) {
        $sql = $this->select()->from(array("t" => "terms"), array("*"))
                ->where("t.id_company=?", (int) $id);

        return $this->fetchRow($sql);
    }
    public function getProfileLeaderboards($id_company, $start = null, $end = null) {
        $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
        $end = (null !== $end) ? $end : date("Y-m-d");

        $modeUser = new models_User;
        $data = $modeUser->getJobLeaderboardsRest();
        
        $dateColumn = 'DATE(g.date)';
        $sumColumn = 'SUM(g.value)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(g.date as DATE)';
            $sumColumn = 'SUM(CAST(g.value as INT))';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.firstname', 'u.lastname', 'u.image',
            '(SELECT '.$sumColumn.' FROM campaign_actions_detailed g WHERE g.id_recommender = u.id AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS total_points',
            '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source!= \'\') AS total_views',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id) AS total_recommendation',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id AND jr.id_user !=0) AS account_created',
            '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
            '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS real_views',
            '(SELECT count(ja.id) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status=5) AS hires',
            '(SELECT sum(ga.points) FROM gamification_points ga WHERE ga.id_user = u.id AND ga.action IS NOT NULL) AS gamification_points',
        ));
        
        if (null !== $id_company) {
            $sql->where("u.id_company = ?", $id_company);
        }
        
        $candidateAccess = $this->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $AdminAccess = $this->getCompaniesAllowExternal(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_external'];
        if(!$candidateAccess) {
            $sql->where("u.type != 9");
        }
        if(!$AdminAccess) {
            $sql->where("u.type != 2");
        }
        $leaderboards['referral'] = $this->fetchAssoc($sql);
        
        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }

        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 4 AND ja.status_reason = 5 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS success',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS pending',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS application_on_recommend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
                ->order("total_points DESC");
        
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }
                
        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }


        $leaderboards['user'] = $this->fetchAssoc($sql);

        $i = 1;
        foreach ($leaderboards['user'] as $id_user => $value) {
            if ($id_user == $this->_user->id) {
                $leaderboards['place'] = $i;
            }
            $i++;
        }
        if ($leaderboards['place'] == 0) {
            $sql = $this->select()->from(array("u" => "users"), array("count(id) as place"))->where("u.id_company =?", $this->_user->id_company);
            $leaderboards['place'] = $this->fetchRow($sql)['place'];
        }
        
        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 4 AND ja.status_reason = 5 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS success',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS pending',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS application_on_reccomend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND g.action IN (4,5,6) AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->join(array("c" => "companies"), "u.id_company = c.id", array("title AS company"))
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
                ->order("total_points DESC");
        
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }        
                
                
        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }

        $leaderboards['company'] = $this->fetchAssoc($sql);


        return $leaderboards;
    }
    
    public function getEmailCompany() {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $company = $this->getCompanyBySubdomainSlug();
            if(!empty($company)) {
                return $company['email'];
            }
            return $this->_settings['siteemail'];
        }
        
    }
    
    public function getLeaderboardsPagination($id_company, $start = null, $end = null,$length = null,$begin = null,$order_by = null,$order_value = null) {
//        $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
//        $end = (null !== $end) ? $end : date("Y-m-d");
        $length = 20;
        $allow_admin_recommend = $this->getCompaniesAllowExternal($this->_user->id_company);
        $modeUser = new models_User;
        $data = $modeUser->getJobLeaderboardsRest();
        
        $dateColumn = 'DATE(g.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(g.date as DATE)';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.image',"CONCAT(u.firstname, ' ', u.lastname) AS user",
            '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id GROUP BY id_user) AS total_points',
//            '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source!=\'\') AS total_views',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id) AS total_recommendation',
//            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id AND jr.id_user !=0) AS account_created',
            '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
            '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS views',
//            '(SELECT count(ja.id) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status=5) AS hires',
//            '(SELECT count(cad.id) FROM campaign_actions_detailed cad WHERE cad.id_recommender = u.id AND cad.action = "completed_profile") AS completed_profile',
//          '(SELECT count(cad.id) FROM campaign_actions_detailed cad WHERE cad.id_recommender = u.id AND cad.action = "successful_hire") AS successful_hire',
            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "7") AS activate_account',
//            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "6") AS extend_network',
            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "3") AS new_applicants',
            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "2") AS new_candidates',
            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "4") AS successful_hire',
//            '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "5") AS completed_profile',
//            '(SELECT count(cad.id) FROM campaign_actions_detailed cad WHERE cad.id_recommender = u.id AND cad.action = "views") AS views',
        ));
        if($allow_admin_recommend)
            {
                $sql->where("u.type != 3");
            } else {
                $sql->where("u.type != 2 AND u.type != 3");
            }

        if (null !== $id_company) {
            $sql->where("u.id_company = ?", $id_company);
        }
//        $sql->order("total_points DESC");
        $sql->limit($length,$begin);
        
        if($order_by != null) {
             $sql->order("$order_by $order_value");
             
        } else {
            $sql->order("total_points DESC");
        }
        
        if($start != "" || $end != "") {
             $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
            $end = (null !== $end) ? $end : date("Y-m-d");
            $sql->where('u.first_login >= ?',$start);
            $sql->where('u.first_login <= ?',$end);
        }
        $sql->where("u.type != 9");
        $leaderboards['referral'] = $this->fetchAll($sql);
        return $leaderboards;
        
        $dateColumn = 'DATE(cad.date)';
        $sumColumn = 'SUM(cad.value)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(cad.date as DATE)';
            $sumColumn = 'SUM(CAST(cad.value as INT))';
        }
        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.firstname', 'u.lastname', 'u.image',
            '(SELECT '.$sumColumn.' FROM campaign_actions_detailed cad WHERE cad.id_recommender = u.id AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_recommender) AS total_points',
            '(SELECT count(d.id) FROM campaign_actions_detailed d WHERE d.id_recommender = u.id AND d.action=\'views\') AS real_views',

        ));
        if($allow_admin_recommend)
            {
                $sql->where("u.type != 3");
            } else {
                $sql->where("u.type != 2 AND u.type != 3");
            }
            


        if (null !== $id_company) {
            $sql->where("u.id_company = ?", $id_company);
        }
        $sql->order("total_points DESC");
        $sql->limit($length,$begin);
       
        $leaderboards['campaign'] = $this->fetchAssoc($sql);



        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 5 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS success',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status = 9 OR ja.status = 11 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS pending',
                    '(SELECT COUNT(ja.id_user) FROM job_applications ja WHERE ja.id_user = u.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS application_on_recommend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
               ->order(new Zend_Db_Expr("total_points DESC"));
        
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }        
                
        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }
        $sql->limit($length,$begin);

        $leaderboards['user'] = $this->fetchAssoc($sql);

        $i = 1;
        foreach ($leaderboards['user'] as $id_user => $value) {
            if ($id_user == $this->_user->id) {
                $leaderboards['place'] = $i;
            }
            $i++;
        }
        if ($leaderboards['place'] == 0) {
            $sql = $this->select()->from(array("u" => "users"), array("count(id) as place"))->where("u.id_company =?", $this->_user->id_company);
            $leaderboards['place'] = $this->fetchRow($sql)['place'];
        }
        
        $dateColumn = 'DATE(ja.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(ja.date as DATE)';
        }
        
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 5 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS success',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status = 2 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\') AS fail',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.status NOT IN (2,4) AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS pending',
                    '(SELECT COUNT(ja.id_job) FROM job_applications ja WHERE ja.id_job = j.id AND ja.id_recommender > 0 AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\' ) AS application_on_reccomend',
                    '(SELECT SUM(g.points) FROM gamification_points g WHERE g.id_user = u.id AND g.action IN (4,5,6) AND '.$dateColumn.' BETWEEN  \'' . $start . '\' AND \'' . $end . '\'  GROUP BY id_user) AS total_points',
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->join(array("c" => "companies"), "u.id_company = c.id", array("title AS company"))
                ->where("$dateColumn BETWEEN '{$start}' AND '{$end}'")
                ->order("total_points DESC");
                $sql->limit($length,$begin);
                
        if(!($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)) {
            $sql->having("total_points > 0");
        }        

        if (null !== $id_company) {
            $sql->where("j.id_company = ?", $id_company);
        }

        $leaderboards['company'] = $this->fetchAssoc($sql);


        return $leaderboards;
    }
    
    public function getAnalyticsId()
    {
        return $this->getCompanyBySubdomainSlug()['analytics'];
    }

    public function getHeatmap()
    {
        return $this->getCompanyBySubdomainSlug()['heatmap'];
    }

    public function getFooter()
    {
        return $this->getCompanyBySubdomainSlug()['footer'];
    }
    public function getRank()
    {
          $data = $this->getProfileLeaderboradsForRank($this->_model->_user->id_company)['referral'];
        usort($data, function($a, $b) {
            return $a['gamification_points'] < $b['gamification_points'];
        });
        $read_place;
        foreach ($data as $key => $value)
            {
            if ($value['firstname'] == $this->_user->firstname && $value['lastname'] == $this->_user->lastname)
                {
                $read_place = $key;
                }
            }
        $leaderboards['place'] = 1 + $read_place;

        return $leaderboards;
    }
    public function getProfileLeaderboradsForRank($id_company, $start = null, $end = null)
    {
        $start = (null !== $start) ? $start : date("Y-m-d", strtotime("2014-01-01"));
        $end = (null !== $end) ? $end : date("Y-m-d");

        $modeUser = new models_User;

        $dateColumn = 'DATE(g.date)';
        $sumColumn = 'SUM(g.value)';
        if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)
            {
            $dateColumn = 'CAST(g.date as DATE)';
            $sumColumn = 'SUM(CAST(g.value as INT))';
            }

        $sql = $this->select()
                ->from(array("u" => "users"), array(
            'u.id', 'u.firstname', 'u.lastname', 'u.image',
            '(SELECT ' . $sumColumn . ' FROM campaign_actions_detailed g WHERE g.id_recommender = u.id AND ' . $dateColumn . ' BETWEEN \'' . $start . '\' AND \'' . $end . '\') AS total_points',
            '(SELECT count(js.id) FROM job_views js WHERE js.id_user = u.id AND js.source!= \'\') AS total_views',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id) AS total_recommendation',
            '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id AND jr.id_user !=0) AS account_created',
            '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
            '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS real_views',
            '(SELECT count(ja.id) FROM job_applications ja WHERE ja.id_recommender = u.id AND ja.status=5) AS hires',
            '(SELECT sum(ga.points) FROM gamification_points ga WHERE ga.id_user = u.id AND ga.action IS NOT NULL) AS gamification_points',
        ));

        if (null !== $id_company)
            {
            $sql->where("u.id_company = ?", $id_company);
            }

        $candidateAccess = $this->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $AdminAccess = $this->getCompaniesAllowExternal(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_external'];
        if (!$candidateAccess)
            {
            $sql->where("u.type != 9");
            }
        if (!$AdminAccess)
            {
            $sql->where("u.type != 2");
            }
        $leaderboards['referral'] = $this->fetchAssoc($sql);
        return $leaderboards;
    }

    public function getCompanyEmailTemplate($site = null)
    {
        $host = ($site == null) ? $_SERVER['HTTP_HOST'] : $site = preg_replace('#^https?://#', '', $site);
        $host = "https://{$host}";
        $template = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails'."/template.phtml";
        $logo = $host."/resources/frontend/landing/files/icons/logo.png";
        $id_company = (Zend_Auth::getInstance()->getIdentity()->id_company > 0) ?
            Zend_Auth::getInstance()->getIdentity()->id_company : $this->getCompanyBySubdomainSlug($site)['id'];
        $sql = $this->select()
            ->from(['c' => 'companies'],['facebook','linkedin','website'])
            ->where('c.id=?',(int) $id_company);
        $result = $this->fetchRow($sql);
        $social = "";
        foreach($result as $key => &$value)
        {
            if($result[$key] == "") unset($result[$key]);
        }

        foreach($result as $key => $value)
        {
            switch($key)
            {
                case('facebook'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/facebook-logo-button.png"></a>';
                    break;

                case('linkedin'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/linkedin-logo-button.png"></a>';
                    break;
                case('website'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/house.png"></a>';
                    break;
            }
        }
        $smarty = new Smarty;
        $landing = $host."/resources/frontend/css/theme/images/landing_image_-_social_recruiting.jpg";
        $model_company = new models_Company;
        $landing_company = $model_company->getCompanyById($id_company)['landing'];

        $template_landing = ($landing_company == "") ? $landing :
            $host.'/resources/frontend/css/theme/images/'.$landing_company;

        $template_landing = strip_tags($template_landing);
        $smarty->assign("landing", $template_landing);
        $smarty->assign("logo", $logo);
        $smarty->assign("social", $social);
        $smarty->assign("subject", '{$subject}');
        $smarty->assign("content", '{$content}');

        $handle = fopen($template, "r");
        $fcontents = fread($handle, filesize($template));
        fclose($handle);
        // get template from db
        $sql = $this->select()->from(['et' =>'email_template'],['template']);
        $fcontents = $this->fetchRow($sql)['template'] ;
        $content = $smarty->fetch('string:'.$fcontents);
        return  $content;
        die;

    }

    public function getCompanyEmailButtonColor()
    {
        return "";
    }

    public function getBrandingColors()
    {
        $sql = $this->select()
            ->from(['b' => 'branding'],['*']);
        return $this->fetchAll($sql);
    }

    public function getBrandEmail()
    {
        $sql = $this->select()->from(['et' => 'email_template'],['*']);
        return $this->fetchRow($sql);
    }


    public function getEmailTemplateContainerByIdCompany($id_company = 0)
    {
        $id_company = (Zend_Auth::getInstance()->hasIdentity()) ?
            Zend_Auth::getInstance()->getIdentity()->id_company : $id_company;
        if($id_company == 0)
        {
            throw new Exception('id cannot be null ');
        }
        $site = null;
        $host = ($site == null) ? "http://".$_SERVER['HTTP_HOST'] : $this->_settings['sitename'];
        if(php_sapi_name() === 'cli')
        {
            $host = $this->_settings['sitename'];
        }
        $logo = $host."/resources/frontend/landing/files/icons/logo.png";

        $sql = $this->select()
            ->from(['c' => 'companies'],['facebook','linkedin','website'])
            ->where('c.id=?',(int) $id_company);
        $result = $this->fetchRow($sql);
        $social = "";
        foreach($result as $key => &$value)
        {
            if($result[$key] == "") unset($result[$key]);
        }

        foreach($result as $key => $value)
        {
            switch($key)
            {
                case('facebook'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/facebook-logo-button.png"></a>';
                    break;

                case('linkedin'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/linkedin-logo-button.png"></a>';
                    break;
                case('website'):
                    $social .= '<a href="'.$value.'"><img 
                    style="width:50px; height:50px; padding: 10px 10px 10px 10px" 
                    src="'.$host.'/resources/files/images/house.png"></a>';
                    break;
            }
        }
        $smarty = new Smarty;
        $landing = $host."/resources/frontend/css/theme/images/landing_image_-_social_recruiting.jpg";
        $model_company = new models_Company;
        $landing_company = $model_company->getCompanyById($id_company)['landing'];
        if(file_exists(ABSOLUTE_PATH.'/public/resources/frontend/css/theme/images/'.$landing_company) &&
            $landing_company != ""
        )
        {
            $template_landing = $host.'/resources/frontend/css/theme/images/'.strip_tags($landing_company);

        } else {
            $template_landing = $landing;
        }
//        $template_landing = ($landing_company == "") ? $landing :
//            $host.'/resources/frontend/css/theme/images/'.$landing_company;
        $smarty->assign("landing", $template_landing);
        $smarty->assign("logo", $logo);
        $smarty->assign("social", $social);
        $smarty->assign("subject", '{$subject}');
        $smarty->assign("content", '{$content}');


        // get template from db
        $sql = $this->select()->from(['et' =>'email_template'],['template']);
        $fcontents = $this->fetchRow($sql)['template'] ;
        $content = $smarty->fetch('string:'.$fcontents);
        return  $content;

}
    public function getLanguageById($id)
    {
        $sql = $this->select()->from(['lt' => 'language_translate'],['key','value'])
            ->where('lt.id_lang =?',(int) $id);
        return $this->fetchPairs($sql);

    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->_settings = $settings;
    }

    public function getSettings()
    {
        return $this->_settings ;
    }

    public function getLeaderboardsDetails($filters = array(),$offset)
    {
        $mCampaign = new models_Campaign ;
        $mContent = new models_Content();
        $status = $mContent->getCampaignStatusPairs();
        $sql = $this->select()
            ->from(['c' => 'campaigns'],['id','start','end','title','status']);

        $start_date = $filters['start_date'];
        $end_date = $filters['end_date'];
        if(!empty($filters))
        {
            foreach ($filters as $key => $value) {

                switch ($key)
                {
                    case('status') : // 1 or 0
                        $sql->where('c.status =?',(int) $value);
                    break;

                    case('start'):
                        $sql->order('c.start '.(string) $value);
                    break;

                    case('end'):
                        $sql->order('c.end '.(string) $value);
                    break;

                }
            }
        }
        $start_date = ($start_date == "") ? date('Y-m-d',strtotime('Last year')) : $start_date;
        $end_date = ($end_date == "") ? date('Y-m-d') : $end_date;
        $sql->where('c.start >=?',$start_date);
        $sql->where('c.start <=?',$end_date);
        $sql->where('c.status !=2');
        $sql->order('c.created DESC');
        $sql->order('c.status asc');
        $sql->limit(20,$offset);
        $result =  $this->fetchAll($sql);
        foreach($result as $key => &$value)
        {
            $date = new DateTime($value['start']);
            $now = new DateTime();
            $value['can_edit'] = 1 ;
            if($date < $now) {
                $value['can_edit'] = 0;
            }

            $value['status'] = ($status[$value['status']]);
            $value['jobs'] = $mCampaign->getCampaignJob($value['id']);
            $value['actions'] = $mCampaign->getCampaignAction($value['id']);
            $value['1st_place'] = $mCampaign->getCampaignFirstPlacesById($value['id'])[0] ?: [];
            $value['2nd_place'] = $mCampaign->getCampaignFirstPlacesById($value['id'])[1] ?: [];
            $value['3rd_place'] = $mCampaign->getCampaignFirstPlacesById($value['id'])[2] ?: [];

        }
        if(isset($filters['place']) && !empty($filters['place']))
        {
            $filter_place = $filters['place'];
            foreach ($filter_place as $index => $item) {
                if(strtolower($item) == 'desc')
                {
                    usort($result, function($a, $b) use($index) {
                        return $b[$index]['total_points'] - $a [$index]['total_points'];
                    });
                }

                if(strtolower($item) == 'asc')
                {

                    usort($result, function($a, $b) use($index) {
                        return $a[$index]['total_points'] - $b[$index]['total_points'];
                    });
                }

            }
        }

        return $result;
    }


    public function getLeaderboardsDetailsById($filters,$offset,$id_campaing)
    {
        $start = (isset($filters['start_date'])) ? $filters['start_date'] : date('Y-m-d', StrToTime("Last year"));
        $end = (isset($filters['end_date'])) ? $filters['end_date'] : date('Y-m-d',StrToTime("next year"));
        $mCampaign = new models_Campaign ;
        $gamification_action = $mCampaign->getCampaignAction($id_campaing);
//        return $gamification_action;
        $id_campaing = (int) $id_campaing ;
        $mCampaign = new models_Campaign ;
        $sql = $this->select()
            ->from(['c' => 'campaign_actions_detailed'],['id_campaign','id_user'])
            ->join(['cad' => 'campaign_gamification_actions'],'cad.id_campaign=c.id_campaign',[])
        ->join(['u' => 'users'],'u.id=c.id_user',["CONCAT(firstname, ' ', lastname) AS user",'image'])->group(['c.id_user'])
        ->where('u.type =1');
        $sql->where('c.id_campaign =?',(int) $id_campaing);

            $sql->where('c.date >=?', $start);
            $sql->where('c.date <=?', $end.' 23:59:59');
            $sql->limit(20,$offset);
//        die($sql);
        $result = $this->fetchAll($sql);
        foreach ($result as $index => &$user) {
            $total = 0;
            foreach ($gamification_action as $i => $action) {
                $total_count = $this->countActionInCampiagnByUserId($user['id_user'],$action);
                $user[$action['title']]['points'] = "<strong class='first-line'>".($total_count * $action['points'])."</strong>" ;
                $user[$action['title']]['calcul'] = "<span class='second-line'>".$action['points'] .' points X '.$total_count."</span>" ;
                $total += ($total_count * $action['points']) ;

            }
            $user['total'] = $total;
        }



        if(!empty($filters['sorting']))
        {
            foreach ($filters['sorting'] as $key => $value)
            {
                if(strtolower($value) == 'asc')
                {
                    usort($result, function($a, $b) use($key) {
                        return $a[$key]['points'] - $b[$key]['points'];
                    });
                }

                if(strtolower($value) == 'desc')
                {
                    usort($result, function($a, $b) use($key) {
                        return $b[$key]['points'] - $a[$key]['points'];
                    });
                }

            }
        } else {
            // sorting default dupa total
            usort($result, function($a, $b) {
                return $b['total'] - $a['total'];
            });
        }
        return $result;

    }

    public function getLeaderboardsDetailsByIdWinner($filters,$offset,$id_campaing)
    {
//        $start = (isset($filters['start_date'])) ? $filters['start_date'] : date('Y-m-d', StrToTime("Last year"));
//        $end = (isset($filters['end_date'])) ? $filters['end_date'] : date('Y-m-d',StrToTime("next year"));
        $mCampaign = new models_Campaign ;
        $gamification_action = $mCampaign->getCampaignAction($id_campaing);
//        return $gamification_action;
        $id_campaing = (int) $id_campaing ;
        $mCampaign = new models_Campaign ;
        $sql = $this->select()
            ->from(['c' => 'campaign_actions_detailed'],['id_campaign','id_user'])
            ->join(['cad' => 'campaign_gamification_actions'],'cad.id_campaign=c.id_campaign',[])
            ->join(['u' => 'users'],'u.id=c.id_user',["CONCAT(firstname, ' ', lastname) AS user",'image'])->group(['c.id_user'])
            ->where('u.type =1');
        $sql->where('c.id_campaign =?',(int) $id_campaing);

//        $sql->where('c.date >=?', $start);
//        $sql->where('c.date <=?', $end);
//        $sql->limit(20,$offset);
//        die($sql);
        $result = $this->fetchAll($sql);
//        $filters['sorting']['views'] =  "DESC";
        foreach ($result as $index => &$user) {
            $total = 0;
            foreach ($gamification_action as $i => $action) {
                $total_count = $this->countActionInCampiagnByUserId($user['id_user'],$action);
                $user[$action['title']]['points'] = "<strong class='first-line'>".($total_count * $action['points'])."</strong>" ;
                $user[$action['title']]['calcul'] = "<span class='second-line'>".$action['points'] .' points X '.$total_count."</span>" ;
                $total += ($total_count * $action['points']) ;

            }
            $user['total'] = $total;
        }

        usort($result, function($a, $b) {
            return $b['total'] - $a['total'];
        });

        return $result;

    }

    public function countActionInCampiagnByUserId($id_user,$action)
    {
        $sql = $this->select()->from(['c' => 'campaign_actions_detailed'],['count(id) as total'])
            ->where('c.id_action =?',(int) $action['id_action'])
            ->where('c.id_user =?',(int) $id_user);
        return $this->fetchRow($sql)['total'];

    }

    public function existAction($action)
    {
        $sql = $this->select()->from(['ga' => 'gamifications_actions'])->where('ga.action =?',$action);
        $result = $this->fetchRow($sql);

        if(!empty($result)) return true;
        return false;
    }

    public function collectGamificationInfo($id_user, $action)
    {
        $id_user = (int) $id_user;
        $action = (string) $action;
        $action_data = $this->getGamificationActionByAction($action);
        $id_action = $action_data['id'];
        $sql = $this->select()->from(['gp' => 'gamification_points'],['COUNT(gp.id) as total'])
            ->where('gp.action =?', (int) $id_action)
            ->where('gp.id_user =?', (int) $id_user);
        $result = $this->fetchRow($sql);

        $data['calcul'] = "{$action_data['points']} points X {$result['total']}";
        $data['points'] = $result['total'] * $action_data['points'];
        return $data;
    }

    public function getGamificationActionByAction($action)
    {
        $sql = $this->select()->from(['ga' => 'gamifications_actions'])->where('ga.action =?',$action);
        return $this->fetchRow($sql);
    }

    public function getLeaderboardsInfo($filters = [],$offset = 0) {
        $start = ($filters['start_date'] != "") ? $filters['start_date'] : date("Y-m-d", strtotime("-1 week"));
        $end = ($filters['end_date'] != "") ? $filters['end_date'] : date("Y-m-d");
        $id_company = Zend_Auth::getInstance()->getIdentity()->id_company ;
        $length = 20;
        $allow_admin_recommend = $this->getCompaniesAllowExternal($this->_user->id_company);
        $modeUser = new models_User;
//        $data = $modeUser->getJobLeaderboardsRest();

        $dateColumn = 'DATE(g.date)';
        if($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql) {
            $dateColumn = 'CAST(g.date as DATE)';
        }

        $sql = $this->select()
            ->from(array("u" => "users"), array(
                'u.id', 'u.image',"CONCAT(u.firstname, ' ', u.lastname) AS user",
                '(SELECT COUNT(jr.id) FROM job_recommendations jr WHERE jr.id_recommender = u.id) AS total_recommendation',
                '(SELECT count(js.id) FROM jobs_share js WHERE js.id_user = u.id) AS total_shares',
                '(SELECT count(d.id) FROM gamification_points d WHERE d.id_user = u.id AND d.action=1 GROUP BY id_user) AS views',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "7") AS activate_account',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "3") AS new_applicants',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "2") AS new_candidates',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "4") AS successful_hire',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "5") AS completed_profile',
                '(SELECT count(cad.id) FROM gamification_points cad WHERE cad.id_user = u.id AND cad.action = "1") AS views',
            ))->join(['gp' => 'gamification_points'],"u.id=gp.id_user",[]);
            $sql->where("u.type != 2 AND u.type != 3");

            $sql->where('gp.date >=?',$start);
            $sql->where('gp.date <=?',$end);


            $sql->where("u.id_company = ?", (int) $id_company);
//        $sql->limit($length,$offset);
        $sql->order('gp.date DESC')->group('u.id');
        $result = $this->fetchAll($sql);

        $leaderboards['referral'] = $result;
        return $leaderboards;


    }


}
