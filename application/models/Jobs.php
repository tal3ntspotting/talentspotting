<?php

class models_Jobs extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'jobs';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    public $id_lang;
    protected $_modelUser;
    protected $_modelContent;
    protected $_modelCompany;
    public $_stats;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $this->_modelUser = new models_User;
        $this->_modelContent = new models_Content;
        $this->_modelCompany = new models_Company;

        $this->_stats = new models_Stats;

        $this->_user = $this->_modelUser->_user;
        $this->id_lang = $this->_modelUser->id_lang;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }



    public function getJobs($filters = array(), $limit = null, $offset = null) {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('*',
                    '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations',
                    '(SELECT COUNT(id) FROM job_applications ja WHERE ja.id_job = j.id AND status = 0 AND status_reason = 0) AS new_applicant'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
//                ->order('j.start DESC')
                ->order('j.status DESC')
        ->order('j.created_at DESC');

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case "id_country":
                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
                        break;

                    case "id_city":
                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
                        break;

                    case "id_company":
                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
                        break;

                    case "company":
                        $sql->where("c.slug = ?", $filter);
                        break;

                    case "id_department":
                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
                        break;

                    case "id_industry":
                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
                        break;

                    case "id_skill":
                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
                        break;

                    case "search":
                        $sql->where("jl.title LIKE '%{$filters['search'][0]}%' OR c.title LIKE '%{$filters['search'][0]}%' OR jl.responsabilities LIKE '%{$filters['search'][0]}%' OR jl.tagline LIKE '%{$filters['search'][0]}%' OR jl.description LIKE '%{$filters['search'][0]}%' OR jl.requirements LIKE '%{$filters['search'][0]}%' OR jl.bounty_requirements LIKE '%{$filters['search'][0]}%'");
                        $sql->group("j.id");
                        break;

                    case "is_active":
                        if ($filter == 1) {
                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
                        }
                        break;
                }
            }
        }

        if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
            $sql->where("c.id = ?", $this->_user->id_company);
        }

        if (null !== $offset) {
            $sql->limit($offset, $limit);
        } else if (null !== $limit) {
            $sql->limit($limit);
        }
        //die($sql->__toString());
        $data = $this->fetchAll($sql);

        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
            }

            $jobs = $this->_getJobsCities($jobs, $ids);
        }


        return $jobs;
    }

    public function getRemainingJobs($filters = array(), $campaigns) {

        $check_is_external = $this->_settings['external_platform'];

        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('*', '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
                ->joinLeft(array("jd" => "job_departments"), "jd.id_job=j.id", array("id_department"))
                ->joinLeft(array("dl" => "departments_lang"), "dl.id_department=jd.id_department", array("title as department"))
                ->joinLeft(array("cjd" => "custom_job_departments"), "cjd.id_job=j.id", array("id_department as id_custom_department"))
                ->joinLeft(array("cd" => "custom_departments"), "cjd.id_department=cd.id", array("title as custom_department"))
//                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("j.status =?", 1)
                ->order('j.is_promoted DESC')
                ->order('j.updated DESC');

        foreach ((array) $campaigns as $id => $campaign) {
            foreach ((array) $campaign['jobs'] as $id_job => $job) {
                $jobs[$id_job] = array_unique($job);
            }
        }

//        if (!empty($jobs))
//        {
//            $sql->where("j.id NOT IN (".implode(",", $jobs).")");
//            $jobs = array();
//        }

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case "id_county":
                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
                        break;

                    case "id_city":
                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
                        break;

                    case "id_company":
                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
                        break;

                    case "company":
                        $sql->where("c.slug = ?", $filter);
                        break;

                    case "id_department":
                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
                        break;

                    case "id_industry":
                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
                        break;

                    case "id_skill":
                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
                        break;

                    case "search":
                        $sql->where("jl.title LIKE '%{$filters['search']}%' OR c.title LIKE '%{$filters['search']}%' OR jl.responsabilities LIKE '%{$filters['search']}%' OR jl.tagline LIKE '%{$filters['search']}%' OR jl.description LIKE '%{$filters['search']}%' OR jl.requirements LIKE '%{$filters['search']}%' OR jl.bounty_requirements LIKE '%{$filters['search']}%'");
                        break;

                    case "is_active":
                        if ($filter == 1) {
                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
                        }
                        break;
                }
            }
        }

        if ($check_is_external) {
            $sql->where("c.id = ?", (int) $this->_modelCompany->getCompanyBySubdomainSlug()['id']);
        } else {
            if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
                $sql->where("c.id = ?", $this->_user->id_company);
            }
            if ($this->_user->type == 9) {
                $sql->where("c.id = ?", $this->_user->id_company);
            }
        }



        if (null !== $offset) {
            $sql->limit($offset, $limit);
        } else if (null !== $limit) {
            $sql->limit($limit);
        }
        //die($sql->__toString());
        $data = $this->fetchAll($sql);
        $jobs = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
                $jobs[$row['id']]['skills'] = $this->_getJobSkills($row['id']) + $this->_getCustomJobSkills($row['id']);
            }

            $jobs = $this->_getJobsCities($jobs, $ids);
        }


        usort($jobs, function($a, $b) {
            return $a['is_promoted'] < $b['is_promoted'];
        });
        usort($jobs, function($a, $b) {
            return $a['updated'] < $b['updated'];
        });
        foreach($jobs as $index => &$job)
            {
                $jobs[$index]['custom_cities'] = $this->getJobCustomCitiesByJobId($job['id']);
            }
        return $jobs;
    }

    public function getAllJobs($filters = array()) {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('*', '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
//                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("j.status = 1")
                ->order('j.is_promoted DESC')
                ->order('j.start DESC');

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case "id_country":
                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
                        break;

                    case "is_active":
                        if ($filter == 1) {
                            $sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
                        }
                        break;
                }
            }
        }

        return $this->fetchAll($sql);
    }

    /*
      details
     */

    public function getJobById($id,$flag=false,$status = false) {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('*', '(SELECT COUNT(id_job) FROM job_applications ja WHERE ja.id_job = j.id) AS applications', '(SELECT COUNT(id_job) FROM job_recommendations jr WHERE jr.id_job = j.id) AS recommendations'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('id_lang', 'id_job', 'title', 'tagline', 'description', 'requirements', 'bounty_requirements', 'slug', 'responsabilities','role'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array('rating AS company_rating', 'title AS company_title', 'slug AS company_slug', 'description as company_description', 'long_description as company_long_description', 'logo AS company_logo', 'email AS company_email', 'phone AS company_phone', 'url AS company_website', 'size AS company_size', 'rating_work AS company_rating_work', 'rating_career AS company_rating_career', 'rating_benefits AS company_rating_benefits', 'rating_balance AS company_rating_balance', 'id_city AS company_city', 'id_country AS company_country','about_us'))
                ->where("j.id = ?", (int) $id);
        if($status == true)
        {
            $sql->where('j.status = 1');
        }
        $job = $this->fetchRow($sql);

        if (!empty($job)) {
            $modelContent = new models_Content;
            $modelJobs = new models_Jobs ;
            $job['countries'] = $this->__getJobCountries($job['id']);
            $job['cities'] = $this->__getJobCities($job['id']);
            $job['counties'] = $modelJobs->__getCountiesByJobId($id);
            $job['departments'] = ($this->__getJobDepartments($job['id']));
            $job['sub_departments'] = $modelJobs->__getJobSubDepartments($id);
            $job['sub_sub_departments'] = $modelJobs->__getJobSubSubDepartments($id);
            $job['industries'] = $this->_getJobIndustries($job['id']);
            $job['skills'] = $this->__getJobSkills($job['id']) + $this->_getCustomJobSkills($job['id']);
            $job['job_views'] = $this->_getJobViewsCount($job['id']);
            $job['company_city'] = $modelContent->getCityById($job['company_city']);
            $job['company_country'] = $modelContent->getCountryById($job['company_country']);
            $job['cities_finaland_countries'] = $this->_getJobCitiesAndCountries($job['id']);
            $job['projects'] = $modelJobs->_getJobprojectsByJobId($id);
            $job['job_headcount'] = $modelJobs->getJobHeadCountJobId($id)['headcount'];
//            $job['id_lang'] = array_keys($job['id_lang']);
            $job['start'] = date("d-m-Y", strtotime($job['start']));
            $job['end'] = date("d-m-Y", strtotime($job['end']));
            $job['recruiters'] = $modelJobs->__getRecruiters($id);
            $job['id_lang'] = $modelJobs->getJobLanguages($id);
            $job['career_level'] = $this->__getJobCareerLevel($job['career_level']);
            $job['type'] = $this->__getJobTypeLevel($job['type']);
            $job['job_periode'] = $this->__getJobPeriode($job['job_periode']);
            $job['id_currency'] = $this->__getJobCurrency($job['id_currency']);
            $job['bounty_payment_type'] = $this->__getJobPyamentType($job['bounty_payment_type']);
            $job['job_ad_lifetime'] = $this->__getJobBountyDueDate($job['job_ad_lifetime']);
            $job['in_favourite'] = $this->__jobIsFavourites($job['id']);
            $job['has_job_bounty'] = $this->checkJobBounty($job);


            $job['job_url'] = Zend_Auth::getInstance()->hasIdentity() ? WebApp_Tools::createJobSlug($job) .
                "?track=" . md5(Zend_Auth::getInstance()->getIdentity()->id) .
                "&ts=" . uniqid(time()) .
                '&source=' . $this->getSocilaMediaIdByType('website') :
                WebApp_Tools::createJobSlug($job)
            ;

            $job['social']['facebook_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                "https://www.facebook.com/sharer.php?u=". WebApp_Tools::createJobSlug($job).
                "?track=".md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('facebook') :
                "/login";
            $job['social']['unique_facebook_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                WebApp_Tools::createJobSlug($job).
                "?track=".md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('facebook') :
                "/login";

            $job['social']['linkedin_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                'https://www.linkedin.com/shareArticle?mini=true&url='. WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('linkedin').
                '&title='.$job['title'] :
                "/login" ;

            $job['social']['unique_linkedin_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                 WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('linkedin') :
                "/login" ;

            $job['social']['twitter_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                'http://www.twitter.com/share?url='.WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('twitter').
                '&text=Check out this great Job posted on Talentspotting:'.$job['title'] :
                "/login" ;

            $job['social']['unique_twitter_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('twitter')  :
                "/login" ;

            $job['social']['email_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                'mailto:?subject='.nl2br('Aplică acum&amp;body=Salut %20%0A %20%0A %20%0A Avem o poziție vacantă de '.strtoupper($job['title']).' aici: %20%0A').
                WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "%26ts=" . uniqid(time()) .
                '%26source='.$this->getSocilaMediaIdByType('email') .'%20%0A %20%0A %20%0A Succes!' :
                "/login";

            $job['social']['unique_email_url'] = Zend_Auth::getInstance()->hasIdentity() ?
                WebApp_Tools::createJobSlug($job).
                '?track='.md5(Zend_Auth::getInstance()->getIdentity()->id).
                "&ts=" . uniqid(time()) .
                '&source='.$this->getSocilaMediaIdByType('email')  :
                "/login";

            $job_image = ($job['job_public_image'] != "") ? "/resources/files/jobs/".$job['job_public_image'] : "";
            if(is_null($job['job_public_image']) || $job['job_public_image'] == "")
            {
//                $job_image = ($job['company_logo'] != "") ? "/resources/files/jobs/".$job['company_logo'] : "" ;
                $job_image = ($job['company_logo'] != "") ? "/resources/files/companies/original/".$job['company_logo'] : "" ;
            }
            $job['job_public_image'] = $job_image;
            $job['bounty_file'] = ($job['bounty_file'] !="") ? "/resources/files/bounties/".$job['bounty_file'] : "";
            $job['company_logo'] = ($job['company_logo'] !="") ? "/resources/files/companies/original/".$job['company_logo'] : "";

            $job['apply'] = (Zend_Auth::getInstance()->hasIdentity()) ? "/apply/{$job['slug']}?id={$job['id']}" :
            '/login?apply='.$job['id'] ;

            $job['applied'] = 0 ;
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $res = $this->userHasApplied($job['id']);
                if(!empty($res))
                {
                    $job['applied'] = 1 ;
                }
            }

            if($flag == true) {
                $job['in_campaign'] = empty($this->jobIsPartOfCampaign($job['id']))
                    ? false : true;
                if($job['in_campaign'])
                {
                    $mCampaign = new models_Campaign();
                    $job['campaign'] = $mCampaign->getCampaignById($this->jobIsPartOfCampaign($job['id'])['id']);
                    if($job['bounty_file'] == "" || is_null($job['bounty_file']))
                    {
                        $job['bounty_file'] = $job['campaign']['bounty_file'];
                    }
                    $job['campaign']['bounty_file'] = $job['campaign']['bounty_file'];
                    $jobs = $job['campaign']['jobs'] ;
                    $job['other_jobs_campaign'] = [] ;
                    if(!empty($jobs)) {
                        foreach ($jobs as $jb) {
                            if($jb['id'] == $job['id']) continue;
                            $other_jobs_campaign = $this->getJobById($jb['id'],false,true);
//                            $job['other_jobs_campaign'][]
                            if(!empty($other_jobs_campaign))
                            {

                                $job['other_jobs_campaign'][] = $other_jobs_campaign ;
                            }
                        }
                    }
                    $job['other_jobs_campaign'];

                }
            }

            if(empty($this->userHasAccessToCampaign()))
            {
                $job['in_campaign'] = false;
                unset($job['campaign']) ;
                unset($job['other_jobs_campaign']) ;
            }
            $job['home_image'] = $this->getJobImage($job);
        }

        return $job;
    }

    public function getJobBySlug($slug) {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('*'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('id_lang', 'id_job', 'title', 'tagline', 'description', 'requirements', 'bounty_requirements', 'slug', 'responsabilities'))
                ->join(array('c' => 'companies'), "j.id_company = c.id", array('rating AS company_rating', 'title AS company_title', 'slug AS company_slug', 'description as description_description', 'long_description as company_long_description', 'logo AS company_logo', 'email AS company_email', 'phone AS company_phone'))
                ->where("jl.slug = ?", (string) $slug)
                ->where("jl.id_lang = ?", $this->id_lang);
        $job = $this->fetchRow($sql);

        if (!empty($job)) {
            $job['cities'] = $this->_getJobCities($job['id']);
            $job['departments'] = $this->_getJobDepartments($job['id']);
            $job['industries'] = $this->_getJobIndustries($job['id']);
            $job['skills'] = $this->_getJobSkills($job['id']);
            $job['job_views'] = $this->_getJobViewsCount($job['id']);
        }

        return $job;
    }

    /*
      save
     */

    public function saveJob($data) {
        if ($data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->update($data, "id = {$id}", "skills");
        } else {
            $this->insert($data, "skills");
            $id = $this->lastInsertId();
            $this->_stats->setGamificationPoint($this->_user->id, "upload_job");
        }

        return $id;
    }

    /*
      misc
     */

    private function _getJobsCities($jobs, $ids) {
        $sql = $this->select()
                ->from(array('c' => "cities"), array("title as city", "id as id_city"))
                ->join(array("co" => "counties"), "c.id_county = co.id", array("title as country", "id as id_county"))
                ->join(array("jc" => "job_cities"), "jc.id_city = c.id", array("id_job"))
                ->where("jc.id_job IN (" . implode(",", $ids) . ")");
        $data = $this->fetchAll($sql);
        foreach ((array) $data as $row) {
            $jobs[$row['id_job']]['cities'][$row['id_city']] = $row['city'];
            $jobs[$row['id_job']]['countries'][$row['id_country']] = $row['country'];
            $jobs[$row['id_job']]['location'][$row['id_city']] = $row['city'] . ", " . $row['country'];
        }

        return $jobs;
    }

    /**
     * @param $result
     * @return array
     */
    public function formatHomeJobs($result)
    {
        $jobs = [];
        foreach ($result as $key => $value) {
//            $jobs[$value['id_job']] = $this->getJobById($value['id_job'], true, true);
            array_push($jobs,$this->getJobById($value['id_job'], true, true));
        }
        return $jobs;
    }

    private function _getJobCountries($id) {
        $sql = $this->select()
                ->from(array("c" => "countries"), array("id", "title"))
                ->join(array("ci" => "cities"), "c.id = ci.id_county", array())
                ->join(array("jc" => "job_cities"), "ci.id = jc.id_city")
                ->where("jc.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function __getJobCountries($id) {
        $sql = $this->select()
            ->from(array("c" => "counties"), array())
            ->join(array("ci" => "cities"), "c.id = ci.id_county", array())
            ->join(array("jc" => "job_cities"), "ci.id = jc.id_city",array())
            ->join(array('con' => 'countries'),"con.id=c.id_country",array('id','title'))
            ->where("jc.id_job = ?", $id);
        return $this->fetchAll($sql);
    }

    private function _getJobCities($id) {
        $sql = $this->select()
                ->from(array("c" => "cities"), array("id", "title"))
                ->join(array("jc" => "job_cities"), "c.id = jc.id_city")
                ->where("jc.id_job = ?", $id);
        return ($this->fetchPairs($sql) + $this->_getJobCustomCities($id));
    }

    public function __getJobCities($id) {
        $sql = $this->select()
            ->from(array("c" => "cities"), array("id", "title"))
            ->join(array("jc" => "job_cities"), "c.id = jc.id_city",array())
            ->where("jc.id_job = ?", $id);
        return ($this->fetchAll($sql));
    }

    public function _getJobCustomCities($id) {
        $sql = $this->select()->from(array("jc" => "custom_job_cities"), array("id_city"))
                ->join(array("c" => "custom_cities"), "jc.id_city = c.id", array("title"))
                ->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function _getJobCitiesAndCountries($id) {
        $sql = $this->select()
                ->from(array("c" => "cities"), array("id", "title AS city"))
                ->join(array("co" => "countries"), "c.id_county = co.id", array("title AS country"))
                ->join(array("jc" => "job_cities"), "c.id = jc.id_city")
                ->where("jc.id_job = ?", $id);
        return $this->fetchAll($sql);
    }

    private function _getJobDepartments($id) {
        $sql = $this->select()
                ->from(array("d" => "departments"), array("id"))
                ->join(array("dl" => "departments_lang"), "d.id = dl.id_department", array("title"))
                ->join(array("jd" => "job_departments"), "d.id = jd.id_department")
                ->where("jd.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function __getJobDepartments($id) {
        $sql = $this->select()
            ->from(array("d" => "departments"), array("id"))
            ->join(array("dl" => "departments_lang"), "d.id = dl.id_department", array("title"))
            ->join(array("jd" => "job_departments"), "d.id = jd.id_department",array())
            ->where("jd.id_job = ?", $id);
        return $this->fetchAll($sql);
    }


    private function _getCustomJobDepartments($id) {
        $sql = $this->select()
            ->from(array("d" => "custom_departments"), array("id","title"))
            ->join(array("jd" => "custom_job_departments"), "d.id = jd.id_department")
            ->where("jd.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function __getCustomJobDepartments($id) {
        $sql = $this->select()
            ->from(array("d" => "custom_departments"), array("id","title"))
            ->join(array("jd" => "custom_job_departments"), "d.id = jd.id_department")
            ->where("jd.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function _getJobIndustries($id) {

        $sql = $this->select()
                ->from(array("i" => "industries"), array("id"))
                ->join(array("il" => "industries_lang"), "i.id = il.id_industry", array("title"))
                ->join(array("ji" => "job_industries"), "i.id = ji.id_industry")
                ->where("ji.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function _getJobSkills($id) {
        $sql = $this->select()
                ->from(array("s" => "skills"), array("id"))
                ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title"))
                ->join(array("js" => "job_skills"), "s.id = js.id_skill")
                ->where("js.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function __getJobSkills($id) {
        $sql = $this->select()
            ->from(array("s" => "skills"), array("id"))
            ->join(array("sl" => "skills_lang"), "s.id = sl.id_skill", array("title"))
            ->join(array("js" => "job_skills"), "s.id = js.id_skill",array())
            ->where("js.id_job = ?", $id);
        return $this->fetchAll($sql);
    }

    private function _getCustomJobSkills($id) {
        $sql = $this->select()
                ->from(array("s" => "custom_skills"), array("id", "title"))
                ->join(array("cjs" => "custom_job_skills"), "cjs.id_skill = s.id AND cjs.id_company = s.id_company", array())
                ->where("cjs.id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    private function _getJobViewsCount($id) {
        $sql = $this->select()->from(array("jv" => "job_views"), array("COUNT(id) as total"))->where("id_job = ?", $id);
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function authenticate($email, $password) {
        return $this->_modelUser->authenticate($email, $password);
    }

    public function authenticateLinkedin($email) {
        return $this->_modelUser->authenticateLinkedin($email);
    }

    public function getIdViewByShareHash($hash)
    {
        $sql = $this->select()->from(['js' => 'jobs_share'],['id as id_share'])
            ->join(['jb' => 'job_views'],'jb.id_jobs_share=js.id',['id as id_view'])
            ->where('js.unique =?',$hash)
            ->order('js.date DESC')->limit(1);
        return $this->fetchRow($sql);
    }


    public function getIdShareByIdView($id_view)
    {
        $sql = $this->select()->from(['jv' => 'job_views'],['*'])->where('jv.id =?',(int) $id_view);
        return $this->fetchRow($sql);
    }

    public function apply($user, $job, $moveNeeded = false) {

        $campaign_track = new Zend_Session_Namespace("campaign_track");
        $session = new Zend_Session_Namespace("track");
        $data['id_job'] = $job['id'];
//        $data['id_user'] = ($this->_user->id < 1 )? (int) $user['id'] : $this->_user->id;
        $data['id_user'] = (int) $user['id'];
        $data['id_recommender'] = (int) $user['id_parent'];


//        echo '<pre>',print_r($data,1),'</pre>';
//        echo (int) $user['id_parent'].' parentttttttttttttt';
//        echo '<pre>',print_r($this->_user,1),'</pre>';
//        die;

//        $data['id_recommender'] = ($this->getRecommenderFromSession() == 0) ? $this->_user->id_parent : $this->getRecommenderFromSession();
        $data['date'] = date("Y-m-d H:i:s");
        $data['status'] = 1 ;
        if ($data['id_recommender'] == "")
            $data['id_recommender'] = 0;

        $data['status_reason'] = 0;
//        $source = new Zend_Session_Namespace('source');
        $s = new Zend_Session_Namespace('source');
        $data['source'] = ($s->source > 0 ) ? $s->source : false ;
        if (isset($campaign_track->data)) {

//            $decoded = (array) json_decode($_COOKIE[$job['id']]);
//            $data['source'] = (isset($this->getSourceByName($decoded['source'])['id'])) ? $this->getSourceByName($decoded['source'])['id'] : $this->getSourceByName('Website')['id'];
            $data['source'] = $campaign_track->data['source'];
            if (!$data['source']) {
                $data['source'] = $this->getSourceByName('website')['id'];
            }
        } else {

            $data['source'] = $this->getSourceByName('website')['id'];
        }
        $rec_id = $this->checkRecommnederContract($data);
        if($rec_id != false) $data['id_recommender'] = $rec_id;
//        $data['id_recommender'] = ($data['id_recommender']  > 0) ? $data['id_recommender'] : $this->getUserBytrack($campaign_track->data['track']);
//        $data['id_recommender'] = (int) $data['id_recommender'];
        $campaign_track = new Zend_Session_Namespace("campaign_track");
        $data['id_view'] = 0 ;
//        echo '<pre>';print_r($_REQUEST);
//        echo '<pre>';print_r($campaign_track->data);
//        die;
        if(is_array($campaign_track->data) && count($campaign_track->data) > 0)
        {
            $id_job_session = $campaign_track->data['id_job'];
            $id_view = ($campaign_track->data['id_view'] > 0) ? $campaign_track->data['id_view'] : 0 ;
            if($data['id_job'] == $id_job_session)
            {
                $data['id_view'] = $id_view;
                $hash = $campaign_track->data['ts'];

                if($data['id_view'] < 1)
                {
                    if($this->getIdViewByShareHash($hash)['id_view'] > 0)
                    {
                        $data['id_view'] = $this->getIdViewByShareHash($hash)['id_view'];
                        $campaign_track->data['id_view'] = $data['id_view'];
                    }
                }
            }
        }

        $data['id_share'] = 0 ;
        if($data['id_view'] > 0)
        {
            if(!empty($this->getIdShareByIdView($data['id_view'])))
            {
                $data['id_share'] = $this->getIdShareByIdView($data['id_view'])['id_jobs_share'] ;
            }
        }
        $this->insert($data, "job_applications");
        $id_application = $this->lastInsertId();
        $this->_modelUser->setjobApplicationStatusHistory(0,
            $id_application,
            $data['status'],
            $data['status_reason'],"");
        $this->insert(['id_company' => $job['id_company'],
            "id_job" => $data['id_job'],
            'id_recommender' => $data['id_recommender'],
            'id_user' => $data['id_user'],
            'message' => "New Applicant",
            "date" => $data['date']], "notification");
        $this->insert(array("id_user" => $data['id_user'], "id_company" => $job['id_company']), "company_unblocked_cvs");


        if ($jobIsPartOfCampain = $this->jobIsPartOfCampaign($job['id'])) {;
            $id_view = $campaign_track->data['id_view'] > 0 ? $campaign_track->data['id_view'] : 0  ;
            if($jobIsPartOfCampain)
            {
                $id_campaign = $jobIsPartOfCampain['id'];
                $campaign_actions = $this->getGamificationActionsByCampaignId($id_campaign);
                if (!empty($campaign_actions))
                {
                    $action = "new_applicants";
                    // 3 - Check  the id of $actions
                    $result = $this->getActionIDbyName($action);
                    $action_data = $this->getActionDataByCampainIdActionId($result['id'],$id_campaign);
                    if($action_data['active']) {
                        if($this->_model->checkValidation($action_data,$hash))
                        {
                            $d['id_user'] = $data['id_recommender'];
                            $d['value'] = $action_data['points'];
                            $d['id_campaign'] = $action_data['id_campaign'];
                            $d['id_job'] = $data['id_job'];
                            $d['id_action'] = $action_data['id'];
                            $d['id_job_view'] = $id_view  ;
                            $modeStats = new models_Stats;
                            $modeStats->saveCampaignReport($d);
                        }
                    } else {
                        $d['id_user'] = $data['id_recommender'];
                        $d['value'] = $action_data['points'];
                        $d['id_campaign'] = $action_data['id_campaign'];
                        $d['id_job'] = $data['id_job'];
                        $d['id_action'] = $action_data['id'];
                        $d['id_job_view'] = $id_view  ;
                        $modeStats = new models_Stats;
                        $modeStats->saveCampaignReport($d);
                    }
//
//
//                    echo '<pre>';
//                    print_r($action_data);
//                    echo $id_campaign;
//                    die;
                }

            }

//            $this->insert(array("id_campaign" => $campaign['id'], "id_user" => (int) $this->_user->id, "action" => "application", "value" => 1), "campaign_reports_detailed");
//            $this->_stats->setCampaignReport($data['id_recommender'], "new_applicants", $job['id']);
        }
//        else {
//            $this->_stats->setGamificationPoint($data['id_recommender'], "new_applicants", $job['id']);
//        }


//        echo '<pre>',print_r($data,1),'</pre>';
//        echo (int) $user['id_parent'].' parentttttttttttttt';
//        echo '<pre>',print_r($this->_user,1),'</pre>';
//        die;
        if ($data['id_recommender'] > 0) {
            $this->_stats->setGamificationPoint($data['id_recommender'], "new_applicants", $job['id']);
            if($session->track) {
                $track = $session->track;
                $this->update(array("id_user" => $data['id_user']), "track = '{$track}'", "job_recommendations");
            }
        }

        $userObject =  Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
        $userObject->JobApplicationEmail($job);
        if ($data['id_recommender'] > 0) {
            $userObject->notifyRecommenderJobApplicationEmail($data);
        }

        $userObject->notifyRecruiterJobApplicationEmail($data);
//        $transport = null;
//        if (!empty($this->_settings['email_smtp'])) {
//            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//        }
//
//        $emailComp        $smarty = new Smarty;
//        $emailCompanyBody = $smarty->fetch('eval:' . $emailCompany['content']);
//        $emailCompanySubject = $smarty->fetch('eval:' . $emailCompany['subject']);
//        $template = $this->_modelCompany->getEmailTemplateContainerByIdCompany($this->_settings['sitename'])any = $this->_modelContent->getEmailByType(26);
//;
//        $admins = $this->_modelUser->getAllAdminsCompany();
//        $company = $this->_modelCompany->getCompanyById($job['id_company']);
//        if($company['notification'])
//            {
//                foreach ($admins as $index => $admin)
//                {
//                $mail = new WebApp_Mail();
//                $mail->addTo($admin['email'], $job['company_title']);
//                $mail->setFrom($this->_modelCompany->getCompanyById(
//                                Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
//
//
//                }
//            } else {
//                $mail = new WebApp_Mail();
//                $mail->addTo($job['company_email'], $job['company_title']);
//                $mail->setFrom($this->_modelCompany->getCompanyById(
//                                Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
//
//
//            }
//                $smarty->assign('subject',$emailCompanySubject);
//                $smarty->assign('content',$emailCompanyBody);
//                $content = $smarty->fetch('eval:'.$template);
//                $mail->setBodyHtml($content);
//                $mail->send($transport);


        return (int) $id_application ;
    }

    public function setJobViews($job, $social = null) {
        $data['id_job'] = $job['id'];
        $data['id_user'] = (int) $this->_user->id;
        $data['date'] = date("Y-m-d H:i:s");
        $data['source'] = $social;
        if (!isset($social)) {
            $data['source'] = "website";
        }
        $this->insert($data, "job_views");
    }

    public function saveUser($data) {
        return $this->_modelUser->saveAndRegister($data);
    }

    public function updateUser($data) {
        return $this->_modelUser->saveUser($data);
    }

    public function saveFromLinkedin($data) {
        return $this->_modelUser->saveFromLinkedin($data);
    }

    public function getUserByEmail($email) {
        return $this->_modelUser->getUserByEmail($email);
    }

    public function getUserDetailsByEmail($email) {
        return $this->_modelUser->getUserDetailsByEmail($email);
    }

    public function getUserByLinkedinId($id) {
        return $this->_modelUser->getUserByLinkedinId($id);
    }

    public function setUserCertifications($data, $id) {
        $certifications = $this->_modelContent->getCertificationsSelect();
        if (!empty($data) && $data['_total'] > 0) {
            foreach ($data['values'] as $value) {
                if (in_array($value['name'], $certifications)) {
                    $id_certification = array_search($value['name'], $certifications);
                } else {
                    $id_certification = $this->_modelContent->saveCertification(array("title" => $value['name']));
                }

                $this->insert(array("id_user" => $id, "id_certification" => $id_certification), "user_certifications");
            }
        }
    }

    public function setUserEducations($data, $id) {
        if (!empty($data) && $data['_total'] > 0) {
            foreach ($data['values'] as $value) {
                $education['id_user'] = $id;
                $education['start'] = $value['startDate']['year'];
                $education['end'] = $value['endDate']['year'];
                $education['title'] = $value['schoolName'];
                $education['field'] = $value['fieldOfStudy'];
                $education['activities'] = $value['activities'];
                $education['degree'] = $value['degree'];

                $this->insert($education, "user_education");
            }
        }
    }

    public function setUserPositions($data, $id) {
        if (!empty($data) && $data['_total'] > 0) {
            foreach ($data['values'] as $value) {
                $position['id_user'] = $id;
                $position['is_current'] = (int) $value['isCurrent'];
                $position['company'] = $value['company']['name'];
                $position['summary'] = $value['summary'];
                $position['title'] = $value['title'];
                $position['start'] = $value['startDate']['year'] . "-" . $value['startDate']['month'];
                $position['end'] = $value['endDate']['year'] . "-" . $value['startDate']['month'];

                $this->insert($position, "user_positions");
            }
        }
    }

    public function setUserPosition($data, $id) {
        if (!empty($data)) {
            $data['id_user'] = $id;
            $this->insert($data, "user_positions");
        }
    }

    public function setUserSkills($data, $id) {
        $skills = $this->_modelContent->getSkillsSelect();
        if (!empty($data) && $data['_total'] > 0) {
            foreach ($data['values'] as $value) {
                if (in_array($value['skill']['name'], $skills)) {
                    $id_skill = array_search($value['skill']['name'], $skills);
                } else {
                    $id_skill = $this->_modelContent->saveSkill(array("title" => $value['skill']['name']));
                }

                $this->insert(array("id_user" => $id, "id_skill" => $id_skill), "user_skills");
            }
        }
    }

    public function setUserConnections($data, $id) {
        if (!empty($data) && $data['_total'] > 0) {
            foreach ($data['values'] as $value) {
                $connections['id_user'] = $id;
                $connections['firstname'] = $value['firstName'];
                $connections['lastname'] = $value['lastName'];
                $connections['location'] = $value['location']['name'];
                $connections['title'] = $value['headline'];
                $connections['linkedin_id'] = $value['id'];
                $connections['industry'] = $value['industry'];
                $connections['image'] = $value['pictureUrl'];
                $connections['profile_url'] = $value['apiStandardProfileRequest']['url'];

                $this->insert($connections, "user_connections");
            }
        }
    }

    public function getUserApplications($id) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array("id_company"))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title", "slug", "id_job"))
                ->join(array("c" => "companies"), "j.id_company = c.id", array("title AS company", "slug AS company_slug"))
                ->joinLeft(array("sp" => "status_options"), "sp.id = ja.status_reason", array("public_title as status_reason_public_title"))
                ->joinLeft(array("s" => "status"), "s.id = ja.status", array("public_title as status_public_title"))
                ->where("ja.id_user = ?", (int) $id)
                ->where("ja.removed = ?", (int) 0);
//                ->where("jl.id_lang = ?", $this->id_lang);
        return $this->fetchAll($sql);
    }

    public function getUserAlerts($id) {
        $sql = $this->select()->from(array("ua" => "user_alerts"))->where("id_user = ?", (int) $id);
        return $this->fetchAll($sql);
    }

    public function getUserAlertsByConditions($id) {
        $sql = $this->select()->from(array("ua" => "user_alerts"), array("id", "conditions"))->where("id_user = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getJobsViewsCounted() {
        $sql = $this->select()->from(array("jv" => "job_views"), array("id_job", "COUNT(id) as total"))->group("id_job");
        return $this->fetchPairs($sql);
    }

    public function getJobsViewsCounter() {
        $sql = $this->select()->from(array("jv" => "job_views"), array("jv.id_job", "COUNT(jv.id) as total"))
                ->join(array("j" => "jobs"), "j.id = jv.id_job",[])
                ->where("j.id_company = ?", $this->_user->id_company)
                ->where("jv.source != ''")
                ->group("jv.id_job");
        return $this->fetchPairs($sql);
    }

    public function getJobsApplicationsCounted() {
        $sql = $this->select()->from(array("ja" => "job_applications"), array("id_job", "COUNT(id) as total"))->group("id_job");
        return $this->fetchPairs($sql);
    }

    public function getJobApplicationsDetails($id) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"))
                ->join(array("u" => "users"), "ja.id_user = u.id", array("u.firstname", "u.lastname", "u.title", "u.status AS user_status", "image"))
                ->joinLeft(array("uu" => "users"), "ja.id_recommender = uu.id", array("CONCAT(uu.firstname, ' ',uu.lastname) AS recommender"))
                ->where("ja.id_job = ?", (int) $id)
//                    ->where("u.type = 1")
                ->order("ja.id DESC");

        return $this->fetchAll($sql);
    }

    public function getJobApplicationById($id) {
        $sql = $this->select()
                        ->from(array("ja" => "job_applications"))->where("ja.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function didIApply($id) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"))
                ->where("ja.id_job = ?", $id)
                ->where("ja.id_user = ?", (int) $this->_user->id);
        return $this->fetchRow($sql);
    }

    public function save($data) {
        $kennexa_data = $data;
        $skills = $data['skills'];
        unset($data['skills']);
        unset($data['countries']);
        unset($data['counties']);
        $cities = $data['cities'];
        unset($data['cities']);

        $industries = $data['industries'];
        unset($data['industries']);

        $departments = array_unique($data['departments']);
        unset($data['departments']);

        $recruiters = $data['recruiters'];
        unset($data['recruiters']);

        $sub_departments = $data['sub_departments'];
        unset($data['sub_departments']);

        $sub_sub_departments = $data['sub_sub_departments'];
        unset($data['sub_sub_departments']);

        $projects = $data['projects'];
        unset($data['projects']);

        $job_headcount = $data['job_headcount'];
        unset($data['job_headcount']);

        $hiring_manager = $data['hiring_manager'];
        unset($data['hiring_manager']);


        $data['start'] = date("Y-m-d", strtotime($data['start']));
        $data['end'] = date("Y-m-d", strtotime($data['end']));
        $data['end'] = ($data['end'] == '1970-01-01') ? '0000-00-00' : $data['end'];
        $data['start'] = ($data['start'] == '1970-01-01') ? '0000-00-00' : $data['start'];

        $data_lang['title'] = $data['title'];
        unset($data['title']);

        $data_lang['tagline'] = $data['tagline'];
        unset($data['tagline']);

        $data_lang['responsabilities'] = $data['responsabilities'];
        unset($data['responsabilities']);

        $data_lang['requirements'] = $data['requirements'];
        unset($data['requirements']);

        $data_lang['description'] = $data['description'];
        unset($data['description']);

        $data_lang['bounty_requirements'] = $data['bounty_requirements'];
        unset($data['bounty_requirements']);

        $data_lang['role'] = $data['role'];
        unset($data['role']);

        $languages = array("0" => 1);
        unset($data['id_lang']);

        if(  $data['is_promoted'] == 'true')
        {
            $data['is_promoted'] = 1;
        } else {
            $data['is_promoted'] = 0;
        }

        $data_lang['slug'] = WebApp_Tools::CreateAlias($data_lang['title']);
        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);

            $this->activeJobIfDateModified($id, $data);
            $this->update($data, "id = {$id}", "jobs");

            $available_languages = $this->getJobLanguages($id);

            foreach ($languages as $lang) {
                if (array_key_exists($lang, $available_languages)) {
                    $this->update($data_lang, "id_job = {$id} AND id_lang = {$lang}", "jobs_lang");
                } else {
                    $data_lang['id_job'] = $id;
                    $data_lang['id_lang'] = $lang;
                    $this->insert($data_lang, "jobs_lang");
                }
            }

            $kennexaApi = new kennexa_api($kennexa_data, kennexa_api::edit);
            $kennexaApi->createJob();
        } else {
            if ($this->_user->id_company > 0) {
                $data['id_company'] = $this->_user->id_company;
            }
            unset($data['id']);
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['status'] = 0 ;
            $this->insert($data, "jobs");
            $id = $this->lastInsertId();
            $this->_stats->setGamificationPoint($this->_user->id, "upload_job");

            foreach ($languages as $lang) {
                $data_lang['id_job'] = $id;
                $data_lang['id_lang'] = $lang;
                $this->insert($data_lang, "jobs_lang");
            }


            $kennexa_data['id_job'] = $id;
            $kennexaApi = new kennexa_api($kennexa_data, kennexa_api::create);
            $kennexaApi->createJob();
        }

        $this->assignRecruiterJobs($recruiters,$id);
        $this->assignHrManagerJobs($hiring_manager,$id);
        if (Zend_Auth::getInstance()->getIdentity()->type == 4) {
            $this->assignRecruiterJobs([Zend_Auth::getInstance()->getIdentity()->id],$id);
        }
        $custom_skills = array_keys($this->_modelContent->getCustomSkillsSelect());
        $custom_skills_array = array_intersect($custom_skills, $skills);
        $skills = array_diff($skills, $custom_skills_array);

        $sql = $this->delete("id_job = {$id}", "job_skills");
        foreach ($skills as $skill) {
            $this->insert(array("id_job" => $id, "id_skill" => $skill), "job_skills");
        }

        $sql = $this->delete("id_job = {$id} AND id_company={$this->_user->id_company}", "custom_job_skills");
        foreach ($custom_skills_array as $skill) {
            $this->insert(array("id_job" => $id, "id_skill" => $skill, "id_company" => $this->_user->id_company), "custom_job_skills");
        }



        $custom_cities = array_keys($this->_modelContent->getCustomCitiesSelect());
        $custom_cities_array = array_intersect($custom_cities, $cities);
        $cities = array_diff($cities, $custom_cities_array);

        $sql = $this->delete("id_job = {$id}", "job_cities");
        foreach ($cities as $city) {
            if($city > 0 ) $this->insert(array("id_job" => $id, "id_city" => $city), "job_cities");
        }

        $sql = $this->delete("id_job = {$id} AND id_company = {$this->_user->id_company}", "custom_job_cities");
        foreach ($custom_cities_array as $city) {
            $this->insert(array("id_job" => $id, "id_city" => $city, "id_company" => $this->_user->id_company), "custom_job_cities");
        }

        $sql = $this->delete("id_job = {$id}", "job_industries");
        foreach ((array) $industries as $industry) {
            $this->insert(array("id_job" => $id, "id_industry" => $industry), "job_industries");
        }


        $custom_departments = array_keys($this->_modelContent->getCustomDepartmentsSelect());
        $custom_departments_array = array_intersect($custom_departments, $departments);
        $departments = array_diff($departments, $custom_departments_array);


        $sql = $this->delete("id_job = {$id}", "job_departments");
        foreach ($departments as $department) {
            $this->insert(array("id_job" => $id, "id_department" => $department), "job_departments");
        }


        $sql = $this->delete("id_job = {$id}", "job_sub_departments");
        foreach ($sub_departments as $sub_department) {
            $this->insert(array("id_job" => $id, "id_sub_department" => $sub_department), "job_sub_departments");
        }

        $sql = $this->delete("id_job = {$id}", "job_sub_sub_departments");
        foreach ($sub_sub_departments as $sub_sub_department) {
            $this->insert(array("id_job" => $id, "id_sub_sub_department" => $sub_sub_department), "job_sub_sub_departments");
        }

        $sql = $this->delete("id_job = {$id}", "job_headcount");
        $this->insert(array("id_job" => $id, "headcount" => $job_headcount), "job_headcount");

        $sql = $this->delete("id_job = {$id}", "job_projects");
        foreach ($projects as $project) {
            $this->insert(array("id_job" => $id, "id_project" => $project), "job_projects");
        }


        $sql = $this->delete("id_job = {$id} AND id_company ={$this->_user->id_company}", "custom_job_departments");
        foreach ($custom_departments_array as $department) {
            $this->insert(array("id_job" => $id, "id_department" => $department, "id_company" => $this->_user->id_company), "custom_job_departments");
        }


        if ($_FILES['bounty_file']['name'] != "") {
            move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $_FILES['bounty_file']['name']);
            $this->update(array("bounty_file" => $_FILES['bounty_file']['name']), "id = {$id}", "jobs");
        }
        if ($_FILES['job_public_image']['name'] != "") {
            move_uploaded_file($_FILES['job_public_image']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/jobs/" . $_FILES['job_public_image']['name']);
            $this->update(array("job_public_image" => $_FILES['job_public_image']['name']), "id = {$id}", "jobs");
        }
        return $id;
    }

    public function getJobSkills($id) {
        $sql = $this->select()->from(array("js" => "job_skills"), array("id_skill", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getCustomJobSkills($id) {
        $sql = $this->select()->from(array("js" => "custom_job_skills"), array("id_skill", "id_job"))->where("id_job = ?", $id)->where("id_company =?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getJobCities($id) {
        $sql = $this->select()->from(array("jc" => "job_cities"), array("id_city", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getCountiesByJobId($id)
    {
        $counties = [];
        $cities = $this->getJobCities($id);
        foreach ($cities as $key => $city) {
            $sql = $this->select()
                ->from(['c' => 'cities'],['id_county'])
                ->where('c.id=?',(int) $key);
            $counties[] = $this->fetchRow($sql)['id_county'];
        }
        return (array_values(array_unique($counties)));
        return $counties;
    }

    public function __getCountiesByJobId($id)
    {
        $counties = [];
        $cities = $this->getJobCities($id);
//        echo '<pre>'; print_r($cities);die;
        foreach ($cities as $key => $city) {
            $sql = $this->select()
                ->from(['c' => 'cities'],[])
                ->join(['cc' => 'counties'],"cc.id=c.id_county",array('title','cc.id'))
                ->where('c.id=?',(int) $key);
            $counties[] = $this->fetchRow($sql);
        }
        return (array_values(array_unique($counties)));
        return $counties;
    }

    public function getCountriesByCountyIds($counties)
    {
        $countries = [];
        foreach ($counties as $county)
        {
            $sql = $this->select()
                 ->from(['c'=> 'counties'],['id_country'])
                ->where('c.id =?',(int) $county);
            $countries[] = $this->fetchRow($sql)['id_country'];
        }
        return $countries;
    }

    public function getJobCustomCities($id) {
        $sql = $this->select()->from(array("jc" => "custom_job_cities"), array("id_city", "id_job"))->where("id_job = ?", $id)->where("id_company =?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getJobDepartments($id) {
        $sql = $this->select()->from(array("jd" => "job_departments"), array("id_department", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getCustomJobDepartments($id) {
        $sql = $this->select()->from(array("jd" => "custom_job_departments"), array("id_department", "id_job"))->where("id_job = ?", $id)->where("id_company =?", $this->_user->id_company);
        return $this->fetchPairs($sql);
    }

    public function getJobIndustries($id) {
        $sql = $this->select()->from(array("jd" => "job_industries"), array("id_industry", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getJobLanguages($id) {
        $sql = $this->select()->from(array("jl" => "jobs_lang"), array("id_lang", "id_job"))->where("id_job = ?", $id);
        return $this->fetchPairs($sql);
    }

    public function getCompanyById($id) {
        $modelCompanies = new models_Company;
        return $modelCompanies->getCompanyById($id);
    }

    public function getLatestJobPosts() {
        $sql = $this->select()->from(array("j" => "jobs"), array("COUNT(id) AS total"))->where("updated >= '{$this->_user->last_login}'");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getBountiesUnpaid() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("SUM(bounty) AS total"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array())
                ->where("ja.status = 4")
                ->where("ja.status_reason = 5")
                ->where("j.bounty_paid = 0");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getTodayJobsApplications() {
        $today = date("Y-m-d");

        $sql = $this->select()
                ->from(array("j" => "jobs"), array("COUNT(ja.id) AS total"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array())
                ->where("ja.date BETWEEN '{$today} 00:00:00' AND '{$today} 23:59:59'");
        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }
        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function getTodayJobsApplicationsDetailed($total) {
        $today = date("Y-m-d");

        $sql = $this->select()
                ->from(array("j" => "jobs"), array("COUNT(ja.id) AS total", "id as id_job"))
                ->join(array("c" => "companies"), "j.id_company = c.id", array("title as company_name"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array())
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title"))
                ->where("ja.date BETWEEN '{$today} 00:00:00' AND '{$today} 23:59:59'")
                ->where("jl.id_lang = ?", $this->id_lang)
                ->group(["j.id","c.title","jl.title"]);
        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }
        $data = $this->fetchAll($sql);
        $applications = [];
        foreach ((array) $data as $row) {
            $applications[$row['id_job']]['title'] = $row['title'];
            $applications[$row['id_job']]['company_name'] = $row['company_name'];
            $applications[$row['id_job']]['percent'] = intval($row['total'] / $total * 100);
        }

        return (array) $applications;
    }

    public function getMonthJobsApplications() {
        $start = date("Y-m") . "-01 00:00:00";
        $end = date("Y-m") . "-31 23:59:59";

        $sql = $this->select()
                ->from(array("j" => "jobs"), array())
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("ja.id", "COUNT(ja.id) AS total"))
                ->group(["ja.id", "DAY(ja.date)"])
                ->where("ja.date BETWEEN '{$start}' AND '{$end}'");
        if ($this->_user->type == 2) {
            $sql->where("j.id_company = '{$this->_user->id_company}'");
        }

        return $this->fetchPairs($sql);
    }

    public function getJobClosedPositions($id = null, $filters = array()) {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("*"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array(""))
                ->where("ja.status = 4")
                ->where("status_reason = 5");
        if (null !== $id) {
            $sql->where("ja.id_job = ?", $id);
        }

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($value != "") {
                    switch ($key) {
                        case "id_department":
                            $sql->join(array("jd" => "job_departments"), "ja.id_job = jd.id_job", array(""));
                            $sql->where("jd.id_department = {$value}");
                            break;

                        case "id_skill":
                            $sql->join(array("js" => "job_skills"), "ja.id_job = js.id_job", array(""));
                            $sql->where("js.id_skill = {$value}");
                            break;

                        case "id_industry":
                            $sql->join(array("ji" => "job_industries"), "ja.id_job = ji.id_job", array(""));
                            $sql->where("ji.id_industry = {$value}");
                            break;

                        case "start_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.start >= '{$date}'");
                            break;

                        case "end_date":
                            $date = date("Y-m-d", strtotime($value));
                            $sql->where("j.end <= '{$date}'");
                            break;

                        case "status":
                            if ($value == 0) {
                                $sql->where("j.end < '" . date("Y-m-d") . "' OR j.status = 0");
                            } else if ($value == 1) {
                                $sql->where("j.end >= '" . date("Y-m-d") . "' AND j.status = 1");
                            }
                            break;
                    }
                }
            }
        }

        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $positions['month'][date("F", strtotime($row['updated']))] += 1;
            $positions['year'][date("Y", strtotime($row['updated']))] += 1;
            $positions['timing'][] += strtotime($row['updated']) - strtotime($row['date']);
        }

        if (!empty($positions['timing'])) {
            $positions['avg'] = intval(array_sum($positions['timing']) / count($positions['timing']));
        } else {
            $positions['avg'] = 0;
        }

        return $positions;
    }

    public function getJobsSelect() {
        $sql = $this->select()
                ->from(array('j' => 'jobs'), array('id'))
                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
                ->order('j.start DESC');
        if ($this->_user->type != 3) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        return $this->fetchPairs($sql);
    }

    public function getJobsSelectActive() {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id'))
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
            ->order('j.start DESC')->where("j.status = 1");
            $sql->where("j.id_company = ?", $this->_user->id_company);

        return $this->fetchPairs($sql);
    }

    public function getRemainedJobsInCampaigns() {
        $sql = $this->select()->from(array("c" => "campaigns"), array())->where("c.id_Company =?", (int) $this->_user->id_company)
                ->join(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array('id_job'));
        return $this->fetchPairs($sql);
    }

    public function getRemainedJobsInCampaignsActive() {
        $sql = $this->select()->from(array("c" => "campaigns"), array())
//            ->where("c.id_Company =?", (int) $this->_user->id_company)
            ->join(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array('id_job'))
            ->join(['j' => 'jobs'],'j.id=cj.id_job',[])->where('j.status =1')
            ->where("c.status = 1");
        return $this->fetchPairs($sql);
    }

    public function export($data, $file) {
        return $this->_modelUser->export($data, $file);
    }

    public function getDepartmentsSelect() {
        return $this->_modelContent->getDepartmentsSelect();
    }

    public function getSkillsSelect() {
        return $this->_modelContent->getSkillsSelect();
    }

    public function getIndustriesSelect() {
        return $this->_modelContent->getIndustriesSelect();
    }

    public function getCurrentApplications() {
        $sql = $this->select()
                ->from(array("ja" => "job_applications"), array("date", "id AS id_app", "id_job"))
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title"))
                ->join(array("u" => "users"), "ja.id_user = u.id", array("firstname", "lastname"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("viewed = 0")
                ->where("ja.date = ja.updated");

        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }

        return $this->fetchAll($sql);
    }

    public function getCurrentNotifications() {
        $sql = $this->select()
                ->from(array("n" => "notifications"), array("id AS id_notification", "message", "id_company", "date", "url", "date"))
                ->join(array("c" => "companies"), "n.id_company = c.id", array("*"))
                ->where("viewed = 0")
//                    ->where("n.type ='notification'")
                ->limit(20);

        if ($this->_user->type == 2) {
            $sql->where("id_company = ?", $this->_user->id_company);
            $sql->where("target = 'company'");
        } else {
            $sql->where("target = 'admin'");
        }

        return $this->fetchAll($sql);
    }

    public function getCurrentNotification() {
        $sql = $this->select()
                ->from(array("n" => "notification"), array("n.id"))
                ->join(array("u" => "users"), 'u.id = n.id_user', array("u.firstname", "u.lastname", "u.image"))
//                ->join(array("j" => "jobs"),'j.id= n.id_job AND j.id_company = n.id_company',array("*"))
                ->join(array("jl" => "jobs_lang"), 'jl.id_job= n.id_job', array("jl.title as job_title"))
                ->where("n.id_recommender =?", (int) $this->_user->id)
                ->where("viewed = 0")
                ->group(["n.id"])
                ->limit(20);
        return $this->fetchAll($sql);
    }

    public function getCurrentPayments() {
        $sql = $this->select()
                ->from(array("n" => "notifications"), array("id AS id_notification", "message", "id_company", "date", "url"))
                ->join(array("c" => "companies"), "n.id_company = c.id", array("*"))
                ->where("viewed = 0")
                ->where("n.type = 'payment'")
                ->where("target = 'admin'")
                ->limit(20);
        return $this->fetchAll($sql);
    }

    public function getNotificationById($id) {
        $sql = $this->select()->from(array("n" => "notifications"))->where("id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getTooltipViews() {
        $modelUser = new models_User;
        return $modelUser->getTooltipViews();
    }

    public function userHasApplied($id, $id_user = null) {
        $sql = $this->select()->from(array("ja" => "job_applications"))->where("id_job = ?", $id);
        if (null !== $id_user) {
            $sql->where("id_user =?", $id_user);
        } else {
            $sql->where("id_user =?", $this->_user->id);
        }
        $data = $this->fetchRow($sql);

        return !empty($data);
    }

    public function getUserPositions() {
        return $this->_modelUser->getUserPositions($this->_user->id);
    }

    public function getUserLastPositions() {
        $sql = $this->select()->from(array("up" => "user_positions"));
            $sql->where("id_user = ?", (int) $this->_user->id)
            ->order('up.end DESC');

        return $this->fetchAll($sql);
    }

    public function getUserLastPositionsById($id_user = null) {
       $sql = $this->select()->from(array("up" => "user_positions"));
       $sql->where("id_user = ?", (int) $id_user)
           ->order('up.end DESC');

       $result = $this->fetchAll($sql);
       $jb = "(empty)";
       foreach($result  as $index => $job)
           {
               if($job['is_current']) {
                   $jb = $job['title'];
                   break;
               }

           }
       if(!empty($result) && $jb == "(empty)")
           {
               $jb = $result[0]['title'];
           }
       return $jb;
   }

public function getUserRecommenderById($id_user = null) {
       $sql = $this->select()->from(array("u" => "users"));
       $sql->where("u.id = ?", (int) $id_user)
            ->join(array("c" => "users"), "u.id_parent = c.id", array("firstname as recommender_fname","lastname as recommender_lname"));

       $result = $this->fetchAll($sql);
       $jb = "(empty)";
       foreach($result  as $index => $rec)
           {
               if($rec['recommender_fname']) {
                   $jb = $rec['recommender_lname'].' '.$rec['recommender_fname'];
                   break;
               }

           }
       if(!empty($result) && $jb == "(empty)")
           {
               $jb = $result[0]['title'];
           }
       return $jb;
   }

    public function setUserDefaultAlerts($user, $skills) {
        if ($user['location'] != "") {
            $countries = $this->_modelContent->getCountriesSelect();
            foreach ($countries as $key => $value) {
                if (stripos($user['location'], $value) !== false) {
                    $country_alert[] = $key;
                }
            }

            if (!empty($country_alert)) {
                $conditions['countries'] = $this->_modelContent->getCountriesSelect($country_alert);
                $this->insert(array("id_user" => $user['id'], "conditions" => serialize($conditions), "created" => date("Y-m-d H:i:s")), "user_alerts");
            }
        }

        if (!empty($skills)) {
            $default_skills = $this->_modelContent->getSkillsInAlerts();
            if (!empty($skills) && $skills['_total'] > 0) {
                foreach ($skills['values'] as $sk) {
                    foreach ($default_skills as $skill) {
                        if (stripos($sk, $skill['title']) !== false) {
                            $skills_alert[] = $skill['id'];
                        }
                    }
                }
            }

            if (!empty($skills_alert)) {
                $conditions['skills'] = $this->_modelContent->getSkillsSelect($skills_alert);
                $this->insert(array("id_user" => $user['id'], "conditions" => serialize($conditions), "created" => date("Y-m-d H:i:s")), "user_alerts");
            }
        }
    }

    private function _updateBannerImage($id) {
        if ($_FILES['image']['name'] != "") {
            $image = $_FILES['image'];
            $file = $id . "_" . $this->id_lang . "_" . $image['name'];

            WebApp_UploadImage::saveConstrainedFixedImage(340, 240, $image['tmp_name'], "left", ABSOLUTE_PATH . "/public/resources/files/banners/" . $file);

            $this->update(array("image" => $file), "id_banner = {$id} AND id_lang = {$this->id_lang}", "banners_lang");
            return $file;
        }
    }

    public function setEmailRecommendation($data) {
        $modelUser = new models_User;

        foreach ($data['email'] as $key => $items) {
            $data['id_recommender'] = $this->_user->id;
            $data['id_user'] = $modelUser->matchUserByEmail($data['email'][$key]);
            $data['track'] = $modelUser->generateTrackingNumber();
            $data['date'] = date("Y-m-d H:i:s");

            $sql = $this->select()
                    ->from(array("j" => "job_recommendations"), array("COUNT(id) as total"))
                    ->where("id_recommender = ?", (int) $data['id_recommender'])
                    ->where("id_user = ?", (int) $data['id_user'])
                    ->where("id_job = ?", (int) $data['id_job']);
            $res = $this->fetchRow($sql);

            if ($res['total'] < 100 && $data['email'][$key] != "") {
                $recommendation['id_recommender'] = $data['id_recommender'];
                $recommendation['id_user'] = $data['id_user'];
                $recommendation['track'] = $data['track'];
                $recommendation['date'] = $data['date'];
                $recommendation['id_job'] = $data['id_job'];
                $recommendation['anonymous'] = $data['anonymous'];
                $recommendation['email'] = $data['email'][$key];
                $recommendation['name'] = $data['name'][$key];
                $recommendation['phone'] = $data['phone'][$key];

                $this->insert($recommendation, "job_recommendations");
//                $this->_stats->setGamificationPoint($this->_user->id, "job_recommend");
                $id = $this->lastInsertId();

                $job = $this->getJobById($data['id_job']);

                $user = $modelUser->getUserDetailsByEmail($data['email'][$key]);
                $contentModel = new models_Content;
                $emailRecommendation = $contentModel->getEmailByType(28);

                $me = Domain_User_UserFactory::createInstance($this->_user->id);
                $me->recommendEmail($job,$data,$user['firstname'],$data['email'][$key]);

//                $transport = null;
//                if (!empty($this->_settings['email_smtp'])) {
//                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//                }
//
//
//
//                $smarty = new Smarty;
//
//                $smarty->assign("user", (array) $this->_user);
//                $smarty->assign("job", (array) $job);
//                $smarty->assign("data", (array) $data);
//                $smarty->assign("settings", (array) $this->_settings);
//                $smarty->assign("name", (array) $user['firstname']);
//                $smarty->assign("email", (array) $data['email'][$key]);
//                $smarty->assign("url", WebApp_Tools::createJobSlug($job));
//                $smarty->assign("subdomain_url", $this->_settings['sitename']);
//                $smarty->assign("bg_color", $this->_modelCompany->getCompanyEmailButtonColor());
//                $emailBody = $smarty->fetch('string:' . $emailRecommendation['content']);
//                $emailSubject = $smarty->fetch('string:' . $emailRecommendation['subject']);
//
//                $template = $this->_modelCompany->getCompanyEmailTemplate();
//
//                $smarty->assign('subject',$emailSubject);
//                $smarty->assign('content',$emailBody);
//
//                $content = $smarty->fetch('eval:'.$template);
//
//
//
//                $mail = new WebApp_Mail();
//                $mail->addTo($data['email'][$key], $data['name'][$key]);
//                $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
//                $mail->setSubject($emailSubject);
//                $mail->setBodyHtml($content);
//                $mail->send($transport);

            }
        }
    }

    public function sendMassRecommendation($id_user, $job) {
        $user = $this->_modelUser->getUserById($id_user);
        $data['track'] = $this->_modelUser->generateTrackingNumber();
        $data['date'] = date("Y-m-d H:i:s");



        $recommendation['id_recommender'] = $this->_user->id;
        $recommendation['id_user'] = $user['id'];
        $recommendation['track'] = $data['track'];
        $recommendation['date'] = $data['date'];
        $recommendation['id_job'] = $job['id'];
        $recommendation['email'] = $user['email'];


        $this->insert($recommendation, "job_recommendations");

        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $contentModel = new models_Content;
        $emailRecommendation = $contentModel->getEmailByType(28);
        $smarty = new Smarty;

        $smarty->assign("user", (array) $this->_user);
        $smarty->assign("job", (array) $job);
        $smarty->assign("data", (array) $data);
        $smarty->assign("settings", (array) $this->_settings);
        $smarty->assign("name", (array) $user);
        $smarty->assign("url", WebApp_Tools::createJobSlug($job));
        $emailBody = $smarty->fetch('string:' . $emailRecommendation['content']);
        $emailSubject = $smarty->fetch('string:' . $emailRecommendation['subject']);

        $mail = new WebApp_Mail();
        $mail->addTo($user['email'], $user['firstname']);
        $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject($emailSubject);
        $mail->setBodyHtml($emailBody);
        $mail->send($transport);

        if ($campaign = $this->jobIsPartOfCampaign($job['id'])) {
            $this->insert(array("id_campaign" => $campaign['id'], "id_user" => (int) $this->_user->id, "action" => "recommendation", "value" => 1), "campaign_reports_detailed");
        }
    }

    public function getRecommenderFromSession() {
        $session = new Zend_Session_Namespace("track");
        $id_job = Zend_Controller_Front::getInstance()->getRequest()->getParam('id');
        if (isset($session->track) && $session->track != "") {
            $sql = $this->select()->from(array("j" => "job_recommendations"))->where("track = ?", (string) $session->track)->limit(1);
            $data = $this->fetchRow($sql);

            if (!empty($data)) {
                $session->track = null;
                return $data['id_recommender'];
            } else {
                $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string) $session->track)->limit(1);
                $data = $this->fetchRow($sql);

                if (!empty($data)) {
                    $session->track = null;
                    return $data['id'];
                } else {
                    Zend_Auth::getInstance()->getIdentity()->id_parent;
                }
            }
        } else if (isset($_COOKIE[$id_job]) && (string) strlen($_COOKIE[$id_job]) == 13) {
            $hash = (string) $_COOKIE[$id_job];
            $sql = $this->select()->from(array("j" => "job_recommendations"))->where("track = ?", (string) $hash)->limit(1);
            $data = $this->fetchRow($sql);

            if (!empty($data)) {
                $session->track = null;
                return $data['id_recommender'];
            } else {
                return Zend_Auth::getInstance()->getIdentity()->id_parent;
            }
        } else {

            return Zend_Auth::getInstance()->getIdentity()->id_parent;
        }
    }

    public function getTrackDetails($track) {
        $sql = $this->select()
                ->from(array("j" => "job_recommendations"))
                ->join(array("u" => "users"), "j.id_recommender = u.id", array("id_company"))
                ->where("j.track = ?", (string) $track);
        return $this->fetchRow($sql);
    }

    public function getHashDetails($hash) {
        $sql = $this->select()
                ->from(array('u' => 'users'))
                ->where(' MD5(u.id) = ?', $hash);
        return $this->fetchRow($sql);
    }

    public function getSimilarJobs($job) {
        if (!empty($job['skills'])) {
            $job_skills = array_keys($job['skills']);
        }

        if (!empty($job['departments'])) {
            $job_departments = array_keys($job['departments']);
        }

        if (!empty($job['industries'])) {
            $job_industries = array_keys($job['industries']);
        }

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $modelMatching = new models_Matching;
            $matchedSkills = $modelMatching->matchUserVsJobSkills((array) $job_skills);
            $jobs = $this->getSimilarJobsForUser($job, $matchedSkills);
        } else {
            $jobs = $this->getSimilarJobsForAll($job, (array) $job_skills, (array) $job_departments, (array) $job_industries);
        }

        if (!empty($jobs)) {
            foreach ($jobs as $row) {
                $ids[] = $row['id'];
                $similar[$row['id']] = $row;
            }

            $similar = $this->_getJobsCities($similar, $ids);
        }

        return (array) $similar;
    }

    public function getSimilarJobsForUser($job, $skills) {
        $sql = $this->select()
                ->from(array("j" => "jobs"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id", array("title", "slug"))
                ->join(array("c" => "companies"), "j.id_company = c.id", array("title AS company_title", "logo AS company_logo", "slug AS company_slug"))
                ->join(array("jc" => "job_cities"), "j.id = jc.id_job", array("id_city"))
                ->join(array("js" => "job_skills"), "j.id = js.id_job", array("id_skill"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job AND ja.id_user != '{$this->_user->id}'", array())
                ->where("j.id != ?", $job['id'])
                ->where("j.status = 1")
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->order('j.is_promoted DESC')
                ->order('j.start DESC')
               // ->group("j.id")
                ->limit(12);
        if (!empty($skills)) {
            $sql->where("id_skill IN (" . implode(",", $skills) . ")");
        }
        return $this->fetchAll($sql);
    }

    public function getSimilarJobsForAll($job, $skills, $job_departments, $job_industries) {
        $sql = $this->select()
                ->from(array("j" => "jobs"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id", array("title", "slug"))
                ->join(array("c" => "companies"), "j.id_company = c.id", array("title AS company_title", "logo AS company_logo", "slug AS company_slug"))
                ->join(array("jc" => "job_cities"), "j.id = jc.id_job", array("id_city"))
                ->joinLeft(array("js" => "job_skills"), "j.id = js.id_job", array("id_skill"))
                ->joinLeft(array("jd" => "job_departments"), "j.id = jd.id_job", array("id_department"))
                ->joinLeft(array("ji" => "job_industries"), "j.id = ji.id_job", array("id_industry"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job AND ja.id_user != '{$this->_user->id}'", array())
                ->where("j.id != ?", $job['id'])
                ->where("j.status = 1")
                ->order('RAND()')
             //   ->group("j.id")
                ->limit(12);
        $where = array();

        if (!empty($skills)) {
            $where[] = "id_skill IN (" . implode(",", $skills) . ")";
        }

        if (!empty($job_departments)) {
            $where[] = "id_department IN (" . implode(",", $job_departments) . ")";
        }

        if (!empty($job_industries)) {
            $where[] = "ji.id_industry IN (" . implode(",", $job_industries) . ")";
        }


        if (!empty($where)) {
            $sql->where(implode(" OR ", $where));
        } else {
            $sql->where("id_company = ?", $job['id_company']);
        }

        return $this->fetchAll($sql);
    }

    public function getSimilarUsersForJob($job, $users = array()) {
        $countries = $this->getJobCountries($job['id']);
        $sql = "SELECT u.id_country, up.id_user, MATCH(up.title) AGAINST ('{$job['title']}' IN BOOLEAN MODE) AS relevance FROM user_positions up, users u WHERE up.id_user = u.id AND MATCH (up.title) AGAINST ('{$job['title']}' IN BOOLEAN MODE) ORDER BY relevance";
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            if (!in_array($row['id_user'], (array) $users)) {
                if (in_array($row['id_country'], $countries)) {
                    $matches[$row['relevance'] * 51][] = $row['id_user'];
                } else {
                    $matches[$row['relevance'] * 50][] = $row['id_user'];
                }

                $users[] = $row['id_user'];
            }
        }

        if (!empty($job['skills'])) {
            foreach ($job['skills'] as $skill => $value) {
                $sql = $this->select()
                        ->from(array("us" => "user_skills"))
                        ->join(array("u" => "users"), "us.id_user = u.id", array("id_country"))
                        ->where("id_skill = ?", $skill);
                $data = $this->fetchAll($sql);

                foreach ((array) $data as $row) {
                    if (!in_array($row['id_user'], (array) $users)) {
                        if (in_array($row['id_country'], $countries)) {
                            $matches[21][] = $row['id_user'];
                        } else {
                            $matches[20][] = $row['id_user'];
                        }

                        $users[] = $row['id_user'];
                    }
                }
            }
        }

        $sql = "SELECT u.id_country, up.id_user, MATCH(up.summary) AGAINST ('{$job['title']}' IN BOOLEAN MODE) AS relevance FROM user_positions up, users u WHERE up.id_user = u.id AND MATCH (up.summary) AGAINST ('{$job['title']}' IN BOOLEAN MODE) ORDER BY relevance";
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            if (!in_array($row['id_user'], (array) $users)) {
                if (in_array($row['id_country'], $countries)) {
                    $matches[$row['relevance'] * 11][] = $row['id_user'];
                } else {
                    $matches[$row['relevance'] * 10][] = $row['id_user'];
                }
                $users[] = $row['id_user'];
            }
        }

        if (!empty($job['industries'])) {
            foreach ($job['industries'] as $industry => $value) {
                $sql = $this->select()
                        ->from(array("us" => "user_positions"))
                        ->join(array("u" => "users"), "us.id_user = u.id", array("id_country"))
                        ->where("id_industry = ?", $industry);
                $data = $this->fetchAll($sql);

                foreach ((array) $data as $row) {
                    if (!in_array($row['id_user'], (array) $users)) {
                        if (in_array($row['id_country'], $countries)) {
                            $matches[$row['relevance'] * 11][] = $row['id_user'];
                        } else {
                            $matches[$row['relevance'] * 10][] = $row['id_user'];
                        }
                        $users[] = $row['id_user'];
                    }
                }
            }
        }

        foreach ((array) $matches as $relevance => $value) {
            foreach ($value as $id) {
                $ids[$id] = $relevance;
            }
        }


        if (!empty($ids)) {
            $sql = $this->select()
                    ->from(array("u" => "users"), array("*", "status AS user_status", "id AS id_user"))
                    ->where("id IN (" . implode(",", array_keys($ids)) . ")")
                    ->where("u.type = 1")
                    ->where("u.id_parent > 0");
            $data = $this->fetchAll($sql);

            foreach ($data as $row) {
                $row['relevance'] = $ids[$row['id']];
                $items[] = $row;
            }

            return $items;
        } else {
            return array();
        }
    }

    public function getJobCountries($id) {
        $sql = $this->select()
                ->from(array("c" => "countries"), array("id"))
                ->join(array("ci" => "cities"), "c.id = ci.id_country", array())
                ->join(array("jc" => "job_cities"), "ci.id = jc.id_city", array())
                ->where("jc.id_job = ?", (int) $id);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $countries[] = $row['id'];
        }

        return (array) $countries;
    }

    public function inactivate($ids) {
        if (!empty($ids)) {
            $this->update(array("status" => 0), "id IN (" . implode(",", $ids) . ")", "jobs");
        }
    }


    public function disableExpired() {
            $act=$this->update(array("status" => 0), "status=1 and (end < now() or start>now())", "jobs");
            return $act;
    }

    public function enableCreated() {
            $act=$this->update(array("status" => 1), "status=0 and end >= now() and start<= now()", "jobs");
            return $act;
    }

    public function activate($ids) {
        if (!empty($ids)) {
            $sql = $this->select()->from(array("j" => "jobs"))->where("id IN (" . implode(",", $ids) . ")");
            $data = $this->fetchAll($sql);

            foreach ((array) $data as $row) {
                $start = date("Y-m-d");
                $end = date("Y-m-d", strtotime("+{$this->_settings['jobs_lifetime'][$row['job_ad_lifetime']]}"));

                $this->update(array("status" => 1), "id = {$row['id']}", "jobs");
            }
        }
    }

    public function jobIsPartOfCampaign($id) {
        $sql = $this->select()
                ->from(array("c" => "campaigns"),['c.id'])
                ->join(array("cj" => "campaign_jobs"),
                    "c.id = cj.id_campaign",
                    array("id_job"))
                ->where("cj.id_job = ?", (int) $id)
                ->where("c.status = 1");
        $campaign =  $this->fetchRow($sql);
        if(empty($campaign)) return false ;
        $cModel = new models_Campaign ;
        return $cModel->getCampaignById($campaign['id']);
    }

    public function getJobsRecommenders() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("id"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title"));
        if ($this->_user->type == 2) {
            $sql->where("j.id_company = ?", $this->_user->id_company);
        }
        $sql->group(["j.id", 'jl.title']);

        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value) {
            $result[$key]['recommenders'] = $this->getJobRecommedersById($value['id']);
        }
        return $result;
    }

    public function getJobRecommedersById($id_job) {
        $sql = $this->select()->from(array("jr" => "job_recommendations"), array("count(id) as recommenders"))
                        ->where("jr.id_job =?", (int) $id_job)->group("jr.id_recommender");
        return count($this->fetchAll($sql));
    }

    public function getMyJobsRecommenders() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("*", "(SELECT COUNT(id) FROM job_applications WHERE id_job = j.id AND id_recommender > 0) AS recommenders"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title"))
                ->join(array("jr" => "job_recommendations"), array())
                ->where("jl.id_lang = ?", $this->id_lang);

        if ($this->_user->type == 2) {
            $sql->where("id_company = ?", $this->_user->id_company);
        }
        $sql->where("jr.id_recommender= ?", $this->_user->id);

        return $this->fetchAll($sql);
    }

    public function duplicateJob($job) {
        $id = $this->save($job);
        $ids[] = $id;
        $this->inactivate($ids);
    }

    public function cshare($id_user, $id_job,$hash,$source) {
        $data['id_user'] = $id_user;
        $data['id_job'] = $id_job;
        $data['date'] = date("Y-m-d H:i:s");
        $data['shares'] = 1;
        $data['views'] = 0;
        $data['unique'] = $hash;
        $data['id_source'] = $source;
        $this->insert($data, 'jobs_share');


    }

    public function updateViewShareUser($track, $id_job) {

        $sql = $this->select()->from(array("js" => "jobs_share"));
          if ($this->getAdapter() instanceof Zend_Db_Adapter_Pdo_Mssql)
            {
            $sql->where("SUBSTRING(sys.fn_sqlvarbasetostr(HASHBYTES('MD5','js.id_user')),3,32) = ?", (string) $track);
            } else
            {
            $sql->where("MD5(js.id_user) = ?", (string) $track);
            }
        $sql->where("id_job = ?", $id_job)->limit(1);
        $data = $this->fetchRow($sql);
//                echo '<pre>';print_r($data);die;
        if ($data['id']) {
            $count = $data['views'] + 1;
            $id_job = $data['id_job'];
            $this->update(array("views" => (int) $count), "id = {$data['id']}", "jobs_share");
        }
    }

    public function setUniqueView($id_job, $track) {
        $sql = $this->select()->from(array("jr" => "job_recommendations"), array("id_recommender AS id_user"))->where("track = ?", $track);
        $id_user = $this->fetchRow($sql)['id_user'];
        $_sql = $this->select()->from(array("gp" => "gamifications_actions"), array("*"))->where("action = ?", "views");
        $gamification_point = $this->fetchRow($_sql);
        $data['id_user'] = $id_user;
        $data['id_item'] = $id_job;
        $data['action'] = $gamification_point['id'];
        $data['points'] = $gamification_point['points'];
        $data['date'] = date("Y-m-d H:i:s");
        $this->insert($data, "gamification_points");
    }

    public function removeJobPicture($id) {
        return $this->update(array("bounty_file" => ""), "id = {$id}", "jobs");
    }
    public function removeJobSharePicture($id) {
        return $this->update(array("job_public_image" => ""), "id = {$id}", "jobs");
    }

    public function addCustomCities($string) {
        $data['title'] = $string;
        $data['id_country'] = 18;
        $data['id_company'] = Zend_Auth::getInstance()->getIdentity()->id_company;
        $data['slug'] = str_replace(" ", "-", strtolower($string));
        $sql = $this->insert($data, "custom_cities");
        $last_insert = $this->lastInsertId();
        $sql_object = $this->select()->from(array("c" => "custom_cities"), array("id", "title"))->where("id =?", $last_insert);
        $data = $this->fetchRow($sql_object);
        $data['source'] = "cities";
        return $data;
    }

    public function addCustomSkills($string) {
        $data['title'] = $string;
        $data['id_company'] = Zend_Auth::getInstance()->getIdentity()->id_company;
        $data['slug'] = str_replace(" ", "-", strtolower($string));
        $sql = $this->insert($data, "custom_skills");
        $last_insert = $this->lastInsertId();
        $sql_object = $this->select()->from(array("c" => "custom_skills"), array("id", "title"))->where("id =?", $last_insert);
        $data = $this->fetchRow($sql_object);
        $data['source'] = "skills";
        return $data;
    }

    public function addCustomDepartments($string) {
        $data['title'] = $string;
        $data['id_company'] = Zend_Auth::getInstance()->getIdentity()->id_company;
        $data['slug'] = str_replace(" ", "-", strtolower($string));
        $sql = $this->insert($data, "custom_departments");
        $last_insert = $this->lastInsertId();
        $sql_object = $this->select()->from(array("c" => "custom_departments"), array("id", "title"))->where("id =?", $last_insert);
        $data = $this->fetchRow($sql_object);
        $data['source'] = "departments";
        return $data;
    }

    public function HasLinkedinProfile($id) {
        $sql = $this->select()->from(array("u" => "users"), array("linkedin_link"))->where("id =?", (int) $id);
        return $this->fetchRow($sql)['linkedin_link'];
    }

    public function getJobsByCompanyId($id) {
        $sql = $this->select()->from(array("j" => "jobs"), array("*"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job")
                ->where("j.id_company =?", (int) $id)
                ->where("j.status = 1");

        return $this->fetchAll($sql);
    }

    public function getCompanyTermsAndCondition() {
        $sql = $this->select()->from(array("t" => "terms"), array("status"))->where("t.id_company =?", (int) $this->_user->id_company);
        $result = $this->fetchRow($sql);
        if (!empty($result) && $result['status'] > 0) {
            /**
             *  Check if user alrady clicked terms and conditions
             */
            $sql = $this->select()->from(array("u" => "users"), array("terms"))->where("u.id =?", $this->_user->id);
            return ['terms_company_status' => $result['status'], 'terms_user_status' => $this->fetchRow($sql)['terms']];
        } else {
            return false;
        }
    }

    public function getTermsContent($id_company) {
        $sql = $this->select()->from(array("t" => "terms"), array("content"))->where("t.id_company =?", (int) $id_company);
        return $this->fetchRow($sql)['content'];
    }

    public function userAgreeTerms() {
        $this->update(array("terms" => 1), "id = '{$this->_user->id}'", "users");
    }

    public function getUserBytrack($track) {
        $sql = $this->select()->from(array("u" => "users"))->where("MD5(id) = ?", (string) $track)->limit(1);
        $data = $this->fetchRow($sql);
        if (empty($data)) {
            $sql = $this->select()->from(array("j" => "job_recommendations"))->where("track = ?", (string) $track)->limit(1);
            $data = $this->fetchRow($sql);
            return $data['id_recommender'];
        } else {
            return $data['id'];
        }
    }

    public function setJobUniqueView($id_user, $source, $fingerprint, $id_job,$id_job_views) {
//        $date = date('Y-m-d H:i:s', time() - (30 * 86400));  // 30 days ago
//
//        $sql = $this->select()->from(array("jv" => "job_views"), array("*"))
//                ->where("jv.fingerprint =?", (string) $fingerprint)
//                ->where("jv.source =?", (string) $source)
//                ->where("jv.id_job=?", (int) $id_job)
//                ->where("date >=?", $date)
//                ->where("jv.id_user =?", (int) $id_user);
//
//        $result = $this->fetchRow($sql);
//        if (empty($result)) {
            $data['id_user'] = $id_user;
            $data['fingerprint'] = $fingerprint;
            $data['date'] = date('Y-m-d H:i:s');
            $data['source'] = $source;
            $data['id_job'] = $id_job;
            $data['id_jobs_share'] = $id_job_views;
            $this->insert($data, "job_views");
            return $this->lastInsertId();
//        }
    }

    public function getGamificationActionsByCampaignId($id_campaign) {
        $sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), array("id", "id_gamification_action"))
            ->where("cga.id_campaign =?", (int) $id_campaign);
        return $this->fetchPairs($sql);
    }

    public function getActionIDbyName($action) {
        $sql = $this->select()->from(array("ga" => "gamifications_actions"), array("id"))
            ->where("ga.action =?", (string) $action);
        return $this->fetchRow($sql);
    }


    public function getThisJobView($id_job, $fingerprint, $id_user) {
        $sql = $this->select()->from(array("jv" => "job_views"), array("*"))
                ->where("jv.fingerprint =?", (string) $fingerprint)
                ->where("jv.id_job=?", (int) $id_job)
                ->where("jv.id_user=?", (int) $id_user);
        return $this->fetchRow($sql);
    }

    public function algorithMatch($data) {
        foreach ($data as $key => &$value) {
            $id_user = $value['id_user'];
            $id_job = $value['id_job'];
            $job_skills = $this->getJobSkills($id_job) + $this->getCustomJobSkills($id_job);
            $job_skills = array_keys($job_skills);
            $career_level = $this->getJobById($id_job)['career_level'];
            $user_skills = $this->_modelUser->getUserSkills($id_user) + $this->_modelUser->getCustomUserSkills($id_user);
            $relevance_per_skill = 50 / count($job_skills);
            $i = 0;
            foreach ($user_skills as $skill) {
                if (in_array($skill['id'], $job_skills)) {
                    $i++;
                }
            }
            $user_skill_revelvance = $i * $relevance_per_skill;


            $user_position = $this->getUserPositions();
            $time_stamp_all_positon = 0;
            foreach ($user_position as $position) {
                $time_stamp_all_positon += strtotime($position['end']) - strtotime($position['start']);
            }
            $all_position_time = ($time_stamp_all_positon / 86400) / 365; // in years
            switch ($career_level) {
                case(3):
                    if ($all_position_time >= 5)
                        $experience = 50;
                    elseif ($all_position_time >= 3)
                        $experience = 30;
                    else
                        $experience = 10;
                    break;
                case(2) :
                    if ($all_position_time >= 3)
                        $experience = 50;
                    else
                        $experience = 30;
                    break;
                case(1):
                    if ($all_position_time >= 1)
                        $experience = 50;
                    break;
            }


            $value['relevance'] = $experience + $user_skill_revelvance;
        }
        return $data;
    }

    public function setGamificationActionPoints($action, $id_job, $id_user) {
        $stats = new models_Stats;
        $stats->setGamificationPoint($id_user, $action, $id_job);
    }

    public function getMyRecommendation() {
        $sql = $this->select()->from(array('u' => "users"), array("*"))->where("u.id_parent =?", (int) $this->_user->id);
        return $this->fetchAll($sql);
    }

    public function getUserReviw($id_user, $id_job) {
        $sql = $this->select()->from(array("n" => "notification"), array("*"))
                ->where("n.id_user =?", $id_user)
                ->where("n.id_job =?", $id_job)
                ->join(array("u" => "users"), "n.id_user = u.id", array("firstname", "lastname"));
        return $this->fetchRow($sql);
    }

    public function userHasReview($id_user, $id_job) {
        $sql = $this->select()->from(array("n" => "notification"), array("*"))
                ->where("n.id_user =?", $id_user)
                ->where("n.id_job =?", $id_job);
        $result = $this->fetchRow($sql);

        if ($result['result_orientation'] > 0 || $result['culture_fit'] > 0 || $result['technical_skills'] > 0 || $result['note'] != "") {
            return true;
        } else {
            return false;
        }
    }
    public function userReviewRejected($id_user, $id_job) {
        $sql = $this->select()->from(array("n" => "notification"), array("*"))
                ->where("n.id_user =?", $id_user)
                ->where("n.id_job =?", $id_job);
        $result = $this->fetchRow($sql);
        if(!empty($result)) {
            return $result;
            }
    }

    public function activeJobIfDateModified($id_job, $data) {
        $sql = $this->select()->from(["j" => "jobs"], ["*"])
                ->where("j.id =?", (int) $id_job);
        $job = $this->fetchRow($sql);
        $status = $job['status'];
        if ($status == 0) {
            $end_expire = $job['end'];
            $new_job_expire = $data['end'];
            if ($new_job_expire > $end_expire && $new_job_expire >= date("Y-m-d")) {
                $this->update(['status' => 1], "id = {$id_job}", "jobs");
            }
        }
    }

    

    public static function emailTmpNameContext($email) {
        $temp_file_name = time() . '-tpl';
        $path = tempnam(sys_get_temp_dir(), $temp_file_name);
        rename($path, $path .= '.tpl');
        $handle = fopen($path, "w");
        fwrite($handle, $email);
        fclose($handle);
        return $path;
    }

    public function getJobapplicationHistoryBuyUserId($id_user) {
       $st = $this->getStatus();
        $sql = $this->select()
                ->from(['ja' => "job_applications"], ['updated as updated_application', 'date as application_date', 'status as app_status', 'status_reason','id as job_application_id','removed as job_removed_status'])
                ->joinLeft(['u' => "users"], 'u.id = ja.modified_by', ['CONCAT(u.firstname,\' \',u.lastname) as modified_user', 'u.id as modified_id'])
                ->join(['j' => "jobs"], 'j.id = ja.id_job', ['*'])
                ->order("updated_application DESC")
                ->join(['jl' => "jobs_lang"], 'j.id = jl.id_job', ['*'])
                ->where("ja.id_user =?", (int) $id_user)
                ->where("ja.removed =?", (int) 0);
        $result = $this->fetchAll($sql);
        $status = $this->getStatus();
        foreach ($result as $key => &$value) {
                $result[$key]['application_status'] = $st[$value['app_status']];
//            $result[$key]['application_status'] = $this->_settings['application_status'][$value['app_status']] . " " . $this->_settings['application_status_reason'][$value['app_status']][$value['status_reason']];
        }
        return $result;
    }
    public function getJobapplicationHistoryBuyUserIdAndJob($id_user,$id_job) {
        $st = $this->getStatus();
        $sql = $this->select()
                ->from(['ja' => "job_applications"], ['updated as updated_application', 'date as application_date', 'status as app_status', 'status_reason','id as job_application_id'])
                ->joinLeft(['u' => "users"], 'u.id = ja.modified_by', ['CONCAT(u.firstname,\' \',u.lastname) as modified_user', 'u.id as modified_id'])
                ->join(['j' => "jobs"], 'j.id = ja.id_job', ['*'])
                ->order("updated_application DESC")
                ->join(['jl' => "jobs_lang"], 'j.id = jl.id_job', ['*'])
                ->where("ja.id_user =". (int) $id_user." AND ja.id_job =".(int) $id_job);
        $result = $this->fetchRow($sql);

        foreach ($result as $key => &$value) {
                    @$result[$key]['application_status'] = $st[$value['app_status']];
//            $result[$key]['application_status'] = $this->_settings['application_status'][$value['app_status']] . " " . $this->_settings['application_status_reason'][$value['app_status']][$value['status_reason']];
        }
        return $result;
    }
    public function getNewJobapplicationHistoryBuyUserId($id_user) {
         $st = $this->getStatus();
        $sql = $this->select()
                ->from(['ja' => "job_application_status_history"], ['date as updated_application', 'date as application_date', 'status as app_status', 'status_reason','job_application_id as job_application_id'])
                ->joinLeft(['u' => "users"], 'u.id = ja.modified_by', ['CONCAT(u.firstname,\' \',u.lastname) as modified_user', 'u.id as modified_id'])
                ->join(['j' => "jobs"], 'j.id = ja.id_job', ['*'])
                ->join(['jl' => "jobs_lang"], 'j.id = jl.id_job', ['*'])
                ->order("updated_application DESC")
                ->where("ja.id_user =?", (int) $id_user);
        $result = $this->fetchAll($sql);

        foreach ($result as $key => &$value) {
//            foreach($st as $i => $val) {
//                if($value['app_status'] ==$val[0] && $value['status_reason'] == $val[1]) {
                    $result[$key]['application_status'] = $st[$value['app_status']];
//                }
//            }
//            $result[$key]['application_status'] = $this->_settings['application_status'][$value['app_status']] . " " . $this->_settings['application_status_reason'][$value['app_status']][$value['status_reason']];
        }
        return $result;
    }

    public function getJobApplicationStatus($application_id) {
        $sql = $this->select()
                ->from(['ja' => "job_applications"], ['*'])
                ->where("ja.id =?", (int) $application_id);

        $application = $this->fetchRow($sql);

        if (!empty($application)) {
            return $this->_settings['application_status'][$application['status']] . " " . $this->_settings['application_status_reason'][$application['status']][$application['status_reason']];
        }
    }
    public function getJobNewApplicationStatus($application_id) {
        $sql = $this->select()
                ->from(['ja' => "job_applications"], ['*'])
                ->where("ja.id =?", (int) $application_id);

        $application = $this->fetchRow($sql);
        $all_status = $this->getStatus();
        return $all_status[$application['status']] ;
//        foreach($all_status as $key => $value) {

//                if($application['status'] == $value[0] && $application['status_reason'] == $value[1]) {
//                    return $value[2];

//                }
//        }

        return $this->_modelUser->getAllStatus();

//        if (!empty($application)) {
//            return $this->_settings['application_status'][$application['status']] . " " . $this->_settings['application_status_reason'][$application['status']][$application['status_reason']];
//        }
    }

    public function getJobApplicationByUserEmail($email) {

        $user = $this->_modelUser->getUserDataByEmail($email);
        $sql = $this->select()->from([
                    'ja' => "job_applications"
                        ], ["*"])
                ->join(['jl' => 'jobs_lang'], "jl.id_job = ja.id_job", ['title'])
                ->where("ja.id_user =?", (int) $user['id']);
        return $this->fetchAll($sql);
    }

    public function atsgetJobsHm($filters = array(), $limit = null, $offset = null) {

        $sql = $this->select()->from(['hj' => 'hr_jobs'], ['id_department'])
                ->where('hj.id_user =?', (int) $this->_user->id);

        $departments = $this->fetchAll($sql);
//                    echo '<pre>';print_r($departments);die;

        if(empty($departments)) {
            return [] ;
        }

        $jb_array = [];
//        if (!empty($departments)) {
            $sql = $this->select()->from(['jd' => 'job_departments'], ['id_job']);
            foreach ($departments as $key => $value) {
                $sql->where('jd.id_department =?', (int) $value['id_department']);
            }

            $jb = $this->fetchAll($sql);
            foreach ($jb as $key => $value) {
                $jb_array[] = $value['id_job'];
            }
            $sql = $this->getAllJobsObject($filters , $limit ,$offset);
            $sql->where("j.id IN (" . implode(",", $jb_array) . ")");
            $data = $this->fetchAll($sql);
            if (!empty($data)) {
                foreach ($data as $row) {
                    $ids[] = $row['id'];
                    $jobs[$row['id']] = $row;
                }
                $jobs = $this->_getJobsCities($jobs, $ids);
            }
            return $jobs;

//        $sql = $this->select()
//                ->from(array('j' => 'jobs'), array('*',
//                    '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations',
//                    '(SELECT COUNT(id) FROM job_applications ja WHERE ja.id_job = j.id AND status = 0 AND status_reason = 0) AS new_applicant'))
//                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
//                ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
////                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
//                ->order('j.start DESC');
//
//        if (!empty($filters)) {
//            foreach ($filters as $key => $filter) {
//                switch ($key) {
//                    case "id_country":
//                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
//                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
//                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
//                        break;
//
//                    case "id_city":
//                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
//                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
//                        break;
//
//                    case "id_company":
//                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
//                        break;
//
//                    case "company":
//                        $sql->where("c.slug = ?", $filter);
//                        break;
//
//                    case "id_department":
//                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
//                        break;
//
//                    case "id_industry":
//                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
//                        break;
//
//                    case "id_skill":
//                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
//                        break;
//
//                    case "search":
//                        $sql->where("jl.title LIKE '%{$filters['search'][0]}%' OR c.title LIKE '%{$filters['search'][0]}%' OR jl.responsabilities LIKE '%{$filters['search'][0]}%' OR jl.tagline LIKE '%{$filters['search'][0]}%' OR jl.description LIKE '%{$filters['search'][0]}%' OR jl.requirements LIKE '%{$filters['search'][0]}%' OR jl.bounty_requirements LIKE '%{$filters['search'][0]}%'");
//                        $sql->group("j.id");
//                        break;
//
//                    case "is_active":
//                        if ($filter == 1) {
//                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
//                        }
//                        break;
//                }
//            }
//        }
//
//        $sql->where("j.id IN (" . implode(",", $jb_array) . ")");
//
//        if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
//            $sql->where("c.id = ?", $this->_user->id_company);
//        }
//
//        if (null !== $offset) {
//            $sql->limit($offset, $limit);
//        } else if (null !== $limit) {
//            $sql->limit($limit);
//        }
//        //die($sql->__toString());
//        $data = $this->fetchAll($sql);
//
//        if (!empty($data)) {
//            foreach ($data as $row) {
//                $ids[] = $row['id'];
//                $jobs[$row['id']] = $row;
//            }
//
//            $jobs = $this->_getJobsCities($jobs, $ids);
//        }
//
//
//        return $jobs;
    }

    public function atsRecrutergetJobs($filters = array(), $limit = null, $offset = null) {
//         die('ddd');
        $sql = $this->select()->from(['hj' => 'hr_jobs'], ['id_department'])
                ->where('hj.id_user =?', (int) $this->_user->id);

        $departments = $this->fetchAll($sql);


        $jb_array = [];

        if (!empty($departments)) {
            $sql = $this->select()->from(['jd' => 'job_departments'], ['id_job']);
            foreach ($departments as $key => $value) {
                $sql->where('jd.id_department =?', (int) $value['id_department']);
            }

            $jb = $this->fetchAll($sql);
            foreach ($jb as $key => $value) {
                $jb_array[] = $value['id_job'];
            }
        }
        $sql = $this->select()->from(['rj' => 'recruiter_job'], ['id_job'])
                ->where('rj.id_user =?', (int) $this->_user->id);
        $rj_jobs = $this->fetchAll($sql);
        foreach ($rj_jobs as $key => $value) {
            $jb_array[] = $value['id_job'];
        }
        $jb_array = array_unique($jb_array);
        if(empty($jb_array)) return [];

        $sql = $this->getAllJobsObject($filters , $limit , $offset );
        $sql->$sql->where("j.id IN (" . implode(",", $jb_array) . ")");
        $data = $this->fetchAll($sql);
        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
            }
            $jobs = $this->_getJobsCities($jobs, $ids);
        }
        return $jobs;



//        $sql = $this->select()
//                ->from(array('j' => 'jobs'), array('*',
//                    '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations',
//                    '(SELECT COUNT(id) FROM job_applications ja WHERE ja.id_job = j.id AND status = 0 AND status_reason = 0) AS new_applicant'))
//                ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
//                ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
////                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
//                ->order('j.created_at DESC')
//                ->order('j.start DESC');
//
//        if (!empty($filters)) {
//            foreach ($filters as $key => $filter) {
//                switch ($key) {
//                    case "id_country":
//                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
//                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
//                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
//                        break;
//
//                    case "id_city":
//                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
//                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
//                        break;
//
//                    case "id_company":
//                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
//                        break;
//
//                    case "company":
//                        $sql->where("c.slug = ?", $filter);
//                        break;
//
//                    case "id_department":
//                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
//                        break;
//
//                    case "id_industry":
//                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
//                        break;
//
//                    case "id_skill":
//                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
//                        break;
//
//                    case "search":
//                        $sql->where("jl.title LIKE '%{$filters['search'][0]}%' OR c.title LIKE '%{$filters['search'][0]}%' OR jl.responsabilities LIKE '%{$filters['search'][0]}%' OR jl.tagline LIKE '%{$filters['search'][0]}%' OR jl.description LIKE '%{$filters['search'][0]}%' OR jl.requirements LIKE '%{$filters['search'][0]}%' OR jl.bounty_requirements LIKE '%{$filters['search'][0]}%'");
//                        $sql->group("j.id");
//                        break;
//
//                    case "is_active":
//                        if ($filter == 1) {
//                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
//                        }
//                        break;
//                }
//            }
//        }
//
//        $sql->where("j.id IN (" . implode(",", $jb_array) . ")");
//
//        if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
//            $sql->where("c.id = ?", $this->_user->id_company);
//        }
//
//        if (null !== $offset) {
//            $sql->limit($offset, $limit);
//        } else if (null !== $limit) {
//            $sql->limit($limit);
//        }
//        //die($sql->__toString());
//        $data = $this->fetchAll($sql);
//
//        if (!empty($data)) {
//            foreach ($data as $row) {
//                $ids[] = $row['id'];
//                $jobs[$row['id']] = $row;
//            }
//
//            $jobs = $this->_getJobsCities($jobs, $ids);
//        }
//
//
//        return $jobs;
    }

    public function getRecruiters($id) {
        $sql = $this->select()->from(["rj" => 'recruiter_job'],['rj.id', 'rj.id_user'])
                ->where('rj.id_job =?',(int) $id);
        return $this->fetchPairs($sql);
    }

    public function _getRecruiters($id) {
        $sql = $this->select()->from(["rj" => 'recruiter_job'],['rj.id_user'])
            ->join(['u' => 'users'],'u.id=rj.id_user',['CONCAT(u.firstname, \' \',u.lastname) AS name'])
            ->where('rj.id_job =?',(int) $id);
        return $this->fetchPairs($sql);
    }

    public function __getRecruiters($id) {
        $sql = $this->select()->from(["rj" => 'recruiter_job'],[])
            ->join(['u' => 'users'],'u.id=rj.id_user',['CONCAT(u.firstname, \' \',u.lastname) AS title','id'])
            ->where('rj.id_job =?',(int) $id);
        return $this->fetchAll($sql);
    }

    public function assignRecruiterJobs($recruiters , $id_job) {
        $this->delete("id_job = $id_job","recruiter_job");
        foreach($recruiters as $id ) {
            $this->insert(["id_user" => (int) $id,'id_job' => (int) $id_job,'date' => date("Y-m-d H:i:s")],'recruiter_job');
        }

    }

    public function getRecruitersJobSelect($id_user) {
        $sql = $this->select()->from(['rj' => 'recruiter_job'],['rj.id','rj.id_job'])
                ->where("rj.id_user =?",(int) $id_user);
        return $this->fetchPairs($sql);
    }

    public function assignHrManagerJobs($hiring_managers,$id_job)
    {
        $this->delete("id_job = $id_job","hr_jobs");
        if(!empty($hiring_managers))
        {
            foreach($hiring_managers as $id ) {
                $this->insert(["id_user" => (int) $id,'id_job' => (int) $id_job,'date' => date("Y-m-d H:i:s")],'hr_jobs');
            }
        }
    }

    public function getHrManagerJobSelect($id_user) {
        $sql = $this->select()->from(['rj' => 'hr_jobs'],['rj.id','rj.id_job'])
            ->where("rj.id_user =?",(int) $id_user);
        return $this->fetchPairs($sql);
    }

    public function sendMassRecommendationLinkedin($id_user,$job)
    {
         $user = $this->_modelUser->getUserConnectionsLinkedinById($id_user);
        $data['track'] = $this->_modelUser->generateTrackingNumber();
        $data['date'] = date("Y-m-d H:i:s");



        $recommendation['id_recommender'] = $this->_user->id;
        $recommendation['id_user'] = $user['id'];
        $recommendation['track'] = $data['track'];
        $recommendation['date'] = $data['date'];
        $recommendation['id_job'] = $job['id'];
        $recommendation['email'] = $user['email'];


        $this->insert($recommendation, "job_recommendations");

        $transport = null;
        if (!empty($this->_settings['email_smtp']))
            {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }
        $contentModel = new models_Content;
        $emailRecommendation = $contentModel->getEmailByType(28);
        $smarty = new Smarty;

        $smarty->assign("user", (array) $this->_user);
        $smarty->assign("job", (array) $job);
        $smarty->assign("data", (array) $data);
        $smarty->assign("settings", (array) $this->_settings);
        $smarty->assign("name", (array) $user);
        $smarty->assign("url", WebApp_Tools::createJobSlug($job));
        $emailBody = $smarty->fetch('string:' . $emailRecommendation['content']);
        $emailSubject = $smarty->fetch('string:' . $emailRecommendation['subject']);
        $mail = new WebApp_Mail();
        $mail->addTo($user['email'], $user['firstname']);
        $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject($emailSubject);
        $mail->setBodyHtml($emailBody);
        $mail->send($transport);

        if ($campaign = $this->jobIsPartOfCampaign($job['id']))
            {
            $this->insert(array("id_campaign" => $campaign['id'], "id_user" => (int) $this->_user->id, "action" => "recommendation", "value" => 1), "campaign_reports_detailed");
            }
    }

    public function getJobCustomCitiesByJobId($id_job)
    {
        $custom = [];
        $sql = $this->select()
                ->from(['cjc'=>'custom_job_cities'],[''])
                ->join(['cc' => 'custom_cities'], 'cjc.id_city=cc.id',['title'])
                ->where('cjc.id_job =?',(int) $id_job);
        $cities =  $this->fetchAll($sql);
        foreach($cities as $index => $value) {
                $custom[] = $value['title'];
        }
        return $custom;
    }
    public function getStatus()
    {
        $sql = $this->select()
                ->from(['s' => 'status'],['id','slug'])
        ->where("s.active = 1")
        ->order("s.slug ASC");
        return $this->fetchPairs($sql);

    }
    public function checkRecommnederContract($data)
    {
        $id_company = (int) Zend_Auth::getInstance()->getIdentity()->id_company;
        $company = $this->_modelCompany->getCompanyById($id_company);
        $track_timeline = $company['track_timeline'];
        $first_login = Zend_Auth::getInstance()->getIdentity()->first_login;
        if($track_timeline > 0)
        {
            $today = date('Y-m-d H:i:s');
            $d1 = new DateTime($today);
            $d2 = new DateTime($first_login);
            $months = $d1->diff($d2)->m ;
            if($months >= $track_timeline)
            {
                $url =  new Zend_Session_Namespace('url');
                $link = $url->url;
                $chunks = explode("&",explode("=",$link)[1]);
                $id_hash_recommender =$chunks[0];
                $sql = $this->select()->from(array("u" => "users"))
                    ->where("MD5(id) = ?", (string) $id_hash_recommender)
                    ->limit(1);
                $recommender = $this->fetchRow($sql);
                if(Zend_Auth::getInstance()->getIdentity()->id_parent > 0)
                {
                    $this->update(['first_login' => $today,
                                    'id_parent' => $recommender['id']],
                        "id=".(int) Zend_Auth::getInstance()->getIdentity()->id,'users');
                    $data['id_recommender'] = $recommender['id'];
                    return $data['id_recommender'];

                }
            }
        }
        return false;

    }

    public function socialMediaSourceExist($source)
    {
        $sql = $this->select()
            ->from(['sm' => 'social_media'])
            ->where('sm.id =?',(int) $source);
        $result = $this->fetchRow($sql);
        if(!empty($result)) return true ;
        return false;
    }

    public function getJobsShareByHash($hash)
    {
        $sql = $this->select()
            ->from(['sm' => 'jobs_share'])
            ->where('sm.unique =?',(int) $hash);
        $result = $this->fetchRow($sql);
        if(!empty($result)) return $result ;
        return false;
    }

    public function getJobsShareByHashSource($hash,$source)
    {
        $sql = $this->select()
            ->from(['sm' => 'jobs_share'])
            ->where('sm.unique =?',(int) $hash)
            ->where('sm.id_source =?',(int) $source);
        $result = $this->fetchRow($sql);
        if(!empty($result)) return $result ;
        return false;
    }

    public function getSocilaMediaIdByType($type)
    {
        $type = (string) $type ;
        $sql = $this->select()
            ->from(['sm' => 'social_media'],['*'])
            ->where('sm.title =?', $type);
        $result = $this->fetchRow($sql);
        if(!empty($result)) return $result['id'] ;

        $sql = $this->select()
            ->from(['sm' => 'social_media'],['*'])
            ->where('sm.title = website');
        $result = $this->fetchRow($sql);
        if(!empty($result)) return $result['id'] ;
        return false;
    }

    public function getSocial()
    {
        $sql = $this->select()
            ->from(['sm' => 'social_media'],['*'])
            ->where('sm.title != "facebook"')
            ->where('sm.title != "linkedin"')
            ->where('sm.title != "internal"')
            ->where('sm.title != "website"')
            ->where('sm.title != "twitter"');

        if(
            Zend_Auth::getInstance()->getIdentity()->type == 5 ||
            Zend_Auth::getInstance()->getIdentity()->type == 1 ||
            Zend_Auth::getInstance()->getIdentity()->type == 9

        )
        {
            $sql->where('sm.title != "bestjobs"');
            $sql->where('sm.title != "ejobs"');
            $sql->where('sm.title != "hipo"');
        }

        return $this->fetchAll($sql);
    }

    public function withdrawApplicationById($id_application)
    {
        $this->update(['removed' => 1] ,"id=$id_application",'job_applications');
    }

    public function checkOwnDeleteApplicatinByAppId($id_application)
    {
        $me = Zend_Auth::getInstance()->getIdentity()->id;
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['ja.id_user'])
            ->where('ja.id =? ',($id_application));
        $result = $this->fetchRow($sql);

        if (empty($result)) return false;
        if($result['id_user'] != $me) return false ;
        return true;

    }

    public function getAllStatus($id_user = null)
    {
        $id_user = ($id_user === null) ? null : $id_user;
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],['*']);
        if(!is_null($id_user)) $sql->where('ja.id_user =?', (int) $id_user );
        $result = $this->fetchAll($sql);
        return $result;

    }

    public function getJsonResult()
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['*','id as id_application','id as index'])
            ->join(['u' => 'users'],"u.id =ja.id_user",['u.firstname','u.lastname','u.image','u.id_city','u.type'])
            ->joinLeft(['sm' => 'social_media'],"sm.id = ja.source",['sm.title as source_text'])
            ->joinLeft(['jl' => 'jobs_lang'],"jl.id_job = ja.id_job",['jl.title as jobs'])
            ->joinLeft(['so' => 'status_options'],"ja.status_reason = so.id",['so.color as color'])
            ->join(['j' => 'jobs'],"ja.id_job= j.id",['j.status as job_status'])
            ->joinLeft(['rj' => 'recruiter_job'],'ja.id_job=rj.id_job',['id_user as id_recruiter'])
            ->joinLeft(['uu' => 'users'],'uu.id=rj.id_user',['uu.firstname as recruiter_firstname','uu.lastname as recruiter_last_name','uu.id as recruiter_id'])
//            ->where('j.status =?',1)
            ->group("id_application")->order('updated ASC');
        if(Zend_Auth::getInstance()->getIdentity()->type == 4)
        {
            $id_jobs = $this->getAtsRecruiterJobsIds();
            if(empty($id_jobs)) return array();
            $sql->where("ja.id_job IN (" . implode(",", $id_jobs) . ")");
            return $this->fetchAll($sql);
        }
        elseif(Zend_Auth::getInstance()->getIdentity()->type == 5)
        {
            $id_jobs = $this->getAtsHrJobsIds();
            if(empty($id_jobs)) return array();
            $sql->where("ja.id_job IN (" . implode(",", $id_jobs) . ")");
            return $this->fetchAll($sql);
        } else {
            return $this->fetchAll($sql);
        }

    }

    public function getDisactivatedJsonResult()
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['*','id as id_application','id as index'])
            ->join(['u' => 'users'],"u.id =ja.id_user",['u.firstname','u.lastname','u.image','u.id_city','u.type'])
            ->joinLeft(['jl' => 'jobs_lang'],"jl.id_job = ja.id_job",['jl.title as jobs'])
            ->joinLeft(['so' => 'status_options'],"ja.status_reason = so.id",['so.color as color'])
            ->join(['j' => 'jobs'],"ja.id_job= j.id",['j.status as job_status'])
            ->where('j.status =?',0)
            ->where('ja.status_reason IN (2,4,3,11,16,21,28)')
            ->group("id_application")->order('updated ASC');
        if(Zend_Auth::getInstance()->getIdentity()->type == 4)
        {
            $id_jobs = $this->getAtsRecruiterJobsIds();
            if(empty($id_jobs)) return array();
            $sql->where("ja.id_job IN (" . implode(",", $id_jobs) . ")");
            return $this->fetchAll($sql);
        }
        elseif(Zend_Auth::getInstance()->getIdentity()->type == 5)
        {
            $id_jobs = $this->getAtsHrJobsIds();
            if(empty($id_jobs)) return array();
            $sql->where("ja.id_job IN (" . implode(",", $id_jobs) . ")");
            return $this->fetchAll($sql);
        } else {
            return $this->fetchAll($sql);
        }

    }

    public function getPStatus()
    {
        $sql = $this->select()->from(['s' => 'status'],['*','id as id_status']);
        return $this->fetchAll($sql);
    }

    public function getSubStatus()
    {
        $sql = $this->select()
            ->from(['s' => 'status'],['*'])
        ->where("s.active =1")
        ->where("s.column_order IS NOT NULL")
        ->order("s.column_order ASC");
        $result = $this->fetchAll($sql);
        $data = [];
        foreach($result as $key => &$value){
            $id =  (int) $value['id'];
            $sql = $this->select()->from(['sp' => 'status_options'],['*'])
                ->where('sp.id_status =?',$id);
            $all_sub = $this->fetchAll($sql);
            $result[$key]['status_options'] = $all_sub;
        }
        return $result;
    }

    public function updateUserStatus($id_applications,$id_status,$id_status_reason)
    {

        $this->update(['status' => $id_status,
            'status_reason' => $id_status_reason,
            'updated' => date('Y-m-d H:i:s')],
            "id={$id_applications}",
            'job_applications');
    }

    public function updateUserHiredDate($id_applications,$hired_date)
    {

        $this->update(['hiring_date' => $hired_date,
            'updated' => date('Y-m-d H:i:s')],
            "id={$id_applications}",
            'job_applications');
    }

    public function getSocialSource($sources)
    {
        $sources = array_unique($sources);
        $sql = $this->select()
            ->from(['s' => 'social_media'],['id','title']);
        $sql->where("s.id IN (" . implode(",", $sources) . ")");
        return $this->fetchPairs($sql);
    }

    public function getcities($cities)
    {
        if(empty($cities)) return [] ;
        $cities = array_filter(array_unique($cities));
        $sql = $this->select()
            ->from(['c' => 'cities'],['id','title']);
        $sql->where("c.id IN (" . implode(",", $cities) . ")");
        return $this->fetchPairs($sql);
    }

    public function getJobsSelectFromArray($jobs)
    {
        $jobs = array_unique($jobs);
        $sql = $this->select()->from(['j' => 'jobs'],['j.id'])
            ->join(['jl' => 'jobs_lang'],"jl.id_job=j.id",['jl.title']);
        $sql->where("j.id IN (" . implode(",", $jobs) . ")");
        return $this->fetchPairs($sql);
    }

    public function getAtsRecruiterJobsIds()
    {
        $sql = $this->select()->from(['hj' => 'hr_jobs'], ['id_department'])
            ->where('hj.id_user =?', (int) $this->_user->id);

        $departments = $this->fetchAll($sql);


        $jb_array = [];

        if (!empty($departments)) {
            $sql = $this->select()->from(['jd' => 'job_departments'], ['id_job']);
            foreach ($departments as $key => $value) {
                $sql->where('jd.id_department =?', (int) $value['id_department']);
            }

            $jb = $this->fetchAll($sql);
            foreach ($jb as $key => $value) {
                $jb_array[] = $value['id_job'];
            }
        }

        $sql = $this->select()->from(['rj' => 'recruiter_job'], ['id_job'])
            ->where('rj.id_user =?', (int) $this->_user->id);
        $rj_jobs = $this->fetchAll($sql);
        foreach ($rj_jobs as $key => $value) {
            $jb_array[] = $value['id_job'];
        }
        $jb_array = array_unique($jb_array);
        return $jb_array;
    }


    public function getAtsHrJobsIds()
    {
        $sql = $this->select()->from(['hj' => 'hr_jobs'], ['id_department'])
            ->where('hj.id_user =?', (int) $this->_user->id);

        $departments = $this->fetchAll($sql);
//                    echo '<pre>';print_r($departments);die;

        if(empty($departments)) {
            return [] ;
        }

        $jb_array = [];
//        if (!empty($departments)) {
        $sql = $this->select()->from(['jd' => 'job_departments'], ['id_job']);
        foreach ($departments as $key => $value) {
            $sql->where('jd.id_department =?', (int) $value['id_department']);
        }

        $jb = $this->fetchAll($sql);
        foreach ($jb as $key => $value) {
            $jb_array[] = $value['id_job'];
        }
        return $jb_array;
    }

    public function getJobapplicationHistoryBuyApplicationId($id_application)
    {
        $id_application = (int) $id_application;
        $sql = $this->select()->from(['jah' => 'job_application_status_history'],['*'])
            ->joinleft(['sp' => 'status_options'],'jah.status_reason=sp.id',['sp.icon','sp.color'])
            ->joinleft(['u' => 'users'],"u.id=jah.modified_by",["CONCAT(u.firstname, ' ',u.lastname) AS modified_by_user"])
        ->where('jah.job_application_id =?',$id_application)
        ->order('jah.date ASC');
        return $this->fetchAll($sql);
    }

    public function mapApplicationStatusHistoryByStatus($applications_history)
    {
//        $aplications = [];
//        foreach ($aplications as $key => $value)
//        {
//            $aplications[$value['status']] = $value;
//        }
        $sql = $this->select()->from(['s' => 'status'],['*'])->order('s.column_order ASC')
            ->where('s.active =1');
        $result = $this->fetchAll($sql);
        $status = [];
        foreach($result as $key => $value)
        {
            $status[$value['id']] = $value;
        }
        foreach ($applications_history as $key => $app)
        {
            $index = $app['status'];
            if(isset($status[$index]) && !empty($status[$index]))
            {
                $status[$index]['history'][] = $app;
            }
        }
        $status = array_values($status);
        $days = 0;
        foreach($status as $index => $st)
        {
            $current = $index;
            $previous_status = $current-1;
            $next_status = $current+1;


            $history = $st['history'];
            if(!empty($history))
            {

                foreach($history as $key => $his)
                {

                    $date_1 = $his['date'];
                    $next = $key+1;
                    if(!empty($history[$next]))
                    {
                        $date_2 = $history[$next]['date'];
                        $days = WebApp_Tools::getDaysBetweenDate($date_1,$date_2);
                        $status[$index]['days'] = $days;

                    } else {
                        $rest_status = array_slice($status,$next_status,-1,true);
                        foreach( $rest_status as $k => $v) {
                            if(isset($v['history']))
                            {
                                $last_history_from_previous_change = end($history)['date'];
                                $date_3 = end($v['history'])['date'];
                                $status[$k]['days'] = WebApp_Tools::getDaysBetweenDate($date_3,$last_history_from_previous_change);;
                                break;

                            } else {
                                $today = date('Y-m-d H:i:s');
                                $days = WebApp_Tools::getDaysBetweenDate($today,$date_1);
                                $status[$index]['days'] = $days;
                            }
                        }


                    }
                }
            }

            if(count($history) <= 1)
            {
                $date_1 = $history[0]['date'];

                // if the next status has not
                // changed yet and still the current one : next one didnt has a history
                // so we get difference from today
//                if($status[$next]['history'])

            }
        }
        return $status;
    }

    public function getJobSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jsd" => "job_sub_departments"),
            array("id_sub_department", "id_job"))->where("id_job = ?", $id_job);
        return $this->fetchPairs($sql);

    }

    public function _getJobSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jsd" => "job_sub_departments"), array("id_sub_department"))
            ->join(['sd' => 'sub_departments'],"sd.id=jsd.id_sub_department",'title')
            ->where("id_job = ?", $id_job);
        return $this->fetchPairs($sql);

    }

    public function __getJobSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jsd" => "job_sub_departments"), array())
            ->join(['sd' => 'sub_departments'],"sd.id=jsd.id_sub_department",['title','id'])
            ->where("id_job = ?", $id_job);
        return $this->fetchAll($sql);

    }

    public function getJobSubSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jssd" => "job_sub_sub_departments"),
            array("id_sub_sub_department", "id_job"))->where("id_job = ?", $id_job);
        return $this->fetchPairs($sql);
    }

    public function _getJobSubSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jssd" => "job_sub_sub_departments"),
            array("id_sub_sub_department"))
            ->join(['ssd' => 'sub_sub_departments'],"ssd.id=jssd.id_sub_sub_department",['title'])
            ->where("id_job = ?", $id_job);
        return $this->fetchPairs($sql);
    }

    public function __getJobSubSubDepartments($id_job)
    {
        $sql = $this->select()->from(array("jssd" => "job_sub_sub_departments"),
            array())
            ->join(['ssd' => 'sub_sub_departments'],"ssd.id=jssd.id_sub_sub_department",['title','id'])
            ->where("id_job = ?", $id_job);
        return $this->fetchAll($sql);
    }


    public function getJobprojectsByJobId($id_job)
    {
        $sql = $this->select()->from(array("jp" => "job_projects"),
            array("id", "id_project"))->where("jp.id_job = ?", $id_job);
        return $this->fetchPairs($sql);
    }

    public function _getJobprojectsByJobId($id_job)
    {
        $sql = $this->select()->from(array("jp" => "job_projects"),
            array("id_project as id"))
            ->join(['p' => 'projects'],"p.id=jp.id_project",['title'])
            ->where("jp.id_job = ?", $id_job);
        return $this->fetchAll($sql);
    }

    public function getJobHeadCountJobId($id_job)
    {
        $sql = $this->select()
            ->from(['jd' => 'job_headcount'],['headcount'])
            ->where('jd.id_job =?',(int) $id_job);
        return $this->fetchRow($sql);
    }

    public function getColumnOrderByApplicationId($id_application)
    {
        $id_application = (int) $id_application;
        $sql = $this->select()
            ->from(['ja' => 'job_applications'],[])
            ->join(['s' => 'status'],'ja.status=s.id',['s.column_order'])->where('ja.id =?',$id_application);
        return $this->fetchRow($sql);
    }

    public function getCompanyRecruiter()
    {

        $sql = $this->select()
            ->from(['u' => 'users'],array("CONCAT(u.firstname, ' ',u.lastname) AS recruiter",'id'))
            ->where('u.type IN (4,2)');
        if (Zend_Auth::getInstance()->getIdentity()->type == 4)
        {
            $sql->where('u.id !=?',(int) Zend_Auth::getInstance()->getIdentity()->id);
        }
        return $this->fetchAll($sql);

    }

    public function getChannels()
    {
        $sql = $this->select()->from(['sc' => 'social_media'],['sc.id','sc.title']);
        return $this->fetchAll($sql);
    }

    public function getActiveJobs()
    {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('*',
                '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations',
                '(SELECT COUNT(id) FROM job_applications ja WHERE ja.id_job = j.id AND status = 0 AND status_reason = 0) AS new_applicant'))
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
            ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
//                ->order('j.start DESC')
            ->where('j.status=1')
            ->order('jl.title');

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case "id_country":
                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
                        break;

                    case "id_city":
                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
                        break;

                    case "id_company":
                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
                        break;

                    case "company":
                        $sql->where("c.slug = ?", $filter);
                        break;

                    case "id_department":
                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
                        break;

                    case "id_industry":
                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
                        break;

                    case "id_skill":
                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
                        break;

                    case "search":
                        $sql->where("jl.title LIKE '%{$filters['search'][0]}%' OR c.title LIKE '%{$filters['search'][0]}%' OR jl.responsabilities LIKE '%{$filters['search'][0]}%' OR jl.tagline LIKE '%{$filters['search'][0]}%' OR jl.description LIKE '%{$filters['search'][0]}%' OR jl.requirements LIKE '%{$filters['search'][0]}%' OR jl.bounty_requirements LIKE '%{$filters['search'][0]}%'");
                        $sql->group("j.id");
                        break;

                    case "is_active":
                        if ($filter == 1) {
                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
                        }
                        break;
                }
            }
        }

        if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
            $sql->where("c.id = ?", $this->_user->id_company);
        }

        if (null !== $offset) {
            $sql->limit($offset, $limit);
        } else if (null !== $limit) {
            $sql->limit($limit);
        }
        //die($sql->__toString());
        $data = $this->fetchAll($sql);

        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
            }

            $jobs = $this->_getJobsCities($jobs, $ids);
        }


        return $jobs;
    }

    public function getAdminJobsManagementByCompanyId()
    {

    }

    public function getAllJobsObject()
    {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('*',
                '(SELECT COUNT(id) AS cnt FROM job_recommendations WHERE id_job = j.id) AS recommendations',
                '(SELECT COUNT(id) FROM job_applications ja WHERE ja.id_job = j.id AND status = 0 AND status_reason = 0) AS new_applicant'))
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title', 'tagline', 'description', 'requirements', 'responsabilities', 'bounty_requirements', 'slug'))
            ->join(array('c' => 'companies'), "j.id_company = c.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
//                ->order('j.start DESC')
            ->order('j.created_at DESC');

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                switch ($key) {
                    case "id_country":
                        $sql->join(array("jci" => "job_cities"), "j.id = jci.id_job", array());
                        $sql->join(array("ci" => "cities"), "jci.id_city = ci.id", array());
                        $sql->where("ci.id_country IN (" . implode(",", $filters['id_country']) . ")");
                        break;

                    case "id_city":
                        $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
                        $sql->where("jc.id_city IN (" . implode(",", $filters['id_city']) . ")");
                        break;

                    case "id_company":
                        $sql->where("c.id IN (" . implode(",", $filters['id_company']) . ")");
                        break;

                    case "company":
                        $sql->where("c.slug = ?", $filter);
                        break;

                    case "id_department":
                        $sql->join(array("jd" => "job_departments"), "j.id = jd.id_job", array())->where("jd.id_department IN (" . implode(",", $filters['id_department']) . ")");
                        break;

                    case "id_industry":
                        $sql->join(array("ji" => "job_industries"), "j.id = ji.id_job", array())->where("ji.id_industry IN (" . implode(",", $filters['id_industry']) . ")");
                        break;

                    case "id_skill":
                        $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))->where("js.id_skill IN (" . implode(",", $filters['id_skill']) . ")");
                        break;

                    case "search":
                        $sql->where("jl.title LIKE '%{$filters['search'][0]}%' OR c.title LIKE '%{$filters['search'][0]}%' OR jl.responsabilities LIKE '%{$filters['search'][0]}%' OR jl.tagline LIKE '%{$filters['search'][0]}%' OR jl.description LIKE '%{$filters['search'][0]}%' OR jl.requirements LIKE '%{$filters['search'][0]}%' OR jl.bounty_requirements LIKE '%{$filters['search'][0]}%'");
                        $sql->group("j.id");
                        break;

                    case "is_active":
                        if ($filter == 1) {
                            //$sql->where("j.end >= NOW() OR j.end = '0000-00-00' OR j.start = '0000-00-00'");
                        }
                        break;
                }
            }
        }

//        if (@$this->_user->id_company > 0 && $this->_user->type < 3) {
//            $sql->where("c.id = ?", $this->_user->id_company);
//        }

        if (null !== $offset) {
            $sql->limit($offset, $limit);
        } else if (null !== $limit) {
            $sql->limit($limit);
        }
        return $sql;
        //die($sql->__toString());
        $data = $this->fetchAll($sql);

        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
            }

            $jobs = $this->_getJobsCities($jobs, $ids);
        }

        return $jobs;
    }

    public function getAdminJobByCompany($filters = array(), $limit = null, $offset = null)
    {
        $sql = $this->getAllJobsObject($filters ,$limit , $offset);
        $sql->where("c.id =?",(int) $this->_user->id_company);
        $data = $this->fetchAll($sql);
        if (!empty($data)) {
            foreach ($data as $row) {
                $ids[] = $row['id'];
                $jobs[$row['id']] = $row;
            }
            $jobs = $this->_getJobsCities($jobs, $ids);
        }
        return $jobs;
    }

    public function getRecruitersByJobId($id_job)
    {
        $sql = $this->select()->from(['rj' => 'recruiter_job'],['id_user as id_recruiter'])
            ->where('rj.id_job =?', (int) $id_job);
        return $this->fetchAll($sql);
    }

    public function getActiveJobsReportByDate($start = null,$end=null)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'],['end','bounty','bounty_title','id_currency','id'])
            ->join(['jl' => 'jobs_lang'],"jl.id_job=j.id",['title']);
        $sql->where("j.end BETWEEN '{$start}' AND '{$end}'");
        $sql->order('j.end DESC');
        $jobs = $this->fetchAll($sql);
        $data = [] ;
        foreach ($jobs as $key => &$job) {
            if($job['bounty_title'] == "")
            {
                $job['bounty_title'] = "-";
            }
            $data[$job['id']] = $job;
            $data[$job['id']]['vacancies'] = $this->getJobHeadCountJobId($job['id'])['headcount'];
            $currencies = $this->_modelContent->getCurrenciesSelect();
            $data[$job['id']]['currency'] = $currencies[$job['id_currency']];
            $data[$job['id']]['applicants'] = $this->countApplicantsByJobId($job['id'])['total'];
        }
        return $data ;
    }

    public function countApplicantsByJobId($id_job = 0)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['count(ja.id) as total'])
            ->where('ja.id_job =?',(int) $id_job);
        return $this->fetchRow($sql);
    }

    public function getJobToExpire($start, $end ,$url)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'],['j.end','j.id'])
            ->join(['jl' => 'jobs_lang'],"jl.id_job=j.id",['title']);
            $sql->where("j.end BETWEEN '{$start}' AND '{$end}'");
        $jobs =  $this->fetchAll($sql);
        $data = [] ;
        foreach($jobs as $key => &$job)
        {
            $data[$job['id']] = $job;
            $data[$job['id']]['type'] = 'Job';
            $data[$job['id']]['url'] = $url.'/job?id='.$job['id'];
        }
        return $data;
    }

    public function getCampaignToExpire($start, $end , $url)
    {
        $sql = $this->select()
            ->from(['c' => 'campaigns'],['c.end','c.title','c.id']);
        $sql->where("c.end BETWEEN '{$start}' AND '{$end}'");
        $campaigns =  $this->fetchAll($sql);
        $data = [] ;
        foreach($campaigns  as $key => &$campaign )
        {
            $data[$campaign['id']] = $campaign;
            $data[$campaign['id']]['type'] = 'Campaign';
            $data[$campaign['id']]['url'] = $url.'/add?id='.$campaign['id'];
        }
        return $data;
    }

    public function getActiveJobsReportByDateUserId($start = null,$end=null,$id_user=null)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'],['end','bounty','bounty_title','id_currency','id'])
            ->join(['jl' => 'jobs_lang'],"jl.id_job=j.id",['title']);
        $sql->join(['rj' => 'recruiter_job'],"rj.id_job=j.id",[]);
        $sql->where("rj.id_user =?",(int) $id_user);
        $sql->where("j.end BETWEEN '{$start}' AND '{$end}'");
        $sql->order('j.end DESC');
        $jobs = $this->fetchAll($sql);
        $data = [] ;
        foreach ($jobs as $key => &$job) {
            if($job['bounty_title'] == "")
            {
                $job['bounty_title'] = "-";
            }
            $data[$job['id']] = $job;
            $data[$job['id']]['vacancies'] = $this->getJobHeadCountJobId($job['id'])['headcount'];
            $currencies = $this->_modelContent->getCurrenciesSelect();
            $data[$job['id']]['currency'] = $currencies[$job['id_currency']];
            $data[$job['id']]['applicants'] = $this->countApplicantsByJobId($job['id'])['total'];
        }
        return $data ;
    }

    public function getJobsWithSimilarCandidateBySkills($neededSkills = array(),$start,$end)
    {
        $sql = $this->select()
            ->from(['j' => 'jobs'],['j.end','j.id'])
            ->join(['jl' => 'jobs_lang'],"j.id=jl.id_job",['jl.title','jl.role'])
            ->join(['jk' => 'job_skills'],"jk.id_job=j.id",[])
            ->where("jk.id_skill IN (" . implode(",", $neededSkills) . ")")
            ->where("j.status = 1")
            ->where("j.start BETWEEN '{$start}' AND '{$end}'")
            ->group("j.id")
            ->order("j.end DESC");
        return $this->fetchAll($sql);
    }

    public function __getJobCareerLevel($id_career)
    {
        return ['id' => $id_career , 'title' => $this->_settings['career_levels'][$id_career]];
    }

    public function __getJobTypeLevel($type)
    {
        $sql = $this->select()
            ->from(['jt' => 'job_types'],['id','title'])
            ->where('jt.id =?',(int) $type);
        return $this->fetchRow($sql);
    }
    public function __getJobPeriode($periode)
    {
        $sql = $this->select()
            ->from(['jp' => 'job_periodes'],['id','title'])
            ->where('jp.id =?',(int) $periode);
        return $this->fetchRow($sql);
    }

    public function __getJobCurrency($currency)
    {
        $sql = $this->select()
            ->from(['c' => 'currencies'],['id' ,'title'])
            ->where('c.id =?',(int) $currency);
        return $this->fetchRow($sql);

    }

    public function __getJobPyamentType($bounty_payment_type)
    {
        $sql = $this->select()
            ->from(['pt' => 'payment_type'],['id','title'])
            ->where('pt.id =?',(int) $bounty_payment_type);
        return $this->fetchRow($sql);
    }

    public function __getJobBountyDueDate($job_bounty_dd)
    {
        return ['id' => $job_bounty_dd,'title' => $this->_settings['jobs_lifetime'][$job_bounty_dd]];
    }

    public function getJobApplicationsIdByUserID($id_user)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'], ['ja.id'])
            ->join(['j' => 'jobs'] , 'j.id=ja.id_job',['j.end','j.id as id_job'])
            ->join(['jl' => 'jobs_lang'],'jl.id_job=ja.id_job',['jl.title'])
            ->where("ja.id_user =?", (int)$id_user);
        $result =  $this->fetchAll($sql);
        foreach ($result as $key => &$job)
        {
            $job['department'] = $this->__getJobDepartments($job['id_job']);
        }
        return $result;
    }

    public function getSourceByName($source)
    {
        $sql = $this->select()->from(['sm' => 'social_media'],['id'])
            ->where("sm.title like '%$source%'");
        return $this->fetchRow($sql);
    }

    public function getHeaderApplicationById($id_application)
    {
        $id_application = (int) $id_application ;
        $sql = "select *, if(id_status = 5,sum(datediff(now(),date)), sum(datediff(next_status_date,date))) days from
                     (select h.id, h.id_user, h.id_job,h.modified_by,h.job_application_id,h.date,h.status as id_status,s.status,h.status_reason,u.firstname, DATE_FORMAT(ja.hiring_date, '%d-%m-%y') as hiring_date,
                         (select min(y.date) from job_application_status_history y where h.date<y.date and h.status_reason=0 and h.id_user=y.id_user
                          ) next_status_date
                        
                        from job_application_status_history h
                          INNER JOIN users u on u.id=h.id_user
                           INNER JOIN status s on s.id=h.status
                           INNER JOIN job_applications ja on ja.id=h.job_application_id
                        where h.job_application_id='{$id_application}' and h.status_reason=0
                        order by h.date
                       ) a
                    group by status
                    ORDER BY date";

        $result = $this->query($sql);
        $result = $result->fetchAll();
        $data = [] ;
        foreach ($result as $index => &$item) {
            $data[$item['id_status']] = $item ;
        }
        $sql = $this->select()->from(['s' => 'status'],['*'])->order('s.column_order ASC')
            ->where('s.active =1');
        $all_result = $this->fetchAll($sql);
        foreach ($all_result as $index => &$item) {
            $id_status = $item['id'];
            $item['flag'] = 0;
            if(isset($data[$id_status]) && !empty($data[$id_status]))
            {
                $item['flag'] = 1;
                $item['days'] = $data[$id_status]['days'];
                $item['hiring_date'] = $data[$id_status]['hiring_date'];
            }

            $sql = $this->select()->from(['jash' => 'job_application_status_history'],['*',"DATE_FORMAT(jash.date, '%d-%m-%Y %H:%i') AS date"])
                ->joinleft(['sp' => 'status_options'],'jash.status_reason=sp.id',['sp.icon','sp.color'])
                ->joinleft(['u' => 'users'],"u.id=jash.modified_by",["CONCAT(u.firstname, ' ',u.lastname) AS modified_by_user"])
                ->joinleft(['ja' => 'job_applications'],"ja.id=jash.modified_by",["CONCAT(u.firstname, ' ',u.lastname) AS modified_by_user"])
                ->order('jash.date ASC')
                ->where('jash.job_application_id =?', (int) $id_application)
                ->where("jash.status =?",(int) $id_status);
            $item['history'] = $this->fetchAll($sql);
        }
//        echo '<pre>',print_r($all_result,1),'</pre>';die;
        return $all_result;
    }

    public function getAllJobApplicationStatus($id_user)
    {
        $sql = $this->select()->from(['ja' => 'job_applications'],['*'])
            ->where("ja.id_user =?",(int) $id_user);
        $all_applications = $this->fetchAll($sql);
        $data = [];
        foreach($all_applications as $index => $app)
        {
            $data[$app['id']] = $this->getHeaderApplicationById($app['id']);
        }
        return $data ;
    }

    public function getNextJobId($id_job)
    {
        $id_job = $id_job['id'];
        $sql = $this->select()->from(['j' => 'jobs'],['id'])
            ->join(['jl' => 'jobs_lang'],'jl.id_job=j.id',[])
            ->where('j.id >?',(int) $id_job)
            ->where('j.status =1');
        $sql->order('j.id ASC')
        ->limit(1);
        $job = $this->fetchRow($sql);

        if(empty($job))
        {
            return [];
        }
        $job = $this->getJobById($job['id'],false,true);
        if(empty($job))
        {
            return [];
        }
        $title = $job['title'];
        $url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        return ['title' => $title,'url' => $url];
    }

    public function getPreviousJobId($id_job)
    {
        $id_job = $id_job['id'];
        $sql = $this->select()->from(['j' => 'jobs'],['id'])
            ->join(['jl' => 'jobs_lang'],'jl.id_job=j.id',[])
            ->where('j.id <?',(int) $id_job)
            ->where('j.status =1');
        $sql->order('j.id DESC')
            ->limit(1);
        $job = $this->fetchRow($sql);
        if(empty($job))
        {
            return [];
        }

        $job = $this->getJobById($job['id'],false,true);
        if(empty($job))
        {
            return [];
        }
        $title = $job['title'];
        $url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        return ['title' => $title,'url' => $url];
    }


    public function getActionDataByCampainIdActionId($id_action, $id_campaign)
    {
        $sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), ['cga.*'])
            ->where("cga.id_campaign =?", (int) $id_campaign)
            ->where("cga.id_gamification_action =?",(int) $id_action)
            ->limit(1)
        ;
        return $this->fetchRow($sql);
    }

    public function getViewByShareTracK($track = null)
    {
        if($track)
        {
            $sql = $this->select()->from(['jv' => 'job_views'],['id'])
                ->join(['js' => 'jobs_share'],"js.id=jv.id_jobs_share",[])
                ->where("js.unique =?",(string) $track);
            return $this->fetchRow($sql);
        }

        return [];
    }

    public function getJobMenu()
    {
        $type = (Zend_Auth::getInstance()->hasIdentity()) ? Zend_Auth::getInstance()->getIdentity()->type : 9 ;
        $sql = $this->select()->from(['a' => 'acl'],['*'])
            ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
            ->where("a.id_parent =?", 0)
            ->where("acm.type =?", (int) $type)
//            ->where('a.side_menu_order > 0')
            ->where('a.category ="right-menu"')
            ->where("acm.status =?", (int) 1)->order('acm.right_menu_order ASC');

        $data = [] ;
        $result = $this->fetchAll($sql);
        foreach ($result as $key => &$value)
        {
            $sql = $this->select()->from(['a' => 'acl'],['*'])
                ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
                ->where("a.id_parent =?", $value['id_acl'])
                ->where("acm.type =?", (int) $type)
                ->where('a.side_menu_order > 0')
                ->where("acm.status =?", (int) 1)->order('acm.left_menu_order ASC');
            $res = $this->fetchAll($sql);
            $submenu = (!empty($res)) ? $res : [] ;
            $value['submenu'] = $submenu;
        }
        return $result;
    }


    public function getFavoritesJobs($filters = array(),$offset = 0)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(['f' => 'favorites'],"f.id_job=j.id",[])->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where("f.id_user =?",(int) Zend_Auth::getInstance()->getIdentity()->id);

        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function getLatestJobs($filters = array(),$offset = 0)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->order('j.created_at DESC');
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }


    public function getAllLatestJobs($filters = array())
    {
        $ago = date('c', strtotime('-30 days'));
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->where("j.created_at >=?",$ago)
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->order('j.created_at DESC');
//        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
//        echo $jobs;die;
        return $jobs;
    }

    public function applyHomeFilter($filters,$sql)
    {
        foreach ($filters as $key => $filter) {
            switch ($key) {
                case "id_city":
                    $sql->join(array("jc" => "job_cities"), "j.id = jc.id_job", array());
                    $sql->where("jc.id_city =?", $filters['id_city']);
                    break;
                case "id_skill":
                    $sql->join(array("js" => "job_skills"), "j.id = js.id_job", array(""))
                        ->where("js.id_skill =?",  $filters['id_skill']);
                    break;

                case "search":
                    $sql->where("jl.title LIKE '%{$filters['search']}%'");
                    break;
                case "career_level":
                    $sql->where("j.career_level =?",$filters['career_level']);
                    break;
            }
        }
    }

    public function getPromotedJobs($filters,$offset)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->join(['cj' => 'campaign_jobs'],'cj.id_job=j.id',['id_job as id_job_campaign'])
            ->join(['c' => 'campaigns'],'cj.id_campaign=c.id',[])
            ->where('j.status =1')
            ->group('j.id')
            ->where('c.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }

//        $sql->where("j.bounty_title IS NOT NULL");
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $campain_jobs = $this->formatHomeJobs($result);


        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')

            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }

        $sql->where("j.bounty_title !=''");
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $promoted_jobs = $this->formatHomeJobs($result);

        $jobs = ($campain_jobs+$promoted_jobs);
//        $jobs =  array_splice($jobs,0,4);
        return $jobs ;
    }

    public function getAllPromotedJobs($filters)
    {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->join(['cj' => 'campaign_jobs'],'cj.id_job=j.id',['id_job as id_job_campaign'])
            ->join(['c' => 'campaigns'],'cj.id_campaign=c.id',[])
            ->where('j.status =1')
            ->group('j.id')
            ->where('c.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }

        $result = $this->fetchAll($sql);
        $campain_jobs = $this->formatHomeJobs($result);


        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')

            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }

        $sql->where("j.bounty_title !=''");
        $result = $this->fetchAll($sql);
        $promoted_jobs = $this->formatHomeJobs($result);

        $jobs = ($campain_jobs+$promoted_jobs);
        $jobs =  array_splice($jobs,0,4);
        return $jobs ;
    }

    public function getTemporaryJobs($filters,$offset)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where('j.job_periode = 2 || j.job_periode = 3');
//        $sql->where('j.job_periode = 3');
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function getAllTemporaryJobs($filters)
    {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where('j.job_periode = 2 || j.job_periode = 3');
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function getMostsharedJobs($filters,$offset)
    {

        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->joinLeft(['jss' => 'jobs_share'],'jss.id_job=j.id',['count(jss.id) as total_share']);
        $sql->group("j.id")
            ->where('jss.id is null')
            ->order("j.created_at DESC");
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function getAllNotSharedJobs($filters)
    {

        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->joinLeft(['jss' => 'jobs_share'],'jss.id_job=j.id',['count(jss.id) as total_share']);
        $sql->group("j.id")
            ->where('jss.id is null')
            ->order("j.created_at DESC");
//        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function getFavoritesSimilarJobs($filters,$offset)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->join(['f' => 'favorites'],"f.id_job=j.id",[])
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where("f.id_user =?",(int) Zend_Auth::getInstance()->getIdentity()->id);

        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $favourite_jobs = $this->formatHomeJobs($result);
//        return $favourite_jobs;
        if(empty($favourite_jobs)) return $favourite_jobs;
        $skills = [] ;
        $job_ids = [] ;
        foreach ($favourite_jobs as $index => $job) {
            $job_ids[] = $job['id'];
            $job_skills = $this->_getJobSkills($job['id']);
            foreach ($job_skills as $index => $job) {
                $skills[] = $index;
            }
        }
        $skills = array_unique($skills);

        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(['jc' => 'job_skills'],'jc.id_job=j.id',[])
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        $sql->where("jc.id_skill IN (" . implode(",", $skills) . ")");
        $sql->group('j.id');
//        $sql->where("j.id NOT IN (" . implode(",", $job_ids) . ")");
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $similar_jobs = $this->formatHomeJobs($result);
        $jobs = ($similar_jobs+$favourite_jobs);
        $jobs =  array_splice($jobs,0,5);
        return $jobs ;
    }

    public function getAllFavoritesSimilarJobs($filters)
    {
//        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->join(['f' => 'favorites'],"f.id_job=j.id",[])
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where("f.id_user =?",(int) Zend_Auth::getInstance()->getIdentity()->id);

//        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
//        echo '<pre>',print_r($result,1),'</pre>';die;
        $favourite_jobs = $this->formatHomeJobs($result);
//        return $favourite_jobs;
        if(empty($favourite_jobs)) return $favourite_jobs;
        $skills = [] ;
        $job_ids = [] ;
        foreach ($favourite_jobs as $index => $job) {
            $job_ids[] = $job['id'];
            $job_skills = $this->_getJobSkills($job['id']);
            foreach ($job_skills as $index => $job) {
                $skills[] = $index;
            }
        }
        $skills = array_unique($skills);

        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(['jc' => 'job_skills'],'jc.id_job=j.id',[])
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        $sql->where("jc.id_skill IN (" . implode(",", $skills) . ")");
        $sql->group('j.id');
//        $sql->where("j.id NOT IN (" . implode(",", $job_ids) . ")");
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
//        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $similar_jobs = $this->formatHomeJobs($result);
        $jobs = ($similar_jobs+$favourite_jobs);
        $jobs =  array_splice($jobs,0,5);
        return $jobs ;
    }

    public function getpermanentJobs($filters,$offset)
    {
        $length = 5;
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where('j.job_periode = 1');
        $sql->limit($length,$offset);
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }


    public function getAllPermanentJobs($filters)
    {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id as id_job'))
            ->where('j.status =1')
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'));
        if(!empty($filters))
        {
            $this->applyHomeFilter($filters,$sql);
        }
        $sql->where('j.job_periode = 1');
        $result = $this->fetchAll($sql);
        $jobs = $this->formatHomeJobs($result);
        return $jobs;
    }

    public function userHasAccessToCampaign()
    {
        $type  = 9 ;
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $type = Zend_Auth::getInstance()->getIdentity()->type ;
        }
        $sql = $this->select()->from(['a' => 'acl'],['id'])
        ->join(['acm' => 'acl_matrix'],'acm.id_acl=a.id',[])
        ->where('a.link ="/campaigns"')
            ->where("acm.type =?",(int) $type)->where('acm.status =1');
        $result = $this->fetchRow($sql);
        return $result;
    }

    public function getJobsSelectActiveFilter($filters=[]) {
        $sql = $this->select()
            ->from(array('j' => 'jobs'), array('id'))
            ->join(array('jl' => 'jobs_lang'), "j.id = jl.id_job", array('title'))
//                ->where("jl.id_lang = ?", $this->id_lang)
//                ->order('j.is_promoted DESC')
            ->order('j.created_at DESC')->where("j.status = 1");
        $sql->where("j.id_company =?", (int) $this->_user->id_company);
        $sql->where('j.status =1');

        if(!empty($filters))
        {
            foreach ($filters as $key => $value)
            {
                switch ($key)
                {
                    case('id_city'):
                        $id_city = (array) $value;
                        if(!empty($id_city))
                        {
                            $sql->join(['jc' => 'job_cities'],"jc.id_job=j.id",[]);
                            $sql->where("jc.id_city IN (" . implode(",", $id_city) . ")");
                        }
                    break;

                    case('id_department'):
                        $id_department = (array) $value;
                        if(!empty($id_department))
                        {
                            $sql->join(['jdd' => 'job_departments'],"jdd.id_job=j.id",[]);
                            $sql->where("jdd.id_department IN (" . implode(",", $id_department) . ")");
                        }
                    break;

                    case('id_sub_department'):
                        $id_sub_department = (array) $value;
                        if(!empty($id_sub_department))
                        {
                            $sql->join(['jsd' => 'job_sub_departments'],"jsd.id_job=j.id",[]);
                            $sql->where("jsd.id_sub_department IN (" . implode(",", $id_sub_department) . ")");
                        }
                    break;

                    case('id_sub_sub_department'):

                        $id_sub_sub_department = (array) $value;
                        if(!empty($id_sub_sub_department))
                        {
                            $sql->join(['jssd' => 'job_sub_sub_departments'],"jssd.id_job=j.id",[]);
                            $sql->where("jssd.id_sub_sub_department IN (" . implode(",", $id_sub_sub_department) . ")");
                        }
                    break;

                    case('id_project'):
                        $id_projects = (array) $value;
                        if(!empty($id_projects))
                        {
                            $sql->join(['jpp' => 'job_projects'],"jpp.id_job=j.id",[]);
                            $sql->where("jpp.id_project IN (" . implode(",", $id_projects) . ")");
                        }
                    break;
                }
            }
        }
        $data = array_keys($this->getRemainedJobsInCampaignsActive());
        if(!empty($data))
        {
            $sql->where("j.id NOT IN (" . implode(",", $data) . ")");
        }
        return $this->fetchPairs($sql);
    }

    public function __jobIsFavourites($id_job)
    {
        if(!Zend_Auth::getInstance()->hasIdentity())
        {
            return $status = 0;
        }
        $id_job = (int) $id_job;
        $sql = $this->select()->from(['jf' => 'favorites'])
            ->where('jf.id_job =?',(int) $id_job)
            ->where('jf.id_user =?',(int) $this->_user->id);

        $result = $this->fetchRow($sql);
        $status = 0 ;
        if(!empty($result))
        {
            $status = 1;
        }
        return $status ;
    }

    public function getJobImage($job)
    {
        $type = Zend_Auth::getInstance()->getIdentity()->type ;

        $sql = $this->select()->from(['a' => 'acl'],['*'])
            ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
            ->where("acm.type =?", (int) $type)
            ->where('a.link ="/view-bounties"')
            ->where("acm.status =?", (int) 1);
        $result = $this->fetchRow($sql);

        if(!empty($result) && $result['status'] == 1 && $job['bounty_file'] != "")
        {
            $home_image = $job['bounty_file'];
        } else {
            $home_image = $job['job_public_image'];
        }

        if($home_image == "")
        {
            $dir="/resources/files/companies/original/";
            $name=end(explode('/',$job['company_logo']));
            $home_image = $dir.$name;
        }

        if($home_image == "")
        {
            $home_image = '/resources/files/jobs/default.png';
        }

        return $home_image;

    }

    public function checkJobBounty($job)
    {
        $status = (is_null($job['bounty_title']) || $job['bounty_title'] == "") ? false : true;
        $type = Zend_Auth::getInstance()->getIdentity()->type ;

        $sql = $this->select()->from(['a' => 'acl'],['*'])
            ->join(['acm' => 'acl_matrix'],"a.id=acm.id_acl")
            ->where("acm.type =?", (int) $type)
            ->where('a.link ="/view-bounties"')
            ->where("acm.status =?", (int) 1);
        $result = $this->fetchRow($sql);

        if($result['status'] == 1)
        {
            return $status;
        }
        return false;
    }

    public function checkValidation($action_data,$hash)
    {
        if($action_data['active'] == 0) return false;

        $mCampaign = new models_Campaign ;
        $campaign = $mCampaign->getCampaignById($action_data['id_campaign']);

        $shared_job = $this->getSharedJobByHash($hash);
        if(empty($shared_job)) return false;

        if(WebApp_Tools::check_in_range($campaign['start'],$campaign['end'],$shared_job['date'])) return true;

        return false;

    }

    public function getSharedJobByHash($hash)
    {
        $sql = $this->select()->from(['js' => 'jobs_share'],['*'])->where('js.unique =?',$hash);
        return $this->fetchRow($sql);
    }

    public function getHashByIdView($id_view)
    {
        $sql = $this->select()->from(['jv' => 'job_views'])
//            ->join(['js' => 'jobs_share'],['*'])
            ->join(['js' => 'jobs_share'],"js.id=jv.id_jobs_share",[])
            ->where('jv.id =?',(int) (int) $id_view);
        return $this->fetchRow($sql);
    }

}
