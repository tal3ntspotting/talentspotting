<?php

class models_Payments extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'users';
    public $_user = '';
    protected $_primary = 'id';
    public $cache;
    public $id_lang;
    protected $_modelUser;
    protected $_modelContent;
    protected $_modelCompany;
    protected $_stats;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $this->_modelUser = new models_User;
        $this->_modelContent = new models_Content;
        $this->_modelCompany = new models_Company;

        $this->_stats = new models_Stats;

        $this->_user = $this->_modelUser->_user;
        $this->id_lang = $this->_modelUser->id_lang;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }
    }


    public function getCompanyById($id) {
        return $this->_modelCompany->getCompanyById($id);
    }

    public function getPaymentById($id) {
        $sql = $this->select()->from(array("p" => "payments"))->where("id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getCompanyBounties($id_company = null) {
        $sql = $this->select()
                ->from(array("j" => "jobs"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("*", "id AS id_application", '(SELECT CONCAT(firstname, \' \', lastname) FROM users u WHERE u.id = ja.id_user) AS applicant', '(SELECT CONCAT(firstname, \' \', lastname) FROM users u WHERE u.id = ja.id_recommender) AS refferal', 'updated AS due'))
                ->join(array("u" => "users"), "ja.id_user = u.id", array())
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("ja.status = 4")
                ->where("ja.status_reason = 5")
                ->where("j.id_company = ?", $this->_user->id_company);
        
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $row['status'] = null;
            $finals[$row['id_user']][$row['id_job']] = $row;
        }

        $sql = $this->select()
                ->from(array("cb" => "companies_bounties"))
                ->join(array("j" => "jobs"), "cb.id_job = j.id")
                ->where("j.id_company = ?", $this->_user->id_company)
                ->order("cb.status ASC");
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            if (array_key_exists($row['id_user'], $finals) && array_key_exists($row['id_job'], $finals[$row['id_user']])) {
                $finals[$row['id_user']][$row['id_job']]['status'] = $row['status'];
            }
        }

        foreach ((array) $finals as $user) {
            foreach ($user as $bounty) {
                $bounties[] = $bounty;
            }
        }

        return (array) $bounties;
    }

    public function payCompanyBounty($id) {
        $sql = $this->select()
                ->from(array("j" => "jobs"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("c" => "companies"), "c.id = j.id_company", array("c.title AS company"))
                ->join(array("ja" => "job_applications"), "j.id = ja.id_job", array("*", '(SELECT CONCAT(firstname, \' \', lastname) FROM users u WHERE u.id = ja.id_user) AS applicant', '(SELECT CONCAT(firstname, \' \', lastname) FROM users u WHERE u.id = ja.id_recommender) AS referral'))
                ->join(array("u" => "users"), "ja.id_user = u.id", array())
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("ja.id = ?", $id);
        $data = $this->fetchRow($sql);

        if (!empty($data)) {
            $companies_bounties['id_job'] = $data['id_job'];
            $companies_bounties['id_recommender'] = $data['id_recommender'];
            $companies_bounties['id_user'] = $data['id_user'];
            $companies_bounties['amount'] = $data['bounty'];
            $companies_bounties['status'] = 'initiated';
            $companies_bounties['date'] = date("Y-m-d H:i:s");

            $this->insert($companies_bounties, "companies_bounties");

            $amount = intval($data['bounty'] / 2);
            $bounty['id_job'] = $data['id_job'];
            $bounty['id_company'] = $data['id_company'];
            $bounty['id_user'] = $data['id_user'];
            $bounty['amount'] = $amount;
            $bounty['status'] = 'initiated';
            $bounty['date'] = date("Y-m-d H:i:s");

            $this->insert($bounty, "bounties");

            if ($data['id_recommender'] > 0) {
                $bounty_recommender['id_job'] = $data['id_job'];
                $bounty_recommender['id_company'] = $data['id_company'];
                $bounty_recommender['id_user'] = $data['id_recommender'];
                $bounty_recommender['amount'] = $amount;
                $bounty_recommender['status'] = 'initiated';
                $bounty_recommender['date'] = date("Y-m-d H:i:s");

                $this->insert($bounty_recommender, "bounties");
            }


            $notification['id_company'] = $this->_user->id_company;
            $notification['target'] = 'admin';
            $notification['type'] = 'payment';
            $notification['message'] = 'New bounty payment initated by ' . $data['company'];
            $notification['url'] = 'view-bounties';
            $notification['viewed'] = 0;
            $notification['date'] = date("Y-m-d H:i:s");

            $this->insert($notification, "notifications");

            $sql = $this->select()->from(array("u" => "users"))->where("type = 3");
            $users = $this->fetchAll($sql);

            $transport = null;
            if (!empty($this->_settings['email_smtp'])) {
                $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }

            $mail = new WebApp_Mail();
            $mail->addTo($users[0]['email'], $users[0]['firstname']);
            if (count($users) > 1) {
                for ($i = 1; $i < count($users); $i++) {
                    $mail->addCc($users[$i]['email']);
                }
            }

            $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
            $mail->setSubject("New bounty payment initiated");
            $mail->setBodyFromViewScript("bounty.phtml", array("user" => $this->_user, "bounty" => $data, "settings" => $this->_settings));
            $mail->send($transport);
        }
    }

    public function getBounties() {
        $modelCompany = new models_Company;
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("id AS id_bounty", "*"))
                ->join(array("j" => "jobs"), "b.id_job = j.id", array("id_company", "bounty", "bounty_paid"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("c" => "companies"), "c.id = b.id_company", array("c.title AS company"))
                ->join(array("u" => "users"), "b.id_user = u.id", array("CONCAT(firstname, ' ', lastname) AS name"))
//                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("b.id_company = ?", Zend_Auth::getInstance()->getIdentity()->id_company);
//        if ($modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'] != 1) {
//            $sql->where("u.type !=?", 9);
//        }
        return $this->fetchAll($sql);
    }

    public function getCampaignBounty() {
        $sql = $this->select()->from(array('cb' => 'campaign_bounty'), array("*"))
                ->join(array('c' => "campaigns"), "c.id = cb.id_campaign", array("title"))
                ->joinLeft(array("u" => "users"), "u.id = cb.id_user", array("firstname", "lastname"))
                ->where('cb.id_company =?', $this->_user->id_company);
        $result = $this->fetchAll($sql);
        foreach ($result as $key => $campaign) {
            if (!empty($campaign['id_user'])) {
                $users = unserialize($campaign['id_user']);
                foreach ($users as $user) {
                    $result[$key]['users'][] = $this->_modelUser->getUserById($user);
                }
            }
        }
        return $result;
    }

    public function getBountiesSelect() {
        $sql = $this->select()->from(array("b" => "bounties"), array("id", "amount"));
        return $this->fetchPairs($sql);
    }

    public function getBountyById($id) {
        $sql = $this->select()->from(array("b" => "bounties"))->where("id = ?", (int) $id);
        $result = $this->fetchRow($sql);
        if ($result['amount'] == 0) {
            $sql = $this->select()->from(array("j" => "jobs"), array("bounty"))->where("id = ?", (int) $result['id_job']);
            $result['amount'] = $this->fetchRow($sql)['bounty'];
        }
        return $result;
    }

    public function getPaiementInfo($id) {
        $modelUsers = new models_User;
        $modelJobs = new models_Jobs;
        $bounty = $this->getBountyById($id);
        $job = $modelJobs->getJobById($bounty['id_job']);
        $user = $modelUsers->getUserById($bounty['id_user']);
        return array('user' => $user, "job" => $job);
    }

    public function payBounty($id, $status) {

        $modelUsers = new models_User;
        $modelJobs = new models_Jobs;
        $modelCompanies = new models_Company;
        $bounty = $this->getBountyById($id);
        $job = $modelJobs->getJobById($bounty['id_job']);
        $company = $modelCompanies->getCompanyById($bounty['id_company']);

        switch ($status) {
            case 'pending':
                $this->update(array("status" => $status), "id = {$id}", "bounties");
                break;

            case 'paid':
                $this->update(array("status" => $status), "id = {$id}", "bounties");
                $this->update(array("bounty_paid" => 1), "id = {$job['id']}", "jobs");

                $user = $modelUsers->getUserById($bounty['id_user']);

                $transport = null;
                if (!empty($this->_settings['email_smtp'])) {
                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
                }


//                $contentModel = new models_Content;
//                $bountyEmail = $contentModel->getEmailByType(29);
//                $smarty = new Smarty;
//                $smarty->assign("user", (array) $user);
//                $smarty->assign("bounty", (array) $bounty);
//                $smarty->assign("settings", (array) $this->_settings);
//                $smarty->assign("job", (array) $job);
//                $smarty->assign("company", (array) $company);
//                $emailBody = $smarty->fetch('eval:' . ($bountyEmail['content']));
//                $emailSubject = $smarty->fetch('eval:' . $bountyEmail['subject']);
//
//
//                $mail = new WebApp_Mail();
//                $mail->addTo($user['email'], $user['firstname']);
//                $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
//                $mail->setSubject($emailSubject);
//                $mail->setBodyHtml($emailBody);
//                $mail->send($transport);

                break;

            case 'canceled':
                $this->update(array("status" => $status), "id = {$id}", "bounties");
                $this->update(array("bounty_paid" => NULL), "id = {$job['id']}", "jobs");
                break;
        }
    }

    public function getBountyFiles($id) {
        $sql = $this->select()->from(array("bf" => "bounty_files"))->where("bf.id_bounty = ?", (int) $id);
        return $this->fetchAll($sql);
    }

    public function getBountyDetailed($id) {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("id", "*"))
                ->join(array("j" => "jobs"), "b.id_job = j.id")
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("c" => "companies"), "c.id = b.id_company", array("c.title AS company"))
                ->join(array("u" => "users"), "b.id_user = u.id", array("CONCAT(firstname, ' ', lastname) AS name"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("b.id = ?", (int) $id);
        return $this->fetchRow($sql);
    }

    public function getBountyHistory() {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("id", "*"))
                ->join(array("j" => "jobs"), "b.id_job = j.id")
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("c" => "companies"), "c.id = b.id_company", array("c.title AS company"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("b.id_user = ?", $this->_user->id)
                ->where("b.status IN ('paid', 'canceled')");
        return $this->fetchAll($sql);
    }

    public function getCurrentBounties() {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("id AS id_bounty", "*"))
                ->join(array("j" => "jobs"), "b.id_job = j.id")
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("title AS job", "bounty_requirements"))
                ->join(array("c" => "companies"), "c.id = b.id_company", array("c.title AS company"))
                ->where("jl.id_lang = ?", $this->id_lang)
                ->where("b.id_user = ?", $this->_user->id)
                ->where("b.status IN ('pending')");
        return $this->fetchAll($sql);
    }

    public function getBountiesUploadedFiles() {
        $sql = $this->select()
                ->from(array("b" => "bounties"))
                ->join(array("bf" => "bounty_files"), "b.id = bf.id_bounty", array("file", "title", "date", "id AS id_file"))
                ->where("b.id_user = ?", $this->_user->id);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $files[$row['id']][$row['id_file']] = $row;
        }

        return (array) $files;
    }

    public function uploadFilesOnBounty($id) {
        if (is_array($_FILES['documents']['name']) && $_FILES['documents']['name'][0] != "") {
            for ($i = 0; $i < count($_FILES['documents']['name']); $i++) {
                $title = str_replace(array("_", "-", "."), " ", $_FILES['documents']['name'][$i]);
                $file = $id . "_" . $i . "_" . $_FILES['documents']['name'][$i];

                $this->insert(array("id_bounty" => $id, "file" => $file, "title" => $title, "date" => date("Y-m-d H:i:s")), "bounty_files");
                move_uploaded_file($_FILES['documents']['tmp_name'][$i], ABSOLUTE_PATH . "/public/resources/files/payments/{$file}");
            }

            $notification['id_company'] = $this->_user->id_company;
            $notification['target'] = 'admin';
            $notification['type'] = 'payment';
            $notification['message'] = 'New uploaded files for bounty uploaded by ' . $this->_user->fistname . ' ' . $this->_user->lastname;
            $notification['url'] = 'view-bounties';
            $notification['viewed'] = 0;
            $notification['date'] = date("Y-m-d H:i:s");

            $this->insert($notification, "notifications");
        }
    }

    public function getAdmins() {
        $sql = $this->select()->from(array("u" => "users"))->where("type = 3");
        return $this->fetchAll($sql);
    }

    public function checkExclusive($data) {
        $sql = $this->select()
                ->from(array("u" => "users"), array("COUNT(u.id) AS total"));

        if (isset($data['keywords']) && $data['keywords'] != "") {
            $sql->where("firstname LIKE '%{$data['keywords']}%' OR lastname LIKE '%{$data['keywords']}%' OR availability LIKE '%{$data['keywords']}%' OR title LIKE '%{$data['keywords']}%' OR scope LIKE '%{$data['keywords']}%'");
        }

        if (isset($data['id_skill']) && !empty($data['id_skill'])) {
            $sql->join(array("us" => "user_skills"), "u.id = us.id_user", array());
            $sql->where("us.id_skill IN (" . implode(",", $data['id_skill']) . ")");
        }

        if (isset($data['id_country']) && !empty($data['id_country'])) {
            $sql->where("u.id_country IN (" . implode(",", $data['id_country']) . ")");
        }

        if (isset($data['id_city']) && !empty($data['id_city'])) {
            $sql->where("u.id_city IN (" . implode(",", $data['id_city']) . ")");
        }

        if (isset($data['id_department']) && !empty($data['id_department'])) {
            $sql->join(array("ud" => "user_departments"), "u.id = ud.id_user", array());
            $sql->where("ud.id_department IN (" . implode(",", $data['id_department']) . ")");
        }

        if (isset($data['id_industry']) && !empty($data['id_industry'])) {
            $sql->join(array("ui" => "user_industries"), "u.id = ui.id_user", array());
            $sql->where("ui.id_industry IN (" . implode(",", $data['id_industry']) . ")");
        }

        $data = $this->fetchRow($sql);

        return (int) $data['total'];
    }

    public function exclusiveMail($data, $skills, $countries, $cities, $departments, $industries) {
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $mail = new WebApp_Mail();
        $mail->addTo("adrian.vasilescu@brainspotting.ro", "Adrian");
        $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject("Exclusive newsletter form completion");
        $mail->setBodyFromViewScript("exclusive.phtml", array("user" => $this->_user, "data" => $data, "settings" => $this->_settings, "skills" => $skills, "countries" => $countries, "cities" => $cities, "departments" => $departments, "industries" => $industries));
        $mail->send($transport);
    }

    public function getPotentialRewardsCount() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("COUNT(j.id) AS total"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("id IN (SELECT id_item FROM gamification_points WHERE id_user = '{$this->_user->id}' AND action IS NOT NULL)");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function _getAvailableRewardsCount() {
        $sql = $this->select()
                ->from(array("j" => "jobs"), array("COUNT(j.id) AS total"))
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where('j.status =1')
                ->where('j.bounty_title IS NOT NULL');
//                    ->where("id NOT IN (SELECT id_item FROM gamification_points WHERE id_user = '{$this->_user->id}')")
//                    ->group("j.id");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getEarnedRewardsCount() {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("COUNT(b.id) AS total"))
                ->where("b.id_user = '{$this->_user->id}'")
                ->where("b.status = 'paid'");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }

    public function getAvailableRewardsCount() {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("COUNT(b.id) AS total"))
                ->where("b.id_user = '{$this->_user->id}'")
                ->where("b.status != 'paid'");
        $data = $this->fetchRow($sql);

        return $data['total'];
    }


    public function getProfileBounties() {
        $sql = $this->select()
                ->from(array("b" => "bounties"), array("b.id", 
                    "(SELECT SUM(gp.points) AS points FROM gamification_points gp WHERE gp.id_user = b.id_user AND gp.id_item = b.id_job) AS points"))
                ->join(array("j" => "jobs"), "b.id_job = j.id", array('j.id as _id_job', "j.bounty", "j.end AS due", "j.job_ad_lifetime"))
                ->join(array("jl" => "jobs_lang"), "j.id = jl.id_job", array("jl.title AS position"))
                ->join(array("u" => "users"), "b.id_user = u.id", array("CONCAT(u.firstname, ' ', u.lastname) AS candidate"))
                ->join(array("ja" => "job_applications"), "ja.id_recommender = b.id_user", array("ja.id_user as id_candidate", "ja.updated"))
//                ->joinLeft(array("uu" => "users"), "id_candidate = uu.id", array("firstname AS candidate_firstname", "lastname AS candidate_lastname"))
                ->where("b.id_user = '{$this->_user->id}'")
                ->where("b.id_company = '{$this->_user->id_company}'");
                
        $result = $this->fetchAll($sql);



        if (count($result) > 0) {

            foreach ($result as $key => &$value) {
                $_sql = $this->select()->from(array("u" => "users"), array("CONCAT(u.firstname, ' ', u.lastname) AS applicant"));
                $_sql->where("u.id =?", (int) $value['id_candidate']);
                $data = $this->fetchRow($_sql);
                $value['applicant'] = $data['applicant'];
                
                $date = $value['updated'];
                $date = strtotime($date);
                $days = "+".$value['job_ad_lifetime']." day";
                $date = strtotime($days, $date);


                $value['due_date'] = date('Y-m-d', $date);
            }
        }
//        echo '<pre>';
//        print_r($result);
//        die;
        return $result;
    }

    public function getCampaignReport($id_campaign) {
        $sql = $this->select()->from(array("cad" => "campaign_actions_detailed"), array("*"))
                ->join(array("u" => "users"), "u.id = cad.id_recommender", array("firstname", "lastname", "email"))
                ->where("cad.id_campaign =?", (int) $id_campaign);
        $result = $this->fetchAll($sql);
        $data = [];
        foreach ($result as $key => $row) {
            $data[$row['id_recommender']]['id_recommender'] = $row['id_recommender'];
            $data[$row['id_recommender']]['recommender_firstname'] = $row['firstname'];
            $data[$row['id_recommender']]['recommender_lastname'] = $row['lastname'];
            $data[$row['id_recommender']]['recommender_email'] = $row['email'];
            $data[$row['id_recommender']]['recommender_points'][$row['action']] = (int) $row['value'];
            $data[$row['id_recommender']]['actions'][] = $row['action'];
        }
        $_sql = $this->select()->from(array("cga" => "campaign_gamification_actions"), array('id_gamification_action'))
                ->join(array("ga" => "gamifications_actions"), "ga.id = cga.id_gamification_action", array('action'))
                ->where("cga.id_campaign =?", (int) $id_campaign);
        $gamifications = $this->fetchPairs($_sql);
        foreach ($data as $key => $value) {
            $sum;
            $actions_points = $value['recommender_points'];
            foreach ($actions_points as $action) {
                $sum += $action;
                $data[$key]['totals_points'] = $sum;
                $data[$key]['gamification_actions'] = $gamifications;
            }
        }
//        echo '<pre>';
        foreach ($data as $key => $value) {
            $gamification_actions = array_flip($value['gamification_actions']);
            $gamification_actions = array_map(function($val) {
                return $val = 0;
            }, $gamification_actions);
            $recommender_points = ($value['recommender_points']);
            $count = array_count_values($value['actions']);
            foreach ($recommender_points as $index => $val) {
                if (array_key_exists($index, $gamification_actions)) {

                    $gamification_actions[$index] = ($val * $count[$index]);
                }
                $data[$key]['gamification_partition'] = $gamification_actions;
            }
        }
        return $data;
    }

    public function setCampainWinners($id_campaign, $users) {



        $users = serialize($users);
        $this->update(array('id_user' => $users, 'status' => 'paid'), "id_campaign={$id_campaign}", "campaign_bounty");
    }
    
    public function sendBspNotification($paymentInfo) {
        
         $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }

        $filename = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails' . "/brainspotting_bounty_payment.phtml";
        $handle = fopen($filename, "r");
        $fcontents = fread($handle, filesize($filename));
        fclose($handle);


        $smarty = new Smarty;
        $smarty->assign("user", $paymentInfo['user']['firstname']." ".$paymentInfo['user']['lastname']);
        $smarty->assign("email", $paymentInfo['user']['email']);
        $smarty->assign("phone", $paymentInfo['user']['phone']);
        $smarty->assign("company", $paymentInfo['job']['company_slug']);
        $smarty->assign("bounty", $paymentInfo['job']['bounty']);
        $resource = $smarty->fetch('string:' . $fcontents);

        $mail = new WebApp_Mail();
        $mail->addTo("ana@brainspotting.ro");
        $mail->addTo("adrian@talentspotting.com");
        $mail->setFrom($this->_modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_modelCompany->getCompanyByUserId(Zend_Auth::getInstance()->getIdentity()->id)['contact_person']);
        $mail->setSubject("Bounty Paid from company ");
        $mail->setBodyHtml($resource);
        $mail->send($transport);
    }
    

}
