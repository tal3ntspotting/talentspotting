<?php
class models_Matching extends WebApp_Db_Table
{
	private $_settings = array();
	public $_name = 'users';
	public $_user = '';
	protected $_primary = 'id';
	public $cache;
    public $id_lang;
    protected $_modelUser;
    protected $_modelContent;
    public $_stats;

	public function __construct($id_lang = null)
	{
		parent::__construct();
		$this->cache = new WebApp_Cache($this);
        
        $this->_modelUser = new models_User;
         
        $this->_user = $this->_modelUser->_user;
        $this->id_lang = $this->_modelUser->id_lang;
        
		if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap')))
		{
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
			$this->_settings =  $bootstrap['resources']['Settings'];
		}
        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
	}

    public function matchUserVsJobSkills($job_skills)
    {
        $user_skills = $this->getUserMatchingSkills();
        if (!empty($user_skills))
        {
            $intersection = array_intersect($job_skills, $user_skills);
            if (!empty($intersection))
            { 
                return $intersection;
            }                        
            else
            {
                return $job_skills + $user_skills;
            }
        }
        else
        {
            return $job_skills + $user_skills;
        }
    }	
    
    public function getUserMatchingSkills()
    {
        $sql = $this->select()->from(array("us" => "user_skills"), array("id", "id_skill"))->where("id_user = ?", $this->_user->id);
        return $this->fetchPairs($sql);
    }
}