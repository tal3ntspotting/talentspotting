<?php

class models_Campaign extends WebApp_Db_Table {

    private $_settings = array();
    public $_name = 'campaigns';
    public $_user = '';
    protected $_primary = 'id';
    protected $_modelCompany ;
    protected $_modelUser ;
    public $cache;
    public $id_lang;
    public $_stats;

    public function __construct($id_lang = null) {
        parent::__construct();
        $this->cache = new WebApp_Cache($this);

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->_user = $user;
        $this->_modelCompany = new models_Company;
        $this->_modelUser = new models_User;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
    }

    public function getCampaigns() {
        $sql = $this->select()->from(array("c" => "campaigns"), array("*", '(SELECT count(id) FROM job_recommendations jr WHERE id_job IN (SELECT id_job FROM campaign_jobs as cj WHERE cj.id_campaign = c.id )) AS campaignTotalRecommendation'))
                ->order("c.status DESC");

        if ($this->_user->type == 2) {
            $sql->where("id_company = ?", $this->_user->id_company);
        }
        return $this->fetchAll($sql);
    }

    public function getCompanySlugByCampaignId($id_campaign) {
        $sql = $this->select()
                ->from(array("ca" => "campaigns"))
                ->where("ca.id =?", (int) $id_campaign)
                ->join(array("co" => "companies"), "ca.id_company = co.id", array("slug"));
        return $this->fetchRow($sql);
    }

    public function getCampaignsWithReports() {
        $sql = $this->select()
                ->from(array("c" => "campaigns"))
                ->joinLeft(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array("(SELECT COUNT(id) FROM job_applications WHERE id_job = cj.id_job AND id_recommender > 0) AS recommenders","id_job"));
//                ->group("c.id");

        if ($this->_user->type == 2) {
            $sql->where("id_company = ?", $this->_user->id_company);
        }
        $result  = $this->fetchAll($sql) ;
        
        foreach ($result as $key => $value ) {
            $data[$value['id']]['id'] = $value['id']; 
            $data[$value['id']]['title'] = $value['title']; 
            $data[$value['id']]['url'] = "<a class='link' href='/leaderboards-details?id_campaign={$value['id']}'>{$value['title']}</a>"; 
//            $data[$value['id']]['id_job'][] = $value['id_job']; 
            $data[$value['id']]['recommenders'] = count($this->_modelUser->getCampaignDetailedLeaderboards($value['id']));
        }
       return $data ;
    }
    public function getRecommendersFromjob( array $jobs) {
        foreach($jobs as $job) {
            $sql = $this->select()->from(array("jr" => "job_recommendations"),array("id_recommender"))
                    ->where("jr.id_job =?",(int) $job);
            $result = $this->fetchAll($sql);
            
        }
       return count($result);
    }

    public function getCampaignWithmyAction() {
         $sql = $this->select()
                 ->from(array("crd" => "campaign_actions_detailed"),array("*",
                     "(SELECT count(u.id) FROM users u WHERE crd.id_recommender = u.id AND u.id_company={$this->_user->id_company}) as active_users_in_campaign"))
                 ->join(array("c" =>"campaigns") ,"crd.id_campaign = c.id",array("title"))
                 ->joinLeft(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array("(SELECT COUNT(id) FROM job_applications WHERE id_job = cj.id_job AND id_recommender > 0) AS recommenders"));
                $sql->where("c.status = 1");
                $sql->where("c.id_company =?", (int) $this->_user->id_company);
                $result = $this->fetchAll($sql);
               
                $data = [] ;
                foreach($result as $index => $value) {
                    $data[$value['id_campaign']]['id_campaign'] = $value['id_campaign'];
                    $data[$value['id_campaign']]['users'][] = $value['id_user'];
                    $data[$value['id_campaign']]['title'] = $value['title'];
                } 
                foreach ($data as $index => $value ) {
                    $data[$index]['active_users'] = count($value['users']);
                }
                return $data ;

    }

    public function save($data) {
        $gam_actions = array_intersect_key($this->_settings['gamification']['actions'], array_flip($data['gamification_actions']));
        $JobsModel = new models_Jobs;
        $jobs_data = [];
        $data['title'] = str_replace("/", "-", $data['title']);
        foreach ($data['jobs'] as $job) {
            $jobs_data[] = $JobsModel->getJobById($job);
        }
        foreach ($jobs_data as $key => $job) {
            $jobs_data[$key]['url'] = WebApp_Tools::createJobSlug($job);
        }

        $recommenders = $data['recommenders'];
        unset($data['recommenders']);

        $jobs = $data['jobs'];
        unset($data['jobs']);

        $cities = $data['cities'];
        unset($data['cities']);

        $departments = $data['departments'];
        unset($data['departments']);

        $gamification_actions = $data['gamification_actions'];
        unset($data['gamification_actions']);

        $data['status'] = $data['save_type'];
        unset($data['save_type']);
        unset($data['real_save']);

        $adminLogged = Domain_User_UserFactory::createInstance($this->_user->id);
        if (isset($data['id']) && $data['id'] > 0) {
            $id_campaign = $data['id'];
            unset($data['id']);
            $data['slug'] = WebApp_Tools::CreateAlias($data['title']);
            $this->update($data, "$id = {id_campaign}", "campaigns");
            if ($_FILES['bounty_file']['name'] != "") {
                move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $_FILES['bounty_file']['name']);
                $this->update(array("bounty_file" => $_FILES['bounty_file']['name']), "$id_campaign = {id_campaign}", "campaign_media");
            }
        } else {
            $data['id_company'] = $this->_user->id_company;
            unset($data['id']);
            $data['is_promoted'] = isset($data['is_promoted']) ? $data['is_promoted'] : 0;
            $data['message'] = isset($data['message']) ? $data['message'] : '';

            if($data['status']) {
                
                $data['start'] = date("Y-m-d");
                $data['end'] = date("Y-m-d", strtotime("+{$this->_settings['jobs_lifetime'][$data['duration']]}"));
                unset($data['id']);
                $data['slug'] = WebApp_Tools::CreateAlias($data['title']);
                $this->insert($data, "campaigns");
                $id_campaign = $this->lastInsertId();
                $this->insert(array("id_campaign" => $id_campaign), "campaign_reports");

                $adminLogged->newCampaignEmail($data,$jobs_data,$gam_actions);
                $adminLogged->newCampaignEmailForAdmin($data,$jobs_data,$gam_actions);
                if ($_FILES['bounty_file']['name'] != "") {
                    move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $_FILES['bounty_file']['name']);
                    $this->insert(array("bounty_file" => $_FILES['bounty_file']['name'], "id_campaign" => $id_campaign), "campaign_media");
                } else {
                    $this->insert(array("id_campaign" => $id_campaign), "campaign_media");
                }
            } else {
                
                $data['start'] = date("Y-m-d");
                $data['end'] = date("Y-m-d", strtotime("+{$this->_settings['jobs_lifetime'][$data['duration']]}"));
                unset($data['id']);
                $data['slug'] = WebApp_Tools::CreateAlias($data['title']);
                $this->insert($data, "campaigns");
                $id_campaign = $this->lastInsertId();
                $this->insert(array("id_campaign" => $id_campaign), "campaign_reports");


                if ($_FILES['bounty_file']['name'] != "") {
                    move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $_FILES['bounty_file']['name']);
                    $this->insert(array("bounty_file" => $_FILES['bounty_file']['name'], "id_campaign" => $id_campaign), "campaign_media");
                } else {
                    $this->insert(array("id_campaign" => $id_campaign), "campaign_media");
                }
            }
            
            
        }



        $this->_addJobs($id_campaign, $jobs);
        $this->_addCities($id_campaign, $cities);
        $this->_addDepartments($id_campaign, $departments);
        $this->_addActions($id_campaign, $gamification_actions);
        $this->_addRecommenders($id_campaign, $recommenders);
    }

    private function _addJobs($id_campaign, $data) {
        $this->delete("id_campaign ={$id_campaign}", "campaign_jobs");
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_job" => $item), "campaign_jobs");
        }
    }

    private function _getJobs($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_jobs"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $items[] = $row['id_job'];
        }

        return (array) $items;
    }

    public function getCampaignJobs($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_jobs"), array("id_job"))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = c.id_job", array("title"))
                ->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchPairs($sql);

        return $data;
    }

    private function _addCities($id_campaign, $data) {
        $this->delete("id_campaign = {$id_campaign}", "campaign_cities");
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_city" => $item), "campaign_cities");
        }
    }

    private function _addRecommenders($id_campaign, $data) {
        $this->delete("$id_campaign = {id_campaign}", "campaign_recommenders");
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_user" => $item), "campaign_recommenders");
        }
    }

    private function _getCities($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_cities"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $items[] = $row['id_city'];
        }

        return (array) $items;
    }

    private function _addDepartments($id_campaign, $data) {
        $this->delete("id_campaign = {$id_campaign}", "campaign_departments");
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_department" => $item), "campaign_departments");
        }
    }

    private function _addSubDepartments($id_campaign, $data) {
        $this->delete("id_campaign = {$id_campaign}", "campaign_sub_departments");
        if(empty($data)) return 0 ;
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_sub_department" => $item), "campaign_sub_departments");
        }
    }

    private function _addSubSubDepartments($id_campaign, $data) {
        $this->delete("id_campaign = {$id_campaign}", "campaign_sub_sub_departments");
        foreach ((array) $data as $item) {
            $this->insert(array("id_campaign" => $id_campaign, "id_sub_sub_department" => $item), "campaign_sub_sub_departments");
        }
    }

    private function _getDepartments($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_departments"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $items[] = $row['id_department'];
        }

        return (array) $items;
    }

    private function _addActions($id_campaign, $data) {

        $this->delete("id_campaign ={$id_campaign}", "campaign_gamification_actions");
        foreach ((array) $data as $key => $item) {
            $validation = $item['validation'] ;
            if(empty($validation))
            {
                $this->insert(array("id_campaign" => $id_campaign,
                    "id_gamification_action" => $item['id_action'],
                    "points" => $item['points'],
                    "id_validation" => 0,
                    "active" => 0,
                ), "campaign_gamification_actions");
            } else {
                foreach($validation as $key => $valid)
                {
                    $this->insert(array("id_campaign" => $id_campaign,
                        "id_gamification_action" => $item['id_action'],
                        "points" => $item['points'],
                        "id_validation" => $valid['id_validation'],
                        "active" => $valid['active'],
                    ), "campaign_gamification_actions");
                }
            }

        }
    }

    public function _getActions($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_gamification_actions"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $items[] = $row['id_gamification_action'];
        }

        return (array) $items;
    }

    public function getCampaignGamifiedActions($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_gamification_actions"), array())
                ->join(array("ga" => "gamifications_actions"), "ga.id = c.id_gamification_action", array("id", "action"))
                ->where("id_campaign = ?", (int) $id_campaign);

        return $this->fetchPairs($sql);
    }

    private function _getRecommenders($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaign_recommenders"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $items[] = $row['id_user'];
        }

        return (array) $items;
    }



    public function _getMedia($id_campaign) {
        $sql = $this->select()->from(array("cm" => "campaign_media"), array("bounty_file"))->where("id_campaign = ?", (int) $id_campaign);
        $data = $this->fetchRow($sql);
        return $data["bounty_file"];
    }

    public function _addMedia($id_campaign, $data) {

        $this->insert(array("id_campaign" => $id_campaign, "bounty_file" => $data), "campaign_media");
    }

    public function activate($data) {
        if (!empty($data)) {
            foreach ($data as $id_campaign) {
                $campaign = $this->getCampaignById($id_campaign);
                $start = date("Y-m-d");
                $end = date("Y-m-d", strtotime("+{$this->_settings['jobs_lifetime'][$campaign['duration']]}"));
                $this->update(array("status" => 1, "start" => $start, "end" => $end), "$id = {id_campaign}", "campaigns");
            }
        }
    }

    public function deactivate($data) {
        if (!empty($data)) {
            foreach ($data as $id_campaign) {
                $this->update(array("status" => 0), "$id = {id_campaign}", "campaigns");
            }
        }
    }

    public function deleteCamp($id) {
        $id = (int) $id;
        $this->update(['status' => 2],"id = {$id}", "campaigns");

    }

    public function duplicate($id_campaign) {
        if ($id_campaign > 0) {
            $campaign = $this->getCampaignById($id_campaign);
            if (!empty($campaign)) {
                $insert = $campaign;
                $insert['title'] = "Copy " . $insert['title'];
                $insert['status'] = 0;
                unset($insert['id']);
                unset($insert['jobs']);
                unset($insert['cities']);
                unset($insert['departments']);
                unset($insert['gamification_actions']);
                unset($insert['recommenders']);
                unset($insert['bounty_file']);

                $this->insert($insert, "campaigns");
                $id_campaign = $this->lastInsertId();


                $this->_addActions($id_campaign, $campaign['gamification_actions']);
                $this->_addCities($id_campaign, $campaign['cities']);
                $this->_addJobs($id_campaign, $campaign['jobs']);
                $this->_addDepartments($id_campaign, $campaign['departments']);
                $this->_addRecommenders($id_campaign, $campaign['recommenders']);
                $this->_addMedia($id_campaign, $campaign['bounty_file']);
            }
        }
    }

    public function getCampaignsWithJobs() {
        $check_is_external = $this->_settings['external_platform'];
        $sql = $this->select()
                ->from(array("c" => "campaigns"), array('end AS deadline', "title","slug as campaign_slug" ,"id", "win_title AS campaign_bounty"))
                ->join(array("cj" => "campaign_jobs"), "c.id = cj.id_campaign", array())
                ->joinLeft(array("cm" => "campaign_media"), "c.id = cm.id_campaign", array("bounty_file AS picture"))
                ->join(array("j" => "jobs"), "cj.id_job = j.id", array("start", "end", "bounty", "bounty_file", "id as id_job", "career_level"))
                ->join(array("jl" => "jobs_lang"), "jl.id_job = j.id", array("title AS job", "slug", "requirements", "bounty_requirements", "tagline"))
                ->join(array('co' => 'companies'), "j.id_company = co.id", array('title as company_title', 'logo as company_logo', 'slug as company_slug'))
                ->joinLeft(array("jc" => "job_cities"), "jc.id_job = j.id", array("id_city"))
                ->joinLeft(array("ci" => "cities"), "jc.id_city = ci.id", array("title AS city"))
                ->joinLeft(array('cr' => 'campaign_recommenders'), 'cr.id_campaign=c.id', array('id_user'))
//                ->where("jl.id_lang =?", $this->id_lang)
//                    ->where("c.id_company = ?", (int) $this->_user->id_company)
                ->where("c.status = 1")
                ->where("j.status = 1");
        if ($check_is_external) {
            $sql->where("c.id_company = ?", (int) $this->_modelCompany->getCompanyBySubdomainSlug()['id']);
        } else {
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $sql->where("c.id_company = ?", (int) $this->_user->id_company);
            }
        }
        $data = $this->fetchAll($sql);

        foreach ((array) $data as $row) {
            $jobs[$row['id']]['id'] = $row['id'];
            $jobs[$row['id']]['title'] = $row['title'];
            $jobs[$row['id']]['campaign_slug'] = $row['campaign_slug'];
            $jobs[$row['id']]['deadline'] = $row['deadline'];
            $jobs[$row['id']]['picture'] = $row['picture'];
            $jobs[$row['id']]['campaign_bounty'] = $row['campaign_bounty'];
            $jobs[$row['id']]['campaign_cities'][] = $row['city'];
            $jobs[$row['id']]['campaign_users'][] = $row['id_user'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['id'] = $row['id_job'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['bounty'] = $row['bounty'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['bounty_file'] = $row['bounty_file'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['title'] = $row['job'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['slug'] = $row['slug'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['requirements'] = $row['requirements'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['bounty_requirements'] = $row['bounty_requirements'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['company_title'] = $row['company_title'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['company_logo'] = $row['company_logo'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['company_slug'] = $row['company_slug'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['career_level'] = $row['career_level'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['tagline'] = $row['tagline'];
            $jobs[$row['id']]['jobs'][$row['id_job']]['cities'][$row['id_city']] = $row['city'];
        }
//        echo '<pre>';print_r($jobs);die;
        return (array) $jobs;
    }

    public function updateCampaignsStats() {
        $this->query("UPDATE campaign_reports c SET views = (SELECT IFNULL(SUM(value), 0) as views FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'view')");
        $this->query("UPDATE campaign_reports c SET shares = (SELECT IFNULL(SUM(value), 0) as shares FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'share')");
        $this->query("UPDATE campaign_reports c SET applications = (SELECT IFNULL(SUM(value), 0) as applications FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'application')");
        $this->query("UPDATE campaign_reports c SET recommendations = (SELECT IFNULL(SUM(value), 0) as recommendations FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'recommendation')");
        $this->query("UPDATE campaign_reports c SET email_refferences = (SELECT IFNULL(SUM(value), 0) as email_refferences FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'email_refference')");
        $this->query("UPDATE campaign_reports c SET bounties = (SELECT IFNULL(SUM(value), 0) as bounties FROM campaign_reports_detailed WHERE id_campaign = c.id_campaign AND action = 'bounties')");
    }

    public function getLeaderboards($id_campaign) {
        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    "$(SELECT COUNT(ja.id_user) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{id_campaign}' AND ja.id_user = u.id AND ja.status = 4 AND ja.status_reason = 5) AS success",
                    "$(SELECT COUNT(ja.id_user) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{id_campaign}' AND ja.id_user = u.id AND ja.status = 2) AS fail",
                    "$(SELECT COUNT(ja.id_user) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{id_campaign}' AND ja.id_user = u.id AND ja.status NOT IN (2,4)) AS pending",
                    "$(SELECT COUNT(ja.id_user) FROM job_applications ja, campaign_jobs cj WHERE ja.id_job = cj.id_job AND cj.id_campaign = '{id_campaign}' AND ja.id_user = u.id AND ja.id_recommender > 0) AS application_on_recommend",
                    "$(SELECT SUM(g.points) FROM gamification_points g, campaign_jobs cj WHERE g.id_item = cj.id_job AND cj.id_campaign = '{id_campaign}' AND g.id_user = u.id GROUP BY id_user) AS total_points",
                ))
                ->join(array("ja" => "job_applications"), "u.id = ja.id_user", array())
                ->join(array("j" => "jobs"), "ja.id_job = j.id", array())
                ->join(array("cj" => "campaign_jobs"), "j.id = cj.id_job", array())
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("cj.id_campaign = ?", (int) $id_campaign)
                ->where("u.type != ?", 9)
                ->order("total_points DESC");
        $leaderboards['user'] = $this->fetchAssoc($sql);

        $i = 1;
        foreach ($leaderboards['user'] as $id_user => $value) {
            if ($id_user == $this->_user->id) {
                $leaderboards['place'] = $i;
            }
            $i++;
        }

        $sql = $this->select()
                ->from(array("u" => "users"), array(
                    'u.id', 'u.firstname', 'u.lastname', 'u.image',
                    "$(SELECT SUM(g.points) FROM gamification_points g, campaign_jobs cj WHERE g.id_item = cj.id_job AND cj.id_campaign = '{id_campaign}' AND g.id_user = u.id GROUP BY id_user) AS total_points",
                ))
                ->join(array("jr" => "job_recommendations"), "jr.id_recommender = u.id", array())
                ->join(array("j" => "jobs"), "jr.id_job = j.id", array())
                ->join(array("cj" => "campaign_jobs"), "j.id = cj.id_job", array())
                ->where("j.id_company = ?", (int) $this->_user->id_company)
                ->where("cj.id_campaign = ?", (int) $id_campaign);
        $leaderboards['recommenders'] = $this->fetchAssoc($sql);



        return $leaderboards;
    }

    public function removeCampaignPicture($id_campaign) {
        return $this->update(array("bounty_file" => ""), "$id_campaign = {id_campaign}", "campaign_media");
    }
    
    public function getAllactiveCampaign() {
        $sql = $this->select()
                ->from(['c' => 'campaigns'],['*'])
                ->where('c.status =?',1);
                return $this->fetchAll($sql);
    }

    public function saveCampaign($request)
    {

//        $cities = $request['cities'];
//        unset($request['cities']);
//
//        $countries = $request['countries'];
//        unset($request['countries']);
//
//        $cities = $request['counties'];
//        unset($request['counties']);

        $departments = ($request['departments']);
        unset($request['departments']);
//
        $sub_departments = ($request['sub_departments']);
        unset($request['sub_departments']);
//
        $sub_sub_departments = ($request['sub_sub_departments']);
        unset($request['sub_sub_departments']);

        $jobs = ($request['jobs']);
        unset($request['jobs']);

        $projects = ($request['projects']);
        unset($request['projects']);

        $selected_cities = ($request['selected_cities']);
        unset($request['selected_cities']);

        $selected_departments = ($request['selected_departments']);
        unset($request['selected_departments']);

        $gamification_actions = ($request['gamifications']);
        unset($request['gamifications']);

        $validations = ($request['validations']);
        unset($request['validations']);

        $data['title'] = $request['title'];
        $data['slug'] = WebApp_Tools::CreateAlias($request['title']);
        $data['start'] = $request['start_date'];
        $data['end'] = $request['end_date'];
        $data['win_title'] = $request['win_title'];
        $data['win_description'] = $request['win_description'];
        $data['id_currency'] = $request['id_currency'];
        $data['id_company'] = $this->_user->id_company;
        $data['win_amount'] = $request['win_amount'];
        $data['winner_announcement_date'] = $request['winner_announcement_date'];
        $data['reward_redeem_date'] = $request['reward_redeem_date'];
        $data['minim_point_winner'] = $request['minim_point_winner'];
        $data['created'] = date('Y-m-d H:i:s');
        $data['message'] = $request['message'];
        $data['id_payment_type'] = $request['id_payment_type'];

        $insert = false;

        if(isset($request['id']) && $request['id'] > 0)
        {
            $id_campaign = $request['id'];
            $this->update($data,"id={$id_campaign}","campaigns");
        }
        else
        {
            $data['status'] = 0;
            $this->insert($data,'campaigns');
            $id_campaign = $this->lastInsertId();
            $insert = true;

        }

        $this->_addJobs($id_campaign, $jobs);
        $this->_addActions($id_campaign, $gamification_actions);
        $this->_addValidations($id_campaign, $validations);
        $this->_addProjects($id_campaign, $projects);
        $this->_addCities($id_campaign, $selected_cities);
        $this->_addDepartments($id_campaign, $departments);
        $this->_addSubDepartments($id_campaign, $sub_departments);
        $this->_addSubSubDepartments($id_campaign, $sub_sub_departments);

        if($insert == true)
        {
            $userObject = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
            $userObject->sendNewCampaignEmailCreated($id_campaign);

            if($data['start'] == date("Y-m-d"))
            {
                $this->update(['status' => 1],"id={$id_campaign}","campaigns");
                $userObject->sendNewCampaignEmailForAdmin($data,$jobs,$gamification_actions);
            }

        }

        return $id_campaign;
    }

    public function _addValidations($id_campaign, $data)
    {
        $this->delete("id_campaign ={$id_campaign}", "campaign_validation");
        foreach ((array) $data as $key => $item) {
            $this->insert(array("id_campaign" => $id_campaign,
                "id_validation" => $item['id_validation'],
                "status" => $item['active']
            ), "campaign_validation");
        }
    }

    public function _addProjects($id_campaign, $data)
    {
        $this->delete("id_campaign ={$id_campaign}", "campaign_project");
        if(!empty($data))
        {
            foreach ((array) $data as $key => $item) {
                $this->insert(array("id_campaign" => $id_campaign,
                    "id_project" => $item
                ), "campaign_project");
            }
        }

    }


    public function deleteCampaignById($id)
    {
        $id = (int) $id ;
        $this->delete("id_campaign={$id}","campaign_project");
        $this->delete("id_campaign={$id}","campaign_validation");
        $this->delete("id_campaign={$id}","campaign_jobs");
        $this->delete("id_campaign={$id}","campaign_gamification_actions");
        $this->delete("id_campaign={$id}","campaign_cities");
        $this->delete("id_campaign={$id}","campaign_departments");
    }

    public function getCampaignById($id_campaign) {
        $sql = $this->select()->from(array("c" => "campaigns"),['*'])->where("c.id = ?", (int) $id_campaign)
            ->joinLeft(['cr' => 'currencies'],"cr.id=c.id_currency",['title as currency'])
            ->joinLeft(['pt' => 'payment_type'],"pt.id=c.id_payment_type",['title as payment_type']);
        $data = $this->fetchRow($sql);

        $data['jobs'] = $this->getCampaignJob($id_campaign);
        $data['countries'] = $this->__getCampaignCountries($id_campaign);
        $data['counties'] = $this->__getCampaignCounties($id_campaign);
        $data['cities'] = $this->getCampaignCities($id_campaign);
        $data['departments'] = $this->getCampaignDepartments($id_campaign);
        $data['gamification_actions'] = $this->getCampaignAction($id_campaign);
//        $data['actions'] = $this->getCampaignGamifiedActions($id_campaign);
        $data['bounty_file'] = '/resources/files/bounties/'.$this->_getMedia($id_campaign);
        $data['projects'] = $this->getCampaignProjects($id_campaign);
        $data['departments'] = $this->getCampaignDepartments($id_campaign);
        $data['sub_departments'] = $this->getCampaignSubDepartments($id_campaign);
        $data['sub_sub_departments'] = $this->getCampaignSubSubDepartments($id_campaign);
//        $data['validations'] = $this->getCampaignValidations($id_campaign);

        return $data;
    }

    public function getCampaignProjects($id_campaign)
    {
        $sql = $this->select()->from(['cp' => 'campaign_project'],[])
            ->join(['p' => 'projects'],"cp.id_project=p.id",['id','title'])
        ->where("cp.id_campaign =?",(int) $id_campaign);

        return $this->fetchAll($sql);
    }

    public function getCampaignValidations($id_campaign)
    {
        $sql = $this->select()->from(['cv' => 'campaign_validation'],['status'])
            ->join(['v' => 'validation'],"v.id=cv.id_validation",['id','title'])
            ->where("cv.id_campaign =?",(int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getCampaignAction($id_campaign)
    {
        $sql = $this->select()->from(['cga' => 'campaign_gamification_actions'],['points','id as id_action'])
            ->join(['gc' => 'gamifications_actions'],"gc.id=cga.id_gamification_action",['action as title','id','title as action','explain'])
            ->where('cga.id_campaign =?',(int) $id_campaign)->group("id_gamification_action")
            ->order('gc.action_order');
        $result =  $this->fetchAll($sql);
        foreach($result as $key => &$value)
        {
            $sql = $this->select()->from(['cga' => 'campaign_gamification_actions'],['id_validation','active'])
                ->join(['v' => 'validation'],"v.id=cga.id_validation",['title'])
                ->where('cga.id_campaign =?',(int) $id_campaign)->where("cga.id_gamification_action =?",(int) $value['id']);
            $value['validation'] =  $this->fetchAll($sql);
        }
        return $result;
    }

    public function getCampaignDepartments($id_campaign)
    {
        $sql = $this->select()->from(['cd' => 'campaign_departments'],[])
            ->join(['dl' => 'departments_lang'],"cd.id_department=dl.id_department",
                ['id_department as id','title'])
            ->where('cd.id_campaign =?',(int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getCampaignSubDepartments($id_campaign)
    {
        $sql = $this->select()->from(['cd' => 'campaign_sub_departments'],[])
            ->join(['sd' => 'sub_departments'],"cd.id_sub_department=sd.id",
                ['id','title'])
            ->where('cd.id_campaign =?',(int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getCampaignSubSubDepartments($id_campaign)
    {
        $sql = $this->select()->from(['cd' => 'campaign_sub_sub_departments'],[])
            ->join(['sd' => 'sub_sub_departments'],"cd.id_sub_sub_department=sd.id",
                ['id','title'])
            ->where('cd.id_campaign =?',(int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getCampaignCities($id_campaign)
    {
        $sql = $this->select()->from(['cc' => 'campaign_cities'],[])
            ->join(['c' => 'cities'],"cc.id_city=c.id",
                ['id','title'])
            ->where('cc.id_campaign =?',(int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getCampaignJob($id_campaign)
    {
        $sql = $this->select()->from(array("c" => "campaign_jobs"), array("id_job as id"))
            ->join(array("jl" => "jobs_lang"), "jl.id_job = c.id_job", array("title"))
            ->where("c.id_campaign = ?", (int) $id_campaign);
        return $this->fetchAll($sql);
    }

    public function getAllCampaigns()
    {
        $sql = $this->select()->from(['c' => 'campaigns'],['id'])->order("c.created DESC");
        $result = $this->fetchAll($sql);
        $data = [] ;
        foreach($result as $index => $campaign)
        {
            $data[$campaign['id']] = $this->getCampaignById($campaign['id']);
        }

        return $data ;
    }

    public function changeCampaignStateById($id,$status)
    {
        $campaign = $this->getCampaignById($id);
        if(!empty($campaign))
        {
            $this->update(['status' => $status],"id={$id}","campaigns");
        }
    }

    public function getCampaignFirstPlacesById($id_campaign)
    {
        $sql = $this->select()->from(['cad' => 'campaign_actions_detailed'],['sum(cad.value) as total_points'])
            ->join(['u' => 'users'],"u.id=cad.id_user",['u.firstname','u.lastname'])
            ->group('cad.id_user')
            ->where('u.type =?',(int) 1)
            ->where('cad.id_campaign =?',(int) $id_campaign)->order('total_points DESC')->limit(3);
        return $this->fetchAll($sql);
    }

    public function getAllCampaigains()
    {

    }

    public function getActiveGamificationAction()
    {
        $sql = $this->select()->from(['ga' => 'gamifications_actions'])
            ->where('ga.action_order IS NOT NULL');
        return $this->fetchAll($sql);
    }

    public function generateExport($start = null, $end = null)
    {
        $sql = $this->select()
            ->from(['c' => 'campaigns'],[
                'id as campaign_id',
                'title as campaign_name ',
                'win_amount as campaign_win_amount',
                'win_title as campaign_win_title',
                'message as campaign_good_to_know',
                'start as campaign_start_date',
                'end as campaigns_end_date',
                'winner_announcement_date as campaigns_winners_announce_date',
                'reward_redeem_date as campaigns_winners_reward_date'])
            ->joinLeft(['cc' => 'currencies'],'cc.id=c.id_currency',['cc.code as campaign_win_currency'])
        ->joinLeft(['cad' => 'campaign_actions_detailed'],'c.id=cad.id_campaign',[ 'cad.date as action_registered_date'] )
        ->joinLeft(['u' => 'users'],'u.id=cad.id_user',['firstname as employee_firstname',
            'lastname as employee_surname',
            'email as employee_email'])
        ->join(['cga' => 'campaign_gamification_actions'],'cga.id=cad.id_action',['points as campaign_action_points'])
        ->join(['ga' => 'gamifications_actions'],'cga.id_gamification_action=ga.id',['title as campaign_action_name'])
        ->join(['jl' => 'jobs_lang'],'jl.id_job=cad.id_job',['title as job_name'])
        ->join(['jc' => 'job_cities'],'jl.id_job=jc.id_job',[])
        ->joinLeft(['cit' => 'cities'],'cit.id=jc.id_city',['title as job_city'])
        ->joinLeft(['pt' => 'payment_type'],'pt.id=c.id_payment_type',['pt.title as campaign_win_payment']);
        $sql->where('cad.date >?',$start);
        $sql->where('cad.date <?',$end);
        $result = $this->fetchAll($sql);
        foreach ($result as $index => $item) {
            uksort($result[$index],function ($a,$b){
                $order = [
                    'campaign_id' => 1,
                    'campaign_name ' => 2,
                    'campaign_good_to_know' => 3,
                    'campaign_start_date' => 4,
                    'campaigns_end_date' => 5,
                    'campaigns_winners_announce_date' => 6,
                    'campaigns_winners_reward_date' => 7,
                    'campaign_action_name' => 8,
                    'campaign_action_points' => 9,
                    'campaign_win_title' => 10,
                    'campaign_win_amount' => 11,
                    'campaign_win_currency' => 12,
                    'campaign_win_payment' => 13,
                    'job_name' => 14,
                    'job_city' => 15,
                    'employee_firstname' => 16,
                    'employee_surname' => 17,
                    'employee_email' => 18,
                    'action_registered_date' => 19
                ];
                return $order[$a] - $order[$b];
            });
        }
        return $result;
    }

    private function __getCampaignCountries($id) {
        $sql = $this->select()
            ->from(array("c" => "counties"), array())
            ->join(array("ci" => "cities"), "c.id = ci.id_county", array())
            ->join(array("jc" => "campaign_cities"), "ci.id = jc.id_city",array())
            ->join(array('con' => 'countries'),"con.id=c.id_country",array('id','title'))
            ->where("jc.id_campaign = ?", $id);
        return $this->fetchAll($sql);
    }

    private function __getCampaignCounties($id) {
            $counties = [];
            $cities = $this->getCampaignCities($id);
            foreach ($cities as $key => $city) {
                $sql = $this->select()
                    ->from(['c' => 'cities'],[])
                    ->join(['cc' => 'counties'],"cc.id=c.id_county",array('title','cc.id'))
                    ->where('c.id=?',(int) $city['id']);
                $counties[] = $this->fetchRow($sql);
            }

        return (array_values(array_unique($counties)));
    }

    public function getCampaignDepartmentsDataById($id)
    {
        $departments = $this->getCampaignDepartments($id);

        $id_departments = [];

        foreach($departments as $key => $value)
        {
            $id_departments[] = $value['id'];
        }

        $data['departments'] = $departments;
        $sub_departments = [] ;

        if(!empty($id_departments))
        {
            $sql = $this->select()->from(['sd' => 'sub_departments'],['id','title'])
                ->where("sd.id_department IN (" . implode(",", $id_departments) . ")");
            $sub_departments = $this->fetchAll($sql);
        }

        $data['sub_departments'] = $sub_departments;


        $id_sub_detartments = [] ;

        if(!empty($sub_departments))
        {
            foreach($sub_departments as $key => $value)
            {
                $id_sub_detartments[] = $value['id'];
            }
        }

        $sub_sub_departments = [] ;

        if(!empty($id_sub_detartments))
        {
            $sql = $this->select()->from(['ssd' => 'sub_sub_departments'],['id','title'])
                ->where("ssd.id_sub_department IN (" . implode(",", $id_sub_detartments) . ")");
            $sub_sub_departments = $this->fetchAll($sql);
        }

        $data['sub_sub_departments'] = $sub_sub_departments;
        return $data;
    }

    public function getCloseToEndCampaign()
    {
        $today = date('Y-m-d');
        $next_week = date('Y-m-d',strtotime('next week'));
        $sql = $this->select()
            ->from(['c' => 'campaigns'],['count(id) as total'])
            ->where('c.id_company =?',(int) Zend_Auth::getInstance()->getIdentity()->id_company)
            ->where('c.end >=?',$today)
            ->where('c.end <=?',$next_week)
            ->where('c.status = 1');

        return $this->fetchRow($sql);
    }

}
