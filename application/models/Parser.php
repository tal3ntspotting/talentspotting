<?php
class models_Parser extends WebApp_Db_Table
{
	private $_settings = array();
	public $_name = 'users';
	public $_user = '';
	protected $_primary = 'id';
	public $cache;
    public $id_lang;
    public $content;

	public function __construct($id_lang = null)
	{
		parent::__construct();
		$this->cache = new WebApp_Cache($this);

		$user = Zend_Auth::getInstance()->getIdentity();
		$this->_user = $user;
        
        $this->content = new models_Content;
        
		if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap')))
		{
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
			$this->_settings =  $bootstrap['resources']['Settings'];
		}
        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
	}
    
    public function advance($string, &$data)
    {
        if (stripos($data, $string) !== false)
        {
            $data = substr($data, stripos($data, $string) + strlen($string));            
        }
        else
        {
            $data = null;
        }
        
        return $data;
    }
    
    public function fetch($start, $end, $data)
    {
        return substr($data, strpos($data, $start) + strlen($start), strpos($data, $end) - (strpos($data, $start) + strlen($start)));
    }
    
    public function getDepartmentsEjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            if (stripos($result->textContent, "Departament:") !== false)
            {
                $data = trim(substr($result->textContent, strlen("Departament:")));   
            }                
        }
        
        $departments = explode("/", $data);        
        $dbDepartment = $this->content->getDepartmentsSelectImport();
        foreach ((array) $departments as $department)
        {
            if (!in_array(trim($department), $dbDepartment))
            {
                $id_department = $this->content->saveDepartment(array("title" => trim($department)));
                $user_departments[$id_department] = $id_department;
            }
            else
            {
                $id = array_search(trim($department), $dbDepartment);
                $id_department = $this->content->getDepartmentByLangId($id);
                
                $user_departments[$id_department] = $id_department;
            }
        }
        
        $this->content->saveUserDepartments($user_departments);
        
    }
    
    public function getDepartmentsBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
                
        foreach ($results as $result)
        {
            $children = $result->childNodes;
            foreach ($children as $child)
            {
                if (stripos($child->textContent, "Desired domains:") !== false)
                {
                    $data = substr($child->textContent, strlen("Desired domains:"));
                }
            }
        }
        
        $departments = explode(",", $data);        
        $dbDepartment = $this->content->getDepartmentsSelectImport();
        foreach ((array) $departments as $department)
        {
            if (!in_array(trim($department), $dbDepartment))
            {
                $id_department = $this->content->saveDepartment(array("title" => trim($department)));
                $user_departments[$id_department] = $id_department;
            }
            else
            {
                $id = array_search(trim($department), $dbDepartment);
                $id_department = $this->content->getDepartmentByLangId($id);
                
                $user_departments[$id_department] = $id_department;
            }
        }
        
        $this->content->saveUserDepartments($user_departments);
    }
    
    public function getIndustriesEjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            if (stripos($result->textContent, "Industrie:") !== false)
            {
                $data = trim(substr($result->textContent, strlen("Industrie:")));   
            }                
        }
        
        $industries = explode("/", $data);
        $dbIndustries = $this->content->getIndustriesSelectImport();
        foreach ((array) $industries as $industry)
        {
            if (!in_array(trim($industry), $dbIndustries))
            {
                $id_industry = $this->content->saveIndustry(array("title" => trim($industry)));
                $user_industries[$id_industry] = $id_industry;
            }
            else
            {
                $id = array_search(trim($industry), $dbIndustries);
                $id_industry = $this->content->getIndustryByLangId($id);
                
                $user_industries[$id_industry] = $id_industry;
            }
        }
        
        $this->content->saveUserIndustries($user_industries);
    }
    
    public function getLocationEjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            if (stripos($result->textContent, "Oras de lucru:") !== false)
            {
                return trim(substr($result->textContent, strlen("Oras de lucru:")));   
            }                
        }
    }
    
    public function getLocationBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            $children = $result->childNodes;
            foreach ($children as $child)
            {
                if (stripos($child->textContent, "Desired locations:") !== false)
                {
                    $data = substr($child->textContent, strpos($child->textContent, "Desired locations:") + strlen("Desired locations:"));
                }
            }
        }
        
        return (string) $data;
    }
    
    public function getSkillsBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        $skills = array();
        foreach ($results as $result)
        {
            $children = $result->childNodes;
            foreach ($children as $child)
            {
                if ($child->textContent == 'Skills')
                {
                    $parent = $child->parentNode;
                    foreach ($parent->childNodes as $node)
                    {
                        if ($node->tagName == "div" && $node->textContent != "Skills")
                        {
                            foreach ($node->childNodes as $last_node)
                            {
                                if (!isset($last_node->tagName) && trim($last_node->textContent) != "")
                                {
                                    $skill = str_replace(";", "", trim($last_node->textContent));
                                    $sk = explode(",", $skill);
                                    $skills = array_merge($sk, $skills);
                                }
                            }
                        }
                    }
                }
            }
        }
                
        $dbSkills = $this->content->getSkillsSelectImport();
        foreach ((array) $skills as $skill)
        {
            if (!in_array(trim($skill), $dbSkills))
            {
                $id_skill = $this->content->saveSkill(array("title" => trim($skill)));
                $user_skills[$id_skill] = $id_skill;
            }
            else
            {
                $id = array_search(trim($skill), $dbSkills);
                $id_skill = $this->content->getSkillByLangId($id);
                
                $user_skills[$id_skill] = $id_skill;
            }
        }
        
        $this->content->saveUserSkills($user_skills);
    }
    
    public function getAvailabilityEjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            if (stripos($result->textContent, "Disponibil:") !== false)
            {
                return trim(substr($result->textContent, strlen("Disponibil:")));   
            }                
        }
    }
    
    public function getExperienceEjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);                 
        
        foreach ($results as $result)
        {
            $table = $result->parentNode->parentNode;
            if ($table->tagName == 'table')
            {
                $children = $table->childNodes;
                $i = 0;
                foreach ($children as $child)
                {                    
                    if ($child->tagName == 'tr' && stripos(trim($child->textContent), "experienta") === false)
                    {                        
                        $elements = $child->childNodes;
                        foreach ($elements as $element)
                        {                            
                            $paragraphs = $element->childNodes;
                            if (!empty($paragraphs))
                            {
                                foreach ($paragraphs as $paragraph)
                                {
                                    $contents = $paragraph->childNodes;
                                    if (!empty($contents))
                                    {
                                        foreach ($contents as $content)
                                        {
                                            if ($content->tagName == 'b')
                                            {
                                                $jobs[$i]['title'][] = $content->textContent;    
                                            }
                                            else if (isset($content->tagName))
                                            {                                        
                                                $jobs[$i]['content'][] = $content->textContent;
                                            }
                                        }   
                                    }                                    
                                }    
                            }                                                                                    
                        }
                    }
                    
                    $i++;
                }
            }
                   
        }
        
        foreach ($jobs as $key => $job)
        {
            $positions[$key]['title'] = $job['title'][0];
            $positions[$key]['id_user'] = $this->_user->id;
            $date = explode("-", $job['content'][0]);
            $positions[$key]['start'] = date("Y-m-d", strtotime($date[0]));
            if (stripos($date[1], "prezent") !== false)
            {
                $positions[$key]['is_current'] = 1;
            }
            else
            {
                $positions[$key]['is_current'] = 0;
                $positions[$key]['end'] = date("Y-m-d", strtotime($date[1]));
            }
            foreach ($job['title'] as $k => $tag)
            {
                if (stripos($tag, "firma") !== false)
                {
                    $positions[$key]['company'] = $job['content'][$k];
                }
                
                if (stripos($tag, "responsabilitati") !== false)
                {
                    $positions[$key]['summary'] = $job['content'][$k];
                }
            }
        }
        
        return (array) $positions;
    }
    
    public function getPhotoBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);        
        
        if ($this->_user->image == "")
        {            
            foreach ($results as $result)
            {
                $image = $result->getAttribute("href");
                if ($image != "")
                {                                 
                    $file = $this->_user->id."_".substr($image, strrpos($image, "/")+1);
                    file_put_contents(ABSOLUTE_PATH."/public/resources/files/user/".$file, file_get_contents($image));
                    WebApp_UploadImage::constrainImage(300, 300, ABSOLUTE_PATH."/public/resources/files/user/".$file, ABSOLUTE_PATH."/public/resources/files/user/".$file);                    
                }
            }    
        }
        
        return (string) $file;
    }
    
    public function getEducationBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);
        
        $education = array();
        $i = 0;
        foreach ($results as $result)
        {            
            $children = $result->childNodes;
            foreach ($children as $child)
            {
                if ($child->tagName == 'div' && $child->getAttribute('class') == 'cv-section-period')
                {
                    $period = explode("-", trim($child->textContent));
                    $education[$i]['start'] = trim($period[1]);
                    $education[$i]['id_user'] = $this->_user->id;
                    if (strpos($period[0], 'Present') === false)
                    {
                        $education[$i]['end'] = trim($period[0]);                        
                    }
                }
                else if ($child->tagName == 'div' && ($child->getAttribute('class') == 'cv-section-circled' || $child->getAttribute('class') == 'cv-section-circled-last'))
                {
                    $nodes = $child->childNodes;
                    foreach ($nodes as $node)
                    {
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'stud-name')
                        {
                            $education[$i]['title'] = trim($node->textContent);
                        }
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'stud-degree')
                        {
                            $education[$i]['degree'] = trim($node->textContent);
                        }
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'stud-domain')
                        {
                            $education[$i]['field'] = trim($node->textContent);
                        }
                    }
                }
            }
            
            $i++;
        }
        
        
        $modelUser = new models_User;
        
        $dbEducation = $modelUser->getUserEducation();
        foreach ((array) $education as $key => $edu)
        {
            foreach ($dbEducation as $row)
            {
                if ($row['title'] == $edu['title'] && $row['start'] == $edu['start'])
                {
                    unset($education[$key]);
                }
            }
        }
        
        foreach ((array)$education as $data)
        {
            $this->insert($data, "user_education");
        }
    }
    
    public function getExperienceBestjobs($query, $data)
    {
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);
        
        $experience = array();
        $i = 0;
        foreach ($results as $result)
        {            
            $children = $result->childNodes;
            foreach ($children as $child)
            {
                if ($child->tagName == 'div' && $child->getAttribute('class') == 'cv-section-period')
                {
                    $period = explode("-", trim($child->textContent));
                    $experience[$i]['id_user'] = $this->_user->id;
                    
                    $experience[$i]['end'] = date("Y-m-d", strtotime(trim($period[1])));                    
                    if (strpos($period[0], 'Present') === false)
                    {
                        $experience[$i]['start'] = date("Y-m-d", strtotime(trim($period[0])));                        
                        $experience[$i]['is_current'] = 0;
                    }
                    else
                    {
                        $experience[$i]['is_current'] = 1;
                    }
                }
                else if ($child->tagName == 'div' && ($child->getAttribute('class') == 'cv-section-circled' || $child->getAttribute('class') == 'cv-section-circled-last'))
                {
                    $nodes = $child->childNodes;
                    foreach ($nodes as $node)
                    {
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'cmp-name')
                        {
                            $experience[$i]['company'] = trim($node->textContent);
                        }
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'position')
                        {
                            $experience[$i]['title'] = trim($node->textContent);
                        }
                        if ($node->tagName == 'span' && $node->getAttribute('class') == 'job-desc')
                        {
                            $experience[$i]['summary'] = trim($node->textContent);
                        }
                    }
                }
            }
            
            $i++;
        }
        
        $modelUser = new models_User;
        
        $dbExperience = $modelUser->getUserPositions();
        
        foreach ((array)$experience as $key => $position)
        {
            foreach ($dbExperience as $exp)
            {
                if ($exp['title'] == $position['title'] && $exp['company'] == $position['company'])
                {
                    unset($experience[$key]);
                }
            }
        }
        
        foreach ((array) $experience as $data)
        {
            $this->insert($data, "user_positions");
        }                
    }
    
    public function fetchCompany($id)
    {
        $data = file_get_contents("https://www.linkedin.com/company/{$id}");
        $query = "div.basic-info-description";
        
        $dom = new Zend_Dom_Query($data);
        $results = $dom->query($query);
        
        foreach ($results as $result)
        {
            $description = trim($result->textContent);
            $companyData['description'] = $description;
        }       

        if ($description != "")
        {
            $dom = new Zend_Dom_Query($data);
            $query = "div.basic-info-about > div.specialties > p";
            $results = $dom->query($query);
            
            foreach ($results as $result)
            {
                $companyData['specialities'] = trim($result->textContent);
            }
            
            $dom = new Zend_Dom_Query($data);
            $query = "div.basic-info-about > ul > li";
            $results = $dom->query($query);
            
            foreach ($results as $result)
            {
                foreach ($result->attributes as $k => $v)
                {
                    if ($v->name == 'class')
                        $nodes[] = $v->value;    
                }
                
                
                foreach ($result->childNodes as $child)
                {                                        
                    if ($child->tagName == 'p')
                    {
                        $values[] = trim($child->textContent);
                    }
                }
            }
            
            $specs = @array_combine((array) $nodes, (array) $values);
            foreach ($specs as $key => $value)
            {
                switch ($key)
                {
                    case 'website':
                        $companyData['url'] = $value;
                    break;
                    
                    case 'industry':
                        $companyData['id_industry'] = $this->searchIndustry($value);
                    break;
                    
                    case 'type':
                        $companyData['company_type'] = $value;
                    break;
                    
                    case 'vcard hq':
                        $companyData['address'] = str_replace("<br />", " ", nl2br($value));
                    break;
                    
                    case 'company-size':
                        $companyData['size'] = $value;
                    break;
                    
                    case 'founded':
                        $companyData['founded'] = $value;
                    break;
                }
            }

            return $companyData;
        }
        else
        {
            return false;
        }
    }
    
    public function searchIndustry($value)
    {
        $sql = $this->select()
                    ->from(array("il" => "industries_lang"), array("id"))
                    ->where("title LIKE '%{$value}%'")
                    ->limit(1);
        $data = $this->fetchRow($sql);
        
        return (int) $data['id'];
    }
}