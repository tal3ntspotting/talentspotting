<?php

class Domain_Job_JobRepository
{
    public $gateway ;

    public function __construct(models_Jobs $gateway)
    {
        $this->gateway = $gateway;
    }

    public function getJobByID($id)
    {
        return $this->gateway->getJobById($id,true);
    }

    public function saveJob($job)
    {
        $this->gateway->save($job);
    }


}