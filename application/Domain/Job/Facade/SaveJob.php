<?php

class Domain_Job_Facade_SaveJob
{
    public function execute($job)
    {
        $repository = (new Domain_Job_JobRepository(new models_Jobs()))->saveJob($job);
    }
}