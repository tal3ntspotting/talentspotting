<?php

class Domain_Job_Facade_getJob
{
    public function execute($id)
    {
        return  (new Domain_Job_JobRepository((new models_Jobs())))->getJobByID($id);
    }

}