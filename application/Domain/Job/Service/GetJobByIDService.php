<?php

class Domain_Job_Service_GetJobByIDService
{
    public $id ;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        return (new Domain_Job_Facade_getJob())->execute($this->id);

    }

}