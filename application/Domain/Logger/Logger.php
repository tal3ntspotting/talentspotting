<?php

class Domain_Logger_Logger
{
    public $type ;

    public function __construct($type)
    {
        $this->type = $type ;
    }

    public function log(WebApp_Mail $email)
    {
        $model = new models_User ;
        $data = [];
        $data['email_type'] = $email->getEmailType();
        $data['send'] = $email->getLog();
        $recipients = $email->getRecipients();
        $mail = array_shift(array_slice($recipients, 0, 1));
        $userExistInDb = $model->getUserDataByEmail($mail);
        $id_user = 0 ;
        if(!empty($userExistInDb))
        {
            $id_user = $userExistInDb['id'];
        }
        $data['email_user'] = $mail ;
        $data['id_user'] = $id_user;
        $data['cron_file'] = $email->getCondition();
        $data['conditions'] = $email->getCondition();
        $data['content'] = serialize($email);
        $data['subject'] = $email->getSubject();
        $logger = new models_Cron;
        $logger->logCron($data);
    }
}