<?php
trait Domain_Cron_Trait_ServiceManager
{

    /**
     * @param $value
     * @param $user
     * @param $content
     * @param $transport
     * @throws Zend_Mail_Exception
     */
    public function send($value, $user, $content)
    {
        $mail = new WebApp_Mail();
        $mail->setFrom($value['email_company'], "https://talentspotting.com");
        $mail->setSubject($value['email_subject']);
        $mail->addTo($user['user_email'], $value['email_subject']);
        $mail->setBodyHtml($content);
        $mail->send($this->transport);
    }

    /**
     * @param $user
     * @param $cron
     * @param $value
     * @param $content
     * @return mixed
     */
    public function logEmailContent($user, $cron, $value, $content)
    {
        $cron['id_user'] = $user['id_user'];
        $cron['email_user'] = $user['user_email'];
        $cron['email_type'] = 37;
        $cron['cron_file'] = "adminReport.php";
        $cron['conditions'] = "Friday, 9:00";
        $cron['subject'] = $value['email_subject'];
        $cron['content'] = $content;
        $cron['date'] = date('Y-m-d H:i:s');
        $this->_cronModel->logCron($cron);
        return $cron;
    }



    public function assignData($value, $user)
    {

        $smarty = $smarty = new Smarty();
        $c = new models_Cron;
        $smarty->assign("applications", $value['applications']);
        $smarty->assign("reached", $value['user_reached']);
        $smarty->assign("bounty", $value['bountyToPay']);
        $smarty->assign("username", $user['firstname']);
        $smarty->assign('subdomain_url', $this->sitename);
        $c->updareCronTipsTable($user['id_user'], $value['id_company']);
        $resource = $smarty->fetch('eval:' . $value['email_content']);
        $company = new models_Company;
        $template = $company->getCompanyEmailTemplate($this->sitname);
        $smarty->assign('subject', $value['email_subject']);
        $smarty->assign('content', $resource);
        $content = $smarty->fetch('eval:' . $template);
        return array($resource, $content);
    }
}
?>