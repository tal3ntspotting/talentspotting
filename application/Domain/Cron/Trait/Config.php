<?php

trait Domain_Cron_Trait_Config
{
    public $sitname;
    public $transport ;
    public $_cronModel;
    public $_JobModel;
    public $_UserModel;
    public $_settings_array;


    public function getConfig()
    {
        $this->_cronModel = new models_Cron;
        $this->_JobModel = new models_Jobs ;
        $this->_UserModel = new models_User ;
        $params_productions = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', "production");
        $sitename = $params_productions->resources->Settings->sitename;
        $transport = null;
        $configuration = ['auth' => "login",
            'username' => $params_productions->resources->Settings->email_smtp->username,
            'password' => $params_productions->resources->Settings->email_smtp->password
        ];
        $transport = new Zend_Mail_Transport_Smtp($params_productions->resources->Settings->email_smtp->host, $configuration);
        $this->sitname = $sitename ;
        $this->transport = $transport;
        $this->_settings_array = $params_productions->resources->Settings->toArray();

    }
}
?>