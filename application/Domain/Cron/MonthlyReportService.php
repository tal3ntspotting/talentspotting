<?php

class Domain_Cron_MonthlyReportService implements Domain_Cron_Interface_CronInterface,
Domain_Cron_Interface_ConfigInterface
{
    use Domain_Cron_Trait_Config ;

    public function execute()
    {
        $result = $this->getItems();
        $table_data = $this->_cronModel->report();
//echo '<pre>';print_r($table_data);die;
        $today = date('Y-m-d');
        $last_month_day = date("Y-m-t", strtotime($a_date));

        if($today == $last_month_day)
        {
            foreach ($result as $key => $value)
            {

                if (!empty($value['users']))
                {
                    foreach ($value['users'] as $user)
                    {

                        list($resource, $content) = $this->assignData($table_data, $user, $value);

                        if (filter_var($user['user_email'], FILTER_VALIDATE_EMAIL))
                        {
                            if($resource == "" or $value['email_subject'] == "")
                            {
                                $cron['send'] = 0;

                                $cron = $this->logEmailContent($user, $cron, $value, $content);
                                continue;
                            }
                            $this->send($value, $user, $content);
                            $cron['send'] = 1;
                            $cron = $this->logEmailContent($user, $cron, $value, $content);
                        }


                    }
                }
            }
        }

    }

    /**
     * @return array
     */
    public function getItems()
    {
        $result = $this->_cronModel->getAllUsersByCompany(54, 2);
        return $result;
    }

    /**
     * @param $table_data
     * @param $user
     * @param $value
     * @return array
     * @throws SmartyException
     * @throws Zend_View_Helper_Partial_Exception
     */
    public function assignData($table_data, $user, $value)
    {
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $table_applicants = $partialHelper->partial('report.phtml', $table_data);


        $smarty = new Smarty();
        $smarty->assign("recommender", $user);
        $smarty->assign("sitename", $this->sitename);
        $smarty->assign("settings", ['sitename' => $this->sitename]);
        $smarty->assign("company", ['title' => $value['title_company']]);
        $smarty->assign("table", $table_applicants);
        $smarty->assign('subdomain_url', $this->sitename);
        $resource = $smarty->fetch('eval:' . $value['email_content']);


        $company = new models_Company;
        $template = $company->getCompanyEmailTemplate($this->sitename);
        $smarty->assign('subject', $value['email_subject']);
        $smarty->assign('content', $resource);
        $content = $smarty->fetch('eval:' . $template);
        return array($resource, $content);
    }

    /**
     * @param $user
     * @param $cron
     * @param $value
     * @param $content
     * @return mixed
     */
    public function logEmailContent($user, $cron, $value, $content)
    {
        $cron['id_user'] = $user['id_user'];
        $cron['email_user'] = $user['user_email'];
        $cron['email_type'] = 54;
        $cron['cron_file'] = "your-monthly-talentspotting-report.php";
        $cron['conditions'] = "monthly, last working day from each month at 10:00";
        $cron['subject'] = $value['email_subject'];
        $cron['content'] = $content;
        $cron['date'] = date('Y-m-d H:i:s');
        $this->_cronModel->logCron($cron);
        return $cron;
    }

    /**
     * @param $value
     * @param $user
     * @param $content
     * @throws Zend_Mail_Exception
     */
    public function send($value, $user, $content)
    {
        $mail = new WebApp_Mail();
        $mail->setFrom($value['email_company'], $value['email_company']);
        $mail->setSubject($value['email_subject']);
        $mail->addTo($user['user_email'], $value['email_subject']);
        $mail->setBodyHtml($content);
        $mail->send($this->transport);
    }
}

?>