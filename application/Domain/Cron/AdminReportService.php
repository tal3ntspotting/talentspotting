<?php

class Domain_Cron_AdminReportService implements
    Domain_Cron_Interface_CronInterface,
    Domain_Cron_Interface_ConfigInterface
{
    use Domain_Cron_Trait_Config;
    use Domain_Cron_Trait_ServiceManager;


    public function __construct()
    {
        $this->getConfig();
    }

    public function execute()
    {
        $cronGateway = new models_Cron();
        $result = (new Domain_Cron_CronRepository($cronGateway))->getAdminReport();
        foreach ($result as $key => $value)
        {
            if (!empty($value['users']))
            {
                foreach ($value['users'] as $user)
                {
                    list($resource, $content) = $this->assignData($value, $user);
                    if (filter_var($user['user_email'], FILTER_VALIDATE_EMAIL))
                    {
                        if($resource == "" or $value['email_subject'] == "")
                        {
                            $cron['send'] = 0;
                            $cron = $this->logEmailContent($user, $cron, $value, $content);
                            continue;
                        }
                        $this->send($value, $user, $content);
                        $cron['send'] = 1;
                        $cron = $this->logEmailContent($user, $cron, $value, $content);
                    }
                }
            }
        }

    }


}
?>