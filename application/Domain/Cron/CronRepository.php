<?php

class Domain_Cron_CronRepository
{
    public $gateway ;

    public function __construct(models_Cron $gateway)
    {
        $this->gateway = $gateway;
    }

    public function getAdminReport()
    {
        return $this->gateway->adminReport();
    }
}

?>