<?php
class Domain_Cron_InviteColleeaguesOnTalentSpottingService implements 
                    Domain_Cron_Interface_ConfigInterface,
                    Domain_Cron_Interface_CronInterface
{
    use Domain_Cron_Trait_Config;
    
    public function execute()
    {
        // TODO: Implement execute() method.

        $result = $this->_cronModel->getAllUsersByCompany(48, 2);
        $now = date('Y-m-d H:i:s');
        $t1 = StrToTime($now);

        foreach ($result as $key => $value) {
            if (!empty($value['users'])) {
                foreach ($value['users'] as $user) {
                    $t2 = StrToTime($user['first_login']);
                    $diff = $t1 - $t2;
                    $hours = $diff / ( 60 * 60 );
                    if ($hours > 96 && $hours <= 120) {
                        list($resource, $content) = $this->assignData($user, $value);
                        if (filter_var($user['user_email'], FILTER_VALIDATE_EMAIL))
                        {
                            if($resource == "" or $value['email_subject'] == "")
                            {
                                $cron['send'] = 0;

                                $cron = $this->logEmailContent($user, $cron, $value, $content);
                                continue;
                            }
                            $this->send($value, $user, $content);
                            $cron['send'] = 1;
                            $cron = $this->logEmailContent($user, $cron, $value, $content);
                        }
                    }
                }
            }
        }


    }

    /**
     * @param $user
     * @param $value
     * @return array
     * @throws SmartyException
     */
    public function assignData($user, $value)
    {
        $smarty = new Smarty();
        $smarty->assign("recommender", $user);
        $smarty->assign("sitename", $this->sitename);
        $smarty->assign("settings", ['sitename' => $this->sitename]);
        $smarty->assign("company", ['title' => $value['title_company']]);
        $smarty->assign('subdomain_url', $this->sitename);
        $resource = $smarty->fetch('eval:' . $value['email_content']);
        $company = new models_Company;
        $template = $company->getCompanyEmailTemplate($this->sitename);
        $smarty->assign('subject', $value['email_subject']);
        $smarty->assign('content', $resource);
        $content = $smarty->fetch('eval:' . $template);
        return array($resource, $content);
    }

    /**
     * @param $user
     * @param $cron
     * @param $value
     * @param $content
     * @return mixed
     */
    public function logEmailContent($user, $cron, $value, $content)
    {
        $cron['id_user'] = $user['id_user'];
        $cron['email_user'] = $user['user_email'];
        $cron['email_type'] = 48;
        $cron['cron_file'] = "invite-your-colleagues-on-talentspotting.php";
        $cron['conditions'] = "first login + 4 days";
        $cron['subject'] = $value['email_subject'];
        $cron['content'] = $content;
        $cron['date'] = date('Y-m-d H:i:s');
        $this->_cronModel->logCron($cron);
        return $cron;
    }

    /**
     * @param $value
     * @param $user
     * @param $content
     * @throws Zend_Mail_Exception
     */
    public function send($value, $user, $content)
    {
        $mail = new WebApp_Mail();
        $mail->setFrom($value['email_company'], $value['email_company']);
        $mail->setSubject($value['email_subject']);
        $mail->addTo($user['user_email'], $value['email_subject']);
        $mail->setBodyHtml($content);
        $mail->send($this->transport);
    }

}
?>