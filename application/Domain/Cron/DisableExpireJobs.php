<?php

class Domain_Cron_DisableExpireJobs implements
    Domain_Cron_Interface_CronInterface,
    Domain_Cron_Interface_ConfigInterface
{
    use Domain_Cron_Trait_Config;

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_cronModel->disableExpireJobs();
        $this->logEmailContent();
    }

    /**
     * @param $cron
     */
    public function logEmailContent()
    {
        $cron = [];
        $cron['id_user'] = 0;
        $cron['email_user'] = "";
        $cron['email_type'] = 0;
        $cron['cron_file'] = "expire_jobs.php";
        $cron['conditions'] = "expire jobs";
        $cron['subject'] = "WITHOUT SUBJECT";
        $cron['content'] = "WITHOUT CONTENT";
        $cron['date'] = date('Y-m-d H:i:s');
        $cron['send'] = 1;
        $this->_cronModel->logCron($cron);
    }

}
?>