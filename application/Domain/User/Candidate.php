<?php


class Domain_User_Candidate extends Domain_User_UserAbstract
    implements Domain_User_Interfaces_UserInterface
{

    public function getJobManagement(models_Jobs $model, $filters = array(), $limit = null, $offset = null)
    {
        throw new Exception('You Dont have the right to see this page',403);
    }


    public function sendWelcomeEmail()
    {
        $type = 1 ;
        $email = $this->getEmailByType($type);
        $generatedPassword = $this->modelUser->generateStrongPassword();
        $password = ($this->getPassword() == false) ? $generatedPassword : $this->getPassword();
        if($this->getPassword() == false)
        {
            $this->modelUser->regenerateUserPassword($password,$this->_auth['id']);
        }
        $link = $this->_settings['sitename'] . '/activate?token=' . sha1($this->_auth['email']);


        $data['firstname'] = $this->_auth['firstname'];
        $data['role'] = "candidate";
        $data['email'] = $this->_auth['email'];
        $data['password'] = $password ;
        $data['sitename'] = $this->_settings['sitename'] ;
        $data['link'] = $link;
        $data['user'] = $this->_auth ;
        $data["subdomain_url"] = $this->_settings['sitename'];
        $data["company"] = $this->modelCompany->getCompanyById($data['id_company']);
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);


        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }
    

//    public function sendCandidateStatusChange($id_user)
//    {
//        $type = 40 ;
//        $email = $this->getEmailByType($type);
//        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
//        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
//        $this->sendMail($this->_auth['email'],
//            $emailSubject,
//            $emailBody,
//            $type,
//            __METHOD__
//        );
//    }

    public function similarJobSkill()
    {
        $type = 95 ;
        $email = $this->getEmailByType($type);

        $today = date("d.m.Y",strtotime("today"));
        $last_week = date("d.m.Y", strtotime("last week"));
        $period = "$last_week - $today";

        $start = date("Y-m-d", strtotime("last week"));
        $end = date("Y-m-d", strtotime("today"));
        $mySkills = $this->modelUser->getUserSkills($this->_auth['id']);
        $neededSkills = [] ;
        foreach ($mySkills as $key => $skill)
        {
            $neededSkills[] = $skill['id'];
        }
        if(empty($neededSkills)) return ;
        $jobs = $this->modelJob->getJobsWithSimilarCandidateBySkills($neededSkills,$start,$end);
        if(empty($jobs)) return ;
        $all_job_count = count($jobs);
        $job_data = array_slice($jobs,0,10);
        foreach ($job_data as $key => &$job)
        {
            $grouped_Skills = [] ;
            $current_job_skills = $this->modelJob->_getJobSkills($job['id']);
            foreach ($current_job_skills as $key => $skl) {
                $grouped_Skills[] = $skl;
            }
            $job['skills'] = $grouped_Skills;
        }

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $similar_jobs = $partialHelper->partial('similar_jobs_skill.phtml', ['data' => $job_data,
            'total_count' => $all_job_count]);


        $data["company"] = $this->modelUser->getCompanyById($this->_auth['id_company'])['title'];
        $data["firstname"] = $this->_auth['firstname'];
        $data["period"] = $period;
        $data["similar_jobs"] = $similar_jobs;

        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendCandidateOfferStatusChange($application)
    {
        $type = 69 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendCandidateUnfitStatusChange($application)
    {
        $type = 24 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }
}