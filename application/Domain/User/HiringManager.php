<?php


class Domain_User_HiringManager extends Domain_User_UserAbstract
    implements Domain_User_Interfaces_UserInterface
{

    public function getJobManagement(models_Jobs $model, $filters = array(), $limit = null, $offset = null)
    {
        return $model->atsgetJobsHm($filters, $limit, $offset);
    }

}