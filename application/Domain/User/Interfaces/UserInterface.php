<?php

interface Domain_User_Interfaces_UserInterface
{
    public function getJobManagement(models_Jobs $model,$filters = array(), $limit = null, $offset = null);

    public function sendWelcomeEmail();
}