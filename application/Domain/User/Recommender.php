<?php


class Domain_User_Recommender extends Domain_User_UserAbstract
    implements Domain_User_Interfaces_UserInterface
{

    public function getJobManagement(models_Jobs $model, $filters = array(), $limit = null, $offset = null)
    {
        throw new Exception('You Dont have the right to see this page',403);
    }


    public function sendWelcomeEmail()
    {
        $type = 27 ;
        $email = $this->getEmailByType($type);
        $generatedPassword = $this->modelUser->generateStrongPassword();
        $password = ($this->getPassword() == false) ? $generatedPassword : $this->getPassword();
        if($this->getPassword() == false)
        {
            $this->modelUser->regenerateUserPassword($password,$this->_auth['id']);
        }
        $data['firstname'] = $this->_auth['firstname'];
        $data['role'] = "recommender";
        $data['email'] = $this->_auth['email'];
        $data['password'] = $password ;
        $data['sitename'] = $this->_settings['sitename'] ;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function shareJob()
    {
        $type = 90 ;
        $email = $this->getEmailByType($type);
        $data['firstname'] = $this->_auth['firstname'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function feedback()
    {
        $type = 91 ;
        $email = $this->getEmailByType($type);
        $data['firstname'] = $this->_auth['firstname'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function activeJobs()
    {
        $type = 92 ;
        $email = $this->getEmailByType($type);
        $today = date("d.m.Y",strtotime("today"));
        $last_week = date("d.m.Y", strtotime("last week"));
        $period = "$last_week - $today";

        $start = date("Y-m-d", strtotime("last week"));
        $end = date("Y-m-d", strtotime("today"));

        $active_jobs = $this->modelJob->getActiveJobsReportByDate($start,$end);
        if(empty($active_jobs)) return ;
        $total_jobs = count($active_jobs);
        $total_bounty = 0 ;
        $total_vacandies = 0 ;
        $total_applicants = 0 ;
        foreach($active_jobs as $index => $job)
        {
            $total_bounty += $job['bounty'];
            $total_vacandies += $job['vacancies'];
            $total_applicants += $job['applicants'];
        }
        $data['jobs'] = $active_jobs ;
        $data['total_jobs'] = $total_jobs;
        $data['total_bounty'] = $total_bounty;

        $count = count($data['jobs']) ;
        if($count > 10)
        {
            $data['jobs'] = array_slice($data['jobs'],0,1);
        }

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $active_jobs_table = $partialHelper->partial('active_jobs.phtml', ['data' => $data]);


        $data['firstname'] = $this->_auth['firstname'];
        $data['period'] = $period ;
        $data['active_jobs'] = $active_jobs_table;
        $data['total_active_jobs'] = $total_jobs;
        $data['vacancies'] = $total_vacandies;
        $data['candidates'] = $total_applicants;
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];

        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );

    }


    public function report()
    {
        $type = 93 ;
        $email = $this->getEmailByType($type);
        $first_day =  date('01',strtotime('last month')) ;
        $last_day =  date('t',strtotime('last month'));
        $month_name = date('F',strtotime('last month'));
        $month_digit = date('m',strtotime('last month'));
        $year = date('Y',strtotime('last month'));
        $report_period = $first_day." - ".$last_day."th of ".$month_name . " ". $year ." .";

        $start = "$year-$month_digit-$first_day";
        $end = "$year-$month_digit-$last_day";

        $data[0]['count']  = $this->modelUser->CountSharedJobsByUserId($start,$end,$this->_auth['id']) ;
        $data[0]['kpi']  = "Shares" ;
        $data[0]['definition']  = "Job shares in selected period using Facebook, LinkedIn, CV upload or email" ;

        $data[1]['count']  = $this->modelUser->countJobsViewsUserByUserId($start,$end,$this->_auth['id']) ;
        $data[1]['kpi']  = "Views" ;
        $data[1]['definition']  = "Job views in selected period" ;

        $data[2]['count']  = $this->modelUser->countJobApplicationsByUserId($start,$end,$this->_auth['id']) ;
        $data[2]['kpi']  = "Applications" ;
        $data[2]['definition']  = "Job applications in selected period" ;

        $data[3]['count']  = $this->modelUser->countViewersByPeriodUserId($start,$end,$this->_auth['id']) ;
        $data[3]['kpi']  = "Potential candidates" ;
        $data[3]['definition']  = "people that viewed at least one job in selected period" ;

        $data[4]['count']  = $this->modelUser->countJobApplicationsByUserIdPeriod($start,$end,$this->_auth['id']) ;
        $data[4]['kpi']  = "Applicants" ;
        $data[4]['definition']  = "people that applied to at least one job in selected period" ;

        $data[5]['count']  = $this->modelUser->countHiredApplicationsByUserId($start,$end,$this->_auth['id']) ;
        $data[5]['kpi']  = "Hired applicants" ;
        $data[5]['definition']  = "people that applied to at least one job in selected period" ;

//        $data[0]['count'] = $this->modelUser->countActiveEmployeesByPeriodUserId($start,$end,$this->_auth['id']);
//        $data[0]['kpi'] = "Active employees";
//        $data[0]['definition'] = "employees with valid company email";
//
//        $data[1]['count'] = $this->modelUser->countEngagementEmployeeByPeriodUserId($start,$end,$this->_auth['id']);
//        $data[1]['kpi'] = "Engaged employees";
//        $data[1]['definition'] = "employees that accessed Talentspotting at least once sice created";
//
//        $data[2]['count']  = $this->modelUser->countEmployeesEvanghelistsByPeriodUserId($start,$end,$this->_auth['id']) ;
//        $data[2]['kpi']  = "Promoters" ;
//        $data[2]['definition']  = "employees that shared a job at least once in $month_name" ;
//
//        $data[3]['count']  = $this->modelUser->countActiveJobsByPeriodUserId($start,$end,$this->_auth['id']) ;
//        $data[3]['kpi']  = "Available jobs" ;
//        $data[3]['definition']  = "jobs available to apply to in $month_name" ;
//
//        $data[4]['count']  = $this->modelUser->CountSharedJobsByPeriodUserId($start,$end,$this->_auth['id']) ;
//        $data[4]['kpi']  = "Jobs shared" ;
//        $data[4]['definition']  = "jobs shared to friends in $month_name" ;
//
//        $data[5]['count']  = $this->modelUser->countJobApplicationsByPeriodUserId($start,$end,$this->_auth['id']) ;
//        $data[5]['kpi']  = "Jobs with applicants" ;
//        $data[5]['definition']  = "jobs with at least one applicant in $month_name" ;
//
//        $data[6]['count']  = $this->modelUser->countJobsViewsUserIdUserId($start,$end,$this->_auth['id']) ;
//        $data[6]['kpi']  = "Unique job viewers" ;
//        $data[6]['definition']  = "people that viewed at least one job in $month_name" ;
//
//        $data[7]['count']  = $this->modelUser->countUniqueCandidateUserId($start,$end,$this->_auth['id']) ;
//        $data[7]['kpi']  = "unique applicants" ;
//        $data[7]['definition']  = "people that applied to at least one job in $month_name" ;
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $report = $partialHelper->partial('monthly_report.phtml', ['data' => $data]);
        $data['firstname'] = $this->_auth['firstname'];
        $data['report'] = $report;
        $data['report_periode'] = $report_period;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

}