<?php


class Domain_User_UserAbstract implements Domain_User_Interfaces_UserInterface
{
    protected $_auth ;
    protected $_user ;
    public $smarty = null ;
    public $mailClient = null ;
    public $transport = null ;
    public $id_lang;
    public $modelCompany ;
    public $modelContent ;
    public $modelUser ;
    public $modelJob ;
    public $modelCampaign ;
    public $_settings = array();

    public function __construct($auth)
    {
        $this->_auth = $auth;
        $this->modelCompany = new models_Company;
        $this->modelContent = new models_Content;
        $this->modelUser = new models_User;
        $this->modelJob = new models_Jobs;
        $this->modelCampaign = new models_Campaign ;

        if (is_object(Zend_Controller_Front::getInstance()->getParam('bootstrap'))) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            $this->_settings = $bootstrap['resources']['Settings'];
        }

        $languages = Zend_Registry::get("_LANG_DETAILS");
        $id_lang = null ;
        $this->id_lang = (null !== $id_lang) ? $id_lang : $languages[APPLICATION_LOCALE];
        $this->smarty = new Smarty();
        $this->transport = null;

        $configuration = array('auth' => $this->_settings['email_smtp']['auth'],
            'username' => $this->_settings['email_smtp']['username'],
            'password' => $this->_settings['email_smtp']['password']
        );

        $this->transport = new Zend_Mail_Transport_Smtp(
            $this->_settings['email_smtp']['host'],
            $configuration);

    }

    public function isLogged()
    {
        return $this->_auth->hasIdentity() ? true : false;
    }

    public function me()
    {
        return $this->_auth->getIdentity() ;
    }

    public function getJobManagement(models_Jobs $model,$filters = array(), $limit = null, $offset = null)
    {
        $jobs = $model->getAllJobsObject($filters = array(), $limit = null, $offset = null);
        return $model->fetchAll($jobs);
    }

    public function sendWelcomeEmail()
    {
        // TODO: Implement sendWelcomeEmail() method.
    }

    public function getEmailTemplateContainerByIdCompany($id_company)
    {
        try{
            return $this->modelCompany->getEmailTemplateContainerByIdCompany($id_company);
        }  catch (Exception $e)
        {
            echo $e->getMessage();
        }
    }

    public function getEmailByType($type)
    {
        return $this->modelContent->getEmailByType($type);
    }


    public function assign($data)
    {
        $data['sitename'] = $this->_settings['sitename'];
        $data['subdomain_url'] = $this->_settings['sitename'];
        $data['settings'] = $this->_settings;
        $data['bg_color'] = $this->getBrandColorButton();
        foreach($data as $key => $value)
        {
            $this->smarty->assign($key,$value);
        }
        return $this;
    }

    public function getBrandColorButton()
    {
        $class = "branding-brand-button-background-color";
        return $this->modelContent->getBrandColorButton($class)." !important";

    }


    public function sendMail($to,$emailSubject,$emailBody,$type,$condition)
    {

        $id_company = $this->modelUser->getUserDataByEmail($to)['id_company'];
        $this->assign(
            [
                'subject' => $emailSubject,
                'content' => $emailBody
            ]
        );
        $container = $this->getEmailTemplateContainerByIdCompany($id_company);
        $content = $this->smarty->fetch('eval:'.$container);
        $company = $this->modelCompany->getCompanyById($id_company);
        $from = $company['email'];

        $logger = new Domain_Logger_Logger($type);
        $this->mailClient = new WebApp_Mail();
        $transport = null ;
        $configuration = array('auth' => $this->_settings['email_smtp']['auth'],
            'username' => $this->_settings['email_smtp']['username'],
            'password' => $this->_settings['email_smtp']['password']
        );
        $transport = new Zend_Mail_Transport_Smtp(
            $this->_settings['email_smtp']['host'],
            $configuration);

        $this->mailClient->addTo($to);
        $this->mailClient->setFrom($from);
        $this->mailClient->setSubject($emailSubject);
        $this->mailClient->setBodyHtml($content,"UTF-8",Zend_Mime::TYPE_HTML);
        $this->mailClient->addEmailType($type);
        $this->mailClient->setCondition($condition);

//        $this->mailClient->setLog(0);
//        $logger->log($this->mailClient);
//        return 0;
//        die;

        if (filter_var($to, FILTER_VALIDATE_EMAIL))
        {
            if($emailSubject == "" or $content == "" or $emailBody == "")
            {
                $this->mailClient->setLog(0);
                $logger->log($this->mailClient);
            }
            else {
                $this->mailClient->send($transport);
                $this->mailClient->setLog(1);
                $logger->log($this->mailClient);
            }
        }
    }

    public function setPassword($password)
    {
        $this->_auth['_passowrd'] = $password;
    }

    public function getPassword()
    {
        if(isset($this->_auth['_passowrd'])) return $this->_auth['_passowrd'];

        return false;
    }


    public function JobApplicationEmail($job)
    {
        $type = 25 ;
        $email = $this->getEmailByType($type);
        $user = $this->_auth ;
        $data["user"] = $user;
        $data["job"] = $job;
        $data["settings"] = $this->_settings;
        $data['subdomain_url'] = $this->_settings['sitename'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function changeStatus($id_Capplication,$send_email=0 , $id_status)
    {
        $type = 39 ;
        $email = $this->getEmailByType($type);

        $sql = $this->modelJob->select()->from(array("ja" => "job_applications"))->where("id = ?", (int) $id_Capplication);
        $application = $this->modelJob->fetchRow($sql);

        $applicant = $this->modelUser->getUserById($application['id_user']);
        $recommender = $this->modelUser->getUserById($applicant['id_parent']);
        $data["candidate"] = $applicant;
        $data["recommender"] = $recommender;
        $data["job"] = $this->modelJob->getJobById($application['id_job']);
        $this->assign($data);

        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);

        // add condition if status change note is null
//        if (!empty($recommender)) {
//            $this->sendMail($this->_auth['email'],
//                $emailSubject,
//                $emailBody,
//                $type,
//                __METHOD__
//            );
//        }
        if(!$send_email) return 0 ;
        $userObject = Domain_User_UserFactory::createInstance($application['id_user']);
        if($application['status'] == 9)
        {
            // offer email
            $userObject->sendCandidateOfferStatusChange($application);
        } elseif(in_array($application['status_reason'],[2,4,3,11,16,21,28]))
        {
            // unfit email
            $userObject->sendCandidateUnfitStatusChange($application);

        }
        elseif($application['status'] == 5)
        {
            $userObject->sendCandidateHiredStatusChange($application);
        }
        else{
            $userObject->sendCandidateStatusChange($application,$id_status);
        }
//        $recruiters = $this->modelJob->getRecruitersByJobId($application['id_job']);
//        foreach($recruiters as $key => $recruiter)
//        {
//            $data['user'] = $recruiters;
//            $this->assign($data);
//            $to = $recruiter['email'];
//            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
//            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
//            $this->sendMail($to,
//                $emailSubject,
//                $emailBody,
//                $type,
//                __METHOD__
//            );
//
//        }

//        $admins = $this->modelUser->getAllAdminsCompany();
//        foreach ($admins as $key => $admin)
//        {
//            $data['user'] = $admin;
//            $this->assign($data);
//            $to = $admin['email'];
//            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
//            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
//            $this->sendMail($to,
//                $emailSubject,
//                $emailBody,
//                $type,
//                __METHOD__
//            );
//        }

    }


    public function recommendEmail($job,$userData,$name,$userEmail)
    {
        $type = 28;
        $email = $this->getEmailByType($type);
        $data['user'] = $this->_auth;
        $data['job'] = $job;
        $data['data'] = $userData;
        $data['name'] = $name;
        $data['email'] = $email;
        $data['url'] = WebApp_Tools::createJobSlug($job);
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($userEmail,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );

    }

    public function uploadUserCvEmail($data,$to)
    {
        $type = 41;
        $email = $this->getEmailByType($type);
        $data['email'] = $email;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function UploadUserCvEmailForCandidate($data,$to)
    {
        $type = 42;
        $email = $this->getEmailByType($type);
        $data['email'] = $email;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function newCampaignEmail($campaign,$jobs,$gam_action)
    {

        $j_array = [];
        for($i = 0 ; $i < count($jobs);$i++)
        {
            $j_array[] = $jobs[$i]['title']." ".($i+1);
        }
        $jobs = str_replace('_',' ',implode($j_array,' ,'));
        $rules = str_replace('_',' ',implode($gam_action,' ,'));
        $type = 31;
        $email = $this->getEmailByType($type);
        $data['campaign'] = $campaign['title'];
        $data['bounty'] = $campaign['bounty'];
        $data['start'] = $campaign['start'];
        $data['end'] = $campaign['end'];
        $data['jobs'] = $jobs;
        $data['gamification'] = $gam_action;
        $data['rules'] = $rules;
        $users = $this->modelUser->getRecruitersByCompany();
        foreach ($users as $key => $user) {
            $data['username'] = $user['firstname'];
            $to = $user['email'];
            $this->assign($data);
            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
            $this->sendMail($to,
                $emailSubject,
                $emailBody,
                $type,
                __METHOD__
            );
        }

    }

    public function newCampaignEmailForAdmin($campaign,$jobs,$gam_action)
    {

        $j_array = [];
        for($i = 0 ; $i < count($jobs);$i++)
        {
            $j_array[] = $jobs[$i]['title']." ".($i+1);
        }
        $jobs = str_replace('_',' ',implode($j_array,' ,'));
        $rules = str_replace('_',' ',implode($gam_action,' ,'));
        $type = 65 ;
        $email = $this->getEmailByType($type);
        $data['campaign'] = $campaign['title'];
        $data['bounty'] = $campaign['bounty'];
        $data['start'] = $campaign['start'];
        $data['end'] = $campaign['end'];
        $data['jobs'] = $jobs;
        $data['gamification'] = $gam_action;
        $data['rules'] = $rules;
        $users = $this->modelUser->getAllAdminsCompany();
        foreach ($users as $key => $user) {
            $data['username'] = $user['firstname'];
            $to = $user['email'];
            $this->assign($data);
            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
            $this->sendMail($to,
                $emailSubject,
                $emailBody,
                $type,
                __METHOD__
            );
        }

    }

    public function sendSuccesfulEmailToRecommenderForHiredCandidate($recommender,$candidate,$job,$company_name)
    {
        $type = 32;
        $email = $this->getEmailByType($type);
        $user = $this->_auth;
        $data['company'] = $company_name;
        $data["username"] = $recommender['firstname'];
        $data["candidate"] = $candidate['firstname'] . " " . $candidate['lastname'];
        $data["job"] = $job;
        $data["user"] = $user;

        $job_bounty = '<p>
                You are now entitled to collect the bounty - {$job_bounty} , considering  communicated due date. Check the Bounties section to see when it is due.
            </p>';
        $job_bounty = str_replace('{$job_bounty}',$job['bounty_title'],$job_bounty);

        $data['job_bounty'] = "";
        if($job['bounty_title'])
        {
            $data['job_bounty'] = $job_bounty;
        }


        $this->assign($data);
        $to = $recommender['email'];
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function notifyRecommenderJobApplicationEmail($data)
    {
        if(!$data['id_recommender']) return false  ;
        $recommender = $this->modelUser->getUserById($data['id_recommender']);
        $applicant = $this->modelUser->getUserById($data['id_user']);
        $job = $this->modelJob->getJobById($data['id_job']) ;
        $type = 66;
        $email = $this->getEmailByType($type);
        $data['recommender'] = $recommender;
        $data['candidate'] = $applicant ;
        $data['job'] = $job ;
        $data['id'] = base64_encode($applicant['id']) ;
        $this->assign($data);
        $to = $recommender['email'];
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );

    }

    public function notifyRecruiterJobApplicationEmail($data)
    {

        $type = 26;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($data['id_job']) ;
        $recruiters_job = $this->modelJob->getRecruitersByJobId($data['id_job']);
        if(empty($recruiters_job)) return false ;
        $candidate = $this->modelUser->getUserById($data['id_user']); // candidate
        foreach ($recruiters_job as $key => $user)
        {
            $recruiter = $this->modelUser->getUserById($user['id_recruiter']);
            $data['job'] = $job ;
            $data['candidate'] = $candidate  ;
            $data['user'] = $recruiter;
            $data['application'] = $data;
            $this->assign($data);
            $to = $recruiter['email'];
            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
            $this->sendMail($to,
                $emailSubject,
                $emailBody,
                $type,
                __METHOD__
            );

        }

    }

    public function notifyAdminJobApplicationEmail($data)
    {

        $type = 26;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($data['id_job']) ;
        $admins = $this->modelUser->getAllAdminsCompany();
        if(empty($recruiters_job)) return false ;
        $candidate = $this->modelUser->getUserById($data['id_user']); // candidate
        foreach ($admins as $key => $user)
        {
            $recruiter = $this->modelUser->getUserById($user['id_recruiter']);
            $data['job'] = $job ;
            $data['candidate'] = $candidate  ;
            $data['user'] = $recruiters_job ;
            $data['application'] = $data;
            $this->assign($data);
            $to = $recruiter['email'];
            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
            $this->sendMail($to,
                $emailSubject,
                $emailBody,
                $type,
                __METHOD__
            );

        }

    }

    public function notifyAdminsForSuccesfullHiring($admin,$recommender,$candidate,$job)
    {
        if($candidate['type'] != 9) return 0 ;
        $type = 68;
        $email = $this->getEmailByType($type);
        $to = $admin['email'];
        $data['recommender'] = $recommender ;
        $data['candidate'] = $candidate ;
        $data['job'] = $job ;
        $data['firstname'] = $this->_auth['firstname'] ;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );

    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->_settings = $settings;
        $this->modelCompany->setSettings($settings);
    }


    public function sendReminder($message,$job_title,$subject)
    {
        $type = 94;
        $email = $this->getEmailByType($type);
        $to = $this->_auth['email'];
        $data['message'] = $message ;
        $data['job'] = $job_title ;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        if($subject != "")
        {
            $emailSubject = $subject;
        }
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }


    public function sendCandidateStatusChange($application,$id_status)
    {

        $type = 40 ;
        $email = $this->getEmailByType($type);
        $data["job"] = $this->modelJob->getJobById($application['id_job'])['title'];
        $data["firstname"] = $this->_auth['firstname'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
         if($this->modelContent->getPublicStatusByStatusIdFromApplicationHistory($application,$id_status) == true)
         {
             $this->sendMail($this->_auth['email'],
                 $emailSubject,
                 $emailBody,
                 $type,
                 __METHOD__
             );
         }
    }

    public function sendCandidateStatusChangeCollect($application,$status)
    {

        $type = 40 ;
        $email = $this->getEmailByType($type);
        $data["job"] = $this->modelJob->getJobById($application['id_job'])['title'];
        $data["firstname"] = $this->_auth['firstname'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $collection = [] ;
        if($this->modelContent->getPublicStatusByStatusIdFromApplicationHistory($application,$status) == true)
        {
            $data['to'] = $this->_auth['email'];
            $data['subject'] = $emailSubject;
            $data['body'] = $emailBody;
            $data['email_type'] = $type;
            $data['role'] = $this->_settings['user_types'][$this->_auth['type']];
            $data['id_email'] = $email['id_email'];
            array_push($collection,$data);
//            $this->sendMail($this->_auth['email'],
//                $emailSubject,
//                $emailBody,
//                $type,
//                __METHOD__
//            );
        }
        return $collection;
    }

    public function sendCandidateOfferStatusChange($application)
    {
        $type = 69 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendCandidateHiredStatusChange($application)
    {
        $type = 23 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendCandidateHiredStatusChangeCollect($application)
    {
        $type = 23 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $return[0]['to'] = $this->_auth['email'];
        $return[0]['subject'] = $emailSubject;
        $return[0]['body'] = $emailBody;
        $return[0]['email_type'] = $type;
        $return[0]['role'] = $this->_settings['user_types'][$this->_auth['type']];
        $return[0]['id_email'] = $email['id_email'];

        $type = 32;
        $email = $this->getEmailByType($type);
        $user = $this->_auth;
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $data["username"] = $this->modelUser->getUserById($application['id_recommender'])['firstname'];
        $data["candidate"] = $this->modelUser->getUserById($application['id_user'])['firstname'] . " " . $this->modelUser->getUserById($application['id_user'])['lastname'];
        $data["job"] = $job;
        $data["user"] = $user;
        $this->assign($data);
        $to = $this->modelUser->getUserById($application['id_recommender'])['email'];
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $return[1]['to'] = $to;
        $return[1]['subject'] = $emailSubject;
        $return[1]['body'] = $emailBody;
        $return[1]['email_type'] = $type;
        $return[1]['role'] = $this->_settings['user_types'][$this->modelUser->getUserById($application['id_recommender'])['type']];
        $return[1]['id_email'] = $email['id_email'];


        $type = 68;
        $email = $this->getEmailByType($type);
        $to = "all admin companie";
        $data['recommender'] = $this->modelUser->getUserById($application['id_recommender']) ;
        $data['candidate'] = $this->modelUser->getUserById($application['id_user']) ;
        $data['job'] = $this->modelJob->getJobById($application['id_job']) ;
        $data['firstname'] = $this->_auth['firstname'] ;
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);

        $return[2]['to'] = $to;
        $return[2]['subject'] = $emailSubject;
        $return[2]['body'] = $emailBody;
        $return[2]['email_type'] = $type;
        $return[2]['role'] = $this->_settings['user_types'][2];
        $return[2]['id_email'] = $email['id_email'];

        return $return;
    }

    public function sendCandidateOfferStatusChangeCollect($application)
    {
        $type = 69 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $collection = [] ;
        $data['to'] = $this->_auth['email'];
        $data['subject'] = $emailSubject;
        $data['body'] = $emailBody;
        $data['email_type'] = $type;
        $data['role'] = $this->_settings['user_types'][$this->_auth['type']];
        $data['id_email'] = $email['id_email'];
//        $this->sendMail($this->_auth['email'],
//            $emailSubject,
//            $emailBody,
//            $type,
//            __METHOD__
//        );
        array_push($collection,$data);
        return $collection;
    }


    public function sendCandidateUnfitStatusChange($application)
    {
        $type = 24 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendCandidateUnfitStatusChangeCollect($application)
    {
        $type = 24 ;
        $email = $this->getEmailByType($type);
        $job = $this->modelJob->getJobById($application['id_job']);
        $data['firstname'] = $this->_auth['firstname'];
        $data['job'] = $job['title'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $collect = [] ;
        $data['to'] = $this->_auth['email'];
        $data['subject'] = $emailSubject;
        $data['body'] = $emailBody;
        $data['email_type'] = $type;
        $data['role'] = $this->_settings['user_types'][$this->_auth['type']];
        $data['id_email'] = $email['id_email'];
        array_push($collect,$data);
        return $collect;
//        $this->sendMail($this->_auth['email'],
//            $emailSubject,
//            $emailBody,
//            $type,
//            __METHOD__
//        );
    }

    public function changeStatusCollect($id_Capplication,$status,$status_reason)
    {
        $type = 39 ;
        $email = $this->getEmailByType($type);

        $sql = $this->modelJob->select()->from(array("ja" => "job_applications"))->where("id = ?", (int) $id_Capplication);
        $application = $this->modelJob->fetchRow($sql);
        if(empty($application)) return [];

        $applicant = $this->modelUser->getUserById($application['id_user']);
        $recommender = $this->modelUser->getUserById($applicant['id_parent']);
        $data["candidate"] = $applicant;
        $data["recommender"] = $recommender;
        $data["job"] = $this->modelJob->getJobById($application['id_job']);
        $this->assign($data);

        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);

        // add condition if status change note is null
//        if (!empty($recommender)) {
//            $this->sendMail($this->_auth['email'],
//                $emailSubject,
//                $emailBody,
//                $type,
//                __METHOD__
//            );
//        }
        $return = [] ;
        $userObject = Domain_User_UserFactory::createInstance($application['id_user']);
        if($status == 9)
        {
            // offer email
            $return = $userObject->sendCandidateOfferStatusChangeCollect($application);
        } elseif(in_array($status_reason,[2,4,3,11,16,21,28]))
        {
            // unfit email
            $return = $userObject->sendCandidateUnfitStatusChangeCollect($application);

        }
        elseif($status == 5)
        {
            $return = $userObject->sendCandidateHiredStatusChangeCollect($application);
        }
        else{
            $return = $userObject->sendCandidateStatusChangeCollect($application,$status);
        }

        return $return;

    }

    public function rememberToJoin()
    {
        $type = 96 ;
        $email = $this->getEmailByType($type);
        $password = sha1($this->_auth['email'] . time());
        $this->modelUser->update(array("password_token" => $password), "id = {$this->_auth['id']}");
        $url = $this->modelCompany->getSettings()['sitename'];
        $data['firstname'] = $this->_auth['firstname'];
        $data['reset_link'] = "$url/reset/?token=$password";
        $data['subdomain'] = preg_replace("(^https?://)", "", $url );
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendVerifiedEmail()
    {
        $type = 0 ;

        $link = $this->_settings['sitename'] . '/activate?token=' . sha1($this->_auth['email']);
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $content = $partialHelper->partial('activate.phtml', ['link' => $link]);

        $subject = "Email Verification";
        $data['firstname'] = $this->_auth['firstname'];

        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $content);
        $emailSubject = $this->smarty->fetch('eval:' . $subject);


        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

    public function sendNewCampaignEmail($campaign,$jobs,$gam_action)
    {

        $j_array = [];
        for($i = 0 ; $i < count($jobs);$i++)
        {
            $j_array[] = $jobs[$i]['title']." ".($i+1);
        }
        $jobs = str_replace('_',' ',implode($j_array,' ,'));
        $rules = "";
        foreach ($gam_action as $i => $v )
        {
            $rules .= str_replace('_',' ',ucfirst($v['title'])).' ,';
        }
//        $rules = str_replace('_',' ',implode($gam_action,' ,'));
        $type = 31;
        $email = $this->getEmailByType($type);
        $data['campaign'] = $campaign['title'];
        $data['bounty'] = ucfirst($campaign['win_title']);
        $data['start'] = $campaign['start'];
        $data['end'] = $campaign['end'];
        $data['jobs'] = $jobs;
        $data['gamification'] = $gam_action;
        $data['rules'] = $rules;
//        $users = $this->modelUser->getRecruitersByCompany();
//        foreach ($users as $key => $user) {
            $data['username'] = $this->_auth['firstname'];
            $to = $this->_auth['email'];
            $this->assign($data);
            $emailBody = $this->smarty->fetch('eval:' . $email['content']);
            $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
            $this->sendMail($to,
                $emailSubject,
                $emailBody,
                $type,
                __METHOD__
            );
//        }

    }

    public function sendNewCampaignEmailForAdmin($campaign,$jobs,$gam_action)
    {

        $j_array = [];
        for($i = 0 ; $i < count($jobs);$i++)
        {
            $j_array[] = $jobs[$i]['title']." ".($i+1);
        }
        $jobs = str_replace('_',' ',implode($j_array,' ,'));
        $rules = "";
        foreach ($gam_action as $i => $v )
        {
            $rules .= str_replace('_',' ',ucfirst($v['title'])).' ,';
        }
//        $rules = str_replace('_',' ',implode($gam_action,' ,'));
        $type = 65;
        $email = $this->getEmailByType($type);
        $data['campaign'] = $campaign['title'];
        $data['bounty'] = ucfirst($campaign['win_title']);
        $data['start'] = $campaign['start'];
        $data['end'] = $campaign['end'];
        $data['jobs'] = $jobs;
        $data['gamification'] = $gam_action;
        $data['rules'] = $rules;
//        $users = $this->modelUser->getRecruitersByCompany();
//        foreach ($users as $key => $user) {
        $data['username'] = $this->_auth['firstname'];
        $to = $this->_auth['email'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($to,
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
//        }


    }

    public function sendNewCampaignEmailCreated($id_campaign)
    {
        $type = 97 ;
        $email = $this->getEmailByType($type);
        $campaign = $this->modelCampaign->getCampaignById($id_campaign);
        $jobs = $campaign['jobs'];
        $j_array = [];
        for($i = 0 ; $i < count($jobs);$i++)
        {
            $j_array[] = $jobs[$i]['title']." ".($i+1);
        }
        $jobs = str_replace('_',' ',implode($j_array,' ,'));

        $prize['title'] = $campaign['win_title'];
        $prize['value'] = $campaign['win_value'];
        $prize['currency'] = $this->modelContent->getCurrenciesSelect()[$campaign['id_currency']];
        $data['prize'] = $prize;
        $data['jobs'] = $jobs;
        $data['end'] = $campaign['end'];
        $data['start'] = $campaign['start'];
        $data['firstname'] = $this->_auth['firstname'];
        $data['company'] = $this->modelCompany->getCompanyById($this->_auth['id_company'])['title'];
        $this->assign($data);
        $emailBody = $this->smarty->fetch('eval:' . $email['content']);
        $emailSubject = $this->smarty->fetch('eval:' . $email['subject']);
        $this->sendMail($this->_auth['email'],
            $emailSubject,
            $emailBody,
            $type,
            __METHOD__
        );
    }

}