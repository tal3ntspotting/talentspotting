<?php


class Domain_User_UserFactory
{
    public static function create(Zend_Auth $user)
    {
        $type = (int) $user->getIdentity()->type;
        if($type <= 0 )
        {
            throw new Exception("$type dosent exist ,");
        }

        switch ($type)
        {
            case(1):
                return new Domain_User_Recommender($user);
            break;
            case(2):
                return new Domain_User_Admin($user);
            break;
            case(3):
                return new Domain_User_MasterAdmin($user);
            break;
            case(4):
                return new Domain_User_Recruiter($user);
            break;
            case(5):
                return new Domain_User_HiringManager($user);
            break;
            default:
                return new Domain_User_Candidate($user) ;
            break;
        }
    }

    public static function createInstance( $id_user)
    {
        $m = new models_User ;
        $user = $m->getUserById($id_user);
        $user_type = (int) $user['type'];

        switch ($user_type)
        {
            case(1):
                return new Domain_User_Recommender($user);
                break;
            case(2):
                return new Domain_User_Admin($user);
                break;
            case(3):
                return new Domain_User_MasterAdmin($user);
                break;
            case(4):
                return new Domain_User_Recruiter($user);
                break;
            case(5):
                return new Domain_User_HiringManager($user);
                break;
            default:
                return new Domain_User_Candidate($user) ;
                break;
        }
    }
}