<?php


$data['id_company'] = $id_company;
$data['type'] = 62;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Your referred candidates ";
    $subject = <<<'HERE'
                        Your referred candidates 
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	    Hi,<br>
        	<br>
        	<br>
        	<p>Nice work out there this week! Here’s a quick recap of your applicants on Talentspotting.</p>

                 {$table}
                  
                  <p>Send reminders to friends who haven’t applied yet in the <a href="{$settings.sitename}/profile#/recommendations"><b>Recommendations</b></a> section of your account.</p>

                  <p>Your position in the Talentspotting Leaderboard: {$leaderboard.position} </p>

                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}






$data['id_company'] = $id_company;
$data['type'] = 63;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;

$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "apply";
    $subject = <<<'HERE'
                        Join us on {$sitename}
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hello {$user.firstname} <br>
        	<br>
        	<br>
      		  <p>Activate your account in {$sitename}  and SHARE {$company} jobs in your network. </p> 
                  <p>You already received username & password from us, but if you can't find them you can always reset your password 
                  <a href="{$sitename}/login">here</a>.</p>
                   <p>  Choose friends to spend your working time and win bounties if your referred friends get hired! </p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;

    $this->insert($_data, 'emails_lang');
}




$data['id_company'] = $id_company;
$data['type'] = 60;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Talentspotting tip 5:5 Fastest way to refer a friend";
    $subject = <<<'HERE'
                        Talentspotting tip 5:5 Fastest way to refer a friend
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname},<br>
        	<br>
        	<br>
      		    <p>Time is important - so why waste it switching back and forth between an endless sea of tabs and e-mails? Talentspotting lets you refer in a few seconds and brings all your referral data in one place.</p>
                  
                  <p>Here’s how:
                    <ol>
                      <li><b>Refer even without a CV</b><br>Copy your unique job URL and send it to your friends via e-mail, social media, Whatsapp, sms or simply by posting it to your social accounts. Try it on mobile too!</li>
                      <li><b>Referral tracking</b><br>Click Recommendations to see all the people you've invited to apply for your Company's jobs on Talentspotting. If they activated their account, their name will show up next to their e-mail address.</li>
                      <li><b> Know what your candidates are up to</b><br>We send you e-mail notifications every time Recruiters move your referred candidates further along in the recruitment process.</li>
                      <li><b>Never miss out on a reward!</b><br>You'll get notified about new jobs open for referral as soon as they are posted. If you refer and your candidate is hired, you’ll see your reward and its due date in the Rewards section.</li>
                      <li><b>Know what else you can do to get rewards</b><br>Track your performance in referral campaigns and rise to the top of the recommender Leaderboard by accumulating more gamification points. To get many points fast try sharing jobs on social media.</li>
                    </ol>
                  </p>  
                  <p>Refer fast and easy from your Talentspotting account!</p>


                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id_company;
$data['type'] = 58;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Talentspotting Tip 3:5 Gamification Points";
    $subject = <<<'HERE'
                        Talentspotting Tip 3:5 Gamification Points
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'


<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},<br>
        	<br>
        	<br>
      		    <p>At Talentspotting we think all your referral efforts deserve to be rewarded, even if your candidate is hired or not. </p>
                 

                 <p><p>That’s why all your actions get gamification points:</p><ul>
                  <li>1 view = 1 point </li>
                  <li>Active account = 10 points</li>
                  <li>Candidate registered on Talentspotting = 10 points</li>
                  <li>Candidate applied on Talentspotting = 20 points</li>
                  <li>Candidate on Talentspotting is hired = 50 points</li>
                 </ul></p>


                 <p>If recruiters activate a referral Campaign, you can win the campaign bounty on top of the regular rewards by getting the most points for that campaign’s objectives, e.g. views, new candidates registered, new applicants, etc.</p>

                 <p><b>Don’t miss out!</b> See your ranking and how many points you still need to top your company’s referral Leaderboard in the <a href="{$settings.sitename}/profile#/leaderboards">Campaigns & Leaderboards</a> section of your account. </p>

                 <p><br>Good luck!<br>The Talentspotting Team</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id_company;
$data['type'] = 55;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Your campaign is closed! The winner is";
    $subject = <<<'HERE'
                        Your campaign is closed! The winner is
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
        	<p><b>Campaign "{$campaign.title}" is now closed. The top 3 winners by points are:</b></p>
                  <ul>
                    {foreach from=$winners key=key item=item}
                    {$key + 1 }. {$item.firstname} {$item.firstname} : {$item.total_points} <br />
                    {/foreach} 
                  </ul>
            <br>
            <p>Don't forget to give them their award and congratulate them for the good work!</p>
                  
            <p>For more details, please access Analytics - Leaderboard - Campaigns in your account. </p>
			<br>
			<p>Activate another campaign in <a href="{$settings.sitename}/view-campaigns">Campaigns</a>  - Add new Campaign. </p>
			<br>
			<p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<br><br>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}




$data['id_company'] = $id_company;
$data['type'] = 54;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Your monthly Talentspotting report";
    $subject = <<<'HERE'
                        Your monthly Talentspotting report
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	    Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>Nice work out there this month! Here’s a quick recap of your activity on Talentspotting.</p>
                  


                  {$table}




                  <p>Keep up the good work and please let us know if you need help with your referral strategy at <a href="mailto:hello@talentspotting.com.">hello@talentspotting.com.</a> </p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}



$data['id_company'] = $id_company;
$data['type'] = 52;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Wow! New applicants on Talentspotting";
    $subject = <<<'HERE'
                        Wow! New applicants on Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>Nice work out there this week! Here’s a quick recap of your applicants on Talentspotting.</p>
                  <p>Make sure you change their status in the hiring process. Priority processing of referred candidates helps you boost the results of your referral programme and keeps your recommenders engaged.</p>


{$table}




                  <p>Track your progress in the Job Management section of your account.</p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id_company;
$data['type'] = 53;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "These jobs & campaigns are about to expire";
    $subject = <<<'HERE'
                        These jobs & campaigns are about to expire
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname},</p>
        	<br>
        	<br>
        	<p>Here are all the Talentspotting jobs and campaigns due to expire next week. If you make no changes, they will be automatically deactivated after the expiry date. </p>

 {$table}




                  <p>Edit job and campaign details if you want to keep them visible for your Recommenders after the current expiry date.</p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>

			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id_company;
$data['type'] = 50;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Top your next recruitment peak with Talentspotting";
    $subject = <<<'HERE'
                        Top your next recruitment peak with Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	 <b>Want to meet your KPIs faster? </b>

                  <p>One of the quickest ways to dive in is to use Talentspotting campaigns for your recruitment peak: Choose your hot jobs, assign the referral objectives you want to reward and set bounties for the winners.</p>

                  <p>You can even target certain departments or people to build relevant engagement for any objective. Many of our first-time users say this was a quick way for them to experience Talentspotting’s benefits. Hope it helps you as well!</p>

                  <p>Once you login, follow these steps:</p>

                  <ol>
                    <li>Click Campaigns > Add new Campaign</li>
                    <li>Add a name for Campaign<br>For example: "Get extra…"</li>
                    <li>Select Jobs you want to feature<br>Make sure to upload these jobs to your account first </li>
                    <li>Set bounties for the winners</li>
                    <li>Assign referral objectives<br>Reward colleagues who get the most Views, New Applicants, Successful Hires, etc. for the Jobs you featured </li>
                    <li>Target your campaign<br>If your hot jobs are niched, you can activate the campaign only for colleagues in a certain city, department or by name</li>
                    <li>Choose campaign duration</li>
                    <li>Click Save & Activate</li>
                  </ol>

                  <p>All your Recommenders will get notified by e-mail </p>

                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}




$data['id_company'] = $id_company;
$data['type'] = 47;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Super easy way to create jobs on Talentspotting";
    $subject = <<<'HERE'
                        Super easy way to create jobs on Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
      		  <p>Want to get referrals rolling into your Talentspotting account? <br>Upload your open jobs first. </p> 

                  <b>How Does It Work? </b>

                  <ol>

                    <li>First, make sure to log in on {$settings.sitename} with your company e-mail address and password.</li>
                    <li>Next, click Job Management > Add new job.</li>
                    <li>Add Job Title, Cities, Departments, Skills, Description and Requirements.</li>
                    <li>Set the Bounty for colleagues who refer a successfully hired candidate<br> If you write a number, it’ll convert to Euro. If you give out a prize, write its name and upload a picture of the prize.<br>Don’t forget to choose the bounty due date, otherwise we’ll be unable to remind you when to give out rewards to your successful recommenders.</li>
                    <li>Select job Start and End Date<br>Once the end date passes, this job will be automatically deactivated, unless you adjust the date later on.</li>
                    <li>Pin to top<br>If this is a hot job, keep it on top of all other jobs by pressing the Pin to top button.</li>

                  </ol>

                 <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id_company;
$data['type'] = 48;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Invite your colleagues on Talentspotting";
    $subject = <<<'HERE'
                        Invite your colleagues on Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},<br>
        	<br>
        	<br>
        	<p>To complete your Talentspotting set up, invite all your colleagues to recommend on Talentspotting.</p>
            <b>How Does It Work? </b>

                  <ol>

                    <li>First, make sure to log in {$settings.sitename} with your company e-mail address and password. Follow the instructions here</li>
                    <li>Next, click Settings > Users.</li>
                    <li>Import all the Recommenders in your company<br>Click the Import from CSV button, download our template, fill it in with your users data (first name, last name, e-mail, role – Admin or Recommender) and upload it. All those invited will receive an automatic notification by mail and will be able to login into Talentspotting to refer.</li>
                    <li>Add new users<br>For users you want to add one-by-one just press the Add New Users button, fill in their First Name, Last Name, E-mail, assign them roles – Admins or Recommenders – and hit Save. All those invited will receive an automatic notification by mail and will be able to login into Talentspotting to refer.</li>
                    <li>See active users<br>You can track all active users in the Settings > Users section<br>For more details, learn all about setting up your Talentspotting account.</li>

                  </ol>

           <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>


                 <p><br>Happy recruiting!<br>{$settings.sitename}</p>
             
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}



$data['id_company'] = $id_company;
$data['type'] = 49;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "The ONE thing you must do to get results on Talentspotting ";
    $subject = <<<'HERE'
                        The ONE thing you must do to get results on Talentspotting 
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$recommender.firstname},</p>
        	<br>
        	<br>
        	<p>Now that you’ve set up your Talentspotting account and started receiving referrals, you probably wonder what you should be doing next.</p> 

                  <p>We have plenty of tips and tricks in store but today we’ll start with the ONE thing you must do to ensure spectacular referral results:<br><b>Change candidates’ status as fast as possible! </b></p>

                  <p>That’s it! </p>

                  <p>As soon as you receive an applicant notification from us, log in on Talentspotting, go to Job Management, select the job your candidate applied for, and change their recruitment status.</p>

                  <p>Once you do that, both the Candidate and their Recommender get an automatic notification. This will ensure a high level of engagement year-round and will show your colleagues that their involvement matters.</p>
                  
                  <br>
                  <b>Want to build a top-class referral programme?</b>
                  <p>Process referred candidates first. And let everybody know it.<br>If you can also commit to processing referred candidates within a specific period of time (48h, 72h, 7 days, etc.), you’ll reap the benefits of a highly engaged referral community.</p>
                  <p>Remember! Hundreds of recommenders have said that Transparency is the no. <b>#2</b> referral motivator, right after Simplicity.</p>



                 <p><br>So change your candidates’ status now & happy recruiting!<br>{$settings.sitename}</p>

			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}




$data['id_company'] = $id;
$data['type'] = 43;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 9;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Job Reminder";
    $subject = <<<'HERE'
Reminder about the recommendation for {$job.title}
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$user.firstname}, <br>
        	<br>
        	<br>
      		  <P>Quick follow up on the recommendation for {$job.title} position within {$job.company_title}. Hope you get the chance to review it and get excited about it. I’m here if you need additional info.</p>
                <p>Cheers,<br>{$recommender.recommender_firstname} {$recommender.recommender_lastname} <br />{$email}</p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}



$data['id_company'] = $id;
$data['type'] = 44;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Jobs Reminder";
    $subject = <<<'HERE'
What's new in {$company} jobs - open for referral on Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
   
  <td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$username},<br>
        	<br>
        	<br>
      		   <p>Ready to identify the best professionals in your network? Here are all the new <a href="{$settings.sitename}">{$company}</a> opportunities that can benefit from your help this week and the rewards you can get for them :)</p>
            <p>Make sure you share the jobs below on Talentspotting and identify quality candidates in time to qualify for the awards.</p>
            <br>
            <center>

             {$table}
             </center>
             <br>
             <p><b>How does it work? </b> Log in, share the opportunities above with your network and identify the best professionals who would qualify for them. <br />
                  Track your progress in the Recommendations section of your account.</p>


			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}

$id_company = (int) $id;
$data['id_company'] = $id_company;
$data['type'] = 46;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 2;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID)
{
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Set up your Talentspotting account";
    $subject = <<<'HERE'
                        Set up your Talentspotting account
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	   Hi {$recommender.firstname}, <br>
        	<br>
        	<br>
      		  <p>Are you ready to start using Talentspotting to recruit fast and easy? <br>
Set up your company’s account in 30 seconds. </p> 

                  <p>You’re an Administrator for your company’s account, which means you can see all referral data, add and delete jobs, track referral rewards, add campaigns, and more. </p>

                  <b>How Does It Work? </b>

                  <ol>
                    <li>First, make sure to log in {$settings.sitename} with your company e-mail address and password. </li>
                    <li>Next, click Settings > Company<br>Check appropriate boxes to ensure whether Candidates and Admins can also recommend, and whether Recommenders can also apply to jobs on Talentspotting.<br>Select referral track timeline – how long you want a Candidate to be connected to their Recommender. Within that period, if a Candidate applies for another job, their Recommender will be eligible for a referral bounty.</li>
                    <li>Click Settings > Users<br>Import all the Recommenders in your company via the Import from CSV button<br>Add Users one by one and assign them roles – Admins, Recommenders, Candidates via the Add Users button <br>Add Job Title, Cities, Departments, Skills, Description and Requirements.</li> 
                    <li>Check out Settings > Email templates<br>Those are all the notifications your users will receive, depending on their roles. You can edit their content at any time. </li>
                  </ol>

                  <p>For more details, learn all about setting up your Talentspotting account <a style="color: #095556" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#095556'" href="https://www.youtube.com/watch?v=G7jKA_-I51c">here</a>.</p>



			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id;
$data['type'] = 1;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 9;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Registration";
    $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
                    	Hello {$user.firstname},<br>
                    	<br>
                    	<br>
                    	You successfully registered on {$company.title}'s referral platform. <br>
						Here it's easier than ever to apply and get the job you want at {$company.title}. <br><br>
						If you have any questions or comments, please feel free to reach out! <br>
						<br>
						To activate your account please click here <a href='{$link}'>{$link}</a> 
						<br>
						<br><br>
						<center>
						<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
						</center>

						<p>&nbsp;</p>
                </td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}








$data['id_company'] = $id;
$data['type'] = 30;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 9;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "External Recommender";
    $subject = <<<'HERE'
Welcome to Talentspotting
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$username}, <br>
        	<br>
        	<br>
    		 <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.

                  </P>
                  <b>Please activate your account <a href="{$settings.sitename}/login">here</a>, using the initial password: {$password.password}
                  </b>
                  <P>You are now empowered to help your friends find better jobs and bring people like you at {$settings.sitename}. Plus, you can earn amazing bounties for successful hires or for promoting the jobs in your network via our referral platform.
                </P>
                <p>If you have any questions or comments, feel free to reach out :)</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}





$data['id_company'] = $id;
$data['type'] = 33;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Tips & tricks to earn rewards";
    $subject = <<<'HERE'
Talentspotting - Tips & tricks to earn rewards
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	  Hi {$username},<br>
        	<br>
        	<br>
        	<p>We figured you might welcome a few tips and tricks to be successful fast on Talentspotting. Here goes!</p> 
                    <ol>
                        <li>Use all recommendation channels. Share opportunities with all your networks - Facebook, LinkedIn, Twitter.</li>
                        <li>Always think - Who is the best professional you've ever worked with? This will make it easier for you come up with the most suitable people in your network.</li>
                        <li>Drop a personal line when recommending the job to someone. All your recommendations will go out with pre-defined messages so you dont spend time writing promotion line for the jobs. However, we believe it will help if you use a personal line in the message why you love the company or think that the job is suited for your friend</li>
                    </ol>
                    <p>Most of the actions above imply just clicks, so just do them and start getting your rewards.</p>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>

HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}




$data['id_company'] = $id;
$data['type'] = 37;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Admin Report";
    $subject = <<<'HERE'
Your Talentspotting Activity
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'

<td style="font-family: Roboto, sans-serif; font-size: 14px; padding: 20px; line-height: 20px; color: #444; border-bottom: 4px solid #ccc">
        	 Hi {$username} ,<br>
        	<br>
        	<br>
        	<p>Things are moving forward with your referral program! Check out the latest:</p>
                  <ul>
                    <li>{$applications} : new applicants in the last week.</li>
                    <li>{$reached} : users reached.</li>
                    <li>{$bounty} : bounties to be paid.</li>
                  </ul>
            <br>
			<br>
			 <p>View your applicants and boost your referral program some more!</p>
			<br>
			<br><br>
			<center>
			<a class="button" style="background-color:{$bg_color} !important" target="_blank" href="{$subdomain_url}">Complete you profile</a
			</center>

			<p>&nbsp;</p>
</td>

HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}



$data['id_company'] = $id;
$data['type'] = 23;
$data['status'] = 1;
$data['sent'] = 1;
$data['frequency'] = 1;
$data['direction'] = 1;
$data['email'] = $company_email;
$data['from'] = $company_name;
$this->insert($data, "emails");
$lastID = $this->lastInsertId();
if ($lastID) {
    $_data['id_email'] = $lastID;
    $_data['id_lang'] = 1;
    $_data['title'] = "Custom Reminder";
    $subject = <<<'HERE'
HERE;
    $_data['subject'] = $subject;
    $content = <<<'HERE'
<!DOCTYPE html>
<html>
<head>
<title>*|MC:SUBJECT|*</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

{literal}
<style type="text/css">
    /* FONTS */
    @media screen {
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 400;
          src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 400;
          src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
        }
        
        @font-face {
          font-family: 'Open-Sans', 'Arial', 'sans-serif';
          font-style: italic;
          font-weight: 700;
          src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }
    
    /* MOBILE STYLES */
    @media screen and (max-width:600px){
        h1 {
            font-size: 26px !important;
            line-height: 32px !important;
        }
    }

    /* ANDROID CENTER FIX */
{ margin: 0 !important; }
</style>
{/literal}
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    
</div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#55C3D5" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 150px; min-width: 100px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                        <a href="https://talentspotting.com" target="_blank">
                            <img mc:edit="logo" alt="Logo" src="https://s24.postimg.org/o94mgrc2t/Talent_Spotting_01.png" width="100" height="100" style="display: block; width: 150px; max-width: 150px; min-width: 100px; max-height: 150px; min-height:100px; font-family: 'Open-Sans', 'Arial', 'sans-serif'; color: #ffffff; font-size: 18px;" border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#55C3D5" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-weight: 400; letter-spacing: 4px; line-height: 42px;" mc:edit="main heading">
                      <h1 style="font-size: 42px; font-weight: 400; margin: 0;">{$message}</h1>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
            <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                  <p>{$message}</p>
                </td>
              </tr>

              <tr><td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="lead">
                    <a href="{$settings.sitename}/login"><img src="http://dev.talentspotting.com/resources/frontend/images/signinmailbutton.png" alt="Sign in"></a>  
                    </td>
                    </tr>
  
 
                <!-- HEADLINE -->
                
                
                <tr>
                
                  <td bgcolor="#55C3D5" align="center" style="padding: 30px 30px 30px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size: 18px; font-weight: 400; line-height: 25px;" mc:edit="support copy">
                    <h2 style="font-size: 20px; font-weight: 400; color: #ffffff; margin: 0;">Need more help?</h2>
                    <p style="margin: 0;"><a href="mailto:hello@talentspotting.com" style="color: #095556;">We&rsquo;re here, ready to talk</a></p>
                  </td>
                </tr>
            </table>
           
            <br>
      
                  <p style="margin: 0; color:#666666; font-family: 'Open-Sans', 'Arial', 'sans-serif'; max-width: 600px; font-size: 18px">Talentspotting is an automated referral system that taps into your
organization network to simplify and differentiate your recruitment.</p>
<br>
            <a href="https://www.facebook.com/talentspotting"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="https://dev.talentspotting.com/resources/files/images/facebook-logo-button.png"></a><a href="https://www.linkedin.com/showcase/10524842"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="https://dev.talentspotting.com/resources/files/images/linkedin-logo-button.png"></a><a href="https:talentspotting.com"><img style="width:50px; height:50px; padding: 10px 10px 10px 10px" src="http://dev.talentspotting.com/resources/files/images/house.png"></a><br>
            <br>
            <a href="http://www.talentspotting.com" style="font-family: 'Open-Sans', 'Arial', 'sans-serif'; text-decoration: none; color: #666666"><strong>&copy; Talentspotting 2018</strong></a>

            <p>&nbsp;</p>
 
            </td>
          </tr>
        
      </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
      
    




            <center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                    <tr>
                        <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                            <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                <tr>
                                    <td align="center" valign="top" style="color:#606060; font-family: 'Open-Sans', 'Arial', 'sans-serif'; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">
                                        This email was sent to <a href="mailto:*|EMAIL|*" target="_blank" style="color:#404040 !important;">*|EMAIL|*</a>
                                        <br>
                                        <a href="*|ABOUT_LIST|*" target="_blank" style="color:#404040 !important;"><em>why did I get this?</em></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="%sendgrid_unsub_url%" style="color:#404040 !important;">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="*|UPDATE_PROFILE|*" style="color:#404040 !important;">update subscription preferences</a>
                                        <br>
                                        *|LIST:ADDRESSLINE|*
                                        <br>
                                        <br>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {literal}
                <style type="text/css">
                    @media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }
                </style>
                {/literal}
            </center></body>
</html>
HERE;
    $content = <<<'HERE'
{$message}
HERE;
    $_data['content'] = $content;
    $this->insert($_data, 'emails_lang');
}

