<?php

class modules_campaigns_forms_EditCampaign extends WebApp_Form {

    protected $fields;
    protected $_settings;
    public $company;
    public $_campaign ;

    public function __construct($options = array()) {
        
        $this->fields = array(
            'title',
            'countries',
            'counties',
            'cities',
            'selected_cities',
            'departments',
            'sub_departments',
            'sub_sub_departments',
            'projects',
            'jobs',
            'start_date',
            'end_date',
            'winner_announcement_date',
            'reward_redeem_date',
            'valid_share_if_in_campaign_periode',
            'new_candidate_from_previous_share_not_included',
            'new_applicant_from_previous_share_not_included',
            'only_applicants_following_valid_share_links',
            'new_hired_from_previous_not_included',
            'minim_point_winner',
            'win_amount',
            'id_currency',
            'id_shedule',
            'win_title',
            'win_description',
            'message',
            'gamifications',
            'id',
        );
        $this->company = $options['company'];
        $this->_campaign = new models_Campaign ;
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-campaign.phtml", "translate" => $translate, "settings" => $this->_settings))));

        $this->loadElems();
    }

    public function loadElems() 
    {
        if (isset($this->fields)) 
        {
            foreach ($this->fields as $k => $v) 
            {
                switch ($v) {
                    case 'title':
                    case 'message':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('stringLength',true,array(3,6000));
                        $element->getValidator('stringLength')
                            ->setMessage($this->translate->_('VALIDATOR_STRINGLENGTH'));
                    break;
//                    case 'countries':
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRequired(true);
//                        $model = new models_Content;
//                        $data = $model->getCountriesSelect();
//                        $element->addMultioptions($data);
//                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Validator',true);
//                        $element->getValidator('Validator')
//                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
//                    break;
//                    case 'counties':
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRequired(true);
//                        $element->setRegisterInArrayValidator(false);
//                        $data = [];
//                        $element->addMultioptions($data);
//                        $element->addValidator('NotEmpty',true);
//                        $element->getValidator('NotEmpty')
//                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
//                        $element->addValidator('Validator',true);
//                        $element->getValidator('Validator')
//                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
//                    break;
//
//                    case 'cities':
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRegisterInArrayValidator(false);
//                        $data = [];
//                        $element->setRequired(true);
//                        $element->addMultioptions($data);
//                        $element->addValidator('NotEmpty',true);
//                        $element->getValidator('NotEmpty')
//                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
//                        $element->addValidator('Validator',true);
//                        $element->getValidator('Validator')
//                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
//                        break;

                    case 'selected_cities':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->setRegisterInArrayValidator(false);
                        $data = [];
                        $element->setRequired(true);
                        $element->addMultioptions($data);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                    break;

                    case 'departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRequired(true);
                        $data = [] ;
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                    break;

                    case 'gamifications':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->setRequired(true);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                    break;

                    case 'sub_departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRequired(true);
                        $data = [] ;
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                    break;

                    case 'sub_sub_departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->setRequired(true);
                        $data = [] ;
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                    break;
//
                    case 'projects':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $data = [];
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                    break;

                    case 'jobs':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $data = [];
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                    break;

                    case 'start_date':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                    break;

                    case 'end_date':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                    break;

                    case 'winner_announcement_date':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                    break;

                    case 'reward_redeem_date':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                    break;

                    case 'minim_point_winner':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setRequired(true);
                    break;

                    case 'win_amount':
                        $element = new WebApp_Form_Element_Text($v);
                    break;

                    case 'id_currency':
                        $element = new WebApp_Form_Element_Text($v);
                    break;

                    case 'id_shedule':
                        $element = new WebApp_Form_Element_Text($v);
                    break;

                    case 'win_title':
                        $element = new WebApp_Form_Element_Text($v);
                    break;

                    case 'win_description':
                        $element = new WebApp_Form_Element_Text($v);
                    break;

                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                    break;
                }
                $this->addElement($element);
            }
        }
    }

}
