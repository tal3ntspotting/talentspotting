<?php

class Campaigns_AdminController extends Zend_Controller_Action {

    protected $_model;
    protected $_model_social;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_Campaign;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;
        $this->view->current_menu = 'campaigns';
    }

    public function viewAction() {
        if (!Zend_auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }
//        $view = new Zend_View();
//        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'sublayouts');
//        $partialHelper = new Zend_View_Helper_Partial();
//        $partialHelper->setView($view);
//        $view_rendred = $partialHelper->partial('leaderboards.phtml', ['translate' => $this->view->translate ]);
//        $this->view->leaderboards = $view_rendred;

        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'sublayouts');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $data = $partialHelper->partial('campaign-report.phtml', [
            'translate' => $this->view->translate,
            'url' => '/users/admin/campaiagn-info']);
        $this->view->leaderboards = $data;

        $this->view->headTitle()->set('View Campaigns | ' . $this->_settings['sitename'] . '');
    }

    public function campaignAction()
    {
        $form = new modules_campaigns_forms_EditCampaign();
        $this->view->form = $form;
    }

    public function saveAction()
    {
//        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $form = new modules_campaigns_forms_EditCampaign();
        $campaign  = new models_Campaign ;
        $request = $this->_params;
        if($form->isValid($request))
        {

            if($request['id'] > 0)
            {
                $campaign_data = $campaign->getCampaignById($request['id']);
                $date = new DateTime($campaign_data['start']);
                $now = new DateTime();

                if($date < $now) {
                    echo json_encode(['error' => true,'message' => 'You cannot edit campaign after start']);
                    exit();
                }

//                if($campaign['status'] == 1 && $campaign['start'] <= date('Y-m-d'))
//                {
//                    echo json_encode(['error' => true,'message' => 'You cannot edit campaign after start']);
//                    exit();
//                }
            }
            $id = $campaign->saveCampaign($request);
            echo json_encode(['error' => false ,'id' => $id]);
            exit();
        }
        echo json_encode(['error' => true,'message' => WebApp_Tools::mapError($form)]);
        exit();
    }

    public function uploadImageAction()
    {
//        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
//        if($this->getRequest()->isPost())
//        {
//
            $mUser = new models_User ;
            $id_campaign = (int) $this->_params['id'];
            if($id_campaign > 0)
            {
                $id = (int) $id_campaign;
                if ($_FILES['bounty_file']['name'] != "") {
                    $file = $mUser->renameUploadedFile($_FILES['bounty_file']['name']);
                    move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $file);
                    $this->_model->delete("id_campaign=".$id, "campaign_media");
                    $this->_model->insert(array("bounty_file" => $file,"id_campaign" => $id), "campaign_media");
                }
//                echo '<pre>';print_r($_FILES);die;
                echo json_encode(['error',false]);
            }

//        }
        exit();
    }

    public function getCampaignByIdAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $campaign = $this->_model->getCampaignById($id);
//        echo '<pre>';
//        print_r($campaign);
//        die;
        echo json_encode($campaign);
        exit();
    }

    public function campaignsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $campaigns = $this->_model->getAllCampaigns();
        echo json_encode($campaigns);
        die;
    }

    public function activateAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if($this->getRequest()->isPost())
        {
            $status = 1 ;
            $id_campaign = (int) $this->_params['id'];
            $this->_model->changeCampaignStateById($id_campaign,$status);
        }
        echo json_encode([]);
        die;
    }

    public function disactivateAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if($this->getRequest()->isPost())
        {
            $status = 0 ;
            $id_campaign = (int) $this->_params['id'];
            $this->_model->changeCampaignStateById($id_campaign,$status);
        }
        echo json_encode([]);
        die;
    }

    public function addCampaignAction()
    {
        if(!Zend_Auth::getInstance()->hasIdentity()) $this->_redirect('/login');
        $id = $this->_params['id'] ;
        if(($id) > 0)
        {
            $campaign = new models_Campaign();
            $campaign_data = $campaign->getCampaignById($id);
            $date = new DateTime($campaign_data['start']);
            $now = new DateTime();

            if($date < $now) {
                $this->_redirect('/view-campaigns');
            }
            $modeContent = new models_Content ;

            $populate_sub_departments = [] ;
            $selected_sub_departments = [] ;
            $selected_departments = [] ;
            $populate_sub_sub_departments = [] ;
            $selected_sub_sub_departments = [] ;
//            echo '<pre>';print_r($campaign_data);die;
            foreach($campaign_data['departments'] as $key => $value)
            {
                $selected_departments[] = $value['id'];
            }
            foreach($campaign_data['sub_departments'] as $key => $value)
            {
                $selected_sub_departments[] = $value['id'];
            }
            foreach($campaign_data['sub_sub_departments'] as $key => $value)
            {
                $selected_sub_sub_departments[] = $value['id'];
            }

            if(!empty($selected_sub_departments))
            {

                $populate_sub_departments = json_encode($modeContent->getSubDepartmentsByDepartmentList($selected_departments));
            }
//            echo '<pre>';print_r($selected_sub_departments);die;
             if(!empty($selected_sub_departments))
            {

                $populate_sub_sub_departments = json_encode($modeContent->getSubSubDepartmentsByDepartmentByList($selected_sub_departments));
            }


            $selected_sub_departments = json_encode($selected_sub_departments);
            $selected_sub_sub_departments = json_encode($selected_sub_sub_departments);

            echo "<script> var selected_sub_sub_deps = $selected_sub_sub_departments; var populate_sub_sub_deps = $populate_sub_sub_departments; var selected_sub_deps = $selected_sub_departments;var poplute_sub_deps = $populate_sub_departments;</script>";
        }
    }

    public function getCampaignJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Jobs;
        $mCampaign = new models_Campaign ;
        if($this->getRequest()->isPost())
        {
            $filters = $this->_params['filters'];
            $data = $model->getJobsSelectActiveFilter($filters);
//            $used_jobs = $model->getRemainedJobsInCampaignsActive();
//            $remained = (array_diff_key($data,$used_jobs));
            $all = [] ;
            foreach ($data as $key => $job)
            {
                $all[$key] = $model->getJobById($key,false,true);
            }
            echo json_encode($all,true);
            die;
        }
    }

    public function getCampaignsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $data = $this->_model->getAllCampaigains();
        echo json_encode($data,true);
        die;
    }

    public function campaignDetailsAction()
    {
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'sublayouts');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $view_rendred = $partialHelper->partial('leaderboards.phtml', ['translate' => $this->view->translate ]);
        $this->view->result = $view_rendred;
    }

    public function exportAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $modelContent = new models_Content;
        $end = (isset($this->_params['startExport'])) ? date('Y-m-d', strtotime($this->_params['endExport'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['endExport'])) ? date('Y-m-d', strtotime($this->_params['startExport'])) : date('Y-m-d', strtotime('-1 month'));
        $data = $this->_model->generateExport($start,$end);
        $output = fopen("php://output", 'w') or die("Can't open php://output");
        $columns = array();

        foreach ($data as $row) {
            foreach ($row as $roll => $set) {
                $columns[$roll] = $roll;
                continue;
            }
        }
        $cols = array_values($columns);
        $filename = 'Talentspotting_campaign_report_'.date('Y-m-d').'.csv';


        header("Content-Type: application/force-download");
        header("Content-Type:application/octet-stream");
        header("Content-Type: application/download");

        header('Content-Disposition: attachment; filename="' . $filename . '"');
        fputcsv($output, $cols);
        foreach ($data as $product) {
            fputcsv($output, $product);
        }
        fclose($output) or die("Can't close php://output");
        exit;
    }

}
