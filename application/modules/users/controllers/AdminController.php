<?php

/**
 * index controller for default module
 */
class Users_AdminController extends Zend_Controller_Action {

    protected $_model;
    protected $_modelJobs;
    protected $_modelCompany;
    protected $_modelCampaign;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;
    protected $translate;
    public $a = 4;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_User;
        $this->_modelJobs = new models_Jobs;
        $this->_modelCompany = new models_Company;
        $this->_modelCampaign = new models_Campaign;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;

        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
    }

    public function viewCompaniesAction() {
        $this->view->current_menu = 'companies';
        $this->view->current_sub_menu = 'view';

        $modelCompany = new models_Company;

        $this->view->items = $modelCompany->getCompanies();

        if (isset($this->_params['export'])) {
            $this->_model->export($this->view->items, "companies_list.csv");
        }

        if (isset($this->_params['export-pdf'])) {
            echo "<script>window.print();</script>";
        }

        $this->view->headTitle()->set('View Companies | ' . $this->_settings['sitename'] . '');
    }

    public function editCompanyAction() {
        $this->view->current_menu = 'companies';
        $this->view->current_sub_menu = 'edit';

        $modelCompany = new models_Company;

        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $modelCompany->getCompanyById($id);
        }

        $form = new modules_users_forms_EditCompany(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $id = $modelCompany->saveCompany($_POST);
            $modelCompany->addEmailTemplates($id);
            $this->_redirect("/view-companies");
        }

        $this->view->headTitle()->set('Add / Edit Company | ' . $this->_settings['sitename'] . '');
    }

    public function ratingDetailsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];

        $this->view->company = $this->_model->getCompanyById($id);
        $this->view->items = $this->_model->getCompanyRatingDetails($id);
    }

    public function activateAction() {
        $id = $this->_params['id'];
        $this->_model->activateCompany($id);
        $this->_redirect("/view-companies");
    }

    public function inactivateAction() {
        $id = $this->_params['id'];
        $this->_model->inactivateCompany($id);
        $this->_redirect("/view-companies");
    }

    public function candidateAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $this->view->permission = (isset($this->_params['permission'])) ? $this->_params['permission'] : 0 ;
        $user = $this->_model->getUserById($id);
        $constructed_user[] = $user ;
        $this->view->userJobApplications = array_shift($this->_model->getUserJobsApplication($constructed_user))['jobs'];
        $model_job = new models_Jobs;

        $this->view->uploadedcv = $this->_model->getUploadedCvById($id);

        $id_application = $this->_params['id_application'];
        if($this->_params['id_application'])
        {
            $myApplication = $model_job->getJobApplicationById($id_application);
            if($myApplication['status'] == 1)
            {
                $model_job->update(['status' => 2,'status_reason' => 0,'updated'=>date('Y-m-d H:i:s')],
                    "id={$myApplication['id']}",'job_applications');
                $this->_model->setjobApplicationStatusHistory($myApplication['id_job'],$myApplication['id'],2,0);
            }
        }

        $this->_model->updateApplicationStatus($id_application);

        if ($message != "error") {
//            $this->view->positions = $this->_model->getFullUserPositions($id);
            $this->view->positions = $this->_model->getUserPositions($id);
            $this->view->skills = $this->_model->getUserSkills($id);
            $this->view->education = $this->_model->getUserEducation($id);
            $this->view->certifications = $this->_model->getUserCertifications($id);
            $this->view->user = $user;
            $this->view->id_application = $id_application;
            $this->view->application = $this->_model->getApplicationById($id_application);
        }
        $this->view->status = $this->_modelJobs->getStatus();
//        echo '<pre>';print_r($this->view->positions);die;
    }

    public function searchAction() {
        $this->view->current_menu = "candidates";
        $this->view->current_sub_menu = "cv";

        $modelContent = new models_Content;

        $filters['id_skill'] = $this->_params['id_skill'];
        $filters['id_certification'] = $this->_params['id_certification'];
        $filters['id_department'] = $this->_params['id_department'];
        $filters['id_industry'] = $this->_params['id_industry'];
        $filters['search'] = $this->_params['search'];

        if ($this->getRequest()->isPost()) {
            $url = "/users/admin/search?";
            if (!empty($filters)) {
                foreach ($filters as $key => $value) {
                    if ($key != "search") {
                        foreach ($value as $val) {
                            $url .= "&{$key}[]={$val}";
                        }
                    } else {
                        $url .= "&{$key}={$value}";
                    }
                }
            }

            $this->_redirect($url);
        }
        $this->view->certifications = $modelContent->getCertificationsSelect();
        $this->view->skills = $modelContent->getSkillsSelect();
        $this->view->departments = $modelContent->getDepartmentsSelect();
        $this->view->industries = $modelContent->getIndustriesSelect();
        $this->view->positions = $this->_model->getPositionsSelect();
        $this->view->filters = $filters;
        $this->view->items = $this->_model->searchUsers($filters);
        $this->view->user_skills = $this->_model->getUsersSkills();
        $this->view->user_certifications = $this->_model->getUsersCertifications();
        $this->view->mycvs = $this->_model->getMyCVs();
        if ($this->_model->_user->type == 2) {
            $this->view->company = $this->_model->getCompanyById($this->_model->_user->id_company);
        }
        $this->view->headTitle()->set('Search CV | ' . $this->_settings['sitename'] . '');
    }

    public function mycvsAction() {

        $t['t_to'] = $this->translate->_('You are going to change the status for');
        $t['t__to'] = $this->translate->_('to');
        $t['t__from'] = $this->translate->_('from');
        $t['new'] = ucfirst($this->translate->_('New'));
        $t['viewed'] = ucfirst($this->translate->_('Viewed'));
        $t['hired'] = ucfirst($this->translate->_('Hired'));
        $t['called'] = ucfirst($this->translate->_('Called'));
        $t['interview'] = ucfirst($this->translate->_('Interview'));
        $t['test'] = ucfirst($this->translate->_('Test'));
        $t['offer'] = ucfirst($this->translate->_('Offer'));
        $t['final_interview'] = ucfirst($this->translate->_('Final interview'));
        $t['end'] = ucfirst($this->translate->_('end'));
        $t['background_check'] = ucfirst($this->translate->_('Background Check'));
        $t['assesment'] = ucfirst($this->translate->_('Assesment'));
        $t['passed'] = ucfirst($this->translate->_('Passed'));
        $t['no_show'] = ucfirst($this->translate->_('No show'));
        $t['to_be_called'] = ucfirst($this->translate->_('To be called'));
        $t['unfit'] = ucfirst($this->translate->_('Unfit'));
        $t['not_interested'] = ucfirst($this->translate->_('Not interested'));
        $t['no_answer'] = ucfirst($this->translate->_('No answer'));
        $t['moved_to_another_process'] = ucfirst($this->translate->_('Moved to another process'));
        $t['not_passed'] = ucfirst($this->translate->_('Not passed'));
        $t['offer_accepted'] = ucfirst($this->translate->_('Offer accepted'));
        $t['offer_rejected'] = ucfirst($this->translate->_('Offer rejected'));
        $t['candidates'] = ucfirst($this->translate->_('candidates'));
        $t['applied_on'] = ucfirst($this->translate->_('Applied on'));
        $t['latest_lob'] = ucfirst($this->translate->_('Latest Job'));
        $t['recommender'] = ucfirst($this->translate->_('recommender'));
        $t['status_days'] = ucfirst($this->translate->_('status days'));
        $t['applied_for'] = ucfirst($this->translate->_('Applied for'));
        $t['job_status'] = ucfirst($this->translate->_('Job status'));
        $t['active_jobs'] = ucfirst($this->translate->_('Active jobs'));
        $t['inactive_jobs'] = ucfirst($this->translate->_('Inactive jobs'));
        $t['job_name'] = ucfirst($this->translate->_('Job name'));
        $t['source'] = ucfirst($this->translate->_('Source'));
        $t['no_em'] = ucfirst($this->translate->_('No emails will be sent on this status update'));
        $t['em_tbs'] = ucfirst($this->translate->_('The following emails are going to be sent, if remain checked'));
        $t['phone'] = ucfirst($this->translate->_('phone'));
        $t['days'] = ucfirst($this->translate->_('days'));
        $t['reset'] = ucfirst($this->translate->_('Reset'));
        $t['city'] = ucfirst($this->translate->_('City'));
        $t['job'] = ucfirst($this->translate->_('job'));
        $t['move_to_another_two'] = ucfirst($this->translate->_('Create a new job-application for this candidate for a more suitable position - same recruiting step'));
        $t['aware'] = ucfirst($this->translate->_('Please be aware that by moving candidate to another process, you will close the current process and a new job application will be created'));
        $t['confirm_delete_user'] = $this->translate->_('Are you sure you want to delete this user');
        $t['confirm_duplicate'] = $this->translate->_('Are you sure you want to duplicate this item');
        $t['confirm_delete_job'] = $this->translate->_('Are you sure you want to delete this job');
        $t['confirm_activate_item'] = $this->translate->_('Are you sure you want to activate this item');
        $t['view_all'] = $this->translate->_('view all');
        $t = json_encode($t);
        $this->view->dg_js = "<script> var trans_key = $t ;</script>";

        $substyles = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' .
            DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR.'sublayouts' ;
        $view = new Zend_View();
        $view->setScriptPath($substyles);
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $data = ['translate' => $this->translate];
        $bodyData = $partialHelper->partial( 'dragndrop.phtml', $data );
//        $this->_helper->viewRenderer->setRender('dragndrop');
        $this->view->dragndrop = $bodyData;

//        $ats = $this->_settings['ats'];
//        if($ats) {
//            $this->_helper->viewRenderer->setRender('ats/mycvs');
//            return 0 ;
//        }
//        $this->view->current_menu = "candidates";
//        $this->view->current_sub_menu = "my_cvs";
//
//        $modelContent = new models_Content;
//        $filters['id_skill'] = $this->_params['id_skill'];
//        $filters['id_certification'] = $this->_params['id_certification'];
//        $filters['search'] = $this->_params['search'];
//        $this->view->certifications = $modelContent->getCertificationsSelect();
//        $this->view->skills = $modelContent->getSkillsSelect();
//        $this->view->positions = $this->_model->getPositionsSelect();
//        $this->view->filters = $filters;
//        $this->view->items = $this->_model->searchUsersMine($filters);
//        $this->view->items = $this->_model->getApplication($this->view->items);
//        $this->view->items = $this->_model->getUserJobsApplication($this->view->items);
//        $this->view->items = $this->_model->getUserRecommendedDate($this->view->items);
//        $this->view->items = $this->_model->getUserLastJobStatus($this->view->items);
//        $this->view->user_skills = $this->_model->getUsersSkills();
//        $this->view->user_certifications = $this->_model->getUsersCertifications();
//        $this->view->mycvs = $this->_model->getMyCVs();
//        $this->view->form = new modules_users_forms_Filter;
//        if ($this->_model->_user->type == 2) {
//            $this->view->company = $this->_model->getCompanyById($this->_model->_user->id_company);
//        }
//        $this->view->headTitle()->set('My CVs | ' . $this->_settings['sitename'] . '');
//        $this->view->status = $this->_modelJobs->getStatus();
        $this->view->current_menu = "candidates";
    }

    public function viewScoringAction() {
        $this->view->current_menu = "scoring";
        $this->view->current_sub_menu = "view";
        $this->view->items = $this->_model->getUsersWithScoring();
        $this->view->headTitle()->set('View users scoring | ' . $this->_settings['sitename'] . '');
    }

    public function scoringDetailsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];

        $modelJobs = new models_Jobs;
        $modelPayments = new models_Payments;

        $this->view->jobs = $modelJobs->getJobsSelect();
        $this->view->companies = $this->_model->getCompaniesSelect();
        $this->view->bounties = $modelPayments->getBountiesSelect();

        $this->view->user = $this->_model->getUserById($id);
        $this->view->points = $this->_model->getUserGamificationPoints($id);
    }


    public function leaderboardsAction() {

        $id_company = ($this->_model->_user->type == 2) ? $this->_model->_user->id_company : null;
        $start = (isset($this->_params['start'])) ? $this->_params['start'] : null;
        $end = (isset($this->_params['end'])) ? $this->_params['end'] : null;

//        $modelCampaign = new models_Campaign();
//        $modelJobs = new models_Jobs();
//
////        $this->view->campaigns = $modelCampaign->getCampaignsWithReports();
////        $this->view->jobs = $modelJobs->getJobsRecommenders();
////        $result = $this->_modelCompany->getLeaderboardsDetails();
//        if ($this->getRequest()->isPost()) {
//            $filters = $this->_params['filters'];
//            $offset = $this->_params['offset'];
//            $result = $this->_modelCompany->getLeaderboardsDetails($filters,$offset);
//        }
////        echo '<pre>';
////        print_r($filters);die;
//
//        echo '<pre>';print_r($result);die;
//        $this->view->leaderboards = $result;

        $campaign_close_to_end = $this->_modelCampaign->getCloseToEndCampaign();
        $this->view->total_end = $campaign_close_to_end['total'];
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'sublayouts');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $view_rendred = $partialHelper->partial('campaign-report.phtml', [
            'translate' => $this->view->translate,
            'url' => '/users/admin/campaiagn-info']);
        $this->view->campaign_report = $view_rendred;



        $this->view->start = $start;
        $this->view->end = $end;
        $this->view->current_menu = "analytics";
        $this->view->current_sub_menu = "leaderboards";

        $this->view->headTitle()->set('Analytics - Leaderboards | ' . $this->_settings['sitename'] . '');
    }

    public function leaderboardsInfoAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if($this->getRequest()->isPost())
        {
                $filters = $this->_params['filters'];
                $offset = $this->_params['offset'];
                $result = $this->_modelCompany->getLeaderboardsDetails($filters,$offset);
                echo json_encode($result);
                die;
        }
    }

    public function leaderboardsProfileAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id_company = ($this->_model->_user->type == 2) ? $this->_model->_user->id_company : null;
        $start = (isset($this->_params['start'])) ? $this->_params['start'] : null;
        $end = (isset($this->_params['end'])) ? $this->_params['end'] : null;

        $modelCampaign = new models_Campaign();
        $modelJobs = new models_Jobs();

        $this->view->campaigns = $modelCampaign->getCampaignsWithReports();
        $this->view->jobs = $modelJobs->getJobsRecommenders();

        if ($this->getRequest()->isPost()) {
            $this->view->leaderboards = $this->_model->getProfileLeaderboards($this->_model->_user->id_company, $start, $end);
        } else {
            $this->view->leaderboards = $this->_model->getProfileLeaderboards($this->_model->_user->id_company, $start, $end);
        }

//        echo '<pre>'; print_r($this->view->leaderboards);die;

        $this->view->start = $start;
        $this->view->end = $end;
        $this->view->current_menu = "analytics";
        $this->view->current_sub_menu = "leaderboards";

        $this->view->headTitle()->set('Analytics - Leaderboards | ' . $this->_settings['sitename'] . '');
    }

    function campaignProfileAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelCampaign = new models_Campaign();
        $this->view->campaigns = $modelCampaign->getCampaignWithmyAction();
    }

    function jobsProfileAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelJobs = new models_Jobs();
        $this->view->jobs = $modelJobs->getMyJobsRecommenders();
    }

    public function profileLeaderboardsDetailsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $input = json_decode(file_get_contents("php://input"), true);
            $id_campaign = (int) $input['id_campaign'];
            $id_job = (int) $input['id_job'];
            if ($id_campaign) {
                $result = (array) $this->_model->getUserCampaignDetailedLeaderboards($id_campaign);
                print_r(json_encode($result));
            }
            if ($id_job) {
              $result = $this->_model->getUserJobLeaderboards($id_job);
              print_r(json_encode($result));
            }

            die;
        }
    }

    public function importAction() {
        if ($this->getRequest()->isPost()) {

            $file = uniqid() . "_" . $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], "/tmp/{$file}");
            $csv = array_map('str_getcsv', file("/tmp/{$file}"));
            if ($_POST['ignore'] == 1) {
                unset($csv[0]);
            }

            if (!empty($csv)) {

                    $message = $this->_model->importUsersFromCsv($csv);
            }

            $this->_flashMessenger->addMessage($message);
            $this->_redirect("/users");
        }

        $this->view->current_menu = "users";
        $this->view->current_sub_menu = "import";

        $this->view->headTitle()->set('Import users from file | ' . $this->_settings['sitename'] . '');
    }


    public function viewAction() {
        $this->view->settings = $this->_settings;
        if (!Zend_auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }
        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'view';

        $filters = array();

        if ($this->_params['type'] > 0) {
            $filters['type'] = $this->_params['type'];
        }
        // echo '<pre>';
        // print_r($filters);die;
        $this->view->items = $this->_model->getSettingsUsers($filters);
//        echo '<pre>';print_r($this->view->items);die;

        if (isset($this->_params['export'])) {
            $this->_model->export($this->view->items, "users_list.csv");
        }

        if (isset($this->_params['export-pdf'])) {
            echo "<script>window.print();</script>";
        }
        $this->view->filters = $filters;

        $this->view->headTitle()->set('View Users | ' . $this->_settings['sitename'] . '');
    }

    public function editAction() {
        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'edit';

        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getUserById($id);
            if ($data['type'] == 1 && $data['id_company'] == 0) {
                $data['type'] = 9;
            }
        }

        $form = new modules_users_forms_EditUser(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $cache = WebApp_Cache_Memcache::getInstance();
            $cache->delete('get-contents');
            if ($this->_model->_user->type == 2) {
                $_POST['id_company'] = $this->_model->_user->id_company;
                if ($_POST['type'] == 2) {
                    $_POST['is_company_admin'] = 1;
                }
                //$_POST['type'] = 2;
            }

            if ($_POST['id'] < 1) {
                $_POST['password'] = $this->generateStrongPassword();
                if ($user = $this->_model->getUserByEmail($_POST['email'])) {
                    $msg = $this->translate->_("account-exists");
                    $this->_flashMessenger->addMessage($msg);
                    if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
                        $this->_redirect("/users");
                    } else {
                        $this->_redirect("/edit-user");
                    }
                }
            }

            $_POST['status'] = 1;
            $_POST['id_parent'] = Zend_Auth::getInstance()->getIdentity()->id;
            array_walk_recursive($_POST, function(&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });
            $data = $_POST;
//            $data['email'] = "erwerwe".rand(0,111111111)."sdsd@.com";
            $id_user = $this->_model->addUser($data, true, 27);


//            if($_POST['type'] == 1) {
//               $this->_model->addUser($_POST, true, 27);
//            } else
//            {
//                $this->_model->addUser($_POST, false, 27);
//                $transport = null;
//                if (!empty($this->_settings['email_smtp'])) {
//                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//                }
//                $mail = new WebApp_Mail();
//                $mail->addTo($_POST['email'], $_POST['firstname']);
//                $mail->setFrom('hello@talentspotting.com', $this->_settings['sitename']);
//                $mail->setSubject("Welcome to Talentspotting");
//                $mail->setBodyFromViewScript("add-admin.phtml", array('lastname' => $_POST['lastname'], 'firstname' => $_POST['firstname'], 'password' => $_POST['password'],"sitename" => $this->_settings['sitename']));
//                $mail->send($transport);
//            }
            




            $this->_redirect("/users");
        }

        $this->view->headTitle()->set('Add / Edit User | ' . $this->_settings['sitename'] . '');
    }


    public function companyEventsAction() {
        $this->view->current_menu = "users";
        $this->view->current_sub_menu = "companies_events";

        $id_event = $this->_params['id_event'];

        $company = $this->_model->getCompanyById($this->_model->_user->id_company);

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/login");
        $client->setParameterPost("email", $this->_settings['booth_api']['email']);
        $client->setParameterPost("password", $this->_settings['booth_api']['password']);

        $request = json_decode($client->request("POST")->getBody());

        if ($request->data != "invalid_login_data") {
            $token = $request->data;
        }

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/get-company-booths");
        $client->setParameterGet("token", $token);
        $client->setParameterGet("email", $company['email']);

        $request = json_decode($client->request("POST")->getBody());

        $this->view->items = $request->data;
        $this->view->company = $company;

        $this->view->headTitle()->set('Company Virtual Booths | ' . $this->_settings['sitename'] . '');
    }

    public function pushCompanyAction() {
        $id = $this->_params['id'];
        $company = $this->_model->getCompanyById($id);
        $countries = $this->_model->getCountries();

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/login");
        $client->setParameterPost("email", $this->_settings['booth_api']['email']);
        $client->setParameterPost("password", $this->_settings['booth_api']['password']);

        $request = json_decode($client->request("POST")->getBody());

        if ($request->data != "invalid_login_data") {
            $token = $request->data;
        }
        $request = null;

        if ($this->getRequest()->isPost()) {
            $request['id_event'] = $this->_params['id_event'];
            $request['email'] = $company['email'];
            $request['interior_type'] = $this->_params['interior_type'];
            $request['exterior_type'] = $this->_params['exterior_type'];
            $request['video'] = $this->_params['video'];
            $request['chat_id'] = $this->_params['chat_id'];
            $request['chat_url'] = $this->_params['chat_url'];
            $request['jobs_url'] = $this->_params['jobs_url'];
            $request['poster'] = $this->_model->setCompanyPoster($id);
            $request['logo'] = APPLICATION_URL . "/resources/files/companies/original/" . $company['logo'];
            $request['country'] = $countries[$company['id_country']];
            $request['title'] = $company['title'];
            $request['description'] = $company['description'];
            $request['profile'] = $company['long_description'];

            $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/add-company");
            $client->setParameterPost("token", $token);
            $client->setParameterPost("data", $request);
            $response = $client->request("POST");

            $this->_redirect("/users/index/companies-events");
        }

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/get-interior-types");
        $client->setParameterGet("token", $token);

        $request = json_decode($client->request("POST")->getBody());
        $interiors = $request->data;

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/get-exterior-types");
        $client->setParameterGet("token", $token);

        $request = json_decode($client->request("POST")->getBody());
        $exteriors = $request->data;

        $client = new Zend_Http_Client($this->_settings['booth_api']['url'] . "/api/rest/get-events");
        $client->setParameterGet("token", $token);

        $request = json_decode($client->request("POST")->getBody());
        $events = $request->data;

        $this->view->company = $company;
        $this->view->interiors = $interiors;
        $this->view->exteriors = $exteriors;
        $this->view->events = $events;

        $this->view->headTitle()->set('Create Virtual Booth | ' . $this->_settings['sitename'] . '');
    }

    public function companyAction() {
        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'company';

        $id = $this->_model->_user->id_company;
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getCompanyById($id);
        }

        $session = new Zend_Session_Namespace("redirector");
        $session->url = "";

        $data['redir'] = $this->_params['r'];

        $form = new modules_users_forms_EditCompany(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
//            echo '<pre>';print_r($_POST);die;
            $_POST['id'] = $id;
            $redir = $_POST['redir'];
            unset($_POST['redir']);
//            echo '<pre>';print_r($_POST);
            $track_timeline = $_POST['track_timeline'];
            $this->_model->saveCompany($_POST);

            switch ($redir) {
                case "recharge":
                    $this->_redirect("/recharge");
                    break;

                default:
                    $this->_redirect("/view-billing");
                    break;
            }
        }

        $this->view->headTitle()->set('Add / Edit Company | ' . $this->_settings['sitename'] . '');
    }

    public function viewBillingAction() {

        $company = $this->_model->getCompanyById($this->_model->_user->id_company);
        $company_howItWorks = $this->_model->getHowItWorks()['content'] ;
//        $this->view->items = $this->_model->getBillingInfo();
        $form_billing = new modules_users_forms_Billing();
        $form_company = new modules_users_forms_company(array(
            "content" => $company['content'],
            "how-it-works" => $company_howItWorks,
            'about_us' => $company['about_us']));
        $form_company->populate($company);
        $form_billing->populate((array) $this->view->items[0]);
        $this->view->form = $form_billing;
        $this->view->form_company = $form_company;
        $this->view->current_menu = "users";
        $this->view->current_sub_menu = "billing";

        $this->view->headTitle()->set('View Billing Info | ' . $this->_settings['sitename'] . '');
    }

    public function editBillingAction() {
        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'billing';

        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getBillingInfoById($id);
        }

        $form = new modules_users_forms_EditBilling(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $this->_model->saveBilling($_POST);
            $this->_redirect("/view-billing");
        }

        $this->view->headTitle()->set('Add / Edit Billing Info | ' . $this->_settings['sitename'] . '');
    }

    public function deleteBillingAction() {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "payment_billing_info");
        $this->_redirect("/view-billing");
    }

    public function deleteAction() {
        $id = $this->_params['id'];
        $for_good = $this->_params['for_good'];
        if ($for_good == 1) {
            $this->_model->delete("id = '{$id}'", "users");
            $this->_model->delete("id_user = '{$id}'", "user_alerts");
            $this->_model->delete("id_user = '{$id}'", "user_certifications");
            $this->_model->delete("id_user = '{$id}'", "user_connections");
            $this->_model->delete("id_user = '{$id}'", "user_departments");
            $this->_model->delete("id_user = '{$id}'", "user_education");
            $this->_model->delete("id_user = '{$id}'", "user_hobbies");
            $this->_model->delete("id_user = '{$id}'", "user_industries");
            $this->_model->delete("id_user = '{$id}'", "user_positions");
            $this->_model->delete("id_user = '{$id}'", "user_skills");
            $this->_model->delete("id_user = '{$id}'", "user_tooltip_view");
            $this->_model->delete("id_user = '{$id}'", "salary_calculator");
            $this->_model->delete("id_user = '{$id}'", "quizez_completions");
            $this->_model->delete("id_user = '{$id}'", "payments");
            $this->_model->delete("id_user = '{$id}'", "job_views");
            $this->_model->delete("id_user = '{$id}'", "job_recommendations");
            $this->_model->delete("id_user = '{$id}'", "job_applications");
            $this->_model->delete("id_user = '{$id}'", "company_unblocked_cvs");
            $this->_model->delete("id_user = '{$id}'", "companies_rating");
            $this->_model->delete("id_user = '{$id}'", "company_users");
            $this->_model->delete("id_user = '{$id}'", "bounties");
        } else {
            $this->_model->deleteUser($id);
        }
        $this->_redirect("/users");
    }

    public function deleteCandidatesAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        if ($_REQUEST) {
            $ids = $this->_params['id'];
            $result = array_filter($ids, function ($arrayEntry) {
                return is_numeric($arrayEntry);
            }
            );

            foreach ($result as $id) {
                $this->_model->delete("id = '{$id}'", "users");
                $this->_model->delete("id_user = '{$id}'", "user_alerts");
                $this->_model->delete("id_user = '{$id}'", "user_certifications");
                $this->_model->delete("id_user = '{$id}'", "user_connections");
                $this->_model->delete("id_user = '{$id}'", "user_departments");
                $this->_model->delete("id_user = '{$id}'", "user_education");
                $this->_model->delete("id_user = '{$id}'", "user_hobbies");
                $this->_model->delete("id_user = '{$id}'", "user_industries");
                $this->_model->delete("id_user = '{$id}'", "user_positions");
                $this->_model->delete("id_user = '{$id}'", "user_skills");
                $this->_model->delete("id_user = '{$id}'", "user_tooltip_view");
                $this->_model->delete("id_user = '{$id}'", "salary_calculator");
                $this->_model->delete("id_user = '{$id}'", "quizez_completions");
                $this->_model->delete("id_user = '{$id}'", "payments");
                $this->_model->delete("id_user = '{$id}'", "job_views");
                $this->_model->delete("id_user = '{$id}'", "job_recommendations");
                $this->_model->delete("id_user = '{$id}'", "job_applications");
                $this->_model->delete("id_user = '{$id}'", "company_unblocked_cvs");
                $this->_model->delete("id_user = '{$id}'", "companies_rating");
                $this->_model->delete("id_user = '{$id}'", "company_users");
                $this->_model->delete("id_user = '{$id}'", "bounties");
            }
        }

        $this->_redirect("/users/admin/mycvs");
    }

    public function deleteCompanyAction() {
        $id = $this->_params['id'];
        $this->_model->update(array("status" => 2), "id = {$id}", "companies");
        $this->_redirect("/view-companies");
    }

    public function leaderboardsDetailsAction() {
        $id_job = $this->_params['id_job'];
        $id_campaign = $this->_params['id_campaign'];
        if ($id_job > 0) {
            $leaderboards = $this->_model->getJobLeaderboards($id_job);
            $gamification_actions = $this->_model->getActionFromCampaignbyId($id_campaign);
            
                        foreach($leaderboards as $key => &$user) {
                foreach($gamification_actions as $action) {
                    $leaderboards[$key][$action] = $this->_model->getGamificationResultByIdAndAction($user['id'],$action) ;
                }
            }
            
            $columns = array();

            foreach ($leaderboards as $row) {
                foreach ($row as $roll => $set) {
                    $columns[$roll] = $roll;
                    continue;
                }
            }
            $cols = array_values($columns );
            
        }

        if ($id_campaign > 0) {
            
            $leaderboards = $this->_model->getCampaignDetailedLeaderboards($id_campaign);
//            echo '<pre>';
//            print_r($leaderboards);
//            die;
            $this->view->campaign = $this->_model->getCampaignDetails($id_campaign);
            $gamification_actions = $this->_model->getActionFromCampaignbyId($id_campaign);
            
            foreach($leaderboards as $key => &$user) {
                foreach($gamification_actions as $action) {
                    $leaderboards[$key][$action] = $this->_model->getGamificationResultByIdAndAction($user['id'],$action) ;
                }
            }
            
            $columns = array();

            foreach ($leaderboards as $row) {
                foreach ($row as $roll => $set) {
                    $columns[$roll] = $roll;
                    continue;
                }
            }
            $cols = array_values($columns );
            
//            echo '<pre>';
//            print_r( $this->view->columns);
//            print_r($leaderboards);
//            die;
//            echo '<pre>';
//            print_r($this->_model->getActionFromCampaignbyId($id_campaign));
//            die;
//            echo '<pre>';
//            print_r($leaderboards);
//            die;
        }
        if ($id_job != "") {
            $this->view->hashTag = "#jobs";
        } elseif ($id_campaign != "") {
            $this->view->hashTag = "#campaigns";
        } else {
            $this->view->hashTag = "";
        }
        $candidateCanRecommend = $this->_modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_Company);
        if($candidateCanRecommend['allow_candidate_recommend'])
        {
            $this->view->leaderboards = $leaderboards;
            $this->view->columns = $cols;
        }else{
            foreach ($leaderboards as $key => &$value)
            {
                unset($value['extend_network']);
            }

            foreach ($cols as $key => &$col)
            {
                if($col == 'extend_network') unset($cols[$key]);
            }
            $this->view->leaderboards = $leaderboards;
            $this->view->columns = $cols;
        }
    }

    public function jobRecommandersAction() {
        
    }

    public function templateAction() {
        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'template';

        $modelsContent = new models_Content;
        $this->view->items = $modelsContent->getEmails();
    }

    public function  editTemplateAction() {
//        Zend_Layout::getMvcInstance()->disableLayout();

        $this->view->current_menu = 'users';
        $this->view->current_sub_menu = 'template';

        $modelsContent = new models_Content;

        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $modelsContent->getEmailByIdEdit($id);
        }
        $form = new modules_users_forms_EditTemplate(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $modelsContent->saveEmail($_POST);
            $this->_redirect("/emails");
        }
    }

    public function rateAction() {
        $id = base64_decode($this->_params['id']);
        if($id) {
            $this->_model->setViewedRate($id);
            $this->view->user = $this->_model->getrateById($id);
            $this->view->id = $id;
        }
        
        if ($this->getRequest()->isPost()) {
            $id = $this->_params['id'];
            $text = WebApp_Tools::ClearString(strip_tags($this->_params['text']),"");
            $rate = $this->_params['rate'];
            $this->_model->updateRateUser($rate,$id,$text);
//            echo '<pre>';
//            print_r($this->_params);
//            die;
        }

}

    public function appjobsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $_REQUEST['id'];
        $data = $this->_model->getUserApplicationsById($id);
        foreach ($data as $key => $value) {
            echo "<p class='appjobtitlestl'>{$value['title']}</p>";
        }
    }

    public function removeMeAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) Zend_Auth::getInstance()->getIdentity()->id;
        
        $kennex = new kennexa_api([],kennexa_api::edit);
        $kennex->Delete_USer(kennexa_api::delete);
        
        $this->_model->delete("id = '{$id}'", "users");
        $this->_model->delete("id_user = '{$id}'", "user_alerts");
        $this->_model->delete("id_user = '{$id}'", "user_certifications");
//        $this->_model->delete("id_user = '{$id}'", "user_connections");
        $this->_model->delete("id_user = '{$id}'", "user_departments");
        $this->_model->delete("id_user = '{$id}'", "user_education");
        $this->_model->delete("id_user = '{$id}'", "user_hobbies");
        $this->_model->delete("id_user = '{$id}'", "user_industries");
        $this->_model->delete("id_user = '{$id}'", "user_positions");
        $this->_model->delete("id_user = '{$id}'", "user_skills");
//        $this->_model->delete("id_user = '{$id}'", "user_tooltip_view");
//        $this->_model->delete("id_user = '{$id}'", "salary_calculator");
//        $this->_model->delete("id_user = '{$id}'", "quizez_completions");
//        $this->_model->delete("id_user = '{$id}'", "payments");
        $this->_model->delete("id_user = '{$id}'", "job_views");
        $this->_model->delete("id_user = '{$id}'", "job_recommendations");
        $this->_model->delete("id_user = '{$id}'", "job_applications");
//        $this->_model->delete("id_user = '{$id}'", "company_unblocked_cvs");
//        $this->_model->delete("id_user = '{$id}'", "companies_rating");
        $this->_model->delete("id_user = '{$id}'", "company_users");
//        $this->_model->delete("id_user = '{$id}'", "bounties");
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirect("/");
    }

    public function userPaginationAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $total_record = count($this->_model->getUsersPagination($filters));
        $length = (int) $this->_params['length'];
        $start = (int) $this->_params['start'];
        $draw = (int) $this->_params['draw'];
        $columns = $this->_params['columns'];
        $search = $this->_params['search'];

        $this->view->items = $this->_model->getUsersPagination($filters, $length, $start, $columns, $search);
        foreach ($this->view->items as $key => &$value) {
            if ($value['adder_firstname'] == "null") {
                $value['adder_firstname'] = "";
            }
            if (!isset($value['adder_firstname'])) {
                $this->view->items[$key]['adder_firstname'] = "";
            }
            if (!isset($value['adder_lastname'])) {
                $this->view->items[$key]['adder_lastname'] = "";
            }
            $value['id'] = '<div class=""><input value="' . $value['id'] . '" id="checkbox' . $value['id'] . '" class="" type="checkbox"><label for="checkbox' . $value['id'] . '"></label></div>';
            $value['image'] = '<img src = "/resources/files/user/' . $value['image'] . '" style = "width: 50px;border-radius: 50%"/>';
            $value['options'] = <<<HERE

            <a href="/user?id={$value['id']}" data-target="#myModal" data-toggle="modal" class="btn-edit"><i class="fas fa-edit"></i></a>
            <a href="/reminder?email={$value['email']}" data-toggle="modal" data-target="#myModa" class="btn-approuve"><i class="fas fa-check"></i></a>
            <a href="/delete-user?id={$value['id']}" class="delete_item btn-del"><i class="fas fa-trash"></i></a>

HERE;
        }

        $response = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($total_record),
            "recordsFiltered" => intval($total_record / $length),
            "data" => $this->view->items,
        );
        echo (json_encode($response));
        die;
    }

    public function editTermsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $terms = base64_decode($this->_params['data']);
            $terms = htmlspecialchars($terms, ENT_QUOTES, 'UTF-8');
            $this->_model->saveTerms($terms);
            die;
        }
        $this->view->content = preg_replace("/\r|\n/", "", $this->_model->getTerms());
    }

    public function termsConditionAction() {

        $companyModel = new models_Company;
        Zend_Layout::getMvcInstance()->disableLayout();
        $company_id = (Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getIdentity()->id_company : $companyModel->getCompanyBySubdomainSlug()['id']);

//       $company_id = $companyModel->getCompanyBySubdomainSlug()['id'];
        $this->view->content = htmlspecialchars_decode($companyModel->getTermsAndConditionByComapnyId($company_id)['content']);
    }
    
    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }

        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }

        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }

        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
    
    public function atsUserAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setRender('ats/ats-user');
        $id = (int) $this->_params['id'];
        $form = new modules_users_forms_AtsUser;

        if ($id > 0) {
            $modelUser = new models_User;
            $modelJob = new models_Jobs;
            $user = $modelUser->getUserById($id);
            $user['department'] = array_keys($modelUser->getUserDepartmentById($id) + $modelUser->getUserCustomDepartmentById($id));
            $user['jobs'] = $modelJob->getRecruitersJobSelect($id);
            $form->populate($user);
            $this->view->type = $user['type'];
            $this->view->id = $id;

            $this->view->user = (array) $user;
        }

        $this->view->form = $form;
    }
    
    public function leaderboardsPaginationAction() {

        $order = $this->_params['order'][0]['column'];
        $columns = $this->_params['columns'];
        
        
        
//                echo '<pre>';
//          $start_date = explode('.', $_REQUEST['search']['value'])[0];
//          $end_date = explode('.', $_REQUEST['search']['value'])[1];
//        print_r($start_date);
//        print_r($end_date);
//        print_r($order_by);
//        print_r($order_value);
//        die;
        
        
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $start_date = (isset($this->_params['startDate'])) ? $this->_params['startDate'] : null;
        $end_date = (isset($this->_params['endDate'])) ? $this->_params['endDate'] : null;
        $order_by =  $columns[$order]['data'];
        $order_value =  $this->_params['order'][0]['dir'];
        
//        $this->view->items = $this->_model->getUsersPagination($filters);

        if($this->_params['search']['value'] != "") {
              $start_date = explode('.', $_REQUEST['search']['value'])[0];
              $end_date = explode('.', $_REQUEST['search']['value'])[1];
        } 


        $this->view->items = $this->_model->getLeaderboardsPagination($this->_model->_user->id_company, $start_date, $end_date,$length,$begin,$order_by ,$order_value)['referral'];
//        foreach($this->view->items as $key => $user)
//        {
//            echo '<pre>';print_r($user);die;
//        }
        
       
        
//        $total_record = count($this->_model->getLeaderboardsPagination($this->_model->_user->id_company, $start_date, $end_date,$length,$begin)['referral']);
        $total_record = count( $this->view->items);
        $length = (int) $this->_params['length'];
        $start = (int) $this->_params['start'];
        $draw = (int) $this->_params['draw'];
        $columns = $this->_params['columns'];
        $search = $this->_params['search'];
        
        $begin = (int) $this->_params['start'];
        
         
//        echo $order_by.'<br />';
//        echo $order_value.'<br />';
//        $this->view->items = $this->_model->getLeaderboardsPagination($this->_model->_user->id_company, $start_date, $end_date,$length,$begin,$order_by ,$order_value)['referral'];
        
        
        
        
        $response = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($total_record),
            "recordsFiltered" => intval($total_record / $length),
//            "recordsFiltered" => 50,
            "data" => $this->view->items,
        );

        echo json_encode($response,true);
        die;
    }
    
    public function campaignReportsAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $modelCampaign = new models_Campaign();
        $this->view->campaigns = array_values($modelCampaign->getCampaignsWithReports());
        
        $response = array(
//            "draw" => intval($draw),
//            "recordsTotal" => intval($total_record),
//            "recordsFiltered" => intval($total_record / $length),
//            "recordsFiltered" => 50,
            "data" => $this->view->campaigns,
        );

        echo json_encode($response,true);
        die;
    }
    
    public function xyzAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $a = 4 ;
                return $a;
    }
    public function noRatedAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_rate = $this->_params['id_rate'];
        $ignore = $this->_params['ignore'];
        if($ignore) {
            $data['viewed'] = 1;
            $data['ignore_rate'] = 1;
            $this->_model->updateRateById($id_rate, $data);
        }
        die;
    }
    public function importCvAction()
    {

    }

    public function usersPaginationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $length = (int) $this->_params['length'];
        $start = (int) $this->_params['start'];
        $draw = (int) $this->_params['draw'];
        $search = $this->_params['search']['value'];
        $items = $this->_model->getSettingsUsers([],$length,$start,$search);
        $total_record= $items['count'];
        $response = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($total_record),
            "recordsFiltered" => intval($total_record / $length),
//            "recordsFiltered" => 50,
            "data" => $items['items'],
        );

        echo json_encode($response,true);
        die;

    }

    public function templateEditAction()
    {
        $template = $this->_modelCompany->getBrandEmail();
        $this->view->template = $template;
        if ($this->getRequest()->isXmlHttpRequest()) {
            echo 'ajax come';die;
        }
    }

    public function templateEditSaveAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isPost()) {
            $template = "
<!DOCTYPE html>
<html>
<head>
{$_POST['template']}
            </center></body>
            </html>
            ";
            $this->_modelCompany->update(['template' => $template],"id={$_POST['id']}","email_template");
            $this->redirect('/users/admin/template-edit');
        }
    }

    public function userAction()
    {

    }

    public function bulkCvAction()
    {
        // bulk cv front end
    }
    public function bulkUploadCvAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model_user = new models_User ;
        if ($this->getRequest()->isPost()) {

            $source = $this->_params['source'];
            $_bulk_education = $this->_params['_bulk_education'];
            $_bulk_position = $this->_params['_bulk_position'];
            $_bulk_language = $this->_params['_bulk_language'];
            $_bulk_skill = $this->_params['_bulk_skill'];
            $_bulk_user = $this->_params['_bulk_user'];
            $_bulk_cv = $this->_params['_bulk_cv'];
            $_bulk_education = WebApp_Tools::mapInputArray($_bulk_education,true);
            $_bulk_position = WebApp_Tools::mapInputArray($_bulk_position,true);
            $_bulk_language = WebApp_Tools::mapInputArray($_bulk_language,true);
            $_bulk_skill = WebApp_Tools::mapInputArray($_bulk_skill,true);
            $_bulk_user = WebApp_Tools::mapInputArray($_bulk_user);

            $validator = new Zend_Validate_EmailAddress();
            if(!$validator->isValid($_bulk_user['email'])) {
                $result = [
                    'error' => true,
                    'messag' => 'Please enter valid Email'
                ] ;
                echo json_encode($result);
                exit();
            }

            if ($this->_model->getUserByEmail($_bulk_user['email']))
            {
                echo json_encode(['message' => 'User already exist','error' => true]);
                exit;
            }
            if($source > 0) $_bulk_user['source'] = $source;
            $_bulk_user['first_login'] = date('Y-m-d H:i:s');
            $id_user = $model_user->addNewUser($_bulk_user);

            if(!empty($_bulk_education)) $model_user->addUserEducations($_bulk_education,$id_user);
            if(!empty($_bulk_position)) $model_user->addUserPositions($_bulk_position,$id_user);
            if(!empty($_bulk_skill)) $model_user->addUserSkills($_bulk_skill,$id_user);
            if(!empty($_bulk_language)) $model_user->setUserLangauge($_bulk_language,$id_user);
            if(!empty($_bulk_cv))
            {
                $model_user->insert(array("id_user" => $id_user, "title" => $_bulk_cv['name'], "file" => $_bulk_cv['hash'], "date" => date("Y-m-d H:i:s")), "user_cv");
            }

            $id_job = (int) $this->_params['job'];
            $id_recruiter = (int) $this->_params['recruiter'];
            if($id_job > 0)
            {
                $source = ($source > 0) ? $source : 0 ;
                $application = ['id_job' => $id_job,
                    'id_user' => $id_user,
                    'id_recommender' => Zend_Auth::getInstance()->getIdentity()->id,
                    "date" => date("Y-m-d H:i:s"),'status'=>1,'status_reason'=>0,'source' => $source];
                $model_user->insert($application,'job_applications');

                $id_application = $model_user->lastInsertId();
                $app['id_job'] = $id_job;
                $app['id_user'] = $id_user;
                $app['id_recommender'] = Zend_Auth::getInstance()->getIdentity()->id;
                $app['job_application_id'] = $id_application ;
                $app['modified_by'] = Zend_Auth::getInstance()->getIdentity()->id ;
                $app['status'] = 1;
                $app['status_reason'] = 0;
                $app['date'] =  date("Y-m-d H:i:s");
                $app['notes'] =  "";
                $model_user->insert($app,'job_application_status_history');

            }

            if($id_recruiter > 0 && $id_job > 0)
            {
                $model_user->insert(["id_user" => (int) $id_recruiter,'id_job' => (int) $id_job,'date' => date("Y-m-d H:i:s")],'recruiter_job');
            }
                echo json_encode(['message' => 'User saved successfully','id_user' => $id_user]);
                die;
        }
    }

    public function campaignInfoAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if($this->getRequest()->isPost())
        {

            $id = $this->_params['id'];
            $offset = $this->_params['offset'];
            $filers = $this->_params['filters'];
            $result = $this->_modelCompany->getLeaderboardsDetailsById($filers,$offset,$id);
            echo json_encode($result);
            die;
        }
    }

    public function campaignWinnerAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if($this->getRequest()->isPost())
        {

            $id = $this->_params['id'];
            $result = $this->_modelCompany->getLeaderboardsDetailsByIdWinner($filers,$offset,$id);
            echo json_encode(['user' =>$result[0]]);
            die;
        }
    }

    public function campaignReportAction()
    {
        $id = (int) $this->_params['id'];

        $mCampaign = new models_Campaign ;
        $campaign = $mCampaign->getCampaignById($id);
        $mCampaign = new models_Campaign();
        $gamification_action = $mCampaign->getCampaignAction($id);
        $all_active_actions = $mCampaign->getActiveGamificationAction();
        foreach($all_active_actions as $key => &$value)
        {
            foreach($gamification_action as $k => $action)
            {
                if($value['id'] == $action['id']){
                    $value['exist_in_campaign'] = true;
                }
            }
        }
        $style = ($id > 0) ? "style='display:none'" : "";
        $this->view->title = $campaign['title'];
        $view = new Zend_View();
        $view->setScriptPath(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'sublayouts');
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $data = $partialHelper->partial('leaderboards.phtml',
            ['translate' => $this->view->translate ,"url" => '/users/admin/campaign-info',
            'actions' => $all_active_actions,
            'style' => $style
            ]);
        $this->view->campaign_report = $data;
        $this->view->id = $id;
    }

    public function campaiagnInfoAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if($this->getRequest()->isPost())
        {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
            $result = $this->_modelCompany->getLeaderboardsDetails($filters,$offset);
            echo json_encode($result);
            die;
        }
    }

    public function leaderboardInfoAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
//        $id_company = Zend_Auth::getInstance()->getIdentity()->id_company;
//        $data = $this->_modelCompany->getLeaderboardsInfo()['referral'];
//        $update = new models_Update;
////        $update->upContent();
//        foreach($data as $i => &$value)
//        {
//            $total = 0;
//            foreach($value as $key => $val)
//            {
//                $action = $key;
//                if($this->_modelCompany->existAction($action))
//                {
//                    $value[$key] = $this->_modelCompany->collectGamificationInfo($value['id'],$action);
//                    $total += $value[$key]['points'];
//                }
//            }
//            $value['total_points'] = $total;
//        }
        $offset = 0 ;
        $cache = WebApp_Cache_Memcache::getInstance();
        if($this->getRequest()->isPost())
        {

            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];

            if($cache->get('leaderboard'))
            {
                $data = $cache->get('leaderboard');
            }
            else
            {
                $data = $this->_modelCompany->getLeaderboardsInfo($filters,$offset)['referral'];
                foreach($data as $i => &$value)
                {
                    $total = 0;
                    foreach($value as $key => $val)
                    {
                        $action = $key;
                        if($this->_modelCompany->existAction($action))
                        {
                            $value[$key] = $this->_modelCompany->collectGamificationInfo($value['id'],$action);
                            $total += $value[$key]['points'];
                        }
                    }
                    $value['total_points'] = $total;
                }
                krsort($data);
                $cache->set('leaderboard',$data);
            }




//            $result = $this->_modelCompany->getLeaderboardsDetails($filters,$offset);
        }
        $result = $data;
        if(!empty($filters['sorting']))
        {
            foreach ($filters['sorting'] as $key => $value)
            {
                if(strtolower($value) == 'asc')
                {
                    usort($result, function($a, $b) use($key) {
                        return $a[$key]['points'] - $b[$key]['points'];
                    });
                }

                if(strtolower($value) == 'desc')
                {
                    usort($result, function($a, $b) use($key) {
                        return $b[$key]['points'] - $a[$key]['points'];
                    });
                }

            }
        } else {
            // sorting default dupa total
            usort($result, function($a, $b) {
                return $b['total_points'] - $a['total_points'];
            });
        }
        echo json_encode(array_splice($result,$offset,20));
        die;
    }
}
