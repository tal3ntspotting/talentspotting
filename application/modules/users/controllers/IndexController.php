<?php

/**
 * index controller for default module
 */
class Users_IndexController extends Zend_Controller_Action
{

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;
    protected $_user;


    public function init()
    {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();
        $this->_user = Zend_Auth::getInstance()->getIdentity();

        $this->_model = new models_User;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;

        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
    }

    public function indexAction()
    {

    }

    public function profileAction()
    {
        $modelContent = new models_Content;
        $modelStats = new models_Stats;
        $modelJobs = new models_Jobs;
        $modelPayments = new models_Payments();

        $this->view->status = $modelJobs->getStatus();
        $ats = $this->_settings['ats'];
        $modelCompany = new models_Company;
        $this->view->allow_external = $modelCompany->getCompaniesAllowExternal(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_external'];
        $this->view->allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        $id_user = $this->_params['id'];
        $id_job = $this->_params['id_job'];
        $this->view->is_public = false;


        $email_verification = $this->_model->checkActivatedUser(Zend_Auth::getInstance()->getIdentity()->email);

//        $this->view->rewards = $modelContent->getRewardsPage();
        $this->view->gamificationActions = $modelStats->getScoringPoints();
        $this->view->hobbies = $this->_model->getProfileHobbies();

        $this->view->myLinkedinConnections = $this->_model->getMyLinkedinConnections();

        if ($id_job > 0) {
            $this->view->job = $modelJobs->getJobById($id_job);
        }
        $this->view->company = $this->_model->getCompanybyId(Zend_Auth::getInstance()->getIdentity()->id_company);

        if ($id_user > 0) {

            $this->view->is_public = true;
            $this->view->user = $this->_model->getUserById($id_user);
            if (!empty($this->view->user)) {
                $this->view->positions = $this->_model->getUserPositions($id_user);
                $this->view->skills = $this->_model->getUserSkills($id_user);
                $this->view->education = $this->_model->getUserEducation($id_user);
                $this->view->certifications = $this->_model->getUserCertifications($id_user);
                $this->view->points = $this->_model->getUserScoringPoints($id_user);

                $t['lastname'] = $this->view->translate->_('Lastname');
                $t['firstname'] = $this->view->translate->_('Firstname');
                $t['recommender'] = $this->view->translate->_('recommender');
                $t = json_encode($t);

                $this->view->profile_js = "<script>
                var trans_key = $t;  
            </script>";

            } else {
                throw new Zend_Controller_Action_Exception('No user found', 404);
            }
        } else if ($this->_model->_user->id > 0) {
            $user = $this->_model->getUserById($this->_model->_user->id);
            $user['signature'] = $signature;
            $signature = $this->_model->getSignatureByUserId($user['id']);
            $this->view->user = $user;
            $this->view->positions = $this->_model->getUserPositions();
            $this->view->skills = array_merge($this->_model->getUserSkills(), $this->_model->getCustomUserSkills());
            $this->view->education = $this->_model->getUserEducation();
            $this->view->certifications = $this->_model->getUserCertifications();

            $this->view->alerts = $this->_model->getUserAlerts();
            foreach ($this->view->alerts as $k => &$v) {
                $title = $v['title'];
                $condition = unserialize($v['conditions']);
                $skills = $condition['skills'];
                $departments = $condition['departments'];
                $cities = $condition['cities'];
                $v['alert_counter'] = $this->_model->getCounterAlertJobs($title, $skills, $departments, $cities);
            };
            $this->view->countries = $this->_model->getCountries();
            $this->view->cities = $this->_model->getCities() + $this->_model->getCustomCities();
            $this->view->params = $this->_params;
            $this->view->company = $this->_model->getCompanyById($this->_model->_user->id_company);
            $this->view->recommendations = $this->_model->getUserRecommendations();
            $this->view->pending_recommendations = $this->_model->getUserPendingRecommendations();
            $this->view->points = $this->_model->getUserScoringPoints() + $this->_model->getUserLeaderBoardsProfilePoints($id_user);
            $this->view->not_applied = $this->_model->getNotAppliedJobs();
            $this->view->viewed_applications = $this->_model->getReccomendedApplicationViews();
            $this->view->hires = $this->_model->getHiredRecommendations();
            $this->view->uploaded_cv = $this->_model->getUploadedCVs();
            $this->view->campaigns = $this->_model->getUserActiveCampaigns();
            $this->view->potential_rewards = $modelPayments->getPotentialRewardsCount();
            $this->view->earned_rewards = $modelPayments->getEarnedRewardsCount();
            $this->view->available_rewards = $modelPayments->_getAvailableRewardsCount();
            $this->view->bounties = $modelPayments->getProfileBounties();
            $this->view->languages = $this->_model->collecAlltUserLang(Zend_Auth::getInstance()->getIdentity()->id);


            $errors = [];
            if ($email_verification == false &&
                is_null($this->_user->facebook_id) &&
                is_null($this->_user->linkedin_id) &&
                is_null($this->_user->google_id)
            ) {
                $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//                print_r($this->view->translate('resend confirmation email link'));die;
                $errors[] = $this->view->translate->_('confirm your email to be able to use your Talentspotting account') . '. 
                    <a class="send-verification-email-class" href="/ajax/index/send-verification?redirect=">(' . $this->view->translate->_('resend confirmation email link'). ')</a>';
            }
//            if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
            $job = new models_Jobs;
            $linkedin_profile = $job->HasLinkedinProfile(Zend_Auth::getInstance()->getIdentity()->id);
            if (empty($this->view->uploaded_cv) && $linkedin_profile == "") {
                $js = "$(document).ready(function(){var status = 1;console.log(status);$('a#restriction').click()})";
//                    $this->view->headScript()->appendScript($js);
                $errors[] = $this->view->translate->_('add your LinkedIn profile or upload your latest CV');
                $errors[] = "<a href=\"/profile\" style='color:#FFF' target=\"_blank\">".$this->view->translate->_('Complete your profile')."</a>";

            }
//                $positions = $this->_model->getUserPositions(Zend_Auth::getInstance()->getIdentity()->id);
//                $educations = $this->_model->getUserEducation(Zend_Auth::getInstance()->getIdentity()->id);
//                $skills = $this->_model->getUserSkillsWithRating(Zend_Auth::getInstance()->getIdentity()->id);
//                if(empty($positions)) {
//                    $errors[] = "add your work experience";
//                }
//                if(empty($educations)) {
//                    $errors[] = "add your latest education";
//                }
//                if(empty($skills)) {
//                    $errors[] = "add your main skills";
//                }


//            }
            $mCompany = new models_Company;
            $is_require_restrict = $mCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);

            if ($is_require_restrict['signin'] == 0) {
                $errors = [];
            }
            $js = "var errors = 1";
            $this->view->errors = $errors;
            if (!empty($errors)) {
                $this->view->headScript()->appendScript($js);
            }
//            $this->view->leaderboards = $this->_model->getOptimisedLeaderboards($this->_user->id_company);
//            $this->view->leaderboards = $modelCompany->getRank();
            if (isset($this->_params['id_campaign']) && $this->_params['id_campaign'] > 0) {
                $this->view->campaignLeaderboards = $this->_model->getCampaignLeaderboards($this->_params['id_campaign']);
            }
            $modelCampaigns = new models_Campaign;
            $this->view->virality = $this->_model->getViewsById(Zend_Auth::getInstance()->getIdentity()->id);
            $this->view->virality = ($this->view->virality < 1) ? 0 : $this->view->virality;
            $mContent = new models_Content;

            $populated_countries = json_encode([]);
            if (!empty($mContent->getCountries())) {
                $populated_countries = json_encode($mContent->getCountries());
            }
            $selected_country = ($this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_country']);
            $selected_country_text = (string)"'" . $mContent->getCountryById($this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_country'])['title'] . "'";

            $populated_counties = json_encode([]);
            if (!empty($mContent->getCountiesByCountryId($this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_country']))) {
                $populated_counties = json_encode($mContent->getCountiesByCountryId($this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_country']));
            }
            $selected_county = $this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_county'];

            $selected_county_text = "'" . $mContent->getCountiesByCountryId($this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_country'])[$selected_county] . "'";

            $populated_cities = json_encode([]);
            if (!empty($mContent->getCitiesByCountyId($selected_county))) {
                $populated_cities = json_encode($mContent->getCitiesByCountyId($selected_county));
            }
            $selected_cities = $this->_model->getUserDataByEmail(Zend_Auth::getInstance()->getIdentity()->email)['id_city'];
            $selected_cities_text = "'" . $mContent->getCitiesByCountyId($selected_county)[$selected_cities] . "'";

            if (empty($selected_county)) $selected_county = json_encode([]);
            $t['lastname'] = $this->view->translate->_('Lastname');
            $t['firstname'] = $this->view->translate->_('Firstname');
            $t['recommender'] = $this->view->translate->_('recommender');
            $t = json_encode($t);
            $this->view->profile_js = "<script>
                var populated_countries = $populated_countries;
                var selected_country = $selected_country;
                var selected_country_text = $selected_country_text;
                
                var populated_counties = $populated_counties ;
                var selected_county = $selected_county;
                var selected_county_text = $selected_county_text;
                
                var populated_cities = $populated_cities;
                var selected_cities = $selected_cities;
                var selected_cities_text = $selected_cities_text;
                var trans_key = $t;  
            </script>";
            if ($this->getRequest()->isPost()) {

                if ($_FILES['cv']['name'] != "") {
                    $this->_model->uploadCv();
                    $this->_flashMessenger->addMessage("CV uploaded successfully!");
                    $this->_redirect("/profile");
                }
                if (!empty($_POST['company'])) {
                    $_POST['company']['id'] = $this->_model->_user->id_company;
                    $this->_model->saveCompany($_POST['company']);
                } else {
                    $_POST['id'] = $this->_model->_user->id;
                    if (isset($_POST['new_password']) && $_POST['new_password'] == $_POST['new_password_confirm']) {
                        $user = $this->_model->authenticate($this->_model->_user->email, $_POST['original_password']);
                        if (!empty($user)) {
                            $_POST['password'] = $_POST['new_password'];
                        } else {
                            $this->_flashMessenger->addMessage("Password Incorrect!");
                        }
                    }
                    $this->_model->setGamificationCompletedProfileById($_POST['id']);
                    if ($ats) {
                        $signature = $_POST['signature'];
                        unset($_POST['signature']);
                        $this->_model->saveUserSignatue($_POST['id'], $signature);
                    }
                    array_walk_recursive($_POST, function (&$value, $key) {
                        $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
                    });

                    $this->_model->saveUser($_POST);
                }
                $user = (array)$this->_model->_user;
                $this->_model->reauthenticate($user['email']);

                $this->_redirect("/profile");
            }
        } else {
            $this->_redirect("/login");
        }

        $this->view->points = ($this->view->points < 1) ? 0 : $this->view->points;
        $this->view->gamification_points = $this->_model->getUserGamificationPointsProfile($this->_model->_user->id);
        if ($this->view->points > 0) {
            $points = ($this->view->points > 350) ? 350 : $this->view->points;
            $percent = (intval($points / 350 * 100)) . "%";
        } else {
            $percent = "0%";
        }

        foreach ($this->_settings['nicknames'] as $key => $value) {
            if ($key >= $this->view->points) {
                $this->view->nickname = $value;
                break;
            }
        }

        $this->view->completed = $percent;

        $this->view->current_tab = 'resume';

        if (isset($this->_params['a'])) {
            $this->view->current_tab = 'alerts';
        }

        if (isset($this->_params['s'])) {
            $this->view->current_tab = 'settings';
        }

        if (isset($this->_params['c'])) {
            $this->view->current_tab = 'camp';
        }

        if (isset($this->_params['id_campaign'])) {
            $this->view->current_tab = 'camp';
        }

        if (isset($this->_params['r'])) {
            $this->view->current_tab = 'rewards';
        }

//        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
    }

    public function editPositionAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getPositionById($id);
            $data['id_department'] = $this->_model->getDepartmentbyUserId(Zend_Auth::getInstance()->getIdentity()->id);
        }

        $form = new modules_users_forms_EditPosition(array('disableLoadDefaultDecorators' => true, "data" => $data));
        $form->populate($data);

        $this->view->id = $id;
        $this->view->form = $form;
        $this->view->bulk_cv = 0;
        if (isset($this->_params['bulk-cv']) && $this->_params['bulk-cv'] == 1) {
            $this->view->bulk_cv = 1;
        }

        if ($this->getRequest()->isPost()) {
            array_walk_recursive($_POST, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });
            $this->_model->savePosition($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function addSkillAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $form = new modules_users_forms_EditSkill(array('disableLoadDefaultDecorators' => true));

        $this->view->form = $form;

        $this->view->bulk_cv = 0;
        if (isset($this->_params['bulk-cv']) && $this->_params['bulk-cv'] == 1) {
            $this->view->bulk_cv = 1;
        }

        if ($this->getRequest()->isPost()) {
            $this->_model->saveSkill($_POST);
            $kennex = new kennexa_api([], kennexa_api::edit);
            $kennex->updateUSer(kennexa_api::edit);
//            kennexa_api::updateUSer(kennexa_api::edit);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function addLanguageAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $form = new modules_users_forms_EditLanguage(array('disableLoadDefaultDecorators' => true));

        $this->view->form = $form;
        $this->view->bulk_cv = 0;
        if (isset($this->_params['bulk-cv']) && $this->_params['bulk-cv'] == 1) {
            $this->view->bulk_cv = 1;
        }

        if ($this->getRequest()->isPost()) {
//            echo '<pre>';print_r($_POST);die;
            $this->_model->saveLanguge($_POST);

            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function addHobbyAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $form = new modules_users_forms_EditHobby(array('disableLoadDefaultDecorators' => true));

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $this->_model->saveHobby($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function editEducationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getEducationById($id);
        }

        $form = new modules_users_forms_EditEducation(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;
        $this->view->bulk_cv = 0;
        if (isset($this->_params['bulk-cv']) && $this->_params['bulk-cv'] == 1) {
            $this->view->bulk_cv = 1;
        }

        if ($this->getRequest()->isPost()) {
            array_walk_recursive($_POST, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });
            $_POST['id_user'] = $this->_model->_user->id;

            $this->_model->saveEducation($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function addCertificationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getCertificationById($id);
        }

        $form = new modules_users_forms_EditCertification(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            array_walk_recursive($_POST, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });
            $_POST['id_user'] = $this->_model->_user->id;
            $this->_model->saveCertification($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function editAlertAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $data = array();

        if ($id > 0) {
            $data = $this->_model->getAlertById($id);
            $conditions = unserialize($data['conditions']);

            $data['skills'] = @array_keys($conditions['skills']);
            $data['companies'] = @array_keys($conditions['companies']);
            $data['departments'] = @array_keys($conditions['departments']);
            $data['cities'] = @array_keys($conditions['cities']);
            $data['countries'] = @array_keys($conditions['countries']);
            $data['industries'] = @array_keys($conditions['industries']);
        }

        $form = new modules_users_forms_EditAlert(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->id = $id;
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            array_walk_recursive($_POST, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });
            $this->_model->saveAlert($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function editCompanyAlertAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $data = array();

        $company = $this->_model->getCompanyById($this->_model->_user->id_company);
        if ($id > 0) {
            $data['applicants_alert'] = $company['applicants_alert'];
            $data['applicants_status'] = $company['applicants_status'];
            $data['id'] = $id;
        }

        $form = new modules_users_forms_EditCompanyAlert(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);

        $this->view->id = $id;
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $this->_model->saveCompanyAlert($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function deletePositionAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "user_positions");
        $this->_redirect("/profile");
    }

    public function deleteSkillsAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id_skill = {$id} AND id_user = '{$this->_model->_user->id}'", "user_skills");
        $this->_redirect("/profile");
    }

    public function deleteHobbyAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id_hobby = {$id} AND id_user = '{$this->_model->_user->id}'", "user_hobbies");
        $this->_redirect("/profile");
    }

    public function deleteEducationAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "user_education");
        $this->_redirect("/profile");
    }

    public function deleteCertificationsAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id_certification = {$id} AND id_user = '{$this->_model->_user->id}'", "user_certifications");
        $this->_redirect("/profile");
    }

    public function deleteAlertAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "user_alerts");
        $this->_redirect("/profile");
    }

    public function uploadProfilePhotoAction()
    {
        if ($_FILES['file']['name']) {
            $image = $this->_model->saveImageProfile($this->_model->_user->id);

            echo json_encode(array("success" => $image));
        }

        exit();
    }

    public function uploadCompanyPhotoAction()
    {
        if ($_FILES[0]['name'] != "" && $this->_params['id'] > 0) {
            $image = $this->_model->saveCompanyImage($this->_params['id']);

            echo json_encode(array("success" => $image));
        }

        exit();
    }

    public function linkedinAction()
    {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $options = array(
            'localUrl' => APPLICATION_URL . '/sync-linkedin',
            'callbackUrl' => APPLICATION_URL . '/sync-linkedin',
            'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken',
            'userAuthorizationUrl' => 'https://api.linkedin.com/uas/oauth/authorize',
            'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $consumer = new Zend_Oauth_Consumer($options);

        if (empty($ns->request_token)) {
            $token = $consumer->getRequestToken();
            $ns->request_token = serialize($token);
            $ns->token = $token;
            $consumer->redirect();
            die();
        } else {
            try {
                $token = $consumer->getAccessToken($_GET, unserialize($ns->request_token));
                $ns->token = $token;
                $this->_model->update(array("linkedin_token" => serialize($token)), "id = {$this->_model->_user->id}", "users");
            } catch (Exception $e) {
                $this->_model->update(array("linkedin_token" => ""), "id = {$this->_model->_user->id}", "users");
                $ns->request_token = '';
                $ns->token = '';
            }
        }

        return $this->_redirect('/sync-linkedin-details/' . $slug);
    }


    public function rateCompanyAction()
    {
        $data = explode("|", $this->_params['data']);
        $id_company = $data[0];
        $value = $data[1];
        $id_job = $data[2];

        if ($id_company > 0 && isset($this->_model->_user->id)) {
            $rating = $this->_model->getCompanyRating($id_company);
            $new_rating = ($rating + $value) / 2;

            $this->_model->update(array("rating" => $new_rating), "id = {$id_company}", "companies");
            $this->_model->insert(array("id_user" => $this->_model->_user->id, "action" => 3, "points" => $this->_model->_stats['rate'], 'date' => date("Y-m-d H:i:s"), "id_item" => $id_company), "gamification_points");
            $this->_model->insert(array("id_user" => $this->_model->_user->id, "id_company" => $id_company, "value" => $value, 'date' => date("Y-m-d H:i:s"), "id_job" => $id_job), "companies_rating");

            echo json_encode(array("msg" => $this->translate->_("rating_success")));
        } else {
            echo json_encode(array("msg" => $this->translate->_("rating_error")));
        }

        exit();
    }

    public function viewGraphPointsAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];

        $points = $this->_model->getUserScoringPoints($id);
        $points = ($points < 1) ? 50 : $points;

        $max_points = 350;
        $ratio = (($points > 350) ? 350 : $points) / $max_points;
        $image_height = 387;
        $new_height = intval($image_height * $ratio);
        $difference_height = $image_height - $new_height;

        ///echo $ratio."|".$new_height."|".$difference_height; die();

        $image = new Imagick(ABSOLUTE_PATH . "/public/resources/frontend/images/blue_arrow.png");
        $image->setGravity(Imagick::GRAVITY_CENTER);

        $box = new Imagick();
        $box->newImage(747, $new_height, "#167f8e");
        $box->setImageFormat("jpg");
        $box->setImageBackgroundColor("#ffffff");
        $image->compositeImage($box, Imagick::COMPOSITE_OVER, 0, $difference_height);
        //$image->flattenImages();

        $box = new Imagick(ABSOLUTE_PATH . "/public/resources/frontend/images/white_arrow.png");
        $box->cropImage(747, $new_height, 0, $difference_height);
        $box->setImageFormat("png");
        $box->setImageBackgroundColor("#167f8e");
        $image->compositeImage($box, Imagick::COMPOSITE_OVER, 0, $difference_height);
        $image->flattenImages();

        header("Content-Type: image/png");
        $progress = $image->getImageBlob();
        echo $progress;
        exit();
    }


    public function importCvAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $url = $this->_params['url'];
        if ($url != "") {
            $accepted = false;
            foreach ($this->_settings['cvs_url'] as $link) {
                if (stripos($url, $link) !== false) {
                    $type = $link;
                    $accepted = true;
                }
            }

            if ($accepted === false) {
                $message = array("msg" => "error", "error" => $this->translate->_("cv_url_not_accepted"));
            } else {
                $method = "import{$type}";
                $this->_model->$method($url);
                $message = array("msg" => "ok", "mode" => "redirect", "url" => "/profile");
            }

            echo json_encode($message);
            die();
        }
    }


    public function confirmAccountAction()
    {
        $hash = $this->_params['hash'];
        $user = $this->_model->getUserByHash($hash);

        if (empty($user)) {
            $this->_flashMessenger->addMessage($this->translate->_("invalid-hash"));
            $this->_redirect('/login');
        } else {
            $this->_model->update(array("status" => 1, "hash" => ""), "id = '{$user['id']}'", "users");
            $this->_model->authenticateLinkedin($user['email']);
            $this->_redirect('/profile');
        }
    }

    public function bountiesAction()
    {
        $modelPayments = new models_Payments;
        $modelContent = new models_Content;

        $id_bounty = $this->_params['id'];
        $this->view->bounty_history = $modelPayments->getBountyHistory();
        $this->view->current_bounties = $modelPayments->getCurrentBounties();
        $this->view->documents = $modelContent->getBountyDocuments();
        $this->view->bounty_files = $modelPayments->getBountiesUploadedFiles();

        if ($this->getRequest()->isPost()) {
            $modelPayments->uploadFilesOnBounty($id_bounty);
            $this->_redirect("/user-bounties");
        }

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }

    public function removecvAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isPost()) {
            $user_id = (int)$_REQUEST['id'];
            $cv_id = (int)$_REQUEST['cv'];

            $result = $this->_model->removeUserCvByIDs($user_id, $cv_id);
            if ($user_id != Zend_Auth::getInstance()->getIdentity()->id) {
                $msg = "Restricted Action";
                http_response_code(203);
            } else {

                switch ($result) {
                    case(1):
                        $msg = "Cv successfully removed";
                        http_response_code(200);
                        break;
                    case(0):
                        http_response_code(201);
                        $msg = "Cv Not Found";
                        break;
                }
            }

            echo json_encode(['message' => $msg]);
        }

        die;
    }

    public function restrictionAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $form = new modules_users_forms_Linkedin;
        $this->view->form = $form;
    }

    public function removeUserPositionsAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $id = $this->_params['id'];
            $this->_model->removeUserPositions($id);
        }
        die;
    }

    public function removeSkillAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $id = $this->_params['id'];
            $this->_model->removeUserSkill($id);
        }
        die;
    }

    public function removeLanguageAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $id = $this->_params['id'];
            $this->_model->removeUserLanguage($id);
        }
        die;
    }

    public function removeCertificationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $id = $this->_params['id'];
            $this->_model->removeUserCertification($id);;
        }
        die;
    }

    public function removeEducationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $id = $this->_params['id'];
            echo $id;
            $this->_model->removeUserEducation($id);;
        }
        die;
    }

    public function linkedinProfileAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = Zend_Auth::getInstance()->getIdentity()->id;
        $linkedin = $this->_model->getLinkedinProfile($id);
        $form = new modules_users_forms_Linkedin;
        $this->view->form = $form;
        $form->populate($linkedin);
    }

    public function jobalertAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($_REQUEST) {
//            $this->_helper->viewRenderer->setNoRender(true);
            $data = $_REQUEST['data'];
            $template = "";
            foreach ($data as $k => $v) {
                $template .= '<div>'
                    . '<a class="joburi" href="' . WebApp_Tools::createJobSlug($v) . '">' . $v['title'] . '</a>'
                    . '</div>';
            }
            echo $template;
        }
    }

    public function ajaxUploadAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $file = $this->_model->uploadAjaxCv();
//        echo '<pre>';
//        print_r($_FILES);
        print_r(json_encode($file));

        die();
    }


    public function bulkCvUploadAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $file = $this->_model->uploadCvNoId();
//        echo '<pre>';
//        print_r($_FILES);
        print_r(json_encode(['path' => $file]));

        die();
    }


    public function uploadProcessAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $modelJob = new models_Jobs;
        $modelCompany = new models_Company;
        if ($this->getRequest()->isPost()) {

            $password = $this->_model->generateStrongPassword();
            $hashed_password = sha1($password);
            $job_url = $this->_params['job_url'] . '&source=' . md5("website");
            $id_recommender = $this->_params['id_recommender'];
            $id_job = $this->_params['id_job'];
            $full_name = $this->_params['full_name'];
            $first_name = explode(" ", $full_name)[0];
            $last_name = explode(" ", $full_name)[1];
            $mail = $this->_params['mail'];
            $file = $_FILES['file'];
            $id_company = Zend_Auth::getInstance()->getIdentity()->id_company;

            $job = $modelJob->getJobById($id_job);

            $data = ['password' => $hashed_password, "email" => $mail, "status" => 1, "type" => 9, "id_parent" => $id_recommender, "firstname" => $first_name, "lastname" => $last_name, 'id_company' => $id_company, 'created' => date("Y-m-d H:i:s")];
            $track = uniqid();
            $user_exist = $this->_model->getUserDataByEmail($mail);
            if ($user_exist) {
                $error = "Your candidate already has an account";
                $this->_flashMessenger->addMessage($error);
                $this->redirect($this->_params['job_url']);
                exit();
            }
            $id = $this->_model->uploadCandidateCv($data);
            $newRating = ['id_user' => $id,
                'id_recommender' => $id_recommender,
                'message' => 'New Applicant',
                'date' => date('Y-m-d H:i:s'),
                'id_company' => $id_company,
                'id_job' => $id_job
            ];
            $this->_model->setInitalRate($newRating);
            $loggedUser = Domain_User_UserFactory::createInstance($this->_user->id);
            if ($id) {
                $source = $this->_model->getChannelByTitle('website');
                $candidate = $this->_model->getUserById($id);
                $this->_model->uploadFriendCv($id);
                $this->_model->setRecommendationUploadCv(['id_recommender' => $id_recommender, "id_user" => $id, 'id_job' => $id_job, 'email' => $mail, 'track' => $track, "date" => date("Y-m-d H:i:s")]);
                $this->_model->setApplicationuploadCandidateCv(['id_job' => $id_job,
                    'id_user' => $id,
                    'id_recommender' => $id_recommender,
                    "date" => date("Y-m-d H:i:s"), 'status' => 1, 'status_reason' => 0, 'source' => $source]);

                $admins = $this->_model->getAllAdminsCompany();
                $company = $this->_model->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);
                $unique = uniqid(time());
                $id_social = $modelJob->getSocilaMediaIdByType('Upload Cv');
                $job_url = WebApp_Tools::createJobSlug($job) .
                    '?track=' . md5(Zend_Auth::getInstance()->getIdentity()->id) .
                    "&ts=" . $unique .
                    '&source=' . $id_social;
                $_data['job_url'] = $job_url;
                $modelJob->insert(['id_user' => Zend_Auth::getInstance()->getIdentity()->id,
                    'id_job' => $job['id'], 'date' => date('Y-m-d H:i:s'), 'unique' => $unique, 'id_source' => $id_social], 'jobs_share');
                if ($company['notification']) {
                    foreach ($admins as $index => $admin) {
                        $data = ['company_title' => $this->_model->getCompanyById($this->_user->id_company)['title'],
                            'user' => ['firstname' => $this->_user->firstname, 'lastname' => $this->_user->lastname],
                            'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                            'job' => $job,
                            'settings' => $this->_settings,
                            'id_job' => $id_job,
                        ];
                        $to = $admin['email'];
                        $loggedUser->uploadUserCvEmail($data, $to);

                    }
                } else {
                    $data = ['company_title' => $this->_model->getCompanyById($this->_user->id_company)['title'],
                        'user' => ['firstname' => $this->_user->firstname, 'lastname' => $this->_user->lastname],
                        'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                        'job' => $job,
                        'settings' => $this->_settings,
                        'id_job' => $id_job,
                    ];
                    $to = $modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'];
                    $loggedUser->uploadUserCvEmail($data, $to);
                }


                $_candidate = ['company' => $this->_model->getCompanyById($this->_user->id_company),
                    'job' => $job,
                    'password' => $password,
                    'user' => ['firstname' => $this->_user->firstname, 'lastname' => $this->_user->lastname],
                    'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                    'settings' => $this->_settings,
                    'id_job' => $id_job,
                ];
                $_to = $candidate['email'];
                $loggedUser->UploadUserCvEmailForCandidate($_candidate, $_to);
                $this->_flashMessenger->addMessage("success_upload_cv");
                $this->_flashMessenger->addMessage("success");
            } else {
                $user = $this->_model->getUserDataByEmail($mail);
                if ($modelJob->userHasApplied($id_job, $user['id'])) {
                    $text = sprintf($this->translate->_("apply_already"), $modelJob->getJobById($id_job)['title']);
                    $this->_flashMessenger->addMessage($text);
                } else {
                    $this->_model->setApplicationuploadCandidateCv(['id_job' => $id_job, 'id_user' => $user['id'], 'id_recommender' => $user['id_parent'], "date" => date("Y-m-d H:i:s"), 'status' => 0, 'status_reason' => 0, 'source' => 'website']);
                    $this->_flashMessenger->addMessage("success_apply_upload_cv");
                    $this->_flashMessenger->addMessage("success");
                    $_data = ['company_title' => $this->_model->getCompanyById($this->_user->id_company)['title'],
                        'user' => ['firstname' => $this->_user->firstname, 'lastname' => $this->_user->lastname],
                        'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                        'job' => $job,
                        'settings' => $this->_settings,
                        'id_job' => $id_job,
                    ];
                    $to = $modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'];
                    $loggedUser->uploadUserCvEmail($_data, $to);

                    $_data = ['company' => $this->_model->getCompanyById($this->_user->id_company),
                        'job' => $job,
                        'password' => $password,
                        'user' => ['firstname' => $this->_user->firstname, 'lastname' => $this->_user->lastname],
                        'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                        'settings' => $this->_settings,
                        'id_job' => $id_job,
                    ];
                    $to = $user['email'];


                    $loggedUser->UploadUserCvEmailForCandidate($_data, $to);
                }
            }
            $this->redirect($this->_params['job_url']);
        }

    }

    public function facebookSignupAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $code = $this->_params['code'];
        $site = str_replace("http:", "https:", $this->_settings['sitename']);
        $graph_url = "https://graph.facebook.com/oauth/access_token?client_id={$this->_settings['facebook_edin']['api_id']}&redirect_uri={$site}/users/index/facebook-signup&client_secret={$this->_settings['facebook_edin']['api_secret']}&code=$code";
        $data = $this->_model->call($graph_url);
        // access token and expire
        $data_array = explode("&", $data);
        $access_token = json_decode($data_array[0], true)['access_token'];
        $url = "https://graph.facebook.com/me?fields=email,name,id,picture,work&access_token=$access_token";
        $response = file_get_contents($url);
        $response = json_decode($response);
        $exist = $this->_model->getUserDataByEmail($response->email);
        if (!empty($exist)) {
            if ($exist['facebook_id'] === $response->id) {
                $this->_model->registerWithFacebook($response);
                $session_url = new Zend_Session_Namespace('url');
                $apply_auto = new Zend_Session_Namespace('apply_direct');
                $id_job = $apply_auto->id_job;
                if (($id_job) > 0) {
                    $mJob = new models_Jobs;
                    $job = $mJob->getJobById($id_job);
                    $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                    $mJob->apply($logged_user, $job);
                    Zend_Session::namespaceUnset('apply_direct');
                }
                if ($session_url->url && $session_url->url != "") {
                    if ($exist['type'] == 9) $this->_redirect($session_url->url);
                }
                $this->_redirect("/profile");
            } else {
//                $msg = $this->translate->_("account-exists");
                $msg = "This email is already registered. Please type in email & password";
                if ($exist['google_id'] != "") {
                    $msg = "account exist with gmail account";
                }
                if ($exist['linkedin_id'] != "") {
                    $msg = "account exist with linkedin account";
                }
                $this->_flashMessenger->addMessage($msg);
                $url_session = new Zend_Session_Namespace('url');
                if ($url_session->url != "") {
                    $url = $url_session->url;
                    unset($url_session->url);
                    Zend_Session::namespaceUnset('url');
                    $this->_redirect($url);
                }
                $this->_redirect("/register");
            }

        } else {
            $facebook_id = $response->id;
            $facebook_user_image_url = "https://graph.facebook.com/{$facebook_id}/picture?type=large";
            $facebook_user_image_name = time() . "_" . $facebook_id . ".jpg";
            $facebook_user_image_path = ABSOLUTE_PATH . "/public/resources/files/user/" . $facebook_user_image_name;
            try {
                $this->_model->grab_image($facebook_user_image_url, $facebook_user_image_path);
                $response->image = $facebook_user_image_name;
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit();
            }
            $this->_model->registerWithFacebook($response);
            $social = new Zend_Session_Namespace('signin-social');
            $social->source = "facebook";
            $social->response = $response;
            $url_session = new Zend_Session_Namespace('url');
            $apply_auto = new Zend_Session_Namespace('apply_direct');
            $id_job = $apply_auto->id_job;
            if (($id_job) > 0) {
                $mJob = new models_Jobs;
                $job = $mJob->getJobById($id_job);
                $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                $mJob->apply($logged_user, $job);
                Zend_Session::namespaceUnset('apply_direct');
            }

            if ($url_session->url != "") {
                $url = $url_session->url;
                unset($url_session->url);
                Zend_Session::namespaceUnset('url');
                $this->_redirect($url);
            }
            $this->_redirect("/profile");
        }


    }

    public function linkedinSignupAction()
    {
        Zend_Loader::loadFile("linkedin/vendor/autoload.php");
        Zend_Layout::getMvcInstance()->disableLayout();

        $jobModel = new models_Jobs;
        $linkedIn = new Happyr\LinkedIn\LinkedIn($this->_settings['linkedin_edin']['api_key'], $this->_settings['linkedin_edin']['secret']);
        $linkedIn->setHttpClient(new \Http\Adapter\Guzzle6\Client());
        $linkedIn->setHttpMessageFactory(new Http\Message\MessageFactory\GuzzleMessageFactory());

        if ($linkedIn->isAuthenticated()) {
            //we know that the user is authenticated now. Start query the API
            $user = $linkedIn->get('v1/people/~:(id,firstName,lastName,picture-url,public-profile-url,email-address,picture-urls::(original))');
            $access_token = $linkedIn->getAccessToken()->getToken();
            $id_job = $this->_params['job'];

            if ($id_job) {

                $this->_model->setLinkedintoken($access_token);
                $id_autopost = $this->_model->setJobLinkedinAutopost($id_job);
                if ($access_token) {
                    $linkedIn->setAccessToken($access_token);
                    $job = $jobModel->getJobById($id_job);
                    $url = WebApp_Tools::createJobSlug($job)
                        . "?track=" . md5($this->_user->id)
                        . "&source=" . md5('linkedin');
                    $options = ['json' =>
                        [
                            'visibility' => [
                                'code' => 'anyone'
                            ],
                            'content' => [
                                "title" => $job['title'],
                                "description" => $job['description'],
                                "submitted-url" => $url,
                                "submitted-image-url" => APPLICATION_URL . '/resources/files/companies/original/' . $job['company_logo']
                            ]
                        ]
                    ];
                    $post = $linkedIn->post('v1/people/~/shares', $options);
                    if ($post['updateKey']) {
                        $this->view->success_share = true;
                    }
                }
                $this->_helper->viewRenderer('linkedin-graph');
            } else {
                $this->_helper->viewRenderer->setNoRender(true);
                $exist = $this->_model->getUserDataByEmail($user['emailAddress']);
                if (!empty($exist)) {
                    if ($exist['linkedin_id'] === $user['id']) {
                        $this->_model->registerWithLinkedin($user);
                        $session_url = new Zend_Session_Namespace('url');
                        $apply_auto = new Zend_Session_Namespace('apply_direct');
                        $id_job = $apply_auto->id_job;
                        if (($id_job) > 0) {
                            $mJob = new models_Jobs;
                            $job = $mJob->getJobById($id_job);
                            $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                            $mJob->apply($logged_user, $job);
                            Zend_Session::namespaceUnset('apply_direct');
                        }

                        if ($session_url->url && $session_url->url != "") {
                            if ($exist['type'] == 9) $this->_redirect($session_url->url);
                        }
                        $url_session = new Zend_Session_Namespace('url');
                        if ($url_session->url != "") {
                            $url = $url_session->url;
                            unset($url_session->url);
                            Zend_Session::namespaceUnset('url');
                            $this->_redirect($url);
                        }
                        $this->_redirect("/profile");
                    } else {
//                            $msg = $this->translate->_("account-exists");
                        $msg = "This email is already registered. Please type in email & password";
                        if ($exist['google_id'] != "") {
                            $msg = "account exist with gmail account";
                        }
                        if ($exist['facebook_id'] != "") {
                            $msg = "account exist with facebook account";
                        }
                        $this->_flashMessenger->addMessage($msg);
                        $this->_redirect("/register");
                    }
                } else {
                    $social = new Zend_Session_Namespace('signin-social');
                    $social->source = "linkedin";
                    $social->response = $user;
                    $this->_model->registerWithLinkedin($user);
                    $url_session = new Zend_Session_Namespace('url');
                    $apply_auto = new Zend_Session_Namespace('apply_direct');
                    $id_job = $apply_auto->id_job;
                    if (($id_job) > 0) {
                        $mJob = new models_Jobs;
                        $job = $mJob->getJobById($id_job);
                        $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                        $mJob->apply($logged_user, $job);
                        Zend_Session::namespaceUnset('apply_direct');
                    }

                    if ($url_session->url != "") {
                        $url = $url_session->url;
                        unset($url_session->url);
                        Zend_Session::namespaceUnset('url');
                        $this->_redirect($url);
                    }
                    $this->_redirect("/profile");
                }
            }


        } else {
            $this->_redirect($linkedIn->getLoginUrl());
        }

    }

    public function googleSignupAction()
    {
        session_start();
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Loader::loadFile("Google/autoload.php");

        $client_id = $this->_settings['google_edin']['api_key'];
        $client_secret = $this->_settings['google_edin']['secret'];
        $redirect_uri = "{$this->_settings['sitename']}/users/index/google-signup";


        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");

        $service = new Google_Service_Oauth2($client);

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $client->getAccessToken();
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            exit;
        }

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $access_token = (string)json_decode($_SESSION['access_token'], true)['access_token'];
            $isExpire = $this->_model->call("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=$access_token");
            if (json_decode($isExpire)->error != "") {
                unset($_SESSION['access_token']);
                $authUrl = $client->createAuthUrl();
                $this->_redirect($authUrl);
            }
            $client->setAccessToken($_SESSION['access_token']);
            $user = $service->userinfo->get();
            $exist = $this->_model->getUserDataByEmail($user->email);
            if (!empty($user)) {


                if (!empty($exist)) {
                    if ($exist['google_id'] === $user->id) {

                        $this->_model->registerWithGoogle($user);
                        $session_url = new Zend_Session_Namespace('url');
                        $apply_auto = new Zend_Session_Namespace('apply_direct');
                        $id_job = $apply_auto->id_job;
                        if (($id_job) > 0) {
                            $mJob = new models_Jobs;
                            $job = $mJob->getJobById($id_job);
                            $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                            $mJob->apply($logged_user, $job);
                            Zend_Session::namespaceUnset('apply_direct');
                        }

                        if ($session_url->url && $session_url->url != "") {
                            if ($exist['type'] == 9) $this->_redirect($session_url->url);
                        }
                        $url_session = new Zend_Session_Namespace('url');
                        if ($url_session->url != "") {
                            $url = $url_session->url;
                            unset($url_session->url);
                            Zend_Session::namespaceUnset('url');
                            $this->_redirect($url);
                        }
                        $this->_redirect("/profile");
                    } else {

//                        $msg = $this->translate->_("account-exists");
                        $msg = "This email is already registered. Please type in email & password";
                        if ($exist['linkedin_id'] != "") {
                            $msg = "account exist with linkedin account";
                        }
                        if ($exist['facebook_id'] != "") {
                            $msg = "account exist with facebook account";
                        }
                        $this->_flashMessenger->addMessage($msg);
                        $url_session = new Zend_Session_Namespace('url');
                        $apply_auto = new Zend_Session_Namespace('apply_direct');
                        $id_job = $apply_auto->id_job;
                        if (($id_job) > 0) {
                            $mJob = new models_Jobs;
                            $job = $mJob->getJobById($id_job);
                            $logged_user = (array)Zend_Auth::getInstance()->getIdentity();
                            $mJob->apply($logged_user, $job);
                            Zend_Session::namespaceUnset('apply_direct');
                        }

                        if ($url_session->url != "") {
                            $url = $url_session->url;
                            unset($url_session->url);
                            Zend_Session::namespaceUnset('url');
                            $this->_redirect($url);
                        }
                        $this->_redirect("/register");
                    }
                } else {
                    $social = new Zend_Session_Namespace('signin-social');
                    $social->source = "google";
                    $social->response = $user;
                    $this->_model->registerWithGoogle($user);
                    $this->_redirect("/profile");
                }
            }
        } else {
            $authUrl = $client->createAuthUrl();
            $this->_redirect($authUrl);
        }
    }

    public function linkedinConnectionsAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $file = $_FILES['file'];
        $file_name = $this->_model->uploadCsv($file);

//        $file_path = ABSOLUTE_PATH.'/public/resources/files/import/'.$file_name;
        $file_path = APPLICATION_PATH . '/storage/cv/' . $file_name;
        if (file_exists($file_path)) {
            $csv = array_map('str_getcsv', file($file_path));
            if (!file_exists($file_path)) throw new Exception ('File Dosent exist');
            if (empty($csv)) throw new Exception ('Connections are empty');
            $this->_model->importLinkedinconnections($csv);
        }

        $this->_redirect('/profile');
    }

    public function importLinkedinAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function getRankAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $modelCompany = new models_Company();
        $rank = $modelCompany->getRank();
        $place = $rank['place'];
        $position = "";
        switch (true) {
            case($place == 1):
                $position .= " st";
                break;
            case($place == 2) :
                $position .= " nd";
                break;
            case($place == 3):
                $position .= " rd";
                break;
            default:
                $position .= " th";
        }
        echo json_encode([
            'place' => $place,
            'position' => $position
        ]);
        die;
    }

    public function linkedinGraphAction()
    {

        Zend_Loader::loadFile("linkedin/vendor/autoload.php");
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer('profile');
        $jobModel = new models_Jobs;
        $linkedIn = new Happyr\LinkedIn\LinkedIn($this->_settings['linkedin_edin']['api_key'], $this->_settings['linkedin_edin']['secret']);
        $linkedIn->setHttpClient(new \Http\Adapter\Guzzle6\Client());
        $linkedIn->setHttpMessageFactory(new Http\Message\MessageFactory\GuzzleMessageFactory());

        if ($linkedIn->isAuthenticated()) {
            //we know that the user is authenticated now. Start query the API
            $user = $linkedIn->get('v1/people/~:(id,firstName,lastName,picture-url,email-address,picture-urls::(original))');
            $access_token = $linkedIn->getAccessToken()->getToken();
            $id_job = $this->_params['job'];
            $this->_model->setLinkedintoken($access_token);
            $id_autopost = $this->_model->setJobLinkedinAutopost($id_job);
            if ($access_token) {
                $linkedIn->setAccessToken($access_token);
                $job = $jobModel->getJobById($id_job);
                $url = WebApp_Tools::createJobSlug($job)
                    . "?track=" . md5($this->_user->id)
                    . "&source=" . md5('linkedin');
                $options = ['json' =>
                    [
                        'visibility' => [
                            'code' => 'anyone'
                        ],
                        'content' => [
                            "title" => $job['title'],
                            "description" => $job['description'],
                            "submitted-url" => $url,
                            "submitted-image-url" => APPLICATION_URL . '/resources/files/companies/original/' . $job['company_logo']
                        ]
                    ]
                ];
                $post = $linkedIn->post('v1/people/~/shares', $options);
                if ($post['updateKey']) {
                    $this->view->success_share = true;
                }

            }

        } else {
            $this->_redirect($linkedIn->getLoginUrl());
        }
    }


    public function chatAction()
    {
        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $db_name = $bootstrap['resources']['Db']['params']['dbname'];
        $me = Zend_Auth::getInstance()->getIdentity();
        $me->sitename = $this->_settings['sitename'];
        $me->db_name = $db_name;
        $this->view->me = base64_encode(json_encode($me));
        $arry_user = (array)$me;
        $this->view->minimize_me = '<script> var minimize_me = ' . json_encode($arry_user) . '  ;</script>';
    }

    public function getChatTemplateAction()
    {
        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $db_name = $bootstrap['resources']['Db']['params']['dbname'];
        $this->_helper->viewRenderer->setNoRender(true);
        $chat = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR;
        $view = new Zend_View();
        $view->setScriptPath($chat);
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $data['id_user'] = $this->_params['id_user'];
        $data['me'] = $this->_user->id;
        $data['container'] = $db_name;
        $data['sitename'] = $this->_settings['sitename'];

        $sql = $this->_model->select()
            ->from(['c' => 'conversations'], ['*',
                "(SELECT u.firstname FROM users u WHERE u.id='{$data['id_user']}') as user_firstname",
                "(SELECT u.lastname FROM users u WHERE u.id='{$data['id_user']}') as user_lastname",
                "(SELECT u.image FROM users u WHERE u.id='{$data['id_user']}') as user_image",
                "(SELECT u.firstname FROM users u WHERE u.id='{$data['me']}') as me_firstname",
                "(SELECT u.lastname FROM users u WHERE u.id='{$data['me']}') as me_lastname",
                "(SELECT u.image FROM users u WHERE u.id='{$data['me']}') as me_image"])
            ->where('(c.from_user="' . $data['me'] . '" AND to_user="' . $data['id_user'] . '") 
            || (c.from_user="' . $data['id_user'] . '" AND to_user="' . $data['me'] . '")')
            ->order('c.date DESC')->limit(4);
        $data['history'] = array_reverse($this->_model->fetchAll($sql));
        $bodyData = $partialHelper->partial('chat.phtml', $data);
        echo($bodyData);
        die;
    }

    public function saveChatAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $data['from_user'] = $this->_params['data']['from']['id'];
        $data['to_user'] = $this->_params['data']['to'];
        $data['message'] = $this->_params['data']['message'];
        $data['date'] = date('Y-m-d H:i:s');
        $this->_model->insert($data, 'conversations');
        die;

    }

    public function getConversationAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $data['id_user'] = $this->_params['id_user'];
        $counter = $this->_params['counter'];
        $data['me'] = $this->_user->id;
        $start = $counter * 6;
        $sql = $this->_model->select()
            ->from(['c' => 'conversations'], ['*',
                "(SELECT u.firstname FROM users u WHERE u.id='{$data['id_user']}') as user_firstname",
                "(SELECT u.lastname FROM users u WHERE u.id='{$data['id_user']}') as user_lastname",
                "(SELECT u.image FROM users u WHERE u.id='{$data['id_user']}') as user_image",
                "(SELECT u.firstname FROM users u WHERE u.id='{$data['me']}') as me_firstname",
                "(SELECT u.lastname FROM users u WHERE u.id='{$data['me']}') as me_lastname",
                "(SELECT u.image FROM users u WHERE u.id='{$data['me']}') as me_image"])
            ->where('(c.from_user="' . $data['me'] . '" AND to_user="' . $data['id_user'] . '") 
            || (c.from_user="' . $data['id_user'] . '" AND to_user="' . $data['me'] . '")')
            ->order('c.date DESC')
            ->limit(6, $start);
        $result = array_reverse($this->_model->fetchAll($sql));

        $chat = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR;
        $view = new Zend_View();
        $view->setScriptPath($chat);
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);
        $data['history'] = array_reverse($this->_model->fetchAll($sql));
        $data['sitename'] = $this->_settings['sitename'];
        $bodyData = $partialHelper->partial('chat-conversation.phtml', $data);
        $status = true;
        if (empty($data['history'])) $status = false;
        $return['preview'] = $bodyData;
        $return['load'] = $status;
        echo json_encode($return, true);
        die;
    }

    public function applicationHistoryAction()
    {
        $this->view->job_application_history = $this->_model->getUserApplications();
        $this->view->my_viewed_applications = $this->_model->getApplicationViews();
    }

    public function recommendationsAction()
    {

        $this->view->recommendations = $this->_model->getUserRecommendations();
        $this->view->viewed_applications = $this->_model->getReccomendedApplicationViews();
        $this->view->hires = $this->_model->getHiredRecommendations();

    }


    public function leaderboardAction()
    {

    }

    public function alertsAction()
    {
        $this->view->alerts = $this->_model->getUserAlerts();
    }

    public function rewardsAction()
    {
        $modelPayments = new models_Payments;
        $this->view->potential_rewards = $modelPayments->getPotentialRewardsCount();
        $this->view->earned_rewards = $modelPayments->getEarnedRewardsCount();
        $this->view->available_rewards = $modelPayments->_getAvailableRewardsCount();
        $this->view->bounties = $modelPayments->getProfileBounties();
    }

    public function accountAction()
    {

        if ($this->getRequest()->isPost()) {
            if ($_FILES['cv']['name'] != "") {
                $this->_model->uploadCv();
                $this->_flashMessenger->addMessage("CV uploaded successfully!");
                $this->_redirect("/profile");
            }
            if (!empty($_POST['company'])) {
                $_POST['company']['id'] = $this->_model->_user->id_company;
                $this->_model->saveCompany($_POST['company']);
            } else {
                $_POST['id'] = $this->_model->_user->id;
                if (isset($_POST['new_password']) && $_POST['new_password'] == $_POST['new_password_confirm']) {
                    $user = $this->_model->authenticate($this->_model->_user->email, $_POST['original_password']);
                    if (!empty($user)) {
                        $_POST['password'] = $_POST['new_password'];
                    } else {
                        $this->_flashMessenger->addMessage("Password Incorrect!");
                    }
                }
                $this->_model->setGamificationCompletedProfileById($_POST['id']);

                if ($ats = 1) {
                    $signature = $_POST['signature'];
                    unset($_POST['signature']);
                    $this->_model->saveUserSignatue($_POST['id'], $signature);
                }
                array_walk_recursive($_POST, function (&$value, $key) {
                    $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
                });

                $this->_model->saveUser($_POST);
            }
            $user = (array)$this->_model->_user;
            $this->_model->reauthenticate($user['email']);

            $this->_redirect("/profile");
        }
    }

}
