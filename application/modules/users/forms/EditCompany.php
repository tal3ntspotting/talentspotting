<?php
class modules_users_forms_EditCompany extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'email', 'id_city', 'id_country', 'title', 'description', 'long_description', 'phone', 'status', 'id', 'logo', 'linkedin_id', 'company_type', 'specialities', 
                               'id_industry', 'founded', "cover_1", "cover_2", "cover_3", "cover_4", "redir");
		parent::__construct($options);
        
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-company.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'title':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;
                    
                    case 'linkedin_id':
                    case 'company_type':
                    case 'specialities':
                    case 'founded':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
					break;
                    
                    case 'phone':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '10');
                        $element->setAttrib('pattern', '(\+?\d[- .]*){7,13}');

					break;
                    
                    case 'email':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'email');
                        $element->setAttrib('required', 'required');
					break;
                    
                    case 'status':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';
                        
                        $data = array("" => "Choose") + $this->_settings['company_status'];
                        $element->addMultioptions($data);
                        
                        $element->setAttrib('required', 'required');
					break;
                    
                    case 'id_country':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control change-city';
                        
                        $model = new models_Content;
                        $data = array("" => "Choose") + $model->getCountriesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'id_city':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control changed-city';
                        
                        $model = new models_Content;
                        $data = array("" => "Choose") + $model->getCitiesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'id_industry':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';
                        
                        $model = new models_Content;
                        $data = array("" => "Choose") + $model->getIndustriesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'description':
						$element = new WebApp_Form_Element_Textarea($v);
						$element->class = 'form-control';                                                
                        $element->setAttrib("rows", 3);
                        $element->setAttrib("maxlength", 150);
					break;

                    case 'long_description':
						$element = new WebApp_Form_Element_Textarea($v);
						$element->class = 'form-control';                                                
                        $element->setAttrib("rows", 6);
					break;
                    
                    case 'id':
                    case 'cover_1':
                    case 'cover_2':
                    case 'cover_3':
                    case 'cover_4':
                    case 'logo':
                    case 'redir':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}