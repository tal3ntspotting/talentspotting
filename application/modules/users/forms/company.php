<?php

class modules_users_forms_company extends WebApp_Form {

    protected $fields;
    private $_defaults = array();
    private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

    public function __construct($data = array()) {
        $this->fields = array('id','analytics', 'notification','track_timeline','title', 'contact_person', 'brainspotting_bounty','id_country', 'email', 'id_city', 'phone', 'id_industry', 'description', 'linkedin_id', 'logo','allow_external','allow_candidate_recommend','allow_employee_apply');
        parent::__construct($data);
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/company.phtml",'content' => $data['content'],"how-it-works" => $data['how-it-works'],'about_us' => $data['about_us']))));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];
        $languages = Zend_Registry::get("_LANG_DETAILS");

        foreach ($languages as $lang => $val) {
            $this->sf[] = "value_" . $lang;
        }

        $this->languages = $languages;

        $this->go();
    }

    function go() {
        if (empty($this->fields)) {
            return;
        }

        foreach ($this->fields as $field) {
            switch ($field) {
                case 'id':
                    $element = new WebApp_Form_Element_Hidden($field);
                    break;

//                case 'bank_name':
//                    $element = new WebApp_Form_Element_Text($field);
//                    $element->class = 'form-control required';
//                    $element->setAttrib('required', 'required');
//                    break;
                 case 'notification':
                    $element = new WebApp_Form_Element_Checkbox($field);
                    $element->setAttrib('id', 'checkbox3');
                    $element->class = 'form-control';
                    break;
                case 'title':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                  case 'analytics':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control';
                    break;

                case 'contact_person':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;

                case 'id_country':
                    $element = new WebApp_Form_Element_Select($field);

                    $model = new models_Content();
                    $data = $model->getCountriesSelect();

                    $element->setMultiOptions($data);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    break;

                case 'email':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    break;

                case 'id_city':
                    $element = new WebApp_Form_Element_Select($field);
                    $model = new models_Content();
                    $data = $model->getCitiesSelect();

                    $element->setMultiOptions($data);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    break;
                case 'phone':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    break;
                case 'id_industry':
                    $element = new WebApp_Form_Element_Select($field);

                    $model = new models_Content();
                    $data = $model->getIndustriesSelect();
                    $element->setMultiOptions($data);
                    
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    break;
                case 'description':
                    $element = new WebApp_Form_Element_Textarea($field);
                    $element->class = 'form-control';
                    $element->setAttrib('type', 'text');
                    $element->setAttrib('rows', '5');
                    $element->setAttrib('required', 'required');
                    $element->setAttrib('disabled', 'disabled');
                    break;
                case 'linkedin_id':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control';
                    break;
                case 'logo':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'dropzone';
                    break;
                case 'allow_external':
                    $element = new WebApp_Form_Element_Checkbox($field);
                    $element->setAttrib('id', 'checkbox');
                    $element->class = 'form-control';
                    break;
                case 'allow_candidate_recommend':
                    $element = new WebApp_Form_Element_Checkbox($field);
                    $element->setAttrib('id', 'checkbox1');
                     $element->class = 'form-control';
                    break;
                case 'allow_employee_apply':
                    $element = new WebApp_Form_Element_Checkbox($field);
                    $element->setAttrib('id', 'checkbox2');
                     $element->class = 'form-control';
                    break;
                case 'brainspotting_bounty':
                    $element = new WebApp_Form_Element_Checkbox($field);
                    $element->setAttrib('id', 'brainspotting_bounty');
                    $element->class = 'form-control';
                    break;
                case 'track_timeline':
                    $element = new WebApp_Form_Element_Select($field);
                    $data = $this->_settings['track_timeline'];
                    $element->setMultiOptions($data);                   
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
            }

            //set default values for fields

            if (count($this->_defaults) && array_key_exists($field, $this->_defaults)) {
                $element->setValue($this->_defaults[$field]);
            }

            $this->addElement($element);
        }

        foreach ($this->languages as $lang => $val) {
            if (in_array("value_" . $lang, $this->sf)) {
                $element = new WebApp_Form_Element_Textarea("value_" . $lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }
        }
    }

}
