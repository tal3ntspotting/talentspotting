<?php

class modules_users_forms_EditPosition extends WebApp_Form {

    protected $fields;
    protected $_settings;

    public function __construct($options = array()) {
        $this->fields = array('location', 'is_current', 'company', 'summary', 'title', 'start', 'end', 'id', 'id_industry', 'id_department');
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-position.phtml", "translate" => $translate,"data"=>$options['data']))));


        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->loadElems();
    }

    public function loadElems() {
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'title':
                    case 'company':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
                        break;

                    case 'id_industry':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'Select industry';
                        $model = new models_Content;
                        $data = $model->getIndustriesSelect();

                        $element->addMultioptions(array("" => "Select industry") + $data);
                        break;

                    case 'id_department':
                        $model = new models_Content;
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_departments';
                        $element->placeholder = 'Select department';
                        $model = new models_Content;
                        $data = $model->getDepartmentsSelect();
                        $custom_departments_data = $model->getCustomDepartmentsSelect();
                        if (count($custom_departments_data) > 0) {
                            $data = $data + $custom_departments_data;
                        }
                        $element->addMultioptions($data);
                        break;

                    case 'summary':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('minlength', '10');
                        $element->setAttrib('rows', '3');
                        break;
                    case 'location':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->setAttrib('class', 'form-control');
//                        $element->setAttrib('required', 'required');
                        break;

                    case 'is_current':
                        $element = new WebApp_Form_Element_Checkbox($v);
                        $element->class = '';
                        break;

                    case 'start':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control form-control-inline input-medium default-date-picker';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('minlength', '2');
                        $element->setAttrib('required', 'required');
                        break;

                    case 'end':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control form-control-inline input-medium default-date-picker';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        break;

                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}
