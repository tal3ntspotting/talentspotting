<?php

class modules_users_forms_Billing extends WebApp_Form {

    protected $fields;
    private $_defaults = array();
    private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

    public function __construct($data = array()) {
        $this->fields = array('id', 'company_name', 'bank_name', 'tva_code','iban','registration_number','payment_method','address');

        parent::__construct($options);
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/Billing.phtml"))));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];
        $languages = Zend_Registry::get("_LANG_DETAILS");

        foreach ($languages as $lang => $val) {
            $this->sf[] = "value_" . $lang;
        }

        $this->languages = $languages;

        $this->go();
    }

    function go() {
        if (empty($this->fields)) {
            return;
        }

        foreach ($this->fields as $field) {
            switch ($field) {
                case 'id':
                    $element = new WebApp_Form_Element_Hidden($field);
                    break;

                case 'bank_name':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;

                case 'company_name':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                
                case 'tva_code':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                
                 case 'iban':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                
                 case 'registration_number':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                
//                 case 'payment_method':
//                    $options = array('1' => 'Bank','2'=>'paypal','3'=>'other');
//                    $element = new WebApp_Form_Element_Radio($field);
//                    $element->class = '';
//                    $element->setSeparator('');
////                    $element->setOption('placement','append');
//                    $element->addMultiOptions($options);
//                    break;
                 case 'address':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control required';
                    $element->setAttrib('required', 'required');
                    break;
                
                
            }

            //set default values for fields

            if (count($this->_defaults) && array_key_exists($field, $this->_defaults)) {
                $element->setValue($this->_defaults[$field]);
            }

            $this->addElement($element);
        }

        foreach ($this->languages as $lang => $val) {
            if (in_array("value_" . $lang, $this->sf)) {
                $element = new WebApp_Form_Element_Textarea("value_" . $lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }
        }
    }

}
