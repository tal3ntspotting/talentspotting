<?php
class modules_users_forms_EditHobby extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'id_hobby');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/add-hobby.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'id_hobby':
						$element = new WebApp_Form_Element_Text($v);
						$element->class = 'form-control';
                        $element->setAttrib("data-role", "tagsinput");
                        $element->setAttrib("required", "required");
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}