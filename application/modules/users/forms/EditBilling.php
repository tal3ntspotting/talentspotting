<?php
class modules_users_forms_EditBilling extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'title', 'cui', 'j', 'address', 'bank', 'firstname', 'lastname', 'phone', 'email', 'payment_method', 'id');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-billing.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'title':
                    case 'cui':
                    case 'j':
                    case 'bank':
                    case 'firstname':
                    case 'lastname':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;
                    
                    case 'phone':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'phone');
					break;
                    
                    case 'email':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'email');
					break;
                    
                    case 'address':
						$element = new WebApp_Form_Element_Textarea($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');                        
                        $element->setAttrib('minlength', '10');
                        $element->setAttrib('rows', '3');
					break;
                    
                    case 'payment_method':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';
                        
                        $data = array("" => "Choose", "paypal" => "PayPal", "op" => 'Bank Transfer');
                        $element->addMultioptions($data);                        
                        $element->setAttrib('required', 'required');
					break;                                  
                    
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}