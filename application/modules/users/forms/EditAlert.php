<?php
class modules_users_forms_EditAlert extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'reccurence', 'title', 'skills', 'companies', 'departments', 'cities', 'industries', 'id', 'countries');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-alert.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'title':                    
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;
                    
                    case 'reccurence':                    
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';      
                        
                        $data = array("daily" => "Daily", "weekly" => "Weekly", "monthly" => "Monthly");
                        $element->addMultioptions($data);
					break;
                    
                    case 'skills':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getSkillsSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'companies':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Company;
                        $data = $model->getCompaniesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'departments':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getDepartmentsSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'cities':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getCitiesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'countries':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getCountriesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'industries':
						$element = new WebApp_Form_Element_MultiSelect($v);
						$element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getIndustriesSelect();
                        $element->addMultioptions($data);                                                
					break;
                    
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}