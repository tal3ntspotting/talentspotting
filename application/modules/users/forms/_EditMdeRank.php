<?php
class modules_users_forms_EditMdeRank extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'rank', 'year', 'id_company', 'id');
		parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-mde.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'rank':
                    case 'year':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;                    
                    
                    case 'id_company':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';
                        
                        $model = new models_Company;
                        $data = array("" => "Choose") + $model->getCompaniesSelect();
                        $element->addMultioptions($data);
					break;
                    
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}