<?php
class modules_users_forms_EditCertification extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'company', 'title', 'id');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-certification.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'title':
                    case 'company':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;
                    
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}