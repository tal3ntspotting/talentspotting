<?php

class Modules_Users_Forms_Linkedin extends WebApp_Form {

    protected $fields;

    public function __construct($options = []) {
        $this->fields = ['id', 'linkedin_link'];
        parent::__construct($options);
        $translate = Zend_Registry::get("Zend_Translate");
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/linkedin.phtml", "translate" => $translate))));
    }

    public function init() {
        
        if(!empty($this->fields)) {
            foreach($this->fields as $k => $v) {
                switch($v) {
                    case('linkedin_link'):
                        $element = new WebApp_Form_Element_Text($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'https://www.linkedin.com/in/vasilescuadrian0';
                        $element->setAttrib("required", "required");
                        break;
                    case('id'):
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                }
               
            }
             $this->addElement($element);
        }
        
    }

}

?>
