<?php

class modules_users_forms_EditSkill extends WebApp_Form {

    protected $fields;
    protected $_settings;

    public function __construct($options = array()) {
        $this->fields = array('id_skill','rating','years');
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-skill.phtml", "translate" => $translate))));


        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->loadElems();
    }

    public function loadElems() {
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'id_skill':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->class = 'form-control custom_skills';
                        $element->placeholder = 'Select one or more skills';
                        $element->setAttrib('name','id_skill[]');
                        break;
                    case 'rating':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control ';
                        $element->placeholder = 'Select one or more skills';
                        $model = new models_Content;
                        $data = $model->getRatingSelect();
                        $element->addMultioptions($data);
                        break;
                    case 'years':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->class = 'form-control ';
                        $element->placeholder = 'Years';
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}
