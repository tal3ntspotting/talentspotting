<?php
class modules_users_forms_EditEducation extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'start', 'end', 'title', 'field', 'activities', 'degree', 'id');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-education.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'title':
                    case 'field':
                    
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
					break;
                    
                    case 'start':
                    case 'end':
						$element = new WebApp_Form_Element_Html5($v);
						$element->class = 'form-control default-date-picker';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '4');
					break;
                    
                    case 'activities':
						$element = new WebApp_Form_Element_Textarea($v);
						$element->class = 'form-control';
                        $element->setAttrib('type', 'text');                        
                        $element->setAttrib('minlength', '10');
                        $element->setAttrib('rows', '3');
					break;
                    case "degree" :
                        $element = new WebApp_Form_Element_Select($v);
                        $options = array(0 => "select",
                                         1 => "elementary_school",
                                         2=>"high_school",
                                         3 => "graduate",
                                         4 => "masters",
                                         5 => "mba");
                        $element->class = "form-control";
                                $element->setAttrib("required", "required");
                                $element->addMultiOptions($options);
                                break ;
                                
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}