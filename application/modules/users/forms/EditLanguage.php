<?php

class modules_users_forms_EditLanguage extends WebApp_Form {

    protected $fields;
    protected $_settings;

    public function __construct($options = array()) {
        $this->fields = array('language','rating');
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $this->setDecorators(array('PrepareElements',
            array('ViewScript', array('viewScript' => "index/forms/edit-language.phtml",
                "translate" => $translate))));


        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->loadElems();
    }

    public function loadElems() {
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'language':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->class = 'form-control ';
                        $element->setAttrib('required', 'required');
                        $element->placeholder = 'Language , Ex : English';
                        break;
                    case 'rating':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control ';
                        $model = new models_Content;
                        $data = $model->getRatingSelect();
                        $element->addMultioptions($data);
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}
