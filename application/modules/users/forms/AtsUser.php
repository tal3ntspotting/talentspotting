<?php

class modules_users_forms_AtsUser extends WebApp_Form {

    protected $fields;
    protected $_settings;

    public function __construct($options = array()) {
        $this->fields = array('email','jobs', 'password', 'firstname', 'lastname', 'id_company', 'type', 'phone', 'address', 'gender', 'description', 'is_company_admin', 'id', 'job_type', 'department','description');
        parent::__construct($options);
        $translate = Zend_Registry::get("Zend_Translate");
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/ats-edit.phtml",'translate' => $translate))));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->loadElems();
    }

    public function loadElems() {
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'firstname':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control ats-firstname';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
                        break;
                    case 'lastname':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control ats-lastname';
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('type', 'text');

                        break;

                    case 'phone':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control ats-phone';
                        $element->setAttrib('type', 'text');
                        break;
                    case 'job_type':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        break;
                    case 'department':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_departments_user select2 ats-department';
                        $element->setAttrib("multiple","multiple");
                        $model = new models_Content();
                        $data = $model->getDepartmentsSelect();
                        $custom_departments_data = $model->getCustomDepartmentsSelect();
                        if (count($custom_departments_data) > 0) {
                            $data = $data + $custom_departments_data;
                        }
                        uasort($data,function($a,$b) {
                             if ($a == $b) {
                                return 0;
                            }
                            return ($a < $b) ? -1 : 1;
                        });
                        $element->setMultiOptions($data);
                        break;
                        
                        
                        case 'jobs':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_departments_user select2';
                        $element->setAttrib("multiple","multiple");
                        $model = new models_Jobs;
                        $data = $model->getJobsSelect();
                        $element->setMultiOptions($data);
                        break;

                    case 'email':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control ats-email';
                        $element->setAttrib('type', 'email');
                        $element->setAttrib('required', 'required');
                        break;
                    
                     case 'description':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('rows', '5');

                        break;

                    case 'gender':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';

                        $data = array("" => "Choose", "F" => "Female", "M" => 'Male');
                        $element->addMultioptions($data);

                        break;



                    case 'type':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control ats-type';

                        if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
                            unset($this->_settings['user_types'][3]);
                            unset($this->_settings['user_types'][9]);
                        }

                        $data = array("" => "Choose") + $this->_settings['user_types'];
                        $element->addMultioptions($data);

                        $element->setAttrib('required', 'required');
                        break;

                    case 'id_company':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control select2';

                        $model = new models_Company;
                        $data = array("" => "Choose") + $model->getCompaniesSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'address':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->class = 'form-control';
                        $element->setAttrib("rows", 3);
                        break;

                    case 'password':
                        $element = new WebApp_Form_Element_Password($v);
                        $element->class = 'form-control';
                        $element->setAttrib("autocomplete", 'off');
                        break;

                    case 'is_company_admin':
                        $element = new WebApp_Form_Element_Checkbox($v);
                        break;

                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}

