<?php
class modules_users_forms_EditCompanyAlert extends WebApp_Form
{
	protected $fields;
    protected $_settings;

	public function __construct($options = array())
    {
		$this->fields = array( 'applicants_alert', 'applicants_status', 'id');
		parent::__construct($options);
		        
        $translate = Zend_Registry::get("Zend_Translate");
        
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/edit-company-alert.phtml", "translate" => $translate))));
        
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {                    
                    case 'applicants_status':                    
                    case 'applicants_alert':
						$element = new WebApp_Form_Element_Select($v);
						$element->class = 'form-control';      
                        
                        $data = array("daily" => "Daily", "weekly" => "Weekly", "monthly" => "Monthly");
                        $element->addMultioptions($data);
					break;
                                        
                    case 'id':
						$element = new WebApp_Form_Element_Hidden($v);
					break;
				}
                
				$this->addElement($element);
			}
		}
	}
}