<?php

class modules_ajax_forms_EditCampaign extends WebApp_Form {

    protected $fields;
    protected $_settings;
    public $company;
    public $_campaign ;

    public function __construct($options = array()) {
        
        $this->fields = array('bounty', 'is_promoted', 'title', 'end', 'message', 'start', 'status', 'id', 'cities', 'jobs', 'departments', 'gamification_actions', 'recommenders', 'duration', 'reward', 'save_type', 'real_save','description');
        $this->company = $options['company'];
        $this->_campaign = new models_Campaign ;
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-campaign.phtml", "translate" => $translate, "settings" => $this->_settings))));

        $this->loadElems();
    }

    public function loadElems() 
    {
        if (isset($this->fields)) 
        {
            foreach ($this->fields as $k => $v) 
            {
                switch ($v) {
                    case 'title':
                    case 'message':
                    case 'bounty':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control form-control-bsp-c';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
                    break;
                    
                    case 'save_type':
                        $element = new WebApp_Form_Element_Hidden($v);                        
                    break;
                    
                    case 'real_save':
                        $element = new WebApp_Form_Element_Submit($v);
                        $element->setAttrib('style', 'display:none');                        
                    break;
                
                    case 'description':
                        $element = new WebApp_Form_Element_Textarea($v);  
                          $element->class = 'form-control form-control-txtarea';
                          $element
                                ->setAttrib('cols', '40')
                                ->setAttrib('rows', '4');
                        break;
                    
                    case 'jobs':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $element->setAttrib("required", "required");
                        
                        $model = new models_Jobs;
                        $data = $model->getJobsSelect();
                        $used_jobs = $model->getRemainedJobsInCampaigns();
                        $remained = (array_diff_key($data,$used_jobs));
                        if(!$_GET['id']) {
                             $element->addMultioptions($remained);
                        }
                        else {
                            $saved_jobs = $this->_campaign->getCampaignJobs($_GET['id']);
                             $element->addMultioptions($saved_jobs + $remained);
                        }
                       
                        break;

                    case 'departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        
                        $model = new models_Content;
                        $data = $model->getDepartmentsSelect();
                        
                        $element->addMultioptions($data);
                        break;

                    case 'cities':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        
                        $model = new models_Content;
                        $data = $model->getCitiesSelect();
                        
                        $element->addMultioptions($data);
                        break;
                    
                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                                       
                    case 'recommenders' :
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        
                        $model = new models_User;
                        $data = $model->getCompanyRecommenders();
                        
                        $element->addMultioptions($data);
                    break;
                    
                    case 'gamification_actions' :
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $element->setAttrib('required', 'required');
                        $model = new models_Content;
                        $data = $model->getGamification();
                        array_walk_recursive($data, function(&$value , $key){
                            return $value =  ucwords(str_replace("_", " ", $value));
                        });
                        $element->addMultioptions($data);
                    break;
                    case 'reward' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'choose';

                        $data = $this->_settings['campaigns_rewards'];
                        $element->addMultioptions($data);
                    break;
                    case 'duration' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control custom-form-control-duration';
                        $element->placeholder = 'choose';
                        $element->setAttrib('required', 'required');
                        $data = $this->_settings['jobs_lifetime'];
                        $element->addMultioptions($data);
                        break;

                    case 'is_promoted':
                        $element = new WebApp_Form_Element_Checkbox($v);
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}
