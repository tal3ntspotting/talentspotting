<?php

class modules_ajax_forms_detail extends WebApp_Form {

    protected $fields;
    protected $_settings;
    public $company;
    public $_cities;
    public $_sub_departments;
    public $_sub_sub_departments;
    public $_counties;

    public function __construct($options = array()) {
        $this->fields = array(
            'id',
            'title',
            'countries',
            'counties',
            'cities',
            'skills',
            'departments',
            'sub_departments',
            'sub_sub_departments',
            'job_headcount',
            'type',
            'career_level',
            'is_promoted',
            'bounty',
            'start',
            'end',
            'bounty_requirements',
            'bounty_file',
            'job_public_image',
            'job_ad_lifetime',
            'requirements',
            'description',
            'recruiters',
            'projects',
            'job_periode',
            'role',
            'bounty_title',
            'bounty_payment_type',
            'id_currency',
//            'id_company',
//            'career'
//            'tagline',
//            'bounty_requirements',
//            'industries', 'id_lang', 'responsabilities',
//            'job_type','jobs','gamification',
//            'reward','duration','recommader','job_ad_lifetime'
            );
        $this->company = $options['company'];
        $this->_cities = $options['cities'];
        $this->_sub_departments = $options['sub_departments'];
        $this->_sub_sub_departments= $options['sub_sub_departments'];
        $this->_counties = $options['counties'];
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->setDecorators(array('PrepareElements', array('ViewScript',
            array('viewScript' => "admin/forms/detail.phtml",
                "translate" => $translate,
                "settings" => $this->_settings,
                "job_params" => $options['job'],
                "company" => $options['company'],
                ))));
        $this->loadElems();
    }

    public function loadElems() {
        $model = new models_Company();
        $company = $model->getCompanyBySubdomainSlug();
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'title':
//                    case 'tagline':
//                    case 'recommader':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->setAttrib('type', 'text');
                        $element->setRequired(true);
//                        $element->addValidators(array(
//                            array('NotEmpty', true),
//                            array('stringLength', false, array(3, 60)),
//                        ));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('stringLength',true,array(3,6000));
                        $element->getValidator('stringLength')
                            ->setMessage($this->translate->_('VALIDATOR_STRINGLENGTH'));
//                        $element->addValidator('Digits',true);
//                         $element->getValidator('Digits')
//                                                    ->setMessage($this->translate->_('VALIDATOR_INTEGER'));

                        break;

                    case 'bounty_requirements':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->addValidator('NotEmpty',true);
                        $element->addValidator('stringLength',true,array(3,60));
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->getValidator('stringLength')
                            ->setMessage($this->translate->_('VALIDATOR_STRINGLENGTH'));

                        break;
                    case 'bounty':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->addValidator('NotEmpty',true);
                        $element->addValidator('Int',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->getValidator('Int')
                        ->setMessage($this->translate->_('VALIDATOR_INT'));

                        break;

                    case 'id_currency':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
//                        $data = $this->_settings['currencies'];
//                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;

//                    case 'job_type':
//                        $element = new WebApp_Form_Element_Select($v);
//                        $element->class = 'form-control';
//
//                        $data = $this->_settings['job_types'];
//                        $element->addMultioptions($data);
//                        break;
                    case 'job_periode':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $data = [0 => 'nedeterminata',1 => 'test'];
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
//                        $element->getValidator('InArray')
//                            ->setMessage($this->translate->_('abc'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));

                        break;

                    case 'job_headcount':
                        $element = new WebApp_Form_Element_Text($v);
                        $element->class = 'form-control form-control-bsp-c';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->addValidator('Digits',true);
                        $element->getValidator('Digits')

                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->setRequired(true);
                        break;

                    case 'career_level':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';

                        $data = $this->_settings['career_levels'];
                        $element->addMultioptions($data);
                        break;

                    case 'description':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->setAttrib('id', 'editor2');
                         $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->class = 'form-control ckeditor';
                        $element->setAttrib('rows', '20');
                        $element->setAttrib('cols', '20');
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('stringLength',true,array(3,6000));
                        $element->getValidator('stringLength')
                            ->setMessage($this->translate->_('VALIDATOR_STRINGLENGTH'));
                        break ;

                    case 'requirements':
                    case 'role':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->setAttrib('id', 'editor1');
                        $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->class = 'form-control ckeditor';
                        $element->setAttrib('rows', '20');
                        $element->setAttrib('cols', '20');
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('stringLength',true,array(3,6000));
                        $element->getValidator('stringLength')
                            ->setMessage($this->translate->_('VALIDATOR_STRINGLENGTH'));

                        break ;

                    case 'start':
                    case 'end':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control form-control-inline input-medium default-date-picker form-control-bsp-c';
                         $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('minlength', '2');
                        break;

                    case 'skills':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_skills';
                         $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $custom_skills_data = $model->getCustomSkillsSelect();
                        $data = $model->getSkillsSelect();
                        if(count($custom_skills_data) > 0 ) {
                            $data = $data + $custom_skills_data;
                        }
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;


                    case 'departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_departments';
                         $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getDepartmentsSelect();
                        $custom_departments_data = $model->getCustomDepartmentsSelect();
                        if(count($custom_departments_data) > 0) {
                            $data = $data + $custom_departments_data ;
                        }
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        break;

                    case 'sub_departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control sub_departments';
                        $element->placeholder = 'choose';
                        $data = [];
                        if(count($this->_sub_departments) > 0)
                        {
                            $model = new models_Content;
                            $data = $model->getSubDepartmentsSelectByIds($this->_sub_departments);
                        }
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        break;

                    case 'sub_sub_departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control sub_sub_departments';
                        $element->placeholder = 'choose';
                        $element->setRegisterInArrayValidator(false);
                        $data = [];
                        if(count($this->_sub_sub_departments) > 0)
                        {
                            $model = new models_Content;
                            $data = $model->getSubSubDepartmentsSelectByIds($this->_sub_sub_departments);
                        }
                        $element->addMultioptions($data);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        break;

                    case 'projects':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control projects_select';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getProjectSelect();
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;



                    case 'countries':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control countries';
                        $element->placeholder = 'choose';
                        $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $model = new models_Content;
                        $data = $model->getCountriesSelect();
                        $custom_cities_data = $model->getCustomCitiesSelect();
                        $element->addMultioptions($data);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));

//                        $validator = new Domain_Job_Validator();
//                        $element->addValidator($validator);
                        break;

                    case 'counties':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control counties';
                        $element->placeholder = 'choose';
                        $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->setRegisterInArrayValidator(false);
                        $data = [];
                        if(count($this->_cities) > 0)
                        {
                            $model = new models_Content;
                            $data = $model->getCountiesSelect();
                        }
                        $element->addMultioptions($data);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;


                    case 'cities':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control city';
                        $element->placeholder = 'choose';
                        $element->setAttrib('required', 'required');
                        $element->setRegisterInArrayValidator(false);
                        $data = [];
                        if(count($this->_cities) > 0)
                        {
                            $model = new models_Content;
                            $data = $model->getCitiesFromSelected($this->_counties);
//                            $data = $model->getCitiesSelect();
                        }

                        $element->setRequired(true);
                        $element->addMultioptions($data);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;

                    case 'recruiters':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control custom_cities';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getRecruiterSelect();
                        $element->addMultioptions($data);
                        $element->setRequired(true);
                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;

                        case 'hiring_manager':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->setRequired(true);
                        $element->setRegisterInArrayValidator(false);
//                        $element->addValidator('Digits',true);
//                        $element->getValidator('Digits')
//                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                            $element->addValidator('Validator',true);
                            $element->getValidator('Validator')
                                ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;

                    case 'industries':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getIndustriesSelect();
                        $element->addMultioptions($data);
                        break;
                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                    case 'type' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $element->setRegisterInArrayValidator(false);
                        $data = array('2' => 'Full Time' ,'1' => 'Part Time');
                        $element->addMultioptions($data);
                        $element->addValidator('Digits',true);
                        $element->getValidator('Digits')
                            ->setMessage($this->translate->_('VALIDATOR_DIGITS'));
                        $element->addValidator('NotEmpty',true);
                        $element->getValidator('NotEmpty')
                            ->setMessage($this->translate->_('VALIDATOR_NOTEMPTY'));
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;
                    case 'career' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->setAttrib('required', 'required');
                        $element->setRequired(true);
                        $element->placeholder = 'choose';
                        $data = $this->_settings['career_levels'];
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        $element->addMultioptions($data);
                        break;
                    case 'job_ad_lifetime' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control custom-form-control-bounty';
                        $element->placeholder = 'choose';
                        $element->setRegisterInArrayValidator(false);
                        break;
                    case 'bounty_payment_type' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->setRegisterInArrayValidator(false);
                        $element->addValidator('Validator',true);
                        $element->getValidator('Validator')
                            ->setMessage($this->translate->_('VALIDATOR_ARRAY_OF_INTEGER'));
                        break;

                    case 'is_promoted':
                        $element = new WebApp_Form_Element_Checkbox($v);
                        break;

                    case 'bounty_title':
                        $element = new WebApp_Form_Element_Text($v);
                        break;



//                    case 'id_lang':
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->class = 'form-control';
//                        $element->placeholder = 'choose';
//                        $data = $this->_settings['languages'];
//
//                        $translate = Zend_Registry::get("Zend_Translate");
//
//                        foreach ($data as $key => $value) {
//                            $data[$key] = $translate->_($value);
//                        }
//
//                        $element->setAttrib('size', 3);
//                        $element->setAttrib('required', 'required');
//                        $element->addMultioptions($data);
//                        break;


//                    case 'jobs' :
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->class = 'form-control select2';
//                        $element->placeholder = 'choose';
//                        $model = new models_Content;
//                        $data = $model->getJobsSelect();
//                        $element->addMultioptions($data);
//                        break;

//                    case 'gamification' :
//                        $element = new WebApp_Form_Element_MultiSelect($v);
//                        $element->class = 'form-control select2';
//                        $element->placeholder = 'choose';
//                        $model = new models_Content;
//                        $data = $model->getGamification();
//                        $element->addMultioptions($data);
//                        break;
//                    case 'reward' :
//                        $element = new WebApp_Form_Element_Select($v);
//                        $element->class = 'form-control';
//                        $element->placeholder = 'choose';
//                        $model = new models_Content;
//                        $data = array('1' => 'Gamification');
//                        $element->addMultioptions($data);
//                        break;
//                    case 'duration' :
//                        $element = new WebApp_Form_Element_Select($v);
//                        $element->class = 'form-control';
//                        $element->placeholder = 'choose';
//                        $model = new models_Content;
//                        $data = array('1' => 'Select Range');
//                        $element->addMultioptions($data);
//                        break;

                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                }

                $this->addElement($element);
            }
        }
    }
    public function setCities($cities)
    {
        $this->_cities = $cities;
    }
    public function setCounties($counties)
    {
        $this->_counties = $counties;
    }

    public function getCities()
    {
        return $this->_cities;
    }

}
