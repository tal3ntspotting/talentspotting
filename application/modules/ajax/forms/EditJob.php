<?php

class modules_ajax_forms_EditJob extends WebApp_Form {

    protected $fields;
    protected $_settings;
    public $company;

    public function __construct($options = array()) {
        $this->fields = array('id_company', 'id_currency', 'start', 'end', 'bounty', 'type', 'promoted', 'id', 'title', 'tagline', 'description', 'requirements', 'bounty_requirements', 'skills', 'cities', 'departments', 'industries', 'id_lang', 'responsabilities', 'career_level', 'job_type','jobs','gamification','reward','duration','recommader');
        $this->company = $options['company'];
        parent::__construct($options);

        $translate = Zend_Registry::get("Zend_Translate");

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-job.phtml", "translate" => $translate, "settings" => $this->_settings))));

        $this->loadElems();
    }

    public function loadElems() {
        if (isset($this->fields)) {
            foreach ($this->fields as $k => $v) {
                switch ($v) {
                    case 'title':
                    case 'tagline':
                    case 'recommader':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('required', 'required');
                        $element->setAttrib('minlength', '2');
                        break;

                    case 'bounty_requirements':
                    case 'bounty':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control';
                        $element->setAttrib('type', 'text');
//                        $element->setAttrib('required', 'required');
                        break;

                    case 'id_currency':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';

                        $data = $this->_settings['currencies'];
                        $element->addMultioptions($data);
                        break;

                    case 'job_type':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';

                        $data = $this->_settings['job_types'];
                        $element->addMultioptions($data);
                        break;

                    case 'career_level':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';

                        $data = $this->_settings['career_levels'];
                        $element->addMultioptions($data);
                        break;

                    case 'description':
                    case 'requirements':
                    case 'responsabilities':
                        $element = new WebApp_Form_Element_Textarea($v);
                        $element->class = 'form-control';
                        $element->setAttrib('rows', '5');
                        $element->setAttrib('minlength', '2');
                        break;

                    case 'start':
                    case 'end':
                        $element = new WebApp_Form_Element_Html5($v);
                        $element->class = 'form-control form-control-inline input-medium default-date-picker';
                        $element->setAttrib('type', 'text');
                        $element->setAttrib('minlength', '2');
                        break;

                    case 'skills':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getSkillsSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'id_company':
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'choose';
                        $model = new models_Company;
                        $data = $model->getCompaniesSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'departments':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getDepartmentsSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'cities':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getCitiesSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'industries':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getIndustriesSelect();
                        $element->addMultioptions($data);
                        break;

                    case 'id_lang':
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'choose';
                        $data = $this->_settings['languages'];

                        $translate = Zend_Registry::get("Zend_Translate");

                        foreach ($data as $key => $value) {
                            $data[$key] = $translate->_($value);
                        }

                        $element->setAttrib('size', 3);
                        $element->setAttrib('required', 'required');
                        $element->addMultioptions($data);
                        break;

                    case 'id':
                        $element = new WebApp_Form_Element_Hidden($v);
                        break;
                    
                    case 'jobs' :
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getJobsSelect();
                        $element->addMultioptions($data);
                        break;
                    
                    case 'gamification' :
                        $element = new WebApp_Form_Element_MultiSelect($v);
                        $element->class = 'form-control select2';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = $model->getGamification();
                        $element->addMultioptions($data);
                        break;
                    case 'reward' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = array('1' => 'Gamification');
                        $element->addMultioptions($data);
                        break;
                    case 'duration' :
                        $element = new WebApp_Form_Element_Select($v);
                        $element->class = 'form-control';
                        $element->placeholder = 'choose';
                        $model = new models_Content;
                        $data = array('1' => 'Select Range');
                        $element->addMultioptions($data);
                        break;

                    case 'promoted':
                        $element = new WebApp_Form_Element_Checkbox($v);
                        $element->class = "lcs_check check";
                        break;
                }

                $this->addElement($element);
            }
        }
    }

}
