<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Ajax_AdminController extends Zend_Controller_Action {
    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    public $translate;
    protected $_flashMessenger;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $this->view->settings = $this->_settings;
    }
    
       public function jobAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = (int) $this->_params['id'];
        $form = new modules_ajax_forms_detail;

        if ($id > 0) {
            $modelJobs = new models_Jobs;
            $job = $modelJobs->getJobById($id);
            
             

            $job['skills'] = $modelJobs->getJobSkills($id);
            $job['cities'] = $modelJobs->getJobCities($id);
            $job['departments'] = $modelJobs->getJobDepartments($id);
            $job['industries'] = $modelJobs->getJobIndustries($id);
            $job['id_lang'] = $modelJobs->getJobLanguages($id);


            $job['skills'] = array_keys($job['skills']);
            $job['cities'] = array_keys($job['cities']);
            $job['departments'] = array_keys($job['departments']);
            $job['industries'] = array_keys($job['industries']);
            $job['id_lang'] = array_keys($job['id_lang']);
            $job['start'] = date("d-m-YY",strtotime($job['start']));
            $job['end'] = date("d-m-YY",strtotime($job['end']));
//            $job['start'] = date("d-m-Y",$start);
//            $job['end'] = date("d-m-Y",$end);
            $form->populate($job);
            if($job['bounty_file'] != "") {
                $this->view->hasPicture = 1 ;
                $this->view->picture = $job['bounty_file'];
                $this->view->job_id = $job['id'];
                $this->view->js = '<script type="text/javascript"> var job_id = '.$job['id'].'</script>' ;
            } else {
                $this->view->hasPicture = 0 ;
            }
            
            $this->view->job = (array) $job;
        }
        $this->view->detail = $form;
    }
}

