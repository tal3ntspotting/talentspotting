<?php

class Ajax_IndexController extends Zend_Controller_Action
{

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    public $translate;
    protected $_flashMessenger;
    protected $_modelJobs;
    protected $_modelContent;

    public function init()
    {
        $this->_params = $this->getRequest()->getParams();
        $this->_modelContent = new models_Content();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();
        $this->_modelJobs = new models_Jobs;
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $this->view->settings = $this->_settings;
    }

    public function getCitiesAction()
    {
        $id_country = $this->_params['id_country'];
        $modelContent = new models_Content;

        $cities = $modelContent->getCitiesSelect($id_country);

        echo json_encode(array("cities" => $cities));
        exit();
    }

    public function getPackagePriceAction()
    {
        $modelPayments = new models_Payments;

        $job_credits = $this->_params['job_credits'];
        $search_credits = $this->_params['search_credits'];

        $package = $modelPayments->getDefaultPackage();

        $amount = 0;
        if ($job_credits > 0) {
            $cv = $this->_settings['costs']['job'] * $job_credits;
            $amount += $cv;
        } else {
            $cv = $this->_settings['costs']['job'] * 1;
        }

        if ($search_credits > 0) {
            $search = $this->_settings['costs']['search'] * $search_credits;
            $amount += $search;
        } else {
            $search = $this->_settings['costs']['search'] * 1;
        }

        echo json_encode(array("cv_val" => "<span>" . $cv . "</span>$", "search_val" => "<span>" . $search . "</span>$", "total" => "Total value: " . $amount . "$", "total_amount" => $amount));
        exit();
    }

    public function getStatusesAction()
    {
        $status = $this->_params['status'];
        echo json_encode($this->_settings['application_status_reason'][$status]);
        exit();
    }

    public function notificationAction()
    {
        $modelJobs = new models_Jobs;
        $id = (int)$this->_params['id'];

        $notification = $modelJobs->getNotificationById($id);

        $modelJobs->update(array("viewed" => 1), "id = {$id}", "notifications");
        $this->_redirect("/{$notification['url']}");
    }

    public function exportCvAction($id = null)
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        ($id == null) ? $id = $this->_params['id'] : $id = $id;
        $modelUser = new models_User;
        $zip_file_name = str_replace(" ", "_", $modelUser->getUserById($id)['firstname'] . "_" . date("Y-m-d H:i:s") . ".zip");
//        $real = ABSOLUTE_PATH . '/public/resources/files/cv';
        $real = APPLICATION_PATH . '/storage/cv/';
        $zip_file = "{$real}/{$zip_file_name}";
        exec("touch $zip_file");
        $file_name = $modelUser->getUserCv($id);
        if (!empty($file_name)) {
            foreach ($file_name as $key => $cv) {
                $file_name = $cv['file'];
                $file_url = APPLICATION_PATH . '/storage/cv/' . $file_name;
//                $file_url = ABSOLUTE_PATH . '/public/resources/files/cv' . $file_name;
                $zip = new ZipArchive;
                if ($zip->open($zip_file, ZipArchive::CREATE)) {
                    $zip->addFile($file_url, $file_name);
                    $zip->close();
                }
            }
        }
//        $generatedCvProfile = $modelUser->saveExportCV($id, ABSOLUTE_PATH . '/public/resources/files/cv');
        $generatedCvProfile = $modelUser->saveExportCV($id, $real);
        $zip = new ZipArchive;
        if ($zip->open($zip_file, ZipArchive::CREATE)) {
            $zip->addFile($generatedCvProfile, "profile_cv.pdf");
            $zip->close();
        }
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=$zip_file_name");
        header("Content-length: " . filesize($zip_file));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile("$zip_file");
        exit();
    }

    public function viewJobsRemoteAction()
    {
        $modelUser = new models_User;
        $modelJobs = new models_Jobs;

        $token = $this->_params['token'];
        $id = $this->_params['id'];

        if ($token) {
            if (!$user = $modelUser->authenticateByToken($token)) {
                if ($job = $modelJobs->getJobById($id)) {
                    $this->_redirect("/job-details/?id={$id}");
                } else {
                    $this->_redirect("/view-jobs");
                }
            } else {
                $this->_redirect("/login");
            }
        } else {
            $this->_redirect("/login");
        }
    }

    public function recommendEmailAction()
    {
        $modelJobs = new models_Jobs;
        $message = $modelJobs->setEmailRecommendation($_POST);
        $this->redirect($_POST['job_url']);
        echo json_encode(array("msg" => ($message == 'error') ? $this->view->translate->_("email_recommendation_error") : $this->view->translate->_("email_recommendation_sent")));
        exit();
    }

    public function connectionsAction()
    {
        $modelUser = new models_User;
        $modelJobs = new models_Jobs;

        $job = $modelJobs->getJobById($this->_params['id']);

        $sorting = array(1 => 'firstname', 2 => 'title', 3 => 'industry', 4 => 'location', 5 => 'relevance');
        if (array_key_exists($this->_params['iSortCol_0'], $sorting)) {

            $params['orderby'] = $sorting[$this->_params['iSortCol_0']];
            $params['order'] = $this->_params['sSortDir_0'];
        } else {
            $params['orderby'] = 'relevance';
            $params['order'] = 'desc';
        }

        $params['search'] = $this->_params['sSearch'];
        $params['job_title'] = $job['title'];

        $connections = $modelUser->getLinkedinConnections($modelUser->_user->id, $params);

        $iTotalRecords = count($connections);

        $iDisplayLength = intval($this->_params['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->_params['iDisplayStart']);
        $sEcho = intval($this->_params['sEcho']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            if (isset($connections[$i]['id'])) {
                $id = $connections[$i]['id'];

                $records["data"][] = array(
                    '<input name="message_id[]" type="checkbox" class="message_check" value="' . $connections[$i]['id'] . '">',
                    $connections[$i]['firstname'] . " " . $connections[$i]['lastname'],
                    $connections[$i]['title'],
                    $connections[$i]['industry'],
                    $connections[$i]['location'],
                    $connections[$i]['relevance'],
                );
            }
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        echo json_encode($records);
        exit();
    }

    public function recommendLinkedinAction()
    {
        $connections = $this->_params['connections'];
        $id = $this->_params['id_job'];

        $modelSocial = new models_Social;
        $message = $modelSocial->recommentByMessage($connections, $id);

        echo json_encode(array("msg" => $message));
        exit();
    }

    public function messageLinkedinAction()
    {
        $connections = $this->_params['connections'];

        $modelSocial = new models_Social;
        $message = $modelSocial->recommendPlatformByMessage($connections);

        echo json_encode(array("msg" => $message));
        exit();
    }

    public function emailInviteAction()
    {
        $connections = $this->_params['connections'];

        $modelSocial = new models_Social;
        $message = $modelSocial->recommendEmail($connections);

        echo json_encode(array("msg" => $message));
        exit();
    }

    public function loadMoreAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $limit = $this->_params['limit'];
        $existing = $this->_params['existing'];
        $filters = unserialize($this->_params['filters']);

        $modelJobs = new models_Jobs;
        $jobs = $modelJobs->getJobs($filters, $existing, $limit);

        $this->view->jobs = $jobs;
    }

    public function loadMoreNewsAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $limit = $this->_params['limit'];
        $existing = $this->_params['existing'];
        $category = $this->_params['category'];

        $modelContent = new models_Content;
        $news = $modelContent->getNews($category, $existing, $limit);

        $this->view->news = $news;
    }

    public function addSkillsAction()
    {
        $data = $this->_params['skills'];

        $skills['_total'] = count($data);
        foreach ((array)$data as $skill) {
            $skills['values'][] = array("skill" => array("name" => $skill));
        }

        $modelJobs = new models_Jobs;
        $message = $modelJobs->setUserSkills($skills, $modelJobs->_user->id);

        echo json_encode(array("msg" => "ok"));
        exit();
    }

    public function getSkillsAction()
    {
        $search = $this->_params['search'];

        $modelContent = new models_Content;
        $skills = $modelContent->searchSkillsAjax($search);

        echo json_encode(array("skills" => $skills));
        exit();
    }

    public function newAlertAction()
    {
        $id = $this->_params['id'];
        $modelJobs = new models_Jobs;
        $modelUser = new models_User;

        $job = $modelJobs->getJobById($id);
        $myAlerts = $modelJobs->getUserAlertsByConditions($modelJobs->_user->id);

        if (!empty($job['skills'])) {
            $conditions['skills'] = $job['skills'];
            if (!in_array(serialize($conditions), $myAlerts)) {
                $modelJobs->insert(array("id_user" => $modelJobs->_user->id, "created" => date("Y-m-d H:i:s"), "conditions" => serialize($conditions), "title" => implode(", ", $job['skills']) . " alert"), "user_alerts");
                $message = "The " . implode(", ", $job['skills']) . " alert has been successfully created";
            } else {
                $message = "You already have an identical alert created.";
            }
        } else {
            $message = "There are no criteria to create an alert with.";
        }

        $url = WebApp_Tools::createJobSlug($job);

        $this->_flashMessenger->addMessage($message);
        $this->_redirect("/{$url}");
    }

    public function updateSlugsAction()
    {
        $modelContent = new models_Content;
        $cities = $modelContent->getCities();

        foreach ($cities as $city) {
            $modelContent->update(array("slug" => WebApp_Tools::CreateAlias($city['title'])), "id = {$city['id']}", "cities");
        }

        exit();
    }

    public function demoAction()
    {
        $model = new models_Content;
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $model->sendContact($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function jobspopupAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $job_model = new models_Jobs;
//        $filters = ($this->_model->_user->type == 2) ? array("id_company" => array($this->_model->_user->id_company)) : array();
        $filters = "";
        $this->view->views = $job_model->getJobsViewsCounted();
        $this->view->applications = $job_model->getJobsApplicationsCounted();
        $this->view->items = $job_model->getJobs($filters);
    }

    public function copyAction()
    {
        $job_model = new models_Jobs;
        $user_model = new models_User;
        $request_body = json_decode(file_get_contents('php://input'));
        $candidateID = array_filter($request_body->canID);
        if (count((array_filter($request_body->jobID))) > 1) {
            die(json_encode(array('success' => 1)));
        }
        if (count((array_filter($request_body->jobID))) == 0) {
            die(json_encode(array('success' => 1)));
        }

        if (count((array_filter($request_body->canID))) == 0) {
            die(json_encode(array('success' => 2)));
        }

        $jobID = array_search('1', array_filter($request_body->jobID));

        $jobs = $job_model->getJobById($jobID);

        foreach ($candidateID as $id => $value) {
            $user = $user_model->getUserById($id);
            if (!empty($job_model->userHasApplied($jobID, $user['id']))) die(json_encode(array('success' => 3)));
            $result = $job_model->apply($user, $jobs, true);
            if ($result == null) {
                die(json_encode(array('success' => false)));
            }
        }
        die(json_encode(array('success' => true)));
    }

    public function notifyAction()
    {
//        $controller = Zend_Controller_Front::getInstance();
        Zend_Layout::getMvcInstance()->disableLayout();
        $view = new Zend_View();
        $output = $this->view->render('index/notify.phtml');
        $this->view->data = $output;
    }

    public function sendAction()
    {
        $data = base64_decode($this->getRequest()->getPost('data', null));
        $candidate_id = $this->getRequest()->getPost('id', null);
        $model = new models_User;
        if ($data != "" && $candidate_id != "")
            $model->sendNotifyCandidateEmail($data, $candidate_id);
        exit();
    }

    public function addAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Campaign;
        $id = $this->_params['id'];

        $data = array();
        if ($id > 0) {
            $data = $model->getCampaignById($id);
        }

        $form = new modules_ajax_forms_EditCampaign(array('disableLoadDefaultDecorators' => true));
        $form->populate($data);
        if ($data['bounty_file'] != "") {
            $this->view->hasPicture = 1;
            $this->view->picture = $data['bounty_file'];
            $this->view->job_id = $data['id'];
            $this->view->js = '<script type="text/javascript"> var id_campaign = ' . $data['id'] . '</script>';
        } else {
            $this->view->hasPicture = 0;
        }

        $this->view->id = $id;
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $_POST['title'] = strip_tags($_POST['title']);
            $_POST['bounty'] = htmlspecialchars(strip_tags($_POST['bounty']), ENT_QUOTES, 'UTF-8');
            $model->save($_POST);
            $this->_redirect("/view-campaigns");
        }
    }

    public function editAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function importAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function userAction()
    {

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = (int)$this->_params['id'];
        $form = new modules_users_forms_EditUser;

        if ($id > 0) {
            $modelUser = new models_User;
            $user = $modelUser->getUserById($id);
            $user['department'] = array_keys($modelUser->getUserDepartmentById($id) + $modelUser->getUserCustomDepartmentById($id));
            $form->populate($user);

            $this->view->user = (array)$user;
        }

        $this->view->form = $form;
    }

    public function jobAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }

        $form = new modules_ajax_forms_detail();
        if ($this->getRequest()->isPost()) {
            $job = $this->getRequest()->getPost();
            $skills = $job['skills'];
            $job['skills'] = explode(',', $skills[0]);
            if (!$form->isValid($job)) {
                $msg = WebApp_Tools::mapError($form);
                echo json_encode(['error' => true, 'message' => $msg]);
                die;
            } else {
                $m = new models_Jobs();

                $id = $m->save($job);
                echo json_encode(['error' => false, 'message' => 'Job added with success', 'id' => $id]);
                die;
            }
        }
//        if($this->getRequest()->isDelete())
//        {
////            $job = $this->getRequest()->();
////            if($form->isValid($job))
////            {
//                echo '<pre>';
//                print_r(file_get_contents("php://input"));
//                die;
////            }
//        }
//        if($this->getRequest()->isGet())
//        {
////            $job = $this->getRequest()->();
////            if($form->isValid($job))
////            {
//            echo '<pre>';
//            print_r(file_get_contents("php://input"));
//            die;
////            }
//        }

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }


        $this->_helper->viewRenderer->setRender('ats/job');
        $id = (int)$this->_params['id'];
        $current_job = $this->_modelJobs->getJobById($id);
        $id_departments = [];
        $sub_departments = [];
        $sub_sub_departments = [];
        $counties_list = [];
        $selected_cities = [];
        $selected_sub_subdertments_ids = [];
        foreach ($current_job['departments'] as $key => $value) {
            $id_departments[] = $value['id'];
        }
        foreach ($current_job['sub_departments'] as $key => $value) {
            $sub_departments[] = $value['id'];
        }
        foreach ($current_job['sub_sub_departments'] as $key => $value) {
            $sub_sub_departments[] = $value['id'];
        }

        foreach ($current_job['counties'] as $key => $value) {
            $counties_list[] = $value['id'];
        }
        $minimal = [];
        foreach ($current_job['cities'] as $key => $value) {
            $selected_cities[] = $value['id'];
            $minimal[$value['id']] = $value['title'];
        }

        $selected_cities = json_encode($selected_cities);

        $job_ad_lifetime_id = $current_job['job_ad_lifetime']['id'] > 0 ? json_encode($current_job['job_ad_lifetime']['id']) : json_encode(1);
        $model = new models_Content;
//        $populated_cities = json_encode($model->getCitiesByCountyByList($counties_list));
        $populated_cities = json_encode($minimal);
//        echo '<pre>';print_r($populated_cities);die;
        $selected_subdertments_ids = json_encode([]);
        if (!empty($id_departments)) {
            $selected_subdertments_ids = json_encode($model->getSubDepartmentsByDepartmentList($id_departments));

        }

        $selected_sub_subdertments_ids = json_encode([]);
        if (!empty($sub_departments)) {
            $selected_sub_subdertments_ids = json_encode($model->getSubSubDepartmentsByDepartmentByList($sub_departments));

        }
        $select_id_departments_current_job = json_encode($id_departments);
        $select_id_sub_departments_current_job = json_encode($sub_departments);
        $select_id_sub_sub_departments_current_job = json_encode($sub_sub_departments);
        $this->view->job_js = "<script> var selected_cities = $selected_cities ; var populate_cities = $populated_cities ;  var job_ad_lifetime_id = $job_ad_lifetime_id ; var sub_sub_deps_selected = $select_id_sub_sub_departments_current_job;var sub_deps_selected = $select_id_sub_departments_current_job ; var deps_selected = $select_id_departments_current_job;  var populate_sub_department = $selected_subdertments_ids;var populate_sub_sub_department = $selected_sub_subdertments_ids</script>";


        $model_company = new models_Company;
        $company = $model_company->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);
        $form_array = ['company' => $company];
        $job = [];
//        $id = 29;
//        if ($id > 0) {
//            $modelJobs = new models_Jobs;
//            $job = (new Domain_Job_Service_GetJobByIDService($id))->execute();
//            $form_array = ['job' => $job,
//                'company' => $company,
//                'cities' =>$job['cities'],
//                'sub_departments' => $job['sub_departments'],
//                'sub_sub_departments' => $job['sub_sub_departments']];
////            $form->populate($job);
////            $form->setCities($job['cities']);
//            if ($job['bounty_file'] != "") {
//                $this->view->hasPicture = 1;
//                $this->view->picture = $job['bounty_file'];
//                $this->view->job_id = $job['id'];
//                $this->view->js = '<script type="text/javascript"> var job_id = ' . $job['id'] . '</script>';
//            } else {
//                $this->view->hasPicture = 0;
//            }
//
//            if ($job['job_public_image'] != "") {
//                $this->view->job_public_image_hasPicture = 1;
//                $this->view->job_public_image = $job['job_public_image'];
//                $this->view->job_id = $job['id'];
//                $this->view->js = '<script type="text/javascript"> var job_id = ' . $job['id'] . '</script>';
//            } else {
//                $this->view->job_public_image_hasPicture = 0;
//            }
//            $this->view->job = (array) $job;
//            $this->view->hasBounty = 0;
//        }
//


        $form_array['counties'] = $job['counties'];

        $form = new modules_ajax_forms_detail($form_array);
        if (count($job) > 0) {
            $form->populate($job);
            $form->setCities($job['cities']);
        }
        if ($job['bounty']) {
            $this->view->hasBounty = 1;
        }
        $this->view->detail = $form;
    }

    public function reminderAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->view->email = $this->_params['email'];
        $id = $this->_params['id'];
        $message = $this->_params['message'];
        $subject = $this->_params['subject'];
        $job_title = base64_decode($this->_params['job']);
        $modelUser = new models_User;
        $modelsContent = new models_Content;

        if ($this->getRequest()->isPost()) {
            $user = $modelUser->getUserDataByEmail($this->_params['email']);
            $userObject = Domain_User_UserFactory::createInstance($user['id']);
            $userObject->sendReminder($message, $job_title, $subject);
//            WebApp_Tools::Debug($_REQUEST);
//            $modelsContent = new models_Content;
//
//            if (is_array($id)) {
//
//                foreach ($id as $_id) {
//                    $email = $modelUser->getUserById($_id)['email'];
//                    $modelsContent->sendReminder(array("email" => $email, "message" => $message, "subject" => $subject));
//                }
//            } else {
//
//                $user = $modelUser->getUserDataByEmail($_POST['email']);
//                if (!in_array($user['id'], $id))
//                    $modelsContent->sendReminder($_POST);
//            }
            $this->_redirect("/users");
//            echo json_encode(array("msg" => "ok"));
//            exit();
        }
    }

    public function disableExpiredJobsAction()
    {
        $modelJobs = new models_Jobs;
        $disabled=$modelJobs->disableExpired();
        $enabled=$modelJobs->enableCreated();


        echo json_encode($disabled);
        echo json_encode($enabled);

        exit();
    }


    public function inactivateJobAction()
    {
        $ids = $this->_params['ids'];

        $modelJobs = new models_Jobs;
        $modelJobs->inactivate($ids);

        echo json_encode($ids);

        exit();
    }

    public function activateJobAction()
    {
        $ids = $this->_params['ids'];

        $modelJobs = new models_Jobs;
        $modelJobs->activate($ids);

        echo json_encode($ids);

        exit();
    }

    public function activateCampaignAction()
    {
        $ids = $this->_params['ids'];

        $modelCampaign = new models_Campaign;
        $modelCampaign->activate($ids);

        echo json_encode($ids);

        exit();
    }

    public function deactivateCampaignAction()
    {
        $ids = $this->_params['ids'];

        $modelCampaign = new models_Campaign;
        $modelCampaign->deactivate($ids);

        echo json_encode($ids);

        exit();
    }

    public function deleteCampaignAction()
    {
        $modelCampaign = new models_Campaign;
        if ($this->getRequest()->isGet()) {
            $ids = $this->_params['id'];
            $modelCampaign->deleteCamp($ids);
        }
        $this->_redirect('/view-campaigns');
        exit();
    }

    public function duplicateCampaignAction()
    {
        $id = $this->_params['id'];

        $modelCampaign = new models_Campaign;
        $modelCampaign->duplicate($id);

        echo json_encode(array("0" => $id));

        exit();
    }

    public function campaignRecommendersAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->view->id = $this->_params['id'];
        $modelUsers = new models_User;
        $this->view->items = $modelUsers->getCompanyRecommenders();
    }

    public function changeRecommenderAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $modelUsers = new models_User;

        $id_bounty = $this->_params['id_bounty'];
        $id_user = $this->_params['id_user'];
        $modelUsers->update(array("id_user" => (int)$id_user[1], "status" => "initiated"), "id = {$id_bounty}", "bounties");

        echo json_encode(array($id_user));
        exit();
    }

    public function boostAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = (int)$this->_params['id'];
        $this->view->id = $id;

        if ($this->getRequest()->isPost()) {
            $modelsContent = new models_Content;
            $modelsContent->sendBoost($_POST);

            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function changeStatusAction()
    {
        $status = $this->_params['status'];
        $id = $this->_params['id'];

        $modelUsers = new models_User;
        $modelUsers->changeStatus($id, $status);
        $kennexa = new kennexa_api(['id_application' => $id], kennexa_api::edit);
        $kennexa->updateApplicationStatus();
        echo json_encode(array("msg" => "ok"));
        exit();
    }

    public function mailAction()
    {
        $url = $this->_params['url'];
        $this->_helper->viewRenderer->setNoRender(true);
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $mail = new WebApp_Mail();
        $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
        $mail->setSubject("New application on test");
        $mail->setBodyFromViewScript("recommend-email.phtml");
        if ($_REQUEST) {
            $mails = $this->_params['email'];
            foreach ($mails as $email) {
                $mail->addTo($email, 'test');
            }
        }

        $mail->send($transport);
        $this->redirect($url);
    }

    public function removePictureAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_job = (int)$this->_params['id_job'];
        $modelJobs = new models_Jobs;
        $result = $modelJobs->removeJobPicture($id_job);
        echo json_encode(array("msg" => $result));
        die;
    }

    public function removeSharePictureAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_job = (int)$this->_params['id_job'];
        $modelJobs = new models_Jobs;
        $result = $modelJobs->removeJobSharePicture($id_job);
        echo json_encode(array("msg" => $result));
        die;
    }

    public function removeCampaignPictureAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $id_campain = (int)$this->_params['id_campaign'];
        $modelCampaign = new models_Campaign;
        $result = $modelCampaign->removeCampaignPicture($id_campain);
        echo json_encode(array("msg" => $result));
        die;
    }

    public function deleteJobsAction()
    {
        $models_jobs = new models_Jobs;
        if ($_REQUEST) {
            $id_job = $this->_params['ids'];
            foreach ($id_job as $id) {
                $models_jobs->delete("id = {$id}", "jobs");
            }
        }

        die;
    }

    public function customCitiesAction()
    {
        $modelJobs = new models_Jobs;
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {
                $source = $this->_params['source'];
                $value = $this->_params['value'];
                $value = strip_tags($value);
                if ($value !== "") {
                    switch ($source) {
                        case "cities":
                            $data = $modelJobs->addCustomCities($value);
                            print_r(json_encode($data));
                            die;
                            break;
                        case "skills":
                            $data = $modelJobs->addCustomSkills($value);
                            print_r(json_encode($data));
                            break;
                        case "departments":
                            $data = $modelJobs->addCustomDepartments($value);
                            print_r(json_encode($data));
                            break;
                    }
                }
            }
        }
        die;
    }

    public function addLinkedinAction()
    {
        if ($this->getRequest()->isPost()) {
            $linkedin = $this->_params['link'];
            $url = $this->_params['url'];
            $userModel = new models_User;
            $userModel->updateLinkedinAjax($linkedin);
            $this->_redirect($url);
        }
        die;
    }

    public function reminderCampaignAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/home');
            exit();
        }
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelUsers = new models_User;
        $id_campaign = $this->_params['id'];
        $total_recommenders = [];
        $result = $modelUsers->getRecommenders($id_campaign);
        foreach ($result as $rec) {
            $total_recommenders[] = $rec;
        }
        if (empty($result)) {
            $users = $modelUsers->getUserByCompany();
            foreach ($users as $index => $rec) {
                $total_recommenders[] = $rec['id'];
            }
        }

        $departments = $modelUsers->getDepartments($id_campaign);
        if (!empty($departments)) {
            foreach ($departments as $department) {
                $data = $modelUsers->getUsersByDepartments($department);
                foreach ($users as $index => $rec) {
                    $total_recommenders[] = $rec['id'];
                }
            }
        }
        $total_recommenders = array_unique($total_recommenders);
        if ($this->getRequest()->isPost()) {
            $subject = $this->_params['subject'];
            $message = $this->_params['message'];
            $modelContent = new models_Content;
            foreach ($total_recommenders as $usr) {
                $email = $modelUsers->getUserById($usr)['email'];
                $modelContent->sendReminder(array("email" => $email, "message" => $message, "subject" => $subject));
            }
            $this->_redirect('/view-campaigns');
        }
    }

    public function customLocationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $session = new Zend_Session_Namespace('flow');
        $session->_CUSTOM_LOCATION = 1;
        if ($this->getRequest()->isXmlHttpRequest()) {

            array_walk_recursive($this->_params, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });

            $UserModel = new models_User;
            $data['id_user'] = $this->_params['user_id'];
            $data['title'] = $this->_params['value'];
            $data['date'] = date("Y-m-d H:i:s");
//			if ($data['id_user']) {
            $result = $UserModel->setUserCustomLocation($data);
            print_r(json_encode($result));
            die;
//			}
        }
        die;
    }

    public function customSkillsAction()
    {
        $session = new Zend_Session_Namespace('flow');
        $session->_CUSTOM_SKILLS = 1;
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isXmlHttpRequest()) {

            array_walk_recursive($this->_params, function (&$value, $key) {
                $value = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8');
            });

            $UserModel = new models_User;
            $data['id_user'] = $this->_params['user_id'];
            $data['title'] = $this->_params['value'];
            $data['date'] = date("Y-m-d H:i:s");
//			if ($data['id_user']) {
            $result = $UserModel->setUserCustomskills($data);
            print_r(json_encode($result));
            die;
//			}
        }
        die;
    }

    public function bountyViaBspAction()
    {
        $companyModel = new models_Company;
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $status = $this->_params['status'];
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        if ($status) {
            $filename = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails' . "/brainspotting_bounty.phtml";
        } else {
            $filename = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails' . "/without_brainspotting_bounty.phtml";
        }

        $handle = fopen($filename, "r");
        $fcontents = fread($handle, filesize($filename));
        fclose($handle);

        $smarty = new Smarty;
        $smarty->assign("company", $companyModel->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['title']);
        $resource = $smarty->fetch('string:' . $fcontents);
        $mail = new WebApp_Mail();
        $mail->addTo("ana@brainspotting.ro");
        $mail->addTo("adrian@talentspotting.com");
        $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
        $mail->setSubject("Bounty Payment Via Brainspotting account");
        $mail->setBodyHtml($resource);
        $mail->send($transport);
        die;
    }

    public function rateAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id_user = (int)$this->_params['user'];
        $id_job = (int)$this->_params['jid'];
        $this->view->user = $this->_modelJobs->getUserReviw($id_user, $id_job);
    }

    public function recommendAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isXmlHttpRequest()) {

            $users = $this->_params['users'];
            $id_job = $this->_params['id_job'];
            $job = $this->_modelJobs->getJobById($id_job);
            foreach ($users as $index => $user) {
                $user = json_decode($user, true);
                if ($user['table'] == 'linkedin_connections') {

                    $this->_modelJobs->sendMassRecommendationLinkedin($user['id_user'], $job);
                } else {
                    $this->_modelJobs->sendMassRecommendation($user['id_user'], $job);
                }

            }
        }
    }

    public function fbGraphAction()
    {
        require APPLICATION_PATH . '/../library/WebApp/Mail/Oempro/libraries/facebook.php';
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $config = array();
        $config['appId'] = '1062377973856382';
        $config['secret'] = '8f8718a3c26a44fb4c83e38a3ff4c099';
        $config['fileUpload'] = false; // optional

        $access_token = $this->_params['access_token'];
        $token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $config['appId'] . "&client_secret=" . $config['secret'] . "&grant_type=fb_exchange_token&fb_exchange_token=" . $access_token;

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_URL, $token_url);
        $contents = curl_exec($c);
        $err = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);

        $paramsfb = null;
        parse_str($contents, $paramsfb);
        $access_token = json_decode($contents)->access_token;
        $userModel = new models_User;
        $companyModel = new models_Company;
        $userModel->setFaceBookAccessToken($access_token);
        $id_autopost = $userModel->setJobAutopost($this->_params['id_job']);
        $comapanyData = $companyModel->getCompanyById(
            Zend_Auth::getInstance()->getIdentity()->id_company
        );
        $job = $this->_modelJobs->getJobById($this->_params['id_job']);

        $image = APPLICATION_URL . '/resources/files/companies/original/' . $comapanyData['logo'];
//        $access_token = "EAAPGOiWZCuH4BADR4v26XHZAw8l4mWk3H3P4gk8EZCAZBqR83iG4MFvzTMhhBQFrRvKOya9nTiotdaTwi27AHgO4YYNrQIvcqLtekx0DNGZC9yfm9unZCe01FUufZBsm1sD1koMV8tLMCwAe8NwXawD11vEW1mv9BMNju4VxGr6nQZDZD";
//        $access_token = "EAACEdEose0cBAMQVQLIn0ZC5RHiHzboemoZAgA5rmssvgqU9OSWLYRlEXLl1RI1aDaF8D6u0IS9RQvmRW9k9pZCuVaRCUpsfFyDoxumkGRxCi0njk5FrXnQ9K1MUMNZAjfSKwr4S8eDhiiyvq5XjJya1yEwyEb4yfvDcsaDd74h63EmsKSH9ZCzZC1PrcNrqymGZB7DydZC5FAZDZD";
//        $access_token = "EAACEdEose0cBAKkHQrH9ICua2sFoCArFAklRlzzZBqjErZC4Ao5XqZAnrHoHl5lMIZCbsLTKww0SL3kPtFjQ0TlSa7dwB0RH2eYauAktppPUz5ZCJSkMA7vjQr4ZCpbTLcQ4WPLowGGG0pTZBlZA0kw3ffZB2wmgRG6T9cvkDwa9AW3PiOTBSryoHZCDcBuujdbwZAO7VXzIhh1FAZDZD";

        $privacy = array(
//            'value' => 'SELF' //private
            'value' => 'EVERYONE' //private
        );

        $config = array();
        $config['appId'] = '1062377973856382';
        $config['secret'] = '8f8718a3c26a44fb4c83e38a3ff4c099';
        $config['fileUpload'] = false; // optional

        $fb = new Facebook($config);

// define your POST parameters (replace with your own values)
        $params = array(
            "access_token" => $access_token,
            "link" => WebApp_Tools::createJobSlug($job) . "?track=" . md5(Zend_Auth::getInstance()->getIdentity()->id) . "&source=" . md5('facebook'),
            "picture" => $image,
            'privacy' => $privacy,
            "name" => $job['title'],
            "caption" => $job['title'],
            "description" => $job['description']
        );


// post to Facebook
// see: https://developers.facebook.com/docs/reference/php/facebook-api/
        try {
            $ret = $fb->api('/me/feed', 'POST', $params);
            $userModel->updateAutoPostDate($id_autopost);
            echo json_encode(['msg' => 'Successfully posted to Facebook']);

        } catch (Exception $e) {
            echo json_encode(['msg' => $e->getMessage()]);
//            echo $e->getMessage();
        }
        die;
    }


    public function kennexaAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        echo 'api';
        die;
    }

    public function linkedinGraphAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userModel = new models_User;
        $access_token = $this->_params['access_token'];
        $id_job = $this->_params['id_job'];
        $userModel->setLinkedintoken($access_token);

        $id_autopost = $userModel->setJobLinkedinAutopost($id_job);

    }

    public function getStatusApplicationsAction()
    {
        $job_model = new models_Jobs;
        $job_model->getAllStatus();
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
//        if(!Zend_Auth::getInstance()->hasIdentity()) exit();
        $id_company = Zend_Auth::getInstance()->getIdentity()->id;
        $applicants = $this->_modelJobs->getJsonResult();
        $cities = [];
        $sources = [];
        $jobs = [];
        if (!empty($applicants)) {
            foreach ($applicants as $index => &$user) {
//            if($user['jobs'] == "" || is_null($user['jobs']) || !isset($user['jobs']))
//            {
//                $user['jobs'] = "-";
//            }
                if ($user['id_city'] > 0) array_push($cities, $user['id_city']);
                array_push($sources, $user['source']);
                array_push($jobs, $user['id_job']);
                $image = $this->_settings['sitename'] . '/resources/files/user/' . $user['image'];
                if (is_null($user['image']) || $user['image'] == "") {
                    $image = $this->_settings['sitename'] . '/resources/admin/images/no-picture.jpg';
                }
                $applied_date = date_create(date('Y-m-d', strtotime($user['updated'])));
                $today = date_create(date('Y-m-d'));
                $diff = date_diff($applied_date, $today);
                $user['days'] = $diff->format("%a");
                if ($user['status'] == 1) {
                    $applied_date = date_create(date('Y-m-d', strtotime($user['date'])));
                    $diff = date_diff($applied_date, $today);
                    $user['days'] = $diff->format("%a");
                }
                $user['image'] = $image;
                $user['date'] = date("d-m", strtotime($user['date']));
                $user['date'] = str_replace('-', '/', $user['date']);
                $user_job_history = $this->_modelJobs->getUserLastPositionsById($user['id_user']);
                $user_recommender = $this->_modelJobs->getUserRecommenderById($user['id_user']);
//            $last_job = "";
//            foreach($user_job_history  as $index => $job)
//            {
//                if($job['is_current']) {
//                    $last_job=$job['title'];
//                    break;
//                }
//
//            }
//            if(!empty($user_job_history) && $last_job == "")
//            {
//                $last_job = $user_job_history[0]['title'];
//            }
//            $user['last_job'] = $last_job;
//
                $user['last_job'] = $user_job_history;
                $user['user_recommender'] = $user_recommender;
            }
        }
        $data['status_applications'] = $applicants;
        $data['status'] = $this->_modelJobs->getSubStatus();
        if (!empty($data['status_applications'])) {
            $data['source'] = $this->_modelJobs->getSocialSource($sources);
            $data['cities'] = $this->_modelJobs->getcities($cities);
            $data['jobs'] = $this->_modelJobs->getJobsSelectFromArray($jobs);
        }
        echo json_encode($data, true);
        exit();
    }

    public function updateStatusAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelUser = new models_User;
        $id_applications = $this->_params['id_application'];
        $id_status = $this->_params['id_status'];
        $id_status_reason = $this->_params['id_sub_status'];
        $send_email = (isset($this->_params['send_email'])) ? $this->_params['send_email'] : 0;
        $send_admin_hired_email = (isset($this->_params['send_email'])) ? $this->_params['send_admin_email'] : 0;
        $send_recommender_hired_email = (isset($this->_params['send_email'])) ? $this->_params['send_recommender_email'] : 0;
        $note = $this->_params['notes'];
        if ($this->getRequest()->isPost()) {
            $userObject = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
            $userObject->changeStatus($id_applications, $send_email, $id_status);

            if ($id_status == 5)
            {
                $note.=':'.$this->_params['start_date'];
            }

            if ($id_status_reason == 22 or $id_status_reason == 12  or $id_status_reason == 6  or $id_status_reason == 17   or $id_status_reason == 29   or $id_status_reason == 33 )
            {
                $job=$this->_params['job'];
                $note.=' - moved to another process';
                $ja=$this->_modelJobs->getJobApplicationById($id_applications);
                //insert new application
                $new_app_data=[];
                $new_app_data['id_job']=$job;
                $new_app_data['id_user']=$ja['id_user'];
                $new_app_data['id_recommender']=$ja['id_recommender'];
                $new_app_data['status']=$id_status;
                $new_app_data['source']=$ja['source'];
                $new_app_data['status_reason']=0;
                $new_app_data['date']=date("Y-m-d H:i:s");

                $this->_modelJobs->insert($new_app_data, "job_applications");
                $new_application = $this->_modelJobs->lastInsertId();
                $modelUser->setjobApplicationStatusHistory($job, $new_application, $id_status, 0, $note);
                $id_status=16;
            }

            $modelUser->setjobApplicationStatusHistory(0, $id_applications, $id_status, $id_status_reason, $note);
            $this->_modelJobs->updateUserStatus($id_applications, $id_status, $id_status_reason);
            $modelUser->changeStatus($id_applications, $id_status, $id_status_reason, $send_admin_hired_email, $send_recommender_hired_email);
            if ($id_status == 5) {
                $hired_date = $this->_params['start_date'];
                $this->_modelJobs->updateUserHiredDate($id_applications, $hired_date);
                $modelUser->DisableJobIfHeadCountReached($id_applications);
                $application = $this->_modelJobs->getJobApplicationById($id_applications);
                $id_job = $application['id_job'];
                $id_user = $application['id_recommender'];
                $jobIsPartOfCampain = $this->_modelJobs->jobIsPartOfCampaign($id_job);
                $id_view = $application['id_view'];
                $hash = $this->_modelJobs->getHashByIdView($id_view)['unique'];
                $action = "successful_hire";
                $this->_model->setGamificationActionPoints($action, $id_job, $id_user);
                if ($jobIsPartOfCampain && $id_user > 0) {
                    $id_campaign = $jobIsPartOfCampain['id'];
                    $campaign_actions = $this->_modelJobs->getGamificationActionsByCampaignId($id_campaign);
                    if (!empty($campaign_actions)) {
                        // 3 - Check  the id of $actions
                        $result = $this->_modelJobs->getActionIDbyName($action);
                        $action_data = $this->_modelJobs->getActionDataByCampainIdActionId($result['id'], $id_campaign);

                        if (empty($action_data)) return;
                        if ($action_data['active']) {
                            if ($this->_model->checkValidation($action_data, $hash)) {
                                $data['id_user'] = $id_user;
                                $data['value'] = $action_data['points'];
                                $data['id_campaign'] = $action_data['id_campaign'];
                                $data['id_job'] = $id_job;
                                $data['id_action'] = $action_data['id'];
                                $data['id_job_view'] = $id_view;
                                $modeStats = new models_Stats;
                                $modeStats->saveCampaignReport($data);
                            }
                        } else {
                            $data['id_user'] = $id_user;
                            $data['value'] = $action_data['points'];
                            $data['id_campaign'] = $action_data['id_campaign'];
                            $data['id_job'] = $id_job;
                            $data['id_action'] = $action_data['id'];
                            $data['id_job_view'] = $id_view;
                            $modeStats = new models_Stats;
                            $modeStats->saveCampaignReport($data);
                        }

//                        $data['id_user'] = $id_user;
//                        $data['value'] = $action_data['points'];
//                        $data['id_campaign'] = $action_data['id_campaign'];
//                        $data['id_job'] = $id_job;
//                        $data['id_action'] = $action_data['id'];
//                        $data['id_job_view'] = $id_view  ;
//                        $modeStats = new models_Stats;
//                        $modeStats->saveCampaignReport($data);
                    }

                }
            }
        }

    }

    public function getUserInfoAction()
    {
        header('Content-Type: application/json');
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model_job = new models_Jobs;
        if (!isset($this->_params['id'])) {
            echo json_encode(['error' => true, 'message' => 'user is not setted']);
            die;
        }

        $id_user = (int)$this->_params['id'];
        $model_user = new models_User;
        $userProfile = $model_user->getUserById($id_user);
        unset($userProfile['password']);
        $user['user'] = $userProfile;

        $image = $this->_settings['sitename'] . '/resources/files/user/' . $user['user']['image'];
        if (is_null($user['user']['image']) || $user['user']['image'] == "") {
            $image = $this->_settings['sitename'] . '/resources/admin/images/no-picture.jpg';
        }
        $user['user']['image'] = $image;
        $positions = $model_user->getUserPositions($id_user);
        $educations = $model_user->getUserEducation($id_user);

        $industries = [];
        $companies = [];

        foreach ($positions as $index => &$position) {
            $d1 = new DateTime($position['end']);
            $d2 = new DateTime($position['start']);
            $diff = $d2->diff($d1);
            $positions[$index]['years'] = $diff->m;
            if ($position['is_current']) {
                $d1 = new DateTime(date('Y-m-d'));
                $diff = $d2->diff($d1);
                $positions[$index]['years'] = ($diff->m / 12);
                $positions[$index]['end'] = date('Y-m-d');
            }
            $industries[$position['id_industry']]['id_industry'] = $position['id_industry'];
            $industries[$position['id_industry']]['id_user'] = $position['id_user'];
            $industries[$position['id_industry']]['years'] = $position['years'];

            $company = ['title' => $position['company'],
                'years' => $position['years'],
//                'id_user' => $position['id_user'],
//                'id_position' => $position['id']
            ];
            if (!is_null($position['company'])) {
                array_push($companies, $company);
            }

        }

        foreach ($educations as $index => &$education) {
            $d1 = new DateTime($education['end']);
            $d2 = new DateTime($education['start']);
            $diff = $d2->diff($d1);
            $educations[$index]['years'] = $diff->y;
            $educations[$index]['graduation'] = $education['end'];
        }

        foreach ($industries as $index => &$industry) {
            $industries[$index]['title'] = $model_user->getInductryById($industry['id_industry'])['title'];
        }

        $user['positions'] = $positions;
        $user['education'] = $educations;
        $user['industries'] = $industries;
        $user['companies'] = $companies;
        $user['skills'] = $model_user->getUserSkillsWithRating($id_user);
        $user['languages'] = $model_user->getUserLanguages($id_user);
        $user['references'] = $model_user->getUserReference($id_user);
        $user['user']['cv'] = $model_user->getUserLinkCv($id_user);
        if (isset($this->_params['id_application'])) {
            $id_application = (int)$this->_params['id_application'];
            $application = $model_job->getJobapplicationHistoryBuyApplicationId($id_application);
            $header = $model_job->getHeaderApplicationById($id_application);
            $user['application'] = $header;
            $current_column_order = $model_job->getColumnOrderByApplicationId($id_application);
            if (isset($current_column_order['column_order'])) $user['user']['column_order'] = $current_column_order['column_order'];
        }

        $user['all_applications_id'] = $model_job->getJobApplicationsIdByUserID($id_user);
        $user_recommender = $model_user->getUserById($userProfile['id_parent']);
        $user_recommender = (empty($user_recommender)) ? "-" : $user_recommender['firstname'] . ' ' . $user_recommender['lastname'];
        $user['recommender'] = $user_recommender;
        echo json_encode($user, true);
        die;

    }

    public function sendgridBounceAction()
    {
        header('Access-Control-Allow-Origin: *');
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);
        $model->disableBouncedUsers($data);
        die;
    }

    public function getJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Jobs;
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $model->getActiveJobs();
        echo json_encode($jobs);
        exit;
    }

    public function getRecruitersAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Jobs();
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $model->getCompanyRecruiter();
        echo json_encode($jobs);
        exit;
    }

    public function getChannelsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Jobs();
        $this->_helper->viewRenderer->setNoRender(true);
        $channels = $model->getChannels();
        echo json_encode($channels);
        exit;
    }

    public function getHiringManagersAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $model->getHiringManagers();
        echo json_encode($jobs);
        exit;
    }

    public function getJobTypesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $model->getJobTypes();
        echo json_encode($jobs);
        exit;
    }

    public function getJobCareersAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $model->getJobCareers();
        echo json_encode($jobs);
        exit;
    }

    public function getJobPeriodeAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getJobPeriode();
        echo json_encode($data);
        exit;
    }

    public function getCountriesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getCountriesSelect();
        echo json_encode($data);
        exit;
    }

    public function getCountiesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $id_country = (int)$this->_params['id'];
            $model = new models_Content;
            $this->_helper->viewRenderer->setNoRender(true);
            $data = $model->getCountiesByCountryId($id_country);
            echo json_encode($data);
        }
        exit;
    }

    public function getCountiesByListAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $countries = (array)$this->_params['countries'];
            $model = new models_Content;
            $this->_helper->viewRenderer->setNoRender(true);
            $data = $model->getCountiesByCountryByArray($countries);
            echo json_encode($data);
        }
        exit;
    }

    public function getCitieAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $id_county = (int)$this->_params['id'];
            $model = new models_Content;
            $this->_helper->viewRenderer->setNoRender(true);
            $data = $model->getCitiesByCountyId($id_county);
            echo json_encode($data);
        }
        exit;
    }

    public function getCitieByListAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $counties = (array)$this->_params['counties'];
            $model = new models_Content;
            $this->_helper->viewRenderer->setNoRender(true);
            $data = $model->getCitiesByCountyByList($counties);
            echo json_encode($data);
        }
        exit;
    }

    public function getDepartmentsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getDepartmentsSelect();
        echo json_encode($data);
    }

    public function getSubDepartmentAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $id_department = (int)$this->_params['id'];
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getSubDepartmentsByDepartmentId($id_department);
        echo json_encode($data);
    }

    public function getSubDepartmentByListAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $id_departments = (array)$this->_params['departments'];
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getSubDepartmentsByDepartmentList($id_departments);
        echo json_encode($data);
    }

    public function getSubSubDepartmentAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $id_sub_department = (int)$this->_params['id'];
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getSubSubDepartmentsByDepartmentId($id_sub_department);
        echo json_encode($data);
    }

    public function getSubSubDepartmentByListAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $model = new models_Content;
        $id_sub_departments = (array)$this->_params['sub_departments'];
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $model->getSubSubDepartmentsByDepartmentByList($id_sub_departments);
        echo json_encode($data);
    }

    public function getJobByIdAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_job = (int)$this->_params['id'];
        $job = [];
        if ($id_job > 0) {
            $job = (new Domain_Job_Service_GetJobByIDService($id_job))->execute();
        }
        echo json_encode(['error' => false, $job]);
        exit();

    }

    public function getProjectsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model = new models_Content;
        $data = $model->getProjectSelect();
        echo json_encode($data);
    }


    public function getSkillAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model = new models_Content;
        if ($this->getRequest()->isPost()) {
            $skill = $this->_params['skill'];
            $pattern = "/^$skill.*/i";
            $cache = WebApp_Cache_Memcache::getInstance();
            if ($cache->get('skills')) {
                $skills = $cache->get('skills');
            } else {
                $skills = $model->getSkillsSelect();
                $cache->set('skills', $skills);
            }
            $skills = preg_grep($pattern, $skills);
            uasort($skills);
            echo json_encode(array_slice($skills, 0, 100, true));
            exit();
        }
    }

    public function getCurrenciesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model = new models_Content;
        $data = $model->getCurrenciesSelect();
        echo json_encode($data);
    }

    public function getPaymentTypesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model = new models_Content;
        $data = $model->getPaymentTypesSelect();
        echo json_encode($data);
    }

    public function getBountyDdAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $this->_settings['jobs_lifetime'];
        echo json_encode($data);
    }

    public function uploadImageAction()
    {
//        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
//        if($this->getRequest()->isPost())
//        {
//            echo '<pre>';print_r($_FILES) ;die;
        $mUser = new models_User;
        $id_job = (int)$this->_params['id'];
        if ($id_job > 0) {
            $id = $id_job;
            if ($_FILES['bounty_file']['name'] != "") {
                $file = $mUser->renameUploadedFile($_FILES['bounty_file']['name']);
                move_uploaded_file($_FILES['bounty_file']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/bounties/" . $file);
                $this->_modelJobs->update(array("bounty_file" => $file), "id = {$id}", "jobs");
            }
            if ($_FILES['job_public_image']['name'] != "") {
                $file = $mUser->renameUploadedFile($_FILES['job_public_image']['name']);
                move_uploaded_file($_FILES['job_public_image']['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/jobs/" . $file);
                $this->_modelJobs->update(array("job_public_image" => $file), "id = {$id}", "jobs");
            }
            echo json_encode(['error', false]);
        }

//        }
        exit();
    }

    public function getContentsAction()
    {
        $model_job = new models_Jobs();
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $cache = WebApp_Cache_Memcache::getInstance();
        if ($cache->get('get-contents')) {
            $data = $cache->get('get-contents');
        } else {
            $model = new models_Content;
            $data['payment_types'] = $model->getPaymentTypesSelect();
            $data['departments'] = $model->getDepartmentsSelect();
            $data['projects'] = $model->getProjectSelect();
            $data['skills'] = ($model->getSkillsSelect() + $model->getCustomSkillsSelect());
            $data['currenices'] = $model->getCurrenciesSelect();
            $data['bounty_dd'] = $this->_settings['jobs_lifetime'];
            $data['recruiters'] = $model_job->getCompanyRecruiter();;
            $data['countries'] = $model->getCountriesSelect();
            $data['careers'] = $model->getJobCareers();
            $data['types'] = $model->getJobTypes();
            $data['periode'] = $model->getJobPeriode();
            $data['hiring_managers'] = $model->getHiringManagers();
            $cache->set('get-contents', $data);
        }
        echo json_encode($data);
    }


    public function getTranslateAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_language = $this->_id_lang;
        $mContent = new models_Content();

        if ($this->getRequest()->isPost()) {
            $name = $this->_params['search'];
            $id_language = $this->_params['language'];
        }
        $data = $mContent->getTranslateByIdSearch($id_language, $name);
        echo json_encode($data);
    }

    public function disactivatedJobsAction()
    {
        $job_model = new models_Jobs;
        $job_model->getAllStatus();
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
//        if(!Zend_Auth::getInstance()->hasIdentity()) exit();
        $id_company = Zend_Auth::getInstance()->getIdentity()->id;
        $applicants = $this->_modelJobs->getDisactivatedJsonResult();
        $cities = [];
        $sources = [];
        $jobs = [];
        if (!empty($applicants)) {
            foreach ($applicants as $index => &$user) {
//            if($user['jobs'] == "" || is_null($user['jobs']) || !isset($user['jobs']))
//            {
//                $user['jobs'] = "-";
//            }
                array_push($cities, $user['id_city']);
                array_push($sources, $user['source']);
                array_push($jobs, $user['id_job']);
                $image = $this->_settings['sitename'] . '/resources/files/user/' . $user['image'];
                if (is_null($user['image']) || $user['image'] == "") {
                    $image = $this->_settings['sitename'] . '/resources/admin/images/no-picture.jpg';
                }
                $applied_date = date_create(date('Y-m-d', strtotime($user['updated'])));
                $today = date_create(date('Y-m-d'));
                $diff = date_diff($applied_date, $today);
                $user['days'] = $diff->format("%a");
                if ($user['status'] == 1) {
                    $applied_date = date_create(date('Y-m-d', strtotime($user['date'])));
                    $diff = date_diff($applied_date, $today);
                    $user['days'] = $diff->format("%a");
                }
                $user['image'] = $image;
                $user['date'] = date("d-m", strtotime($user['date']));
                $user['date'] = str_replace('-', '/', $user['date']);
                $user_job_history = $this->_modelJobs->getUserLastPositionsById($user['id_user']);
                $user_recommender = $this->_modelJobs->getUserRecommenderById($user['id_user']);
//            $last_job = "";
//            foreach($user_job_history  as $index => $job)
//            {
//                if($job['is_current']) {
//                    $last_job=$job['title'];
//                    break;
//                }
//
//            }
//            if(!empty($user_job_history) && $last_job == "")
//            {
//                $last_job = $user_job_history[0]['title'];
//            }
//            $user['last_job'] = $last_job;
//
                $user['last_job'] = $user_job_history;
                $user['user_recommender'] = $user_recommender;
            }
        }
        $data['status_applications'] = $applicants;
        $data['status'] = $this->_modelJobs->getSubStatus();
        if (!empty($data['status_applications'])) {
            $data['source'] = $this->_modelJobs->getSocialSource($sources);
            $data['cities'] = $this->_modelJobs->getcities($cities);
            $data['jobs'] = $this->_modelJobs->getJobsSelectFromArray($jobs);
        }
        echo json_encode($data, true);
        exit();

        /*header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = $this->_modelJobs->getDisactivatedJsonResult();
        echo json_encode($data);
        die;
    */
    }

    public function saveLanguagesAction()
    {
//        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isPost()) {
            $languages = $this->_params['languages'];
            if (!empty($languages)) {
                $mContent = new models_Content;
                $mContent->saveLanguage($languages);
                echo json_encode(['success' => true]);
            }
        }
        die;
    }


    public function emailStatusChangeAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_applications = $this->_params['id_application'];
        $status = $this->_params['status'];
        $status_reason = $this->_params['status_reason'];
        if ($this->getRequest()->isGet()) {
            $userObject = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
            $emails = $userObject->changeStatusCollect($id_applications, $status, $status_reason);
            foreach ($emails as $key => &$email) {
                $userObject->smarty->assign(
                    [
                        'subject' => $email['subject'],
                        'content' => $email['body']
                    ]
                );
                $container = $userObject->getEmailTemplateContainerByIdCompany(Zend_Auth::getInstance()->getIdentity()->id);
                $content = $userObject->smarty->fetch('eval:' . $container);
                $email['preview'] = $content;
            }
            echo json_encode($emails);

        }
        exit();
    }

    public function sendVerificationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $link = $this->_params['redirect'];
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            throw new Exception('you need to logged in  before to send email');
        }
        $userObject = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
        $userObject->sendVerifiedEmail();
        $this->_redirect($link);
        exit();
    }

    public function getGamificationsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mContent = new models_content;
        $action = $mContent->getGamifications();
        echo json_encode($action);
        exit();
    }

    public function getValidationsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mContent = new models_content;
        $action = $mContent->getValidations();
        echo json_encode($action);
        exit();
    }

    public function getCampaignLocationAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mContent = new models_content;
        $action = $mContent->getCampaignLocations();
        echo json_encode($action);
        exit();
    }

    public function setFavoriteAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }

        if ($this->getRequest()->isPost()) {
            $id_job = (int)$this->_params['id_job'];
            $id = $this->_modelContent->setFavoriteJob($id_job);
            echo json_encode(['id' => $id, 'success' => true], true);
            die;
        }
    }

    public function getFavoritesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            echo json_encode(['message' => 'not logged'], true);
            exit();
        }
        $data = $this->_modelContent->getFavorites();
        echo json_encode($data, true);
        exit();


    }

    public function deleteFavoriteAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }
        if ($this->getRequest()->isPost()) {
            $id_job = (int)$this->_params['id_job'];
            $this->_modelContent->deleteFavoriteById($id_job);

        }
    }

    public function getSelectedCitiesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_country = $this->_params['id_country'];
        $modelContent = new models_Content;

        $cities = $modelContent->getSetectedCitiesJobs($id_country);

        echo json_encode(array("cities" => $cities));
        exit();
    }

    public function languageAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $lang = $this->_params['lang'];
        $modelContent = new models_Content;

        $languages = $modelContent->getLangaugesByCode($lang);

        echo json_encode($languages);
        exit();
    }

    public function getDepartmemntsDataAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isPost()) {
            $id = $this->_params['id'];
            $mCampaign = new models_Campaign();
            $data = $mCampaign->getCampaignDepartmentsDataById($id);

            echo json_encode($data);
        }
        exit();
    }

    public function fetchLinkedinAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Loader::loadFile("linkedin/vendor/autoload.php");
        Zend_Layout::getMvcInstance()->disableLayout();

        $jobModel = new models_Jobs;
        $linkedIn = new Happyr\LinkedIn\LinkedIn($this->_settings['linkedin_edin']['api_key'], $this->_settings['linkedin_edin']['secret']);
        $linkedIn->setHttpClient(new \Http\Adapter\Guzzle6\Client());
        $linkedIn->setHttpMessageFactory(new Http\Message\MessageFactory\GuzzleMessageFactory());

        if ($linkedIn->isAuthenticated()) {
            //we know that the user is authenticated now. Start query the API
            $user = $linkedIn->get('v1/people/~:(id,firstName,lastName,picture-url,public-profile-url,email-address,picture-urls::(original))');
            $access_token = $linkedIn->getAccessToken()->getToken();
            $id = (int)Zend_Auth::getInstance()->getIdentity()->id;
            $this->_modelJobs->update(
                ['linkedin_link' => $user['publicProfileUrl']],
                "id={$id}",
                "users"
            );
            $this->_redirect('/profile');
        } else {
            $this->_redirect($linkedIn->getLoginUrl());
        }

    }

    public function downloadAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if (!Zend_Auth::getInstance()->hasIdentity()) die('you need to login');
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");

        $this->_helper->viewRenderer->setNoRender(true);
        $cv = $this->_params['cv'];
        $file = APPLICATION_PATH . '/storage/cv/' . $cv;

        if (file_exists($file)) {
            header("Content-disposition: attachment; filename=\"" . basename($cv) . "\"");
            readfile($file); // do the double-download-dance (dirty but worky)
        } else {
            echo $cv . '  not exist';
        }

        exit();
    }

    public function readFileAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if (!Zend_Auth::getInstance()->hasIdentity()) die('you need to login');
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Transfer-Encoding: binary");
        $this->_helper->viewRenderer->setNoRender(true);
        $cv = $this->_params['cv'];
        $file = APPLICATION_PATH . '/storage/cv/' . $cv;

        if (file_exists($file)) {
            header("Content-disposition: inline; filename=" . basename($file));
            header("Content-Type: " . mime_content_type($file));
            header("Content-Length: " . filesize($file));
            readfile($file); // do the double-download-dance (dirty but worky)
        } else {
            echo $cv . '  not exist';
        }

        exit();
    }
}
