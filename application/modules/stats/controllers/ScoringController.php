<?php
class Stats_ScoringController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;
    public $translate;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Stats;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
        
        $this->view->current_menu = 'scoring';
	}

    public function editAction()
	{
        $data = $this->_model->getScoringPoints();        
        $this->view->data = $data;
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveScoringPoints($_POST);
            $this->_redirect("/edit-points");
        }        
        
        $this->view->current_sub_menu = 'edit';
        $this->view->headTitle()->set('Edit scoring points | '.$this->_settings['sitename'].'');
	}        
}