<?php

class Api_IndexController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    public $translate;
    protected $_flashMessenger;
    protected $_modelJobs;

    public function init()
    {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();
        $this->_modelJobs = new models_Jobs;
        $this->_model = new models_User;
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $this->view->settings = $this->_settings;
    }

    public function insertUserAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $request = new Zend_Controller_Request_Http();
        
//        print_r($_REQUEST);
//        echo '<pre>';
//        print_r($request->getHeader('Content-Type'));
//        die;
        if ($this->getRequest()->isPost())
            {
            $data = $this->_params;
//            echo '<pre>';
//            print_r($_REQUEST);die;
            if ($data['id'])
                {
                $id = $data['id'];
                unset($data['id']);
                }

            if (!empty($data))
                {
                $token = base64_decode($data['_token']);
                $email = explode(':', $token)[0];
                $passwd = explode(':', $token)[1];

                $salt = $this->_model->getUserByEmail($email);
                $pwd = $passwd;

                $sql = $this->_model->select()->from(['u' => 'users'])
                        ->where('u.email =?', $email);
                
                $user = $this->_model->fetchRow($sql);
               
                if (empty($user) || $user['password'] !== $pwd)
                    {
                    echo json_encode(['success' => false, "message" => "Invalid token"]);
                    die;
                    }
                unset($data['_token']);
                    $data['id_parent'] = $user['id'];
                $this->_model->insert($data, 'users');
                $id = $this->_model->lastInsertId();
                if ($id)
                    {
                    echo json_encode(['success' => true, "message" => "User added"]);
                    die;
                    }
                }
            }
    }

}
