<?php
class modules_content_forms_EditEmail extends WebApp_Form
{

	protected $fields;
	private $_defaults = array();
	private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

	// public function __construct ($options = array(), $defaults = array())
	public function __construct ($data = array())
	{
		$this->fields = array('id', 'type', 'status', 'frequency', 'title', 'subject', 'content');

        parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-email.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        
        foreach ($languages as $lang => $val)
        {
            $this->sf[] = "title_".$lang;
            $this->sf[] = "subject_".$lang;
            $this->sf[] = "content_".$lang;
        }
        
        $this->languages = $languages;

		$this->go();
	}

	function go()
	{
		if(empty($this->fields))
		{
			return;
		}

		foreach ($this->fields as $field)
		{
			switch($field)
			{
                case 'id':
					$element = new WebApp_Form_Element_Hidden($field);
				break;
                
                case 'title':
                case 'subject':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control';
                    $element->setAttrib("required", "true");
                    $this->addElement($element);
                break;
                
                case 'content':
                    $element = new WebApp_Form_Element_Textarea($field);
                    $element->class = 'form-control ckeditor';
                    $element->setAttrib("required", "true");
                    $element->setAttrib("rows", "3");
                    $this->addElement($element);
                break;
                
                case 'type':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['email_types']);                     
                    $element->class = 'form-control select2';
				break;
                
               case 'status':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['company_status']);                     
                    $element->class = 'form-control select2';
				break;
                
                case 'frequency':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['email_freqs']);                     
                    $element->class = 'form-control select2';
				break;
			}
			
			//set default values for fields

			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
			{
				$element->setValue($this->_defaults[$field]);
			}

			$this->addElement($element);
		}
	}
}