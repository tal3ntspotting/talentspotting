<?php
//class modules_content_forms_EditNewsCategory extends WebApp_Form
//{
//
//	protected $fields;
//	private $_defaults = array();
//	private $_data = array();
//    public $languages;
//    public $sf;
//    protected $_settings;
//
//	// public function __construct ($options = array(), $defaults = array())
//	public function __construct ($data = array())
//	{
//		$this->fields = array('id', 'id_news','id_user', 'comment');
//
//        parent::__construct($options);
//		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-news-comment.phtml"))));
//
//        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
//		$this->_settings =  $bootstrap['resources']['Settings'];
//        $languages = Zend_Registry::get("_LANG_DETAILS");
//
//
//        $this->languages = $languages;
//
//		$this->go();
//	}
//
//	function go()
//	{
//		if(empty($this->fields))
//		{
//			return;
//		}
//
//		foreach ($this->fields as $field)
//		{
//			switch($field)
//			{
//                case 'id':
//					$element = new WebApp_Form_Element_Hidden($field);
//				break;
//
//                case 'id_news':
//					$element = new WebApp_Form_Element_Select($field);
//
//                    $model = new models_Content;
//                    $data = $model->getNewsSelect();
//					$element->addMultiOptions(array("" => "All") + (array) $data);
//                    $element->class = 'form-control select2-input';
//				break;
//
//                case 'id_user':
//					$element = new WebApp_Form_Element_Select($field);
//
//                    $model = new models_User;
//                    $data = $model->getUsersSelect();
//					$element->addMultiOptions(array("" => "All") + (array) $data);
//                    $element->class = 'form-control select2-input';
//				break;
//
//                case 'comment':
//					$element = new WebApp_Form_Element_Textarea($field);
//                    $element->class = 'form-control';
//                    $element->setAttrib("rows", "3");
//                    $element->setAttrib("maxlength", "500");
//				break;
//			}
//
//			//set default values for fields
//
//			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
//			{
//				$element->setValue($this->_defaults[$field]);
//			}
//
//			$this->addElement($element);
//		}
//	}
//}
?>