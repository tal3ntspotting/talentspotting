<?php
class modules_content_forms_EditSubdomain extends WebApp_Form
{

	protected $fields;
	private $_defaults = array();
	private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

	// public function __construct ($options = array(), $defaults = array())
	public function __construct ($data = array())
	{
		$this->fields = array('id', 'id_lang', 'id_country', 'title', 'ga_code', 'language_code');

        parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-subdomain.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];        
        $languages = Zend_Registry::get("_LANG_DETAILS");
                
        
        $this->languages = $languages;

		$this->go();
	}

	function go()
	{
		if(empty($this->fields))
		{
			return;
		}

		foreach ($this->fields as $field)
		{
			switch($field)
			{
                case 'id':
					$element = new WebApp_Form_Element_Hidden($field);
				break;
                
                case 'id_lang':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['languages']);                     
                    $element->class = 'form-control select2-input';
				break;
                                
                case 'title':
                case 'language_code':
					$element = new WebApp_Form_Element_Text($field);                    
					$element->class = 'form-control';                     
                    $element->setAttrib('required', 'required');
				break;   
                
                case 'ga_code':
					$element = new WebApp_Form_Element_Textarea($field);                    
					$element->class = 'form-control';           
                    $element->setAttrib('rows', '4');                              
				break;          
                
                case 'id_country':
					$element = new WebApp_Form_Element_Select($field);
                    
                    $model = new models_Content;
                    $data = $model->getCountriesSelect();                    
					$element->addMultiOptions((array) $data);                     
                    $element->class = 'form-control select2-input';
				break;
			}
			
			//set default values for fields

			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
			{
				$element->setValue($this->_defaults[$field]);
			}

			$this->addElement($element);
		}
	}
}