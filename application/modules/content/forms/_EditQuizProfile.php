<?php
class modules_content_forms_EditQuizProfile extends WebApp_Form
{

	protected $fields;
	private $_defaults = array();
	private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

	// public function __construct ($options = array(), $defaults = array())
	public function __construct ($data = array())
	{
		$this->fields = array('id', 'id_quiz', 'profiles_adjectives');
        $this->_data = $data;

        parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-quiz-profile.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        
        foreach ($languages as $lang => $val)
        {
            $this->sf[] = "title_".$lang;
            $this->sf[] = "good_text_".$lang;
            $this->sf[] = "bad_text_".$lang;
            $this->sf[] = "good_content_".$lang;
            $this->sf[] = "bad_content_".$lang;
        }
        
        $this->languages = $languages;

		$this->go();
	}

	function go()
	{
		if(empty($this->fields))
		{
			return;
		}

		foreach ($this->fields as $field)
		{
			switch($field)
			{
                case 'id':
                case 'id_quiz':
					$element = new WebApp_Form_Element_Hidden($field);
				break;             
                
                case 'profiles_adjectives':
                    $element = new WebApp_Form_Element_MultiSelect($field);
					$element->class = 'form-control select2';
                    $element->placeholder = 'Select one or more adjectives';
                    
                    $model = new models_Content;
                    $data = $model->getQuizAdjectivesSelect($this->_data['id_quiz']);
                    $element->addMultioptions($data);             
                break;   
			}
			
			//set default values for fields

			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
			{
				$element->setValue($this->_defaults[$field]);
			}

			$this->addElement($element);
		}
        
        foreach ($this->languages as $lang => $val)
        {
            if (in_array("good_content_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Textarea("good_content_".$lang);
                $element->class = 'form-control ckeditor';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }   
            
            if (in_array("bad_content_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Textarea("bad_content_".$lang);
                $element->class = 'form-control ckeditor';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            } 
            
            if (in_array("title_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Text("title_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $this->addElement($element);
            }
            
            if (in_array("good_text_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Text("good_text_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $this->addElement($element);
            }
            
            if (in_array("bad_text_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Text("bad_text_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $this->addElement($element);
            }
        }
	}
}