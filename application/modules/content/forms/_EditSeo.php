<?php
class modules_content_forms_EditSeo extends WebApp_Form
{

	protected $fields;
	private $_defaults = array();
	private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

	// public function __construct ($options = array(), $defaults = array())
	public function __construct ($data = array())
	{
		$this->fields = array('id', 'type', 'id_subdomain', 'page');

        parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-seo.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        
        foreach ($languages as $lang => $val)
        {
            $this->sf[] = "value_".$lang;
        }
        
        $this->languages = $languages;

		$this->go();
	}

	function go()
	{
		if(empty($this->fields))
		{
			return;
		}

		foreach ($this->fields as $field)
		{
			switch($field)
			{
                case 'id':
					$element = new WebApp_Form_Element_Hidden($field);
				break;
                
                case 'page':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['seo_pages']);                     
                    $element->class = 'form-control select2-input';
				break;
                
               case 'type':
					$element = new WebApp_Form_Element_Select($field);                    
					$element->addMultiOptions($this->_settings['seo_types']);                     
                    $element->class = 'form-control select2-input';
				break;
                
                case 'id_subdomain':
					$element = new WebApp_Form_Element_Select($field);
                    
                    $model = new models_Content;
                    $data = $model->getSubdomainsSelect();                    
					$element->addMultiOptions(array("" => "All") + (array) $data);                     
                    $element->class = 'form-control select2-input';
				break;
			}
			
			//set default values for fields

			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
			{
				$element->setValue($this->_defaults[$field]);
			}

			$this->addElement($element);
		}
        
        foreach ($this->languages as $lang => $val)
        {
            if (in_array("value_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Textarea("value_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }
        }
	}
}