<?php
class modules_content_forms_EditQuizAdjective extends WebApp_Form
{

	protected $fields;
	private $_defaults = array();
	private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

	// public function __construct ($options = array(), $defaults = array())
	public function __construct ($data = array())
	{
		$this->fields = array('id', 'id_quiz');

        parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "admin/forms/edit-quiz-adjective.phtml"))));
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_settings =  $bootstrap['resources']['Settings'];        
        $languages = Zend_Registry::get("_LANG_DETAILS");
        
        foreach ($languages as $lang => $val)
        {
            $this->sf[] = "title_".$lang;
            $this->sf[] = "description_".$lang;
        }
        
        $this->languages = $languages;

		$this->go();
	}

	function go()
	{
		if(empty($this->fields))
		{
			return;
		}

		foreach ($this->fields as $field)
		{
			switch($field)
			{
                case 'id':
                case 'id_quiz':
					$element = new WebApp_Form_Element_Hidden($field);
				break;                
			}
			
			//set default values for fields

			if (count($this->_defaults) && array_key_exists($field, $this->_defaults))
			{
				$element->setValue($this->_defaults[$field]);
			}

			$this->addElement($element);
		}
        
        foreach ($this->languages as $lang => $val)
        {
            if (in_array("description_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Textarea("description_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }    
            
            if (in_array("title_".$lang, $this->sf))
            {
                $element = new WebApp_Form_Element_Text("title_".$lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $this->addElement($element);
            }
        }
	}
}