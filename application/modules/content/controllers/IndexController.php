<?php
/**
 * index controller for default module
*/
class Content_IndexController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Content;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
	}

    
    
    public function personalizeAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
    }
    
    
    
    public function contactFormAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost())
        {
            $this->_model->sendContact($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    } 
    
    public function contactFormNgoAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost())
        {
            $this->_model->sendContactNgo($_POST);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }

    public function getCountiesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $id_country = (int) $this->_params['id_country'];
//        $id_country = (int) 18;
        $modelContent = new models_Content ;
        $data = $modelContent->getCountiesByCountryId($id_country);;
        $options = "<option value='0'>Select County</option>";
        foreach($data as $key => $value)
        {
            $options .= "<option value='{$key}'>{$value}</option>";
        }

        echo $options;
        exit();
    }

    public function getCitiesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $id_county = (int) $this->_params['id_county'];
//        $id_country = (int) 18;
        $modelContent = new models_Content ;
        $data = $modelContent->getCitiesByCountyId($id_county);
        $options = "<option value='0'>Select Cities</option>";
        foreach($data as $key => $value)
        {
            $options .= "<option value='{$key}'>{$value}</option>";
        }

        echo $options;
        exit();
    }

    public function getJobCountriesAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $countries  = $this->_params['data'];
        $countries = json_decode($countries,true);
        $counties = [];
        $modelContent = new models_Content ;
        foreach($countries as $key => $country)
        {
            $counties[] = $modelContent->getCountiesByCountryId($country['id']);
        }
        $all = [] ;
        foreach($counties as $county)
        {
            foreach($county as $k => $v)
            {
                $all[] = ['id' => $k,'title' => $v] ;
            }
        }
        echo json_encode($all);
        exit();
    }

    public function getJobCityAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $counties  = $this->_params['data'];
        $counties = json_decode($counties,true);
        $cities = [];
        $modelContent = new models_Content ;
        foreach($counties as $key => $county)
        {
            $cities[] = $modelContent->getCitiesByCountyId($county['id']);
        }
        $all = [] ;
        foreach($cities as $city)
        {
            foreach($city as $k => $v)
            {
                $all[] = ['id' => $k,'title' => $v] ;
            }
        }
        echo json_encode($all);
        exit;
    }

    public function getSubDepartmentsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $departments = json_decode($this->_params['data'],true);
        $sub_departments = [];
        $modelContent =  new models_Content;
        foreach($departments as $key => $department)
        {
            $sub_departments[] = $modelContent->getSubDepartmentsByDepartmentId($department['id']);
        }
        $all = [] ;
        foreach($sub_departments as $sub_department)
        {
            foreach($sub_department as $k => $v)
            {
                $all[] = ['id' => $k,'title' => $v] ;
            }
        }
        echo json_encode($all);
        die;
    }

    public function getSubSubDepartmentsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $sub_departments = json_decode($this->_params['data'],true);
        $sub_sub_departments = [];
        $modelContent =  new models_Content;
        foreach($sub_departments as $key => $sub_department)
        {
            $sub_sub_departments[] = $modelContent->getSubSubDepartmentsByDepartmentId($sub_department['id']);
        }
        $all = [] ;
        foreach($sub_sub_departments as $sub_sub_department)
        {
            foreach($sub_sub_department as $k => $v)
            {
                $all[] = ['id' => $k,'title' => $v] ;
            }
        }
        echo json_encode($all);
        die;
    }
}