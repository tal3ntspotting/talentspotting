<?php
/**
 * index controller for default module
*/
class Content_AdminController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Content;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
	}
    
    public function viewSkillsAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'skills';
        
        $this->view->items = $this->_model->getSkills();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "skills_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Skills | '.$this->_settings['sitename'].'');
    }
    
    public function viewCertificationsAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'certifications';
        
        $this->view->items = $this->_model->getCertifications();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "certifications_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Certifications | '.$this->_settings['sitename'].'');
    }
    
    public function viewCountriesAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'countries';
        
        $this->view->items = $this->_model->getCountries();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "countries_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Countries | '.$this->_settings['sitename'].'');
    }
    
    public function viewCitiesAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'cities';
        
        $this->view->items = $this->_model->getCities();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "cities_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Cities | '.$this->_settings['sitename'].'');
    }
    
    public function viewIndustriesAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'industries';
        
        $this->view->items = $this->_model->getIndustries();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "industries_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Industries | '.$this->_settings['sitename'].'');
    }
    
    public function viewDepartmentsAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'departments';
        
        $this->view->items = $this->_model->getDepartments();
        
        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "departments_list.csv");
        }
        
        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }
        
        $this->view->headTitle()->set('View Departments | '.$this->_settings['sitename'].'');
    }
    
    public function editSkillAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'skills';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getSkillById($id);          
        }
        
        $form = new modules_content_forms_EditSkill(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveSkill($_POST);
            $this->_redirect("/view-skills");
        }
        
        $this->view->headTitle()->set('Add/Edit Skill | '.$this->_settings['sitename'].'');
    }
    
    public function editCertificationAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'certifications';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getCertificationById($id);          
        }
        
        $form = new modules_content_forms_EditCertification(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveCertification($_POST);
            $this->_redirect("/view-certifications");
        }
        
        $this->view->headTitle()->set('Add/Edit Certification | '.$this->_settings['sitename'].'');
    }
    
    public function editCountryAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'countries';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getCountryById($id);          
        }
        
        $form = new modules_content_forms_EditCountry(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveCountry($_POST);
            $this->_redirect("/view-countries");
        }
        
        $this->view->headTitle()->set('Add/Edit Country | '.$this->_settings['sitename'].'');
    }
    
    public function editCityAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'cities';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getCityById($id);          
        }
        
        $form = new modules_content_forms_EditCity(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveCity($_POST);
            $this->_redirect("/view-cities");
        }
        
        $this->view->headTitle()->set('Add/Edit City | '.$this->_settings['sitename'].'');
    }
    
    public function editIndustryAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'industries';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getIndustryById($id);          
        }
        
        $form = new modules_content_forms_EditIndustry(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveIndustry($_POST);
            $this->_redirect("/view-industries");
        }
        
        $this->view->headTitle()->set('Add/Edit Industry | '.$this->_settings['sitename'].'');
    }
    
    public function editDepartmentAction()
    {
        $this->view->current_menu = 'content';
        $this->view->current_sub_menu = 'departments';
        
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getDepartmentById($id);          
        }
        
        $form = new modules_content_forms_EditDepartment(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveDepartment($_POST);
            $this->_redirect("/view-departments");
        }
        
        $this->view->headTitle()->set('Add/Edit Department | '.$this->_settings['sitename'].'');
    }
    
    
    public function deleteSkillAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "skills");
        $this->_redirect("/view-skills");
    }
    
    public function deleteCertificationAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "certifications");
        $this->_redirect("/view-certifications");
    }
    
    public function deleteCountryAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "countries");
        $this->_model->delete("id_country = {$id}", "cities");
        $this->_redirect("/view-countries");
    }
    
    public function deleteCityAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "cities");
        $this->_redirect("/view-cities");
    }
    
    public function deleteIndustryAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "industries");
        $this->_redirect("/view-industries");
    }
    
    public function deleteDepartmentAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "departments");
        $this->_redirect("/view-departments");
    }
    
    
    public function documentsAction()
    {
        $this->view->items = $this->_model->getBountyDocuments();
        
        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "documents";
                
        $this->view->headTitle()->set('View bounty documents');
    } 
    
    public function editDocumentAction()
    {
        $id = $this->_params['id'];
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getDocumentById($id);          
        }
        
        $form = new modules_content_forms_EditDocument(array('disableLoadDefaultDecorators' => true));		
        $form->populate($data);
        
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->saveDocument($_POST);
            $this->_redirect("/view-documents");
        }
        
        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "documents";
                
        $this->view->headTitle()->set('Add/Edit bounty document | ');
    }
    
    public function downloadDocumentAction()
    {
        $id = $this->_params['id'];
        $id_bounty = $this->_params['id_bounty'];
        $this->_model->downloadDocument($id, $id_bounty);
        
        exit();
    }
}