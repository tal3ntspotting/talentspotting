<?php

/**
 * index controller for default module
 */
class IndexController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_stats;
    protected $_flashMessenger;
    protected $_modelCompany;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();
        $this->_modelCompany = new models_Company;

        $this->_model = new models_User;
        $this->_stats = new models_Stats;
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;
    }

    public function campaignAction() {
        $check_is_external = $this->_settings['external_platform'];
        if ($check_is_external) {
            $this->_helper->viewRenderer->setRender('external/campaign');
        } else {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                $this->redirect('/login');
            }

            if ($this->_model->_user->id < 1) {
                $this->_forward("features");
            }
        }


        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $this->view->form = new modules_default_forms_Filter;
        $modelCompany = new models_Company;
        $this->view->allow_external = $modelCompany->getCompaniesAllowExternal(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_external'];
        $this->view->allow_employee_apply = $modelCompany->getCompaniesEmployeeApply(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_employee_apply'];
        $this->view->allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];



        $slug = $this->_params['slug'];
        $id = $this->_params['id'];

        $modelsCampaign = new models_Campaign;
        $modelsJobs = new models_Jobs;

        $campaign = $modelsCampaign->getCampaignById((int) $id);
        $this->view->campaignActions = $modelsCampaign->getCampaignGamifiedActions($id);
        $this->view->description = $campaign['description'];
        $jobs = $campaign['jobs'];
        $data = [];
        foreach ($jobs as $id) {
            $data[] = $modelsJobs->getJobById($id);
        }
        $this->view->jobs = $data;
    }

    public function indexAction() {

        $check_is_external = $this->_settings['external_platform'];
        if(!$check_is_external)
        {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                $this->redirect('/login');
            }
        }
//        $company = $this->_modelCompany->getCompanyBySubdomainSlug();
//        $this->view->company = $company ;
//        $mContent = new models_Content();
//        $this->view->currencies = $mContent->getCurrenciesSelect();
//        $data = array("search" => $this->_params['search']);
//        $form = new modules_default_forms_Search;
//        $form->populate($data);
//
//        $this->view->search = $form;


        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

//        $first_login = new Zend_Session_Namespace("first-login");
//        if ($first_login->access == 1) {
//            $this->view->gift = 1;
//            $first_login->access == 0;
//            Zend_Session::namespaceUnset("first-login");
//        }
//        $modelContent = new models_Content;
//        $modelJobs = new models_Jobs;
//        $modelCampaigns = new models_Campaign;
//        $modelCompany = new models_Company;
//        $this->view->allow_external = $modelCompany->getCompaniesAllowExternal($modelCompany->getCompanyBySubdomainSlug()['id'])['allow_external'];
//        $this->view->allow_employee_apply = $modelCompany->getCompaniesEmployeeApply($modelCompany->getCompanyBySubdomainSlug()['id'])['allow_employee_apply'];
//        $this->view->allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend($modelCompany->getCompanyBySubdomainSlug()['id'])['allow_candidate_recommend'];
//        echo '<pre>';
//        print_r($this->view->allow_external);
////        print_r($this->view->allow_employee_apply);
//        print_r($this->view->allow_candidate_recommend);
//        die;
//        $this->view->skills = $modelContent->getSkillsSelect();
//        $this->view->cities = $modelContent->getCitiesSelect();


//
//        $this->view->form = new modules_default_forms_Filter;
//        $filters['is_active'] = 1;
//        $results = $modelCampaigns->getCampaignsWithJobs();
//        $campaigns = [];
//        foreach ($results as $campaign) {
//            $campaign_users = array_filter($campaign['campaign_users']);
//            if (empty($campaign_users)) {
//                $campaigns[$campaign['id']] = $campaign;
//            }
//            if (in_array(Zend_Auth::getInstance()->getIdentity()->id, $campaign_users)) {
//                $campaigns[$campaign['id']] = $campaign;
//            }
//        }
//        $this->view->campaigns = $campaigns;
//
//
//        foreach ((array) $this->view->campaigns as $id_campaign => $campaign) {
//            foreach ((array) $campaign['jobs'] as $id_job => $job) {
//                if (isset($this->_model->_user->id)) {
//                    $this->_model->insert(array("id_campaign" => $id_campaign, "id_user" => $this->_model->_user->id, "action" => "view", "value" => 1), "campaign_reports_detailed");
//                }
//            }
//        }

//        $modelCampaigns->updateCampaignsStats();

//        if ($this->getRequest()->isPost()) {
//            $filters['search'] = WebApp_Tools::ClearString($this->_params['search'],'');
//            $filters['id_city'] = htmlspecialchars(WebApp_Tools::ClearString($this->_params['cities'], ''), ENT_QUOTES, 'UTF-8');
//            $filters['id_skill'] = htmlspecialchars(WebApp_Tools::ClearString($this->_params['skills'], ''), ENT_QUOTES, 'UTF-8');
//
//            $filters = array_filter($filters);
//
//            $this->view->jobs = $modelJobs->getRemainingJobs($filters);
//        } else {
//            $this->view->jobs = $modelJobs->getRemainingJobs($filters, $this->view->campaigns);
//        }
//        $this->view->filters = $filters;

//        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
//        $this->view->doctype('XHTML1_RDFA');
//        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setProperty("og:type", "Job Platform");
//        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
//        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
        $this->view->current_tab = "home";
        $i['type'] = 9 ;
        $i['user'] = 'candidate';
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $i['type'] = Zend_Auth::getInstance()->getIdentity()->type;
            $i['user'] = $this->_settings['user_types'][Zend_Auth::getInstance()->getIdentity()->type];
            $i['id_user'] = Zend_Auth::getInstance()->getIdentity()->id;

        }
        $i['logged'] = (Zend_Auth::getInstance()->hasIdentity()) ? true : false ;
        $i = json_encode($i);
        $js = "<script> var user_type = $i </script>";
        $this->view->js = $js;

        $t['more_details'] = $this->translate->_('more_details');
        $t['social_href'] = $this->translate->_('Check out this great Job posted on Talentspotting');
        $t['job_application'] = strtoupper($this->translate->_('job application'));
        $t['recommender'] = ($this->translate->_('recommender'));
        $t['apply'] = $this->translate->_('apply');
        $t['here'] = $this->translate->_('here');
        $t['hello'] = $this->translate->_('hello');
        $t['on_win'] = $this->translate->_('on_win');
        $t['on_hire'] = $this->translate->_('on_hire');
        $t['view_all'] = $this->translate->_('view all');
        $t = json_encode($t);
        $this->view->js_t  = "<script> var trans_key = $t </script>";
    }

    public function sitemapAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $modelContent = new models_Content;
        $modelCompany = new models_Company;
        $modelJobs = new models_Jobs;

        $cities = $modelContent->getCitiesSlugSelect();
        $domains = $modelContent->getDepartmentsSlugSelect();
        $countries = $modelContent->getCountriesSlugSelect();
        $industries = $modelContent->getIndustriesSlugSelect();
        $companies = $modelCompany->getCompaniesSlugSelectWithJobs();
//        $pages = $modelContent->getPagesSlugSelect();

        $filters['is_active'] = 1;

        $session = new Zend_Session_Namespace("active_subdomain");
        if ($session->active_subdomain > 0) {
            $subdomain = $modelContent->getSubdomainById($session->active_subdomain);
            $filters['id_country'][] = $subdomain['id_country'];
        }
        $jobs = $modelJobs->getAllJobs($filters);


        $sitemap = new DOMDocument('1.0', 'utf-8');
        $root = $sitemap->createElement("urlset");
        $sitemap->appendChild($root);

        $root_attr = $sitemap->createAttribute('xmlns');
        $root->appendChild($root_attr);

        $root_attr_text = $sitemap->createTextNode('http://www.sitemaps.org/schemas/sitemap/0.9');
        $root_attr->appendChild($root_attr_text);

        foreach ((array) $companies as $company) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/company~" . $company . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $countries as $country) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/countries~" . $country . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $cities as $city) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/cities~" . $city . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $domains as $domain) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/domains~" . $domain . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $industries as $industry) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/industries~" . $industry . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $jobs as $job) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/{$job['id']}/{$job['slug']}/{$job['company_slug']}";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

//        foreach ((array) $pages as $page) {
//            $url = $sitemap->createElement("url");
//            $root->appendChild($url);
//
//            $loc = $sitemap->createElement("loc");
//            $lastmod = $sitemap->createElement("lastmod");
//            $changefreq = $sitemap->createElement("changefreq");
//            $priority = $sitemap->createElement("priority");
//
//            $http_url = APPLICATION_URL . "/page/{$page}";
//            $url->appendChild($loc);
//            $url_text = $sitemap->createTextNode($http_url);
//            $loc->appendChild($url_text);
//
//            $url->appendChild($lastmod);
//            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
//            $lastmod->appendChild($lastmod_text);
//
//            $url->appendChild($changefreq);
//            $changefreq_text = $sitemap->createTextNode("daily");
//            $changefreq->appendChild($changefreq_text);
//
//            $url->appendChild($priority);
//            $priority_text = $sitemap->createTextNode("1.0");
//            $priority->appendChild($priority_text);
//        }

        $output = $sitemap->saveXML();
        $this->getResponse()->setHeader('Content-Type', 'text/xml; charset=utf-8')->setBody($output);
    }

    public function searchAction() {
        if (isset($this->_params['filters_id_city'])) {
            $this->_params['id_city'] = $this->_params['filters_id_city'];
            unset($this->_params['filters_id_city']);
        }

        if (isset($this->_params['filters_id_department'])) {
            $this->_params['id_department'] = $this->_params['filters_id_department'];
            unset($this->_params['filters_id_department']);
        }

        if (isset($this->_params['filters_id_industry'])) {
            $this->_params['id_industry'] = $this->_params['filters_id_industry'];
            unset($this->_params['filters_id_industry']);
        }

        if (isset($this->_params['filters_career_level'])) {
            $this->_params['career_level'] = $this->_params['filters_career_level'];
            unset($this->_params['filters_career_level']);
        }

        if (isset($this->_params['filters_job_type'])) {
            $this->_params['job_type'] = $this->_params['filters_job_type'];
            unset($this->_params['filters_job_type']);
        }

        if (isset($this->_params['search_type'])) {
            switch ($this->_params['search_type']) {
                case "skills":
                    $modelContent = new models_Content;
                    $skills = $modelContent->searchSkills($this->_params['search']);
                    unset($this->_params['search']);
                    $this->_params['id_skill'] = $skills;
                    break;

                case "companies":
                    $modelCompany = new models_Company;
                    $companies = $modelCompany->searchCompany($this->_params['search']);

                    if (!empty($companies)) {
                        $this->_redirect("/company/{$companies[0]['slug']}");
                    }

                    unset($this->_params['search']);
                    $this->_params['id_company'] = $companies;
                    break;
            }
        }

        $modelContent = new models_Content;
        $cities = $modelContent->getCitiesSlugSelect();
        $domains = $modelContent->getDepartmentsSlugSelect();
        $skills = $modelContent->getSkillsSlugSelect();
        $industries = $modelContent->getIndustriesSlugSelect();
        $country = $modelContent->getCountriesSlugSelect();

        foreach ($this->_params as $key => $value) {
            if (array_key_exists($key, $this->_settings['filters'])) {
                $var = $this->_settings['filters'][$key];
                if (is_array($value)) {
                    foreach ($value as $val) {
                        if (array_key_exists($val, $$var) && $val != "") {
                            $method = $$var;
                            $filters[$this->_settings['filters'][$key]][] = $method[$val];
                        }
                    }
                } else {
                    if ($value != "") {
                        $filters[$this->_settings['filters'][$key]] = $value;
                    }
                }
            }
        }

        $url = $this->_getSearchUrl((array) $filters);

        $this->_redirect($url);
    }

    private function _getSearchUrl($filters = array()) {
        $url = "/jobs/";
        $url_parts = array();
        if (!empty($filters)) {
            foreach ($filters as $filter => $value) {
                if (is_array($value)) {
                    $url_parts[$filter] = implode("_", $value);
                } else {
                    $url_parts[$filter] = $value;
                }
            }
        }

        foreach ((array) $url_parts as $key => $value) {
            $url .= "{$key}~{$value}|";
        }

        $url = substr($url, 0, -1);
        return $url;
    }

    public function lostPasswordAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $email = $this->_params['email'];
        $message = $this->_model->resetPassword($email);
        $this->_redirect('/login');
        echo json_encode(array("msg" => "ok"));
        exit();
        /*
          $msg = (strpos($message, "@") !== false) ? sprintf($this->translate->_("recover_success"), $message) : $this->translate->_("recover_error");
          $this->_flashMessenger->addMessage($msg);
          $this->_redirect("/login");
         */
    }

    public function resetAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('login');

        $token = $this->_params['token'];

        if ($this->getRequest()->isPost()) {
            if (!$user = $this->_model->getUserByToken($token)) {
                $msg = $this->translate->_("expired_token");
                $this->_flashMessenger->addMessage($msg);
            } else {
                $password = $this->_params['password'];
                $match = preg_match("/^\S*(?=\S{6,})(?=\S*[A-Z])(?=\S*[\d])\S*$/", $password);
                if ($match) {
                    $password = sha1($this->_params['password']);
                    $this->_model->update(array("password" => $password.$user['salt']), "id = {$user['id']}", "users");
                    $msg = $this->translate->_("success_changed_password");
                    $this->_flashMessenger->addMessage($msg);
                } else {
                    $msg = $this->translate->_("strong_password_error");
                    $this->_flashMessenger->addMessage($msg);
                    $url = $this->getRequest()->getRequestUri();
                    $this->_redirect($url);
                }
            }

            $this->_redirect("/login");
        }

        if (!$user = $this->_model->getUserByToken($token)) {
            $msg = $this->translate->_("expired_token");
            $this->view->msg = $msg;
        }

//        $this->view->headTitle()->set($this->translate->_('reset_password'));
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(1);
//
//        $this->view->doctype('XHTML1_RDFA');
//        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setProperty("og:type", "Job Platform");
//        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
//        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function logoutAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        Zend_Auth::getInstance()->clearIdentity();
        session_destroy();
        $past = time() - 3600;
        foreach ( $_COOKIE as $key => $value )
        {
            setcookie( $key, $value, $past, '/' );
        }
        Zend_Session::namespaceUnset('url');

        $this->_redirect('/');
        exit();
    }

    public function loginAction() {
        $apply_direct = $this->_params['apply'];
        if($apply_direct > 0)
        {
            $apply_auto = new Zend_Session_Namespace('apply_direct');
            $apply_auto->id_job = $apply_direct;
        }

        $this->view->link = '<link rel="stylesheet" href="/resources/frontend/css/theme/css/flow_signin.css" />';
        $this->view->fb_url = "https://www.facebook.com/dialog/oauth?client_id={$this->_settings['facebook_edin']['api_id']}&redirect_uri={$this->_settings['sitename']}/users/index/facebook-signup&scope=email,user_friends,public_profile";
        $check_is_external = $this->_settings['external_platform'];
        $this->view->company = $this->_modelCompany->getCompanyBySubdomainSlug();
        if (Zend_Auth::getInstance()->hasIdentity()) {
            if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
                $this->_redirect('/dashboard');
            } else {
                $this->_redirect('/home');
            }
        }
        $sess = new Zend_Session_Namespace('signin');

        if (isset($sess->access) && $sess->access == 1) {
            $this->view->signIn = 1;
        }
//        $this->_helper->viewRenderer->setRender('login-mobile');
//            $this->renderScript('index/login.mobile.phtml');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->current_menu = "login";

        $session = new Zend_Session_Namespace("redirector");

        if ($this->getRequest()->isPost()) {
            $dbAdapter = Zend_Registry::get('dbMysqlAdapter');
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $user = $this->_model->getUserDataByEmail($this->_params['email']);
            WebApp_Tools::Debug($user);
            WebApp_Tools::Debug($_POST);
            WebApp_Tools::Debug(sha1($_POST['password']) . $user['salt']);
            if (empty($user) && $user['salt'] != "")
                $salt = "";
            else {
                $salt = $user['salt'];
            }
            $authAdapter->setTableName('users')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password')
//                    ->setCredentialTreatment("CONCAT(SHA1(?),'$salt') AND status = 1 AND type != 2")
                ->setIdentity($this->_params['email'])
                ->setCredential(sha1($this->_params['password']).$user['salt']);
//            $authAdapter->getDbSelect()->where("status = 1 AND type !=2");
            $authAdapter->getDbSelect()->where("status = 1");

            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    $this->_flashMessenger->addMessage('no_account_or_invalid');
                    break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    $this->_flashMessenger->addMessage('credentials_incorrect');
                    $this->view->email = $this->_params['email'];
                    $this->_redirect('/login');
//                    $this->view->link = "";
//                    return $this->renderScript('index/password.mobile.phtml');
                    break;

                case Zend_Auth_Result::SUCCESS:
                    if ($userInfo = $authAdapter->getResultRowObject(null, 'password')) {
                        if ($userInfo->id_company > 0 && $userInfo->type == 2) {
                            $this->_model->insert(array("id_user" => $userInfo->id, "view" => 1), "user_tooltip_view");
                        }

                        Zend_Auth::getInstance()->getStorage()->write($userInfo);
                        $this->_model->userLoggedIn();
                        if (Zend_Auth::getInstance()->getIdentity()->first_login == NULL) {
                            $this->_model->setFirstTimeLogin(Zend_Auth::getInstance()->getIdentity()->id);
                        }
                        $this->activatedAccountGamificationPointAction($userInfo->id_company, $userInfo->id);
                        $isFirst = $this->_model->isFirstLogin($userInfo->id);
                        $this->_model->setActivateAccountbyCampaign($userInfo);
                        if ($isFirst['last_login'] == "" && $userInfo->id_company == 470) {
                            $first_login = new Zend_Session_Namespace("first-login");
                            $first_login->access = 1;
                        }
                        $this->_model->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

                        if (isset($this->_params['id_package'])) {
                            $this->_redirect("/recharge/?id_package={$this->_params['id_package']}");
                        }
                        $isPilote = $this->_model->isPilote($userInfo->id_company);
                        if ($isPilote['is_pilote'] == 1) {
                            $result = $this->_model->countPilote($userInfo->id_company);
                            if ($result >= 51) {
                                Zend_Auth::getInstance()->clearIdentity();
                                $msg = $this->translate->_("pilote-limit");
                                $this->_flashMessenger->addMessage($msg);
                                $this->_redirect('/login');
                            }
                        }

                        if ($userInfo->type == 2 || $userInfo->type == 3) {
                            $this->_redirect('/dashboard');
                        } elseif ($userInfo->type == 1)
                        {
                            $this->_redirect('/home');
                        }
                        elseif ($userInfo->type == 5 || $userInfo->type == 4)
                        {
                            $this->_redirect('/users/admin/mycvs');
                        } else {
                            $url_session = new Zend_Session_Namespace('url');
                            $apply_auto = new Zend_Session_Namespace('apply_direct');
                            $id_job = $apply_auto->id_job ;
                            if(($id_job) > 0)
                            {
                                $mJob = new models_Jobs ;
                                $job = $mJob->getJobById($id_job);
                                $logged_user = (array) Zend_Auth::getInstance()->getIdentity();
                                $mJob->apply($logged_user,$job);
                                Zend_Session::namespaceUnset('apply_direct');
                            }
                            if ($url_session->url != "")
                            {
                                $url = $url_session->url;
                                unset($url_session->url);
                                Zend_Session::namespaceUnset('url');
                                $this->_redirect($url);
                            }
                            $this->_redirect('/home');
                        }
                    } else {
                        $msg = 'login_error_auth';
                        $this->_redirect('/login');
                    }
                    break;

                default:
                    $msg = 'login_error_auth';
                    break;
            }

            $this->_flashMessenger->addMessage($msg);
            $this->_redirect('/login');
        }

//        $this->view->headTitle()->set($this->translate->_('login'));
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
//
//        $this->view->doctype('XHTML1_RDFA');
//        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setProperty("og:type", "Job Platform");
//        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
//        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function dashboardAction() {
        $this->view->current_menu = 'dashboard';
        $this->view->job_posts = $this->_model->getLatestJobPosts();
//        print_r($this->_model->getLatestJobPosts());

        $this->view->new_companies = $this->_model->getUnconfirmedCompanies();
        $this->view->bounties_to_give = $this->_model->getBountiesUnpaid();
        $this->view->platform_users = $this->_model->getPlatformUsers();
        $this->view->today_applications = $this->_model->getTodayJobsApplications();
        $this->view->applications = $this->_model->getTodayJobsApplicationsDetailed($this->view->today_applications);
        $this->view->applications_this_month = $this->_model->getMonthJobsApplications();

        if ($this->_model->_user->type == 2) {
            $this->view->company = $this->_model->getCompanyById($this->_model->_user->id_company);
        }

        $this->view->headTitle()->set($this->translate->_('dashboard'));
    }

    public function setLangAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $lang = $this->_params['lang'];

        $redir = "/";

        if ($_SERVER['HTTP_REFERER'] != "" && strpos($_SERVER['HTTP_REFERER'], "/page/") !== false) {
            $slug = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], "/") + 1);
            $current_lang = APPLICATION_LOCALE;
            $languages = Zend_Registry::get('_LANG_DETAILS');
            $id_lang = $languages[$current_lang];

            if ($id_lang > 0) {
                $modelContent = new models_Content($id_lang);
                $page = $modelContent->getPageBySlug($slug);
                $id = $page['id'];

                $modelContent = new models_Content($languages[$lang]);
                $page = $modelContent->getPageById($page['id']);

                $redir = "/page/" . $page['slug'];
            }
        } else if ($_SERVER['HTTP_REFERER'] != "" && strpos($_SERVER['HTTP_REFERER'], "/jobs/") !== false) {
            $url = explode("/", $_SERVER['HTTP_REFERER']);
            $id = $url[4];

            $current_lang = APPLICATION_LOCALE;
            $languages = Zend_Registry::get('_LANG_DETAILS');
            $id_lang = $languages[$current_lang];

            if ($id_lang > 0) {
                $modelJobs = new models_Jobs($id_lang);
                $job = $modelJobs->getJobById($id);

                if (!empty($job)) {
                    $modelJobs = new models_Jobs($languages[$lang]);
                    $job = $modelJobs->getJobById($id);

                    if (!empty($job)) {
                        $redir = WebApp_Tools::createJobSlug($job);
                    } else {
                        $redir = $_SERVER['HTTP_REFERER'];
                    }
                } else {
                    $redir = $_SERVER['HTTP_REFERER'];
                }
            } else {
                $redir = $_SERVER['HTTP_REFERER'];
            }
        } else {
            $redir = $_SERVER['HTTP_REFERER'];
        }

        if (array_key_exists($lang, $this->_settings['languages_names'])) {
            $namespace = new Zend_Session_Namespace('Language');
            $change = new Zend_Session_Namespace('language_change');
            $namespace->lang = $lang;
            $change->lang = $lang;
        } else {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }
        $this->_redirect($redir);
    }

//    public function linkedinAction() {
//        $ns = new Zend_Session_Namespace('linkedin_oauth');
//
//        $options = array(
//            'localUrl' => APPLICATION_URL . '/register-linkedin/',
//            'callbackUrl' => APPLICATION_URL . '/register-linkedin/',
//            'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken',
//            'userAuthorizationUrl' => 'https://api.linkedin.com/uas/oauth/authorize',
//            'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
//            'consumerKey' => $this->_settings['linkedin']['api_key'],
//            'consumerSecret' => $this->_settings['linkedin']['secret'],
//        );
//
//        $consumer = new Zend_Oauth_Consumer($options);
//
//        if (empty($ns->request_token)) {
//            $token = $consumer->getRequestToken();
//            $ns->request_token = serialize($token);
//            $ns->token = $token;
//            $consumer->redirect();
//            die();
//        } else {
//            try {
//                $token = $consumer->getAccessToken($_GET, unserialize($ns->request_token));
//                $ns->token = $token;
//                $this->_model->update(array("linkedin_token" => serialize($token)), "id = '{$this->_model->_user->id}'", "users");
//            } catch (Exception $e) {
//                $this->_model->update(array("linkedin_token" => ""), "id = '{$this->_model->_user->id}'", "users");
//                $ns->request_token = '';
//            }
//        }
//
//        return $this->_redirect('/linkedin-details');
//    }

    public function linkedinDetailsAction() {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $slug = $this->_params['slug'];

        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $token = $ns->token;

        $client = $token->getHttpClient($options);

        $client->setUri('https://api.linkedin.com/v1/people/~:(id,firstName,lastName,industry,location,headline,current-status,positions,educations,skills,certifications,pictureUrl,email-address)');
        $client->setMethod(Zend_Http_Client::GET);
        $client->setHeaders("x-li-format", "json");
        $response = $client->request();
        $content = $response->getBody();
        $data = Zend_Json::decode($content);

        if (!$user = $this->_model->getUserDetailsByEmail($data['emailAddress'])) {
            $user_data['firstname'] = $data['firstName'];
            $user_data['lastname'] = $data['lastName'];
            $user_data['title'] = $data['headline'];
            $user_data['firstname'] = $data['firstName'];
            $user_data['location'] = $data['location']['name'];
            $user_data['image'] = $data['pictureUrl'];
            $user_data['linkedin_token'] = serialize($token);
            $user_data['linkedin_id'] = $data['id'];
            $user_data['status'] = 1;
            $user_data['type'] = 1;
            $user_data['email'] = $data['emailAddress'];
            $user_data['password'] = uniqid();
            $user = $this->_model->saveFromLinkedin($user_data);

            $this->_model->setUserCertifications($data['certifications'], $user['id']);
            $this->_model->setUserEducations($data['educations'], $user['id']);
            $this->_model->setUserPositions($data['positions'], $user['id']);
            $this->_model->setUserSkills($data['skills'], $user['id']);
        } else {
            $this->_flashMessenger->addMessage($this->translate->_("account-exists"));
            $this->_redirect('/register');
        }

        $ns->request_token = null;
        $ns->token = null;

        $this->_redirect("/register-linkedin-skills");
    }

    private function _getLinkedinProfile($url) {
        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );
        $user = $this->_model->getUserById($this->_model->_user->id);

        $token = unserialize($user['linkedin_token']);
        $client = $token->getHttpClient($options);

        $client->setUri($url);
        $client->setMethod(Zend_Http_Client::GET);
        $response = $client->request();
        $content = $response->getBody();

        $data = json_decode($content);

        echo "<pre>";
        print_R($content);
        die();
    }

    public function linkedinMessageAction() {
        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );
        $user = $this->_model->getUserById($this->_model->_user->id);

        $token = unserialize($user['linkedin_token']);
        $client = $token->getHttpClient($options);

        $values[]['person']['_path'] = '/people/CPpLTCDNVi';
        $values[]['person']['_path'] = '/people/~';
        $message = json_encode(array("recipients" => array("values" => $values), "subject" => "test message din platforma", "body" => "tralalalal"));
        //die($message);
        $client->setUri('http://api.linkedin.com/v1/people/~/mailbox');
        $client->setMethod(Zend_Http_Client::POST);
        $client->setHeaders("Content-Type", "application/json");
        $client->setHeaders("x-li-format", "json");
        $client->setRawData($message);
        $response = $client->request();
        $content = $response->getBody();

        $this->_redirect("/dashboard");
    }

    public function registerAction() {
//        echo '<pre>';
//        print_r(Zend_Layout::getMvcInstance()->getView()->headLink()->appendStylesheet('http://lh.talentspotting.com/resources/frontend/css/theme/css/xxx.css'));
//        die;

        if(isset($this->_params['type']) && $this->_params['type'] != "" ) {
            $type = base64_decode($this->_params['type']);
            if($type == 'recommender') {
                $recommender_cookie = new Zend_Session_Namespace('recommender');
                $recommender_cookie->time = time();
                $recommender_cookie->recommender = true;
            }
        }
        if(Zend_Auth::getInstance()->hasIdentity()) $this->_redirect ('/home');
        $this->view->fb_url = "https://www.facebook.com/dialog/oauth?client_id={$this->_settings['facebook_edin']['api_id']}&redirect_uri={$this->_settings['sitename']}/users/index/facebook-signup&scope=email,user_friends,public_profile";
        $this->view->hasTerms = $this->_model->companyhasTerms();
        if (!empty($this->view->hasTerms)) {
            $this->view->hasTerm = 1;
        }
        $check_is_external = $this->_settings['external_platform'];
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();
        $this->view->company = $this->_modelCompany->getCompanyBySubdomainSlug();
        if ($this->getRequest()->isPost()) {
            if (!$user = $this->_model->getUserByEmail($_POST['email'])) {
                    $email = strtolower($_POST['email']);
                    $password = $_POST['password'];
                    $validator = new Zend_Validate_EmailAddress();
                    $validEmpty = new Zend_Validate_NotEmpty();
                    if($validator->isValid($email) && $validEmpty->isValid($password))
                    {
                        $confirm_password = $password ;

                        $data = ['email' => $email,
                            'password' => $password ,
                            'password_confirm' => $confirm_password
                        ];
                        $mUser = new models_User;
                        $mUser->saveNewFlowSignIn($data);
                        $this->_redirect('/profile') ;

                    } else {
                        $msg = $this->translate->_("email-not-valid-or-password-empty");
                    }
            } else {
                $msg = $this->translate->_("account-exists");
            }
            $this->_flashMessenger->addMessage($msg);
            $this->_redirect('/register');
//            unset($_POST['terms']);
//            unset($_POST['password_confirm']);
//            unset($_POST['password-retype']);
//            if (!empty($_POST['company'])) {
//                $company = $_POST['company'];
//                unset($company['password_confirm']);
//                if (!$user = $this->_model->getUserByEmail($company['email'])) {
//                    $userData['firstname'] = $company['firstname'];
//                    $userData['lastname'] = $company['lastname'];
//                    $userData['password'] = $company['password'];
//                    $userData['email'] = $company['email'];
//                    $userData['type'] = 2;
//                    $userData['status'] = 1;
//                    $userData['created'] = date("Y-m-d H:i:s");
//
//
//                    if ($id_user = $this->_model->saveUser($userData, true)) {
//                        $companyData['title'] = $company['title'];
//                        $companyData['email'] = $company['email'];
//                        $companyData['id_country'] = $company['id_country'];
//                        $companyData['id_city'] = $company['id_city'];
//                        $companyData['description'] = $company['description'];
//                        $companyData['address'] = $company['address'];
//                        $companyData['phone'] = $company['phone'];
//                        $companyData['status'] = 1;
//
//                        $id_company = $this->_model->saveCompany($companyData);
//                        $this->_model->update(array("id_company" => $id_company, "is_company_admin" => 1), "id = {$id_user}", "users");
//                        $this->_stats->setGamificationPoint($id_user, "register_company");
//
//                        //$this->_model->authenticate($userData['email'], $userData['password']);                                         
//                        $session = new Zend_Session_Namespace("redirector");
//
//                        $this->_redirect('/profile');
//                    } else {
//                        $msg = $this->translate->_("account-exists");
//                    }
//                } else {
//                    $msg = $this->translate->_("account-exists");
//                }
//            } else {
//                if (!$user = $this->_model->getUserByEmail($_POST['email'])) {
//
//                    $_POST['type'] = 9;
//                    $_POST['status'] = 1;
//                    $_POST['created'] = date("Y-m-d H:i:s");
////                    $_POST['password'] = uniqid();
//                    $session = new Zend_Session_Namespace("track");
//                    $track = $session->track;
//                    if ($track != "") {
//                        $modelJobs = new models_Jobs;
//                        $tracking = $modelJobs->getTrackDetails($track);
//                        $_POST['id_company'] = $tracking['id_company'];
//                        $_POST['id_parent'] = $tracking['id_recommender'];
//                        if (empty($tracking)) {
//                            $tracking = $modelJobs->getHashDetails($track);
//                            $_POST['id_company'] = $tracking['id_company'];
//                            $_POST['id_parent'] = $tracking['id'];
//                        }
//                    }
//
//                    if ($check_is_external) {
//                        $_POST['id_company'] = (int) $this->_modelCompany->getCompanyBySubdomainSlug()['id'];
//                    }
//                    
//                    
//
//                    
//                    $id = $this->_model->saveUser($_POST, true);
//                    $this->_model->setExtendedNetworkPoint($_POST['id_parent'], $id);
//                    // if id to check that the user is alrady added
//                    if ($id) {
//                        $this->_stats->newCandidateAction($tracking['id_recommender'], "new_candidates", $tracking['id_job'], $id);
//                    }
//
//                    $session = new Zend_Session_Namespace('id');
//                    $session->id = $id;
//                    $session->id_company = $_POST['id_company'];
//                    $this->_flashMessenger->addMessage($this->translate->_("account-pending"));
//                    $this->_redirect('/upload');
//;
//                } else {
//                    $msg = $this->translate->_("account-exists");
//                }
//            }
//
//            $this->_flashMessenger->addMessage($msg);
//            $this->_redirect('/register');
        }

//        $this->view->headLink()->appendStylesheet('sssss.css');
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
//
//        $this->view->doctype('XHTML1_RDFA');
//        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setProperty("og:type", "Job Platform");
//        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
//        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
//        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
        $this->view->link = '<link rel="stylesheet" href="/resources/frontend/css/theme/css/flow_signin.css" />';
    }

    public function uploadAction() {


        if(Zend_Auth::getInstance()->hasIdentity()) $this->_redirect ('/home');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $session = new Zend_Session_Namespace('id');
        $id = (int) $session->id;
        $this->view->id = $id;
        $id_company = (int) $session->id_company;
        $form = new modules_default_forms_upload(array("id_company" => $id_company));
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {

            $data = $this->getRequest()->getPost();
            $signin = new WebApp_signin($data);
            $signin->registerStep(2);
            $this->_redirect('/final');
            /*    $this->_model->saveNewUser($id, $data);
                $email = $this->_model->getUserById($id)['email']; */




//            $this->_model->reauthenticate($email);
            $sess = new Zend_Session_Namespace('signin');
            $sess->access = 0;
            $url = new Zend_Session_Namespace('url');
            if ($url && $url->url != "") {
                $this->_model->setUserCompletedProfile($url->url);
                $this->_redirect($url->url);
                $url->url = "";
            }
            Zend_Session::namespaceUnset("url");
            $this->_redirect('/home');
        }
        $this->view->link = '<link rel="stylesheet" href="/resources/frontend/css/theme/css/flow_signin.css" />';
    }

    public function registerCompanyAction() {

        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();
        $this->view->id_package = $this->_params['id_package'];

        if ($this->getRequest()->isPost()) {
            if (!empty($_POST)) {
                $company = $_POST['company'];
                if (!$user = $this->_model->getUserByEmail($company['email'])) {
                    $userData['firstname'] = $company['firstname'];
                    $userData['password'] = $company['password'];
                    $userData['lastname'] = $company['lastname'];
                    $userData['email'] = $company['email'];
                    $userData['type'] = 2;
                    $userData['status'] = 1;
                    $userData['created'] = date("Y-m-d H:i:s");
                    if ($id_user = $this->_model->saveUser($userData, true)) {
                        $companyData['title'] = $company['title'];
                        $companyData['email'] = $company['email'];
                        $companyData['id_country'] = $company['id_country'];
                        $companyData['id_city'] = $company['id_city'];
                        $companyData['address'] = $company['address'];
                        $companyData['status'] = 1;

                        $id_company = $this->_model->saveCompany($companyData);
                        $this->_model->update(array("id_company" => $id_company, "is_company_admin" => 1), "id = {$id_user}", "users");
                        $this->_stats->setGamificationPoint($id_user, "register_company");
                        $this->_model->authenticate($userData['email'], $userData['password']);

                        $this->_redirect('/recharge/?id_package=' . $this->_params['id_package'] . '&acc=new');
                    } else {
                        $msg = $this->translate->_("account-exists");
                    }
                } else {
                    $msg = $this->translate->_("account-exists");
                }
            }
        }

        $this->view->headTitle()->set($this->translate->_('register'));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function checkAuthAction() {
        echo (string) Zend_Auth::getInstance()->hasIdentity();
        exit();
    }

    public function loginRechargeAction() {
        if ($user = $this->_model->authenticate($this->_params['email'], $this->_params['password'])) {
            $message['msg'] = 'ok';
            $message['mode'] = "redirect";
            $message['url'] = APPLICATION_URL . "/recharge/id_package/" . $this->_params['id_package'];
        } else {
            $message['msg'] = 'error';
            $message['error'] = $this->translate->_("no_account_or_invalid");
        }

        echo json_encode($message);
        die();
    }

    public function clickAction() {
        $id = $this->_params['id'];
        $type = $this->_params['type'];

        $url = "/";

        switch ($type) {
            case "banner":
                $modelJobs = new models_Jobs;
                $banner = $modelJobs->getBannerById($id);

                if (!empty($banner)) {
                    $url = $banner['url'];
                    $modelJobs->update(array("clicks" => $banner['clicks'] + 1, "views" => $banner['views'] + 1), "id = {$id}", "banners");
                }
                break;
        }

        $this->redirect($url);
    }

    public function registerEmailAction() {
        $this->view->user = $this->_model->_user;


        $this->view->headTitle()->set($this->translate->_('register'));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function registerLinkedinSkillsAction() {
        $this->view->headTitle()->set($this->translate->_('login'));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");

        $this->view->user = $this->_model->_user;
        $this->view->skills = $this->_model->getUserSkills();
    }

    public function signInAction() {
        $this->view->headTitle()->set($this->translate->_('login'));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function loginCompanyAction() {
        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();
        $this->view->id_package = $this->_params['id_package'];

        $this->view->headTitle()->set($this->translate->_('login'));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        if (isset($this->_params['r'])) {
            $session = new Zend_Session_Namespace("redirector");
            $session->url = "/demo";
        }

        if ($this->getRequest()->isPost()) {
            $dbAdapter = Zend_Registry::get('dbMysqlAdapter');
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);


            $authAdapter->setTableName('users')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('SHA1(?) AND status = 1')
                ->setIdentity($this->_params['email'])
                ->setCredential($this->_params['password']);

            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    $this->_flashMessenger->addMessage('no_account_or_invalid');
                    break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    $this->_flashMessenger->addMessage('credentials_incorrect');
                    break;

                case Zend_Auth_Result::SUCCESS:
                    if ($userInfo = $authAdapter->getResultRowObject(null, 'password')) {
                        if ($userInfo->id_company > 0 && $userInfo->type == 2) {
                            $this->_model->insert(array("id_user" => $userInfo->id, "view" => 1), "user_tooltip_view");
                        }

                        Zend_Auth::getInstance()->getStorage()->write($userInfo);
                        $this->_model->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

                        if (isset($this->_params['id_package'])) {
                            $this->_redirect("/recharge/?id_package={$this->_params['id_package']}");
                        }

                        if ($session->url != "") {
                            $url = $session->url;
                            unset($session->url);
                            $this->_redirect($url);
                        }

                        if ($userInfo->type > 1) {
                            $this->_redirect('/dashboard');
                        } else {
                            $this->_redirect('/');
                        }
                    } else {
                        $msg = 'login_error_auth';
                        $this->_redirect('/login-company');
                    }
                    break;

                default:
                    $msg = 'login_error_auth';
                    break;
            }

            $this->_flashMessenger->addMessage($msg);
            $this->_redirect('/login-company');
        }

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(1, 3));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(1, 5));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    public function constrainImageAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $url = base64_decode($this->_params['url']);
        $pos = isset($this->_params['pos']) ? $this->_params['pos'] : 'center';
        $width = $this->_params['width'];
        $height = $this->_params['height'];
        $file = ABSOLUTE_PATH . "/public/resources/files" . $url;
        WebApp_UploadImage::saveConstrainedFixedImage($width, $height, $file, $pos);
    }

    public function cropImageAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $url = base64_decode($this->_params['url']);
        $image = ABSOLUTE_PATH . "/public/resources/files/" . $url;

        WebApp_UploadImage::constrainImage(150, 150, $image, ABSOLUTE_PATH . "/public/resources/files/cropped_" . md5($url) . ".jpg");

        $image = ABSOLUTE_PATH . "/public/resources/files/cropped_" . md5($url) . ".jpg";

        list($source_width, $source_height, $source_type) = getimagesize($image);
        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_image = imagecreatefromgif($image);
                break;
            case IMAGETYPE_JPEG:
                $source_image = imagecreatefromjpeg($image);
                break;
            case IMAGETYPE_PNG:
                $source_image = imagecreatefrompng($image);
                break;
        }

        $radius = 50;

        $corner_image = imagecreatetruecolor($radius, $radius);
        $clear_colour = imagecolorallocate($corner_image, 0, 0, 0);
        $solid_colour = imagecolorallocate($corner_image, 255, 255, 255);
        imagecolortransparent($corner_image, $clear_colour);
        imagefill($corner_image, 0, 0, $solid_colour);
        imagefilledellipse($corner_image, $radius, $radius, $radius * 2, $radius * 2, $clear_colour);
        imagecopymerge($source_image, $corner_image, 0, 0, 0, 0, $radius, $radius, 100);
        $corner_image = imagerotate($corner_image, 90, 0);
        imagecopymerge($source_image, $corner_image, 0, $source_height - $radius, 0, 0, $radius, $radius, 100);

        $corner_image = imagerotate($corner_image, 90, 0);

        imagecopymerge($source_image, $corner_image, $source_width - $radius, $source_height - $radius, 0, 0, $radius, $radius, 100);

        $corner_image = imagerotate($corner_image, 90, 0);

        imagecopymerge($source_image, $corner_image, $source_width - $radius, 0, 0, 0, $radius, $radius, 100);


        unlink(ABSOLUTE_PATH . "/public/resources/files/cropped_" . md5($url) . ".jpg");
        header("Content-type: image/png");
        imagepng($source_image);

        exit();
    }

    public function featuresAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->current_menu = 'features';
    }

    public function recoverAction() {
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $this->_redirect('/home');
        }
    }

    public function homeAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $layout = Zend_Layout::getMvcInstance();
        $check_is_external = $this->_settings['external_platform'];
        if ($check_is_external) {
            $this->_redirect('/home');
        }

        $layout->setLayout('landing.header');
    }

    public function mailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//         die('111');
    }

    public function subscribeAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $mail = new WebApp_Mail();
        $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
        $mail->setSubject("New Demo");
        if ($_REQUEST) {
            $mail->setBodyFromViewScript("ask-demo.phtml", array('data' => $_REQUEST));
            $mail->addTo('ana@brainspotting.ro', 'New Demo');
            $mail->send($transport);
        }
    }

    public function contactAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $layout = Zend_Layout::getMvcInstance();
//        $this->_redirect('/contact');

        $layout->setLayout('landing.header');
    }

    public function demorequestAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $mail = new WebApp_Mail();
        $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
        $mail->setSubject("New Demo");
        if ($_REQUEST) {
            $mail->setBodyFromViewScript("ask-demo.phtml", array('data' => $_REQUEST));
            $mail->addTo('jeddey.seifeddine@talentspotting.com', 'New Demo');
            $mail->send($transport);
        }

        die();
    }

    public function activatedAccountGamificationPointAction($id_company, $id_user) {
        // check if company has the activate account gamification is set in compaign

        $result = $this->_model->activatedAccountGamificationPoint($id_company, $id_user);
    }

    public function testAction() {
        // update on platform
//        $id_company = Zend_Auth::getInstance()->getIdentity()->id_company ; 
//        $jobsModel = new models_Jobs ;
//        $data = $jobsModel->getJobsByCompanyId($id_company) ;
//        
//        foreach ($data as &$job) {
//            $job["url"] = WebApp_Tools::createJobSlug($job) ;
//        }
//        $transport = null;
//        if (!empty($this->_settings['email_smtp'])) {
//            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//        }
//        $mail = new WebApp_Mail();
//        $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
//        $mail->setSubject("What’s new on Talentspotting");
//        $mail->setBodyFromViewScript("update-on-platform.phtml", array('data' => $data));
//        $mail->addTo('jeddey.seifeddine@gmail.com', 'What’s new on Talentspotting');
//        $mail->send($transport);
        // Tricks and Tips 
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $modeCompany = new models_Company;
        $modeCompany->getCompanyEmailTemplate();
        die;
        $contentModel = new models_Content;
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $smarty = new Smarty;
        $smarty->assign("username", "Test");
        for ($i = 1; $i <= 42; $i++) {
            $email = $contentModel->getEmailByType($i);
            $resource = $smarty->fetch('string:' . $email['content']);
            $subject = $smarty->fetch('string:' . $email['subject']);
            $mail = new WebApp_Mail();
            $mail->setFrom($modeCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email'], $this->_settings['sitename']);
            $mail->setSubject($subject);
            $mail->setBodyHtml($resource);
            $mail->addTo('jeddey.seifeddine@gmail.com', $subject);
            $mail->send($transport);
        }








        ////////////////////////////////////////////////////
        // Debug Smarty
        // 
        // $filename = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails'."/new-campaign.phtml";
        //$handle = fopen($filename, "r");
        //$fcontents = fread($handle, filesize($filename));
        //fclose($handle);
        //     $resource = $smarty->fetch('string:' . $fcontents);
        //     echo $resource
        //
        //
        //
        //
        //
        //
        //
        //
        ///////////////////////////////////////////////////////
    }

    public function termsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $this->view->id = $id;
    }

    public function webhelpAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function activateAction() {
        $this->_helper->viewRenderer->setNoRender();
        $token = $this->_params['token'];
        $this->_model->activateUserAccount($token);
        $this->_redirect("/login");
    }

    public function apiAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if($this->getRequest()->isPost()) {
            $myfile = fopen("/var/www/html/talentspotting/public/newfile.txt", "a") or die("Unable to open file!");
            $txt = "John Doe\n".$_REQUEST;
            fwrite($myfile, $txt);
            fclose($myfile);
            echo json_encode($_REQUEST,true);
        }


//         $this->_modelCompany->update(['note' => $_POST['language']],'id=25','ats_note');
//         echo '<pre>';print_r($_REQUEST);die;
    }

    public function finalAction(){
        $this->view->link = '<link rel="stylesheet" href="/resources/frontend/css/theme/css/flow_signin.css" />';
        if(Zend_Auth::getInstance()->hasIdentity()) $this->_redirect ('/home');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $session = new Zend_Session_Namespace('flow');
        $social = new Zend_Session_Namespace('signin-social');
        $linkedinurl = WebApp_signin::getInstance()->getLinkedin();
        $this->view->linkedinurl = $linkedinurl;
        $required_flow = $this->_modelCompany->getCompanyBySubdomainSlug()['signin'];
        if($this->getRequest()->isPost()) {
            if ($this->_params['linkedin_link'] != "") {
                $flow = new WebApp_signin();
                $flow->setLinkedin($this->_params['linkedin_link']);
                $this->_redirect('/final');
            }
            if(!empty($social->response)) {
                if ($this->_params['phone'] != "") {

                    if($required_flow) {
                        if (is_null($session->_LINKEDIN) && $_FILES['file']['name'] == "")
                        {
                            $error = "Please add your CV or Linkedin Profile link";
                            $this->_flashMessenger->addMessage($error);
                            $this->_redirect('/final');
                            exit();
                        }
                    }


                    $flow = WebApp_signin::getInstance();
                    switch($social->source) {
                        case('facebook'):
                            $id_user =  $this->_model->registerWithFacebook($social->response);
                            break;
                        case('linkedin'):
                            $id_user =  $this->_model->registerWithLinkedin($social->response);
                            break;
                        case('google'):
                            $id_user =  $this->_model->registerWithGoogle($social->response);
                            break;
                    }


                    $recommender_session = new Zend_Session_Namespace('recommender');
                    if($recommender_session->recommender) {
                        $this->_model->update(['type' => 1], 'id='.$id_user,'users');
                    }


                    $this->_model->updateUserById($id_user, ['phone' => $this->_params['phone']]);
                    if($flow->getLinkedin() != "") $this->_model->updateUserById($id_user, ['linkedin_link'=> $flow->getLinkedin()]);
                    $this->_model->uploadCvById($id_user);
                    $this->_model->reauthenticate($social->response->email);
                    $userObject = $this->_model->getUserById($id_user);
//                    echo '<pre>';
//                    print_r($userObject);die;
                    $kennexa = new kennexa_api($userObject, kennexa_api::create);
                    $kennexa->createUser();
                    $this->_redirect('/login');
                    Zend_Session::namespaceUnset('flow');
                    Zend_Session::destroy('flow');
                    Zend_Session::namespaceUnset('recommender');
                    Zend_Session::destroy('recommender');
                    unset($flow);

                }



            } else {
                if ($this->_params['phone'] != "") {
                    if($required_flow) {
                        if(is_null($session->_LINKEDIN) && $_FILES['file']['size'] == 0) {
                            $this->_flashMessenger->addMessage('Linkedin Or cv is required !');
                            $this->_redirect('/final');
                            exit();
                        }
                    }
                    $flow = new WebApp_signin($this->_params['phone']);
                    $id = $flow->registerStep(3);
                    $this->_redirect('/home');
                    Zend_Session::namespaceUnset('flow');
                    Zend_Session::destroy('flow');
                    Zend_Session::destroy('recommender');
                    Zend_Session::namespaceUnset('recommender');
                    unset($flow);
                }
            }



        }
    }

    public function legalAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $content = $this->_model->companyhasTerms()['content'];
        echo '<pre>';
        $this->view->hasTerms = htmlspecialchars_decode($content);
    }

    public function aboutAction()
    {

    }

    public function insightsAction()
    {

    }

    public function clientsAction()
    {

    }

    public function favouritesAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        if(!zend_Auth::getInstance()->hasIdentity())
        {
            echo json_encode([]);
            exit();
        }
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getFavoritesJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }
    public function latestJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getLatestJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }
    public function jobsSectionsTotalAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        $total_jobs=[];
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
//            $offset = $this->_params['offset'];
        }

        $fav_jobs = $mJobs->getAllFavoritesSimilarJobs($filters);
        $total_jobs[] = count($fav_jobs);

        $first_jobs = $mJobs->getAllNotSharedJobs($filters);
        $total_jobs[] = count($first_jobs);

        $promoted_jobs = $mJobs->getAllPromotedJobs($filters);
        $total_jobs[] = count($promoted_jobs);

        $latest_jobs = $mJobs->getAllLatestJobs($filters);
        $total_jobs[] = count($latest_jobs);

        $perm_jobs = $mJobs->getAllPermanentJobs($filters);
        $total_jobs[] = count($perm_jobs);

        $temp_jobs = $mJobs->getAllTemporaryJobs($filters);
        $total_jobs[] = count($temp_jobs);

        echo json_encode($total_jobs);
        exit();
    }

    public function promotedJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getPromotedJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }

    public function temporaryJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getTemporaryJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }

    public function sharedJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getMostsharedJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }


    public function favouritesSimilarAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getFavoritesSimilarJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }

    public function permanentJobsAction()
    {
        header("Content-type: application/json");
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $mJobs = new models_Jobs ;
        $filters = [] ;
        $offset = 0 ;
        if($this->getRequest()->isPost()) {
            $filters = $this->_params['filters'];
            $offset = $this->_params['offset'];
        }
        $jobs = $mJobs->getpermanentJobs($filters,$offset);
        echo json_encode($jobs);
        exit();
    }

//    public function contactAction()
//    {
//
//    }
}
