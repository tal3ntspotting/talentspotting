<?php
class ErrorController extends Zend_Controller_Action
{
    /**
     * init
     */
    public function init()
    {
        $this->logger = Zend_Registry::get('Zend_Log');
        $this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;
    }

    public function errorAction()
    {
        $storage = [];
        $errors = $this->_getParam('error_handler');
        /*
        echo "<pre>";
        print_R($errors->exception);
        die();
        */
//        $storage = $errors->exception->getResponse();
        
        switch ($errors->type)
        {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                $this->view->errorType = 404;
            break;

            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER:
                // 404 error -- controller or action not found
                if (isset($storage['error']['type']) && $storage['error']['type'] != '')
                {
                    $this->getResponse()->setHttpResponseCode(503);
                    $this->view->message = 'Application error';
                    $this->view->errorType = 503;
                }
                else
                {
                    $this->getResponse()->setHttpResponseCode(403);
                    $this->view->message = 'Authorization failed';
                    $this->view->errorType = 403;
                    $this->logErrors($errors);
                }
            break;

            default:
                // application error
                $this->getResponse()->setHttpResponseCode(503);
                $this->logErrors($errors);
                $this->view->message = 'Application error';
                $this->view->errorType = 503;
            break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request   = $errors->request;
        $this->view->headTitle()->set($this->view->message);
    }

    protected function logErrors($errors)
    {
        $logger = Zend_Registry::get('Zend_Log');

        // Request URI
        $requestUri = $errors->request->getRequestUri();

        // Error message
        $erroMessage = $errors->exception->getMessage();

        // Find file with error
        $trace = $errors->exception->getTrace();
        $i = 0; $cnt= sizeof($trace); $file = null;
        while ( $i < $cnt && !$file )
        {
            if ( strcmp( $trace[$i]['function'], '__call') !=0 && strpos($trace[$i]['file'], '/Zend/') === false)
            {
                $file = $trace[$i];
            }

            $i++;
        }

        $create_function=function($item) {
            if(isset($item["file"])) return $item["file"];
        };

        $traceFiles = array_map($create_function, $trace);

        // Arguments
        if ( isset($file['args']) && is_array( $file['args'] ) )
        {
            array_pop($file['args']);
            $args = implode(' | ', $file['args']);
        }

        // Landing zone
        $zone = $errors->request->getParam('module') . '/' . $errors->request->getParam('controller') . '/' . $errors->request->getParam('action');
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $logger->log('  ' . $erroMessage, Zend_Log::ERR);
        $logger->log('URL = ' . 'http://' . $_SERVER['HTTP_HOST'] . $requestUri, Zend_Log::DEBUG);
        $logger->log('HTTP_REFERER = ' . $referer, Zend_Log::DEBUG);
        $logger->log('USER AGENT = ' .$_SERVER['HTTP_USER_AGENT'], Zend_Log::DEBUG);
        $logger->log('FILE = ' . $file['file']. ' :: line=' . $file['line'], Zend_Log::DEBUG);
        $logger->log('ZONE LANDING = ' . $zone, Zend_Log::DEBUG);
        $logger->log('CLASS/FUNCTION = ' . $file['class'] . ' -> ' . $file['function'], Zend_Log::DEBUG);

        foreach ( $trace as $k => $line )
        {
            $_line = isset($line['file']) ? $line['file'] : '';
            $traceAux = "TRACE $k = ..." . substr($_line, -47) . ' : line=' . $_line . ' : func=' . $line['function'];
            $traceAux .= ( $k == (count($trace)-1) ) ? PHP_EOL : '';
            $logger->log($traceAux, Zend_Log::DEBUG);
        }
    }
}
