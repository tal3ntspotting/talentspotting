<?php

/**
 * index controller for default module
 */
class AdminController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;
    protected $_payload;
    protected $_content;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_User;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;
        $body = $this->getRequest()->getRawBody();
        $this->_content = new models_Content();
//        if ($body)
//            $this->_payload = Zend_Json::decode($body, Zend_Json::TYPE_ARRAY);
    }

//    public function indexAction() {
//        $modelContent = new models_Content;
//        $modelJobs = new models_Jobs;
//        $modelCompany = new models_Company;
//
//        $this->view->countries = $modelContent->getCountriesSelect();
//        $this->view->skills = $modelContent->getSkillsSelect();
//        $this->view->industries = $modelContent->getIndustriesSelect();
//        $this->view->cities = $modelContent->getCitiesSelect();
//        $this->view->departments = $modelContent->getDepartmentsSelect();
//        $this->view->companies = $modelCompany->getCompaniesSlugSelectHomeWithJobs();
//        $this->view->news = $modelContent->getLatestNews(2);
//
//        $banners = $modelJobs->getBannersFiltered(array("position" => "home", "status" => 1));
//        $jobs_limit = (!empty($banners)) ? 3 : 4;
//        if ($this->_settings['app_type'] == 'hr') {
//            $jobs_limit = null;
//        }
//
//        $jobs_limit = 12;
//        $filters['is_active'] = 1;
//
//        $session = new Zend_Session_Namespace("active_subdomain");
//        if ($session->active_subdomain > 0) {
//            $subdomain = $modelContent->getSubdomainById($session->active_subdomain);
//            $filters['id_country'][] = $subdomain['id_country'];
//        }
//
//        $this->view->jobs = $modelJobs->getJobs($filters, $jobs_limit);
//        $this->view->filters = $filters;
//        $this->view->banners = $banners;
//
//        if ($this->getRequest()->isPost()) {
//            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
//                $transport = null;
//                if (!empty($this->_settings['email_smtp'])) {
//                    $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
//                    $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
//                }
//
//                $mail = new WebApp_Mail();
//                $mail->addTo("hello@'.$this->_settings['sitename'].'", '' . $this->_settings['sitename'] . '');
//                //$mail->addTo("marius@mkt2.ro", ''.$this->_settings['sitename'].'');
//                $mail->setFrom($this->_params['email'], $this->_params['contact_name']);
//                $mail->setSubject("New exhibitor form on '.$this->_settings['sitename'].'");
//                $mail->setBodyFromViewScript("exhibitor.phtml", array("data" => $_POST));
//                $mail->send($transport);
//
//                echo json_encode(array("msg" => "ok", "text" => $this->translate->_("email_success")));
//            } else {
//                echo json_encode(array("error" => $this->translate->_("email_error")));
//            }
//
//            exit();
//        }
//
//        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(1, 3) . ' | ' . $this->_settings['sitename']);
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
//    

    public function sitemapAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $modelContent = new models_Content;
        $modelCompany = new models_Company;
        $modelJobs = new models_Jobs;

        $cities = $modelContent->getCitiesSlugSelect();
        $domains = $modelContent->getDepartmentsSlugSelect();
        $countries = $modelContent->getCountriesSlugSelect();
        $industries = $modelContent->getIndustriesSlugSelect();
        $companies = $modelCompany->getCompaniesSlugSelectWithJobs();
//        $pages = $modelContent->getPagesSlugSelect();

        $filters['is_active'] = 1;

        $session = new Zend_Session_Namespace("active_subdomain");
        if ($session->active_subdomain > 0) {
            $subdomain = $modelContent->getSubdomainById($session->active_subdomain);
            $filters['id_country'][] = $subdomain['id_country'];
        }
        $jobs = $modelJobs->getAllJobs($filters);


        $sitemap = new DOMDocument('1.0', 'utf-8');
        $root = $sitemap->createElement("urlset");
        $sitemap->appendChild($root);

        $root_attr = $sitemap->createAttribute('xmlns');
        $root->appendChild($root_attr);

        $root_attr_text = $sitemap->createTextNode('http://www.sitemaps.org/schemas/sitemap/0.9');
        $root_attr->appendChild($root_attr_text);

        foreach ((array) $companies as $company) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/company~" . $company . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $countries as $country) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/countries~" . $country . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $cities as $city) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/cities~" . $city . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $domains as $domain) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/domains~" . $domain . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $industries as $industry) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/industries~" . $industry . "/";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

        foreach ((array) $jobs as $job) {
            $url = $sitemap->createElement("url");
            $root->appendChild($url);

            $loc = $sitemap->createElement("loc");
            $lastmod = $sitemap->createElement("lastmod");
            $changefreq = $sitemap->createElement("changefreq");
            $priority = $sitemap->createElement("priority");

            $http_url = APPLICATION_URL . "/jobs/{$job['id']}/{$job['slug']}/{$job['company_slug']}";
            $url->appendChild($loc);
            $url_text = $sitemap->createTextNode($http_url);
            $loc->appendChild($url_text);

            $url->appendChild($lastmod);
            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
            $lastmod->appendChild($lastmod_text);

            $url->appendChild($changefreq);
            $changefreq_text = $sitemap->createTextNode("daily");
            $changefreq->appendChild($changefreq_text);

            $url->appendChild($priority);
            $priority_text = $sitemap->createTextNode("1.0");
            $priority->appendChild($priority_text);
        }

//        foreach ((array) $pages as $page) {
//            $url = $sitemap->createElement("url");
//            $root->appendChild($url);
//
//            $loc = $sitemap->createElement("loc");
//            $lastmod = $sitemap->createElement("lastmod");
//            $changefreq = $sitemap->createElement("changefreq");
//            $priority = $sitemap->createElement("priority");
//
//            $http_url = APPLICATION_URL . "/page/{$page}";
//            $url->appendChild($loc);
//            $url_text = $sitemap->createTextNode($http_url);
//            $loc->appendChild($url_text);
//
//            $url->appendChild($lastmod);
//            $lastmod_text = $sitemap->createTextNode(date("Y-m-d"));
//            $lastmod->appendChild($lastmod_text);
//
//            $url->appendChild($changefreq);
//            $changefreq_text = $sitemap->createTextNode("daily");
//            $changefreq->appendChild($changefreq_text);
//
//            $url->appendChild($priority);
//            $priority_text = $sitemap->createTextNode("1.0");
//            $priority->appendChild($priority_text);
//        }

        $output = $sitemap->saveXML();
        $this->getResponse()->setHeader('Content-Type', 'text/xml; charset=utf-8')->setBody($output);
    }

    public function searchAction() {
        $modelContent = new models_Content;
        $cities = $modelContent->getCitiesSlugSelect();
        $domains = $modelContent->getDepartmentsSlugSelect();
        $skills = $modelContent->getSkillsSlugSelect();
        $industries = $modelContent->getIndustriesSlugSelect();
        $country = $modelContent->getCountriesSlugSelect();


        foreach ($this->_params as $key => $value) {
            if (array_key_exists($key, $this->_settings['filters'])) {
                $var = $this->_settings['filters'][$key];
                if (is_array($value)) {
                    foreach ($value as $val) {
                        if (array_key_exists($val, $$var) && $val != "") {
                            $method = $$var;
                            $filters[$this->_settings['filters'][$key]][] = $method[$val];
                        }
                    }
                } else {
                    if ($value != "") {
                        $filters[$this->_settings['filters'][$key]] = $value;
                    }
                }
            }
        }

        $url = $this->_getSearchUrl((array) $filters);

        $this->_redirect($url);
    }

    private function _getSearchUrl($filters = array()) {
        $url = "/jobs/";
        if (!empty($filters)) {
            foreach ($filters as $filter => $value) {
                if (is_array($value)) {
                    $url .= $filter . "~" . implode("-", $value) . "_";
                } else {
                    $url .= $filter . "~" . $value . "_";
                }
            }
        }
        $url = substr($url, 0, -1);
        return $url;
    }

    public function lostPasswordAction() {
        $email = $this->_params['email'];
        $message = $this->_model->resetPassword($email);
        $msg = (strpos($message, "@") !== false) ? sprintf($this->translate->_("recover_success"), $message) : $this->translate->_("recover_error");
        $this->_flashMessenger->addMessage($msg);
        $this->_redirect("/login");
    }

    public function resetAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('login');

        $token = $this->_params['token'];

        if ($this->getRequest()->isPost()) {
            if (!$user = $this->_model->getUserByToken($token)) {
                $msg = $this->translate->_("expired_token");
                $this->_flashMessenger->addMessage($msg);
            } else {
                $password = sha1($this->_params['password']);
                $this->_model->update(array("password" => $password), "id = {$user['id']}", "users");
                $msg = $this->translate->_("success_changed_password");
                $this->_flashMessenger->addMessage($msg);
            }

            $this->_redirect("/login");
        }

        if (!$user = $this->_model->getUserByToken($token)) {
            $msg = $this->translate->_("expired_token");
            $this->view->msg = $msg;
        }

        $this->view->headTitle()->set($this->translate->_('reset_password') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(1);
    }

    public function logoutAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        Zend_Auth::getInstance()->clearIdentity();
        session_destroy();
        $past = time() - 3600;
        foreach ( $_COOKIE as $key => $value )
        {
            setcookie( $key, $value, $past, '/' );
        }
        Zend_Session::namespaceUnset('url');

        $this->_redirect('/');
        exit();
    }

    public function loginAction() {
        //        $db = new Zend_Db_Adapter_Pdo_Mssql(array(
//            'host' => 'MSSQL', // parklife
//            'username' => 'SA',
//            'password' => 'Edin123!',
//            'dbname' => 'lidl',
//            'pdoType' => 'dblib')
//        );
//        $sql = 'SELECT * FROM terms';
//        $result = $db->fetchAll($sql);
//        echo '<pre>';
//        print_r($result);
//        die;
//        $this->_modelCompany->test();
        $this->view->fb_url = "https://www.facebook.com/dialog/oauth?client_id={$this->_settings['facebook_edin']['api_id']}&redirect_uri={$this->_settings['sitename']}/users/index/facebook-signup&scope=email,user_friends,public_profile";
        $check_is_external = $this->_settings['external_platform'];
        if (Zend_Auth::getInstance()->hasIdentity()) {
            if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
                $this->_redirect('/dashboard');
            } else {
                $this->_redirect('/home');
            }
        }
        $sess = new Zend_Session_Namespace('signin');

        if (isset($sess->access) && $sess->access == 1) {
            $this->view->signIn = 1;
        }
//        $this->_helper->viewRenderer->setRender('login-mobile');
//            $this->renderScript('index/login.mobile.phtml');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->current_menu = "login";

        $session = new Zend_Session_Namespace("redirector");

        if ($this->getRequest()->isPost()) {
            $dbAdapter = Zend_Registry::get('dbMysqlAdapter');
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $user = $this->_model->getUserDataByEmail($this->_params['email']);
            WebApp_Tools::Debug($user);
            WebApp_Tools::Debug($_POST);
            WebApp_Tools::Debug(sha1($_POST['password']) . $user['salt']);
            if (empty($user) && $user['salt'] != "")
                $salt = "";
            else {
                $salt = $user['salt'];
            }
            $authAdapter->setTableName('users')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password')
//                    ->setCredentialTreatment("CONCAT(SHA1(?),'$salt') AND status = 1 AND type != 2")
                    ->setIdentity($this->_params['email'])
                    ->setCredential(sha1($this->_params['password']) . $user['salt']);
            $authAdapter->getDbSelect()->where("status = 1 AND type = 2");

            $result = $authAdapter->authenticate();

            switch ($result->getCode()) {
                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    $this->_flashMessenger->addMessage('no_account_or_invalid');
                    break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    $this->_flashMessenger->addMessage('credentials_incorrect');
                    $this->view->email = $this->_params['email'];
                    return $this->renderScript('index/password.mobile.phtml');
                    break;

                case Zend_Auth_Result::SUCCESS:
                    if ($userInfo = $authAdapter->getResultRowObject(null, 'password')) {
                        if ($userInfo->id_company > 0 && $userInfo->type == 2) {
                            $this->_model->insert(array("id_user" => $userInfo->id, "view" => 1), "user_tooltip_view");
                        }

                        Zend_Auth::getInstance()->getStorage()->write($userInfo);
                        $this->_model->userLoggedIn();
                        if (Zend_Auth::getInstance()->getIdentity()->first_login == NULL) {
                            $this->_model->setFirstTimeLogin(Zend_Auth::getInstance()->getIdentity()->id);
                        }
//                        $this->activatedAccountGamificationPointAction($userInfo->id_company, $userInfo->id);
                        $isFirst = $this->_model->isFirstLogin($userInfo->id);
                        $this->_model->setActivateAccountbyCampaign($userInfo);
                        if ($isFirst['last_login'] == "" && $userInfo->id_company == 470) {
                            $first_login = new Zend_Session_Namespace("first-login");
                            $first_login->access = 1;
                        }
                        $this->_model->update(array('last_login' => date('Y-m-d H:i:s'), 'last_login_ip' => ip2long(getenv("REMOTE_ADDR"))), "id = {$userInfo->id}");

                        if (isset($this->_params['id_package'])) {
                            $this->_redirect("/recharge/?id_package={$this->_params['id_package']}");
                        }
                        $isPilote = $this->_model->isPilote($userInfo->id_company);
                        if ($isPilote['is_pilote'] == 1) {
                            $result = $this->_model->countPilote($userInfo->id_company);
                            if ($result >= 51) {
                                Zend_Auth::getInstance()->clearIdentity();
                                $msg = $this->translate->_("pilote-limit");
                                $this->_flashMessenger->addMessage($msg);
                                $this->_redirect('/login');
                            }
                        }

                        /*
                          if ($session->url != "" && strpos($session->url, "delete") === false)
                          {
                          $url = $session->url;
                          unset($session->url);
                          $this->_redirect($url);
                          }
                         */

                        if ($userInfo->type == 2 || $userInfo->type == 3) {
                            $this->_redirect('/dashboard');
                        } elseif ($userInfo->type == 5 || $userInfo->type == 4) {
                            $this->_redirect('/users/admin/mycvs');
                        } else {
                            $this->_redirect('/home');
                        }
                    } else {
                        $msg = 'login_error_auth';
                        $this->_redirect('/admin');
                    }
                    break;

                default:
                    $msg = 'login_error_auth';
                    break;
            }

            $this->_flashMessenger->addMessage($msg);
            $this->_redirect('/admin');
        }
    }

    public function dashboardAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }
        if (Zend_Auth::getInstance()->hasIdentity() && Zend_Auth::getInstance()->type == 9) {
            $this->_redirect("/profile");
        }
        $user = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id) ;
//        echo '<pre>';print_r($user->isLogged());die;
        $this->view->current_menu = 'dashboard';
        $modelReports = new models_Reports;
        $this->view->new_applications = $modelReports->getDashboardsNewApplications();
        $this->view->campaigns = $modelReports->getDashboardCampaignsInfo();
//        $this->view->campaigns_views = $modelReports->getDashboardCampaignsViewsInfo();
        $this->view->campaigns_shares = $modelReports->getDashboardCampaignsSharesInfo();
        $this->view->topDepartmentRecommenders = $modelReports->topDepartmentRecommenders();
        $jobData = $modelReports->getnumjobs();
        $this->view->numJobs = $jobData['job'];
        $numjobid = $jobData['id'];
        $this->view->RecommenderNumforjobById = $modelReports->RecommenderNumforjobById($numjobid);
        $this->view->recommendersTotal = $modelReports->recommendersTotal((int) Zend_Auth::getInstance()->getIdentity()->id_company);
        $arr['top_job']["{$this->view->numJobs['job']}"] = $this->view->numJobs['recommendations'];
        $arr['top_department'][$this->view->topDepartmentRecommenders['title_department']] = $this->view->topDepartmentRecommenders['recommenders'];
        $arr['top_recommneders']["Total Recommenders"] = $this->view->recommendersTotal['recommendersTotal'];
        $this->view->leaderboards = $arr;
        $this->view->engagement = $modelReports->getDashboardEmployeesEngagement();
        $this->view->top_channels = $modelReports->getDashboardTopChannels();
//        echo '<pre>'; print_r($this->view->top_channels); die;
        $this->view->top_viewed_jobs = $modelReports->getDashboardTopViewedJobs();

        $this->view->refferals_by_departments = $modelReports->getDashboardTopRefferals();
        $this->view->headTitle()->set($this->translate->_('dashboard') . ' | ' . $this->_settings['sitename'] . '');
        $trans['t_channel'] = $this->translate->_('Views & applications by recruiting channel source');
        $trans['app_rate'] = $this->translate->_('Jobs application rate looks great at');
        $trans['app_conversion'] = $this->translate->_('Applicants conversion rate from candidates base can be improved from <b> candidate_through_applicants %, by asking your employees to send reminders to their referees');
        $trans['engaged_employee'] = $this->translate->_('Employees promoters score should be improved from <b> a %</b> out of your <b> engaged_employees_no </b> currently signed in employees. Your potential could grow even more if we engage (convince to sign in) more than currently <b> b_data %</b> of employees.');
        $trans['engaged_great'] = $this->translate->_('Employees engagement looks great at');
        $trans['employee_shares'] = $this->translate->_('Employee shares');
        $trans['employee_created'] = $this->translate->_('Employees created');
        $trans['active_employees'] = $this->translate->_('Active employees');
        $trans['eng_employees'] = $this->translate->_('Engaged employees');
        $trans['promoters'] = ucfirst($this->translate->_('promoters'));
        $trans['shared_jobs'] = ucfirst($this->translate->_('Shared jobs'));
        $trans['viewed_jobs'] = ucfirst($this->translate->_('Viewed jobs'));
        $trans['jobs_with_applications'] = ucfirst($this->translate->_('Jobs with applications'));
        $trans['job_views'] = ucfirst($this->translate->_('Job views'));
        $trans['candidates_accounts'] = ucfirst($this->translate->_('Candidates accounts (base)'));
        $trans['candidates_new'] = ucfirst($this->translate->_('New candidate accounts'));
        $trans['unique_candidates'] = ucfirst($this->translate->_('Unique candidates'));
        $trans['unique_employees'] = ucfirst($this->translate->_('Unique employees'));
        $trans['unique_applicants_status'] = ucfirst($this->translate->_('Unique applicants in status'));
        $trans['total_applications'] = ucfirst($this->translate->_('Total applications'));
        $trans['views'] = ucfirst($this->translate->_('views'));
        $trans['applications'] = ucfirst($this->translate->_('applications'));
        $trans['number'] = ucfirst($this->translate->_('number'));
        $trans['jobs_app_rate'] = ucfirst($this->translate->_('Jobs application rate can be improved from'));
        $trans['engaged'] = ucfirst($this->translate->_('engaged'));
        $trans['current'] = ucfirst($this->translate->_('current'));
        $trans['promoters'] = ucfirst($this->translate->_('promoters'));
        $trans['e_over_one_job'] = ucfirst($this->translate->_('Employees that promoted at elast one job'));
        $trans['new'] = ucfirst($this->translate->_('New'));
        $trans['viewed'] = ucfirst($this->translate->_('Viewed'));
        $trans['hired'] = ucfirst($this->translate->_('Hired'));
        $trans['called'] = ucfirst($this->translate->_('Called'));
        $trans['interview'] = ucfirst($this->translate->_('Interview'));
        $trans['test'] = ucfirst($this->translate->_('Test'));
        $trans['offer'] = ucfirst($this->translate->_('Offer'));
        $trans['final_interview'] = ucfirst($this->translate->_('Final interview'));
        $trans['end'] = ucfirst($this->translate->_('end'));
        $trans['background_check'] = ucfirst($this->translate->_('Background Check'));
        $trans['assesment'] = ucfirst($this->translate->_('Assesment'));
        $trans['confirm_delete_user'] = $this->translate->_('Are you sure you want to delete this user');
        $trans['confirm_duplicate'] = $this->translate->_('Are you sure you want to duplicate this item');
        $trans['confirm_delete_job'] = $this->translate->_('Are you sure you want to delete this job');
        $trans['confirm_activate_item'] = $this->translate->_('Are you sure you want to activate this item');
        $trans['view_all'] = $this->translate->_('view all');
//        $trans['new'] = ucfirst($this->translate->_('New'));
        $trans = json_encode($trans);
        $this->view->dash_js = "<script> var trans_key = $trans;</script>";
//        die('asasa');
    }

    public function hwroksAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->view->content = preg_replace("/\r|\n/", "", $this->_model->getHowItWorks()['content']);
    }

    public function editWorksAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $data = base64_decode($this->_params['data']);
            if ($data != "") {
                $this->_model->setHowitWorksContent($data);
            } else {
                // disable how it works tabs
                $this->_model->disableHowitWorksContent();
            }
        }
        $this->view->content = preg_replace("/\r|\n/", "", $this->_model->getHowItWorks()['content']);
    }

    public function analyticsAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }


        $modelReports = new models_Reports();
        $this->view->current_menu = 'analytics';
        $this->view->current_sub_menu = 'stats';
        $this->view->current_sub_menu = 'leaderboards';
        $this->view->skills = $this->_model->getUSerSkillBars();
        $this->view->closing = $this->_model->getJobClosingRate();
        $this->view->company_overview = $this->_model->getCompanyOverview();
        $this->view->statuses = $this->_model->getCandidatesByStatus();
        $this->view->refferals_by_departments = $modelReports->getDashboardTopRefferals();
        $this->view->campaigns = $modelReports->getDashboardCampaignsInfo();
        $this->view->engagement = $modelReports->getDashboardEmployeesEngagement();
        $this->view->top_channels = $modelReports->getDashboardTopChannels();
        $this->view->referrals_by_jobs = $modelReports->getTopJobRefferals();
        $modelJob = new models_Jobs;
        $this->view->allStatus = $modelJob->getStatus();
    }

    public function setLangAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $lang = $this->_params['lang'];

        $redir = "/";

        if ($_SERVER['HTTP_REFERER'] != "" && strpos($_SERVER['HTTP_REFERER'], "/page/") !== false) {
            $slug = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], "/") + 1);
            $current_lang = APPLICATION_LOCALE;
            $languages = Zend_Registry::get('_LANG_DETAILS');
            $id_lang = $languages[$current_lang];

            if ($id_lang > 0) {
                $modelContent = new models_Content($id_lang);
                $page = $modelContent->getPageBySlug($slug);
                $id = $page['id'];

                $modelContent = new models_Content($languages[$lang]);
                $page = $modelContent->getPageById($page['id']);

                $redir = "/page/" . $page['slug'];
            }
        } else if ($_SERVER['HTTP_REFERER'] != "" && strpos($_SERVER['HTTP_REFERER'], "/jobs/") !== false) {
            $url = explode("/", $_SERVER['HTTP_REFERER']);
            $id = $url[4];

            $current_lang = APPLICATION_LOCALE;
            $languages = Zend_Registry::get('_LANG_DETAILS');
            $id_lang = $languages[$current_lang];

            if ($id_lang > 0) {
                $modelJobs = new models_Jobs($id_lang);
                $job = $modelJobs->getJobById($id);

                if (!empty($job)) {
                    $modelJobs = new models_Jobs($languages[$lang]);
                    $job = $modelJobs->getJobById($id);

                    if (!empty($job)) {
                        $redir = "/" . WebApp_Tools::createJobSlug($job);
                    } else {
                        $redir = $_SERVER['HTTP_REFERER'];
                    }
                } else {
                    $redir = $_SERVER['HTTP_REFERER'];
                }
            } else {
                $redir = $_SERVER['HTTP_REFERER'];
            }
        } else {
            $redir = $_SERVER['HTTP_REFERER'];
        }

        if (array_key_exists($lang, $this->_settings['languages_names'])) {
            $namespace = new Zend_Session_Namespace('Language');
            $change = new Zend_Session_Namespace('language_change');
            $namespace->lang = $lang;
            $change->lang = $lang;
        } else {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }
        $this->_redirect($redir);
    }

    public function linkedinAction() {
        $ns = new Zend_Session_Namespace('linkedin_oauth');

        $options = array(
            'localUrl' => APPLICATION_URL . '/register-linkedin/',
            'callbackUrl' => APPLICATION_URL . '/register-linkedin/',
            'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken',
            'userAuthorizationUrl' => 'https://api.linkedin.com/uas/oauth/authorize',
            'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $consumer = new Zend_Oauth_Consumer($options);

        if (empty($ns->request_token)) {
            $token = $consumer->getRequestToken();
            $ns->request_token = serialize($token);
            $ns->token = $token;
            $consumer->redirect();
            die();
        } else {
            try {
                $token = $consumer->getAccessToken($_GET, unserialize($ns->request_token));
                $ns->token = $token;
                $this->_model->update(array("linkedin_token" => serialize($token)), "id = '{$this->_model->_user->id}'", "users");
            } catch (Exception $e) {
                $this->_model->update(array("linkedin_token" => ""), "id = '{$this->_model->_user->id}'", "users");
                $ns->request_token = '';
            }
        }

        return $this->_redirect('/linkedin-details');
    }

    public function linkedinDetailsAction() {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $slug = $this->_params['slug'];

        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $token = $ns->token;

        $client = $token->getHttpClient($options);

        $client->setUri('https://api.linkedin.com/v1/people/~:(id,firstName,lastName,industry,location,headline,current-status,positions,educations,skills,certifications,pictureUrl,email-address)');
        $client->setMethod(Zend_Http_Client::GET);
        $client->setHeaders("x-li-format", "json");
        $response = $client->request();
        $content = $response->getBody();
        $data = Zend_Json::decode($content);

        if (!$user = $this->_model->getUserDetailsByEmail($data['emailAddress'])) {
            $user_data['firstname'] = $data['firstName'];
            $user_data['lastname'] = $data['lastName'];
            $user_data['title'] = $data['headline'];
            $user_data['firstname'] = $data['firstName'];
            $user_data['location'] = $data['location']['name'];
            $user_data['image'] = $data['pictureUrl'];
            $user_data['linkedin_token'] = serialize($token);
            $user_data['linkedin_id'] = $data['id'];
            $user_data['status'] = 1;
            $user_data['type'] = 1;
            $user_data['email'] = $data['emailAddress'];
            $user_data['password'] = uniqid();
            $user = $this->_model->saveFromLinkedin($user_data);

            $this->_model->setUserCertifications($data['certifications'], $user['id']);
            $this->_model->setUserEducations($data['educations'], $user['id']);
            $this->_model->setUserPositions($data['positions'], $user['id']);
            $this->_model->setUserSkills($data['skills'], $user['id']);
        } else {
            $this->_flashMessenger->addMessage($this->translate->_("account-exists"));
            $this->_redirect('/register');
        }

        $ns->request_token = null;
        $ns->token = null;

        $this->_redirect("/register-linkedin-skills");
    }

    private function _getLinkedinProfile($url) {
        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );
        $user = $this->_model->getUserById($this->_model->_user->id);

        $token = unserialize($user['linkedin_token']);
        $client = $token->getHttpClient($options);

        $client->setUri($url);
        $client->setMethod(Zend_Http_Client::GET);
        $response = $client->request();
        $content = $response->getBody();

        $data = json_decode($content);

        echo "<pre>";
        print_R($content);
        die();
    }

    public function linkedinMessageAction() {
        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );
        $user = $this->_model->getUserById($this->_model->_user->id);

        $token = unserialize($user['linkedin_token']);
        $client = $token->getHttpClient($options);

        $values[]['person']['_path'] = '/people/CPpLTCDNVi';
        $values[]['person']['_path'] = '/people/~';
        $message = json_encode(array("recipients" => array("values" => $values), "subject" => "test message din platforma", "body" => "tralalalal"));
        //die($message);
        $client->setUri('http://api.linkedin.com/v1/people/~/mailbox');
        $client->setMethod(Zend_Http_Client::POST);
        $client->setHeaders("Content-Type", "application/json");
        $client->setHeaders("x-li-format", "json");
        $client->setRawData($message);
        $response = $client->request();
        $content = $response->getBody();

        $this->_redirect("/dashboard");
    }

    public function registerAction() {
        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();

        if ($this->getRequest()->isPost()) {
            unset($_POST['password_confirm']);
            if (!empty($_POST['company'])) {
                $company = $_POST['company'];
                unset($company['password_confirm']);
                if (!$user = $this->_model->getUserByEmail($company['email'])) {
                    $userData['firstname'] = $company['firstname'];
                    $userData['lastname'] = $company['lastname'];
                    $userData['password'] = $company['password'];
                    $userData['email'] = $company['email'];
                    $userData['type'] = 2;
                    $userData['status'] = 1;
                    $userData['created'] = date("Y-m-d H:i:s");
                    if ($id_user = $this->_model->saveUser($userData, true)) {
                        $companyData['title'] = $company['title'];
                        $companyData['email'] = $company['email'];
                        $companyData['id_country'] = $company['id_country'];
                        $companyData['id_city'] = $company['id_city'];
                        $companyData['description'] = $company['description'];
                        $companyData['address'] = $company['address'];
                        $companyData['phone'] = $company['phone'];
                        $companyData['status'] = 1;

                        $id_company = $this->_model->saveCompany($companyData);
                        $this->_model->update(array("id_company" => $id_company, "is_company_admin" => 1), "id = {$id_user}", "users");
                        //$this->_model->insert(array("id_user" => $id_user, "id_company" => $id_company), "company_users");

                        $this->_model->authenticate($userData['email'], $userData['password']);

                        $this->_redirect('/recharge');
                    } else {
                        $msg = $this->translate->_("account-exists");
                    }
                } else {
                    $msg = $this->translate->_("account-exists");
                }
            } else {
                if (!$user = $this->_model->getUserByEmail($_POST['email'])) {
                    $_POST['type'] = 1;
                    $_POST['status'] = 1;
                    $_POST['created'] = date("Y-m-d H:i:s");
                    $this->_model->saveUser($_POST, true);
                    $this->_model->authenticate($_POST['email'], $_POST['password']);
                    $this->_redirect('/register-email');
                } else {
                    $msg = $this->translate->_("account-exists");
                }
            }

            $this->_flashMessenger->addMessage($msg);
            $this->_redirect('/login');
        }

        $this->view->headTitle()->set($this->translate->_('register') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
    }

    public function registerCompanyAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('login');

        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();
        $this->view->id_package = $this->_params['id_package'];

        if ($this->getRequest()->isPost()) {
            if (!empty($_POST)) {
                $company = $_POST['company'];
                if (!$user = $this->_model->getUserByEmail($company['email'])) {
                    $userData['firstname'] = $company['firstname'];
                    $userData['password'] = $company['password'];
                    $userData['lastname'] = $company['lastname'];
                    $userData['email'] = $company['email'];
                    $userData['type'] = 2;
                    $userData['status'] = 1;
                    $userData['created'] = date("Y-m-d H:i:s");
                    if ($id_user = $this->_model->saveUser($userData, true)) {
                        $companyData['title'] = $company['title'];
                        $companyData['email'] = $company['email'];
                        $companyData['id_country'] = $company['id_country'];
                        $companyData['id_city'] = $company['id_city'];
                        $companyData['address'] = $company['address'];
                        $companyData['status'] = 1;

                        $id_company = $this->_model->saveCompany($companyData);
                        $this->_model->update(array("id_company" => $id_company, "is_company_admin" => 1), "id = {$id_user}", "users");

                        $this->_model->authenticate($userData['email'], $userData['password']);

                        $this->_redirect('/recharge/?id_package=' . $this->_params['id_package']);
                    } else {
                        $msg = $this->translate->_("account-exists");
                    }
                } else {
                    $msg = $this->translate->_("account-exists");
                }
            }
        }

        $this->view->headTitle()->set($this->translate->_('register') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
    }

    public function checkAuthAction() {
        echo (string) Zend_Auth::getInstance()->hasIdentity();
        exit();
    }

    public function loginRechargeAction() {
        if ($user = $this->_model->authenticate($this->_params['email'], $this->_params['password'])) {
            $message['msg'] = 'ok';
            $message['mode'] = "redirect";
            $message['url'] = APPLICATION_URL . "/recharge/id_package/" . $this->_params['id_package'];
        } else {
            $message['msg'] = 'error';
            $message['error'] = $this->translate->_("no_account_or_invalid");
        }

        echo json_encode($message);
        die();
    }

    public function clickAction() {
        $id = $this->_params['id'];
        $type = $this->_params['type'];

        $url = "/";

        switch ($type) {
            case "banner":
                $modelJobs = new models_Jobs;
                $banner = $modelJobs->getBannerById($id);

                if (!empty($banner)) {
                    $url = $banner['url'];
                    $modelJobs->update(array("clicks" => $banner['clicks'] + 1, "views" => $banner['views'] + 1), "id = {$id}", "banners");
                }
                break;
        }

        $this->redirect($url);
    }

    public function registerEmailAction() {
        $this->view->user = $this->_model->_user;
    }

    public function registerLinkedinSkillsAction() {
        $this->view->headTitle()->set($this->translate->_('login') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);

        $this->view->user = $this->_model->_user;
        $this->view->skills = $this->_model->getUserSkills();
    }

    public function signInAction() {
        $this->view->headTitle()->set($this->translate->_('login') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
    }

    public function loginCompanyAction() {
        $this->view->countries = $this->_model->getCountries();
        $this->view->cities = $this->_model->getCities();
        $this->view->id_package = $this->_params['id_package'];

        $this->view->headTitle()->set($this->translate->_('login') . ' | ' . $this->_settings['sitename'] . '');
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(1, 2));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 1));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(2);
    }

    public function xlExportAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }


        $modelReports = new models_Reports();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->view->skills = $this->_model->getUSerSkillBars();
        $this->view->closing = $this->_model->getJobClosingRate();
        $this->view->company_overview = $this->_model->getCompanyOverview();
        $this->view->statuses = $this->_model->getCandidatesByStatus();
        $this->view->refferals_by_departments = $modelReports->getDashboardTopRefferals();
        $this->view->campaigns = $modelReports->getDashboardCampaignsInfo();
        $this->view->engagement = $modelReports->getDashboardEmployeesEngagement();
        $this->view->top_channels = $modelReports->getDashboardTopChannels();
        $this->view->referrals_by_jobs = $modelReports->getTopJobRefferals();

        $filename = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'emails' . "/xls.phtml";
        $handle = fopen($filename, "r");
        $fcontents = fread($handle, filesize($filename));
        fclose($handle);
        $smarty = new Smarty;
        $smarty->assign("skill", $this->view->skills);
        $smarty->assign("closing", $this->view->closing);
        $smarty->assign("company_overview", $this->view->company_overview);
        $smarty->assign("statuses", $this->view->statuses);
        $smarty->assign("application_status", $this->_settings['application_status']);
        $smarty->assign("refferals_by_departments", $this->view->refferals_by_departments);
        $smarty->assign("engagement", $this->view->engagement);
        $smarty->assign("top_channels", $this->view->top_channels);
        $smarty->assign("referrals_by_jobs", $this->view->referrals_by_jobs);

        $resource = $smarty->fetch('string:' . $fcontents);
        echo $resource;
//        die;
        $file_name = "analytics_" . date("Y-m-d H:i:s") . ".xls";
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename={$file_name}");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        exit();
    }

    public function getAllCitiesJobsByCompanyAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            throw new Exception('You need To be logged In');
            exit();
        }
        Zend_Layout::getMvcInstance()->disableLayout();

        $allUsedCities = [];
        $jobModel = new models_Jobs();
        $modelcontent = new models_Content();
        $user = Zend_Auth::getInstance()->getIdentity();
        $id_company = (int) $user->id_company;
//        $jobs = $jobModel->getJobsByCompanyId($id_company);
        $jobs = $jobModel->getJobs();
        $custom_cities = $modelcontent->getCustomCitiesSelect();
        $default_cities = $modelcontent->getCitiesSelect();

        $structured_custom_cities = [];
        foreach ($custom_cities as $key => $c) {
            $structured_custom_cities[] = [
                'id' => $key,
                'city' => $c,
                'type' => 'custom_cities'
            ];
        } foreach ($default_cities as $key => $c) {
            $structured_custom_cities[] = [
                'id' => $key,
                'city' => $c,
                'type' => 'custom_cities'
            ];
        }

        $allJobsUsedCities = [];
        foreach ($jobs as $key => $job) {
            $all_cities = $jobModel->getJobCities($job['id']) + $jobModel->getJobCustomCities($job['id']);
            $jobs[$key]['custom_cities'] = $all_cities;
            $allJobsUsedCities[] = array_keys($all_cities);
        }
        $associative_cities = [];
        for ($i = 0; $i <= count($allJobsUsedCities); $i++) {
            foreach ($allJobsUsedCities[$i] as $value) {
                $associative_cities[] = $value;
            }
        }
        $returned_used_cities = [];
        foreach ($structured_custom_cities as $key => $city) {
            if (in_array($city['id'], $associative_cities))
                $returned_used_cities[] = $structured_custom_cities[$key];
        }
        echo json_encode(['cities' => $returned_used_cities]);
        exit();
    }

    public function getPercentAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            exit();
        }
        $date = $this->_payload['date'];
        $cities = (array) $this->_payload['cities'];

        $start_date = date("Y-m-d");
//        $end_date = date("Y-m-d",strtotime("-$date days"));
        $oneMonthAgo = date("Y-m-d", strtotime("-30 days"));
        $twoMonthAgo = date("Y-m-d", strtotime("-60 days"));

        $items = $this->_content->getApplicationByDateAndCities($start_date, $oneMonthAgo);
        $items_comparisation = $this->_content->getApplicationByDateAndCities($oneMonthAgo, $twoMonthAgo);

        $sign = ($items['applications'] >= $items_comparisation['applications']) ? "+" : "-";

        $one_percent = ($items_comparisation['applications'] / 100);
        echo '<pre>';
        print_r($sign . ($one_percent * 100) . '%');
        print_r($items);
        print_r($items_comparisation);
        echo json_encode($items, true);
        exit();
    }

    public function getBoxesAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            exit();
        }
        $items = $this->_content->getBoxesContent();
        echo json_encode(['boxes' => $items], true);
        exit();
    }

    public function getDashboardAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));

        $user_created_chart = $this->_model->getStatsCreatedUsers($start, $end);
        $total_users = $this->_model->countCreatedUserByPeriod($start, $end);
        $vs_prior_period = $this->_model->countCreatedUserByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countCreatedUserByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['users_created']['chart'] = $user_created_chart;
        $data[0]['users_created']['total_users'] = $total_users;
        $data[0]['users_created']['vs_prior_period'] = $vs_prior_period;
        $data[0]['users_created']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['users_created']['vs_ly'] = $vs_ly;
        $data[0]['users_created']['vs_ly_percent'] = $vs_ly_percent;

        $active_employees_chart = $this->_model->getActiveEmployees($start, $end);
        $total_users = $this->_model->countActiveEmployeesByPeriod($start, $end);
        $vs_prior_period = $this->_model->countActiveEmployeesByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countActiveEmployeesByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['active_employee']['chart'] = $active_employees_chart;
        $data[0]['active_employee']['total_users'] = $total_users;
        $data[0]['active_employee']['vs_prior_period'] = $vs_prior_period;
        $data[0]['active_employee']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['active_employee']['vs_ly'] = $vs_ly;
        $data[0]['active_employee']['vs_ly_percent'] = $vs_ly_percent;


        $engaged_employees_chart = $this->_model->getEngagedEmployees($start, $end);
        $total_users = $this->_model->countEngagementEmployeeByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEngagementEmployeeByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEngagementEmployeeByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['engaged_employees']['chart'] = $engaged_employees_chart;
        $data[0]['engaged_employees']['total_users'] = $total_users;
        $data[0]['engaged_employees']['vs_prior_period'] = $vs_prior_period;
        $data[0]['engaged_employees']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['engaged_employees']['vs_ly'] = $vs_ly;
        $data[0]['engaged_employees']['vs_ly_percent'] = $vs_ly_percent;

        $employee_evanghelists_chart = $this->_model->getEmployeesEvanghelists($start, $end);
        $total_users = $this->_model->countEmployeesEvanghelistsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEmployeesEvanghelistsByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEmployeesEvanghelistsByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['employee_evanghelists']['chart'] = $employee_evanghelists_chart;
        $data[0]['employee_evanghelists']['total_users'] = $total_users;
        $data[0]['employee_evanghelists']['vs_prior_period'] = $vs_prior_period;
        $data[0]['employee_evanghelists']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['employee_evanghelists']['vs_ly'] = $vs_ly;
        $data[0]['employee_evanghelists']['vs_ly_percent'] = $vs_ly_percent;

        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['users_created']['total_users'], $data[0]['active_employee']['total_users']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['active_employee']['total_users'], $data[0]['engaged_employees']['total_users']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['engaged_employees']['total_users'], $data[0]['employee_evanghelists']['total_users']);
        $data[0]['users_created_to_active_employess_rate'] = $_rate_1;
        $data[0]['active_employees_to_engaged_employees_rate'] = $_rate_2;
        $data[0]['engaged_employees_to_employee_evanghelists_rate'] = $_rate_3;

        $active_jobs_chart = $this->_model->getActiveJobs($start, $end);
        $total_jobs = $this->_model->countActiveJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countActiveJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countActiveJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['active_jobs']['chart'] = $active_jobs_chart;
        $data[1]['active_jobs']['total_jobs'] = $total_jobs;
        $data[1]['active_jobs']['vs_prior_period'] = $vs_prior_period;
        $data[1]['active_jobs']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['active_jobs']['vs_ly'] = $vs_ly;
        $data[1]['active_jobs']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_shared_chart = $this->_model->getJobsShared($start, $end);
        $total_jobs = $this->_model->CountSharedJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->CountSharedJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->CountSharedJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_shared']['chart'] = $jobs_shared_chart;
        $data[1]['jobs_shared']['total_jobs'] = $total_jobs;
        $data[1]['jobs_shared']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_shared']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_shared']['vs_ly'] = $vs_ly;
        $data[1]['jobs_shared']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_viewed_chart = $this->_model->getViewedJobs($start, $end);
        $total_jobs = $this->_model->countViewedJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countViewedJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countViewedJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_viewed']['chart'] = $jobs_viewed_chart;
        $data[1]['jobs_viewed']['total_jobs'] = $total_jobs;
        $data[1]['jobs_viewed']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_viewed']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_viewed']['vs_ly'] = $vs_ly;
        $data[1]['jobs_viewed']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_with_applications_chart = $this->_model->getJobApplications($start, $end);
        $total_jobs = $this->_model->countJobApplicationsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countJobApplicationsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countJobApplicationsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_with_applications']['chart'] = $jobs_with_applications_chart;
        $data[1]['jobs_with_applications']['total_jobs'] = $total_jobs;
        $data[1]['jobs_with_applications']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_with_applications']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_with_applications']['vs_ly'] = $vs_ly;
        $data[1]['jobs_with_applications']['vs_ly_percent'] = $vs_ly_percent;


        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['active_jobs']['total_jobs'], $data[1]['jobs_shared']['total_jobs']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['jobs_shared']['total_jobs'], $data[1]['jobs_viewed']['total_jobs']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['jobs_viewed']['total_jobs'], $data[1]['jobs_with_applications']['total_jobs']);
        $data[1]['active_jobs_to_jobs_shared'] = $_rate_1;
        $data[1]['jobs_shared_to_jobs_viewed'] = $_rate_2;
        $data[1]['jobs_viewed_to_jobs_with_applications'] = $_rate_3;

        $views_of_jobs_chart = $this->_model->getJobsViews($start, $end);
        $total = $this->_model->countJobsViews($start, $end);
        $vs_prior_period = $this->_model->countJobsViews($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countJobsViews($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['views_of_jobs']['chart'] = $views_of_jobs_chart;
        $data[2]['views_of_jobs']['total'] = $total;
        $data[2]['views_of_jobs']['vs_prior_period'] = $vs_prior_period;
        $data[2]['views_of_jobs']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['views_of_jobs']['vs_ly'] = $vs_ly;
        $data[2]['views_of_jobs']['vs_ly_percent'] = $vs_ly_percent;

        $potential_candidates_chart = $this->_model->getPotentialCandidates($start, $end);
        $total = $this->_model->countPotentialCandidatesByPeriod($start, $end);
        $vs_prior_period = $this->_model->countPotentialCandidatesByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countPotentialCandidatesByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['potential_candidates']['chart'] = $potential_candidates_chart;
        $data[2]['potential_candidates']['total'] = $total;
        $data[2]['potential_candidates']['vs_prior_period'] = $vs_prior_period;
        $data[2]['potential_candidates']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['potential_candidates']['vs_ly'] = $vs_ly;
        $data[2]['potential_candidates']['vs_ly_percent'] = $vs_ly_percent;

        $total_applications_chart = $this->_model->getTotalApplications($start, $end);
        $total = $this->_model->countTotalApplicationsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countTotalApplicationsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countTotalApplicationsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['total_applications']['chart'] = $total_applications_chart;
        $data[2]['total_applications']['total'] = $total;
        $data[2]['total_applications']['vs_prior_period'] = $vs_prior_period;
        $data[2]['total_applications']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['total_applications']['vs_ly'] = $vs_ly;
        $data[2]['total_applications']['vs_ly_percent'] = $vs_ly_percent;

        $unique_applicants_chart = $this->_model->getUniqueCandidate($start, $end);
        $total = $this->_model->countUniqueCandidate($start, $end);
        $vs_prior_period = $this->_model->countUniqueCandidate($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countUniqueCandidate($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['unique_applicants']['chart'] = $unique_applicants_chart;
        $data[2]['unique_applicants']['total'] = $total;
        $data[2]['unique_applicants']['vs_prior_period'] = $vs_prior_period;
        $data[2]['unique_applicants']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['unique_applicants']['vs_ly'] = $vs_ly;
        $data[2]['unique_applicants']['vs_ly_percent'] = $vs_ly_percent;

        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['views_of_jobs']['total'], $data[2]['potential_candidates']['total']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['potential_candidates']['total'], $data[2]['total_applications']['total']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['total_applications']['total'], $data[2]['unique_applicants']['total']);

        $data[2]['views_of_jobs_to_potential_candidates'] = $_rate_1;
        $data[2]['potential_candidates_to_total_applications'] = $_rate_2;
        $data[2]['total_applications_to_unique_applicants'] = $_rate_3;

        $channels = $this->_model->getDashboardTopChannels($start, $end);
        $data['channels'] = $channels;
        echo json_encode($data);
        die;
    }

    public function getApplicantsAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));

//        Total views
        $views_of_jobs_chart = $this->_model->getJobsViews($start, $end);
        $total = $this->_model->countJobsViews($start, $end);
        $vs_prior_period = $this->_model->countJobsViews($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countJobsViews($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['views_of_jobs']['chart'] = $views_of_jobs_chart;
        $data[2]['views_of_jobs']['total'] = $total;
        $data[2]['views_of_jobs']['vs_prior_period'] = $vs_prior_period;
        $data[2]['views_of_jobs']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['views_of_jobs']['vs_ly'] = $vs_ly;
        $data[2]['views_of_jobs']['vs_ly_percent'] = $vs_ly_percent;

//        Unique viewers
        $potential_candidates_chart = $this->_model->getPotentialCandidates($start, $end);
        $total = $this->_model->countPotentialCandidatesByPeriod($start, $end);
        $vs_prior_period = $this->_model->countPotentialCandidatesByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countPotentialCandidatesByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['potential_candidates']['chart'] = $potential_candidates_chart;
        $data[2]['potential_candidates']['total'] = $total;
        $data[2]['potential_candidates']['vs_prior_period'] = $vs_prior_period;
        $data[2]['potential_candidates']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['potential_candidates']['vs_ly'] = $vs_ly;
        $data[2]['potential_candidates']['vs_ly_percent'] = $vs_ly_percent;

//      total applications
        $total_applications_chart = $this->_model->getTotalApplications($start, $end);
        $total = $this->_model->countTotalApplicationsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countTotalApplicationsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countTotalApplicationsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['total_applications']['chart'] = $total_applications_chart;
        $data[2]['total_applications']['total'] = $total;
        $data[2]['total_applications']['vs_prior_period'] = $vs_prior_period;
        $data[2]['total_applications']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['total_applications']['vs_ly'] = $vs_ly;
        $data[2]['total_applications']['vs_ly_percent'] = $vs_ly_percent;

//      unique applicants
        $unique_applicants_chart = $this->_model->getUniqueCandidate($start, $end);
        $total = $this->_model->countUniqueCandidate($start, $end);
        $vs_prior_period = $this->_model->countUniqueCandidate($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countUniqueCandidate($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['unique_applicants']['chart'] = $unique_applicants_chart;
        $data[2]['unique_applicants']['total'] = $total;
        $data[2]['unique_applicants']['vs_prior_period'] = $vs_prior_period;
        $data[2]['unique_applicants']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['unique_applicants']['vs_ly'] = $vs_ly;
        $data[2]['unique_applicants']['vs_ly_percent'] = $vs_ly_percent;


//      candidates accounts
        $unique_applicants_chart = $this->_model->getCandidateAccounts($start, $end);
        $total = $this->_model->countCandidateAccounts($start, $end);
        $vs_prior_period = $this->_model->countCandidateAccounts($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countCandidateAccounts($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['total_candidates_accounts']['chart'] = $unique_applicants_chart;
        $data[2]['total_candidates_accounts']['total'] = $total;
        $data[2]['total_candidates_accounts']['vs_prior_period'] = $vs_prior_period;
        $data[2]['total_candidates_accounts']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['total_candidates_accounts']['vs_ly'] = $vs_ly;
        $data[2]['total_candidates_accounts']['vs_ly_percent'] = $vs_ly_percent;


//      candidates accounts
        $unique_applicants_chart = $this->_model->getNewCandidateAccounts($start, $end);
        $total = $this->_model->countNewCandidateAccounts($start, $end);
        $vs_prior_period = $this->_model->countNewCandidateAccounts($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countNewCandidateAccounts($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['new_candidates_accounts']['chart'] = $unique_applicants_chart;
        $data[2]['new_candidates_accounts']['total'] = $total;
        $data[2]['new_candidates_accounts']['vs_prior_period'] = $vs_prior_period;
        $data[2]['new_candidates_accounts']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['new_candidates_accounts']['vs_ly'] = $vs_ly;
        $data[2]['new_candidates_accounts']['vs_ly_percent'] = $vs_ly_percent;


//      in_recruiting_process_applications
        $unique_applicants_chart = $this->_model->getInRecruitingProcessApplications($start, $end);
        $total = $this->_model->countInRecruitingProcessApplications($start, $end);
        $vs_prior_period = $this->_model->countInRecruitingProcessApplications($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countInRecruitingProcessApplications($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['in_recruiting_process_applications']['chart'] = $unique_applicants_chart;
        $data[2]['in_recruiting_process_applications']['total'] = $total;
        $data[2]['in_recruiting_process_applications']['vs_prior_period'] = $vs_prior_period;
        $data[2]['in_recruiting_process_applications']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['in_recruiting_process_applications']['vs_ly'] = $vs_ly;
        $data[2]['in_recruiting_process_applications']['vs_ly_percent'] = $vs_ly_percent;


//      applicants_by_status
        $unique_applicants_chart = $this->_model->getApplicantsByStatus($start, $end);
        $total = $this->_model->countApplicantsByStatus($start, $end);
        $vs_prior_period = $this->_model->countApplicantsByStatus($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countApplicantsByStatus($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['applicants_by_status']['chart'] = $unique_applicants_chart;
        $data[2]['applicants_by_status']['total'] = $total;
        $data[2]['applicants_by_status']['vs_prior_period'] = $vs_prior_period;
        $data[2]['applicants_by_status']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['applicants_by_status']['vs_ly'] = $vs_ly;
        $data[2]['applicants_by_status']['vs_ly_percent'] = $vs_ly_percent;

        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['views_of_jobs']['total'], $data[2]['potential_candidates']['total']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['potential_candidates']['total'], $data[2]['total_applications']['total']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[2]['total_applications']['total'], $data[2]['unique_applicants']['total']);

        $data[2]['views_of_jobs_to_potential_candidates'] = $_rate_1;
        $data[2]['potential_candidates_to_total_applications'] = $_rate_2;
        $data[2]['total_applications_to_unique_applicants'] = $_rate_3;

        $channels = $this->_model->getDashboardTopChannels($start, $end);
        $data['channels'] = $channels;
        echo json_encode($data);
        die;
    }

    public function getJobsAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));


        $active_jobs_chart = $this->_model->getActiveJobs($start, $end);
        $total_jobs = $this->_model->countActiveJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countActiveJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countActiveJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['active_jobs']['chart'] = $active_jobs_chart;
        $data[1]['active_jobs']['total_jobs'] = $total_jobs;
        $data[1]['active_jobs']['vs_prior_period'] = $vs_prior_period;
        $data[1]['active_jobs']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['active_jobs']['vs_ly'] = $vs_ly;
        $data[1]['active_jobs']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_shared_chart = $this->_model->getJobsShared($start, $end);
        $total_jobs = $this->_model->CountSharedJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->CountSharedJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->CountSharedJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_shared']['chart'] = $jobs_shared_chart;
        $data[1]['jobs_shared']['total_jobs'] = $total_jobs;
        $data[1]['jobs_shared']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_shared']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_shared']['vs_ly'] = $vs_ly;
        $data[1]['jobs_shared']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_viewed_chart = $this->_model->getViewedJobs($start, $end);
        $total_jobs = $this->_model->countViewedJobsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countViewedJobsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countViewedJobsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_viewed']['chart'] = $jobs_viewed_chart;
        $data[1]['jobs_viewed']['total_jobs'] = $total_jobs;
        $data[1]['jobs_viewed']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_viewed']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_viewed']['vs_ly'] = $vs_ly;
        $data[1]['jobs_viewed']['vs_ly_percent'] = $vs_ly_percent;

        $jobs_with_applications_chart = $this->_model->getJobApplications($start, $end);
        $total_jobs = $this->_model->countJobApplicationsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countJobApplicationsByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_prior_period);
        $vs_ly = $this->_model->countJobApplicationsByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_jobs, $vs_ly);
        $data[1]['jobs_with_applications']['chart'] = $jobs_with_applications_chart;
        $data[1]['jobs_with_applications']['total_jobs'] = $total_jobs;
        $data[1]['jobs_with_applications']['vs_prior_period'] = $vs_prior_period;
        $data[1]['jobs_with_applications']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[1]['jobs_with_applications']['vs_ly'] = $vs_ly;
        $data[1]['jobs_with_applications']['vs_ly_percent'] = $vs_ly_percent;


        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['active_jobs']['total_jobs'], $data[1]['jobs_shared']['total_jobs']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['jobs_shared']['total_jobs'], $data[1]['jobs_viewed']['total_jobs']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[1]['jobs_viewed']['total_jobs'], $data[1]['jobs_with_applications']['total_jobs']);
        $data[1]['active_jobs_to_jobs_shared'] = $_rate_1;
        $data[1]['jobs_shared_to_jobs_viewed'] = $_rate_2;
        $data[1]['jobs_viewed_to_jobs_with_applications'] = $_rate_3;


        echo json_encode($data);
        die;
    }

    public function getEmployeesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));

        $user_created_chart = $this->_model->getStatsCreatedUsers($start, $end);
        $total_users = $this->_model->countCreatedUserByPeriod($start, $end);
        $vs_prior_period = $this->_model->countCreatedUserByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countCreatedUserByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['users_created']['chart'] = $user_created_chart;
        $data[0]['users_created']['total_users'] = $total_users;
        $data[0]['users_created']['vs_prior_period'] = $vs_prior_period;
        $data[0]['users_created']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['users_created']['vs_ly'] = $vs_ly;
        $data[0]['users_created']['vs_ly_percent'] = $vs_ly_percent;

        $active_employees_chart = $this->_model->getActiveEmployees($start, $end);
        $total_users = $this->_model->countActiveEmployeesByPeriod($start, $end);
        $vs_prior_period = $this->_model->countActiveEmployeesByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countActiveEmployeesByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['active_employee']['chart'] = $active_employees_chart;
        $data[0]['active_employee']['total_users'] = $total_users;
        $data[0]['active_employee']['vs_prior_period'] = $vs_prior_period;
        $data[0]['active_employee']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['active_employee']['vs_ly'] = $vs_ly;
        $data[0]['active_employee']['vs_ly_percent'] = $vs_ly_percent;


        $engaged_employees_chart = $this->_model->getEngagedEmployees($start, $end);
        $total_users = $this->_model->countEngagementEmployeeByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEngagementEmployeeByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEngagementEmployeeByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['engaged_employees']['chart'] = $engaged_employees_chart;
        $data[0]['engaged_employees']['total_users'] = $total_users;
        $data[0]['engaged_employees']['vs_prior_period'] = $vs_prior_period;
        $data[0]['engaged_employees']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['engaged_employees']['vs_ly'] = $vs_ly;
        $data[0]['engaged_employees']['vs_ly_percent'] = $vs_ly_percent;

        $employee_evanghelists_chart = $this->_model->getEmployeesEvanghelists($start, $end);
        $total_users = $this->_model->countEmployeesEvanghelistsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEmployeesEvanghelistsByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEmployeesEvanghelistsByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['employee_evanghelists']['chart'] = $employee_evanghelists_chart;
        $data[0]['employee_evanghelists']['total_users'] = $total_users;
        $data[0]['employee_evanghelists']['vs_prior_period'] = $vs_prior_period;
        $data[0]['employee_evanghelists']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['employee_evanghelists']['vs_ly'] = $vs_ly;
        $data[0]['employee_evanghelists']['vs_ly_percent'] = $vs_ly_percent;

        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['users_created']['total_users'], $data[0]['active_employee']['total_users']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['active_employee']['total_users'], $data[0]['engaged_employees']['total_users']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['engaged_employees']['total_users'], $data[0]['employee_evanghelists']['total_users']);
        $data[0]['users_created_to_active_employess_rate'] = $_rate_1;
        $data[0]['active_employees_to_engaged_employees_rate'] = $_rate_2;
        $data[0]['engaged_employees_to_employee_evanghelists_rate'] = $_rate_3;


        echo json_encode($data);
        die;
    }

    public function getEmployeesRatesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));

        $data = [];
        $_rate_1 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['users_created']['total_users'], $data[0]['active_employee']['total_users']);
        $_rate_2 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['active_employee']['total_users'], $data[0]['engaged_employees']['total_users']);
        $_rate_3 = $this->_model->calculatePercentFromTwoVal(
                $data[0]['engaged_employees']['total_users'], $data[0]['employee_evanghelists']['total_users']);
        $data[0]['users_created_to_active_employess_rate'] = $_rate_1;
        $data[0]['active_employees_to_engaged_employees_rate'] = $_rate_2;
        $data[0]['engaged_employees_to_employee_evanghelists_rate'] = $_rate_3;


        echo json_encode($data);
        die;
    }

    public function getEvanghelistsEmployeesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';
        $data = [];
        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));

        $employee_evanghelists_chart = $this->_model->getEmployeesEvanghelists($start, $end);
        $total_users = $this->_model->countEmployeesEvanghelistsByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEmployeesEvanghelistsByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEmployeesEvanghelistsByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['employee_evanghelists']['chart'] = $employee_evanghelists_chart;
        $data[0]['employee_evanghelists']['total_users'] = $total_users;
        $data[0]['employee_evanghelists']['vs_prior_period'] = $vs_prior_period;
        $data[0]['employee_evanghelists']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['employee_evanghelists']['vs_ly'] = $vs_ly;
        $data[0]['employee_evanghelists']['vs_ly_percent'] = $vs_ly_percent;

        //        Total shares
        $shares_of_jobs_chart = $this->_model->getJobsShares($start, $end);
        $total = $this->_model->countJobsShares($start, $end);
        $vs_prior_period = $this->_model->countJobsShares($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_prior_period);
        $vs_ly = $this->_model->countJobsShares($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total, $vs_ly);
        $data[2]['shares_of_jobs']['chart'] = $shares_of_jobs_chart;
        $data[2]['shares_of_jobs']['total'] = $total;
        $data[2]['shares_of_jobs']['vs_prior_period'] = $vs_prior_period;
        $data[2]['shares_of_jobs']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[2]['shares_of_jobs']['vs_ly'] = $vs_ly;
        $data[2]['shares_of_jobs']['vs_ly_percent'] = $vs_ly_percent;

        echo json_encode($data);
        die;
    }

    public function getEngagedEmployeesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));
        $data = [];
        $engaged_employees_chart = $this->_model->getEngagedEmployees($start, $end);
        $total_users = $this->_model->countEngagementEmployeeByPeriod($start, $end);
        $vs_prior_period = $this->_model->countEngagementEmployeeByPeriod($previous_start_period, $previous_end_period);
        $vs_ly = $this->_model->countEngagementEmployeeByPeriod($start_last_year, $end_last_year);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['engaged_employees']['chart'] = $engaged_employees_chart;
        $data[0]['engaged_employees']['total_users'] = $total_users;
        $data[0]['engaged_employees']['vs_prior_period'] = $vs_prior_period;
        $data[0]['engaged_employees']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['engaged_employees']['vs_ly'] = $vs_ly;
        $data[0]['engaged_employees']['vs_ly_percent'] = $vs_ly_percent;

        echo json_encode($data);
        die;
    }

    public function getActiveEmployeesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));
        $data = [];
        $active_employees_chart = $this->_model->getActiveEmployees($start, $end);
        $total_users = $this->_model->countActiveEmployeesByPeriod($start, $end);
        $vs_prior_period = $this->_model->countActiveEmployeesByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countActiveEmployeesByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['active_employee']['chart'] = $active_employees_chart;
        $data[0]['active_employee']['total_users'] = $total_users;
        $data[0]['active_employee']['vs_prior_period'] = $vs_prior_period;
        $data[0]['active_employee']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['active_employee']['vs_ly'] = $vs_ly;
        $data[0]['active_employee']['vs_ly_percent'] = $vs_ly_percent;

        echo json_encode($data);
        die;
    }

    public function getCreatedEmployeesAction() {
//        date_default_timezone_set('Europe/Bucharest');
        header('Content-Type: application/json');
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $end = (isset($this->_params['start'])) ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['end'])) ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
        if ($this->getRequest()->isPost()) {
//            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? date('Y-m-d', strtotime($this->_params['start'])) : date('Y-m-d', strtotime('-1 month'));
//            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'])) : date('Y-m-d', strtotime('+1 day'));
            $start = (isset($this->_params['end']) && $this->_params['end'] != "") ? $this->_params['start'] : date('Y-m-d', strtotime('-1 month'));
            $end = (isset($this->_params['start']) && $this->_params['start'] != "") ? date('Y-m-d', strtotime($this->_params['end'] . ' +1 day')) : date('Y-m-d', strtotime('+1 day'));
        }
//        $start='2018-08-01';
//        $end='2018-9-20';

        $earlier = new DateTime($start);
        $later = new DateTime($end);

        $day_diff_between_filters = (int) $later->diff($earlier)->format("%a");

        $previous_start_period = date('Y-m-d', strtotime($start . ' -' . $day_diff_between_filters . ' days'));
        $previous_end_period = $start;

        $start_last_year = date('Y-m-d', strtotime($start . '-365 days'));
        $end_last_year = date('Y-m-d', strtotime($end . '-365 days'));
        $data=[];
        $user_created_chart = $this->_model->getStatsCreatedUsers($start, $end);
        $total_users = $this->_model->countCreatedUserByPeriod($start, $end);
        $vs_prior_period = $this->_model->countCreatedUserByPeriod($previous_start_period, $previous_end_period);
        $vs_prior_period_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_prior_period);
        $vs_ly = $this->_model->countCreatedUserByPeriod($start_last_year, $end_last_year);
        $vs_ly_percent = $this->_model->calculatePercentFromTwoVal($total_users, $vs_ly);
        $data[0]['users_created']['chart'] = $user_created_chart;
        $data[0]['users_created']['total_users'] = $total_users;
        $data[0]['users_created']['vs_prior_period'] = $vs_prior_period;
        $data[0]['users_created']['vs_prior_period_percent'] = $vs_prior_period_percent;
        $data[0]['users_created']['vs_ly'] = $vs_ly;
        $data[0]['users_created']['vs_ly_percent'] = $vs_ly_percent;

        echo json_encode($data);
        die;
    }

    public function exportAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $modelContent = new models_Content;
        $end = (isset($this->_params['startExport'])) ? date('Y-m-d', strtotime($this->_params['endExport'])) : date('Y-m-d', strtotime('+1 day'));
        $start = (isset($this->_params['endExport'])) ? date('Y-m-d', strtotime($this->_params['startExport'])) : date('Y-m-d', strtotime('-1 month'));
        $data = $modelContent->generateExport($start,$end);
//        echo '<pre>';
//        print_r($data);
//        die;
        $output = fopen("php://output", 'w') or die("Can't open php://output");
        $columns = array();

        foreach ($data as $row) {
            foreach ($row as $roll => $set) {
                $columns[$roll] = $roll;
                continue;
            }
        }
        $cols = array_values($columns);
	    $filename = 'Talentspotting report '.date('Y-m-d').'.csv';


	    header("Content-Type: application/force-download");
        header("Content-Type:application/octet-stream");
	    header("Content-Type: application/download");
        header('Content-Encoding: UTF-8');
        header("Content-type: text/csv; charset=UTF-8");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        print "\xEF\xBB\xBF"; // UTF-8 BOM
        fputcsv($output, $cols);
        foreach ($data as $product) {
            fputcsv($output, $product);
        }
        fclose($output) or die("Can't close php://output");
        exit;
    }

    public function aboutUsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $data = base64_decode($this->_params['data']);
//            if ($data != "") {
//                $this->_model->setAboutUs($data);
//            } else {
//                // disable how it works tabs
//                $this->_model->disableHowitWorksContent();
//            }
            $this->_model->setAboutUs($data);
        }
        $this->view->content = preg_replace("/\r|\n/", "",
            $this->_model->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['about_us']);
    }

}
