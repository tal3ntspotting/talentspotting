<?php
class modules_default_forms_Register extends WebApp_Form {

	protected $_fields = array();

	public function __construct($options = array()) {
		$this->_fields = array(
							'name',
							'email',
							'password',
							'password_confirm',
							'timezone'
						 );

		parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => 'index/forms/register.phtml'))));
	}

	public function init () {
		if (!isset($this->_fields)) {
			return;
		}

		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$settings = $bootstrap['resources']['Settings'];

		foreach ($this->_fields as $field) {
			switch ($field) {
				case 'name':
					$element = new WebApp_Form_Element_Text($field);
					break;

				case 'email':
					$element = new WebApp_Form_Element_Html5($field);
					$element->setAttribs(array('type' => 'email'));
					$element->addValidator(new Zend_Validate_EmailAddress());
					break;

				case 'password':
				case 'password_confirm':
					$element = new WebApp_Form_Element_Password($field);

					if ($field === 'password_confirm') {
						$matchValidator = new Zend_Validate_Identical('password');
						$matchValidator->setMessage(
							$this->getView()->translate('register-password-confirm-not-match-error'),
							Zend_Validate_Identical::NOT_SAME
						);

						$element->addValidator($matchValidator);
					}

					break;

				case 'timezone':
					$element = new WebApp_Form_Element_Select($field);
					$timezones = $settings['timezones'];
					$options = array();
					$negative_timezones = array();
					$positive_timezones = array();
					$middle_timezone = array();
					$last_timezone = array();
					$first_timezone = array();

					foreach ($timezones as $id => $timezone) {
						$translation = $this->getView()->translate('timezone-' . $timezone);

						if (strpos($translation, '(GMT-') !== false) { // found
							$negative_timezones[$id] = $translation;
						} elseif (strpos($translation, '(GMT+') !== false) {
							$positive_timezones[$id] = $translation;
						} elseif ($timezone === 'TZ_PACIFIC_EASTER') {
							$middle_timezone = array($id => $translation);
						} elseif ($timezone === 'TZ_NUM_TIMEZONES') {
							$last_timezone = array($id => $translation);
						} elseif ($timezone === 'TZ_UNKNOWN') {
							$first_timezone = array('' => $translation);
						}
					}

					arsort($negative_timezones);
					asort($positive_timezones);

					$options = array_merge($first_timezone, $negative_timezones, $middle_timezone, $positive_timezones, $last_timezone);

					$element->addMultiOptions($options);
					break;

				default:
					$element = new WebApp_Form_Element_Text($field);
					break;
			}

			if ($field !== 'name') {
				$element->setRequired(true);
			}

			$this->addElement($element);
		}
	}

}