<?php

class Modules_default_Forms_Upload extends WebApp_Form {

    protected $_fields = array();

    public function __construct($options = array()) {
        $this->_fields = array(
            'firstname',
            'lastname',
            'phone',
            'location',
            'job_title',
            'skills',
            'company',
            'country',
            'county',
            'lastjob_lifetime',
            'education',
            'foreign_language'
        );

        parent::__construct($options);
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => 'index/forms/upload.phtml',array("id_company"=>$options['id_company'])))));
    }

    public function init() {
        if (!isset($this->_fields)) {
            return;
        }

        $model = new models_Company();
        $company = $model->getCompanyBySubdomainSlug();
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $settings = $bootstrap['resources']['Settings'];
        foreach ($this->_fields as $field) {
            switch ($field) {
                case 'firstname':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;
                case 'lastname':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;
                case 'phone':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                case 'country' :

                    $element = new WebApp_Form_Element_Select($field);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    $element->placeholder = 'choose';
                    $element->addMultiOption('0');
                    $model = new models_Content;
                    $data = $model->getCountrySelect();
                    $element->addMultioptions($data);

                    break;

                case 'county' :

                    $element = new WebApp_Form_Element_Select($field);
                    $element->class = 'form-control county';
                    $element->setAttrib('required', 'required');
                    $element->placeholder = 'choose';
                    $element->addMultioptions([]);

                    break;

                case 'location':

                   $element = new WebApp_Form_Element_Select($field);
                    $element->class = 'form-control';
                    $element->setAttrib('required', 'required');
                    $element->placeholder = 'choose';
                    $model = new models_Content;
                    $data = $model->getCitiesSelect($company['id_country']);
                    $element->addMultioptions([]);
                    
                    break;
                case 'job_title':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                case 'lastjob_lifetime':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                case 'education':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                case 'foreign_language':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                case 'skills':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->class = 'form-control custom_user_skill';
                    $element->setAttrib('required', 'required');
                    $element->setAttrib('name', 'skills[]');
                    $element->placeholder = 'choose';
                    break;
                case 'company':
                    $element = new WebApp_Form_Element_Text($field);
                    $element->setAttrib('class', 'form-control');
                    $element->setAttrib('required', 'required');
                    break;

                default:
                    $element = new WebApp_Form_Element_Text($field);
                    break;
            }
            $this->addElement($element);
        }
    }

}

?>
