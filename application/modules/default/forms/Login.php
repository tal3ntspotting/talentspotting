<?php
class modules_default_forms_Login extends WebApp_Form
{
	protected $fields;

	public function __construct($options = array())
    {
		$this->fields = array( 'email', 'password');
		parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => 'index/forms/login.phtml'))));

		$this->loadElems();
   }

	public function loadElems()
    {
		if(isset($this->fields))
        {
			foreach ($this->fields as $k => $v)
            {
				switch($v)
                {
                    case 'email':
						$element = new WebApp_Form_Element_Text($v);
						// $element->class = '';
                        $element->addValidator(new Zend_Validate_EmailAddress());
					break;

                    case 'password':
						$element = new WebApp_Form_Element_Password($v);
					break;
				}

                $element->setRequired(true);
				$this->addElement($element);
			}
		}
	}
}