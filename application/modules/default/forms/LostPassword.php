<?php
class modules_default_forms_LostPassword extends WebApp_Form {

	protected $fields;

	public function __construct($options = array()) {
		$this->fields = array('email');
		parent::__construct($options);
		$this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => 'index/forms/lost-password.phtml'))));
	}

	public function init () {
		if (!isset($this->fields)) {
			return;
		}

		foreach ($this->fields as $k => $v) {
			$element = new WebApp_Form_Element_Text($v);
            $element->addValidator(new Zend_Validate_EmailAddress());
            $element->setRequired(true);

			$this->addElement($element);
		}
	}

}