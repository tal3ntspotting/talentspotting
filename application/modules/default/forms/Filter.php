<?php

class modules_default_forms_Filter extends WebApp_Form {

    protected $fields;
    private $_defaults = array();
    private $_data = array();
    public $languages;
    public $sf;
    protected $_settings;

    public function __construct($options = array(), $defaults = array()) {
        $this->fields = array('id', 'skills', 'cities');

        parent::__construct($options);
        $this->setDecorators(array('PrepareElements', array('ViewScript', array('viewScript' => "index/forms/filter.phtml"))));

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];
        $languages = Zend_Registry::get("_LANG_DETAILS");

        foreach ($languages as $lang => $val) {
            $this->sf[] = "value_" . $lang;
        }

        $this->languages = $languages;

        $this->go();
    }

    public function go() {

        if (empty($this->fields)) {
            return;
        }
        foreach ($this->fields as $field) {
            switch ($field) {
                case 'id':
                    $element = new WebApp_Form_Element_Hidden($field);
                    break;

//                case 'bank_name':
//                    $element = new WebApp_Form_Element_Text($field);
//                    $element->class = 'form-control required';
//                    $element->setAttrib('required', 'required');
//                    break;


                case 'cities':
                    $element = new WebApp_Form_Element_MultiSelect($field);
                    $element->class = 'form-control';
                    $element->placeholder = 'choose';
                    $model = new models_Content;
                    $data = $model->getCitiesSelect();
                    $element->addMultioptions($data);
                    break;
                case 'skills':
                    $element = new WebApp_Form_Element_MultiSelect($field);
                    $element->class = 'form-control';
                    $element->placeholder = 'choose';
                    $model = new models_Content;
                    $data = $model->getSkillsSelect();
                    $element->addMultioptions($data);
                    break;
            }
            //set default values for fields
            if (count($this->_defaults) && array_key_exists($field, $this->_defaults)) {
                $element->setValue($this->_defaults[$field]);
            }
            $this->addElement($element);
        }
        foreach ($this->languages as $lang => $val) {
            if (in_array("value_" . $lang, $this->sf)) {
                $element = new WebApp_Form_Element_Textarea("value_" . $lang);
                $element->class = 'form-control';
                $element->setAttrib("data-required", "true");
                $element->setAttrib("rows", "3");
                $this->addElement($element);
            }
        }
    }

}

?>
