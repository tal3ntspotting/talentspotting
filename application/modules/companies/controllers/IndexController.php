<?php
class Campaigns_IndexController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Company;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
	}
    
    public function rateAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        $id_job = $this->_params['id_job'];
        $score = $this->_params['score'];
        
        $this->view->company = $this->_model->getCompanyById($id);
        $this->view->score = $score;
        $this->view->id_job = $id_job;
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->setRating($_POST, $id, $id_job);
            echo json_encode(array("msg" => "ok"));
            exit();
        }
    }
    
    public function detailsAction()
    {
        $modelJobs = new models_Jobs;
        $slug = $this->_params['slug'];
        $company = $this->_model->getCompanyBySlug($slug);
        $this->view->company = $company;
        $this->view->covers = $this->_model->getCompanyCovers($company['id']);
        $this->view->applications = $this->_model->getCompanyJobApplications($company['id']);
        $this->view->recommendations = $this->_model->getCompanyJobRecommendations($company['id']);
        $this->view->ratings = $this->_model->getCompanyRatings($company['id'], 150);
        $this->view->jobs = $modelJobs->getJobs(array("id_company" => array($company['id'])));

        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(8,3, array("company" => $company)));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(8,2, array("company" => $company)));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(8,1, array("company" => $company)));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(8, array("company" => $company));
        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(8,3, array("company" => $company)));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(8,2, array("company" => $company)));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(8,2, array("company" => $company)));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(8,3, array("company" => $company)));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(8,5, array("company" => $company)));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }
}
