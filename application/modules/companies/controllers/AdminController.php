<?php

class Companies_AdminController extends Zend_Controller_Action {

    protected $_model;
    protected $_model_content;
    protected $_model_social;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();
        $this->_model = new models_Company;
        $this->_model_content = new models_Content;
        $this->_model_social = new models_Social;
        $this->_model_user = new models_User;
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;
        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];
        $this->view->settings = $this->_settings;
        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
    }

    public function brandAction() {
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/home');
        $this->view->current_menu = "users";
        $this->view->current_sub_menu = "brand";
        $styles = $this->_model_content->getDashboardStyles();
        $this->view->dashboard_style = "dashboard_styles";
//        $this->view->custom_profile = "custom_profile";
        $this->view->dashboard = $styles;
        $this->view->companie = $this->_model->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);

        $color = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR ;
        $view = new Zend_View();
        $view->setScriptPath($color);
        $partialHelper = new Zend_View_Helper_Partial();
        $partialHelper->setView($view);

        $sql = $this->_model->select()
            ->from(['bc' => 'branding_category'],["*"]);
        $res = $this->_model->fetchAll($sql);
        foreach($res as $key => &$value) {
            $q = $this->_model->select()
                ->from(['b' => 'branding'],["*"])->where('b.id_category =?',(int) $value['id']);
            $res[$key]['color'] = $this->_model->fetchAll($q) ;
        }
//        echo '<pre>';print_r($res);die;

//        $sql = $this->_model->select()
//            ->from(['b' => 'branding'],['class_name','value']);
//        $data = $this->_model->fetchPairs($sql);
//        $final = [];
//        foreach($data as $key => $value)
//        {
//            $class = str_replace([".",">","-"],["___","__","_"],$key);
////            echo $class.'<br />';
//            $final[$class] = $value;
//        }


        $bodyData = $partialHelper->partial( 'color-picker.phtml', ['styles' => $res] );
//        echo $bodyData;die;
        $this->view->color_picker = $bodyData;
    }

    public function addStylesAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isPost()) {
            $db = $this->_params['storage'];
            $style = $this->_getParam('style')[0];
            $this->_model_content->addStyle($db, $style);
            die;
        }
    }
    
    public function buildAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $db = $this->_params['storage'];
        $content = $this->_model_content->buildStyles();
        foreach($content as $key => $value ) {
            $path = $this->_settings['brand'][$value['target']];
            ($value['style'] != "") ? file_put_contents($path, $value['style']) : file_put_contents($path, "");
        }
        die;
    }
    
    public function addAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        if($this->getRequest()->isPost()) {
            extract($_FILES);
            if($logo && $logo['name'] != "") {
                $extension = substr($logo['name'], strrpos($logo['name'], ".") + 1);
                $file_name = "logo.$extension";
                move_uploaded_file($logo['tmp_name'], ABSOLUTE_PATH . "/public/resources/frontend/landing/files/icons/{$file_name}");
            }
            
            if($share && $share['name']) {
                $extension = substr($share['name'], strrpos($share['name'], ".") + 1);
                $file_no_ext = substr($share['name'], 0, strrpos($share['name'], "."));
                $file_name = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;
                move_uploaded_file($share['tmp_name'], ABSOLUTE_PATH . "/public/resources/files/companies/original/{$file_name}");
                $this->_model_content->updateShareimage($file_name);
            }
            
            if($landing && $landing['name']) {
                $extension = substr($landing['name'], strrpos($landing['name'], ".") + 1);
                $file_no_ext = substr($landing['name'], 0, strrpos($landing['name'], "."));
                $file_name = sha1(WebApp_Tools::CreateAlias($file_no_ext)) . "." . $extension;
                move_uploaded_file($landing['tmp_name'], ABSOLUTE_PATH . "/public/resources/frontend/css/theme/images/{$file_name}");
                $this->_model_content->updateLandingimage($file_name);
            }

            
            $_REQUEST['signin'] = ($_REQUEST['signin'] == 'on') ? 1 : 0;
            $_REQUEST['job_skills'] = ($_REQUEST['job_skills'] == 'on') ? 1 : 0;
            $_REQUEST['job_departments'] = ($_REQUEST['job_departments'] == 'on') ? 1 : 0;
            $_REQUEST['upload_cv'] = ($_REQUEST['upload_cv'] == 'on') ? 1 : 0;
            $_REQUEST['import_linkedin'] = ($_REQUEST['import_linkedin'] == 'on') ? 1 : 0;
            $_REQUEST['launch_date'] = ($_REQUEST['launch_date'] !='') ? $_REQUEST['launch_date'] : '';
            $_REQUEST['launch_time'] = ($_REQUEST['launch_time'] !='') ? $_REQUEST['launch_time'] : '';

//            $_REQUEST['end_date'] = ($_REQUEST['end_date'] !='') ? $_REQUEST['end_date'] : '';
//            $_REQUEST['end_time'] = ($_REQUEST['end_time'] !='') ? $_REQUEST['end_time'] : '';
//            $_REQUEST['signin_text'] = ($_REQUEST['signin_text'] !='') ? $_REQUEST['signin_text'] : '';
//            $_REQUEST['register_text'] = ($_REQUEST['register_text'] !='') ? $_REQUEST['register_text'] : '';

            foreach($_REQUEST as $key => $value) {
                if(!preg_match('/n_*/', $key))
                {
                    // You can access $value or create a new array based off these values
                    $this->_model_content->updateSocial($key,$value);
                } elseif (strpos($key,"_text")) {
	                // You can access $value or create a new array based off these values
	                $this->_model_content->updateSocial($key,$value);
                }

//                if($_REQUEST[$key] != "")
//                {

//                }
	        }
//            echo '<pre>';
////            print_r($logo);
//            print_r($keys);
//            print_r($keys2);
//            die;
//            print_r($_FILES);
//			echo $_REQUEST['signin_text'];
//            die;
        }

        $this->_redirect('/companies/admin/brand');
        die;
    }
    public function settingsAction()
    {

    }

    public function saveBrandAction()
    {
        $company = new models_Company;
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $colors = $this->_params['data'];
        foreach($colors as $key => $value)
        {
            $id = explode('.',$value['class'])[1];
            $company->update(['value' => $value['value']],"id='{$id}'","branding");
        }
        die;

    }

    public function translateAction()
    {
        
    }

}
