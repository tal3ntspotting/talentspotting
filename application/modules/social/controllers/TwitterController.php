<?php

require_once ABSOLUTE_PATH . '/library/Google/autoload.php';

class Social_TwitterController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Social;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('frontend');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
        
    public function loginAction()
    {        
        if (isset($this->_params['oauth_token']))
        {
            $config = array(
                "callbackUrl" => APPLICATION_URL."/social/twitter/login",
                "siteUrl" => "https://api.twitter.com/oauth",
                "consumerKey" => $this->_settings['twitter']['api_key'],
                "consumerSecret" => $this->_settings['twitter']['secret']
            );
            
            $consumer = new Zend_Oauth_Consumer($config);
            
            $session = new Zend_Session_Namespace("twitter_token");
            $token = $consumer->getAccessToken( $this->_request->getQuery() , unserialize( $session->requestToken ) );
            $session->accessToken = serialize( $token );
            unset( $session->requestToken );
            
            $id = $token->getParam("user_id");
            if ($id)
            {
                $modelUser = new models_User;
                if ($user = $modelUser->authenticateTwitter($id))
                {
                    $this->_model->update(array("twitter_token" => serialize($token), "twitter_id" => $id), "id = '{$user->id}'", "users");
                    return $this->_redirect('/profile');
                }
                else
                {
                    $user_data['firstname'] = $token->getParam("screen_name");
                    $user_data['twitter_token'] = serialize($token);
                    $user_data['twitter_id'] = $id;
                    $user_data['status'] = 1;
                    $user_data['type'] = 1;
                    $user_data['password'] = uniqid();            
                    
                    $user = $modelUser->saveFromTwitter($user_data);
                    $modelUser->authenticateTwitter($user['twitter_id']);
                
                    return $this->_redirect('/register-twitter');
                }
            }
            else
            {
                $this->_redirect("/login");
            }
        }
        else
        {
            $config = array(
                "callbackUrl" => APPLICATION_URL."/social/twitter/login",
                "siteUrl" => "https://api.twitter.com/oauth",
                "consumerKey" => $this->_settings['twitter']['api_key'],
                "consumerSecret" => $this->_settings['twitter']['secret']
            );
            
            $consumer = new Zend_Oauth_Consumer($config);
            $session = new Zend_Session_Namespace("twitter_token");
            $session->requestToken = serialize( $consumer->getRequestToken() );
            $consumer->redirect();
        }       
     }
     
     public function registerTwitterAction()
     {
        $this->view->user = $this->_model->_user;
        if ($this->getRequest()->isPost())
        {
            $modelUser = new models_User;
            $modelUser->updateTwitter($_POST);
            
            $this->_redirect("/profile");
        }  
     }
}