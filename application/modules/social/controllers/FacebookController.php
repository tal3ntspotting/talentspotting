<?php

require_once ABSOLUTE_PATH . '/library/Facebook/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphUser;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookOtherException;
use Facebook\GraphSessionInfo;
use Facebook\Entities\AccessToken;
use Facebook\Entities\SignedRequest;

class Social_FacebookController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Social;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
        
    public function loginAction()
    {
        if ($this->_params['code'] != "" && !isset($this->_params['error']))
        {
            $redirect_url = APPLICATION_URL."/social/facebook/login";
            $helper = new FacebookRedirectLoginHelper($redirect_url, $this->_settings['facebook']['api_key'], $this->_settings['facebook']['secret']);        
            FacebookSession::setDefaultApplication($this->_settings['facebook']['api_key'], $this->_settings['facebook']['secret']);
            
            try 
            {            
                $session = $helper->getSessionFromRedirect();
            } 
            catch(FacebookRequestException $ex) 
            {
                $this->_flashMessenger->addMessage('login_error_auth');
                return $this->_redirect('/login');               
            } 
            catch(\Exception $ex) 
            {
                $this->_flashMessenger->addMessage('login_error_auth');
                return $this->_redirect('/login');    
            }
            
            if ($session) 
            {            
                $session->getLongLivedSession();
    			$token = $session->getToken();
                
                $request = new FacebookRequest($session, 'GET', '/me?fields=first_name,last_name,email,location');
                $profile = $request->execute()->getGraphObject(GraphUser::className());
                
                $request = new FacebookRequest($session, 'GET', '/me/picture', array ('redirect' => false,'height' => '200','type' => 'normal','width' => '200'));
                $image = $request->execute()->getGraphObject();
                
                $modelUser = new models_User;
                
                if ($user = $modelUser->authenticateLinkedin($profile->getProperty("email")))
                {
                    $this->_model->update(array("facebook_token" => $token, "facebook_id" => $profile->getProperty("id")), "id = '{$user->id}'", "users");
                    return $this->_redirect('/profile');
                }
                else
                {
                    $user_data['firstname'] = $profile->getProperty("first_name");
                    $user_data['lastname'] = $profile->getProperty("last_name");                           
                    $user_data['image'] = $image->getProperty("url");
                    $user_data['facebook_token'] = $token;
                    $user_data['facebook_id'] = $profile->getProperty("id");
                    $user_data['status'] = 1;
                    $user_data['type'] = 1;
                    $user_data['email'] = $profile->getProperty("email");
                    $user_data['password'] = uniqid();            
                    
                    $user = $modelUser->saveFromLinkedin($user_data);
                    $modelUser->authenticateLinkedin($return->emailAddress);
                
                    return $this->_redirect('/register-email');
                }
                
                          
            }
            else
            {
                $this->_flashMessenger->addMessage('login_error_auth');
                return $this->_redirect('/login');  
            }
        }
        else
        {
            $redirect_url = APPLICATION_URL."/social/facebook/login";
            $helper = new FacebookRedirectLoginHelper($redirect_url, $this->_settings['facebook']['api_key'], $this->_settings['facebook']['secret']);
            $login_url = $helper->getLoginUrl(array('public_profile', 'email'));        
            $this->_redirect($login_url);    
        }
        
    }
}