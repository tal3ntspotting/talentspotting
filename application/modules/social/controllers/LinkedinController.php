<?php
/**
 * index controller for default module
*/
class Social_LinkedinController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Social;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
    
    public function connectAction()
    {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
               
        $code = $this->_params['code'];
        $error = $this->_params['error'];
        $ref = $this->_params['ref'];
        
        if (!isseT($code) && !isset($error))
        {
            $url = "https://www.linkedin.com/uas/oauth2/authorization";
            $params = array(
                        'client_id' => $this->_settings['linkedin']['api_key'],
                        'response_type' => 'code',
                        'redirect_uri' => APPLICATION_URL.'/social/linkedin/connect?ref='.$ref,
                        'state' => base64_encode(md5(time())),
                        'scope' => 'r_basicprofile' 
                        );
            $query = http_build_query($params);
            
            $redirect = $url."?".$query;
            $this->redirect($redirect);    
        }
        else
        {
            if ($code != "")
            {
                $client = new Zend_Http_Client('https://www.linkedin.com/uas/oauth2/accessToken');
                $client->setParameterPost('grant_type', "authorization_code");
                $client->setParameterPost('code', $code);
                $client->setParameterPost('redirect_uri', APPLICATION_URL.'/social/linkedin/connect?ref='.$ref);
                $client->setParameterPost('client_id', $this->_settings['linkedin']['api_key']);
                $client->setParameterPost('client_secret', $this->_settings['linkedin']['secret']);
                
                $response = $client->request(Zend_Http_Client::POST)->getBody();
                $return = json_decode($response);
                
                $ns->token = $return->access_token;
                $this->_model->update(array("linkedin_token" => $return->access_token), "id = '{$this->_model->_user->id}'", "users");       
                
                if ($ref != "")
                {
                    return $this->_redirect('/'.$ref);    
                }
                else
                {
                    return $this->_redirect('/profile');
                }
            }
            else
            {
                $this->_flashMessenger->addMessage($this->translate->_('login_error_auth'));
                return $this->_redirect('/login');   
            }
        }
    }
    
    public function loginAction()
    {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $slug = $this->_params['slug'];
        
        if ($slug)
        {
            $ns->job_slug = $slug;
        }
              
        $code = $this->_params['code'];
        $error = $this->_params['error'];
        $ref = $this->_params['ref'];
        
        if (!isset($code) && !isset($error))
        {
            $url = "https://www.linkedin.com/uas/oauth2/authorization";
            $params = array(
                        'client_id' => $this->_settings['linkedin']['api_key'],
                        'response_type' => 'code',
                        'redirect_uri' => APPLICATION_URL.'/social/linkedin/login',
                        'state' => base64_encode(md5(time())),
                        'scope' => 'r_basicprofile r_emailaddress' 
                        );
            $query = http_build_query($params);
            
            $redirect = $url."?".$query;
            $this->redirect($redirect);    
        }
        else
        {
            if ($code != "")
            {
                $client = new Zend_Http_Client('https://www.linkedin.com/uas/oauth2/accessToken');
                $client->setParameterPost('grant_type', "authorization_code");
                $client->setParameterPost('code', $code);
                $client->setParameterPost('redirect_uri', APPLICATION_URL.'/social/linkedin/login');
                $client->setParameterPost('client_id', $this->_settings['linkedin']['api_key']);
                $client->setParameterPost('client_secret', $this->_settings['linkedin']['secret']);
                $client->setHeaders("Content-Type", "application/x-www-form-urlencoded");
                
                $response = $client->request(Zend_Http_Client::POST)->getBody();
                $return = json_decode($response);
                
                if ($return->access_token != "")
                {
                    $slug = $ns->job_slug;
                    $ns->job_slug = null;
                    $token = $return->access_token;
                    
                    $client = new Zend_Http_Client('https://api.linkedin.com/v1/people/~:(id,firstName,lastName,industry,location,headline,current-status,positions,educations,skills,certifications,pictureUrl,email-address)');
                    $client->setParameterGet('oauth2_access_token', $token);
                    $client->setMethod(Zend_Http_Client::GET);
                    $client->setHeaders("Content-Type", "application/json");
                    $client->setHeaders("x-li-format", "json");
                    
                    $response = $client->request(Zend_Http_Client::GET)->getBody();
                    $return = json_decode($response);
                    
                    if ($return->emailAddress != "")
                    {
                        $modelUser = new models_User;
                        if ($user = $modelUser->authenticateLinkedin($return->emailAddress))
                        {
                            $this->_model->update(array("linkedin_token" => $token), "id = '{$user->id}'", "users");
                            
                            if ($slug != "")
                            {
                                $modelJobs = new models_Jobs;
                                $job = $modelJobs->getJobBySlug($slug);
                                $modelJobs->apply((array) $user, $job);
                                return $this->_redirect('/profile');
                            }
                            else
                            {
                                return $this->_redirect('/profile');    
                            }
                            
                        }
                        else
                        {
                            $user_data['firstname'] = $return->firstName;
                            $user_data['lastname'] = $return->lastName;
                            $user_data['title'] = $return->headline;                            
                            $user_data['image'] = $return->pictureUrl;
                            $user_data['linkedin_token'] = $token;
                            $user_data['linkedin_id'] = $return->id;
                            $user_data['status'] = 1;
                            $user_data['type'] = 1;
                            $user_data['email'] = $return->emailAddress;
                            $user_data['password'] = uniqid();            
                            $user = $modelUser->saveFromLinkedin($user_data);
                            $modelUser->authenticateLinkedin($return->emailAddress);
                            
                            if ($slug != "")
                            {
                                $modelJobs = new models_Jobs;
                                $job = $modelJobs->getJobBySlug($slug);
                                $modelJobs->apply($user, $job);
                            }
                            else
                            {
                                return $this->_redirect('/register-email');    
                            }                        
                            
                        }
                    }
                    else
                    {
                        $this->_flashMessenger->addMessage($this->translate->_('login_error_auth'));
                        return $this->_redirect('/login');
                    }
                }
                else
                {
                    $this->_flashMessenger->addMessage($this->translate->_('login_error_auth'));
                    return $this->_redirect('/login');   
                }                
                
                return $this->_redirect('/profile');
            }
            else
            {
                $this->_flashMessenger->addMessage($this->translate->_('login_error_auth'));
                return $this->_redirect('/login');   
            }
        }
    }
    
//    public function syncAction()
//    {
//        $ns = new Zend_Session_Namespace('linkedin_oauth');
//               
//        $code = $this->_params['code'];
//        $error = $this->_params['error'];
//        $ref = $this->_params['ref'];
//        
//        if (!isset($code) && !isset($error))
//        {
//            $url = "https://www.linkedin.com/uas/oauth2/authorization";
//            $params = array(
//                        'client_id' => $this->_settings['linkedin']['api_key'],
//                        'response_type' => 'code',
//                        'redirect_uri' => APPLICATION_URL.'/social/linkedin/sync',
//                        'state' => base64_encode(md5(time())),
//                        'scope' => 'r_fullprofile' 
//                        );
//            $query = http_build_query($params);
//            
//            $redirect = $url."?".$query;
//            $this->redirect($redirect);    
//        }
//        else
//        {
//            if ($code != "")
//            {
//                $client = new Zend_Http_Client('https://www.linkedin.com/uas/oauth2/accessToken');
//                $client->setParameterPost('grant_type', "authorization_code");
//                $client->setParameterPost('code', $code);
//                $client->setParameterPost('redirect_uri', APPLICATION_URL.'/social/linkedin/sync');
//                $client->setParameterPost('client_id', $this->_settings['linkedin']['api_key']);
//                $client->setParameterPost('client_secret', $this->_settings['linkedin']['secret']);
//                
//                $response = $client->request(Zend_Http_Client::POST)->getBody();
//                $return = json_decode($response);
//                               
//                $ns->token = $return->access_token;
//                $this->_model->update(array("linkedin_token" => $return->access_token), "id = '{$this->_model->_user->id}'", "users");       
//                
//                if ($ref != "")
//                {
//                    return $this->_redirect('/'.$ref);    
//                }
//                else
//                {
//                    return $this->_redirect('/profile');
//                }
//            }
//            else
//            {
//                $this->_flashMessenger->addMessage($this->translate->_('login_error_auth'));
//                return $this->_redirect('/profile');   
//            }
//        }
//    }
}