<?php

require_once ABSOLUTE_PATH . '/library/Google/autoload.php';

class Social_GoogleController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	protected $_params;
	protected $_flashMessenger;

	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Social;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;		
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
        
    public function loginAction()
    {
        $client = new Google_Client();
        $client->setApplicationName("WorkinAfrica");
        $client->setClientId($this->_settings['google']['client_id']);
        $client->setClientSecret($this->_settings['google']['secret']);
        $client->setRedirectUri(APPLICATION_URL."/social/google/login");
        $client->setScopes(array('https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/plus.profile.emails.read', 'https://www.googleapis.com/auth/plus.login'));                
        
        if (isset($this->_params['code']))
        {
            $oauth = new Google_Service_Plus($client);
            $client->authenticate($this->_params['code']);
            $token = $client->getAccessToken();
            
            $data = $oauth->people->get("me");
            
            if (!empty($data['modelData']))
            {
                $email = $data['modelData']['emails'][0]['value'];
                
                if ($email != "")
                {
                    $modelUser = new models_User;
                    if ($user = $modelUser->authenticateLinkedin($email))
                    {
                        $this->_model->update(array("gplus_token" => $token), "id = '{$user->id}'", "users");
                        return $this->_redirect('/profile');
                    }
                    else
                    {
                        $user_data['firstname'] = $data['modelData']['name']['givenName'];
                        $user_data['lastname'] = $data['modelData']['name']['familyName'];                           
                        $user_data['image'] = str_replace("?sz=50", "?sz=200", $data['modelData']['image']['url']);
                        $user_data['gplus_token'] = $token;
                        $user_data['status'] = 1;
                        $user_data['type'] = 1;
                        $user_data['email'] = $email;
                        $user_data['password'] = uniqid();            
                        $user = $modelUser->saveFromLinkedin($user_data);
                        $modelUser->authenticateLinkedin($email);
                        return $this->_redirect('/register-email');   
                    }
                }
                else
                {
                    $this->_flashMessenger->addMessage('login_error_auth');
                    return $this->_redirect('/login');
                } 
            }
            else
            {
                $this->_flashMessenger->addMessage('login_error_auth');
                return $this->_redirect('/login');
            }
            
        }
        else
        {
            $authUrl = $client->createAuthUrl("email");
            $this->_redirect($authUrl);
        }
    }
}