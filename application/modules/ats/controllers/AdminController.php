<?php

class Ats_AdminController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_payload;
    public $translate;
    protected $_flashMessenger;
    public $_user;
    public $_modelUser;
    public $_modelJob;
    public $_modelContent;
    public $_modelCompany;

    public function init() {
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/login');
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $body = $this->getRequest()->getRawBody();
        if ($body)
            $this->_payload = Zend_Json::decode($body, Zend_Json::TYPE_ARRAY);


        $layout = Zend_Layout::getMvcInstance();
        $this->_user = Zend_Auth::getInstance()->getIdentity();
        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->_modelUser = new models_User;
        $this->_modelJob = new models_Jobs;
        $this->_modelContent = new models_Content;
        $this->_modelCompany = new models_Company;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $this->view->settings = $this->_settings;
    }

    public function getCandidateAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $candidates = $this->_modelUser->getCandidates();
        foreach ($candidates as $index => $user) {
            $candidates[$index]['skills'] = $this->_modelUser->collectUserSkillById($user['id']);
            $candidates[$index]['status'] = $this->_modelUser->collectUserStatusById($user['id']);
            $candidates[$index]['job_application'] = $this->_modelUser->collectJobsApplicationById($user['id']);
            $candidates[$index]['user_departments'] = $this->_modelUser->collectUserDepartmentsById($user['id']);
            $candidates[$index]['emails'] = $this->_modelUser->collectUserEmailsBy($user['id']);
        }
        $candidates = $this->_modelUser->checkBannedStatus($candidates);
        echo json_encode(['candidates' => $candidates, "me" => (array) $this->_user]);
        die;
    }

    public function getCandidateInfoAction() {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $user = $user = $this->_modelUser->getCandidateHistory($this->_payload['id']);
        $user_application_history = $this->_modelJob->getJobapplicationHistoryBuyUserId($this->_payload['id']);
        $user_all_application_history = $this->_modelJob->getNewJobapplicationHistoryBuyUserId($this->_payload['id']);
        $user_position = $this->_modelUser->getUserPositions($this->_payload['id']);
        $skills = $this->_modelUser->getUserSkills($this->_payload['id']);
        $education = $this->_modelUser->getUserEducation($this->_payload['id']);
        $All_status = $this->_modelJob->getStatus();
        $notes = $this->_modelUser->getNoteByUserId($this->_payload['id']);
        $emails = $this->_modelUser->collectUserEmailsBy($this->_payload['id']);
        $language = $this->_modelUser->collectUserLang($this->_payload['id']);
        if(is_array($user_application_history)) {
            $last_status = ($user_application_history[0]['application_status'] != "") ? $user_application_history[0]['application_status'] : "No data yet";
        }

        $modelJobs = new models_Jobs();
        $status = $modelJobs->getStatus();
        $status[0] ='Status';
        ksort($status);
        array_walk_recursive($status,function(&$value) {
            $value = $this->translate->_($value);
        });

        if(isset($this->_payload['status']) && $this->_payload['status'] == true) $this->_modelUser->changeAllNewJobsToview($user_application_history);
        echo json_encode([
            'candidate' => $user,
            'applications' => $user_application_history,
            'status_history' => $user_all_application_history,
            'position' => $user_position,
            'skills' => $skills,
            'educations' => $education,
            'all_status' => $status,
            'note' => $notes,
            'last_status' => $this->translate->_($last_status),
            'emails' => array_slice($emails,0,3),
            'email_history' =>  array_slice($emails,3),
            'language' =>  $language
                ]
        );
        die;
    }
    
    
    public function getCandidateJobInfoAction() {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $user = $user = $this->_modelUser->getCandidateHistory($this->_payload['id']);
        $user_application_history = $this->_modelJob->getJobapplicationHistoryBuyUserId($this->_payload['id']);
        $user_application_history_by_job = $this->_modelJob->getJobapplicationHistoryBuyUserIdAndJob($this->_payload['id'],$this->_payload['id_job']);
        $user_all_application_history = $this->_modelJob->getNewJobapplicationHistoryBuyUserId($this->_payload['id']);
        $user_position = $this->_modelUser->getUserPositions($this->_payload['id']);
        $skills = $this->_modelUser->getUserSkills($this->_payload['id']);
        $education = $this->_modelUser->getUserEducation($this->_payload['id']);
        $All_status = $this->_modelJob->getStatus();
        $notes = $this->_modelUser->getNoteByUserId($this->_payload['id']);
        $emails = $this->_modelUser->collectUserEmailsBy($this->_payload['id']);
        if (is_array($user_application_history)) {
            $last_status = ($user_application_history[0]['application_status'] != "") ? $user_application_history[0]['application_status'] : "No data yet";
        }
        $language = $this->_modelUser->collectUserLang($this->_payload['id']);
        if (isset($this->_payload['status']) && $this->_payload['status'] == true)
            $this->_modelUser->changeAllNewJobsToview($user_application_history);

        $modelJobs = new models_Jobs();
        $status = $modelJobs->getStatus();
        $status[0] ='Status';
        ksort($status);
        array_walk_recursive($status,function(&$value) {
            $value = $this->translate->_($value);
        });

        echo json_encode([
            'candidate' => $user,
            'applications' => $user_application_history,
            'application_job' => $user_application_history_by_job,
            'status_history' => $user_all_application_history,
            'position' => $user_position,
            'skills' => $skills,
            'educations' => $education,
            'all_status' => $status,
            'note' => $notes,
            'last_status' => $this->translate->_($last_status),
            'emails' => array_slice($emails, 0, 3),
            'email_history' => array_slice($emails, 3),
            'language' =>  $language,
            'application_removed_status' => 1
                ]
        );
        die;
    }

    public function getApplicationStatusAction() {

        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $application_status = $this->_modelJob->getJobNewApplicationStatus($this->_payload['id']);
        echo json_encode([
            'status' => $application_status]);
        die;
    }

    public function changeApplicationStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
//        die;
    }
    public function changeApplicationJobStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
//        die;
    }

    public function getSkillsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $modelContent = new models_Content;
        $skills = $modelContent->getSkills();
        usort($skills, function($a, $b) {
            return strcmp($a["title"], $b["title"]);
        });
        array_unshift($skills, ['title' => 'Skills']);
        echo json_encode([
            'skills' => $skills
        ]);
        exit;
    }

    public function getStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $status = $this->_settings['application_status'];
        $status = $this->_modelUser->getNewAllStatus();
        $modelJobs = new models_Jobs();
        $status = $modelJobs->getStatus();
//        echo '<pre>';print_r($this->_modelUser->getNewAllStatus());die;
//        array_unshift($status, ["-1",'Status']);
        $status[0] ='Status';
        ksort($status);
        array_walk_recursive($status,function(&$value) {
           $value = $this->translate->_($value);
        });
        $key = array_keys($status, 'Status');
        echo json_encode([
            'status' => $status,
            'default' => 0
        ]);
        exit;
    }

    public function getJobsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = [];
        $jobs = $this->_modelJob->getJobs();

        foreach ($jobs as $key => $job) {
            $data[] = ['id' => $job['id'], 'title' => $job["title"]];
        }
        array_unshift($data, ['id' => 0, 'title' => 'Jobs']);
        echo (json_encode([
            'jobs' => $data
        ]));
        die;
    }

    public function getDepartmentsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $departments = $this->_modelContent->getDepartments();
        $CustomDepartments = $this->_modelContent->getCustomDepartmentsSelect();
        usort($departments, function($a, $b) {
            return strcmp($a["title"], $b["title"]);
        });
        array_unshift($departments, ['id' => 0, 'title' => 'Departments']);
        echo (json_encode([
            'departments' => $departments
        ]));
        die;
    }

    public function noteAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function getApplicationAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $userEmail = ($this->_payload['email']);
        $applications = $this->_modelJob->getJobApplicationByUserEmail($userEmail);
        echo json_encode(['user_applications' => $applications, 'email' => $userEmail]);
        die;
    }
    
    
    public function noteUploadFileAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $upload_path = ABSOLUTE_PATH . "/public/resources/files/notes/";
        $file_no_ext = (string) substr($_FILES['file']['name'], 0, strrpos($_FILES['file']['name'], "."));
        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $file_name = WebApp_Tools::CreateAlias($file_no_ext) . "." . $extension;
        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . $file_name);
        $file_upload = new Zend_Session_Namespace('ats-upload');
        $file_upload->name = $file_name;
        die;
    }

    public function addNoteAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $file_upload = new Zend_Session_Namespace('ats-upload');
        $file_name = $file_upload->name;
        $id_note  = $this->_modelUser->saveNote($this->_payload);
        $this->_modelUser->saveNoteFile($id_note, $file_name);
        Zend_Session::namespaceUnset('ats-upload');
        exit;
    }

    public function setCandidateStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_user = $this->_modelUser->changeStatus($this->_payload['id_application'], $this->_payload['status']);
        $this->_modelUser->setjobApplicationStatusHistory($this->_payload['id_job'], $this->_payload['id_application'], $this->_payload['status'], $this->_payload['status_reason']);
        $user_application_history = $this->_modelJob->getJobapplicationHistoryBuyUserId($id_user);
        $user_all_application_history = $this->_modelJob->getNewJobapplicationHistoryBuyUserId($id_user);
        foreach ($user_application_history as $index => &$app)
        {
            if($app['application_status'] != "")
            {
                $user_application_history[$index]['application_status'] = $this->translate->_($app['application_status']);
            }
        }
        echo json_encode(['applications' => $user_application_history,
            'status_history' => $user_all_application_history,'data_target' => $this->_modelUser->getUserById($id_user)['email']], true);
        die;
    }

    public function getCandidateByEmailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $email = $this->_payload['email'];
        $user = $this->_modelUser->getUserDataByEmail($email);
        $notes = $this->_modelUser->getNoteByUserId($user['id']);
        echo (json_encode([
            'note' => $notes
        ]));
        die;
    }

    public function exportCvAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $users = $this->_payload['users'];
        $date = date("Y-m-d H:i:s");
        $zip_file_name = "Ats-Export-CV_" . $date . ".zip";
//        $real = ABSOLUTE_PATH . '/public/resources/files/cv';
        $real = APPLICATION_PATH.'/storage/cv/';
        $zip_file = "{$real}/{$zip_file_name}";
        exec("touch $zip_file");
        foreach ($users as $id) {
            $file_name = $this->_modelUser->getUserCv($id);
            if (!empty($file_name)) {
                foreach ($file_name as $key => $cv) {
                    $file_name = $cv['file'];
//                    $file_url = ABSOLUTE_PATH . '/public/resources/files/cv/' . $file_name;
                    $file_url = $real.$file_name;
                    $zip = new ZipArchive;
                    if ($zip->open($zip_file, ZipArchive::CREATE)) {
                        $zip->addFile($file_url, $file_name);
                        $zip->close();
                    }
                }
            } else {
                $generatedCvProfile = $this->_modelUser->saveExportCV($id, $real);
                $zip = new ZipArchive;
                if ($zip->open($zip_file, ZipArchive::CREATE)) {
                    $zip->addFile($generatedCvProfile, @end(explode("/", $generatedCvProfile)));
                    $zip->close();
                }
            }
        }

        echo json_encode(['path' => $real . $zip_file_name, 'name' => $zip_file_name], true);
        exec("rm -rf " . ABSOLUTE_PATH . '/public/' . $date . ".zip");

//        return $this->_modelUser->saveExportCV($id);
        die;
    }

    public function mailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $users = $this->_payload['users'];

            $data['sender'] = $this->_user->firstname . " " . $this->_user->lastname;
//            if ($users && is_array($users)) {
            foreach ($users as $id_user) {
                $user = $this->_modelUser->getUserById($id_user);
                $data['to'][] = $user['firstname'] . ' ' . $user['lastname'];
            }
//            }
            $data['message'] = 'Hi  {$candidate_name}, <br /> <br /> <br /> {$signature}';
            $data['subject'] = $this->_user->firstname . " " . $this->_user->lastname;
            if($this->_payload['job-details'] && $this->_payload['job-details'] > 0) {
                $id_job = (int) $this->_payload['job-details'];
                $job = $this->_modelJob->getJobById($id_job);
                $data['subject'] = $job['title']." : ".$this->_user->firstname . " " . $this->_user->lastname;
            }
//            if ($this->_payload['hm']) {
//                $data['to'][] = "HM";
//            }
            echo json_encode($data);
            die;
        }
    }

    public function sendMailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $sender = $this->_user;
        $signature = $this->_modelUser->getSignatureByUserId($sender->id);
        $company = $this->_modelCompany->getCompanyById($sender->id_company);
        $bcc = $this->_payload['bcc'];
        $message = $this->_payload['message'];
        $subject = $this->_payload['subject'];

        foreach ($bcc as $id_user) {
            $user = $this->_modelUser->getUserById($id_user);
            $smarty = new Smarty;
            $smarty->assign("candidate_name", $user['firstname']);
            $smarty->assign("signature", $signature['signature']);
            $smarty->assign("company", $company['title']);
            $emailBody = $smarty->fetch('eval:' . $message);
            $emailSubject = $smarty->fetch('eval:' . $subject);
            $mail = new WebApp_Mail();
            $mail->addTo($user['email'], $user['firstname']);
            $mail->setFrom($sender->email);
            $mail->setSubject($emailSubject);
            $mail->setBodyHtml($emailBody);
            $mail->send($transport);
            $this->_modelUser->insert(['id_sender' => Zend_Auth::getInstance()->getIdentity()->id,'id_receiver' => $user['id'],'subject' => $emailSubject,'content' => $emailBody,'date'=>date('Y-m-d H:i:s')],'ats_emails_history');
        }
        die;
    }

    public function hmMailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $users = $this->_payload['users'];
            $data['sender'] = $this->_user->firstname . " " . $this->_user->lastname;
            $data['message'] = 'Hello {$hm_name}, <br /> <br /> <br /> These are candidates that I think are suitable for the job. <br /><br /><br /> {$signature}';
            $data['to'][] = "HM";
            $data['subject'] = $this->_user->firstname . " " . $this->_user->lastname;
            if($this->_payload['job-details'] && $this->_payload['job-details'] > 0) {
                $id_job = (int) $this->_payload['job-details'];
                $job = $this->_modelJob->getJobById($id_job);
                $data['subject'] = $job['title']." : ".$this->_user->firstname . " " . $this->_user->lastname;
            }
            echo json_encode($data);
            die;
        }
//        exit;
    }

    public function sendHmMailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $message = $this->_payload['message'];
        $subject = $this->_payload['subject'];
        $candidates = $this->_payload['list_candidate'];
        $transport = null;
        if (!empty($this->_settings['email_smtp'])) {
            $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
            $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
        }
        $sender = $this->_user;
        $signature = $this->_modelUser->getSignatureByUserId($sender->id);
        $company = $this->_modelCompany->getCompanyById($sender->id_company);
        $hm = $this->_modelUser->getHmUsers();
        if (is_array($hm) && !empty($hm)) {
            foreach ($hm as $index => $user) {
                $smarty = new Smarty;
                $smarty->assign("hm_name", $user['firstname']);
                $smarty->assign("signature", $signature['signature']);
                $smarty->assign("company", $company['title']);
                $emailBody = $smarty->fetch('eval:' . $message);
                $emailSubject = $smarty->fetch('eval:' . $subject);
                $mail = new WebApp_Mail();
                $mail->addTo($user['email'], $user['firstname']);
                $mail->setFrom($sender->email);
                foreach ($candidates as $id_candidate) {
//                   $zip_file_name = str_replace(" ", "_", $this->_modelUser->getUserById($id_candidate)['firstname'] . "_" . date("Y-m-d H:i:s") . ".zip");
//                   $real = ABSOLUTE_PATH . '/public/resources/files/cv';
//                   $zip_file = "{$real}/{$zip_file_name}";
//                   exec("touch $zip_file");
                    //APPLICATION_PATH . "/storage/cv/
                    $generatedCvProfile = $this->_modelUser->saveExportCV($id_candidate, APPLICATION_PATH . '/storage/cv/');
//                    $generatedCvProfile = $this->_modelUser->saveExportCV($id_candidate, ABSOLUTE_PATH . '/public/resources/files/cv');
//                   $zip = new ZipArchive;
//                    if ($zip->open($zip_file, ZipArchive::CREATE)) {
//                        $zip->addFile($generatedCvProfile, "profile_cv.pdf");
//                        $zip->close();
//                    }

                    $content = file_get_contents($generatedCvProfile); // e.g. ("attachment/abc.pdf")
                    $attachment = new Zend_Mime_Part($content);
                    $attachment->type = 'application/pdf';
                    $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = Zend_Mime::ENCODING_BASE64;
                    $attachment->filename = end(explode("/", $generatedCvProfile)); // name of file
                    $mail->addAttachment($attachment);
                }


                $mail->setSubject($emailSubject);
                $mail->setBodyHtml($emailBody);
                $mail->send($transport);
                $this->_modelUser->insert(['id_sender' => Zend_Auth::getInstance()->getIdentity()->id,'id_receiver' => $user['id'],'subject' => $emailSubject,'content' => $emailBody,'date'=>date('Y-m-d H:i:s')],'ats_emails_history');
            }
        }
    }

    public function userApplicationAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_job = $this->_payload['id_job'];
        $candidates = $this->_modelUser->getCandidateJobApplication($id_job);
        foreach ($candidates as $index => $user) {
            $candidates[$index]['skills'] = $this->_modelUser->collectUserSkillById($user['id']);
            $candidates[$index]['status'] = $this->_modelUser->collectUserStatusByJobId($user['id'], $id_job);
            $candidates[$index]['job_application'] = $this->_modelUser->collectJobsApplicationById($user['id']);
            $candidates[$index]['user_departments'] = $this->_modelUser->collectUserDepartmentsById($user['id']);
        }
        $candidates = $this->_modelUser->checkBannedStatus($candidates);
        echo json_encode(['candidates' => $candidates, 'me' => (array) Zend_Auth::getInstance()->getIdentity()]);
        die;
    }

    public function applicantChangeStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function applicantChangeAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $All_status = $this->_modelJob->getStatus();
        echo json_encode(['all_status' => $All_status], true);
        die;
    }

    public function setBulkStatusAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $users = $this->_payload['applicants'];
        $id_job = $this->_payload['id_job'];
        $status = $this->_payload['status'];
        $status_reason = $this->_payload['status_reason'];
        $id_applications = [];
        foreach ($users as $id) {
            $id_applications[] = $this->_modelUser->getApplicationbyids($id, $id_job);
        }
        foreach ($id_applications as $id_application) {
            $this->_modelUser->changeStatus($id_application, "{$status}.{$status_reason}");
        }


        exit;
    }

    public function importCvAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $upload_path = ABSOLUTE_PATH . "/public/resources/files/cv/";
        $upload_path = APPLICATION_PATH."/storage/cv/";
//        $preview_path = "/resources/files/cv/";
        $preview_path = APPLICATION_PATH."/storage/cv/";
        $bulk = new Zend_Session_Namespace('bulk-cv');
        if ($this->getRequest()->isPost()) {
            $allow_extension = ['pdf', 'docx', 'doc', 'jpg', 'png'];
            $file_no_ext = (string) substr($_FILES['file']['name'], 0, strrpos($_FILES['file']['name'], "."));
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $file_name = WebApp_Tools::CreateAlias($file_no_ext) . "." . $extension;
            $original_cv = $file_name;
            if (in_array($extension, $allow_extension)) {
                switch (@end(explode('.', $file_name))) {
                    case 'jpg':
                        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . "{$file_name}");
                        break;
                    case 'png':
                        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . "{$file_name}");
                        break;
                    case 'pdf':
                        $file_renamed = uniqid() . '.jpg';
                        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . "{$file_name}");
                        $im = new imagick($upload_path . $file_name);
                        $im->setImageFormat('jpg');
                        $im->writeImage($upload_path . $file_renamed);
                        $file_name = $file_renamed;
                        break;
                    case 'docx':
                        $file_renamed = uniqid() . '.jpg';
                        $pdf_file = uniqid() . '.pdf';
                        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . $file_name);
                        Unoconv::convertToPdf($upload_path . $file_name, $upload_path . $pdf_file);
                        $im = new imagick($upload_path . $pdf_file);
                        $im->setImageFormat('jpg');
                        $im->writeImage($upload_path . $file_renamed);
                        $file_name = $file_renamed;
                        break;
                    case 'doc':
                        $file_renamed = uniqid() . '.jpg';
                        $pdf_file = uniqid() . '.pdf';
                        move_uploaded_file($_FILES['file']['tmp_name'], $upload_path . $file_name);
                        Unoconv::convertToPdf($upload_path . $file_name, $upload_path . $pdf_file);
                        $im = new imagick($upload_path . $pdf_file);
                        $im->setImageFormat('jpg');
                        $im->writeImage($upload_path . $file_renamed);
                        $file_name = $file_renamed;
                        break;
                }
                $cv['file_name'] = $file_name;
                $cv['file_preview_path'] = $preview_path . $file_name;
                $cv['file_icon'] = $extension;
                $cv['cv'] = $original_cv;
                $bulk->bulk_cv[] = $cv;

                echo '<pre>';
                print_r($bulk->bulk_cv);
                die;
            } else {
                echo json_encode(['error' => true, 'message' => "Extension Not Allowed"], true);
                exit;
            }
        }
    }

    public function removeBulkSessionAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Session::namespaceUnset('bulk-cv');
    }

    public function getCvsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $bulk = new Zend_Session_Namespace('bulk-cv');
        echo json_encode(['cvs' => $bulk->bulk_cv], true);
        exit;
    }

    public function uploadCvsAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function saveBulkCvAction() {
        Zend_Session::namespaceUnset('bulk-cv');
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $modelCompany = new models_Company ;
        $cv = $this->_payload['cv'];
        $id_job = ($this->_payload['id_job'] > 0) ? $this->_payload['id_job'] : 0 ;
        $candidates = [];
        $i = 0;
        if(!empty($cv)) {
            foreach ($cv as $c) {
                $i++;
                foreach ($c as $key => $value) {
                    $candidates[$i][array_shift(array_keys($value))] = array_shift(array_values($value));
                    $candidates[$i]['type'] = '9';
                    $candidates[$i]['id_company'] = $this->_user->id_company;
                    $candidates[$i]['created'] = date("Y-m-d H:i:s");
                    $candidates[$i]['status'] = 1;
                }
            }
        }

        if (!empty($candidates)) {
            foreach ($candidates as $key => $candidate) {
//                if ($candidate['email'] == "") unset($candidates[$key]);
                $files = json_decode($candidate['files'], true);
                unset($candidate['files']);
                $last_job = $candidate['last_job'];
                unset($candidate['last_job']);
                $id_user = (int) $this->_modelUser->saveCandidateUser($candidate, true,45);
                $this->_modelUser->insert(['id_user' => $id_user, 'title' => $files['cv'], 'file' => $files['cv'], 'date' => date('Y-m-d H:i:s')], 'user_cv');
                $this->_modelUser->insert(['id_user' => $id_user,'title' => $last_job],'user_positions');

                if($id_job && $id_job > 0) {
                    $candidate = $this->_modelUser->getUserById($id_user);
                    $this->_modelUser->setApplicationuploadCandidateCv(['id_job' => $id_job, 'id_user' => $id_user, 'id_recommender' => Zend_Auth::getInstance()->getIdentity()->id, "date" => date("Y-m-d H:i:s"), 'status' => 0, 'status_reason' => 0, 'source' => 'website']);
                    $this->_modelUser->sendEmailTemplate(
                            ['company_title' => $this->_modelUser->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['title'],
                        'user' => ['firstname' => Zend_Auth::getInstance()->getIdentity()->firstname, 'lastname' => Zend_Auth::getInstance()->getIdentity()->lastname],
                        'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                        'job' => $job,
                        'settings' => $this->_settings,
                        'id_job' => $id_job,
                            ], 41, $modelCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company)['email']);

                    $this->_modelUser->sendEmailTemplate(
                            ['company' => $this->_modelUser->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company),
                        'job' => $job,
                        'password' => $password,
                        'user' => ['firstname' => Zend_Auth::getInstance()->getIdentity()->firstname, 'lastname' => Zend_Auth::getInstance()->getIdentity()->lastname],
                        'candidate' => ['firstname' => $candidate['firstname'], 'lastname' => $candidate['lastname']],
                        'settings' => $this->_settings,
                        'id_job' => $id_job,
                            ], 42, $candidate['email']);
                }
            }
        }

        die;
    }
    
    public function getJobsCvAction(){
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $jobs = $this->_modelJob->getJobsByCompanyId($this->_user->id_company);
        foreach ($jobs as $key => $job) {
            $data[] = ['id' => $job['id'], 'title' => $job["title"]];
        }
//        array_unshift($data, ['id' => 0, 'title' => 'Select Jobs']);
        echo json_encode(['jobs' => $data],true);
        die;
    }
    
    public function historyEmailAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
    }
    public function historyAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_payload['data'];
        $data = $this->_modelUser->getEmailTemplateHistory($id);
        echo json_encode(['email' => $data],true);
        die;
    }
    
    public function getAppInfoAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_application = $this->_payload['id_application'];
        $id_job = $this->_payload['id_job'];
        $email_user = $this->_payload['email'];
        $job = $this->_modelJob->getJobById($id_job);
        $user_application_history_by_job = $this->_modelJob->getJobapplicationHistoryBuyUserIdAndJob($this->_payload['id'],$this->_payload['id_job']);
        echo json_encode(['email' => $email_user,'id_application' => $id_application,'id_job'=>$id_job,'title' => $job['title']],true);
        die;
        
    }

    public function getSourcesAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $model_content = new models_Content;
        $sources = $model_content->getSources();
        echo json_encode($sources,true);
        die;
    }

}
