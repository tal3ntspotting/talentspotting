<?php

class Jobs_PaymentsController extends Zend_Controller_Action {

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;
    public $translate;

    public function init() {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_Payments;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;
        $this->view->headTitle()->set('Brainspotting');

        if ($this->_model->_user->type > 1) {
            $session = new Zend_Session_Namespace("redirector");
            $session->url = $this->getRequest()->getRequestUri();
        }
    }



    public function billingAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $id_billing = $this->_params['id_billing'];
        $this->view->billing_info = $this->_model->getBillingInfo();
        $this->view->billing = $this->_model->getBillingData($id_billing);
        $this->view->id_billing = $this->view->billing['id'];

        if ($this->getRequest()->isPost()) {
            $company = $this->_model->getCompanyById($this->_model->_user->id_company);

            if ($this->_params['id_billing'] < 1) {
                $billingData['title'] = $this->_params['title'];
                $billingData['cui'] = $this->_params['cui'];
                $billingData['j'] = $this->_params['j'];
                $billingData['address'] = $this->_params['address'];
                $billingData['bank'] = $this->_params['bank'];
                $billingData['firstname'] = $this->_params['firstname'];
                $billingData['lastname'] = $this->_params['lastname'];
                $billingData['phone'] = $this->_params['phone'];
                $billingData['email'] = $this->_params['email'];
                $billingData['payment_method'] = $this->_params['payment_method'];
                $billingData['id_company'] = $this->_model->_user->id_company;

                $this->_model->insert($billingData, "payment_billing_info");
                $id_billing = $this->_model->lastInsertId();
            } else {
                $id_billing = $this->_params['id_billing'];
            }

            if ($this->_params['id_package'] < 1) {
                $package['title'] = webapp_tools::CreateAlias($company['title'] . "-" . $this->_params['job_credits'] . "-" . $this->_params['search_credits']);
                $package['job_credits'] = $this->_params['job_credits'];
                $package['search_credits'] = $this->_params['search_credits'];
                $package['amount'] = $this->_getPackageAmount($this->_params['job_credits'], $this->_params['search_credits']);
                $package['currency'] = 1;

                $this->_model->insert($package, "packages");
                $id_package = $this->_model->lastInsertId();
            } else {
                $id_package = $this->_params['id_package'];
                $package = $this->_model->getPackageById($id_package);
            }

            $payment['id_billing_info'] = $id_billing;
            $payment['id_company'] = $this->_model->_user->id_company;
            $payment['id_user'] = $this->_model->_user->id;
            $payment['id_package'] = $id_package;
            $payment['currency'] = $package['currency'];
            $payment['amount'] = $package['amount'];
            $payment['type'] = $this->_params['payment_method'];
            $payment['status'] = 'pending';



            $this->_model->insert($payment, "payments");
            $this->_model->update(array("had_demo" => 1), "id = {$payment['id_company']}", "companies");
            //$this->_model->update(array("job_credits" => $package['job_credits'] + $company['job_credits'], "search_credits" => $package['search_credits'] + $company['search_credits']), "id_company = {$company['id']}", "company_credits");

            switch ($this->_params['payment_method']) {
                case "op":
                    $msg = sprintf($this->translate->_("payment_pending"), round($package['amount']) . " USD", $package['job_credits'] + $company['job_credits'], $package['search_credits'] + $company['search_credits']);
                    break;

                default:
                    $msg = sprintf($this->translate->_("payment_success"), round($package['amount']) . " USD", $package['job_credits'] + $company['job_credits'], $package['search_credits'] + $company['search_credits']);
                    break;
            }


            $this->_model->insert(array("id_company" => $company['id'], "type" => "payment", "message" => "new_package_purchase", "url" => "view-payments"), "notifications");
            $this->_model->insert(array("id_company" => $company['id'], "target" => "admin", "type" => "payment", "message" => "new_package_purchase", "url" => "view-payments"), "notifications");

            $transport = null;
            if (!empty($this->_settings['email_smtp'])) {
                $config = array('auth' => $this->_settings['email_smtp']['auth'], 'username' => $this->_settings['email_smtp']['username'], 'password' => $this->_settings['email_smtp']['password']);
                $transport = new Zend_Mail_Transport_Smtp($this->_settings['email_smtp']['host'], $config);
            }

            $users = $this->_model->getAdmins();

            $mail = new WebApp_Mail();
            $mail->addTo($users[0]['email'], $users[0]['firstname']);
            if (count($users) > 1) {
                for ($i = 1; $i < count($users); $i++) {
                    $mail->addCc($users[$i]['email']);
                }
            }

            $mail->setFrom($this->_settings['email_smtp']['username'], $this->_settings['sitename']);
            $mail->setSubject("New payment made");
            $mail->setBodyFromViewScript("payment.phtml", array("user" => $users[0], "company" => $company, "package" => $package, "settings" => $this->_settings, "payment" => $payment));
            $mail->send($transport);

            $this->_flashMessenger->addMessage($msg);
            if ($this->_params['acc'] == 'new') {
                $this->_redirect("/company");
            } else {
                $this->_redirect("/view-payments");
            }
        }

        $this->view->current_menu = "payments";

        $this->view->headTitle()->set('Billing | ' . $this->_settings['sitename'] . '');
    }

    private function _getPackageAmount($job_credits, $search_credits) {
        $modelPayments = new models_Payments;
        $package = $modelPayments->getDefaultPackage();

        $amount = 0;
        if ($job_credits > 0) {
            $amount += $this->_settings['costs']['job'] * $job_credits;
        }

        if ($search_credits > 0) {
            $amount += $this->_settings['costs']['search'] * $search_credits / 3;
        }

        return $amount;
    }

    public function confirmAction() {
        $id = $this->_params['id'];
        $payment = $this->_model->getPaymentById($id);
        $package = $this->_model->getPackageById($payment['id_package']);
        $company = $this->_model->getCompanyById($payment['id_company']);

        $this->_model->update(array("job_credits" => $package['job_credits'] + $company['job_credits'], "search_credits" => $package['search_credits'] + $company['search_credits'], "highlight_credits" => $package['highlight_credits'] + $company['highlight_credits'], "added" => date("Y-m-d H:i:s")), "id_company = {$company['id']}", "company_credits");
        $this->_model->update(array("status" => "confirmed"), "id = {$id}", "payments");
        $this->_model->insert(array("id_company" => $company['id'], "type" => "notification", "message" => "payment_confirmed"), "notifications");
        $this->_redirect("/view-payments");
    }

    public function bountiesAction() {
        
        $this->view->items = $this->_model->getBounties();
        $this->view->campaign_items = $this->_model->getCampaignBounty();
        $modelsUser = new models_User;
        
//        $this->view->bounties_to_approve = $modelsUser->getBountiesToApprove($this->_model->_user->id_company) + $modelsUser->getCampaignToPaidBounties($this->_model->_user->id_company);
        $this->view->bounties_to_approve = $modelsUser->getBountiesToApprove($this->_model->_user->id_company);
        $this->view->bounties_paid = $modelsUser->getPaidBounties($this->_model->_user->id_company) + $modelsUser->getCampaignPaidBounties($this->_model->_user->id_company);
            
        $this->view->current_menu = "payments";
        if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
            $this->view->current_menu = "bounties";
        }
        $this->view->current_sub_menu = "bounty";

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }
    
    public function campaignBountiesAction() {
         
        $this->view->items = $this->_model->getCampaignBounty();
        $this->view->job_items = $this->_model->getBounties();
        $modelsUser = new models_User;

        $this->view->bounties_to_approve = $modelsUser->getCampaignToPaidBounties($this->_model->_user->id_company);
        $this->view->bounties_paid = $modelsUser->getCampaignPaidBounties($this->_model->_user->id_company);
//        echo '<pre>'; print_r($this->view->items);die;


        $this->view->current_menu = "payments";
        if (Zend_Auth::getInstance()->getIdentity()->type == 2) {
            $this->view->current_menu = "bounties";
        }
        $this->view->current_sub_menu = "bounty";

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }

    public function payBountyAction() {
        $id = $this->_params['id'];
        $this->_model->payCompanyBounty($id);

        $this->_redirect("/bounties");
    }

    public function viewBountiesAction() {
        $this->view->items = $this->_model->getBounties();

        $this->view->current_menu = "payments";
        $this->view->current_sub_menu = "bounty";

        $this->view->headTitle()->set('View bounties | ' . $this->_settings['sitename'] . '');
    }

    public function confirmBountyAction() {
        $id = $this->_params['id'];
        $status = $this->_params['status'];
        $this->_model->payBounty($id, $status);
        $paymentInfo = $this->_model->getPaiementInfo($id);
        if($status == "paid")
        {
           $message = $this->translate->_("bounty_paid_success");
           $this->_flashMessenger->addMessage(sprintf($message,$paymentInfo['job']['title'],$paymentInfo['user']['firstname']." ".$paymentInfo['user']['lastname']));
           $bsp_bounty = $this->_model->getCompanyById($this->_model->_user->id_company)['brainspotting_bounty'];
           if($bsp_bounty) {
               $this->_model->sendBspNotification($paymentInfo);
           }
        } else if($status == "canceled") 
        {
            $message = $this->translate->_("bounty_cancelled");
            $this->_flashMessenger->addMessage(sprintf($message,$paymentInfo['user']['firstname']." ".$paymentInfo['user']['lastname']));
        }
        $this->_redirect("/bounties");
    }

    public function bountyFilesAction() {
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = $this->_params['id'];
        $this->view->bounty = $this->_model->getBountyDetailed($id);
        $this->view->files = $this->_model->getBountyFiles($id);
    }


    public function demoAction() {
        $company = $this->_model->getCompanyById($this->_model->_user->id_company);

        if ($company['had_demo'] < 1 && $company['id'] > 0) {
            $this->_model->update(array("credits" => 1, "job_credits" => 1, "search_credits" => 1, "highlight_credits" => 0, "added" => date("Y-m-d H:i:s")), "id_company = {$company['id']}", "company_credits");
            $this->_model->update(array("had_demo" => 1), "id = {$company['id']}", "companies");
            $this->_redirect("/dashboard");
        } else if (!isset($company['id'])) {
            $this->_flashMessenger->addMessage("You have to register a company first. <a href='/login-company'>Click here</a> to create a new company.");
            $this->_redirect("/recharge");
        } else {
            $this->_flashMessenger->addMessage("You already have a purchased package. You can try the demo only if you had no previous subscription.");
            $this->_redirect("/recharge");
        }
    }
    
    public function campaignWinnerAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id_campaign = $this->_params['id'];
        $this->view->items = $this->_model->getCampaignReport($id_campaign);
        $this->view->id_campaign = $id_campaign ;
        
          if ($this->getRequest()->isPost()) {
            $users = (array) $this->_params['users'] ;
            $id_campaign = (int) $this->_params['id'] ;
            $this->_model->setCampainWinners($id_campaign,$users);
            $this->_redirect('/campaign-bounties');

        }
    }

    

}
