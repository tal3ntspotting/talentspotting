<?php
class Jobs_AdminController extends Zend_Controller_Action
{
	protected $_model;
	protected $_id_lang;
	protected $_settings;
	private $_params;
	protected $_flashMessenger;
    protected $dg_js;


	public function init()
	{
		$this->_params = $this->getRequest()->getParams();
		$this->view->params = $this->_params;
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

		$this->_model = new models_Jobs;

		$layout = Zend_Layout::getMvcInstance();
		$layout->setLayout('admin');

		$this->view->lang = APPLICATION_LOCALE;
		$lang = Zend_Registry::get('_LANG_DETAILS');
        
        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

		$bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
		$this->_settings = $bootstrap['resources']['Settings'];

		$this->view->settings = $this->_settings;
        
        $session = new Zend_Session_Namespace("redirector");
        $session->url = $this->getRequest()->getRequestUri();
	}
    
    public function viewAction()
    {
        if(!Zend_auth::getInstance()->hasIdentity()) {
            $this->_redirect("/login");
        }

        $t['t_to'] = $this->translate->_('You are going to change the status for');
        $t['t__to'] = $this->translate->_('to');
        $t['t__from'] = $this->translate->_('from');
        $t['new'] = ucfirst($this->translate->_('New'));
        $t['viewed'] = ucfirst($this->translate->_('Viewed'));
        $t['hired'] = ucfirst($this->translate->_('Hired'));
        $t['called'] = ucfirst($this->translate->_('Called'));
        $t['interview'] = ucfirst($this->translate->_('Interview'));
        $t['test'] = ucfirst($this->translate->_('Test'));
        $t['offer'] = ucfirst($this->translate->_('Offer'));
        $t['final_interview'] = ucfirst($this->translate->_('Final interview'));
        $t['end'] = ucfirst($this->translate->_('end'));
        $t['background_check'] = ucfirst($this->translate->_('Background Check'));
        $t['assesment'] = ucfirst($this->translate->_('Assesment'));
        $t['passed'] = ucfirst($this->translate->_('Passed'));
        $t['no_show'] = ucfirst($this->translate->_('No show'));
        $t['to_be_called'] = ucfirst($this->translate->_('To be called'));
        $t['unfit'] = ucfirst($this->translate->_('Unfit'));
        $t['not_interested'] = ucfirst($this->translate->_('Not interested'));
        $t['no_answer'] = ucfirst($this->translate->_('No answer'));
        $t['moved_to_another_process'] = ucfirst($this->translate->_('Moved to another process'));
        $t['not_passed'] = ucfirst($this->translate->_('Not passed'));
        $t['offer_accepted'] = ucfirst($this->translate->_('Offer accepted'));
        $t['offer_rejected'] = ucfirst($this->translate->_('Offer rejected'));
        $t['candidates'] = ucfirst($this->translate->_('candidates'));
        $t['applied_on'] = ucfirst($this->translate->_('Applied on'));
        $t['latest_lob'] = ucfirst($this->translate->_('Latest Job'));
        $t['recommender'] = ucfirst($this->translate->_('recommender'));
        $t['status_days'] = ucfirst($this->translate->_('status days'));
        $t['applied_for'] = ucfirst($this->translate->_('Applied for'));
        $t['job_status'] = ucfirst($this->translate->_('Job status'));
        $t['active_jobs'] = ucfirst($this->translate->_('Active jobs'));
        $t['inactive_jobs'] = ucfirst($this->translate->_('Inactive jobs'));
        $t['job_name'] = ucfirst($this->translate->_('Job name'));
        $t['source'] = ucfirst($this->translate->_('Source'));
        $t['no_em'] = ucfirst($this->translate->_('No emails will be sent on this status update'));
        $t['em_tbs'] = ucfirst($this->translate->_('The following emails are going to be sent, if remain checked'));
        $t['phone'] = ucfirst($this->translate->_('phone'));
        $t['days'] = ucfirst($this->translate->_('days'));
        $t['reset'] = ucfirst($this->translate->_('Reset'));
        $t['city'] = ucfirst($this->translate->_('City'));
        $t['job'] = ucfirst($this->translate->_('job'));
        $t['move_to_another_two'] = ucfirst($this->translate->_('Create a new job-application for this candidate for a more suitable position - same recruiting step'));
        $t['aware'] = ucfirst($this->translate->_('Please be aware that by moving candidate to another process, you will close the current process and a new job application will be created'));
        $t['confirm_delete_user'] = $this->translate->_('Are you sure you want to delete this user');
        $t['confirm_duplicate'] = $this->translate->_('Are you sure you want to duplicate this item');
        $t['confirm_delete_job'] = $this->translate->_('Are you sure you want to delete this job');
        $t['confirm_activate_item'] = $this->translate->_('Are you sure you want to activate this item');
        $t['view_all'] = $this->translate->_('view all');
        $t['more_details'] = $this->translate->_('more_details');
        $t['social_href'] = $this->translate->_('Check out this great Job posted on Talentspotting');
        $t['job_application'] = strtoupper($this->translate->_('job application'));
        $t['apply'] = $this->translate->_('apply');
        $t['here'] = $this->translate->_('here');
        $t['hello'] = $this->translate->_('hello');
        $t['on_win'] = $this->translate->_('on_win');
        $t['on_hire'] = $this->translate->_('on_hire');
        $t = json_encode($t);
        $this->view->dg_js = "<script> var trans_key = $t ;</script>";
//        $substyles = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'layouts' .
//            DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR.'sublayouts' ;
//        $view = new Zend_View();
//        $view->setScriptPath($substyles);
//        $partialHelper = new Zend_View_Helper_Partial();
//        $partialHelper->setView($view);
//        $data = [];
//        $bodyData = $partialHelper->partial( 'dragndrop.phtml', $data );
//        $this->_helper->viewRenderer->setRender('dragndrop');
//        $this->view->dragndrop = $bodyData;

//        if(Zend_Auth::getInstance()->getIdentity()->type == 5 )  $this->_helper->viewRenderer->setRender('hm/view');
//        if(Zend_Auth::getInstance()->getIdentity()->type == 4)  $this->_helper->viewRenderer->setRender('recruiter/view');
        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "view";
        $filters = ($this->_model->_user->type == 2) ? array("id_company" => array($this->_model->_user->id_company)) : array();

        $user = Domain_User_UserFactory::createInstance(Zend_Auth::getInstance()->getIdentity()->id);
        $this->view->items = $user->getJobManagement((new models_Jobs()),$filters);
//        if($this->_settings['ats'] && Zend_Auth::getInstance()->getIdentity()->type == 5) {
//            $this->view->items = $this->_model->atsgetJobsHm($filters);
//        }
//        else if($this->_settings['ats'] && Zend_Auth::getInstance()->getIdentity()->type == 4) {
//            $this->view->items = $this->_model->atsRecrutergetJobs($filters);
//
//        } else {
//            $this->view->items = $this->_model->getJobs($filters);
//        }


        $this->view->views = $this->_model->getJobsViewsCounter();

        $this->view->applications = $this->_model->getJobsApplicationsCounted();

        if (isset($this->_params['export']))
        {
            $this->_model->export($this->view->items, "jobs_list.csv");
        }

        if (isset($this->_params['export-pdf']))
        {
            echo "<script>window.print();</script>";
        }

        $this->view->headTitle()->set('View Jobs | '.$this->_settings['sitename'].'');
        
    }
    
    public function editAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->_params['id'];
        if ($this->_model->_user->id_company > 0 && $this->_model->_user->type == 2)
        {
//            $company = $this->_model->getCompanyById($this->_model->_user->id_company);
//            if (($id < 1 && $company['job_credits'] < 1) || $company['status'] == 0)
//            {
//                $this->_flashMessenger->addMessage('no_job_credits');
//                $this->_redirect("/view-jobs");
//            }
        }
        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "edit";
        
        $data = array();
        
        if ($id > 0)
        {
            $data = $this->_model->getJobById($id);
//            $job = (new Domain_Job_Service_GetJobByIDService($id))->execute();
//                echo '<pre>';print_r($job);die;
            
            if ($this->_model->_user->id_company > 0 && $data['id_company'] != $this->_model->_user->id_company && $this->_model->_user->type == 2)
            {
                throw new Zend_Controller_Action_Exception('Authorization failed', 403);
                exit();
            }
            
            $data['skills'] = $this->_model->getJobSkills($id);
            $data['cities'] = $this->_model->getJobCities($id);
            $data['departments'] = $this->_model->getJobDepartments($id);
            $data['industries'] = $this->_model->getJobIndustries($id);
            $data['id_lang'] = $this->_model->getJobLanguages($id);
            $data['sub_departments'] = $this->_model->getJobSubDepartments($id);
            $data['sub_sub_departments'] = $this->_model->getJobSubSubDepartments($id);

            $data['skills'] = array_keys($data['skills']);
            $data['cities'] = array_keys($data['cities']);
            $data['departments'] = array_keys($data['departments']);
            $data['sub_departments'] = array_keys($data['sub_departments']);
            $data['sub_sub_departments'] = array_keys($data['sub_sub_departments']);
            $data['industries'] = array_keys($data['industries']);
            $data['id_lang'] = array_keys($data['id_lang']);

        }

        
        $form = new modules_jobs_forms_EditJob(array('disableLoadDefaultDecorators' => true, 'company' => (array) $company));		
        $form->populate($data);
        
        $this->view->id = $id;
        $this->view->form = $form;
        echo '<pre>';print_r($_POST);die;
        if ($this->getRequest()->isPost())
        {
//            echo '<pre>';print_r($_POST);die;
            
            $_POST['title'] = strip_tags($_POST['title']);
            $_POST['bounty'] = htmlspecialchars(strip_tags($_POST['bounty']), ENT_QUOTES, 'UTF-8');
            $_POST['bounty_requirements'] = htmlspecialchars(strip_tags($_POST['bounty_requirements']), ENT_QUOTES, 'UTF-8');
//            if($form->isValid($_POST))
//            {
//                $this->_model->save($_POST);
//            } else {
//                $this->_flashMessenger->addMessage(WebApp_Tools::mapError($form));
//            }
            $this->_model->save($_POST);
            $this->_redirect("/view-jobs");
        }        
        
        $this->view->headTitle()->set('Add/Edit Job | '.$this->_settings['sitename'].'');
    }
    
    public function jobDetailsAction()
    {
        $ats = $this->_settings['ats'];
        if ($ats) {
            $this->_helper->viewRenderer->setRender('ats/job-details');
        }

        $this->view->current_menu = "jobs";
        $this->view->current_sub_menu = "view";
        $this->view->headTitle()->set('Job Applications | '.$this->_settings['sitename'].'');
        
        $id_app = $this->_params['id_app'];
        
        if ($id_app > 0)
        {
            $this->_model->update(array("viewed" => 1), "id = {$id_app}", "job_applications");
        }
        
        $id = $this->_params['id'];
        $job = $this->_model->getJobById($id);
        $application = $this->_model->getJobApplicationById($this->_params['id_application']);
        $modelCompany = new models_Company;
        $company = $modelCompany->getCompanyById($job['id_company']);
        
        if ($this->getRequest()->isPost())
        {
            $this->_model->update(array("status" => $this->_params['status'], "status_reason" => $this->_params['status_reason'], "pending" => 0), "id = '{$this->_params['id_application']}'", "job_applications");
            if ($this->_params['status'] == 4 && $this->_params['status_reason'] == 5)
            {
                $this->_model->insert(array("id_user" => $application['id_user'], "action" => 5, "points" => $this->_model->_stats['complete_application'], 'date' => date("Y-m-d H:i:s")), "gamification_points");
                $this->_model->insert(array("id_user" => $this->_model->_user->id, "action" => 5, "points" => $this->_model->_stats['complete_application'], 'date' => date("Y-m-d H:i:s")), "gamification_points");
                $this->_model->_stats->setNotification($application['id_user'], "user", "jobs", "Congratulations for your new career start!<br>Help your friends as well.<br>Recommend a job now!");
            }
            
            if ($this->_params['status'] == 4 && $this->_params['status_reason'] == 0)
            {
                $this->_model->_stats->setNotification($application['id_user'], "user", "profile", "<strong>Great news!</strong><br>Company {$company['title']} invited you to an interview.<br>Best of luck!");
            }
            
            $this->_flashMessenger->addMessage('status_changed');
            $this->_redirect("/job-details/?id={$this->_params['id']}");
        }
        
        $this->view->job = $job;
        if ($this->_params['location'] == 'applicant')
        {
            $this->view->items = $this->_model->getJobApplicationsDetails($id);    
            $filters['location'] = 'applicant';
            
        }
        else if ($this->_params['location'] == 'database')
        {
            $items = $this->_model->getJobApplicationsDetails($id);
            foreach ((array)$items  as $item)
            {
                $users[] = $item['id'];
            }
//            $this->view->database = $this->_model->getSimilarUsersForJob($job, $users);
            $filters['location'] = 'database';    
            
        }
        else
        {
            $this->view->items = $this->_model->getJobApplicationsDetails($id);
            $this->view->items =  $this->_model->algorithMatch($this->view->items);
            
            foreach ($this->view->items as $key =>  &$user) {
                $this->view->items[$key]['hasReview'] = $this->_model->userHasReview($user['id_user'],$user['id_job']);
                $this->view->items[$key]['hasRejected'] = $this->_model->userReviewRejected($user['id_user'],$user['id_job'])['ignore_rate'];
            }

            foreach ((array)$this->view->items as $item)
            {
                $users[] = $item['id'];
            }
//            $this->view->database = $this->_model->getSimilarUsersForJob($job, $users);
            $filters['location'] = ''; 
        }
        $this->view->status = $this->_model->getStatus();
//    echo '<pre>';
//    print_r($this->view->items);
//    print_r($this->view->status);
//    die;
    }
    
    
    public function deleteAction()
    {
        $id = $this->_params['id'];
        $this->_model->delete("id = {$id}", "jobs");
        $this->_model->delete("id_job = {$id}", "jobs_lang");
        $kennexaApi = new kennexa_api($data,  kennexa_api::delete);
        $kennexaApi->delete($id);
        $this->_redirect("/view-jobs");
    }
    
    public function duplicateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_params['id'];
        
         $modelJobs = new models_Jobs;
         $job = $modelJobs->getJobById($id);

         unset($job['id']);
            
             

//        $job['skills'] = $modelJobs->getJobSkills($id);
        $job['cities'] = $modelJobs->__getJobCities($id);
//        $job['departments'] = $modelJobs->getJobDepartments($id);
//        $job['industries'] = $modelJobs->getJobIndustries($id);
//        $job['id_lang'] = $modelJobs->getJobLanguages($id);


            $data['skills'] = [];
            foreach ($job['skills'] as $i => $v)
            {
                $data['skills'][] = $v['id'];
            }

            $data['cities'] = [] ;
            foreach ($job['cities'] as $i => $v)
            {
                $data['cities'][] = $v['id'];
            }
            $data['departments'] = [];
            foreach ($job['departments'] as $i => $v)
            {
                $data['departments'][] = $v['id'];
            }


            $data['sub_departments'] = [];
            foreach ($job['sub_departments'] as $i => $v)
            {
                $data['sub_departments'][] = $v['id'];
            }


            $data['sub_sub_departments'] = [];
            foreach ($job['sub_sub_departments'] as $i => $v)
            {
                $data['sub_sub_departments'][] = $v['id'];
            }


            $data['skills'] = [];
            foreach ($job['skills'] as $i => $v)
            {
                $data['skills'][] = $v['id'];
            }


            $data['projects'] = [];
            foreach ($job['projects'] as $i => $v)
            {
                $data['projects'][] = $v['id'];
            }

             $data['recruiters'] = [];
                    foreach ($job['recruiters'] as $i => $v)
                    {
                        $data['recruiters'][] = $v['id'];
                    }
          $bnty = end(explode('/',$job['bounty_file']));
         $data['title'] = 'duplicate-'.$job['title'];
//         $data['cities'] = $job['cities'];
         $data['type'] = $job['type']['id'];
         $data['career_level'] = $job['career_level']['id'];
         $data['start'] = $job['start'];
         $data['end'] = $job['end'];
//         $data['skills'] = $job['skills'];
         $data['bounty'] = $job['bounty'];
         $data['bounty_requirements'] = $job['bounty_requirements'];
         $data['requirements'] = $job['requirements'];
         $data['description'] = $job['description'];
         $data['role'] = $job['role'];
         $data['job_headcount'] = $job['job_headcount'];
         $data['id_currency'] = $job['id_currency']['id'];
         $data['bounty_payment_type'] = $job['bounty_payment_type']['id'];
         $data['job_ad_lifetime'] = $job['job_ad_lifetime']['id'];
         $data['job_periode'] = $job['job_periode']['id'];
         $data['bounty_title'] = $job['bounty_title'];
         $data['bounty_file'] = $bnty;
         $data['is_promoted'] = $job['is_promoted'] > 0 ? 'true' : 'false';
        $this->_model->duplicateJob($data);
      
         $this->_redirect("/view-jobs");
    }
}