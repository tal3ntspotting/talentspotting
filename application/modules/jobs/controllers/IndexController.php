<?php

class Jobs_IndexController extends Zend_Controller_Action
{

    protected $_model;
    protected $_id_lang;
    protected $_settings;
    protected $_params;
    protected $_flashMessenger;

    public function init()
    {
        $this->_params = $this->getRequest()->getParams();
        $this->view->params = $this->_params;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flash_messages = $this->_helper->flashMessenger->getMessages();

        $this->_model = new models_Jobs;

        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $this->view->lang = APPLICATION_LOCALE;
        $lang = Zend_Registry::get('_LANG_DETAILS');

        $this->translate = Zend_Registry::get("Zend_Translate");
        $this->view->translate = $this->translate;

        $bootstrap = $this->getInvokeArg('bootstrap')->getOptions();
        $this->_settings = $bootstrap['resources']['Settings'];

        $this->view->settings = $this->_settings;

        $session = new Zend_Session_Namespace("redirector");
        //$session->url = $this->getRequest()->getRequestUri();
    }

    public function detailsAction()
    {

        $url = new Zend_Session_Namespace('url');
        $url->url = $this->getRequest()->getRequestUri();

        $check_is_external = $this->_settings['external_platform'];
        $slug = $this->_params['slug'];
        $id = $this->_params['id'];
        $job = $this->_model->getJobById($id,true,true);
        if ($job['status'] != 1) {
            $this->_redirect('/home');
            exit;
        }

//        if($this->_params['track'] && $this->_params['ts'] && $this->_params['source'])
//        {
//            $mJob = new models_Jobs() ;
//            $user_share_link_id = $mJob->getUserBytrack($this->_params['track']);
//
//            if(!Zend_Auth::getInstance()->hasIdentity()
//                || Zend_Auth::getInstance()->getIdentity()->id != $user_share_link_id
//                || md5(Zend_Auth::getInstance()->getIdentity()->id) != $this->_params['track']
//
//            )
//            {
//                $campaign_track = new Zend_Session_Namespace("campaign_track");
//                $campaign_track_data['track'] = $this->_params['track'];
//                $campaign_track_data['ts'] = $this->_params['ts'];
//                $campaign_track_data['source'] = $this->_params['source'];
//                $campaign_track_data['id_job'] = $id;
//                $campaign_track_data['id_view'] = $this->_model->getViewByShareTracK($campaign_track_data['ts'])['id'];
//                $campaign_track->data = $campaign_track_data;
//            }
//        }

        $mContent = new models_Content();
        $this->view->currencies = $mContent->getCurrenciesSelect();
        $user = new models_User;

        $myUser = $user->getUserById(Zend_Auth::getInstance()->getIdentity()->id);
        $uploaded_cv = $user->getUploadedCVs();
        $linkedin_profile = $this->_model->HasLinkedinProfile(Zend_Auth::getInstance()->getIdentity()->id);

        if (empty($uploaded_cv) && $linkedin_profile == "") {
            $this->view->canApply = 0;
        } else {
            $this->view->canApply = 1;
        }

        $error = [] ;
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $email_verification = $user->checkActivatedUser(Zend_Auth::getInstance()->getIdentity()->email);
            $id_user = Zend_Auth::getInstance()->getIdentity()->id ;
//            $positions = $user->getUserPositions($id_user);
//            $educations = $user->getUserEducation($id_user);
//            $skills = $user->getUserSkillsWithRating($id_user);

//            if(empty($positions)) $error[] = "add your work experience";
//            if(empty($educations)) $error[] = "add your latest education";
//            if(empty($skills)) $error[] = "add your main skills";
            if($this->view->canApply  == 0) {
                $error[] = "add your LinkedIn profile or upload your latest CV";
                $error[] = "<a href=\"/profile\" style='color:#FFF' target=\"_blank\">Complete your profile</a>";
            }
            if($email_verification == false &&
                is_null($this->_user->facebook_id) &&
                is_null($this->_user->linkedin_id) &&
                is_null($this->_user->google_id)
            ) {
                $error[] = 'confirm your email to be able to use your Talentspotting account <a class="send-verification-email-class" href="/ajax/index/send-verification?redirect=">(resend confirmation email link)</a>';
            }

        }
        $mCompany = new models_Company ;
        $is_require_restrict = $mCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);

        if($is_require_restrict['signin'] == 0)
        {
            $error = [];
        }

        $this->view->error = $error;

        $this->view->terms = $this->_model->getCompanyTermsAndCondition();
        if ($this->view->terms['terms_company_status'] > 0) {
            if (Zend_Auth::getInstance()->getIdentity()->type == 1 && $this->view->terms['terms_user_status'] == 0) {
                $js = "$(document).ready(function(){"
                    . "$('.job-alert-box-recommend').remove();"
                    . "$('.job-recommend-box-apply-job').remove();"
                    . "$('.term').click();"
                    . "})";
                $this->view->headScript()->appendScript($js);
            }
        }


        $url = new Zend_Session_Namespace('url');
        $url->url = $this->getRequest()->getRequestUri();

        $this->view->current_menu = 'jobs';
        $session = new Zend_Session_Namespace("track");
//        echo md5(Zend_Auth::getInstance()->getIdentity()->id) ; die;
        if (!Zend_Auth::getInstance()->hasIdentity() ||
        md5(Zend_Auth::getInstance()->getIdentity()->id) != $this->_params['track']) {
//             if (!isset($_COOKIE[$id])) {
//                $this->_model->setJobViews($job, WebApp_Tools::getSocialName($this->_params['source'], $this->_settings['job_source']));
//                setcookie($id, $id, time() + 2592000, '/');
//                
//            }
            $this->view->fingerPrint = 1;
        }
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->view->hasApplied = $this->_model->userHasApplied($job['id']);
        }


        if (isset($this->_params['source'])) {
//            $cookies_array = array("id_job" => (int)$id, "source" => $this->_params['source'], "social" => WebApp_Tools::getSocialName($this->_params['source'], $this->_settings['job_source']));
//            setcookie($id, json_encode($cookies_array), time() + 2592000, '/');
//            setcookie('url', $this->getRequest()->getRequestUri(), time() + 2592000, '/');
            $source = new Zend_Session_Namespace('source');
            $source->source = $this->_params['source'];
        }

        if (isset($this->_params['track'])) {

            $session->track = $this->_params['track'];
            $track = $this->_params['track'];
            if (strlen($track) == "32") {
                $this->_model->updateViewShareUser($track, $id);
            }
            if (strlen($track) == '13') {
                $user->setViewedRecommendation($track);
//                $id_campaign = $this->_model->jobIsPartOfCampaign($id)['id'];
                $cookie = $_COOKIE[$id];
                if (!$cookie) {
                    $this->_model->setUniqueView($id, $track);
                    setcookie($id, $track, time() + 2592000, '/');
                }
            }
        }


//        if ($check_is_external) {
//            $this->_helper->viewRenderer->setRender('external/details');
//        } else {
//            if (!Zend_Auth::getInstance()->hasIdentity() && !isset($session->track)) {
//                $this->_redirect("/");
//            }
//            $this->_helper->viewRenderer->setRender('external/details');
//        }

        if (empty($job)) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        $this->view->job_url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        if ($this->_model->_user->id > 0) {
            $this->view->job_url .= "?trk=" . md5($this->_model->_user->id);
        }

        $modelCompany = new models_Company;


//        $this->view->mde = $modelCompany->getTopMde();

//        echo '<pre>';print_r($job);die;
        $job['next_url'] = $this->_model->getNextJobId($job);
        $job['previous_url'] = $this->_model->getPreviousJobId($job);
        $this->view->job = $job;
        $this->view->application = $this->_model->didIApply($id);
        $this->view->ratings = $modelCompany->getCompanyRatings($job['id_company'], 250);
        $this->view->covers = $modelCompany->getCompanyCovers($job['id_company']);
        $this->view->allow_external = $modelCompany->getCompaniesAllowExternal(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_external'];
        $this->view->allow_employee_apply = $modelCompany->getCompaniesEmployeeApply(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_employee_apply'];
        $this->view->allow_candidate_recommend = $modelCompany->getCompaniesCandidateRecommend(Zend_Auth::getInstance()->getIdentity()->id_company)['allow_candidate_recommend'];
        if ($this->view->allow_candidate_recommend == "") {
            $this->view->allow_candidate_recommend = $modelCompany->getCompanyByJobId($id)['allow_candidate_recommend'];
        }
        $seo_title = $job['company_title'] . " : " . $job['title'];
//        $this->view->headTitle()->set($seo_title);
//        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(3, 2, array("job" => $job)));
//        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(3, 1, array("job" => $job)));
//        $this->view->imageAlt = WebApp_Tools::getImageAlts(4);
//
//        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setName("og:title", $seo_title);
//        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(3, 2, array("job" => $job)));
//        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(3, 2, array("job" => $job)));
//
//        $this->view->headMeta()->setName("twitter:title", $seo_title);
//        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(3, 5, array("job" => $job)));
//        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
        if ($job['job_public_image']) {
            $this->view->seo_image = APPLICATION_URL .  $job['job_public_image'];
        } else {
            $this->view->seo_image = APPLICATION_URL . "/resources/files/companies/original/" . $job['company_logo'];
        }

        $this->view->job_menu = $this->_model->getJobMenu();

        $this->view->company_title = $job['company_title'];
        $this->view->job_url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        if ($this->_model->_user->id > 0) {
            $this->view->job_url .= "?track=" . md5($this->_model->_user->id);
        }
        $this->view->job_facebook_link = WebApp_Tools::createShareFacbook($this->view->job_url, "facebook");
        $this->view->job_facebook_title = $job["title"];
        $this->view->job_facebook_description = preg_replace("/\r|\n/", "", trim(str_replace("-", "", strip_tags(html_entity_decode($job["description"])))));
        if ($job["job_public_image"]) {
            $this->view->job_company_logo = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . "/resources/files/images/" . $job["job_public_image"];
        } else {
            $this->view->job_company_logo = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . "/resources/files/companies/original/" . $job["company_logo"];
        }
        $this->view->job_caption = $job["company_title"];
//        $this->view->social = $this->_model->getSocial();

    }

    public function applyAction()
    {

        Zend_Layout::getMvcInstance()->disableLayout();
        $mUser = new models_User ;
        $slug = $this->_params['slug'];
        $id = $this->_params['id'];
        if ($id > 0) {
            $job = $this->_model->getJobById($id);
        } else {
            $job = $this->_model->getJobBySlug($slug);
        }
        $company = new models_Company();
        $company_settings = $company->getCompanyById($job['id_company']);

        $error = [] ;
        if(Zend_Auth::getInstance()->hasIdentity())
        {
            $uploaded_cv = $mUser->getUploadedCVs();
            $linkedin_profile = $this->_model->HasLinkedinProfile(Zend_Auth::getInstance()->getIdentity()->id);

            if (empty($uploaded_cv) && $linkedin_profile == "") {
                $this->view->canApply = 0;
            } else {
                $this->view->canApply = 1;
            }

            $email_verification = $mUser->checkActivatedUser(Zend_Auth::getInstance()->getIdentity()->email);
            if($this->view->canApply  == 0) {
                $error[] = "add your LinkedIn profile or upload your latest CV";
                $error[] = "<a href=\"/profile\" style='color:#FFF' target=\"_blank\">Complete your profile</a>";
            }
            if($email_verification == false &&
                is_null($this->_user->facebook_id) &&
                is_null($this->_user->linkedin_id) &&
                is_null($this->_user->google_id)
            ) {
                $error[] = 'confirm your email to be able to use your Talentspotting account <a class="send-verification-email-class" href="/ajax/index/send-verification?redirect=">(resend confirmation email link)</a>';
            }

        }

        $mCompany = new models_Company ;
        $is_require_restrict = $mCompany->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);

        if($is_require_restrict['signin'] == 0)
        {
            $error = [];
        }


        if(!empty($error))
        {
            $this->view->redir = "/profile";
            $this->view->error = $error;
           return 0 ;
        }

        if ($this->_model->_user->id > 0 && !$this->getRequest()->isPost()) {
            $positions = $this->_model->getUserPositions();
            if ($this->_model->userHasApplied($job['id'])) {
                $this->view->msg = sprintf($this->translate->_("apply_already"), $job['title']);
                if (!isset($this->_params['noredir'])) {
                    $this->view->redir = "/jobs/{$job['id']}/{$job['slug']}/{$job['company_slug']}";
                }
            }
//            else if (empty($positions) &&
//                $company_settings['completed_profile']) {
//                $this->view->msg = $this->translate->_("apply_no_positions");
//                if (!isset($this->_params['noredir'])) {
//
//                    $js = "$(document).ready(function(){"
//                        . "$(document).on('click',function() {"
//                        . "window.location.href = '/profile';"
//                        . "})"
//                        . "})";
//                    $this->view->js = $js;
//
//
////                    $this->view->redir = "/profile?id_job={$job['id']}";
//                }
//            }
            else {
                $id_application = $this->_model->apply((array)$this->_model->_user, $job);
                $kennexa = new kennexa_api($this->_model->_user, kennexa_api::create);
                $kennexa->jobApplication($id_application);
                $this->view->msg = sprintf($this->translate->_("apply_thankyou"), $job['title']);
                if (!isset($this->_params['noredir'])) {
                    $this->view->redir = "/jobs/{$job['id']}/{$job['slug']}/{$job['company_slug']}";
            }
            }
        }

        if ($this->getRequest()->isPost()) {
            if (count($_POST) == 2) {
                if ($user = $this->_model->authenticate($this->_params['email'], $this->_params['password'])) {
                    $positions = $this->_model->getUserPositions();
                    if ($this->_model->userHasApplied($job['id'], $user['id'])) {
                        $text = sprintf($this->translate->_("apply_already"), $job['title']);
                        $message = "error";
                    } else if (empty($positions)) {
                        $text = $this->translate->_("apply_no_positions");
                        $message = "error";
                    } else {
                        $id_application = $this->_model->apply((array)$this->_model->_user, $job);
                        $text = sprintf($this->translate->_("apply_thankyou"), $job['title']);
                        $message = "ok";
                    }

                    $msg = array("msg" => $message, "error" => $text);
                } else {
                    $msg = array("msg" => "error", "error" => $this->translate->_("login-error"));
                }
            } else if (count($_POST) == 5) {
                $userData['phone'] = $this->_params['phone'];
                $userData['title'] = $this->_params['title'];
                $userData['id'] = $this->_model->_user->id;
                $this->_model->updateUser($userData);


                $positionData['title'] = $this->_params['title'];
                $positionData['company'] = $this->_params['company'];
                $positionData['summary'] = $this->_params['summary'];
                $positionData['start'] = date("Y-m-d", strtotime($this->_params['start']));
                $positionData['is_current'] = 1;


                $this->_model->setUserPosition($positionData, $this->_model->_user->id);
                $this->_model->apply((array)$this->_model->_user, $job);
                $msg = array("msg" => "ok");
            } else {
                if (!$user = $this->_model->getUserByEmail($_POST['email'])) {
                    $_POST['type'] = 1;
                    $_POST['status'] = 1;
                    $_POST['created'] = date("Y-m-d H:i:s");

                    $session = new Zend_Session_Namespace("track");
                    $track = $session->track;
                    if ($track != "") {
                        $tracking = $this->_model->getTrackDetails($track);
                        $_POST['id_company'] = $tracking['id_company'];
                    }

                    if ($user = $this->_model->saveUser($_POST)) {
                        //$this->_model->apply($user, $job);
                        $msg = array("msg" => "ok");
                    } else {
                        $msg = array("msg" => "error", "error" => $this->translate->_("account-exists"));
                    }
                } else {
                    $msg = array("msg" => "error", "error" => $this->translate->_("account-exists"));
                }
            }

            echo json_encode($msg);
            exit();
        }

        $this->view->job = $job;
    }

    public function shareAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $slug = $this->_params['slug'];
        $job = $this->_model->getJobBySlug($slug);

        $this->view->job_url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        if ($this->_model->_user->id > 0) {
            $this->view->job_url .= "?trk=" . md5($this->_model->_user->id);
        }
        $this->view->job = $job;
    }

    public function linkedinAction()
    {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $slug = $this->_params['slug'];

        $options = array(
            'localUrl' => APPLICATION_URL . '/apply-linkedin/' . $slug,
            'callbackUrl' => APPLICATION_URL . '/apply-linkedin/' . $slug,
            'requestTokenUrl' => 'https://api.linkedin.com/uas/oauth/requestToken',
            'userAuthorizationUrl' => 'https://api.linkedin.com/uas/oauth/authorize',
            'accessTokenUrl' => 'https://api.linkedin.com/uas/oauth/accessToken',
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $consumer = new Zend_Oauth_Consumer($options);

        if (empty($ns->request_token)) {
            $token = $consumer->getRequestToken();
            $ns->request_token = serialize($token);
            $ns->token = $token;
            $consumer->redirect();
            die();
        } else {
            try {
                $token = $consumer->getAccessToken($_GET, unserialize($ns->request_token));
                $ns->token = $token;
                //$this->_model->update(array("linkedin_token" => serialize($token)), "id = {$this->_model->_user->id}", "users");
            } catch (Exception $e) {
                //$this->_model->update(array("linkedin_token" => ""), "id = {$this->_model->_user->id}", "users");
                $ns->request_token = '';
                $ns->token = '';
            }
        }

        return $this->_redirect('/apply-linkedin-details/' . $slug);
    }

    public function linkedinDetailsAction()
    {
        $ns = new Zend_Session_Namespace('linkedin_oauth');
        $slug = $this->_params['slug'];

        $job = $this->_model->getJobBySlug($slug);

        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );

        $token = $ns->token;

        $client = $token->getHttpClient($options);

        $client->setUri('https://api.linkedin.com/v1/people/~:(id,firstName,lastName,industry,location,headline,current-status,positions,educations,skills,certifications,pictureUrl,email-address)');
        $client->setMethod(Zend_Http_Client::GET);
        $client->setHeaders("x-li-format", "json");
        $response = $client->request();
        $content = $response->getBody();
        $data = Zend_Json::decode($content);

        if (!$user = $this->_model->getUserDetailsByEmail($data['emailAddress'])) {
            $user_data['firstname'] = $data['firstName'];
            $user_data['lastname'] = $data['lastName'];
            $user_data['title'] = $data['headline'];
            $user_data['firstname'] = $data['firstName'];
            $user_data['location'] = $data['location']['name'];
            $user_data['image'] = $data['pictureUrl'];
            $user_data['linkedin_token'] = serialize($token);
            $user_data['linkedin_id'] = $data['id'];
            $user_data['status'] = 1;
            $user_data['type'] = 1;
            $user_data['email'] = $data['emailAddress'];
            $user_data['password'] = uniqid();
            $user = $this->_model->saveFromLinkedin($user_data);

            $this->_model->setUserCertifications($data['certifications'], $user['id']);
            $this->_model->setUserEducations($data['educations'], $user['id']);
            $this->_model->setUserPositions($data['positions'], $user['id']);
            $this->_model->setUserSkills($data['skills'], $user['id']);

            $this->_model->setUserDefaultAlerts($user, $data['skills']);

            /*
              $client->setUri('https://api.linkedin.com/v1/people/~/connections');
              $client->setMethod(Zend_Http_Client::GET);
              $client->setHeaders("x-li-format", "json");
              $response = $client->request();
              $content = $response->getBody();
              $data = Zend_Json::decode($content);

              $this->_model->setUserConnections($data, $user['id']);
             */
        } else {
            $this->_model->authenticateLinkedin($data['emailAddress']);
        }
        $this->_model->apply($user, $job);

        $ns->request_token = null;
        $ns->token = null;

        $this->_redirect("/profile");
    }

    private function _getLinkedinProfile($url)
    {
        $options = array(
            'consumerKey' => $this->_settings['linkedin']['api_key'],
            'consumerSecret' => $this->_settings['linkedin']['secret'],
        );
        $user = $this->_model->getUserById($this->_model->_user->id);

        $token = unserialize($user['linkedin_token']);
        $client = $token->getHttpClient($options);

        $client->setUri($url);
        $client->setMethod(Zend_Http_Client::GET);
        $response = $client->request();
        $content = $response->getBody();

        $data = json_decode($content);

        echo "<pre>";
        print_R($content);
        die();
    }

    public function jobsAction()
    {
        $this->_redirect('/home');
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');

        $modelContent = new models_Content;
        $companyModel = new models_Company;
        $filters = $this->_getSearchFilters();
        $this->view->news = $modelContent->getLatestNews(2);
        $this->view->quiz = $modelContent->getLatestQuiz();
        $this->view->current_menu = 'jobs';

        $filters['is_active'] = 1;
        $this->view->countries = $modelContent->getCountriesSelect();
        $this->view->cities = $modelContent->getCitiesSelect();
        $this->view->domains = $modelContent->getDepartmentsSelect();
        $this->view->countries = $modelContent->getCountriesSelect();
        $this->view->industries = $modelContent->getIndustriesSelect();
        $this->view->skills = $modelContent->getSkillsSelect();
        $jobs_limit = 15;
        $this->view->jobs = $this->_model->getJobs($filters, $jobs_limit);
        if (array_key_exists("id_company", $filters) && $filters['id_company'][0] > 0) {
            $this->view->companyDetails = $companyModel->getCompanyById($filters['id_company'][0]);
            $this->view->companyFilter = true;
        }

        $this->view->filters = $filters;

        $this->view->headTitle()->set(WebApp_Tools::getSeoTitles(2, 3, array("search" => $filters)));
        $this->view->headMeta()->setName("description", WebApp_Tools::getSeoTitles(2, 2, array("search" => $filters)));
        $this->view->headMeta()->setName("keywords", WebApp_Tools::getSeoTitles(1, 2, array("search" => $filters)));
        $this->view->imageAlt = WebApp_Tools::getImageAlts(3);

        $this->view->doctype('XHTML1_RDFA');
        $this->view->headMeta()->setProperty("og:title", WebApp_Tools::getSeoTitles(2, 3, array("search" => $filters)));
        $this->view->headMeta()->setProperty("og:description", WebApp_Tools::getSeoTitles(2, 2, array("search" => $filters)));
        $this->view->headMeta()->setProperty("og:type", "Job Platform");
        $this->view->headMeta()->setName("twitter:description", WebApp_Tools::getSeoTitles(2, 2, array("search" => $filters)));
        $this->view->headMeta()->setName("twitter:title", WebApp_Tools::getSeoTitles(2, 3, array("search" => $filters)));
        $this->view->headMeta()->setName("twitter:card", WebApp_Tools::getSeoTitles(2, 5, array("search" => $filters)));
        $this->view->headMeta()->setName("twitter:domain", "Job Platform");
    }

    private function _getSearchFilters()
    {
        $modelContent = new models_Content;
        $modelCompany = new models_Company;
        $cities = $modelContent->getCitiesSlugSelect();
        $domains = $modelContent->getDepartmentsSlugSelect();
        $skills = $modelContent->getSkillsSlugSelect();
        $industries = $modelContent->getIndustriesSlugSelect();
        $country = $modelContent->getCountriesSlugSelect();
        $company = $modelCompany->getCompaniesSlugSelect();

        $filters = explode("|", $this->_params['filters']);

        foreach ($filters as $key => $value) {
            $filter = explode("~", $value);
            if (count($filter) == 2) {
                $method = $filter[0];
                if (in_array($filter[0], $this->_settings['filters'])) {
                    $values = explode("_", $filter[1]);

                    if (count($values) > 1) {
                        foreach ($values as $val) {
                            if (in_array($val, $$method)) {
                                $params[array_search($filter[0], $this->_settings['filters'])][] = array_search($val, $$method);
                            }
                        }
                    } else {
                        if (is_array($$method)) {
                            $params[array_search($filter[0], $this->_settings['filters'])][] = array_search($values[0], $$method);
                        } else {
                            $params[array_search($filter[0], $this->_settings['filters'])][] = $values[0];
                        }
                    }
                }
            }
        }

        return (array)$params;
    }

    public function embedFrontAction()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('embed');

        if (isset($this->_params['id_country']))
            $filters['id_country'] = $this->_params['id_country'];

        if (isset($this->_params['id_city']))
            $filters['id_city'] = $this->_params['id_city'];

        if (isset($this->_params['id_skill']))
            $filters['id_skill'] = $this->_params['id_skill'];

        if (isset($this->_params['id_department']))
            $filters['id_department'] = $this->_params['id_department'];

        if (isset($this->_params['id_industry']))
            $filters['id_industry'] = $this->_params['id_industry'];

        if (isset($this->_params['id_company']))
            $filters['id_company'] = $this->_params['id_company'];

        $this->view->jobs = $this->_model->getJobs($filters);
    }

    public function recommendAction()
    {

        Zend_Layout::getMvcInstance()->disableLayout();
        $slug = $this->_params['slug'];
        $id = $this->_params['id'];

        $modelUser = new models_User;

        if ($id > 0) {
            $job = $this->_model->getJobById($id);
        } else {
            $job = $this->_model->getJobBySlug($slug);
        }

        $this->view->company = $this->_model->getCompanyById(Zend_Auth::getInstance()->getIdentity()->id_company);


        $this->view->job_url = APPLICATION_URL . "/jobs/" . $job['id'] . "/" . $job['slug'] . "/" . $job['company_slug'];
        $this->view->id_job = $job['id'];

        if ($this->getRequest()->isPost()) {
            if (count($_POST) == 3) {
                if ($user = $this->_model->authenticate($this->_params['email'], $this->_params['password'])) {
                    $msg = array("msg" => "ok", "url" => $this->view->job_url . "?r");
                } else {
                    $msg = array("msg" => "error", "text" => $this->translate->_("no_account_or_invalid"));
                }
            } else if (!$user = $this->_model->getUserByEmail($_POST['email'])) {
                $_POST['type'] = 1;
                $_POST['status'] = 1;
                $_POST['created'] = date("Y-m-d H:i:s");
                $_POST['password'] = uniqid();
                unset($_POST['id']);

                $modelUser = new models_User;
                if ($user = $modelUser->saveUser($_POST, true)) {
                    $modelUser->authenticate($_POST['email'], $_POST['password']);
                    $msg = array("msg" => "ok", "url" => $this->view->job_url . "?r");
                } else {
                    $msg = array("msg" => "error", "text" => $this->translate->_("account-exists"));
                }
            } else {
                $msg = array("msg" => "error", "text" => $this->translate->_("account-exists"));
            }
            echo json_encode($msg);
            exit();
        }

        $this->view->users = $this->_model->getMyRecommendation();
        $this->view->linkedinConnections = $modelUser->getMyLinkedinConnections();

        if ($this->_model->_user->id > 0) {
            $this->view->job_url .= "?track=" . md5($this->_model->_user->id);
        }


        $this->view->job = $job;
        if ($this->_model->_user->id > 0)
//            $this->view->connections = $modelUser->getLinkedinConnections($this->_model->_user->id, array("job_title" => $job['title'], "industry" => $job['industries']));
            $this->view->company_title = $job['company_title'];
        $this->view->job_facebook_link = WebApp_Tools::createShareFacbook($this->view->job_url, "facebook");
        $this->view->job_facebook_title = $job["title"];
        $this->view->job_facebook_description = preg_replace("/\r|\n/", "", trim(str_replace("-", "", strip_tags(html_entity_decode($job["description"])))));
        if ($job["job_public_image"]) {
            $this->view->job_company_logo = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . "/resources/files/images/" . $job["job_public_image"];
        } else {
            $this->view->job_company_logo = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . "/resources/files/companies/original/" . $job["company_logo"];
        }
        $this->view->job_caption = $job["company_title"];
        $this->view->social = $this->_model->getSocial();
    }

    public function requestAction()
    {
        $id = $this->_params['id'];
        $application = $this->_model->getJobApplicationById($id);
        if (!empty($application)) {
            $job = $this->_model->getJobById($application['id_job']);
            $company = $this->_model->getCompanyById($job['id_company']);

            $this->_model->update(array("pending" => 1, "status" => 4, "status_reason" => 0), "id = {$id}", "job_applications");
            $data["id_company"] = $company['id'];
            $data["target"] = "company";
            $data["type"] = "notification";
            $data["message"] = "Job interview request from {$this->_model->_user->firstname} {$this->_model->_user->lastname}";
            $data["url"] = "/job-details/?id={$job['id']}";
            $data["date"] = date("Y-m-d H:i:s");

            $this->_model->insert($data, "notifications");
        }

        $this->_redirect("/profile?h");
    }

    public function cshareAction()
    {
        /*
         *  Count Shares 
         */

        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $id_user = (int)$this->_params['id_user'];
        $id_job = (int)$this->_params['id_job'];
        $url = str_replace("?","&",$this->_params['url'])."&" ;
        preg_match('~\&ts=(.*?)&~', $url, $output);
        $hash =  $output[1];
        if($hash == "")
        {
            $hash = $this->_params['ts'];
        }
        preg_match('~\&source=(.*?)&~', $url, $output);
        $source  =  $output[1];
        if($source == "")
        {
            $source = $this->_params['source'];
        }
        if (Zend_Auth::getInstance()->getIdentity()->id == $id_user && is_int($id_job)) {
            $this->_model->cshare($id_user, $id_job,$hash,$source);
        }

        $jobIsPartOfCampain = $this->_model->jobIsPartOfCampaign($id_job);
        $id_view = 0;
        $action = "shares";
//        $this->_model->setGamificationActionPoints($action, $id_job, $id_user);
        if($jobIsPartOfCampain)
        {
            $id_campaign = $jobIsPartOfCampain['id'];
            $campaign_actions = $this->_model->getGamificationActionsByCampaignId($id_campaign);
            if (!empty($campaign_actions))
            {
                // 3 - Check  the id of $actions
                $result = $this->_model->getActionIDbyName($action);
                $action_data = $this->_model->getActionDataByCampainIdActionId($result['id'],$id_campaign);
                if(empty($action_data)) return ;
                if($action_data['active']) {
                    if($this->_model->checkValidation($action_data,$hash))
                    {
                        $data['id_user'] = $id_user;
                        $data['value'] = $action_data['points'];
                        $data['id_campaign'] = $action_data['id_campaign'];
                        $data['id_job'] = $id_job;
                        $data['id_action'] = $action_data['id'];
                        $data['id_job_view'] = $id_view  ;
                        $modeStats = new models_Stats;
                        $modeStats->saveCampaignReport($data);
                    }
                } else {
                    $data['id_user'] = $id_user;
                    $data['value'] = $action_data['points'];
                    $data['id_campaign'] = $action_data['id_campaign'];
                    $data['id_job'] = $id_job;
                    $data['id_action'] = $action_data['id'];
                    $data['id_job_view'] = $id_view  ;
                    $modeStats = new models_Stats;
                    $modeStats->saveCampaignReport($data);
                }
            }

        }



        die();
    }

    public function termAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('admin');
        $agree = 0 ;
        if ($this->getRequest()->isPost()) {
            $agree = $this->_params['agree'];
            $link = $this->_params['link'];
            if ($agree > 0) {
                $this->_model->userAgreeTerms();
                $stats = new models_Stats;
                $action = (string)'activate_account';
                $id_user = (int)Zend_Auth::getInstance()->getIdentity()->id;
                $stats->setGamificationPoint($id_user, $action);
                $stats->setActivateAccountPoint($id_user, $action);
//                $url = new Zend_Session_Namespace('url');

//                $this->_redirect(base64_decode($link));
                echo '<meta http-equiv="refresh" content="0; url='.base64_decode($link).'" />';
//                echo json_encode(['url' => base64_decode($link)]);
                exit;

            }
        }
        $id_company = Zend_Auth::getInstance()->getIdentity()->id_company;
        $this->view->content = htmlspecialchars_decode($this->_model->getTermsContent($id_company));
    }

    public function fingerAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            $fingerprint = $this->_params['fingerprint'];
            $url = $this->_params['url'] ;
            $url = str_replace("?","&",$url)."&" ;

            preg_match('~\&track=(.*?)&~', $url, $output);
            $track =  $output[1];

            preg_match('~\&source=(.*?)&~', $url, $output);
            $source  =  $output[1];

            preg_match('~\&ts=(.*?)&~', $url, $output);
            $hash  =  $output[1];

            preg_match('~\&id_job=(.*?)&~', $url, $output);
            $id_job  =  $this->_params['id_job'];

            $id_user = $this->_model->getUserBytrack($track);
            // check if we already have this source id
            $exist = $this->_model->socialMediaSourceExist($source);
            $id_jobs_share = $this->_model->getJobsShareByHashSource($hash,$source)['id'];

            if (!$exist) exit;


            $jobIsPartOfCampain = $this->_model->jobIsPartOfCampaign($id_job);
            $id_jobs_share = ($id_jobs_share > 0)  ? $id_jobs_share : 0 ;
            $id_view = $this->_model->setJobUniqueView($id_user, $source, $fingerprint, $id_job,$id_jobs_share);

            $campaign_track = new Zend_Session_Namespace("campaign_track");
            $campaign_track_data['track'] = $track;
            $campaign_track_data['ts'] = $hash;
            $campaign_track_data['source'] = $source;
            $campaign_track_data['id_job'] = $id_job;
            $campaign_track_data['id_view'] = $id_view;
            $campaign_track->data = $campaign_track_data;

            $action = "views";
            $this->_model->setGamificationActionPoints($action, $id_job, $id_user);
            if($jobIsPartOfCampain)
            {
                $id_campaign = $jobIsPartOfCampain['id'];
                $campaign_actions = $this->_model->getGamificationActionsByCampaignId($id_campaign);
                if (!empty($campaign_actions))
                {
                    // 3 - Check  the id of $actions
                    $result = $this->_model->getActionIDbyName($action);
                    $action_data = $this->_model->getActionDataByCampainIdActionId($result['id'],$id_campaign);
                    if($action_data['active']) {
                        if($this->_model->checkValidation($action_data,$hash))
                        {
                            $data['id_user'] = $id_user;
                            $data['value'] = $action_data['points'];
                            $data['id_campaign'] = $action_data['id_campaign'];
                            $data['id_job'] = $id_job;
                            $data['id_action'] = $action_data['id'];
                            $data['id_job_view'] = $id_view  ;
                            $modeStats = new models_Stats;
                            $modeStats->saveCampaignReport($data);
                        }
                    } else {
                        $data['id_user'] = $id_user;
                        $data['value'] = $action_data['points'];
                        $data['id_campaign'] = $action_data['id_campaign'];
                        $data['id_job'] = $id_job;
                        $data['id_action'] = $action_data['id'];
                        $data['id_job_view'] = $id_view  ;
                        $modeStats = new models_Stats;
                        $modeStats->saveCampaignReport($data);
                    }

//
//
//                    echo '<pre>';
//                    print_r($action_data);
//                    echo $id_campaign;
//                    die;
                }

            }

//            die;
//
//
//                // Set View For the Campain job if has view gamification poinct
//
//
//                if ($jobIsPartOfCampain) {
//                    $id_campaign = $jobIsPartOfCampain['id'];
//                    // 2 - get gamification Action for the campaign
//                    $campaign_actions = $this->_model->getGamificationActionsByCampaignId($id_campaign);
//                    if (!empty($campaign_actions)) {
//                        $action = "views";
//                        // 3 - Check  the id of $actions
//                        $result = $this->_model->getActionIDbyName($action);
//
//                        if (!empty($result)) {
//                            $exist = array_intersect($campaign_actions, $result);
//                            if (!empty($exist)) {
//                                $jobViewedBefore = $this->_model->getThisJobView($id_job, $fingerprint, $id_user);
//                                if (empty($jobViewedBefore)) {
//                                    $modeStats = new models_Stats;
//                                    $modeStats->setCampaignReport($id_user, $action, $id_job, $id_user);
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    $action = "views";
//                    $jobViewedBefore = $this->_model->getThisJobView($id_job, $fingerprint, $id_user);
//                    if (empty($jobViewedBefore)) {
//                        $this->_model->setGamificationActionPoints($action, $id_job, $id_user);
//                    }
//                }

        }
    }

    public function fbGraphAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        echo 123;
        die;

        $config = array();
        $config['appId'] = '1062377973856382';
        $config['secret'] = '8f8718a3c26a44fb4c83e38a3ff4c099';
        $config['fileUpload'] = false; // optional

        $fb = new Facebook($config);

// define your POST parameters (replace with your own values)
        $params = array(
            "access_token" => "EAACEdEose0cBALs30xDqDzcgrdyQSzh2OLCeZClIl9iuarecKnRyDkdUrOHCPB572WqnNkj5zbfH1WYvxwlPx1QV75uSTO8MvH0QeZCrdi2C1yluC97q5JC1IlAhUr9tlDIfBQk91yumCA8FbsBGxgF8kYZAk8DXxahrhq1xsRk2RVKjqMwspoM6ZAJX0H5BZBSDxQviGQgZDZD",
            "message" => "#php-shedule #facebook",
            "link" => "http://talentspotting.com",
            "picture" => "http://i.imgur.com/sdsdsd.png",
            "name" => "test",
            "caption" => "test caption",
            "description" => "auto fb post."
        );

// post to Facebook
// see: https://developers.facebook.com/docs/reference/php/facebook-api/
        try {
            $ret = $fb->api('/me/feed', 'POST', $params);
            echo 'Successfully posted to Facebook';
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        echo '<pre>';
        echo json_encode($_REQUEST);
        die;
    }

    public function withdrawApplicationAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id_application = (int) $this->_params['id'];
        if($this->_model->checkOwnDeleteApplicatinByAppId($id_application))
            $this->_model
            ->withdrawApplicationById($id_application);
        $this->_redirect('/profile#/application');
    }

}
