<?php
class plugins_Layout extends Zend_Controller_Plugin_Abstract
{
    
        protected $_request ;
	const ROUTE_SEPARATOR = '/';
	
	/**
	 * @var Zend_Config_Ini
	 */
	protected $_config;
	
	/**
	 * @var Zend_Config
	 */
	protected $_layout;
	
	/**
	 * @var array
	 */
	protected $_adminControllersName = array('admin','stats');
	public function __construct() {
	}

    public function dispatchLoopStartup( Zend_Controller_Request_Abstract $request ) {
    	$layout_suffix = Zend_Registry::get('LAYOUT_SUFFIX');

        if(isset($layout_suffix) && strlen($layout_suffix) != 0 && strlen($_SERVER['REQUEST_URI']) != 1 && $layout_suffix == '_jurnalul')  {
            $view_suffix = substr($layout_suffix,1);

            Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer')->setViewSuffix("$view_suffix.phtml");
        }
    	if ( $layout_suffix == '') {
    			$this->_config = new Zend_Config_Ini('configs/layouts.ini', 'layouts');
    	} else {

    			$this->_config = new Zend_Config_Ini('configs/layouts.ini', $layout_suffix);
    	}

    	// Set layout
    	$this->setLayout( $request );

    	// Set meta
    	$this->setMeta();

    	// Set styles
    	$this->setStyles();

    	// Set scripts
    	$this->setScripts();
	}

	/**
	 * Choose the corect layout
	 * @param Zend_Controller_Request_Abstract $request
	 */
	protected function setLayout( Zend_Controller_Request_Abstract $request ) {
                $this->_request = $request ;
		$module = $request->getModuleName();
		$controller = $request->getControllerName();
		$action = $request->getActionName();

		$module = ($module) ? $module : 'admin';

		if ( $this->isAdminArea($request) ) {
			$this->_layout = $this->_config->get('admin');
		} else {
			if ( $this->_config->get($module . self::ROUTE_SEPARATOR . $controller . self::ROUTE_SEPARATOR . $action) ) {
				$this->_layout = $this->_config->get($module . self::ROUTE_SEPARATOR . $controller . self::ROUTE_SEPARATOR . $action);
			}
			else if ( $this->_config->get($module . self::ROUTE_SEPARATOR . $controller) ) {
				$this->_layout = $this->_config->get($module . self::ROUTE_SEPARATOR . $controller);
			}
			else if ( $this->_config->get($module) ) {
				$this->_layout = $this->_config->get($module);
			}
			else {
				$this->_layout = $this->_config->get('admin');
			}
		}

		$this->_layout = $this->makeInheritance( new Zend_Config($this->_layout->toArray(), true) );

		if ( $this->_layout->name == null || $this->_layout->name instanceof Zend_Config )
			throw new Zend_Exception('No layout found !');

		Zend_Layout::getMvcInstance()->setLayout( $this->_layout->get('name') );
	}

	/**
	 * Check if current request is a admin area
	 * @param Zend_Controller_Request_Abstract $request
	 * @return boolean
	 */
	protected function isAdminArea(Zend_Controller_Request_Abstract $request) {
	   return false;
		$controller = $request->getControllerName();

		if ( in_array($controller, $this->_adminControllersName) )
			return true;

		foreach ( $this->_adminControllersName as $adminName )
			if ( strpos($controller, $adminName) === 0 )
				return true;

		return false;
	}

	/**
	 * Set meta
	 */
	protected function setMeta() {
		$view = Zend_Layout::getMvcInstance()->getView();
        
        if (is_object($this->_layout->page))
        {
            if ( $this->_layout->page->title )
            {
                $view->headTitle( $this->_layout->page->title );   
            }			     
        }
		if (is_object($this->_layout->page))
        {
            if ( $this->_layout->page->docType )
            {
                $view->doctype( $this->_layout->page->docType );    
            }
        }
			

		if ( $this->_layout->meta ) {
			foreach ($this->_layout->meta as $type => $value) {
                if ( $type == 'httpEquiv' ) {
					foreach ($this->_layout->meta->httpEquiv as $type => $value)
                		$view->headMeta()->appendHttpEquiv($type, $value);
                 } else {
                 	$view->headMeta()->appendName($type, $value);
                 }
			}
		}
	}

	/**
	 * Set styles
	 */
	protected function setStyles() {
		if ( $this->_layout->styles )
			foreach ( $this->_layout->styles as $style )
				Zend_Layout::getMvcInstance()->getView()->headLink()->appendStylesheet( APPLICATION_URL . $style );
                
                  if ($this->_request->module == 'jobs' && $this->_request->controller == 'index' && $this->_request->action == 'details') {
            Zend_Layout::getMvcInstance()->getView()->headLink()->appendStylesheet(APPLICATION_URL . '/resources/admin/css/style.css');
        }
    }


	/**
	 * Set scripts
	 */
	protected function setScripts() {
		if ( $this->_layout->scripts )
			foreach ( $this->_layout->scripts as $script )
				Zend_Layout::getMvcInstance()->getView()->headScript()->appendFile(APPLICATION_URL . $script);
	}

	/**
	 * Make inheritance for current layout
	 * @param Zend_Config $layout
	 */
	protected function makeInheritance( Zend_Config $layout ) {
		if ( $inheritanceName = $layout->get('inheritance') ) {
			if ( $this->_config->get( $inheritanceName ) == null )
				throw new Zend_Exception('Bad layout inheritance !');

			$inheritance =  $this->_config->get( $inheritanceName );

			foreach ( $inheritance as $key => $val ) {
				switch ( $key ) {
					case 'scripts':
					case 'styles':
						if ( !isset($layout->$key) )
							$layout->$key = new Zend_Config( array(), true );

						$layout->$key = new Zend_Config (array_merge( $val->toArray(), $layout->$key->toArray() ), true);
						break;
					case 'name':
						if ( $layout->$key == null )
							$layout->$key = $val;
						break;
					case 'page':
					case 'meta':
					case 'httpEquiv':
						if ( !isset($layout->$key) )
							$layout->$key = new Zend_Config( array(), true );

						$layout->$key = $layout->$key->merge( $val );
						break;
				}
			}


			if ( $inheritance->get('inheritance') ) {
				$layout->inheritance = $inheritance->get('inheritance');
				$layout = $this->makeInheritance($layout);
			}
		}
		return $layout;
	}
}
