<?php
class plugins_Settings extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request) 
        {
            $action = $request->getActionName();
            $controller = $request->getControllerName();
            $module = $request->getModuleName();
            $uri = APPLICATION_URL. $request->getRequestUri();
        } 
        else 
        {
            $action = '-';
            $controller = '-';
            $module = '-';
            $uri = 'http:///';
        }
        
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        
        if(($_SERVER['HTTP_HOST'] == 'm.auto.ro' || $_SERVER['HTTP_HOST'] == 'm.auto.dev') && Zend_Controller_Front::getInstance()->getRequest()->getModuleName() != 'mobile') 
        {
            $redirector->setGotoRoute(array(), 'mobile-mobile_index');
        }
    }
    
    public function dispatchLoopShutdown()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        
        if (Zend_Registry::isRegistered('disableProfiler')) 
        {
            return ;
        }
        
        if ( Zend_Controller_Front::getInstance()->getRequest()->getModuleName() != 'newsletter' ) 
        {
            if (preg_match("/^192\.168\.0\./", $ip)) 
            {
                $frontController = Zend_Controller_Front::getInstance();
                $responseCode = $frontController->getResponse()->getHttpResponseCode();
                if (200 != $responseCode && 404 != $responseCode && 500 != $responseCode && 503 != $responseCode) 
                {
                    return;
                }
                
                $queries = Zend_Registry::get('dbAdapter')->getProfiler()->getQueryProfiles();
                if (!$queries) 
                {
                    $queries = array();
                }
                
                $view = Zend_Layout::getMvcInstance()->getView();
                $view->queries = $queries;
                $response = $frontController->getResponse();
                $response->setBody(str_replace('</body>', $view->render('partials/profiler.phtml'). '</body>', $response->getBody()));
            }
        }
    }
}
