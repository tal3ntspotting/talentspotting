<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class plugins_Access extends Zend_Controller_Plugin_Abstract {

    function __construct() {
        
    }
    
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        
        
        
        $response = $this->getResponse();
        $response->setHeader('X-Frame-Options', 'DENY');

       $permissions = Zend_Registry::get('Access');
       $route = $request->module.'/'.$request->controller.'/'.$request->action ; 
       $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
       $settings = $bootstrap['resources']['Settings'];
       if($settings['check_access'] == 1) {
//            if (Zend_Auth::getInstance()->hasIdentity()) {

                $this->_checkSession();
                Zend_Session::regenerateId(true);
                $this->_checkPermissions($route, $permissions, $request);
//            }
        }

       
//       echo '<pre>';
//       print_r($permissions);
//       die;
    }
    
    
    
    public function _checkPermissions($route,$permissions,$request) {
        $allowed_ip = $permissions['allowed_ip'];
        $blocked_pages = $permissions['blocked_pages'];
        if(in_array($route, $blocked_pages)) {
            $access = false ;
            $myip = $this->get_client_ip() ; 
            if (in_array($myip, $allowed_ip)) {
                $access = true ;
            }
        }
        
        if($access === false) {
            $request->setModuleName("default")->setControllerName("error")->setActionName("error");
            $error = new Zend_Controller_Plugin_ErrorHandler();
            $error->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER;
            $error->request = clone($request);
            $error->exception = new Zend_Controller_Action_Exception("You don't have enough permissions to access this page.", 403);
            $request->setParam('error_handler', $error);
        }
//        var_dump($access);die;
        
        
        
    }
    
    public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    
    public function _checkSession() {
        if(Zend_Auth::getInstance()->getIdentity()->last_login_ip != ip2long(getenv("REMOTE_ADDR"))) {
            Zend_Auth::getInstance()->clearIdentity();
        } 
    }
}