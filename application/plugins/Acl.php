<?php

class plugins_Acl extends Zend_Controller_Plugin_Abstract {

    protected $_config;
    protected $_allowed;
    protected $_blocked;
    protected $_params;

    public function __construct() {
        
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $permissions = Zend_Registry::get('APP_PERMISIONS');
        
     

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $settings = $bootstrap['resources']['Settings'];

        if (strpos($_SERVER['REQUEST_URI'], "sectiune") !== false) {
            header("Location: /wiafair");
            exit();
        }

        if ($settings['check_acl'] == 1) {
            $this->_allowed = $permissions['allowed_pages'];
            $this->_blocked = $permissions['blocked_pages'];
            $this->_params = $request->getParams();
            $this->_checkPermisions($request);
        }
        
        $modelJob = new models_Jobs ;
        $terms = $modelJob->getCompanyTermsAndCondition();
                if(Zend_Auth::getInstance()->getIdentity()->type === 9 || Zend_Auth::getInstance()->getIdentity()->type === 1) {
        if ($terms['terms_company_status'] > 0) {
            if ($terms['terms_user_status'] == 0) {
                  $request->setControllerName('index');
                  $request->setActionName('term');
                  $request->setModuleName('jobs');
            }
        }
        }
    }

    protected function _checkPermisions($request) {
        $hasPermision = false;
        $routeExists = false;

        $route = "/" . $this->_params['module'] . "/" . $this->_params['controller'] . "/" . $this->_params['action'];

        $user = Zend_Auth::getInstance()->getIdentity();
        if (in_array($route, $this->_allowed)) {
            $hasPermision = true;
            $routeExists = true;
        } else {
            foreach ($this->_blocked as $role => $roles) {
                if ($role == $user->type) {
                    if (in_array($route, $roles)) {
                        $hasPermision = true;
                    }
                }

                if (in_array($route, $roles)) {
                    $routeExists = true;
                }
            }
        }

        $mContent = new models_Content ;
        $sidebarMenu = $mContent->getUserMenu();
        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $existInSeparatedAcl = $mContent->existAclDb($uri);
        if($existInSeparatedAcl)
        {
            $hasPermision = $mContent->checkDbAcl($uri);
//            $links = [] ;
//            foreach ($sidebarMenu as $key => $item)
//            {
//                $links[] = $item['link'];
//            }
//
//
//            if (!in_array($uri, $links)) {
//                $hasPermision = false;
//            }
        }




        if ($hasPermision == false) {
            if (!Zend_Auth::getInstance()->hasIdentity()) {
                if ($route == "/jobs/payments/demo") {
                    header("Location: /login-company?r=company");
                } else {
                    header("Location: /login");
                }
                exit();
            } else {
                if ($routeExists === false) {
                    $request->setModuleName("default")->setControllerName("error")->setActionName("error");
                    $error = new Zend_Controller_Plugin_ErrorHandler();
                    $error->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE;
                    $error->request = clone($request);
                    $error->exception = new Zend_Controller_Action_Exception("The page you requested is not available", 404);
                    $request->setParam('error_handler', $error);
                } else {
                    $request->setModuleName("default")->setControllerName("error")->setActionName("error");
                    $error = new Zend_Controller_Plugin_ErrorHandler();
                    $error->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_OTHER;
                    $error->request = clone($request);
                    $error->exception = new Zend_Controller_Action_Exception("You don't have enough permissions to access this page.", 403);
                    $request->setParam('error_handler', $error);
                }
            }
        }
    }

}
