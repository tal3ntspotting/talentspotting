<?php

class plugins_DeviceLayout extends Zend_Controller_Plugin_Abstract {

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $req) {

        $useragent = new Zend_Http_UserAgent;
        $useragent->getDevice();
        $constructed_url = implode('/', $req->getParams());

        $layout = Zend_Registry::get('Mobile');
        if ($useragent->getBrowserType() == 'mobile') {
            
            if (in_array($constructed_url, $layout)) {
                Zend_Controller_Action_HelperBroker::getExistingHelper('ViewRenderer')->setViewSuffix('mobile.phtml');
            }
        }
//        $this->headLink()->appendStylesheet($this->baseUrl() . '/css/aaaa.css');
        return $req;
    }

}
