<?php
require_once realpath(dirname(__FILE__).'/../vendor/autoload.php');
require_once realpath(dirname(__FILE__).'/../scripts/cli_config.php');
use Symfony\Component\Yaml\Yaml;
use LSS\XML2Array;
use XmlIterator\XmlIterator;
class populateDb
{
    public $phinx_config ;

    public $envirement ;

    public $config ;

    public $xml;

    public $data ;


    public function Run()
    {
        if(isset($GLOBALS['argv']))
        {
            $args = $GLOBALS['argv'];
        }
        if($args[1] !== "-e") throw new Exception('Please Provide a Flag for command "-e" envirement');
        if($args[2] == "") throw new Exception('Please Provide a eniviremtn to run command');
        $this->phinx_config = realpath(dirname(__FILE__).'/../phinx.yml');
        $this->envirement = $args[2];
        $this->config = Yaml::parse(file_get_contents($this->phinx_config));
        $this->xml = realpath(dirname(__FILE__).'/../migrations/data.xml');
        $this->xmlToArray();
        $this->populate($this->data);

    }

    public function xmlToArray()
    {
        $strContents = file_get_contents($this->xml);
        $strDatas =  XML2Array::createArray($strContents);
        $table_data = $strDatas['mysqldump']['database']['table_data'];
        $data = [];
        foreach($table_data as $key => $value)
        {
            $table_name = $value['@attributes']['name'];
            if(isset($value['row']))
            {
                $data[$key]['table'] = $value['@attributes']['name'];
                $data[$key]['data'] = [] ;
                $rows = $value['row'];
                $d = [];
                foreach($rows as $row)
                {
                    if(!isset($row['field'])) continue ;
                    $field = $row['field'];
                    foreach($field as $k => $value)
                    {
                        $d[$value['@attributes']['name']] = $value['@value'];
                    }
                    $data[$key]['data'][] = $d;
                }
            }

        }
        $this->data = $data;
    }

    public function populate($data)
    {


        $config = $this->config['environments'][$this->envirement];
        $connection = Zend_Db::factory('mysqli', array(
            'host'             => $config['host'],
            'username'         => $config['user'],
            'password'         => $config['pass'],
            'dbname'           => $config['name']
        ));

        foreach($data as $d)
        {
            if(!empty($d['data']))
            {
                foreach($d['data'] as $row)
                {
                    $connection->insert($d['table'],$row);
                }
            }
        }
        $connection->query('ALTER TABLE `custom_cities` AUTO_INCREMENT = 100000;');
        $connection->query('ALTER TABLE `custom_departments` AUTO_INCREMENT = 100000;');
        $connection->query('ALTER TABLE `custom_skills` AUTO_INCREMENT = 100000;');
        $connection->query('ALTER TABLE `custom_location` AUTO_INCREMENT = 100000;');
        $connection->query('ALTER TABLE `custom_user_skills` AUTO_INCREMENT = 100000;');
    }
}

(new populateDb)->Run();
?>