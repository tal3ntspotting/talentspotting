<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Abc extends AbstractMigration
{
    public function change()
    {
        $this->execute("ALTER DATABASE CHARACTER SET 'latin1';");
        $this->execute("ALTER DATABASE COLLATE='latin1_swedish_ci';");
        $table = $this->table("ats_emails_history", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('ats_emails_history')->hasColumn('id')) {
            $this->table("ats_emails_history")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("ats_emails_history")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_sender', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_receiver', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_sender'])->update();
        $table->addColumn('subject', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_receiver'])->update();
        $table->addColumn('content', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'subject'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'content'])->update();
        $table->save();
        $table = $this->table("ats_note", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('ats_note')->hasColumn('id')) {
            $this->table("ats_note")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("ats_note")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('note', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_recommender'])->update();
        $table->addColumn('general', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'note'])->update();
        $table->addColumn('file', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'general'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'file'])->update();
        $table->save();
        $table = $this->table("autopost_job", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('autopost_job')->hasColumn('id')) {
            $this->table("autopost_job")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("autopost_job")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("autopost_linkedin_job", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('autopost_linkedin_job')->hasColumn('id')) {
            $this->table("autopost_linkedin_job")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("autopost_linkedin_job")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("branding_category", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('branding_category')->hasColumn('id')) {
            $this->table("branding_category")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("branding_category")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('description', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("campaign_actions_detailed", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_actions_detailed')->hasColumn('id')) {
            $this->table("campaign_actions_detailed")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_actions_detailed")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('action', 'enum', ['null' => false, 'limit' => 17, 'values' => ['views','new_candidates','new_applicants','successful_hire','completed_profile','extend_network','activate_account'], 'after' => 'id_job'])->update();
        $table->addColumn('value', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'action'])->update();
        $table->addColumn('date', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'value'])->update();
        $table->save();
        if($this->table('campaign_actions_detailed')->hasIndex('action')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('action');
        }
        $this->table("campaign_actions_detailed")->addIndex(['action'], ['name' => "action", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign'], ['name' => "id_campaign", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('value')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('value');
        }
        $this->table("campaign_actions_detailed")->addIndex(['value'], ['name' => "value", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_2')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_2');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_2", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_3')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_3');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_3", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_4')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_4');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_4", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_5')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_5');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_5", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_6')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_6');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_6", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_7')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_7');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_7", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_8')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_8');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_8", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_9')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_9');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_9", 'unique' => false])->save();
        if($this->table('campaign_actions_detailed')->hasIndex('id_campaign_10')) {
            $this->table("campaign_actions_detailed")->removeIndexByName('id_campaign_10');
        }
        $this->table("campaign_actions_detailed")->addIndex(['id_campaign','id_recommender','id_user','id_job','action','value'], ['name' => "id_campaign_10", 'unique' => false])->save();
        $table = $this->table("campaign_bounty", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_bounty')->hasColumn('id')) {
            $this->table("campaign_bounty")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_bounty")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_company'])->update();
        $table->addColumn('status', 'enum', ['null' => false, 'limit' => 9, 'values' => ['initiated','paid','canceled','pending'], 'after' => 'id_user'])->update();
        $table->addColumn('bounty', 'string', ['null' => false, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'status'])->update();
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'bounty'])->update();
        $table->addColumn('end', 'date', ['null' => false, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_cities", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_cities')->hasColumn('id')) {
            $this->table("campaign_cities")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_cities")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_city', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_departments", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_departments')->hasColumn('id')) {
            $this->table("campaign_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_department', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_gamification_actions", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_gamification_actions')->hasColumn('id')) {
            $this->table("campaign_gamification_actions")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_gamification_actions")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_gamification_action', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_jobs", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_jobs')->hasColumn('id')) {
            $this->table("campaign_jobs")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_jobs")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_media", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_media')->hasColumn('id')) {
            $this->table("campaign_media")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_media")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('bounty_file', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_recommenders", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_recommenders')->hasColumn('id')) {
            $this->table("campaign_recommenders")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_recommenders")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->save();
        $table = $this->table("campaign_reports", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_reports')->hasColumn('id')) {
            $this->table("campaign_reports")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_reports")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('views', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->addColumn('shares', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'views'])->update();
        $table->addColumn('applications', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'shares'])->update();
        $table->addColumn('recommendations', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'applications'])->update();
        $table->addColumn('email_refferences', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'recommendations'])->update();
        $table->addColumn('bounties', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'email_refferences'])->update();
        $table->save();
        $table = $this->table("campaign_reports_detailed", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaign_reports_detailed')->hasColumn('id')) {
            $this->table("campaign_reports_detailed")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaign_reports_detailed")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_campaign', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_campaign'])->update();
        $table->addColumn('action', 'enum', ['null' => false, 'default' => 'view', 'limit' => 16, 'values' => ['view','share','application','email_refference','reccomendation','bounty'], 'after' => 'id_user'])->update();
        $table->addColumn('value', 'string', ['null' => false, 'limit' => 50, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'action'])->update();
        $table->addColumn('date', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'value'])->update();
        $table->save();
        if($this->table('campaign_reports_detailed')->hasIndex('action')) {
            $this->table("campaign_reports_detailed")->removeIndexByName('action');
        }
        $this->table("campaign_reports_detailed")->addIndex(['action'], ['name' => "action", 'unique' => false])->save();
        if($this->table('campaign_reports_detailed')->hasIndex('id_campaign')) {
            $this->table("campaign_reports_detailed")->removeIndexByName('id_campaign');
        }
        $this->table("campaign_reports_detailed")->addIndex(['id_campaign'], ['name' => "id_campaign", 'unique' => false])->save();
        if($this->table('campaign_reports_detailed')->hasIndex('value')) {
            $this->table("campaign_reports_detailed")->removeIndexByName('value');
        }
        $this->table("campaign_reports_detailed")->addIndex(['value'], ['name' => "value", 'unique' => false])->save();
        $table = $this->table("campaigns", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('campaigns')->hasColumn('id')) {
            $this->table("campaigns")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("campaigns")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('bounty', 'string', ['null' => false, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_company'])->update();
        $table->addColumn('duration', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'bounty'])->update();
        $table->addColumn('reward', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'duration'])->update();
        $table->addColumn('is_promoted', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'reward'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'is_promoted'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->addColumn('message', 'string', ['null' => false, 'limit' => 500, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'slug'])->update();
        $table->addColumn('description', 'text', ['null' => true, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'message'])->update();
        $table->addColumn('start', 'date', ['null' => false, 'after' => 'description'])->update();
        $table->addColumn('end', 'date', ['null' => false, 'after' => 'start'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'end'])->update();
        $table->save();
        $table = $this->table("conversations", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('conversations')->hasColumn('id')) {
            $this->table("conversations")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("conversations")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('from_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('to_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'from_user'])->update();
        $table->addColumn('message', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'to_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'message'])->update();
        $table->save();
        $table = $this->table("cron_platform_updates", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('cron_platform_updates')->hasColumn('id')) {
            $this->table("cron_platform_updates")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("cron_platform_updates")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'status'])->update();
        $table->save();
        $table = $this->table("cron_tips", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('cron_tips')->hasColumn('id')) {
            $this->table("cron_tips")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("cron_tips")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'status'])->update();
        $table->save();
        $table = $this->table("custom_cities", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_cities')->hasColumn('id')) {
            $this->table("custom_cities")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_cities")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_country', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_country'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_company'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("custom_departments", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_departments')->hasColumn('id')) {
            $this->table("custom_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_company'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("custom_job_cities", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_job_cities')->hasColumn('id')) {
            $this->table("custom_job_cities")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_job_cities")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_city', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_city'])->update();
        $table->save();
        $table = $this->table("custom_job_departments", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_job_departments')->hasColumn('id')) {
            $this->table("custom_job_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_job_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('id_department', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("custom_job_skills", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_job_skills')->hasColumn('id')) {
            $this->table("custom_job_skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_job_skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_skill', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_skill'])->update();
        $table->save();
        $table = $this->table("custom_location", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_location')->hasColumn('id')) {
            $this->table("custom_location")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_location")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 100, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("custom_skills", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_skills')->hasColumn('id')) {
            $this->table("custom_skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_company'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("custom_user_skills", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('custom_user_skills')->hasColumn('id')) {
            $this->table("custom_user_skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("custom_user_skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 50, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("dashboard_styles", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('dashboard_styles')->hasColumn('id')) {
            $this->table("dashboard_styles")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("dashboard_styles")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('target', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('style', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'target'])->update();
        $table->save();
        $table = $this->table("email_template", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('email_template')->hasColumn('id')) {
            $this->table("email_template")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("email_template")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('template', 'text', ['null' => false, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id'])->update();
        $table->save();
        $table = $this->table("emails", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('emails')->hasColumn('id')) {
            $this->table("emails")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("emails")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('type', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_company'])->update();
        $table->addColumn('status', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'type'])->update();
        $table->addColumn('sent', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'status'])->update();
        $table->addColumn('frequency', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'sent'])->update();
        $table->addColumn('direction', 'string', ['null' => true, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'frequency'])->update();
        $table->addColumn('email', 'string', ['null' => true, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'direction'])->update();
        $table->addColumn('from', 'string', ['null' => true, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'email'])->update();
        $table->save();
        $table = $this->table("emails_lang", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('emails_lang')->hasColumn('id')) {
            $this->table("emails_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("emails_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_email', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_lang', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_email'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_lang'])->update();
        $table->addColumn('subject', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->addColumn('content', 'text', ['null' => false, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'subject'])->update();
        $table->save();
        $table = $this->table("emails_log", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('emails_log')->hasColumn('id')) {
            $this->table("emails_log")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("emails_log")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('email_user', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('email_type', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'email_user'])->update();
        $table->addColumn('cron_file', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'email_type'])->update();
        $table->addColumn('conditions', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'cron_file'])->update();
        $table->addColumn('subject', 'text', ['null' => true, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'conditions'])->update();
        $table->addColumn('content', 'text', ['null' => true, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'subject'])->update();
        $table->addColumn('send', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'content'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'send'])->update();
        $table->save();
        $table = $this->table("hiworks", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('hiworks')->hasColumn('id')) {
            $this->table("hiworks")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("hiworks")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('content', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'status'])->update();
        $table->save();
        $table = $this->table("hobbies", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('hobbies')->hasColumn('id')) {
            $this->table("hobbies")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("hobbies")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->save();
        $table = $this->table("hr_jobs", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('hr_jobs')->hasColumn('id')) {
            $this->table("hr_jobs")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("hr_jobs")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_department', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_department'])->update();
        $table->save();
        $table = $this->table("job_application_status_history", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_application_status_history')->hasColumn('id')) {
            $this->table("job_application_status_history")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_application_status_history")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('job_application_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('modified_by', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'job_application_id'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'modified_by'])->update();
        $table->addColumn('status_reason', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'status'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'status_reason'])->update();
        $table->save();
        $table = $this->table("job_headcount", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_headcount')->hasColumn('id')) {
            $this->table("job_headcount")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_headcount")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('headcount', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("job_shares", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_shares')->hasColumn('id')) {
            $this->table("job_shares")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_shares")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('channel', 'enum', ['null' => false, 'limit' => 8, 'values' => ['facebook','email','twitter','linkedin','gplus'], 'after' => 'id_job'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'channel'])->update();
        $table->save();
        $table = $this->table("job_sub_departments", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_sub_departments')->hasColumn('id')) {
            $this->table("job_sub_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_sub_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_sub_department', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("job_sub_sub_departments", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_sub_sub_departments')->hasColumn('id')) {
            $this->table("job_sub_sub_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_sub_sub_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_sub_sub_department', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("jobs_share", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('jobs_share')->hasColumn('id')) {
            $this->table("jobs_share")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("jobs_share")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('shares', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('views', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'shares'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'views'])->update();
        $table->save();
        if($this->table('jobs_share')->hasIndex('id_user')) {
            $this->table("jobs_share")->removeIndexByName('id_user');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_2')) {
            $this->table("jobs_share")->removeIndexByName('id_user_2');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_2", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_3')) {
            $this->table("jobs_share")->removeIndexByName('id_user_3');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_3", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_4')) {
            $this->table("jobs_share")->removeIndexByName('id_user_4');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_4", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_5')) {
            $this->table("jobs_share")->removeIndexByName('id_user_5');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_5", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_6')) {
            $this->table("jobs_share")->removeIndexByName('id_user_6');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_6", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_7')) {
            $this->table("jobs_share")->removeIndexByName('id_user_7');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_7", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_8')) {
            $this->table("jobs_share")->removeIndexByName('id_user_8');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_8", 'unique' => false])->save();
        if($this->table('jobs_share')->hasIndex('id_user_9')) {
            $this->table("jobs_share")->removeIndexByName('id_user_9');
        }
        $this->table("jobs_share")->addIndex(['id_user','id_job','shares','views','date'], ['name' => "id_user_9", 'unique' => false])->save();
        $table = $this->table("linkedin_connections", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('linkedin_connections')->hasColumn('id')) {
            $this->table("linkedin_connections")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("linkedin_connections")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('firstname', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('lastname', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'firstname'])->update();
        $table->addColumn('email', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'lastname'])->update();
        $table->addColumn('company', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'email'])->update();
        $table->addColumn('position', 'string', ['null' => true, 'limit' => 200, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'company'])->update();
        $table->addColumn('connected', 'date', ['null' => false, 'after' => 'position'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'connected'])->update();
        $table->addColumn('linked', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        if($this->table('linkedin_connections')->hasIndex('email')) {
            $this->table("linkedin_connections")->removeIndexByName('email');
        }
        $this->table("linkedin_connections")->addIndex(['email'], ['name' => "email", 'unique' => true])->save();
        $table = $this->table("notification", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('notification')->hasColumn('id')) {
            $this->table("notification")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("notification")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('message', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('note', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'message'])->update();
        $table->addColumn('viewed', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'note'])->update();
        $table->addColumn('result_orientation', 'boolean', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'viewed'])->update();
        $table->addColumn('culture_fit', 'boolean', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'result_orientation'])->update();
        $table->addColumn('technical_skills', 'boolean', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'culture_fit'])->update();
        $table->addColumn('ignore_rate', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'technical_skills'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'ignore_rate'])->update();
        $table->save();
        $table = $this->table("pilote", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('pilote')->hasColumn('id')) {
            $this->table("pilote")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("pilote")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('count', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'count'])->update();
        $table->save();
        $table = $this->table("projects", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('projects')->hasColumn('id')) {
            $this->table("projects")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("projects")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 100, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 100, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        if($this->table('projects')->hasIndex('id')) {
            $this->table("projects")->removeIndexByName('id');
        }
        $this->table("projects")->addIndex(['id'], ['name' => "id", 'unique' => true])->save();
        $table = $this->table("rating", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('rating')->hasColumn('id')) {
            $this->table("rating")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("rating")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->save();
        $table = $this->table("recommender_reminder", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('recommender_reminder')->hasColumn('id')) {
            $this->table("recommender_reminder")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("recommender_reminder")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("recruiter_job", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('recruiter_job')->hasColumn('id')) {
            $this->table("recruiter_job")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("recruiter_job")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("salary_job_titles", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('salary_job_titles')->hasColumn('id')) {
            $this->table("salary_job_titles")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("salary_job_titles")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->save();
        $table = $this->table("social_media", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('social_media')->hasColumn('id')) {
            $this->table("social_media")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("social_media")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 100, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('url', 'text', ['null' => true, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("status", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('status')->hasColumn('id')) {
            $this->table("status")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("status")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('status', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'status'])->update();
        $table->addColumn('public_title', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'slug'])->update();
        $table->addColumn('column_order', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'public_title'])->update();
        $table->addColumn('active', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'column_order'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'active'])->update();
        $table->addColumn('sla', 'integer', ['null' => false, 'default' => '48', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "hours to change (deadline)", 'after' => 'date'])->update();
        $table->save();
        $table = $this->table("status_options", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('status_options')->hasColumn('id')) {
            $this->table("status_options")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10])->update();
        } else {
            $this->table("status_options")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10])->update();
        }
        $table->addColumn('id_status', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('status', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_status'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'status'])->update();
        $table->addColumn('public_title', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'slug'])->update();
        $table->addColumn('active', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'public_title'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'active'])->update();
        $table->addColumn('sla', 'integer', ['null' => false, 'default' => '48', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'date'])->update();
        $table->addColumn('color', 'text', ['null' => true, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'sla'])->update();
        $table->addColumn('icon', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'color'])->update();
        $table->save();
        $table = $this->table("sub_departments", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('sub_departments')->hasColumn('id')) {
            $this->table("sub_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("sub_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_department', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 300, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_department'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 300, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        if($this->table('sub_departments')->hasIndex('id_2')) {
            $this->table("sub_departments")->removeIndexByName('id_2');
        }
        $this->table("sub_departments")->addIndex(['id'], ['name' => "id_2", 'unique' => true])->save();
        if($this->table('sub_departments')->hasIndex('id')) {
            $this->table("sub_departments")->removeIndexByName('id');
        }
        $this->table("sub_departments")->addIndex(['id'], ['name' => "id", 'unique' => false])->save();
        $table = $this->table("sub_sub_departments", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('sub_sub_departments')->hasColumn('id')) {
            $this->table("sub_sub_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("sub_sub_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_sub_department', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 300, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_sub_department'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 300, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->save();
        if($this->table('sub_sub_departments')->hasIndex('id')) {
            $this->table("sub_sub_departments")->removeIndexByName('id');
        }
        $this->table("sub_sub_departments")->addIndex(['id'], ['name' => "id", 'unique' => true])->save();
        $table = $this->table("terms", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('terms')->hasColumn('id')) {
            $this->table("terms")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("terms")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('content', 'text', ['null' => false, 'limit' => 65535, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'status'])->update();
        $table->save();
        $table = $this->table("user_cv", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_cv')->hasColumn('id')) {
            $this->table("user_cv")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_cv")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('file', 'string', ['null' => false, 'limit' => 150, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'title'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'file'])->update();
        $table->save();
        $table = $this->table("user_hobbies", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_hobbies')->hasColumn('id')) {
            $this->table("user_hobbies")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_hobbies")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_hobby', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("user_languages", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_languages')->hasColumn('id')) {
            $this->table("user_languages")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_languages")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('language', 'string', ['null' => false, 'limit' => 250, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'language'])->update();
        $table->addColumn('rating', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'date'])->update();
        $table->save();
        $table = $this->table("user_login", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_login')->hasColumn('id')) {
            $this->table("user_login")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_login")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("user_profile", ['engine' => "MyISAM", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_profile')->hasColumn('id')) {
            $this->table("user_profile")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_profile")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('completed_history', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('completed_education', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'completed_history'])->update();
        $table->addColumn('completed_profile', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'completed_education'])->update();
        $table->save();
        $table = $this->table("user_signature", ['engine' => "InnoDB", 'encoding' => "latin1", 'collation' => "latin1_swedish_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_signature')->hasColumn('id')) {
            $this->table("user_signature")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_signature")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('signature', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "latin1_swedish_ci", 'encoding' => "latin1", 'after' => 'id_user'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'signature'])->update();
        $table->save();
        $table = $this->table("bounties", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('bounties')->hasColumn('id')) {
            $this->table("bounties")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("bounties")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('amount', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'id_user'])->update();
        $table->addColumn('status', 'enum', ['null' => false, 'limit' => 9, 'values' => ['initiated','paid','canceled','pending'], 'after' => 'amount'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'status'])->update();
        $table->addColumn('due_date', 'datetime', ['null' => false, 'after' => 'date'])->update();
        $table->save();
        $table = $this->table("bounty_documents", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('bounty_documents')->hasColumn('id')) {
            $this->table("bounty_documents")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("bounty_documents")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("bounty_documents_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('bounty_documents_lang')->hasColumn('id')) {
            $this->table("bounty_documents_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("bounty_documents_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_bounty_document', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_lang', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_bounty_document'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_lang'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('content', 'text', ['null' => false, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'slug'])->update();
        $table->save();
        $table = $this->table("bounty_files", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('bounty_files')->hasColumn('id')) {
            $this->table("bounty_files")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("bounty_files")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_bounty', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('file', 'string', ['null' => false, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_bounty'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'file'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("branding", ['engine' => "MyISAM", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('branding')->hasColumn('id')) {
            $this->table("branding")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("branding")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('class_name', 'string', ['null' => true, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id'])->update();
        $table->addColumn('value', 'string', ['null' => true, 'limit' => 7, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'class_name'])->update();
        $table->addColumn('style', 'string', ['null' => false, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'value'])->update();
        $table->addColumn('id_category', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'style'])->update();
        $table->addColumn('name', 'string', ['null' => false, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_category'])->update();
        $table->save();
        if($this->table('branding')->hasIndex('id')) {
            $this->table("branding")->removeIndexByName('id');
        }
        $this->table("branding")->addIndex(['id'], ['name' => "id", 'unique' => true])->save();
        $table = $this->table("calendar", ['id' => false, 'primary_key' => ["zi"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->addColumn('zi', 'date', ['null' => false]);
        $table->addColumn('sapt', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'zi']);
        $table->addColumn('luna', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'sapt']);
        $table->addColumn('lll', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'luna']);
        $table->addColumn('an', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'lll']);
        $table->addColumn('zi_sapt', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'an']);
        $table->addColumn('zzz_sapt', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'zi_sapt']);
        $table->addColumn('zi_luna', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'zzz_sapt']);
        $table->addColumn('zi_an', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'zi_luna']);
        $table->addColumn('weekend', 'boolean', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'zi_an']);
        $table->addColumn('sarbatoare', 'boolean', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'weekend']);
        $table->save();
        if($this->table('calendar')->hasIndex('zi')) {
            $this->table("calendar")->removeIndexByName('zi');
        }
        $this->table("calendar")->addIndex(['zi'], ['name' => "zi", 'unique' => true])->save();
        $table = $this->table("certifications", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('certifications')->hasColumn('id')) {
            $this->table("certifications")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("certifications")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('company', 'string', ['null' => false, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id'])->update();
        $table->addColumn('active_jobs', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'company'])->update();
        $table->addColumn('active_users', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'active_jobs'])->update();
        $table->save();
        $table = $this->table("certifications_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('certifications_lang')->hasColumn('id')) {
            $this->table("certifications_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("certifications_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_lang', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_certification', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_lang'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_certification'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->save();
        $table = $this->table("cities", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('cities')->hasColumn('id')) {
            $this->table("cities")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("cities")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_county', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_country', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_county'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_country'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->save();
        if($this->table('cities')->hasIndex('id_country')) {
            $this->table("cities")->removeIndexByName('id_country');
        }
        $this->table("cities")->addIndex(['id_country'], ['name' => "id_country", 'unique' => false])->save();
        $table = $this->table("companies", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('companies')->hasColumn('id')) {
            $this->table("companies")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("companies")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('allow_candidate_recommend', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('allow_external', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'allow_candidate_recommend'])->update();
        $table->addColumn('id_city', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'allow_external'])->update();
        $table->addColumn('id_country', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_city'])->update();
        $table->addColumn('id_industry', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_country'])->update();
        $table->addColumn('founded', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_industry'])->update();
        $table->addColumn('rating', 'float', ['null' => true, 'default' => '0', 'precision' => 12, 'after' => 'founded'])->update();
        $table->addColumn('rating_work', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating'])->update();
        $table->addColumn('rating_career', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_work'])->update();
        $table->addColumn('rating_benefits', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_career'])->update();
        $table->addColumn('rating_balance', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_benefits'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'rating_balance'])->update();
        $table->addColumn('slug', 'string', ['null' => false, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('description', 'string', ['null' => true, 'limit' => 500, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'slug'])->update();
        $table->addColumn('address', 'string', ['null' => true, 'limit' => 500, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'description'])->update();
        $table->addColumn('long_description', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'address'])->update();
        $table->addColumn('logo', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'long_description'])->update();
        $table->addColumn('landing', 'string', ['null' => true, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'logo'])->update();
        $table->addColumn('website', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'landing'])->update();
        $table->addColumn('linkedin', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'website'])->update();
        $table->addColumn('facebook', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin'])->update();
        $table->addColumn('youtube', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'facebook'])->update();
        $table->addColumn('instagram', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'youtube'])->update();
        $table->addColumn('momentum', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'instagram'])->update();
        $table->addColumn('twitter', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'momentum'])->update();
        $table->addColumn('email', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'twitter'])->update();
        $table->addColumn('url', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'email'])->update();
        $table->addColumn('cover_1', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'url'])->update();
        $table->addColumn('cover_2', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'cover_1'])->update();
        $table->addColumn('cover_3', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'cover_2'])->update();
        $table->addColumn('cover_4', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'cover_3'])->update();
        $table->addColumn('size', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'cover_4'])->update();
        $table->addColumn('phone', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'size'])->update();
        $table->addColumn('linkedin_id', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'phone'])->update();
        $table->addColumn('type', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_id'])->update();
        $table->addColumn('specialities', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'type'])->update();
        $table->addColumn('status', 'boolean', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'specialities'])->update();
        $table->addColumn('synced_linkedin', 'boolean', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'status'])->update();
        $table->addColumn('had_demo', 'boolean', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'synced_linkedin'])->update();
        $table->addColumn('applicants_alert', 'enum', ['null' => false, 'default' => 'daily', 'limit' => 7, 'values' => ['daily','weekly','monthly'], 'after' => 'had_demo'])->update();
        $table->addColumn('applicants_status', 'enum', ['null' => false, 'default' => 'daily', 'limit' => 7, 'values' => ['daily','weekly','monthly'], 'after' => 'applicants_alert'])->update();
        $table->addColumn('created', 'datetime', ['null' => false, 'after' => 'applicants_status'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'created'])->update();
        $table->addColumn('company_type', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'updated'])->update();
        $table->addColumn('contact_person', 'string', ['null' => false, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'company_type'])->update();
        $table->addColumn('allow_employee_apply', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'contact_person'])->update();
        $table->addColumn('is_pilote', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'allow_employee_apply'])->update();
        $table->addColumn('completed_profile', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'is_pilote'])->update();
        $table->addColumn('signin', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'completed_profile'])->update();
        $table->addColumn('brainspotting_bounty', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'signin'])->update();
        $table->addColumn('track_timeline', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'brainspotting_bounty'])->update();
        $table->addColumn('analytics', 'string', ['null' => false, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'track_timeline'])->update();
        $table->addColumn('heatmap', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'analytics'])->update();
        $table->addColumn('footer', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'heatmap'])->update();
        $table->addColumn('register_text', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'footer'])->update();
        $table->addColumn('signin_text', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'register_text'])->update();
        $table->addColumn('notification', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'signin_text'])->update();
        $table->addColumn('upload_cv', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'notification'])->update();
        $table->addColumn('import_linkedin', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'upload_cv'])->update();
        $table->addColumn('job_departments', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'import_linkedin'])->update();
        $table->addColumn('job_skills', 'integer', ['null' => false, 'default' => '1', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'job_departments'])->update();
        $table->addColumn('launch_date', 'date', ['null' => false, 'after' => 'job_skills'])->update();
        $table->addColumn('launch_time', 'time', ['null' => false, 'after' => 'launch_date'])->update();
        $table->save();
        $table = $this->table("companies_bounties", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('companies_bounties')->hasColumn('id')) {
            $this->table("companies_bounties")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("companies_bounties")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('amount', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'id_user'])->update();
        $table->addColumn('status', 'enum', ['null' => false, 'limit' => 9, 'values' => ['initiated','paid','canceled'], 'after' => 'amount'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'status'])->update();
        $table->save();
        if($this->table('companies_bounties')->hasIndex('id_job')) {
            $this->table("companies_bounties")->removeIndexByName('id_job');
        }
        $this->table("companies_bounties")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        $table = $this->table("companies_rating", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('companies_rating')->hasColumn('id')) {
            $this->table("companies_rating")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("companies_rating")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('anonymous', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_job'])->update();
        $table->addColumn('value', 'float', ['null' => true, 'default' => '0', 'precision' => 12, 'after' => 'anonymous'])->update();
        $table->addColumn('rating_work', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'value'])->update();
        $table->addColumn('rating_career', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_work'])->update();
        $table->addColumn('rating_benefits', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_career'])->update();
        $table->addColumn('rating_balance', 'float', ['null' => false, 'default' => '0', 'precision' => 12, 'after' => 'rating_benefits'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'rating_balance'])->update();
        $table->addColumn('job_title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('time', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'job_title'])->update();
        $table->addColumn('worked', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'time'])->update();
        $table->addColumn('content', 'string', ['null' => true, 'limit' => 500, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'worked'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'content'])->update();
        $table->save();
        if($this->table('companies_rating')->hasIndex('id_company')) {
            $this->table("companies_rating")->removeIndexByName('id_company');
        }
        $this->table("companies_rating")->addIndex(['id_company'], ['name' => "id_company", 'unique' => false])->save();
        $table = $this->table("company_credits", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('company_credits')->hasColumn('id')) {
            $this->table("company_credits")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("company_credits")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('credits', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('job_credits', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'credits'])->update();
        $table->addColumn('search_credits', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'job_credits'])->update();
        $table->addColumn('highlight_credits', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'search_credits'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => true, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'highlight_credits'])->update();
        $table->addColumn('added', 'datetime', ['null' => true, 'after' => 'updated'])->update();
        $table->save();
        $table = $this->table("company_unblocked_cvs", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('company_unblocked_cvs')->hasColumn('id')) {
            $this->table("company_unblocked_cvs")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("company_unblocked_cvs")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('date', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'id_company'])->update();
        $table->save();
        $table = $this->table("company_users", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('company_users')->hasColumn('id')) {
            $this->table("company_users")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("company_users")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("counties", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('counties')->hasColumn('id')) {
            $this->table("counties")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("counties")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_country', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_country'])->update();
        $table->save();
        if($this->table('counties')->hasIndex('IDX_A26779F3F92F3E70')) {
            $this->table("counties")->removeIndexByName('IDX_A26779F3F92F3E70');
        }
        $this->table("counties")->addIndex(['id_country'], ['name' => "IDX_A26779F3F92F3E70", 'unique' => false])->save();
        $table = $this->table("countries", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('countries')->hasColumn('id')) {
            $this->table("countries")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("countries")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id'])->update();
        $table->save();
        $table = $this->table("departments", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('departments')->hasColumn('id')) {
            $this->table("departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('active_jobs', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('active_users', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'active_jobs'])->update();
        $table->save();
        $table = $this->table("departments_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('departments_lang')->hasColumn('id')) {
            $this->table("departments_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("departments_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_lang', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_department', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_lang'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_department'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->save();
        if($this->table('departments_lang')->hasIndex('id_department')) {
            $this->table("departments_lang")->removeIndexByName('id_department');
        }
        $this->table("departments_lang")->addIndex(['id_department'], ['name' => "id_department", 'unique' => false])->save();
        if($this->table('departments_lang')->hasIndex('id_lang')) {
            $this->table("departments_lang")->removeIndexByName('id_lang');
        }
        $this->table("departments_lang")->addIndex(['id_lang'], ['name' => "id_lang", 'unique' => false])->save();
        $table = $this->table("gamification_points", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('gamification_points')->hasColumn('id')) {
            $this->table("gamification_points")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("gamification_points")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_item', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('action', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_item'])->update();
        $table->addColumn('points', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'action'])->update();
        $table->addColumn('date', 'datetime', ['null' => true, 'after' => 'points'])->update();
        $table->save();
        if($this->table('gamification_points')->hasIndex('id_user')) {
            $this->table("gamification_points")->removeIndexByName('id_user');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_2')) {
            $this->table("gamification_points")->removeIndexByName('id_user_2');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_2", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_3')) {
            $this->table("gamification_points")->removeIndexByName('id_user_3');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_3", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_4')) {
            $this->table("gamification_points")->removeIndexByName('id_user_4');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_4", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_5')) {
            $this->table("gamification_points")->removeIndexByName('id_user_5');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_5", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_6')) {
            $this->table("gamification_points")->removeIndexByName('id_user_6');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_6", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_7')) {
            $this->table("gamification_points")->removeIndexByName('id_user_7');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_7", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_8')) {
            $this->table("gamification_points")->removeIndexByName('id_user_8');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_8", 'unique' => false])->save();
        if($this->table('gamification_points')->hasIndex('id_user_9')) {
            $this->table("gamification_points")->removeIndexByName('id_user_9');
        }
        $this->table("gamification_points")->addIndex(['id_user','id_item','action','date'], ['name' => "id_user_9", 'unique' => false])->save();
        $table = $this->table("gamifications_actions", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('gamifications_actions')->hasColumn('id')) {
            $this->table("gamifications_actions")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("gamifications_actions")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('action', 'string', ['null' => false, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id'])->update();
        $table->addColumn('points', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'action'])->update();
        $table->save();
        $table = $this->table("geoip", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('geoip')->hasColumn('id')) {
            $this->table("geoip")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("geoip")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_country', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('ip_start', 'string', ['null' => false, 'limit' => 15, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_country'])->update();
        $table->addColumn('ip_end', 'string', ['null' => false, 'limit' => 15, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'ip_start'])->update();
        $table->addColumn('ip_long_start', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_BIG, 'precision' => 19, 'after' => 'ip_end'])->update();
        $table->addColumn('ip_long_end', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_BIG, 'precision' => 19, 'after' => 'ip_long_start'])->update();
        $table->addColumn('country_code', 'string', ['null' => false, 'limit' => 3, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'ip_long_end'])->update();
        $table->addColumn('country', 'string', ['null' => false, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'country_code'])->update();
        $table->save();
        $table = $this->table("industries", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('industries')->hasColumn('id')) {
            $this->table("industries")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("industries")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('active_jobs', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('active_users', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'active_jobs'])->update();
        $table->save();
        $table = $this->table("industries_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('industries_lang')->hasColumn('id')) {
            $this->table("industries_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("industries_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_lang', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_industry', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_lang'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_industry'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->save();
        if($this->table('industries_lang')->hasIndex('id_lang')) {
            $this->table("industries_lang")->removeIndexByName('id_lang');
        }
        $this->table("industries_lang")->addIndex(['id_lang'], ['name' => "id_lang", 'unique' => false])->save();
        if($this->table('industries_lang')->hasIndex('id_industry')) {
            $this->table("industries_lang")->removeIndexByName('id_industry');
        }
        $this->table("industries_lang")->addIndex(['id_industry'], ['name' => "id_industry", 'unique' => false])->save();
        $table = $this->table("job_applications", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_applications')->hasColumn('id')) {
            $this->table("job_applications")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_applications")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('id_recommender', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('modified_by', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('recommend_type', 'enum', ['null' => false, 'default' => 'platform', 'limit' => 8, 'values' => ['internal','platform'], 'after' => 'modified_by'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'recommend_type'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'date'])->update();
        $table->addColumn('status', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'updated'])->update();
        $table->addColumn('status_reason', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'status'])->update();
        $table->addColumn('viewed', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'status_reason'])->update();
        $table->addColumn('pending', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'viewed'])->update();
        $table->addColumn('comments', 'string', ['null' => false, 'default' => '0', 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'pending'])->update();
        $table->addColumn('source', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'comments'])->update();
        $table->addColumn('removed', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'source'])->update();
        $table->save();
        if($this->table('job_applications')->hasIndex('id_job')) {
            $this->table("job_applications")->removeIndexByName('id_job');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender')) {
            $this->table("job_applications")->removeIndexByName('id_recommender');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status')) {
            $this->table("job_applications")->removeIndexByName('status');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_reason')) {
            $this->table("job_applications")->removeIndexByName('status_reason');
        }
        $this->table("job_applications")->addIndex(['status_reason'], ['name' => "status_reason", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date')) {
            $this->table("job_applications")->removeIndexByName('date');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user')) {
            $this->table("job_applications")->removeIndexByName('id_user');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_2')) {
            $this->table("job_applications")->removeIndexByName('id_user_2');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_2", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_3')) {
            $this->table("job_applications")->removeIndexByName('id_user_3');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_3", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_4')) {
            $this->table("job_applications")->removeIndexByName('id_user_4');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_4", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_5')) {
            $this->table("job_applications")->removeIndexByName('id_user_5');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_5", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_job_7')) {
            $this->table("job_applications")->removeIndexByName('id_job_7');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date_6')) {
            $this->table("job_applications")->removeIndexByName('date_6');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date_6", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender_7')) {
            $this->table("job_applications")->removeIndexByName('id_recommender_7');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_6')) {
            $this->table("job_applications")->removeIndexByName('id_user_6');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_6", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_7')) {
            $this->table("job_applications")->removeIndexByName('status_7');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_reason_7')) {
            $this->table("job_applications")->removeIndexByName('status_reason_7');
        }
        $this->table("job_applications")->addIndex(['status_reason'], ['name' => "status_reason_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_job_8')) {
            $this->table("job_applications")->removeIndexByName('id_job_8');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date_7')) {
            $this->table("job_applications")->removeIndexByName('date_7');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender_8')) {
            $this->table("job_applications")->removeIndexByName('id_recommender_8');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_7')) {
            $this->table("job_applications")->removeIndexByName('id_user_7');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_7", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_8')) {
            $this->table("job_applications")->removeIndexByName('status_8');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_reason_8')) {
            $this->table("job_applications")->removeIndexByName('status_reason_8');
        }
        $this->table("job_applications")->addIndex(['status_reason'], ['name' => "status_reason_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_job_9')) {
            $this->table("job_applications")->removeIndexByName('id_job_9');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date_8')) {
            $this->table("job_applications")->removeIndexByName('date_8');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender_9')) {
            $this->table("job_applications")->removeIndexByName('id_recommender_9');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_8')) {
            $this->table("job_applications")->removeIndexByName('id_user_8');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_8", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_9')) {
            $this->table("job_applications")->removeIndexByName('status_9');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_reason_9')) {
            $this->table("job_applications")->removeIndexByName('status_reason_9');
        }
        $this->table("job_applications")->addIndex(['status_reason'], ['name' => "status_reason_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_job_10')) {
            $this->table("job_applications")->removeIndexByName('id_job_10');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date_9')) {
            $this->table("job_applications")->removeIndexByName('date_9');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender_10')) {
            $this->table("job_applications")->removeIndexByName('id_recommender_10');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_9')) {
            $this->table("job_applications")->removeIndexByName('id_user_9');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_9", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_10')) {
            $this->table("job_applications")->removeIndexByName('status_10');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_reason_10')) {
            $this->table("job_applications")->removeIndexByName('status_reason_10');
        }
        $this->table("job_applications")->addIndex(['status_reason'], ['name' => "status_reason_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_job_11')) {
            $this->table("job_applications")->removeIndexByName('id_job_11');
        }
        $this->table("job_applications")->addIndex(['id_job'], ['name' => "id_job_11", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('date_10')) {
            $this->table("job_applications")->removeIndexByName('date_10');
        }
        $this->table("job_applications")->addIndex(['date'], ['name' => "date_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_recommender_11')) {
            $this->table("job_applications")->removeIndexByName('id_recommender_11');
        }
        $this->table("job_applications")->addIndex(['id_recommender'], ['name' => "id_recommender_11", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('id_user_10')) {
            $this->table("job_applications")->removeIndexByName('id_user_10');
        }
        $this->table("job_applications")->addIndex(['id_user'], ['name' => "id_user_10", 'unique' => false])->save();
        if($this->table('job_applications')->hasIndex('status_11')) {
            $this->table("job_applications")->removeIndexByName('status_11');
        }
        $this->table("job_applications")->addIndex(['status'], ['name' => "status_11", 'unique' => false])->save();
        $table = $this->table("job_cities", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_cities')->hasColumn('id')) {
            $this->table("job_cities")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_cities")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_city', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('vacancies', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "number of required people to hire", 'after' => 'id_city'])->update();
        $table->save();
        $table = $this->table("job_departments", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_departments')->hasColumn('id')) {
            $this->table("job_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_department', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        if($this->table('job_departments')->hasIndex('id_job')) {
            $this->table("job_departments")->removeIndexByName('id_job');
        }
        $this->table("job_departments")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_departments')->hasIndex('id_department')) {
            $this->table("job_departments")->removeIndexByName('id_department');
        }
        $this->table("job_departments")->addIndex(['id_department'], ['name' => "id_department", 'unique' => false])->save();
        $table = $this->table("job_industries", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_industries')->hasColumn('id')) {
            $this->table("job_industries")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_industries")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_industry', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        $table = $this->table("job_projects", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_projects')->hasColumn('id')) {
            $this->table("job_projects")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_projects")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_project', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        if($this->table('job_projects')->hasIndex('id')) {
            $this->table("job_projects")->removeIndexByName('id');
        }
        $this->table("job_projects")->addIndex(['id'], ['name' => "id", 'unique' => true])->save();
        if($this->table('job_projects')->hasIndex('id_job')) {
            $this->table("job_projects")->removeIndexByName('id_job');
        }
        $this->table("job_projects")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_projects')->hasIndex('id_project')) {
            $this->table("job_projects")->removeIndexByName('id_project');
        }
        $this->table("job_projects")->addIndex(['id_project'], ['name' => "id_project", 'unique' => false])->save();
        $table = $this->table("job_recommendations", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_recommendations')->hasColumn('id')) {
            $this->table("job_recommendations")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_recommendations")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_recommender', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_recommender'])->update();
        $table->addColumn('id_job', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('anonymous', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_job'])->update();
        $table->addColumn('linkedin_id', 'string', ['null' => true, 'limit' => 15, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'anonymous'])->update();
        $table->addColumn('email', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_id'])->update();
        $table->addColumn('viewed', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'email'])->update();
        $table->addColumn('name', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'viewed'])->update();
        $table->addColumn('phone', 'string', ['null' => true, 'limit' => 20, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'name'])->update();
        $table->addColumn('track', 'string', ['null' => true, 'limit' => 13, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'phone'])->update();
        $table->addColumn('cv', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'track'])->update();
        $table->addColumn('date', 'datetime', ['null' => true, 'after' => 'cv'])->update();
        $table->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender'], ['name' => "id_recommender", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_user')) {
            $this->table("job_recommendations")->removeIndexByName('id_user');
        }
        $this->table("job_recommendations")->addIndex(['id_user'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_job')) {
            $this->table("job_recommendations")->removeIndexByName('id_job');
        }
        $this->table("job_recommendations")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_2')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_2');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_2", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_3')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_3');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_3", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_4')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_4');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_4", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_5')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_5');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_5", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_6')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_6');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_6", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_7')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_7');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_7", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_8')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_8');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_8", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_9')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_9');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_9", 'unique' => false])->save();
        if($this->table('job_recommendations')->hasIndex('id_recommender_10')) {
            $this->table("job_recommendations")->removeIndexByName('id_recommender_10');
        }
        $this->table("job_recommendations")->addIndex(['id_recommender','id_user','id_job','email','track','date'], ['name' => "id_recommender_10", 'unique' => false])->save();
        $table = $this->table("job_skills", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_skills')->hasColumn('id')) {
            $this->table("job_skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_skill', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->save();
        if($this->table('job_skills')->hasIndex('id_job')) {
            $this->table("job_skills")->removeIndexByName('id_job');
        }
        $this->table("job_skills")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_skills')->hasIndex('id_skill')) {
            $this->table("job_skills")->removeIndexByName('id_skill');
        }
        $this->table("job_skills")->addIndex(['id_skill'], ['name' => "id_skill", 'unique' => false])->save();
        $table = $this->table("job_views", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('job_views')->hasColumn('id')) {
            $this->table("job_views")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("job_views")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_job', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_job'])->update();
        $table->addColumn('source', 'string', ['null' => false, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_user'])->update();
        $table->addColumn('fingerprint', 'string', ['null' => false, 'limit' => 32, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'source'])->update();
        $table->addColumn('date', 'datetime', ['null' => false, 'after' => 'fingerprint'])->update();
        $table->save();
        if($this->table('job_views')->hasIndex('id_job')) {
            $this->table("job_views")->removeIndexByName('id_job');
        }
        $this->table("job_views")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_2')) {
            $this->table("job_views")->removeIndexByName('id_job_2');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_2", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_3')) {
            $this->table("job_views")->removeIndexByName('id_job_3');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_3", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_4')) {
            $this->table("job_views")->removeIndexByName('id_job_4');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_4", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_5')) {
            $this->table("job_views")->removeIndexByName('id_job_5');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_5", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_6')) {
            $this->table("job_views")->removeIndexByName('id_job_6');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_6", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_7')) {
            $this->table("job_views")->removeIndexByName('id_job_7');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_7", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_8')) {
            $this->table("job_views")->removeIndexByName('id_job_8');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_8", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_9')) {
            $this->table("job_views")->removeIndexByName('id_job_9');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_9", 'unique' => false])->save();
        if($this->table('job_views')->hasIndex('id_job_10')) {
            $this->table("job_views")->removeIndexByName('id_job_10');
        }
        $this->table("job_views")->addIndex(['id_job','id_user','source','fingerprint'], ['name' => "id_job_10", 'unique' => false])->save();
        $table = $this->table("jobs", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('jobs')->hasColumn('id')) {
            $this->table("jobs")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("jobs")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_currency', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('start', 'date', ['null' => true, 'after' => 'id_currency'])->update();
        $table->addColumn('end', 'date', ['null' => true, 'after' => 'start'])->update();
        $table->addColumn('bounty', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'end'])->update();
        $table->addColumn('bounty_file', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'bounty'])->update();
        $table->addColumn('job_public_image', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'bounty_file'])->update();
        $table->addColumn('type', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'job_public_image'])->update();
        $table->addColumn('is_promoted', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'type'])->update();
        $table->addColumn('bounty_paid', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'is_promoted'])->update();
        $table->addColumn('shares', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'bounty_paid'])->update();
        $table->addColumn('recommendations', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'shares'])->update();
        $table->addColumn('status', 'integer', ['null' => true, 'default' => '1', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'recommendations'])->update();
        $table->addColumn('career_level', 'integer', ['null' => true, 'default' => '1', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'status'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => true, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'career_level'])->update();
        $table->addColumn('created_at', 'datetime', ['null' => false, 'after' => 'updated'])->update();
        $table->addColumn('job_type', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'created_at'])->update();
        $table->addColumn('job_ad_lifetime', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'job_type'])->update();
        $table->save();
        if($this->table('jobs')->hasIndex('id_company')) {
            $this->table("jobs")->removeIndexByName('id_company');
        }
        $this->table("jobs")->addIndex(['id_company'], ['name' => "id_company", 'unique' => false])->save();
        if($this->table('jobs')->hasIndex('start')) {
            $this->table("jobs")->removeIndexByName('start');
        }
        $this->table("jobs")->addIndex(['start'], ['name' => "start", 'unique' => false])->save();
        if($this->table('jobs')->hasIndex('end')) {
            $this->table("jobs")->removeIndexByName('end');
        }
        $this->table("jobs")->addIndex(['end'], ['name' => "end", 'unique' => false])->save();
        $table = $this->table("jobs_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('jobs_lang')->hasColumn('id')) {
            $this->table("jobs_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("jobs_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_lang', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_job', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_lang'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_job'])->update();
        $table->addColumn('tagline', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('description', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'tagline'])->update();
        $table->addColumn('requirements', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'description'])->update();
        $table->addColumn('responsabilities', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'requirements'])->update();
        $table->addColumn('bounty_requirements', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'responsabilities'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'bounty_requirements'])->update();
        $table->save();
        if($this->table('jobs_lang')->hasIndex('id_lang')) {
            $this->table("jobs_lang")->removeIndexByName('id_lang');
        }
        $this->table("jobs_lang")->addIndex(['id_lang'], ['name' => "id_lang", 'unique' => false])->save();
        if($this->table('jobs_lang')->hasIndex('id_job')) {
            $this->table("jobs_lang")->removeIndexByName('id_job');
        }
        $this->table("jobs_lang")->addIndex(['id_job'], ['name' => "id_job", 'unique' => false])->save();
        $table = $this->table("notifications", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('notifications')->hasColumn('id')) {
            $this->table("notifications")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("notifications")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_company', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('target', 'enum', ['null' => true, 'default' => 'company', 'limit' => 7, 'values' => ['admin','company'], 'after' => 'id_company'])->update();
        $table->addColumn('type', 'enum', ['null' => true, 'default' => 'notification', 'limit' => 12, 'values' => ['notification','payment'], 'after' => 'target'])->update();
        $table->addColumn('message', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'type'])->update();
        $table->addColumn('url', 'string', ['null' => true, 'default' => 'dashboard', 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'message'])->update();
        $table->addColumn('viewed', 'boolean', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'url'])->update();
        $table->addColumn('date', 'timestamp', ['null' => true, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'viewed'])->update();
        $table->save();
        $table = $this->table("skills", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('skills')->hasColumn('id')) {
            $this->table("skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('active_jobs', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('active_users', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'active_jobs'])->update();
        $table->addColumn('in_alerts', 'boolean', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'active_users'])->update();
        $table->save();
        $table = $this->table("skills_lang", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('skills_lang')->hasColumn('id')) {
            $this->table("skills_lang")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("skills_lang")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_lang', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_skill', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_lang'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_skill'])->update();
        $table->addColumn('slug', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->save();
        if($this->table('skills_lang')->hasIndex('id_lang')) {
            $this->table("skills_lang")->removeIndexByName('id_lang');
        }
        $this->table("skills_lang")->addIndex(['id_lang'], ['name' => "id_lang", 'unique' => false])->save();
        if($this->table('skills_lang')->hasIndex('id_skill')) {
            $this->table("skills_lang")->removeIndexByName('id_skill');
        }
        $this->table("skills_lang")->addIndex(['id_skill'], ['name' => "id_skill", 'unique' => false])->save();
        $table = $this->table("user_alerts", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_alerts')->hasColumn('id')) {
            $this->table("user_alerts")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_alerts")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('title', 'string', ['null' => false, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_user'])->update();
        $table->addColumn('created', 'datetime', ['null' => false, 'after' => 'title'])->update();
        $table->addColumn('reccurence', 'enum', ['null' => false, 'default' => 'daily', 'limit' => 7, 'values' => ['daily','weekly','monthly'], 'after' => 'created'])->update();
        $table->addColumn('conditions', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'reccurence'])->update();
        $table->save();
        $table = $this->table("user_certifications", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_certifications')->hasColumn('id')) {
            $this->table("user_certifications")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_certifications")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_certification', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('id_linkedin', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_certification'])->update();
        $table->save();
        $table = $this->table("user_departments", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_departments')->hasColumn('id')) {
            $this->table("user_departments")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_departments")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_department', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        if($this->table('user_departments')->hasIndex('id_user')) {
            $this->table("user_departments")->removeIndexByName('id_user');
        }
        $this->table("user_departments")->addIndex(['id_user'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('user_departments')->hasIndex('id_department')) {
            $this->table("user_departments")->removeIndexByName('id_department');
        }
        $this->table("user_departments")->addIndex(['id_department'], ['name' => "id_department", 'unique' => false])->save();
        $table = $this->table("user_education", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_education')->hasColumn('id')) {
            $this->table("user_education")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_education")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('start', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_user'])->update();
        $table->addColumn('end', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'start'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'end'])->update();
        $table->addColumn('field', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('activities', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'field'])->update();
        $table->addColumn('degree', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'activities'])->update();
        $table->addColumn('id_linkedin', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'degree'])->update();
        $table->save();
        $table = $this->table("user_industries", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_industries')->hasColumn('id')) {
            $this->table("user_industries")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_industries")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_industry', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("user_positions", ['engine' => "MyISAM", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_positions')->hasColumn('id')) {
            $this->table("user_positions")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_positions")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_industry', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('id_department', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_industry'])->update();
        $table->addColumn('is_current', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_department'])->update();
        $table->addColumn('company', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'is_current'])->update();
        $table->addColumn('summary', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'company'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'summary'])->update();
        $table->addColumn('start', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('end', 'date', ['null' => true, 'after' => 'start'])->update();
        $table->addColumn('monthly_gross_pay_slip', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'end'])->update();
        $table->addColumn('id_linkedin', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'monthly_gross_pay_slip'])->update();
        $table->save();
        if($this->table('user_positions')->hasIndex('title')) {
            $this->table("user_positions")->removeIndexByName('title');
        }
        $this->table("user_positions")->addIndex(['title'], ['name' => "title", 'unique' => false, 'type' => 'fulltext'])->save();
        if($this->table('user_positions')->hasIndex('summary')) {
            $this->table("user_positions")->removeIndexByName('summary');
        }
        $this->table("user_positions")->addIndex(['summary'], ['name' => "summary", 'unique' => false, 'type' => 'fulltext'])->save();
        $table = $this->table("user_references", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "external references"]);
        $table->save();
        if ($this->table('user_references')->hasColumn('id')) {
            $this->table("user_references")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10])->update();
        } else {
            $this->table("user_references")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "candidate user", 'after' => 'id'])->update();
        $table->addColumn('referee_first_name', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'comment' => "referee ", 'after' => 'id_user'])->update();
        $table->addColumn('referee_last_name', 'text', ['null' => false, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'comment' => "referee", 'after' => 'referee_first_name'])->update();
        $table->addColumn('referee_email', 'string', ['null' => false, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'referee_last_name'])->update();
        $table->addColumn('referee_phone', 'string', ['null' => false, 'limit' => 15, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'referee_email'])->update();
        $table->addColumn('referee_current_position', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'referee_phone'])->update();
        $table->addColumn('referee_current_company', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'referee_current_position'])->update();
        $table->addColumn('collaboration_background', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'comment' => "when and where they worked together", 'after' => 'referee_current_company'])->update();
        $table->addColumn('user_result_orientation', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "1-5 stars", 'after' => 'collaboration_background'])->update();
        $table->addColumn('user_collaborative_skills', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "1-5 stars", 'after' => 'user_result_orientation'])->update();
        $table->addColumn('user_solution_finder', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "1-5 stars", 'after' => 'user_collaborative_skills'])->update();
        $table->addColumn('user_team_work', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "1-5 stars", 'after' => 'user_solution_finder'])->update();
        $table->addColumn('user_technical_skills', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'comment' => "1-5 stars", 'after' => 'user_team_work'])->update();
        $table->addColumn('user_reference_message', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'user_technical_skills'])->update();
        $table->addColumn('referee_accept_contact', 'boolean', ['null' => false, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'user_reference_message'])->update();
        $table->addColumn('request_date', 'timestamp', ['null' => false, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'referee_accept_contact'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'request_date'])->update();
        $table->save();
        $table = $this->table("user_skills", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_skills')->hasColumn('id')) {
            $this->table("user_skills")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_skills")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('id_skill', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->addColumn('id_linkedin', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'id_skill'])->update();
        $table->addColumn('skill_years', 'decimal', ['null' => true, 'precision' => 4, 'scale' => 1, 'after' => 'id_linkedin'])->update();
        $table->addColumn('rating', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'skill_years'])->update();
        $table->save();
        if($this->table('user_skills')->hasIndex('id_user')) {
            $this->table("user_skills")->removeIndexByName('id_user');
        }
        $this->table("user_skills")->addIndex(['id_user'], ['name' => "id_user", 'unique' => false])->save();
        if($this->table('user_skills')->hasIndex('id_skill')) {
            $this->table("user_skills")->removeIndexByName('id_skill');
        }
        $this->table("user_skills")->addIndex(['id_skill'], ['name' => "id_skill", 'unique' => false])->save();
        $table = $this->table("user_tooltip_view", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('user_tooltip_view')->hasColumn('id')) {
            $this->table("user_tooltip_view")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("user_tooltip_view")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('id_user', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id'])->update();
        $table->addColumn('view', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_user'])->update();
        $table->save();
        $table = $this->table("users", ['engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => ""]);
        $table->save();
        if ($this->table('users')->hasColumn('id')) {
            $this->table("users")->changeColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        } else {
            $this->table("users")->addColumn('id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable'])->update();
        }
        $table->addColumn('first_login', 'datetime', ['null' => true, 'after' => 'id'])->update();
        $table->addColumn('id_company', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'first_login'])->update();
        $table->addColumn('id_country', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_company'])->update();
        $table->addColumn('id_city', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_country'])->update();
        $table->addColumn('is_imported', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'id_city'])->update();
        $table->addColumn('is_company_admin', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'is_imported'])->update();
        $table->addColumn('recommendations', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'is_company_admin'])->update();
        $table->addColumn('id_parent', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'recommendations'])->update();
        $table->addColumn('type', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'id_parent'])->update();
        $table->addColumn('status', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'type'])->update();
        $table->addColumn('email', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'status'])->update();
        $table->addColumn('title', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'email'])->update();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'title'])->update();
        $table->addColumn('password', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'scope'])->update();
        $table->addColumn('salt', 'string', ['null' => true, 'limit' => 250, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'password'])->update();
        $table->addColumn('phone', 'string', ['null' => true, 'limit' => 50, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'salt'])->update();
        $table->addColumn('firstname', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'phone'])->update();
        $table->addColumn('lastname', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'firstname'])->update();
        $table->addColumn('source', 'integer', ['null' => true, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'lastname'])->update();
        $table->addColumn('address', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'source'])->update();
        $table->addColumn('availability', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'address'])->update();
        $table->addColumn('facebook_id', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'availability'])->update();
        $table->addColumn('twitter_id', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'facebook_id'])->update();
        $table->addColumn('linkedin_id', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'twitter_id'])->update();
        $table->addColumn('linkedin_link', 'string', ['null' => false, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_id'])->update();
        $table->addColumn('google_id', 'string', ['null' => true, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_link'])->update();
        $table->addColumn('image', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'google_id'])->update();
        $table->addColumn('location', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'image'])->update();
        $table->addColumn('billing_address', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'location'])->update();
        $table->addColumn('billing_bank', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_address'])->update();
        $table->addColumn('billing_iban', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_bank'])->update();
        $table->addColumn('billing_cui', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_iban'])->update();
        $table->addColumn('billing_j', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_cui'])->update();
        $table->addColumn('billing_company', 'string', ['null' => true, 'limit' => 150, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_j'])->update();
        $table->addColumn('lastjob_lifetime', 'string', ['null' => true, 'limit' => 30, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'billing_company'])->update();
        $table->addColumn('salutation', 'string', ['null' => true, 'limit' => 6, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'lastjob_lifetime'])->update();
        $table->addColumn('gender', 'enum', ['null' => true, 'limit' => 1, 'values' => ['M','F'], 'after' => 'salutation'])->update();
        $table->addColumn('facebook_token', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'gender'])->update();
        $table->addColumn('linkedin_token', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'facebook_token'])->update();
        $table->addColumn('password_token', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_token'])->update();
        $table->addColumn('description', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'password_token'])->update();
        $table->addColumn('hash', 'text', ['null' => true, 'limit' => 65535, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'description'])->update();
        $table->addColumn('last_login', 'datetime', ['null' => true, 'after' => 'hash'])->update();
        $table->addColumn('last_login_ip', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'last_login'])->update();
        $table->addColumn('created', 'datetime', ['null' => true, 'after' => 'last_login_ip'])->update();
        $table->addColumn('updated', 'timestamp', ['null' => true, 'default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP', 'after' => 'created'])->update();
        $table->addColumn('job_type', 'string', ['null' => false, 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'updated'])->update();
        $table->addColumn('biography', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_LONG, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'job_type'])->update();
        $table->addColumn('terms', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'biography'])->update();
        $table->addColumn('linkedin_url', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'terms'])->update();
        $table->addColumn('facebook_url', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'linkedin_url'])->update();
        $table->addColumn('twitter_url', 'string', ['null' => false, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'facebook_url'])->update();
        $table->addColumn('instagram_url', 'string', ['null' => true, 'limit' => 200, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'twitter_url'])->update();
        $table->addColumn('bounced', 'integer', ['null' => false, 'default' => '0', 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'instagram_url'])->update();
        $table->addColumn('bounce_date', 'datetime', ['null' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'bounced'])->update();
        $table->save();
        if($this->table('users')->hasIndex('login')) {
            $this->table("users")->removeIndexByName('login');
        }
        $this->table("users")->addIndex(['email','password'], ['name' => "login", 'unique' => false])->save();
        if($this->table('users')->hasIndex('created')) {
            $this->table("users")->removeIndexByName('created');
        }
        $this->table("users")->addIndex(['created'], ['name' => "created", 'unique' => false])->save();
    }
}
