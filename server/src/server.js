var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Base64 = require('js-base64').Base64;
var _ = require('lodash');

var state = {};


io.on('connection', function(socket){
    console.log('a user connected');
    console.log(state);
    var handshakeData = socket.request;
    var connected = JSON.parse(Base64.decode([handshakeData._query['_data']]));
    var user = {};
    user['id_user']= connected.id;
    user['host']= connected.sitename;
    user['_data']= connected;
    socket.broadcast.emit('connected_user', {user: user});

});



io.use(function(socket, next) {
    var handshakeData = socket.request;
    var connection = JSON.parse(Base64.decode([handshakeData._query['_data']]));
    connectUser(connection);
    next();

    socket.on('disconnect', function() {
        console.log('Got disconnect!');
        var disconnection = JSON.parse(Base64.decode([handshakeData._query['_data']]));
        disconnectUser(disconnection)
        var user = {};
        user['id_user']= disconnection.id;
        user['host']= disconnection.sitename;
        user['_data']= disconnection;
        socket.broadcast.emit('disconnected_user', {user: user});
    });
    // socket.on('connect', function() {
    //     var handshakeData = socket.request;
    //     var connected = JSON.parse(Base64.decode([handshakeData._query['_data']]));
    //     socket.broadcast.emit('connected_user', {user: connected});
    // });

    socket.on('getConnectedUsers', function() {
        console.log('getConnectedUsers',connection)
        var container = connection.db_name;
        stateContainer = state[container];
        socket.emit('hello', {ConnectedUsers: stateContainer});
    });

    socket.on('sendMessage', function(data) {
        console.log(state);
        console.log('sendMessage',data)
        var container = state[data.container];
        var to = _.find(container, {id_user: data.to})
        data.to = to;

        socket.broadcast.emit('newMessage', {message: data});
        // var container = connection.db_name;
        // stateContainer = state[container];
        // socket.emit('hello', {ConnectedUsers: stateContainer});
    });

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});

function connectUser(connection){
    // db_name as unique key for each companie
    var state_container = connection.db_name ;
    // console.log(state_container)
    if(typeof state[state_container] == 'undefined') state[state_container] = []

    var arr = _.toArray(state[state_container]);
    if(!_.find(arr, {id_user: connection.id})) {
        state[state_container].push(convert(connection));
    }
    console.log(arr);
}

function convert(obj) {
    var result = {}
    result['id_user'] = obj.id;
    result['_data'] = obj;
    return result
}

function disconnectUser(connection){
    var stateContainer = connection.db_name;
    var id_user = connection.id;
    var newState = state[stateContainer];

    var arr = _.toArray(newState);
    _.remove(arr, {id_user: id_user});
    state[stateContainer] = arr;
    console.log(state);
}

